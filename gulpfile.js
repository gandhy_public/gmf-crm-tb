var gulp = require("gulp"),
    sass = require("gulp-sass"),
    imagemin = require("gulp-imagemin"),
    webpack = require("webpack-stream");

// Style
gulp.task("sass", () => {
  return gulp.src("./dev/sass/**/*.scss")
      .pipe(sass().on('error', function(e){
          console.log(e);
        }))
      .pipe(gulp.dest("./assets/dist/css"));
})

// Javascript
gulp.task("script", () => {
  return gulp.src("./src/js/app.js")
      .pipe(webpack(require("./webpack.config.js")))
      .pipe(gulp.dest("./assets/dist/js"));
})

// Imagemin
gulp.task("imagemin", () => {
  return gulp.src("./dev/images/*")
      .pipe(imagemin({
        progressive: true
      }))
      .pipe(gulp.dest("./dist/images"))
})

// Browser sync And Watch
gulp.task("serve", () => {
  gulp.watch("./dev/sass/**/*.scss", ['sass']);
  // gulp.watch("./src/js/**/*.js", ['script']);
})
