<ul class="nav nav-tabs">
    <?php $menu = $this->session->userdata('menu'); ?>
    <?php foreach ($menu['tab_menu'] as $key => $value) { ?>
        <li <?php isActive($content, $value->PATH )?>>
            <a href="<?php echo base_url() . 'projects/' . $value->PATH  . '/' . $id_project ?>"> <?php echo $value->LABEL ?></a>
        </li>
    <?php } ?>
    
    <?php if ($content == 'admin/projects/crud_daily_menu') { ?>
        <li class="pull-right">
            <a id="print">
                <?php echo "<i class='fa fa-print' aria-hidden='true'></i>" ?>
            </a>
        </li>
    <?php } ?>

</ul>

<?php
    function isActive($content, $path){
        $src = 'customer/' . $path;
        $final = str_replace("view", "c", $content);
        if($src == $final) {
            echo 'class="active"';
        }
    }
?>