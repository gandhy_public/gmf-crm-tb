
<?php $this->load->view('admin/projects/header'); ?>

<section class="content-body">
    <div class="row">
        <div class="col-md-12">
            <div class="nav-tabs-custom">
                <?php $this->load->view('admin/projects/menu'); ?>
            </div>
        </div>
    </div>

    <div class="container-full">
        <div class="col-md-12">
            <div class="box box-solid"><?php $this->load->view('admin/projects/title_dashboard'); ?></div>
        </div>
    </div>

    <div class="container-full">
        <div class="piechart">
            <div class="row">
                <div class="col-lg-6 col-xs-12">
                        <div class="box box-solid">
                            <div class="box-header with-border">
                                <h3 class="box-title">JOBCARD STATUS</h3>
                                <div class="box-tools pull-right">
                                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                                </div>
                            </div>

                                <div class="box-body piechart col-md-4">
                                    <div class="sparkline_jc" data-type="pie" data-offset="90" data-width="100px" data-height="100px">
                                        <?php echo $total_jc_closed; ?>,<?php echo $total_jc_open; ?>,<?php echo $total_jc_progress; ?>
                                    </div>        
                                </div>

                                <div class="box-body piechart col-md-4">
                                    <ul class="chart-legend clearfix">
                                        <li><i class="fa fa-square text-aqua"></i> Open</li>
                                        <li><i class="fa fa-square text-yellow"></i> Progress</li>
                                        <li><i class="fa fa-square text-green"></i> Close</li>
                                    </ul>
                                </div>

                                <div class="box-body piechart col-md-4">
                                    <ul class="chart-legend clearfix">
                                        <li class="text-aqua"><b><?php echo $percentage_jc_open_pie; ?>%</b></li>
                                        <li class="text-yellow"><b><?php echo $percentage_jc_progress_pie; ?>%</b></li>
                                        <li class="text-green"><b><?php echo $percentage_jc_close_pie; ?>%</b></li>
                                    </ul>
                                </div>

                                <div class="clearfix"></div>
                        </div>
                </div>

                 <div class="col-lg-6 col-xs-12">
                            <div class="box box-solid">
                                <div class="box-header with-border">
                                    <h3 class="box-title">MDR STATUS</h3>
                                    <div class="box-tools pull-right">
                                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                                    </div>
                                </div>

                                    <div class="box-body piechart col-md-4">
                                        <canvas id="sparkline_mdr" width="100" height="100" />
                                    </div>

                                    <div class="box-body piechart col-md-4">
                                        <ul class="chart-legend clearfix">
                                            <li><i class="fa fa-square text-red"></i> Open</li>
                                            <li><i class="fa fa-square text-aqua"></i> Progress</li>
                                            <li><i class="fa fa-square text-green"></i> Close</li>
                                        </ul>
                                    </div>

                                    <div class="box-body piechart col-md-4">
                                        <ul class="chart-legend clearfix">
                                            <li class="text-red"><b><?php echo $percentage_mdr_open_pie; ?>%</b></li>
                                            <li class="text-aqua"><b><?php echo $percentage_mdr_progress_pie; ?>%</b></li>
                                            <li class="text-green"><b><?php echo $percentage_mdr_close_pie; ?>%</b></li>
                                        </ul>
                                    </div>

                                <div class="clearfix"></div>
                        </div>
                </div>
            </div>

            <div class="row">
                  
                        <div class="col-md-6">
                            <div class="container-full">
                                <div class="box box-solid">
                                    <div class="box-header with-border">
                                        <h3 id="jobcard-perphase" class="box-title">JOBCARD PERPHASE</h3>
                                        <div class="chart">
                                            <canvas id="bar-chart_146" width="100" height="50"></canvas>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="container-full">
                                <div class="box box-solid">
                                    <div class="box-header with-border">
                                        <h3 class="box-title">PROGRESS STATUS JOBCARD PER AREA</h3>
                                        <div class="chart">
                                            <canvas id="bar-chart_143" width="100" height="50"></canvas>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-12">
                                <div class="box box-solid">
                                    <div class="box-header with-border">
                                        <h3 class="box-title">PROGRESS STATUS MDR PER AREA</h3>
                                        <div class="chart">
                                            <canvas id="bar-chart_144" width="100" height="50"></canvas>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="row">
                        <div class="col-lg-8 col-xs-12">
                            <div class="box box-solid">
                                <div class="box-header with-border">
                                    <h3 class="box-title">MATERIAL STATUS</h3>

                                    <div class="box-tools pull-right">
                                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                        </button>
                                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                                    </div>
                                </div>
                                <div class="box-body col-md-12 fonts">
                                     <canvas id="bar-material-status"></canvas>
                                </div>
                               
                                <div class="clearfix"></div>
                                <!-- /.box-body -->
                            </div>
                        </div>

                        <div class="col-lg-4 col-xs-12">
                            <div class="box box-solid">
                                <div class="box-header with-border">
                                    <h3 class="box-title">Close All Area</h3>

                                    <div class="box-tools pull-right">
                                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                        </button>
                                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                                    </div>
                                </div>

                                <div class="box-body chart-responsive">
                                    <div style="height: 380px; position: relative;">
                                        <div class="info-box">
                                        <span class="info-box-icon" style="background-color: #0dacc1;"><i class="fa fa-list-alt"></i></span>
                                           <div class="info-box-content fonts">
                                              <span class="info-box-text">Job Card Completion </span>
                                              <span class="info-box-number" style="font-size: 30px">
                                                
                                              </span>
                                              <span class="info-box-text">
                                                
                                              </span>
                                           </div>
                                        </div>

                                        <div class="info-box">
                                        <span class="info-box-icon" style="background-color: #ffff1a;"><i class="fa fa-list-alt"></i></span>
                                           <div class="info-box-content fonts">
                                              <span class="info-box-text">MDR Completion </span>
                                              <span class="info-box-number" style="font-size: 30px">
                                                
                                              </span>
                                              <span class="info-box-text">
                                                
                                              </span>
                                           </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.box-body -->
                            </div>
                            <!-- /.box -->
                        </div>
                        <!-- /.col -->
                        </div>
                        <div class="row">
                        
                                <div class="col-md-12">
                                    <div class="container-full">
                                        <div class="box box-solid">
                                            <div class="box-header with-border">
                                                <h3 class="box-title">JOBCARD BY SKILL</h3>
                                             </div>

                                            <div class="chart">
                                                <canvas id="bar-chart_122" width="100" height="50"></canvas>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- end posting area -->
                          
                        <!-- end wrapper -->
                        </div>
                            </div>
                        </div>
                    </div>
            </div>
        </div>
    </div>
</section>

<?php 

    // Jobcard Per Phase 
    $jc_per_phase_title = array();
    $jc_per_phase_data = array();
    foreach ($jobcard_per_phase_bar as $val) {
        array_push($jc_per_phase_title, $val->phase);
        array_push($jc_per_phase_data, $val->count);
    }
?> 

<!-- <script src="<?php echo base_url(); ?>assets/bower_components/chart.js/Chart.min.js"></script> -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.min.js"></script>

<script type="text/javascript">
    // Integer Generator
    function getRandomInt(min, max) {
            return Math.floor(Math.random() * (max - min + 1)) + min;
    }

    // color
    var red = 'rgba(221, 75, 57, 1)'
    var green = 'rgba(0,166,90, 1)'
    var blue = 'rgba(0,192,239, 1)'
    var yellow = 'rgba(243,156,18, 1)'

   $(function() {
      $('.sparkline_jc').sparkline('html',
      {
          type: 'pie',
          height: '11.0em',
          sliceColors: ['#00A65A', '#00c0ef', '#F7BA1C'],
          highlightLighten: 1.1,
          tooltipFormat: '{{value}} item - ({{percent.1}}%)',
      });
    });

   // Sparkline Mdr
    


   var SparklineMdrData = [
    <?php echo $percentage_mdr_open_pie; ?>,
    <?php echo $percentage_mdr_progress_pie; ?>,
    <?php echo $percentage_mdr_close_pie; ?>
   ] 

   // MDR STATUS
   var ctxSparklineMdr = document.getElementById("sparkline_mdr").getContext('2d');
   var chartSparklineMdr = new Chart(ctxSparklineMdr, {
        type: 'pie',
        data: {
            datasets: [{
                data: SparklineMdrData,
                backgroundColor: [red, blue, green],
                borderWidth: 0
            }],
            labels: ["Open", "Progress", "Close"]
        },
        options: {
            responsive: true,
            animation: {
                animateScale: true,
                animateRotate: true
            },
            legend: { display: false }
        }
    }); 

   // JOBCARD PERPHASE
   var dataJobCardPerPhase = [<?php echo implode(',', $jc_per_phase_data ); ?>];
   var labelJobCardPerPhase = [<?php echo '"'.implode('","', $jc_per_phase_title).'"' ?>];
   var ctxJobcardPerPhase = document.getElementById("bar-chart_146").getContext('2d');
   var chartJobcardPerPhase = new Chart(ctxJobcardPerPhase, {
    type: 'bar',
      data: {
            datasets: [{
                data: dataJobCardPerPhase,
                backgroundColor: [red, blue, green, yellow, red, blue, green],
                borderWidth: 0
            }],
            labels: labelJobCardPerPhase
        },
        options: {
            responsive: true,
            animation: {
                animateScale: true,
                animateRotate: true
            },
            legend: { display: false }
        }
   })

   // PROGRESS STATUS JOBCARD PER AREA
   var dataJobCardPerArea = [ getRandomInt(1,100), getRandomInt(1,100), getRandomInt(1,100), getRandomInt(1,100), getRandomInt(1,100), getRandomInt(1,100), getRandomInt(1,100), getRandomInt(1,100), getRandomInt(1,100), getRandomInt(1,100), getRandomInt(1,100), getRandomInt(1,100), getRandomInt(1,100), getRandomInt(1,100), getRandomInt(1,100) ]
   var labelJobCardPerArea = ["AREA","COCKPIT", "FUSELAGE", "LH-WING", "RH-WING", "L/G", "ENG#1", "ENG#2", "ENG#3", "ENG#4", "TAIL", "CABIN", "FWD CARGO", "AFT CARGO", "ELECT"];
   var ctxJobcardPerArea = document.getElementById("bar-chart_143").getContext('2d');
   var chartJobcardArea = new Chart(ctxJobcardPerArea, {
    type: 'bar',
      data: {
            datasets: [{
                data: dataJobCardPerArea,
                backgroundColor: [red, blue, green, yellow, red, blue, green, yellow, red, blue, green, yellow, red, blue, green, yellow,],
                borderWidth: 0
            }],
            labels: labelJobCardPerArea
        },
        options: {
            scales: {
              xAxes: [{
              ticks: {
                autoSkip: false
              }
            }]
            },
            responsive: true,
            animation: {
                animateScale: true,
                animateRotate: true
            },
            legend: { display: false },
        }
   })

var ctxOpenCloseComparassion = document.getElementById("bar-chart_144");  
var chartOpenCloseComparassion = new Chart(ctxOpenCloseComparassion, {
      type: 'bar',
      data: {
              labels: labelJobCardPerArea,
              datasets: [{
              label: 'Open',
              data: [
                  getRandomInt(1,100), 
                  getRandomInt(1,100), 
                  getRandomInt(1,100), 
                  getRandomInt(1,100), 
                  getRandomInt(1,100), 
                  getRandomInt(1,100), 
                  getRandomInt(1,100), 
                  getRandomInt(1,100),
                  getRandomInt(1,100), 
                  getRandomInt(1,100), 
                  getRandomInt(1,100), 
                  getRandomInt(1,100), 
                  getRandomInt(1,100), 
                  getRandomInt(1,100), 
                  getRandomInt(1,100), 
                  getRandomInt(1,100),
                  getRandomInt(1,100)
              ],
              backgroundColor: blue,
          },
          {
              label: 'Close',
              data: [
                  getRandomInt(1,100), 
                  getRandomInt(1,100), 
                  getRandomInt(1,100), 
                  getRandomInt(1,100), 
                  getRandomInt(1,100), 
                  getRandomInt(1,100), 
                  getRandomInt(1,100), 
                  getRandomInt(1,100),
                  getRandomInt(1,100), 
                  getRandomInt(1,100), 
                  getRandomInt(1,100), 
                  getRandomInt(1,100), 
                  getRandomInt(1,100), 
                  getRandomInt(1,100), 
                  getRandomInt(1,100), 
                  getRandomInt(1,100),
                  getRandomInt(1,100)
              ],
              backgroundColor: red
        }
      ]
      },
      options: {
          scales: {
              xAxes: [{
                  stacked: true,
                  ticks: {
                      autoSkip: false,
                      fontSize: 8    
                  }                
              }],
              yAxes: [{
                  stacked: true,
                  autoSkip: false,
              }]
          }
      }
  });


// MATERIAL STATUS
var labelMaterialStatus = ["NIL STOCK", "SOA", "CUSTSPLY", "ORDERED PURC", "CUSTOMER", "SHIPMENT"];
var dataMaterialStatus = [ getRandomInt(1,100), getRandomInt(1,100), getRandomInt(1,100), getRandomInt(1,100), getRandomInt(1,100), getRandomInt(1,100), getRandomInt(1,100) ]
var ctxMaterialStatus = document.getElementById("bar-material-status");
var chartMaterialStatus = new Chart(ctxMaterialStatus, {
  type: 'horizontalBar',
  data: {
          datasets: [{
              data: dataJobCardPerArea,
              backgroundColor: [red, blue, green, yellow, red, blue],
              borderWidth: 0
          }],
          labels: labelMaterialStatus
      },
      options: {
          responsive: true,
          animation: {
              animateScale: true,
              animateRotate: true
          },
          legend: { display: false },
      }
    })

// JOBCARD BY SKIL
var labelJobCardSkill = ["A/P", "CBN", "E/A", "NDT", "PAINTING", "STR", "TBR-SEAL", "TBR-SHOP"];
var ctxJobcardBySkill = document.getElementById("bar-chart_122");
var chartJobCardBySkill = new Chart(ctxJobcardBySkill, {
  type: 'bar',
      data: {
        labels: labelJobCardSkill,
        datasets: [{
            label: 'Open',
            data: [
                getRandomInt(1,100), 
                getRandomInt(1,100), 
                getRandomInt(1,100), 
                getRandomInt(1,100), 
                getRandomInt(1,100), 
                getRandomInt(1,100),
                getRandomInt(1,100),
                getRandomInt(1,100)
            ],
            backgroundColor: blue,
          },
          {
            label: 'Close',
            data: [
                getRandomInt(1,100), 
                getRandomInt(1,100), 
                getRandomInt(1,100), 
                getRandomInt(1,100), 
                getRandomInt(1,100), 
                getRandomInt(1,100),
                getRandomInt(1,100),
                getRandomInt(1,100)
            ],
            backgroundColor: red,
          }
        ]
      },
        options: {
            scales: {
                xAxes: [{
                    stacked: true,
                    ticks: {
                        autoSkip: false,
                        fontSize: 8   
                    }                
                }],
                yAxes: [{
                    stacked: true,
                    autoSkip: false,
                    fontSize: 8
                }]
            }
      }
})


</script>



