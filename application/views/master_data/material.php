<section class="content-header">
    <h1>
        Master Data Material
    </h1>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header with-border">
                    <div class="box-tools">
                        <button class="btn btn-info" id="addNew" data-toggle="modal" data-target="#modalMaterial">Add New</button>
                        <button class="btn btn-info" id="import" data-toggle="modal" data-target="#modalUploadFile">Import</button>
                        <button class="btn btn-warning">Filter</button>
                    </div>
                    <!-- /.box-tools -->
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table class="table table-bordered table-hover dataTable" id="material-list">
                        <thead>
                            <tr>
                                <!-- <th width="20px">No.</th> -->
                                <th>Part Number</th>
                                <th width="20%">Alternatif Part Number</th>
                                <th width="20%">Description</th>
                                <th width="20%">Material Type</th>
                                <th width="15%">Action</th>
                            </tr>
                        </thead>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- nav-tabs-custom -->
        <!-- /.col -->
    </div>
</section>

<div id="modalUploadFile" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Upload Master Data Material</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" id="form-upload" enctype="multipart/form-data" >
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="email">Order Number</label>
                        <div class="col-sm-9">
                            <input type="file" class="form-control" id="material_file" placeholder="Enter Order Number" name="media">
                            <small id="order_number_help" class="text-danger" style="display: none;">
                                Try another! Order Number Not Found.
                            </small> 
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-offset-3 col-sm-9">
                            <button type="submit" class="btn btn-primary" id="upload_data">Upload Data <i class="fa fa-upload" aria-hidden="true"></i></button>

                            <a href="<?php echo base_url() . 'assets/uploads/files/template_import_material.xlsx'?>" class="btn btn-primary" id="download">Download Template <i class="fa fa-download" aria-hidden="true"></i></a>
                        </div>
                    </div>                    
                </form>
            </div>
        </div>

    </div>
</div>

<div id="modalMaterial" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Add New Material</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal">
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="email">PART NUMBER</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="MATNR"/>
                        </div>
                    </div>  
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="email">ALT. PART NUMBER</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="ALT_PART_NUMBER"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="email">DESCRIPTION</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="MAKTX"/>
                        </div>
                    </div>    
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="email">MAT. TYPE</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="CTG"/>
                        </div>
                    </div>                                                                     
                                                                                                            
                    <div class="form-group">
                        <div class="col-md-offset-3 col-sm-9">
                            <a class="btn btn-primary" id="save_material" required>Save</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>

    </div>
</div>


<style>
    .box-header > .box-title, .box-header > .box-tools {
        position: relative;
        display: inline-block;
        vertical-align: middle;
        right: 0;
        top:0;
    }

</style>


<script type="text/javascript">
	var report_table = $('#material-list').dataTable({
		paging: false
	});

	load_material();
	function load_material(){
		var url = '<?php echo base_url() ?>' + '/admin/Master_data/crud_material_load_all'
		$.ajax({
			type: 'POST',
			url: url,
			dataType: 'text',
			success: function(data){
				var response = $.parseJSON(data);
				if(response.status === 'success') {
					performing_datatables(response.body);
				}
			}
		})
	}


	function performing_datatables(data){
		report_table = $('#material-list').dataTable({
		    destroy: true,
		    data: data,
		    columns: [
		        // { title: 'No', data: 'ROW_NUM' },
		        { title: 'PART NUMBER', data: 'MATNR' },
		        { title: 'ALT. PART NUMBER', data: 'ALT_PART_NUMBER' },
		        { title: 'DESCRIPTION', data: 'MAKTX' },
		        { title: 'MAT. TYPE', data: 'CTG' },
		        { title: 'ACTION', data: 'MATNR', render: function(data, type, row){
		            return " <button class='btn btn-sm btn-danger delete' data-id='"+data+"' onclick='delete_material(\""+data+"\")'> <i class='fa fa-trash-o' aria-hidden='true'></i> </button>"
		        } }
		    ]     
		})
	}	

	$(document).ready(function(){
		$("#form-upload").submit(function(evt){  
			evt.preventDefault();
			var formData = new FormData($(this)[0]);
			console.log(formData);

			$.ajax({
				url: "<?php echo base_url('admin/master_data/read_xlsx_file_material')?>",
				type: 'POST',
				data: formData,
				dataType: 'text',
				async: false,
				cache: false,
				contentType: false,
				enctype: 'multipart/form-data',
				processData: false,
				success: function (data) {
					var response = $.parseJSON(data);
					if(response.status === 'success'){
					        swal({
					            title: 'Import Material Success',
					            icon: 'success',
					            confirmButtonText: 'Ok'
						})
						$('#modalUploadFile').modal('hide');
					}
				}
			});

			return false;
		});

		$('#save_material').click(function(){
			var data = {
				MATNR: 			$('#MATNR').val(),
				ALT_PART_NUMBER: 	$('#ALT_PART_NUMBER').val(),
				MAKTX: 			$('#MAKTX').val(),
				CTG: 			$('#CTG').val(),
			}

			var url = ' <?php echo base_url() ?>' + 'admin/master_data/crud_material_add';
			$.ajax({
				url: url,
				method: 'POST',
				dataType: 'text',
				data: data,
				success: function(data){
					var response = $.parseJSON(data);
					if (response.status === 'success') {
					        swal({
					            title: 'Record has been saved!',
					            icon: 'success',
					            confirmButtonText: 'Ok'
						})
						$('#MATNR').val('')
						$('#ALT_PART_NUMBER').val('')
						$('#MAKTX').val('')
						$('#CTG').val('')
						$('#modalMaterial').modal('hide');						
					}
				}
			}) 

		})
    })
    
    function delete_material(id){
        var url = ' <?php echo base_url() ?>' + 'admin/master_data/crud_material_delete';
        swal({
                title: "Warning!",
                text: "do you want to delete this record?",
                icon: "warning",
                buttons: true,
                dangerMode: true,
        })
        .then(function(isConfirm){
            if (isConfirm) {
                $.ajax({
                    url: url,
                    method: 'POST',
                    data: { MATNR: id },
                    dataType: 'text',
                    success: function(data){
                        load_material();
                        var response = $.parseJSON(data);
                        if(response.status === 'success'){
                            swal({
                                title: 'Delete Success',
                                icon: 'success',
                                confirmButtonText: 'Ok'
                            })
                        }
                    }
                })
            }        
        }) 
    }


</script>