<style >
table {
  /* display: block; */
  width: 100%;
}
</style>

<section class="content-header">
    <h4>
      <?php echo strtoupper($title) ?>
      <small></small>
    </h4>
</section>

<!-- Main content -->
<section class="content">
  
    <?= $this->session->flashdata('alert')?>
  
  <div class="row">
    <div class="col-md-12">
      <!-- Custom Tabs -->
      <div class="nav-tabs-custom">
        <!-- <?php //$this->load->view($nav_tabs); ?> -->
        <!-- /.Grocery CRUD -->
        <div class="tab-content">
          <section class="content">
            <!-- <div class="box-header">
              
            </div> -->
            <div class="box-body">
              
              <div class="section">
                <div class="col-lg-12 col-md-12 col-xs-12">
                  <div>

                   <!-- Nav tabs -->
                   <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#list_group" aria-controls="profile" role="tab" data-toggle="tab">List Group</a></li>
                     <li role="presentation" ><a href="#create_group" aria-controls="home" role="tab" data-toggle="tab">Create Group</a></li>  
                     <!-- <li role="presentation" ><a href="#list_menu" aria-controls="home" role="tab" data-toggle="tab">List Menu</a></li> -->  
                   </ul>

                   <!-- Tab panes -->
                   <div class="tab-content">
                      <div role="tabpanel" class="tab-pane fade in active" id="list_group">
                       <div class="box">
                         <div class="box-body">
                           <table id="tbGroup" class="table table-bordered table-hover table-striped" width="100%">
                             <thead style="background-color: #3c8dbc; color:#ffffff;">
                               <tr>
                                 <th> No </th>
                                 <th> Name Group </th>
                                 <th> Status </th>
                                 <th> Act </th>
                               </tr>
                             </thead>
                             <tbody>

                             </tbody>
                           </table>
                         </div>
                       </div>
                      </div>
                      <div role="tabpanel" class="tab-pane fade" id="create_group">
                        <div class="box">
                          <div class="box-body">
                            <form action="<?= base_url('index.php')."/management/create_group"?>" method="post">
                              <div class="col-lg-12 col-xs-12">
                                <div class="form-horizontal">
                                  <div class="form-group">
                                    <label class="col-sm-3 control-label">Name Group</label>
                                    <div class="col-sm-6">
                                      <input type="text" class="form-control input-sm" id="group" name="group" required> 
                                    </div>
                                  </div>
                                  <?php
                                  $modul = array_keys($data_menu);
                                  for($a=0;$a<count($modul);$a++){
                                  ?>
                                  <div class="form-group">
                                    <label class="col-sm-3 control-label">Select Menu in Module <?= $modul[$a]?></label>
                                    <div class="col-sm-6">   
                                      <select class="js-example-placeholder-multiple js-states form-control modul_menu" name="<?=$modul[$a]?>[]" id="<?=$modul[$a]?>" multiple="multiple">
                                        <?php
                                        for($b=0;$b<count($data_menu[$modul[$a]]);$b++){
                                        ?>
                                        <option value="<?= $data_menu[$modul[$a]][$b]['id']?>_<?= $data_menu[$modul[$a]][$b]['modul_id']?>"><?= $data_menu[$modul[$a]][$b]['menu']?></option>
                                        <?php }?>
                                      </select>
                                    </div>
                                  </div>
                                  <?php
                                  if($modul[$a] == 'PROJECT'){
                                  ?>
                                  <div class="form-group tab-menu">
                                    <label class="col-sm-3 control-label">Tab Menu Project</label>
                                    <div class="col-sm-6">
                                      <?php foreach($tab_project as $key){?>
                                      <input type="checkbox" name="<?= $key->modul?>_tab[]" value="<?= $key->menu_id.'_'.$key->modul_id?>"> <?= $key->menu?> &nbsp&nbsp&nbsp
                                      <?php }?>
                                    </div>
                                  </div>
                                  <?php
                                  }
                                  ?>
                                  <?php }?>
                                  <div class="form-group">
                                    <div class="col-sm-4">
                                      <button type="submit" class="btn btn-success pull-right">Create Group</button>      
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </form>
                          </div>
                        </div>
                      </div>
                      <div role="tabpanel" class="tab-pane fade" id="list_menu">
                       <div class="box">
                         <div class="box-body">
                           <table id="tbUser" class="table table-bordered table-hover table-striped" width="100%">
                             <thead style="background-color: #3c8dbc; color:#ffffff;">
                               <tr>
                                 <th> No </th>
                                 <th> Name Module </th>
                                 <th> Name Menu </th>
                                 <th> Status </th>
                                 <th> Act </th>
                               </tr>
                             </thead>
                             <tbody>

                             </tbody>
                           </table>
                         </div>
                       </div>
                      </div>
                   </div>
                 </div>
                </div>
              </div>
            </div>
          </section>
        </div>
        <!-- /.tab-pane -->
      </div>
      <!-- /.tab-content -->
    </div>
    <!-- nav-tabs-custom -->
    <!-- /.col -->
  </div>

</section>

<!-- Form Edit Group Modal -->
<div class="modal fade" id="modalEditGroup" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <form id="formEditGroup" method="post" action="<?= base_url('index.php/management/update_password')?>">
        <div class="modal-header">
          <h3 class="modal-title" id="exampleModalLongTitle">Form Edit Group</h3>
        </div>
        <div class="modal-body col-sm-12">
          <input type="hidden" id="id-user" name="id">
          <div class="col-lg-12 col-xs-12">
            <div class="form-horizontal">
              <div class="form-group">
                <label class="col-sm-4 control-label">Name Group</label>
                <div class="col-sm-8">
                  <input type="text" class="form-control input-sm" name="group_name" required> 
                </div>
              </div>
              <?php
              $modul = array_keys($data_menu);
              for($a=0; $a < count($modul); $a++){
              ?>
              <div class="form-group">
                <label class="col-sm-4 control-label">Select Menu in Module <?= $modul[$a]?></label>
                <div class="col-sm-8">   
                  <select class="js-example-placeholder-multiple js-states form-control modul_menu_edit" name="<?=$modul[$a]?>_edit" id="<?=$modul[$a]?>_edit" multiple="multiple">
                    <?php
                    for($b=0;$b<count($data_menu[$modul[$a]]);$b++){
                    ?>
                    <option value="<?= $data_menu[$modul[$a]][$b]['id']?>_<?= $data_menu[$modul[$a]][$b]['modul_id']?>"><?= $data_menu[$modul[$a]][$b]['menu']?></option>
                    <?php }?>
                  </select>
                </div>
              </div>
              <?php } ?>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary" disabled>Update Group</button>
        </div>
      </form>
    </div>
  </div>
</div>

<script>
  $(document).ready(function(){
    $('.modul_menu, .modul_menu_edit').select2({
      placeholder: "Select a menu",
      multiple: true,
      width: '100%',
    });

    $('.modul_menu').on('select2:selecting', function(e) {
      if(e.params.args.data.text == 'PROJECT'){
        $('.tab-menu').show();
      }
    });
    $('.modul_menu').on('select2:unselecting', function (e) {
      if(e.params.args.data.text == 'PROJECT'){
        $('.tab-menu').hide();
      }
    });
    
    $('.tab-menu').hide();
    $('[data-toggle="tooltip"]').tooltip();

    $("#list_group").on("click", ".btn_edit", function() {
      var id_group = $(this).attr('data-x');
      $('input[name=group_name]').val('');
      // $('.modul_menu_edit').each(function() {
      //   $(this).select2("val", "");
      // });
      // $('#CUSTOMER_edit').select2("val", "");
      $("select.modul_menu_edit").each(function () {
        $(this).select2('val', '')
      });
      $.ajax({
        url: '<?= base_url('index.php/management/get_group_by_id'); ?>',
        type: 'POST',
        data: {
          id_group: id_group
        },
        success: function(response) {
          var data = $.parseJSON(response);
          $('input[name=group_name]').val(data[0]['GROUP_NAME']);
          $.each(data, function(key, item) {
            $('select[name="'+item.MODUL_NAME+'_edit"]').select2('val', [item.MODUL_ID+'_'+item.MENU_ID]);
          });
          // $('#customer_edit').select2('val', ['1_1']);
        }
      });
    });
  });
  $(function () {
    //$(".alert").hide(15000);
    $('.alert').delay(8000).fadeOut();

    $('#tbGroup').DataTable({
        serverSide: true,
        processing: true,
        ajax: '<?=base_url('index.php/management/list_group')?>',
        columns: [            
            {data: 'no', name: 'no'},            
            {data: 'group', name: 'group'},
            {data: 'status', name: 'status'},
            {data: 'act', name: 'act',orderable: false}
        ],
    });

  });
</script>
