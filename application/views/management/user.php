<?php $sess_login = $this->session->userdata('logged_in');?>
<style >
table {
  /* display: block; */
  width: 100%;
}
</style>

<section class="content-header">
    <h4>
      <?php echo strtoupper($title) ?>
      <small></small>
    </h4>
</section>

<!-- Main content -->
<section class="content">
  
    <?= $this->session->flashdata('alert')?>
  
  <div class="row">
    <div class="col-md-12">
      <!-- Custom Tabs -->
      <div class="nav-tabs-custom">
        <!-- <?php //$this->load->view($nav_tabs); ?> -->
        <!-- /.Grocery CRUD -->
        <div class="tab-content">
          <section class="content">
            <!-- <div class="box-header">
              
            </div> -->
            <div class="box-body">
              <div class="section">
                <div class="col-lg-12 col-md-12 col-xs-12">
                  <div>

                   <!-- Nav tabs -->
                   <ul class="nav nav-tabs" role="tablist">
                      <li role="presentation" class="active"><a href="#list_user" aria-controls="profile" role="tab" data-toggle="tab">List User</a></li>
                      <li role="presentation" ><a href="#create_user" aria-controls="home" role="tab" data-toggle="tab">Create User</a></li>
                      <li role="presentation" ><a href="#list_role" aria-controls="profile" role="tab" data-toggle="tab">List Role</a></li>
                      <li role="presentation" ><a href="#create_role" aria-controls="home" role="tab" data-toggle="tab">Create Role</a></li>    
                   </ul>

                   <!-- Tab panes -->
                   <div class="tab-content">

                      <div role="tabpanel" class="tab-pane fade in active" id="list_user">
                       <div class="box">
                         <div class="box-body">
                           <table id="tbUser" class="table table-bordered table-hover table-striped" width="100%">
                             <thead style="background-color: #3c8dbc; color:#ffffff;">
                               <tr>
                                 <th> No </th>
                                 <th> Name </th>
                                 <th> Username </th>
                                 <th> User Role </th>
                                 <th> Status </th>
                                 <th> Act </th>
                               </tr>
                             </thead>
                             <tbody>

                             </tbody>
                           </table>
                         </div>
                       </div>
                      </div>

                      <div role="tabpanel" class="tab-pane fade" id="create_user">
                        <div class="box">
                          <div class="box-body">
                            <form action="<?= base_url('index.php')."/management/create_user"?>" method="post">
                              <div class="col-lg-12 col-xs-12">
                                <div class="form-horizontal">
                                  <div class="form-group">
                                    <label class="col-sm-2 control-label">Name</label>
                                    <div class="col-sm-4">
                                      <input type="text" class="form-control input-sm" id="nama" name="nama" required> 
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <label class="col-sm-2 control-label">Username</label>
                                    <div class="col-sm-4">
                                      <input type="text" class="form-control input-sm" id="username" name="username" required> 
                                      <input type="checkbox" id="ldap" name="ldap"> Is LDAP ? 
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <label class="col-sm-2 control-label">Password</label>
                                    <div class="col-sm-4">
                                      <input type="password" class="form-control input-sm" id="password" name="password"> 
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <label class="col-sm-2 control-label">User Role</label>
                                    <div class="col-sm-4">
                                      <select class="form-control input-sm" id="role" name="role" style="width: 100%;" required>
                                        <option value="" disabled selected>Choose User Role</option>
                                        <?php 
                                          foreach ($role as $data) { 
                                        ?>
                                        <option value="<?= $data->ID_USER_GROUP ?>" data-x="<?= $data->LEVEL?>"><?= $data->USER_GROUP; ?></option>
                                        <?php } ?>
                                      </select> 
                                    </div>
                                  </div>
                                  <div class="form-group select-customer">
                                    <label class="col-sm-2 control-label">Customer</label>
                                    <div class="col-sm-4">
                                      <select class="form-control input-sm select2" id="customer" name="customer" style="width: 100%;">
                                        <option value="" disabled selected>Choose Customer</option>
                                        <?php 
                                          foreach ($customer as $data) { 
                                        ?>
                                        <option value="<?= $data->ID_CUSTOMER ?>"><?= $data->COMPANY_NAME; ?></option>
                                        <?php } ?>
                                      </select>         
                                    </div>
                                  </div>
                                  <div class="form-group select-work">
                                    <label class="col-sm-2 control-label">Work Area</label>
                                    <div class="col-sm-4">
                                      <select class="form-control input-sm select2" id="work" name="work" style="width: 100%;">
                                        <option value="" disabled selected>Choose Work Area</option>
                                        <?php 
                                          foreach ($work as $data) { 
                                        ?>
                                        <option value="<?= $data->ID_WORK_AREA ?>"><?= $data->WORK_AREA; ?></option>
                                        <?php } ?>
                                      </select>         
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <label class="col-sm-2 control-label">User Group</label>
                                    <div class="col-sm-4">
                                      <select class="form-control input-sm select2" id="group" name="group" style="width: 100%;" required>
                                        <option value="" disabled selected>Choose User Group</option>
                                        <?php 
                                          foreach ($group as $data) { 
                                        ?>
                                        <option value="<?= $data->id ?>"><?= $data->name; ?></option>
                                        <?php } ?>
                                      </select>    
                                    </div>
                                  </div>
                                  <div id="menu-top">
                                    
                                  </div>
                                  <div class="form-group">
                                    <div class="col-sm-4">
                                      <button type="submit" class="btn btn-success pull-right">Create User</button>      
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </form>
                          </div>
                        </div>
                      </div>

                      <div role="tabpanel" class="tab-pane fade" id="list_role">
                        <div class="box">
                          <div class="box-body">
                            <table id="tbRole" class="table table-bordered table-hover table-striped" width="100%">
                              <thead style="background-color: #3c8dbc; color:#ffffff;">
                                <tr>
                                  <th>No</th>
                                  <th>Role</th>
                                  <th>Level</th>
                                  <th>Act</th>
                               </tr>
                             </thead>
                             <tbody>

                             </tbody>
                           </table>
                         </div>
                       </div>
                      </div>

                      <div role="tabpanel" class="tab-pane fade" id="create_role">
                        <div class="box">
                          <div class="box-body">
                            <form action="<?= base_url('index.php')."/management/create_role"?>" method="post">
                              <div class="col-lg-12 col-xs-12">
                                <div class="form-horizontal">

                                  <div class="form-group">
                                    <label class="col-sm-2 control-label">Role Name</label>
                                    <div class="col-sm-4">
                                      <input type="text" class="form-control input-sm" id="role_name" name="role_name" required> 
                                    </div>
                                  </div>

                                  <div class="form-group">
                                    <label class="col-sm-2 control-label">Level</label>
                                    <div class="col-sm-4">
                                      <select class="form-control input-sm" id="level" name="level" style="width: 100%;" required>
                                        <option value="" disabled selected>Choose Role Level</option>
                                        <option value="1">Level 1</option>
                                        <option value="2">Level 2</option>
                                        <option value="3">Level 3</option>
                                        <option value="4">Level 4</option>
                                    </select>
                                    </div>
                                  </div>                                  
                                  
                                  <div class="form-group">
                                    <div class="col-sm-4">
                                      <button type="submit" class="btn btn-success pull-right">Create Role</button>      
                                    </div>
                                  </div>

                                </div>
                              </div>
                            </form>
                          </div>
                        </div>
                      </div>


                   </div>
                 </div>
                </div>
              </div>
            </div>
          </section>
        </div>
        <!-- /.tab-pane -->
      </div>
      <!-- /.tab-content -->
    </div>
    <!-- nav-tabs-custom -->
    <!-- /.col -->
  </div>

</section>

<!-- Form Edit Role Modal -->
<div class="modal fade" id="modalEditRole" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <form id="formEditRole" method="post" action="<?= base_url('index.php/management/update_role')?>">
        <div class="modal-header">
          <h3 class="modal-title" id="exampleModalLongTitle">Form Edit Role</h3>
        </div>
        <div class="modal-body col-sm-12">
          <input type="hidden" id="id_user_group" name="id_user_group">
          <div class="col-lg-12 col-xs-12">
            <div class="form-horizontal">
              <div class="form-group">
                <label class="col-sm-3">Role Name</label>
                <div class="col-sm-8">
                  <input type="text" class="form-control input-sm" name="user_group" required> 
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-3">Level</label>
                <div class="col-sm-8">
                  <input type="number" class="form-control input-sm" name="level" required> 
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-success">Save changes</button>
        </div>
      </form>
    </div>
  </div>
</div>

<!-- Form Edit Modal -->
<div class="modal fade" id="modalEdit" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <form id="formEditUser" method="post" action="<?= base_url('index.php/management/update_user')?>">
        <div class="modal-header">
          <h3 class="modal-title" id="exampleModalLongTitle">Form Edit User</h3>
          <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button> -->
        </div>
        <div class="modal-body col-sm-12">
          <input type="hidden" id="id-edit" name="id_user">
          <input type="hidden" id="password-edit" name="password_user">
          <input type="hidden" id="level-edit" name="level_user">
          <div class="col-lg-12 col-xs-12">
            <div class="form-horizontal">
              <div class="form-group">
                <label class="col-sm-3">Name</label>
                <div class="col-sm-8">
                  <input type="text" class="form-control input-sm" id="nama-edit" name="nama-edit" required> 
                </div>
              </div>
              <div class="form-group input-username-edit">
                <label class="col-sm-3">Username</label>
                <div class="col-sm-8">
                  <input type="text" class="form-control input-sm" id="username-edit" name="username-edit"> 
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-3">User Role</label>
                <div class="col-sm-8">
                  <select class="form-control input-sm" id="role-edit" name="role-edit" style="width: 100%;" required>
                    <option value="" disabled selected>Choose User Role</option>
                    <?php foreach ($role as $item): ?>
                      <option value="<?= $item->ID_USER_GROUP ?>" data-x="<?= $item->LEVEL?>"><?= $item->USER_GROUP; ?></option>
                    <?php endforeach; ?>
                  </select>                                                                
                </div>
              </div>
              <div class="form-group select-customer-edit">
                <label class="col-sm-3">Customer</label>
                <div class="col-sm-8">
                  <select class="form-control input-sm select2" id="customer-edit" name="customer-edit" style="width: 100%;">
                    <option value="" disabled selected>Choose Customer</option>
                    <?php foreach ($customer as $item): ?>
                      <option value="<?= $item->ID_CUSTOMER ?>"><?= $item->COMPANY_NAME; ?></option>
                    <?php endforeach; ?>
                  </select>         
                </div>
              </div>
              <div class="form-group select-work-edit">
                <label class="col-sm-3">Work Area</label>
                <div class="col-sm-8">
                  <select class="form-control input-sm select2" id="work-edit" name="work-edit" style="width: 100%;">
                    <option value="" disabled selected>Choose Work Area</option>
                    <?php 
                      foreach ($work as $data) { 
                    ?>
                    <option value="<?= $data->ID_WORK_AREA ?>"><?= $data->WORK_AREA; ?></option>
                    <?php } ?>
                  </select>         
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-3">User Group</label>
                <div class="col-sm-8">
                  <select class="form-control input-sm select2" id="group-edit" name="group-edit" style="width: 100%;" required>
                    <option value="" disabled selected>Choose User Group</option>
                    <?php foreach ($group as $item): ?>
                      <option value="<?= $item->id ?>"><?= $item->name; ?></option>
                    <?php endforeach; ?>
                  </select>                                                                
                </div>
              </div>
              <div id="menu-top-edit">
                
              </div>
              <?php if($sess_login['user_group_level'] == 1 || $sess_login['user_group_level'] == 2){?>
              <div class="form-group">
                <label class="col-sm-3">Status User</label>
                <div class="col-sm-8">
                  <select class="form-control input-sm" id="status-edit" name="status-edit" style="width: 100%;" required>
                    <option value="" disabled selected>Choose User Status</option>
                    <option value="1">Active</option>
                    <option value="0">Non Active</option>
                  </select>                                                                
                </div>
              </div>
              <?php }?>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Save changes</button>
        </div>
      </form>
    </div>
  </div>
</div>

<!-- Form Edit Password Modal -->
<div class="modal fade" id="modalPassword" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <form id="formEditPassword" method="post" action="<?= base_url('index.php/management/update_password')?>">
        <div class="modal-header">
          <h3 class="modal-title" id="exampleModalLongTitle">Form Edit Password User</h3>
          <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button> -->
        </div>
        <div class="modal-body col-sm-12">
          <input type="hidden" id="id-user" name="id">
          <div class="col-lg-12 col-xs-12">
            <div class="form-horizontal">
              <div class="form-group">
                <label class="col-sm-3">New Password</label>
                <div class="col-sm-8">
                  <input type="password" class="form-control input-sm" id="new_password" name="password" required> 
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-3">Confirm New Password</label>
                <div class="col-sm-8">
                  <i id="label-check-password" class="fa fa-check" aria-hidden="true" style="position: absolute;right: 25px;top: 8px;display: none"></i>
                  <input type="password" class="form-control input-sm" id="confirm_new_password" name="repassword" required> 
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary" disabled>Update Password</button>
        </div>
      </form>
    </div>
  </div>
</div>

<script>
  $(document).ready(function(){
    $('#customer').select2();
    $('#group').select2();

    $('#customer-edit').select2();
    $('#group-edit').select2();

    $('.select-customer').hide();
    $('.select-work').hide();

    <?php if(($sess_login['user_group'] == "Admin Hangar") && (!empty($sess_login['work_area_id']))){?>
      $('.select-work').show();
      $('#work').val("<?= $sess_login['work_area_id']?>").prop('disabled',true);
    <?php }?>
  });
  $(function () {
    //$(".alert").hide(15000);
    $('.alert').delay(8000).fadeOut();

    var table_user = $('#tbUser').DataTable({
        serverSide: true,
        processing: true,
        ajax: '<?=base_url('index.php/management/list_user')?>',
        columns: [            
            {data: 'no', name: 'no'},            
            {data: 'name', name: 'name'},
            {data: 'username', name: 'username'},
            {data: 'role', name: 'role'},
            {data: 'status', name: 'status'},
            {data: 'act', name: 'act',orderable: false}
        ],
    });

    var table_role = $('#tbRole').DataTable({
      serverSide: true,
      processing: true,
      ajax: '<?=base_url('index.php/management/list_role')?>',
      columns: [            
          {data: 'no', name: 'no'},            
          {data: 'user_group', name: 'role'},
          {data: 'level', name: 'level'},
          {data: 'act', name: 'act',orderable: false}
      ],
    });

    $('#role').on('change', function() {
      /*var role = $('#role option:selected').text();
      if(role === 'Customer'){
        $('.select-customer').show();
        $('.select-work').hide();
      }else if(role == 'Admin Hangar'){
        $('.select-customer').hide();
        $('.select-work').show();
      }else{
        $('.select-customer').hide();
        $('.select-work').hide();
      }*/

      var role2 = $('#role option:selected').attr('data-x');
      if(role2 == 4){
        $('.select-customer').show();
        $('.select-work').hide();
      }else if(role2 != 4){
        $('.select-customer').hide();
        $('.select-work').show();
      }else{
        $('.select-customer').hide();
        $('.select-work').hide();
      }
    })
    $('#role-edit').on('change', function() {
      /*var role = $('#role-edit option:selected').text();
      if(role === 'Customer'){
        $('.select-customer-edit').show();
        $('.select-work-edit').hide();
      }else if(role == 'Admin Hangar'){
        $('.select-customer-edit').hide();
        $('.select-work-edit').show();
      }else{
        $('.select-customer-edit').hide();
        $('.select-work-edit').hide();
      }*/

      var role2 = $('#role-edit option:selected').attr('data-x');
      if(role2 == 4){
        $('.select-customer-edit').show();
        $('.select-work-edit').hide();
      }else if(role2 != 4){
        $('.select-customer-edit').hide();
        $('.select-work-edit').show();
      }else{
        $('.select-customer-edit').hide();
        $('.select-work-edit').hide();
      }
    })

    $('#group').on('change', function(){
      var id_group = $('#group').val();
      $.ajax({
        url: '<?= base_url('index.php/management/get_menu_top'); ?>',
        type: 'POST',
        data: {
          id_group: id_group
        },
        success: function(response) {
          if(response != null){
            var menu = $.parseJSON(response);
            var html = "";
            $('#menu-top').empty();
            $('#menu-top').html("<hr>");
            $.each(menu, function(i, item) {
              html=$('<div class="form-group"><label class="col-sm-2 control-label">Setting Menu Tab '+ item.m +'</label><div class="col-sm-4"><input type="checkbox" name="config[]" value="'+id_group+'_'+item.c+'"> &nbsp&nbsp&nbspis WRITE</div></div>');
              $('#menu-top').append(html);
            })
          }else{
            $('#menu-top').empty();
          }
        }
      });
    });

    $('#group-edit').on('change', function(){
      var id_group = $('#group-edit').val();
      var id_user = $('#id-edit').val();
      $.ajax({
        url: '<?= base_url('index.php/management/get_menu_top'); ?>',
        type: 'POST',
        data: {
          id_group: id_group
        },
        success: function(response) {
          if(response != null){
            var menu = $.parseJSON(response);
            var html = "";
            $('#menu-top-edit').empty();
            $('#menu-top-edit').html("<hr>");
            $.each(menu, function(i, item) {
              html=$('<div class="form-group"><label class="col-sm-3 control-label">Setting Menu Tab '+ item.m +'</label><div class="col-sm-8"><input type="checkbox" name="config[]" value="'+id_group+'_'+item.c+'"> &nbsp&nbsp&nbspis WRITE</div></div>');
              $('#menu-top-edit').append(html);
            })
          }else{
            $('#menu-top-edit').empty();
          }
        },
        complete: function(response){
          $.ajax({
            url: '<?= base_url('index.php/management/get_menu_top_config'); ?>',
            type: 'POST',
            data: {
              id_group: id_group,
              id_user: id_user
            },
            success: function(resp) {
              var data = $.parseJSON(resp);
              $.each(data, function(i, item) {
                /*html=$('<div class="form-group"><label class="col-sm-3 control-label">Setting Menu Tab '+ item.m +'</label><div class="col-sm-8"><input type="checkbox" name="config[]" value="'+id_group+'_'+item.c+'"> &nbsp&nbsp&nbspis WRITE</div></div>');
                $('#menu-top-edit').append(html);*/

                $("input:checkbox[value='"+id_group+'_'+item+"']").attr("checked",true);
                console.log(id_group+'_'+item);
              })
              $(document).die();
            }
          });
        }
      });
    });

    $("#ldap").change(function() {
      if(this.checked) {
        $('#password').prop('disabled', true);
      }else{
        $('#password').prop('disabled', false);
      }
    });

    $("#list_user").on("click", ".btn_edit", function() {
      var id_user = $(this).attr('data-x');
      var ldap = $(this).attr('data-ldap');
      var id_group = $(this).attr('data-y');
     
      $.ajax({
        url: '<?= base_url('index.php/management/get_user_by_id'); ?>',
        type: 'POST',
        data: {
          id_user: id_user
        },
        success: function(response) {
          var data = $.parseJSON(response)[0];
          //console.log(data);
          $('#modalEdit').find('#id-edit').val(data.ID_USER);
          $('#modalEdit').find('#password-edit').val(data.PASSWORD);
          $('#modalEdit').find('#level-edit').val(data.LEVEL);
          $('#modalEdit').find('#nama-edit').val(data.NAME);
          if(ldap == 'no'){
            $('.input-username-edit').show();
            $('#modalEdit').find('#username-edit').val(data.USERNAME).prop('required',true);
          }else{
            $('.input-username-edit').hide();
            $('#modalEdit').find('#username-edit').val(data.USERNAME).prop('required',false);
          }
          $('#modalEdit').find('#role-edit').val(data.GROUP_ID);
          $('#modalEdit').find('#group-edit').val(data.GROUP_NAME_ID).trigger('change');
          $('#modalEdit').find('#status-edit').val(data.STATUS);

          //untum merubah work area
          var role_edit = $('#role-edit option:selected').text();
          var role_edit2 = $('#role-edit option:selected').attr('data-x');
          if(role_edit2 == 4){
            $('#modalEdit').find('#role-edit').prop('disabled',true);
            $('.select-customer-edit').show();
            $('.select-work-edit').hide();

            $('#modalEdit').find('#customer-edit').val(data.CUSTOMER_ID).trigger('change');
            $('#modalEdit').find('#work-edit').val("");
          }else if(role_edit2 != 4){
            $('#modalEdit').find('#role-edit').prop('disabled',false);
            $('.select-customer-edit').hide();
            $('.select-work-edit').show();

            $('#modalEdit').find('#customer-edit').val("");
            $('#modalEdit').find('#work-edit').val(data.WORK_AREA_ID);
          }else{
            $('#modalEdit').find('#role-edit').prop('disabled',false);
            $('.select-customer-edit').hide();
            $('.select-work-edit').hide();

            $('#modalEdit').find('#customer-edit').val("");
            $('#modalEdit').find('#work-edit').val("");
          }
        }
      });

    });

    $("#list_user").on("click", ".btn_reset", function() {
      var id_user = $(this).attr('data-x');
      $('#formEditPassword').find('#id-user').val(id_user);
      $('#formEditPassword').find('#new_password').val('');
      $('#formEditPassword').find('#confirm_new_password').val('');
      $('#formEditPassword').find('#label-check-password').hide();
      $('#formEditPassword').find('[type=submit]').prop('disabled', true);
    });

    /*$('#formEditUser').submit(function(event) {
      event.preventDefault();
      var form = $(this);
      var url = form.attr('action');
      var data = {
        id_user: form.find('#id-edit').val(),
        nama: form.find('#nama-edit').val(),
        username: form.find('#username-edit').val(),
        group_id: form.find('#role-edit').val(),
        customer_id: form.find('#customer-edit').val(),
        work_id: form.find('#work-edit').val(),
        group_name_id: form.find('#group-edit').val(),
        status: form.find('#status-edit').val(),
        config: form.find('#config-edit').val(),
      }
      $.ajax({  
        url: url,
        type: 'POST',
        data: data,
        success: function(response) {
          // console.log(response);
          $('#modalEdit').modal('hide');
          swal({
            title: "Success Update User",
            text: "Success Update User " + data.nama,
            icon: "success",
            timer: 1000,
            buttons: false
          }).then(
            function(){
              table_user.ajax.reload();
            }
          );
        }
      });
    });*/

    $('#formEditPassword').submit(function(event) {
      event.preventDefault();
      var form = $(this);
      var url = form.attr('action');
      var data = {
        id_user: form.find('#id-user').val(),
        password: form.find('#new_password').val(),
      }
      $.ajax({  
        url: url,
        type: 'POST',
        data: data,
        success: function(response) {
          $('#modalPassword').modal('hide');
          swal({
            title: "Success Update User Password",
            text: "Success Update User Password",
            icon: "success",
            timer: 1000,
            buttons: false
          }).then(
            function(){
              table_user.ajax.reload();
            }
          );
        }
      });
    });

    $('#new_password, #confirm_new_password').on('keyup', function () {
      if($('#confirm_new_password').val() !== '') {
        if($('#new_password').val() === $('#confirm_new_password').val()) {
          $('#label-check-password').removeClass('fa-close');
          $('#label-check-password').addClass('fa-check');
          $('#label-check-password').css({'display': 'block', 'color': 'green'});
          $('#formEditPassword').find('[type=submit]').prop('disabled', false);
        } else {
          $('#label-check-password').removeClass('fa-check');
          $('#label-check-password').addClass('fa-close');
          $('#label-check-password').css({'display': 'block', 'color': 'red'});
          $('#formEditPassword').find('[type=submit]').prop('disabled', true);
        }
      }
    });

    $("#list_role").on("click", ".btn_edit", function() {
      var id_user_group = $(this).attr('data-x');
      $.ajax({
        url: '<?= base_url('index.php/management/get_role_by_id'); ?>',
        type: 'POST',
        data: {
          id_user_group: id_user_group
        },
        success: function(response) {
          var data = $.parseJSON(response)[0];
          $('#modalEditRole').find('#id_user_group').val(data.ID_USER_GROUP);
          $('#modalEditRole').find('[name=user_group]').val(data.USER_GROUP);
          $('#modalEditRole').find('[name=level]').val(data.LEVEL);
        }
      });
    }); 

    $("#list_role").on("click", ".btn_delete", function() {
      var id_user_group = $(this).attr('data-x');
      $.ajax({
        url: '<?= base_url('index.php/management/delete_role'); ?>',
        type: 'POST',
        data: {
          id_user_group: id_user_group
        },
        success: function(response) {
          var data = $.parseJSON(response);
          if(data.status == 'success') {
            swal({
              title: "Success Delete Role",
              text: "Success Delete Role",
              icon: "success",
              timer: 1000,
              buttons: false
            }).then(
              function(){
                table_role.ajax.reload();
              }
            );
          } else {
            swal({
              title: "Failed Delete Role",
              text: "Role already used by user",
              icon: "error",
            });
          }
          // var data = $.parseJSON("response)[0];
          // $('#modalEditRole').find('#id_user_group').val(data.ID_USER_GROUP);
          // $('#modalEditRole').find('#user_group').val(data.USER_GROUP);
          // $('#modalEditRole').find"('#level').val(data.LEVEL);
        }
      });
    });

    $('#formEditRole').submit(function(event) {
      event.preventDefault();
      var form = $(this);
      var url = form.attr('action');
      var data = {
        id_user_group: form.find('#id_user_group').val(),
        user_group: form.find('[name=user_group]').val(),
        level: form.find('[name=level]').val(),
      }
      $.ajax({  
        url: url,
        type: 'POST',
        data: data,
        success: function(response) {
          $('#modalEditRole').modal('hide');
          swal({
            title: "Success Update Role",
            text: "Success Update Role",
            icon: "success",
            timer: 1000,
            buttons: false
          }).then(
            function(){
              table_role.ajax.reload();
            }
          );
        }
      });
    });
  });
</script>
