<style >
table {
  /* display: block; */
  width: 100%;
}
</style>

<section class="content-header">
    <h4>
      <?php echo strtoupper($title) ?>
      <small></small>
    </h4>
</section>

<!-- Main content -->
<section class="content">
  
    <?= $this->session->flashdata('alert')?>
  
  <div class="row">
    <div class="col-md-12">
      <!-- Custom Tabs -->
      <div class="nav-tabs-custom">
        <!-- <?php //$this->load->view($nav_tabs); ?> -->
        <!-- /.Grocery CRUD -->
        <div class="tab-content">
          <section class="content">
            <!-- <div class="box-header">
              
            </div> -->
            <div class="box-body">
              
              <div class="section">
                <div class="col-lg-12 col-md-12 col-xs-12">
                  <a href="<?= base_url('index.php')?>/management/menu"><button type="button" class="btn btn-primary pull-right">Back To List Group</button></a>
                  <div>

                   <!-- Nav tabs -->
                   <ul class="nav nav-tabs" role="tablist">
                     <li role="presentation" class="active"><a href="#edit_group" aria-controls="home" role="tab" data-toggle="tab"><?= $title_tab?></a></li>
                   </ul>

                   <!-- Tab panes -->
                   <div class="tab-content">
                      <div role="tabpanel" class="tab-pane fade in active" id="edit_group">
                        <div class="box">
                          <div class="box-body">
                            <form action="<?= base_url('index.php')."/management/update_group"?>" method="post" id="form-edit-group">
                              <div class="col-lg-12 col-xs-12">
                                <div class="form-horizontal">
                                  <div class="form-group">
                                    <label class="col-sm-3 control-label">Status Group</label>
                                    <div class="col-sm-6">
                                      <select class="form-control input-sm" id="status" name="status" required>
                                        <option value="1">Active</option>
                                        <option value="0">Non-Active</option>
                                      </select>
                                    </div>
                                  </div>
                                  <hr>
                                  <input type="hidden" name="id_group" value="<?= $id_group?>"> 
                                  <div class="form-group">
                                    <label class="col-sm-3 control-label">Name Group</label>
                                    <div class="col-sm-6">
                                      <input type="text" class="form-control input-sm" id="group" name="group_name" required> 
                                    </div>
                                  </div>
                                  <?php
                                  $modul = array_keys($data_menu);
                                  for($a=0;$a<count($modul);$a++){
                                  ?>
                                  <div class="form-group">
                                    <label class="col-sm-3 control-label">Select Menu in Module <?= $modul[$a]?></label>
                                    <div class="col-sm-6">   
                                      <select class="js-example-placeholder-multiple js-states form-control modul_menu" name="<?=$modul[$a]?>[]" id="<?=$modul[$a]?>" multiple="multiple">
                                        <?php
                                        for($b=0;$b<count($data_menu[$modul[$a]]);$b++){
                                        ?>
                                        <option value="<?= $data_menu[$modul[$a]][$b]['id']?>_<?= $data_menu[$modul[$a]][$b]['modul_id']?>"><?= $data_menu[$modul[$a]][$b]['menu']?></option>
                                        <?php }?>
                                      </select>
                                    </div>
                                  </div>
                                  <?php
                                  if($modul[$a] == 'PROJECT'){
                                  ?>
                                  <div class="form-group tab-menu">
                                    <label class="col-sm-3 control-label">Tab Menu Project</label>
                                    <div class="col-sm-6">
                                      <?php foreach($tab_project as $key){?>
                                      <input type="checkbox" name="<?= $key->modul?>_tab[]" value="<?= $key->menu_id.'_'.$key->modul_id?>"> <?= $key->menu?> &nbsp&nbsp&nbsp
                                      <?php }?>
                                    </div>
                                  </div>
                                  <?php
                                  }
                                  ?>
                                  <?php }?>
                                  <div class="form-group">
                                    <div class="col-sm-4">
                                      <button type="submit" class="btn btn-success pull-right">Update Group</button>      
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </form>
                          </div>
                        </div>
                      </div>
                   </div>
                 </div>
                </div>
              </div>
            </div>
          </section>
        </div>
        <!-- /.tab-pane -->
      </div>
      <!-- /.tab-content -->
    </div>
    <!-- nav-tabs-custom -->
    <!-- /.col -->
  </div>

</section>

<script>
  $(document).ready(function(){
    <?php if($view == "y"){ ?>
      $("#form-edit-group :input").prop("disabled", true);
      $(".btn-success").hide();
    <?php } ?>

    $('.modul_menu').select2({
      placeholder: "Select a menu",
      multiple: true,
      width: '100%',
    });

    $('.modul_menu').on('select2:selecting', function(e) {
      if(e.params.args.data.text == 'PROJECT'){
        $('input[name="PROJECT_tab[]"]').attr("checked",false);
        $('.tab-menu').show();
      }
    });
    $('.modul_menu').on('select2:unselecting', function (e) {
      if(e.params.args.data.text == 'PROJECT'){
        $('input[name="PROJECT_tab[]"]').attr("checked",false);
        $('.tab-menu').hide();
      }
    });
    
    $('.tab-menu').hide();
    $('[data-toggle="tooltip"]').tooltip();

    $.ajax({
      url: '<?= base_url('index.php/management/get_group_by_id'); ?>',
      type: 'POST',
      data: {
        id_group: '<?= $id_group; ?>'
      },
      success: function(response) {
        var data = JSON.parse(response);
        $.each(data, function(key, item) {
          $('input[name=group_name]').val(item.name[0]);
          $('select[name=status]').val(item.status[0]);
          $('select[name="'+key+'[]"]').val(item.value).trigger('change');
          if(item.value == "2_2"){
            $('.tab-menu').show();
          }
        })
      }
    });

    $.ajax({
      url: '<?= base_url('index.php/management/get_group_top_by_id'); ?>',
      type: 'POST',
      data: {
        id_group: '<?= $id_group; ?>'
      },
      success: function(response) {
        var data = JSON.parse(response);
        $.each(data, function(key, item) {
          //$("input:checkbox[value='<?= $key->menu_id."_".$key->modul_id?>']").attr("checked",true);
          $('input[name="'+key+'"]').val(item.value).attr("checked",true);
        })
      }
    });
  });
</script>
