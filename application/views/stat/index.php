<!DOCTYPE html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/bootstrap-grid.css" type="text/css" media="screen">
	<!-- <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/bootstrap/dist/css/bootstrap.min.css"> -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/main.css" type="text/css" media="screen">
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/owl.carousel.min.css" type="text/css" media="screen">

	<script type="text/javascript" src="<?php echo base_url(); ?>assets/dist/js/jquery.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
	<script src="<?php echo base_url(); ?>bower_components/bootstrap/dist/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/dist/js/app-stat.js"></script> 
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/dist/js/owl.carousel.min.js"></script>	
	<script src="<?php echo base_url(); ?>assets/bower_components/moment/moment.js"></script>
	
        <title>GMF - Realtime Report</title>
    </head>
    <body class="_dark-bg">
    	<?php $this->load->view($content); ?>
    </body>
</html>