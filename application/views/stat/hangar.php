        <div class="detailcontainer">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 ">
                        <h1 class="_mb-x _text-primary-light ">Hangar <?= substr($VAWRK, -1) ?></h1>
                        <h4 class="_unmargin-t _text-light">Maintenance Project Control Board</h4>
                    </div>
                </div>
            </div>

            <div class="container">
                <div class="row owl-carousel">                                                                                                                
                </div>
            </div>
        </div>

<script type="text/javascript">
    var revnr_list = [];
    var VAWRK = '<?php echo $VAWRK ?>';
    function load_porjects(){
        var url = '<?php echo base_url('Statistic/getProjectByLocation') ?>';
        $.ajax({
            url: url,
            method: 'POST',
            dataType: 'text',
            data: { VAWRK: VAWRK },
            success: function(data) {
                var response = $.parseJSON(data);
                if ( response.status === 'success' ) {
                    $('.owl-carousel').html('');
                    appendProjectToView(response.body);
                    loader();
                }
            }
        })
    }

    function loader(){        
        $('.content-loader').html('<img src="<?php echo base_url('assets/dist/img/loader.svg');?>" style="height: 20px" />');
    }

    function appendProjectToView(data) {
        $.each(data, function(key, value) {
            revnr_list.push(value.REVNR);
            var content = ''
            + '<div class="col-xs-12 col-sm-12 col-md-12">'
            + '<div class="gmf-chart -dark -block _pt-x _pb-x _round-all-s _fb-1-light _mb-s">'
            + '<div class="chart-content">'
            + '<div class="infonumber _centered-text _text-yellow">' + value.TPLNR + '</div>'
            + '<p class="_text-light _centered-text _mb-x _mt-x">' + value.REVTX + '</p>'
            + '</div>'

            + '<div class="chart-footer _bt-1-light">'
            + '<p class="_text-light _centered-text _mb-x _mt-x">' + calculateDayOf(value.REVBD, value.REVED) + '</p>'
            + '</div>'

            + '<div class="chart-content _bt-1-light">'
            + '<div class="chartinfo _mt-x">'
            + '<div class="_centered-text infoitem">'
            + '<p class="_unmargin-t _text-primary-light _mb-x">JOBCARD</p>'
            + '<h2 class="_unmargin _text-light content-loader" id="jc_prog_'+ value.REVNR +'"></h2>'
            + '</div>'

            + '<div class="_centered-text infoitem">'
            + '<p class="_unmargin-t _text-primary-light _mb-x">MDR</p>'
            + '<h2 class="_unmargin _text-light content-loader" id="mdr_prog_'+ value.REVNR +'"></h2>'
            + '</div>'
            + '</div>'
            + '</div>'
            + '</div>'

            + '<div class="gmf-chart -dark -block _pt-x _pb-x _round-all-s _fb-1-light _mb-s">'
            + '<div class="chart-content">'
            + '<div class="chartitle _centered-text">HILITE</div>'
            + '<div class="_text-light" style="width:100%;overflow:auto"><pre width="number">'+ value.HILITE +'</pre></div>'
            + '</div>'
            + '</div>'

            + '<div class="gmf-chart -dark -block _pt-x _pb-x _round-all-s _fb-1-light _mb-s">'
            + '<div class="chart-content">'
            + '<div class="chartitle _centered-text">MATERIAL SHORTAGE</div>'
            + '<canvas id="chart_'+value.REVNR+'"></canvas>'
            + '</div>'
            + '</div>'

            + '<div class="gmf-chart -dark -block _pt-x _pb-x _round-all-s _fb-1-light _mb-s">'
            + '<div class="chart-content">'
            + '<div class="chartitle _centered-text">MHRS CONSUMED </div>'
            + '<div class="infonumber _centered-text content-loader" id="mhrs_cons_' + value.REVNR + '"></div>'
            + '</div>'
            + '<div class="chart-content _mt-x">'
            + '<div class="chartinfo">'
            + '<div class="_centered-text infoitem">'
            + '<p class="_unmargin-t _text-primary-light _mb-x">MHRS PLAN</p>'
            + '<h3 class="_unmargin _text-light content-loader" id="mhrs_plan_' + value.REVNR + '"></h3>'
            + '</div>'
            + '<div class="_centered-text infoitem">'
            + '<p class="_unmargin-t _text-primary-light _mb-x">MHRS ACT</p>'
            + '<h3 class="_unmargin _text-light content-loader" id="mhrs_act_' + value.REVNR + '"></h3>'
            + '</div>'
            + '</div>'
            + '</div>'
            + '</div>'

            // + '<div class="gmf-chart -dark -block _pt-x _pb-x _round-all-s _fb-1-light _mb-s">'
            // + '<div class="chart-content">'
            // + '<div class="chartitle _centered-text">MATERIAL CONSUMED ($) </div>'
            // + '<div class="infonumber _centered-text _text-red">53.85%</div>'
            // + '</div>'
            // + '<div class="chart-content _mt-x">'
            // + '<div class="chartinfo">'
            // + '<div class="_centered-text infoitem">'
            // + '<p class="_unmargin-t _text-primary-light _mb-x">MAT PLAN</p>'
            // + '<h3 class="_unmargin _text-light">95.96%</h3>'
            // + '</div>'
            // + '<div class="_centered-text infoitem">'
            // + '<p class="_unmargin-t _text-primary-light _mb-x">MAT ACT</p>'
            // + '<h3 class="_unmargin _text-light">95.96%</h3>'
            // + '</div>'
            // + '</div>'
            // + '</div>'
            // + '</div>'
            + '</div>';
            $('.owl-carousel').append(content);
        } )


        $.when.apply($, revnr_list).done(function(){
            getOrderProgress();
            getMhrsProgress();
            getMaterialShortage();
        })

        setCarousel();
    }

    function getOrderProgress(){
        var url = '<?php echo base_url('Projects/order_progress_count') ?>';
        var data = { REVLIST: JSON.stringify(revnr_list) };

        $.ajax({
            url:url,
            method: 'POST',
            dataType: 'text',
            data: data,
            success: function(data){
                var response = $.parseJSON(data);
                if (response.status === 'success') {
                    appendOrderProgress(response.body)
                }
            }
        })
    }

    function getMhrsProgress(){
        var url = '<?php echo base_url('Projects/order_mhrs_count') ?>';
        var data = { REVLIST: JSON.stringify(revnr_list) };

        $.ajax({
            url:url,
            method: 'POST',
            dataType: 'text',
            data: data,
            success: function(data){
                var response = $.parseJSON(data);
                if (response.status === 'success') {
                    appendMhrsProgress(response.body)
                }
            }
        })
    }

    function getMaterialShortage(){
        var url = '<?php echo base_url('Statistic/get_mat_shortage') ?>';
        var data = { REVLIST: JSON.stringify(revnr_list) };

        $.ajax({
            url:url,
            method: 'POST',
            dataType: 'text',
            data: data,
            success: function(data){
                var response = $.parseJSON(data);
                if (response.status === 'success') {
                    buildChartShortage(response.body);
                }
            }
        })
    }




    function setCarousel(){
        var item = $(".owl-carousel");
        item.owlCarousel({
            items:4,
            margin:5,
            loop:false,
            autoplay:true,
            autoplayTimeout:5000,
            autoplayHoverPause:true,
            responsiveClass:true,
            responsive:{
                0:{
                    items:1,
                    loop:false,
                    margin:5,
                    autoplay:true,
                    autoplayTimeout:5000,
                    autoplayHoverPause:true,
                },
                600:{
                    items:3,
                    loop:false,
                    margin:5,
                    autoplay:true,
                    autoplayTimeout:5000,
                    autoplayHoverPause:true,
                },
                1000:{
                    items:4,
                    loop:false,
                    margin:5,
                    autoplay:true,
                    autoplayTimeout:5000,
                    autoplayHoverPause:true,
                }
            }
        }); 
    }

    function appendOrderProgress(data){
        $('.owl-item div .gmf-chart .chart-content .chartinfo .infoitem #jc_prog_null').html('0%')
            $('.owl-item div .gmf-chart .chart-content .chartinfo .infoitem #mdr_prog_null').html('0%')
        $.each(data, function(key, value){
            $('.owl-item div .gmf-chart .chart-content .chartinfo .infoitem #jc_prog_'+value.REVNR).html(value.JC_PROGRESS + '%')
            $('.owl-item div .gmf-chart .chart-content .chartinfo .infoitem #mdr_prog_'+value.REVNR).html(value.MDR_PROGRESS + '%')
        })
    }

    function appendMhrsProgress(data){
        $('.owl-item div .gmf-chart .chart-content .chartinfo .infoitem #mhrs_plan_null').html('0')
        $('.owl-item div .gmf-chart .chart-content .chartinfo .infoitem #mhrs_act_null').html('0')
        $('.owl-item div .gmf-chart .chart-content #mhrs_cons_null').html('0%')
        $.each(data, function(key, value){
            $('.owl-item div .gmf-chart .chart-content .chartinfo .infoitem #mhrs_plan_'+value.REVNR).html(value.PLAN)
            $('.owl-item div .gmf-chart .chart-content .chartinfo .infoitem #mhrs_act_'+value.REVNR).html(value.ACTUAL)
            $('.owl-item div .gmf-chart .chart-content #mhrs_cons_'+value.REVNR).html(value.CONSUMED + '%')
            if (value.CONSUMED > 100 ) {
                $('.owl-item div .gmf-chart .chart-content #mhrs_cons_'+value.REVNR).removeClass('_text-green');
                $('.owl-item div .gmf-chart .chart-content #mhrs_cons_'+value.REVNR).addClass('_text-red');
            }else{
                $('.owl-item div .gmf-chart .chart-content #mhrs_cons_'+value.REVNR).removeClass('_text-red');
                $('.owl-item div .gmf-chart .chart-content #mhrs_cons_'+value.REVNR).addClass('_text-green');
            }
        })
    }

    function buildChartShortage(data){
        $.each(data, function(key, value){
            var ctx = document.getElementById("chart_"+value.REVNR);
            var elementId = "chart_"+value.REVNR;
            var chart_data = buildData(value.MATERIAL);
            generateChart(ctx, elementId, chart_data);
        })
    }


    // Owl Carousel
    
    $(document).ready(function(){
        load_porjects();


        // var item = $(".owl-carousel");
        // item.owlCarousel({
        //     items:4,
        //     loop:true,
        //     margin:5,
        //     autoplay:true,
        //     autoplayTimeout:5000,
        //     autoplayHoverPause:true,
        //     responsiveClass:true,
        //     responsive:{
        //         0:{
        //             items:1,
        //             loop:true,
        //             margin:5,
        //             autoplay:true,
        //             autoplayTimeout:5000,
        //             autoplayHoverPause:true,
        //         },
        //         600:{
        //             items:3,
        //             loop:true,
        //             margin:5,
        //             autoplay:true,
        //             autoplayTimeout:5000,
        //             autoplayHoverPause:true,
        //         },
        //         1000:{
        //             items:4,
        //             loop:true,
        //             margin:5,
        //             autoplay:true,
        //             autoplayTimeout:5000,
        //             autoplayHoverPause:true,
        //         }
        //     }
        // });
    });
    
    function generateChart(ctx, elementId, data){
        var defConfy = {
            beginAtZero: true,
            fontColor: 'rgba(236, 240, 241,1.0)'
        };
        var defConfx = {
            beginAtZero: true,
            fontColor: 'rgba(236, 240, 241,1.0)',
            autoSkip: false,
            fontSize: 8
        };
        var chart = new Chart(ctx, {
            type: 'bar',
            data: data,
            options: {
                scales: {
                    yAxes: [{
                        ticks: defConfy
                    }],
                    xAxes: [{
                        ticks: defConfx
                    }]
                },
                legend: {
                    display: false
                }
            }
        });
    }

    function buildData(material){
        var mat_data = {};
        var labels = [];
        var data = [];

        $.each(material, function(key, val){
            labels.push(val.LABELS)
            data.push(parseFloat(val.QTY))          
        })

        mat_data.labels = labels;        
        mat_data.datasets = [{
            // label: 'Dataset 1',
            backgroundColor: 'rgba(255, 99, 132, 0.2)',
            borderColor: 'rgba(255,99,132,1)',
            borderWidth: 1,
            data: data            
        }]
        return mat_data;
    }


    function calculateDayOf(start_date, finish_date) {
        var start = moment(start_date, 'YYYYMMDD')
        var finish = moment(finish_date, 'YYYYMMDD')
        var current = moment();

        /*console.log("Start " + start.format('DD MM YYYY'));
        console.log("Current " + current.format('DD MM YYYY'));
        console.log("Finish " + finish.format('DD MM YYYY'));*/

        var current_day = current.diff(start, 'days');
        var total_day = finish.diff(start, 'days');

        return "DAY " + current_day + 1 + ' OF ' + total_day;
    }


    function getHilte(hilite){
        return hilite ? "<pre> " + hilite : "";
    }
</script>    