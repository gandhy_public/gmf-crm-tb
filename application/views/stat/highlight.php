<div class="detailcontainer">
    <div class="container">
        <div class="row">
            <div class="col-md-12 ">
                <h1 class="_mb-x _text-primary-light ">Hangar</h1>
                <h4 class="_unmargin-t _text-light">Maintenance Project Control Board</h4>
            </div>
        </div>
    </div>

    <div class="container">
            <?php foreach ($workarea as $value) { ?>
                <div class="gmf-chart -dark _p-all-s _round-all-s _fb-1-light _mb-s hidden-btn row">
                    <!-- <div class="row"> -->
                    <div class="col-lg-3 col-md-4 col-sm-12 _flex-middle">
                        <img src="<?php echo base_url(); ?>assets/dist/img/ic_hangar.svg" alt="Hangar Icon" width="75px" height="75px">
                        <div class="chart-content">
                            <div class="infonumber _text-yellow">Hangar <?php echo substr($value->VAWRK, -1); ?></div>
                            <p class="_text-light _mb-x _mt-x"><?php echo $value->AC_COUNT; ?> Aircraft</p>
                        </div>
                    </div>
                    <div class="col-lg-9 col-md-8 col-sm-12 owl-carousel" id="<?php  echo $value->VAWRK ?>">              
                    </div>
                    <!-- </div> -->
                    <a href="<?php echo base_url('Statistic/hangar_project/' . $value->VAWRK ) ?>" class="rounded-btn">
                        <img src="<?php echo base_url(); ?>assets/dist/img/ic_btn.svg" alt="">
                    </a>                           
                </div>
            <?php } ?>    
    </div>
</div>

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->

<script type="text/javascript">
    var revnr_list = [];
    $(document).ready(function(){
        var url = '<?php echo base_url('Statistic/getWorkAreaWithProject') ?>';
        $.ajax({
            url: url,
            method: 'POST',
            dataType: 'text',
            success: function(data){
                var response  = $.parseJSON(data);
                if (response.status = 'success') {
                    $.each(response.body, function(key, value){
                        $.each(value.AC_LIST, function(idx, project){
                            var content = ''
                                + '<div class="_flex-middle progress-info">'
                                + '<img src="<?php echo base_url(); ?>assets/dist/img/ic_aircraft.svg" alt="Aircraft Icon" width="45px" height="60px">'
                                + '<div class="chart-content chart-progress">'
                                + '<div class="infonumber _text-yellow">'+  project.TPLNR +'</div>'
                                + '<p class="_text-light _mb-x _mt-x status_lbl" id="lbl_'+ project.REVNR +'"> Calculate... </p>'
                                + '<div class="prgoress-bar" id="bar_'+ project.REVNR +'">'
                                + '<span class="indicator"></span>'
                                + '</div>'
                                + '</div>'
                            $('#'+value.LOCATION).append(content);
                            revnr_list.push(project.REVNR);
                        })
                        var id = String(value.LOCATION).charAt(3);
                        setCarousel(value.LOCATION, value.AC_LIST.length);
                    })

                    $.when.apply($, revnr_list).done(function(){
                        if (revnr_list.length > 0) { get_progress_bar(revnr_list); }
                    })
                }
            }
        })

        function setCarousel(container, count){
            var item = $('#'+container);
            item.owlCarousel({
                items:3,
                loop: count > 3 ? true : false,
                margin:5,
                autoplay:true,
                autoplayTimeout:5000,
                autoplayHoverPause:true,
                responsiveClass:true,
            responsive:{
                0:{
                    items:1,
                    loop: count > 1 ? true : false,
                    margin:5,
                    autoplay:true,
                    autoplayTimeout:5000,
                    autoplayHoverPause:true,
                },
                600:{
                    items:2,
                    loop: count > 2 ? true : false,
                    margin:1,
                    autoplay:true,
                    autoplayTimeout:5000,
                    autoplayHoverPause:true,
                },
                1000:{
                    items:4,
                    loop:count > 4 ? true : false,
                    margin:5,
                    autoplay:true,
                    autoplayTimeout:5000,
                    autoplayHoverPause:true,
                }
            }
            }); 
        }


        function get_progress_bar(REVNR){
            var url = '<?php echo base_url('Statistic/getProgress') ?>';
            $.ajax({
                url: url,
                method: 'POST',
                dataType: 'text',
                data: { REVLIST: JSON.stringify(REVNR) },
                success: function(data){
                    var response = $.parseJSON(data);
                    if (response.status == 'success') {
                        appendCountToView(response.body);
                    }
                }
            })
        }      
    });

    function appendCountToView(data){
        $.each(data, function(key, val){
            var value = val.CLOSED + ' of ' + val.TOTAL + ' ( ' + val.PERCENTAGE + '% ) '
            $('.owl-carousel .progress-info .chart-progress #lbl_'+val.REVNR).html(value)
            $('.owl-carousel .progress-info .chart-progress #bar_'+val.REVNR+' .indicator').css({'display':'block', 'width':val.PERCENTAGE + '%'})
        })
    }         
</script>  
