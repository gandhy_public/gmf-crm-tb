<div id="modalAddMenu" class="modal fade" role="dialog">
    <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title" id="form-user-title"></h4>
        </div>
        <div class="modal-body">
            <form class="form-horizontal" id="form-menu">
                <div id="msg" class="alert">
                </div>
                <input type="text" class="form-control" name="MENU_ID" id="MENU_ID"  style="display:none">             
                <div class="form-group">
                    <label class="control-label col-sm-2" for="LABEL">Menu Label</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="LABEL" id="LABEL" placeholder="Enter Menu Label">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2" for="PATH">Path</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="PATH" id="PATH" placeholder="Enter Menu Path">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2" for="PARENT">Parent</label>
                    <div class="col-sm-10">
                        <select name="PARENT" id="PARENT" class="form-control">
                            <option value="0">No Parent</option>
                            <?php foreach ($list_menu as $value) { ?>
                                    <option value="<?php echo $value->MENU_ID; ?>"><?php echo $value->LABEL ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2" for="TYPE">Type</label>
                    <div class="col-sm-10">
                        <select name="TYPE" id="TYPE" class="form-control">
                            <option value="T">Tab Menu</option>
                            <option value="S">Side Menu</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2" for="LABEL">Menu Icon</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="ICON" id="ICON" placeholder="Enter FA Menu Icon">
                    </div>
                </div>                                                                                                                                         
            </form>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary" id="btn-save">Save</button>
        </div>
    </div>

    </div>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        $('#msg').hide();
        $('#btn-save').on("click", function(){
            
            var btn = $('#btn-save').html();
            var url_location = '';
            if(btn === 'Save'){
                url_location = '<?php echo base_url() ?>' + 'admin/user_management/add_new_menu';
            }else{
                url_location = '<?php echo base_url() ?>' + 'admin/user_management/update_menu';
            }
            
            var data = $('#form-menu').serializeArray();

            $.ajax({
                type: "POST",
                url: url_location,
                data: data,
                dataType: "text",
                cache: false,
                success: function(data){
                    var msg = $.parseJSON(data);
                    if(msg.status === 'failed'){
                        swal("Error saving Record!", {
                                icon: "error"
                        });
                    }else{
                        swal("User has been saved successfully!", {
                                icon: "success"
                        });

                        $('#MENU_ID').val('');
                        $('#LABEL').val('');
                        $('#PATH').val('');
                        $('#PARENT').val('0');
                        $('#TYPE').val('T');
                        $('#ICON').val('');    

                        datagrid.fetchGrid();
                    }
                }
            }); 
        })
    })
</script>