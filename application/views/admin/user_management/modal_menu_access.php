<div id="modalMenuAccess" class="modal fade" role="dialog">
    <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Menu Configuration</h4>
        </div>
        <div class="modal-body">
            <table class="table">
                <tr>
                    <td width="70%">
                        <input type="text" id="user_group_id" style="display: none;">
                        <select name="selected-menu" id="selected-menu" class="form-control all-menu">
                            <?php foreach ($list_menu as $value) { ?>
                                <option value="<?php echo $value->MENU_ID ?>"><?php echo $value->LABEL ?></option>
                            <?php } ?>
                        </select>
                    </td>
                    <td>
                        <label class="checkbox" style="margin-left: 20px">
                            <input type="checkbox" id="isReadOnly" name="isReadOnly">
                            Read Only
                        </label>
                    </td>
                    <td>
                        <button id="saveMenu" class="btn btn-primary btn-sm pull-right"></button>
                    </td>                    
                </tr>            
            </table>
            
            <div style="overflow-y:auto; max-height:300px";>
                <table class="table" id="datatbles-menu">
                    <thead>
                        <tr>
                            <th width="80%">Label</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
    </div>

    </div>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        $('.all-menu').select2();
        $('.select2').css('width', '100%');


        $('#msg').hide();

        // Save Button
        $('#saveMenu').on("click", function(){
            var btn = $('#saveMenu').html();
            var url_location = '';
            if(btn === 'Save'){
                url_location = '<?php echo base_url() ?>' + 'admin/user_management/add_new_menu_access';
            }else{
                url_location = '<?php echo base_url() ?>' + 'admin/user_management/update_user';
            }
            var isCheck = $('#isReadOnly').is(':checked') ? 'Y' : 'N';
            var data = {   
                        MENU_ID: $('#selected-menu').val(),
                        READ_ONLY: isCheck,
                        USER_GROUP_ID: $('#user_group_id').val(), 
            }

            $.ajax({
                type: "POST",
                url: url_location,
                data: data,
                dataType: "text",
                cache: false,
                success: function(data){
                    var msg = $.parseJSON(data);
                    if(msg.status === 'failed'){
                        swal("Error saving Record!", {
                                icon: "error"
                        });
                    }else{
                        swal("Menu Access has been saved successfully!", {
                                icon: "success"
                        });
                        getMenuAccessByUser(msg.USER_GROUP_ID);
                    }
                }
            }); 
        })

    });
</script>