<section class="content-header">
    <h1>
        Utility
        <small><?php echo $current_menu; ?></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active"><a href="<?php echo base_url(); ?>administrator/user_management/index">Utility</a></li>
        <li class="active"><?php echo $current_menu; ?> </li>
    </ol>
</section>