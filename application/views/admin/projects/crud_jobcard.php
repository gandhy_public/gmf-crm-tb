<?php 
    $this->load->view('admin/projects/header'); 
    $this->load->helper('auth');
    $is_customer = is_customer($session['user_group']);
    $is_edit = $write == 1 ? true : false;
    $id_project = json_encode($id_project);

?>
<style>
    table > thead > tr > th.editablegrid-ITVAL div a,
    table > thead > tr > th.editablegrid-SKILL div a,
    table > thead > tr > th.editablegrid-AREA div a,
    table > thead > tr > th.editablegrid-PHASE div a,
    table > thead > tr > th.editablegrid-DAY div a,
    table > thead > tr > th.editablegrid-DATECLOSE div a,
    table > thead > tr > th.editablegrid-STATUS div a,
    table > thead > tr > th.editablegrid-REMARK div a,
    table > thead > tr > th.editablegrid-DOC_SENT_STATUS div a,
    table > thead > tr > th.editablegrid-FREETEXT div a,
    table > thead > tr > th.editablegrid-DATEPROGRESS div a,
    table > thead > tr > th.editablegrid-CABINSTATUS div a 
    {
        color: #0097e6 !important;
    }
    
</style>

<!-- Main content -->
<section class="content">
    <!-- Live preview-->
    <div class="row">
        <div class="col-md-12">
            <!-- Custom Tabs -->
            <div class="nav-tabs-custom">
                <?php $this->load->view('admin/projects/menu'); ?>
                <div class="tab-content">
                    <section class="content" style="overflow: auto;">
                        <div id="message"></div>
                        <div id="wrap">
                            <!-- <h3>JOBCARD</h3>-->
                            <!-- Feedback message zone -->
                            <div id="toolbar">
                                <input type="text" id="filter" name="filter" placeholder="Filter :type any text here"  />
                                <!-- Check if User Is Customer -->
                                <?php if($is_customer){?>
                                    <div class="pull-right form-inline">
                                        <a href="<?php echo base_url('index.php/Projects/write_xlsx_file_jobcard?REVNR=' . json_decode($id_project) )?>" class="btn btn-info" id="download" target="_blank">Export Excel <i class="fa fa-download" aria-hidden="true"></i></a>
                                    </div>
                                <?php }elseif(!$is_customer && $is_edit) { ?>
                                    <div class="pull-right form-inline">
                                        <a href="<?php echo base_url('index.php/Projects/write_xlsx_file_jobcard?REVNR=' . json_decode($id_project) )?>" class="btn btn-info" id="download" target="_blank">Export Excel <i class="fa fa-download" aria-hidden="true"></i></a>                                       
                                        <button class="btn btn-info" data-toggle="modal" data-target="#modalUploadFile">
                                            Import Excel
                                            <i class="fa fa-upload" aria-hidden="true"></i>
                                        </button>
                                         <a target="_blank" href="<?php echo base_url('projects/crud_jobcard_new/'.json_decode($id_project)) ?>" class="btn btn-success" id="tableExcel">Table Excel <i class="fa fa-table" aria-hidden="true"></i></a>
                                    </div> 
                                <?php } ?>   
                            </div>
                            <!-- Grid contents -->
                            <div id="tablecontent">
                                <div class="lds-ellipsis"><div></div><div></div><div></div><div></div></div>
                            </div>
                            <!-- Paginator control -->
                            <div id="paginator"></div>
                        </div>
                        <script src="<?php echo base_url(); ?>assets/jquery-ui/js/jquery-ui-1.10.4.custom.min.js"></script>
                        <script type="text/javascript">
                            function highlightRow(rowId, bgColor, after)
                            {
                                var rowSelector = $("#" + rowId);
                                rowSelector.css("background-color", bgColor);
                                rowSelector.fadeTo("normal", 0.5, function () {
                                    rowSelector.fadeTo("fast", 1, function () {
                                        rowSelector.css("background-color", '');
                                    });
                                });
                            }

                            function highlight(div_id, style) {
                                highlightRow(div_id, style == "error" ? "#e5afaf" : style == "warning" ? "#ffcc00" : "#8dc70a");
                            }

                            function message(type, message) {
                                $('#message').html("<div class=\"notification  " + type + "\">" + message + "</div>").slideDown('normal').delay(1800).slideToggle('slow');
                            }

                            /**
                             updateCellValue calls the PHP script that will update the database.
                             */
                            function updateCellValue(editableGrid, rowIndex, columnIndex, oldValue, newValue, row, onResponse)
                            {
                                $.ajax({
                                    url: '<?php echo base_url(); ?>projects/crud_jobcard_update',
                                    type: 'POST',
                                    dataType: "html",
                                    data: {
                                        tablename: editableGrid.name,
                                        id: editableGrid.getValueAt(rowIndex, 1),
                                        newvalue: editableGrid.getColumnType(columnIndex) == "boolean" ? (newValue ? 1 : 0) : newValue,
                                        colname: editableGrid.getColumnName(columnIndex),
                                        coltype: editableGrid.getColumnType(columnIndex)
                                    },
                                    success: function (response)
                                    {
                                        // reset old value if failed then highlight row
                                        var success = response.search("ok") > 0;
                                        if (!success) {
                                            editableGrid.setValueAt(rowIndex, columnIndex, oldValue);
                                            display_toast('danger', 'Error to update, please check on Column ' + editableGrid.getColumnName(columnIndex) + ', Order : ' + editableGrid.getValueAt(rowIndex, 1) );
                                            // datagrid.fetchGrid();
                                        } else {
                                            display_toast('success', 'Order : ' + editableGrid.getValueAt(rowIndex, 1) + ', Column '+ editableGrid.getColumnName(columnIndex) + ' updated to ' + editableGrid.getValueAt(rowIndex, columnIndex));
                                            // datagrid.fetchGrid();
                                        }
                                        
                                    },
                                    error: function (XMLHttpRequest, textStatus, exception) {
                                        alert("Ajax failure\n" + errortext);
                                    },
                                    async: true
                                });

                            }

                            function DatabaseGrid()
                            {
                                this.editableGrid = new EditableGrid("V_JOBCARD_PROGRESS", {
                                    editmode: 'static',
                                    enableSort: true,

                                    /* Comment this line if you set serverSide to true */
                                    // define the number of row visible by page
                                    /*pageSize: 50,*/

                                    /* This property enables the serverSide part */
                                    serverSide: true,

                                    // Once the table is displayed, we update the paginator state
                                    tableRendered: function () {
                                        updatePaginator(this);
                                    },
                                    tableLoaded: function () {
                                        datagrid.initializeGrid(this);
                                    },
                                    modelChanged: function (rowIndex, columnIndex, oldValue, newValue, row) {
                                        updateCellValue(this, rowIndex, columnIndex, oldValue, newValue, row);
                                    }
                                });
                                this.fetchGrid();

                                $("#filter").val(this.editableGrid.currentFilter != null ? this.editableGrid.currentFilter : "");
                                if (this.editableGrid.currentFilter != null && this.editableGrid.currentFilter.length > 0)
                                    $("#filter").addClass('filterdefined');
                                else
                                    $("#filter").removeClass('filterdefined');

                            }

                            DatabaseGrid.prototype.fetchGrid = function () {
                                // call a PHP script to get the data
                                display_toast('info',"Wait! Processing Get Data");
                                this.editableGrid.loadJSON("<?php echo base_url(); ?>index.php/projects/crud_jobcard_load_all?edit=" + "<?= $write ?>" + "&REVNR=" + <?= $id_project ?>);
                                //console.log(aaa);
                                /*if(aaa != 0){
                                    console.log('gagal load data');
                                }else{
                                    console.log('sukses load data');
                                }*/
                                //console.log(aaa);

                                /*var xhr = new XMLHttpRequest();
                                console.log('UNSENT', xhr.status);

                                xhr.open('GET', '/server', true);
                                console.log('OPENED', xhr.status);

                                xhr.onprogress = function () {
                                  console.log('LOADING', xhr.status);
                                };

                                xhr.onload = function () {
                                  console.log('DONE', xhr.status);
                                };

                                xhr.send(null);*/
                            };

                            DatabaseGrid.prototype.initializeGrid = function (grid) {

                                var self = this;

                                // render for the action column
//                                grid.setCellRenderer("action", new CellRenderer({
//                                    render: function (cell, AUFNR) {
//                                        cell.innerHTML = "<button class='btn btn-sm btn-danger delete' data-id='"+AUFNR+"' onclick='jc_delete("+AUFNR+")'><i class='fa fa-trash-o' aria-hidden='true'></i> </button>";
//                                    }
//                                }));
//                            var mdr_link = '<?php echo base_url() ?>' + 'projects/crud_mdr/' + '<?php echo $id_project ?>' + '/';     
//                            grid.setCellRenderer("AUFNR", new CellRenderer({
//                                render: function(cell, AUFNR){
//                                    cell.innerHTML = "<a href='" + mdr_link + AUFNR + "/JC'>"+ AUFNR +"</a>"
//                                }
//                            }))

                                grid.setCellRenderer("select", new CellRenderer({
                                    render: function (cell, AUFNR) {
                                        if(AUFNR) {
                                            // cell.appendChild('<input type="checkbox" name="" id="">');
                                        }
                                    }
                                }));
                                
                                grid.renderGrid("tablecontent", "testgrid");
                                $('html, body').scrollTop(1);
                                $('html, body').scrollTop(0);
                               

                            };

                            DatabaseGrid.prototype.deleteRow = function (id)
                            {

                                var self = this;

                                if (confirm('Are you sur you want to delete the row id ' + id)) {

                                    $.ajax({
                                        url: '<?php echo base_url(); ?>projects/crud_jobcard_delete',
                                        type: 'POST',
                                        dataType: "html",
                                        data: {
                                            tablename: self.editableGrid.name,
                                            id: id
                                        },
                                        success: function (response)
                                        {
                                            if (response == "ok") {
                                                message("success", "Row deleted");
                                                self.fetchGrid();
                                            }
                                        },
                                        error: function (XMLHttpRequest, textStatus, exception) {
                                            alert("Ajax failure\n" + errortext);
                                        },
                                        async: true
                                    });


                                }

                            };


                            DatabaseGrid.prototype.addRow = function (id)
                            {
                                var self = this;
                                $.ajax({
                                    url: '<?php echo base_url(); ?>projects/crud_jobcard_add',
                                    type: 'POST',
                                    dataType: "html",
                                    data: {
                                        tablename: self.editableGrid.name,
                                        name: $("#name").val(),
                                        firstname: $("#firstname").val()
                                    },
                                    success: function (response)
                                    {
                                        if (response == "ok") {

                                            // hide form
                                            showAddForm();
                                            $("#name").val('');
                                            $("#firstname").val('');
                                            message("success", "Row added : reload model");
                                            self.fetchGrid();
                                        } else
                                            message("error", "Error occured");
                                    },
                                    error: function (XMLHttpRequest, textStatus, exception) {
                                        alert("Ajax failure\n" + errortext);
                                    },
                                    async: true
                                });
                            };

                            function updatePaginator(grid, divId)
                            {
                                divId = divId || "paginator";
                                var paginator = $("#" + divId).empty();
                                var nbPages = grid.getPageCount();

                                // get interval
                                var interval = grid.getSlidingPageInterval(20);
                                if (interval == null)
                                    return;

                                // get pages in interval (with links except for the current page)
                                var pages = grid.getPagesInInterval(interval, function (pageIndex, isCurrent) {
                                    if (isCurrent)
                                        return "<span id='currentpageindex'>" + (pageIndex + 1) + "</span>";
                                    return $("<a>").css("cursor", "pointer").html(pageIndex + 1).click(function (event) {
                                        grid.setPageIndex(parseInt($(this).html()) - 1);
                                    });
                                });

                                // "first" link
                                var link = $("<a class='nobg'>").html("<i class='fa fa-fast-backward'></i>");
                                if (!grid.canGoBack())
                                    link.css({opacity: 0.4, filter: "alpha(opacity=40)"});
                                else
                                    link.css("cursor", "pointer").click(function (event) {
                                        grid.firstPage();
                                    });
                                paginator.append(link);

                                // "prev" link
                                link = $("<a class='nobg'>").html("<i class='fa fa-backward'></i>");
                                if (!grid.canGoBack())
                                    link.css({opacity: 0.4, filter: "alpha(opacity=40)"});
                                else
                                    link.css("cursor", "pointer").click(function (event) {
                                        grid.prevPage();
                                    });
                                paginator.append(link);

                                // pages
                                for (p = 0; p < pages.length; p++)
                                    paginator.append(pages[p]).append(" ");

                                // "next" link
                                link = $("<a class='nobg'>").html("<i class='fa fa-forward'>");
                                if (!grid.canGoForward())
                                    link.css({opacity: 0.4, filter: "alpha(opacity=40)"});
                                else
                                    link.css("cursor", "pointer").click(function (event) {
                                        grid.nextPage();
                                    });
                                paginator.append(link);

                                // "last" link
                                link = $("<a class='nobg'>").html("<i class='fa fa-fast-forward'>");
                                if (!grid.canGoForward())
                                    link.css({opacity: 0.4, filter: "alpha(opacity=40)"});
                                else
                                    link.css("cursor", "pointer").click(function (event) {
                                        grid.lastPage();
                                    });
                                paginator.append(link);
                            }
                            ;
                        </script>
                        <script type="text/javascript">
                            var datagrid;
                            window.onload = function () {
                                datagrid = new DatabaseGrid();
                                // key typed in the filter field
                                $("#filter").keyup(function () {
                                    datagrid.editableGrid.filter($(this).val(), [0,3,5] );

                                    // To filter on some columns, you can set an array of column index
                                    //datagrid.editableGrid.filter( $(this).val(), [0,3,5]);
                                });

                                $("#addbutton").click(function () {
                                    datagrid.addRow();
                                });
                            };

                            $(function () {
                                $("#dialog").dialog({
                                    autoOpen: false,
                                    modal: true,
                                    height: 600,
                                    width: 800,
                                    open: function (ev, ui) {
                                        $('#add_iframe').attr('src', $('#dialog').data('src'));
                                    },
                                    close: function (ev, ui) {
                                        location.reload();
                                    }
                                });
                            });

                        </script>
                        <!-- simple form, used to add a new row -->
                        <div id="dialog" style="overflow: hidden;">
                            <iframe id="add_iframe" src="" style="border: none; width: 100%; height: 100%; overflow: hidden;"></iframe>
                        </div>
                    </section>
                </div>
                <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
        </div>
        <!-- nav-tabs-custom -->
        <!-- /.col -->
    </div>
</section>

<div id="modalUploadFile" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Import Jobcard</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" id="form-upload" enctype="multipart/form-data" >
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="email">Browse File</label>
                        <div class="col-sm-9">
                            <input type="file" class="form-control" id="jobcard_file" placeholder="Enter Order Number" name="media">
                            <small id="order_number_help" class="text-danger" style="display: none;">
                                Try another! Order Number Not Found.
                            </small> 
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-offset-3 col-sm-9">
                            <!-- <button type="submit" class="btn btn-primary" id="upload_data"> <i id="loading" class='fa fa-circle-o-notch fa-spin' style="display: none;"></i>  Upload Data <i class="fa fa-upload" aria-hidden="true"></i></button> -->
                            <p id="total_data"></p>
                            <div id="progres">
                            <p>Total Sukses : <span id="sukses">0</span></p>
                            <p>Total Gagal : <span id="gagal">0</span></p>
                            </div>
                            <a class="btn btn-primary" id="upload_data"> <i id="loading" class='fa fa-circle-o-notch fa-spin' style="display: none;"></i>  Upload Data <i class="fa fa-upload" aria-hidden="true"></i></a>
                        </div>
                    </div>                    
                </form>
            </div>
        </div>

        <div class="modal-content" style="display: none" id="import_status_parent">
            <div class="modal-header">
                <h4 class="modal-title">Import Status</h4>
            </div>
            <div class="modal-body">
                <table class="table table-bordered table-hover dataTable" id="import_status">
                    <thead>
                        <tr>
                            <th width="60%">Order Number</th>
                            <th width="30%">Status</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>

    </div>
</div>

<div id="modalMdr" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">MDR List </h4>
            </div>
            <div class="modal-body">
                <table class="table table-bordered table-hover dataTable" id="mdr-list">
                    <thead>
                        <tr>
                            <th width="20px">SEQ</th>
                            <th>ORDER</th>
                            <th width="20%">DESCRIPTION</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>

    </div>
</div>

<script type="text/javascript">

    function jc_delete(AUFNR){
        $('#loader').show();
        var url = '<?php echo base_url() ?>' + 'Projects/crud_jobcard_update';
        swal({
                title: "Warning!",
                text: "do you want to delete this record?",
                icon: "warning",
                buttons: true,
                dangerMode: true,
         })
        .then(function(isConfirm){
            if (isConfirm) {
                $.ajax({
                    url: url,
                    method: 'POST',
                    data: { colname: 'IS_ACTIVE', id: AUFNR, newvalue: 0},
                    dataType: 'text',
                    success: function(data){
                        $('#loader').hide();
                        datagrid.fetchGrid();
                    }
                })  
            }      
        })  
    }

    var idx = 0;
    var size = 20;
    var sukses = parseInt($('#sukses').text());
    var gagal = parseInt($('#gagal').text());
    var total = 0;
    var time = 0;

    $(document).ready(function(){
        $('#progres').hide();
        $('#upload_data').on("click", function(){  
            $('#progres').show();

            idx = 0;
            size = 20;
            sukses = parseInt($('#sukses').text());
            gagal = parseInt($('#gagal').text());
            total = 0;
            time = 0;
            
            $('#upload_data').html('<i id="loading" class="fa fa-circle-o-notch fa-spin"></i> Uploading Data...'); 
            $('#upload_data').attr('disabled', true);
            $("#import_status tbody").html("");

            var url = URL.createObjectURL(document.getElementById('jobcard_file').files[0]);
            var oReq = new XMLHttpRequest();
            oReq.open("GET", url, true);
            oReq.responseType = "arraybuffer";

            oReq.onload = function(e) {
                var arraybuffer = oReq.response;

                /* convert data to binary string */
                var data = new Uint8Array(arraybuffer);
                var arr = new Array();
                for(var i = 0; i != data.length; ++i) arr[i] = String.fromCharCode(data[i]);
                var bstr = arr.join("");
                /* Call XLSX */
                var workbook = XLSX.read(bstr, {type:"binary"});

                /* DO SOMETHING WITH workbook HERE */
                var first_sheet_name = workbook.SheetNames[0];
                /* Get worksheet */
                var worksheet = workbook.Sheets[first_sheet_name];
                var dataJSON = XLSX.utils.sheet_to_json(worksheet,{range:1});
                $('#total_data').text("Total Data : " + dataJSON.length);
                UpdateJobcardFormCsv(dataJSON)
            }

            oReq.send();
        });

        function UpdateJobcardFormCsv(jc_list){
            
            size = jc_list.length < size ? jc_list.length : size;
            var jc = jc_list.slice(idx, size);
            $.each(jc, function(key, val){
                delete jc[key].STATUS_SAP;
                delete jc[key].KTEXT;
            })

            $.ajax({
                url: "<?php echo base_url('Projects/read_xlsx_file_jobcard3')?>",
                type: 'POST',
                data: { myData: JSON.stringify(jc_list.slice(idx, size)) },
                dataType: "json",
                async: true,
                error: function(request, status, error){
                    if(request.status == 500){
                        swal({
                            title: "Opss!",
                            text: error,
                            icon: "info",
                            buttons: {
                                cancel: "Cancel",
                                attempt: {
                                    text: "Try Again!",
                                    value: true,
                                }
                            }
                        })
                        .then(function(value){
                            if(!value) {
                                $('#modalUploadFile').modal('hide');
                                $('#upload_data').html('Upload Data <i class="fa fa-upload" aria-hidden="true"></i>'); 
                                $('#upload_data').attr('disabled', false);
                                document.getElementById("jobcard_file").value = "";
                                return;
                            }
                            UpdateJobcardFormCsv(jc_list); 
                        })    
                    }

                }
            })
            .done(function(data){

                buildImportStatus(data.result);
                
                $("#import_status_parent").show();
                
                sukses = sukses + data.result.filter(function(value){ return value.STATUS === 1 }).length;
                gagal = gagal + data.result.filter(function(value){ return value.STATUS === 0 }).length;
                
                $('#gagal').text(gagal);
                $('#sukses').text(sukses);
                
                setTimeout(function(){
                    if(data.status === 'success') {

                        idx =+ size;
                        size = size + 20;

                        if(jc_list.length > idx){   

                            UpdateJobcardFormCsv(jc_list); 

                        } else {

                            swal({
                                title: 'Import Success',
                                icon: 'success',
                                confirmButtonText: 'Ok'
                            })

                            datagrid.fetchGrid();

                            $('#upload_data').html('Upload Data <i class="fa fa-upload" aria-hidden="true"></i>'); 
                            $('#upload_data').attr('disabled', false);
                            
                        }

                    } else if (data.status === 'failed') {

                        idx = idx + data.current_error + 1 ;
                        if( jc_list.length > idx ) {
                            UpdateJobcardFormCsv(jc_list); 
                        }
                    
                    }
                }, 3e3);
            });        
        }

        function buildImportStatus(data){
            var tbody = $('#import_status tbody');
            $.each(data, function(key, val){
                if(val.STATUS == 0) {
                    var content = "<tr>" +
                    "<td>" + val.ORDER + "</td>" +
                    "<td>" + setStatusIcon(val.STATUS) + "</td>" +
                    "</tr>";
                    tbody.append(content);
                }
            })
        }

        function setStatusIcon(status){
            if(status == 1) {
                return '<span class="label label-success" id="report-status">SUCCESS </span>'
            } else {
                return '<span class="label label-danger" id="report-status">ERROR </span>'
            }
        }

        $('#modalUploadFile').on('hidden.bs.modal', function(){
            document.getElementById("jobcard_file").value = "";
            $("#import_status_parent").hide();
            $("#import_status tbody").html("");
            $('#total_data').hide();
            $('#progres').hide();
        })          
                
    })


    function get_mdr($JC_NUMBER){
        var url = "<?= base_url('Projects/get_mdr_by_jc') ?>";
        $.ajax({
            url: url,
            method: 'POST',
            dataType: 'text',
            data: { JC_REFF: $JC_NUMBER },
            success: function(data){
                var response = $.parseJSON(data);
                if (response.status === 'success'){
                    $('#modalMdr').modal('show');
                    performing_datatables(response.body)
                } else {
                    swal({
                        title: 'Opps!',
                        text: 'Something Wrong',
                        icon: 'warning'
                    })
                }
            }
        })
    }

    var mdr_table = $('#mdr-list').dataTable({
        paging: false
    });

    function performing_datatables(data){
        mdr_table = $('#mdr-list').dataTable({
            destroy: true,
            searching: false,
            paging: false,
            data: data,
            columns: [
                { title: 'SEQ', data: 'SEQ_NUM' },
                { title: 'ORDER NUMBER', data: 'AUFNR' },
                { title: 'DESCRIPTION', data: 'KTEXT' }                
            ]     
        })
    }    

    
</script>