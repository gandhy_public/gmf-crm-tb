<?php 
    $this->load->view('admin/projects/header'); 
    $project = $project_data[0];
    $list_area = $project_area;
    $list_day = $project_day;
    $id_project = json_encode($id_project);
?>

<style>
    @media print {
        .section {
            display: none;
        }

        .nav-tabs {
            display: none !important;
        }

        .print-area {
            display: block !important;
        }

        .dataTable {
            width: 500px !important;
        }

        table th::after {
            display: none !important;
        }
    }
</style>


<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <!-- Custom Tabs -->
            <div class="nav-tabs-custom">
                <?php $this->load->view('admin/projects/menu'); ?>
                <div class="tab-content print-area">
                    <section class="content" style="overflow: auto;">
                        <div id="message"></div>
                        <div id="wrap">
                            <!-- <h3>JOBCARD</h3>-->
                            <!-- Feedback message zone -->
                            <div id="toolbar">
                                <table class="table">
                                    <tbody>
                                        <tr>
                                            <td width="10%">A/C REG</td>
                                            <td><strong><?php echo $project->TPLNR ?></strong></td>
                                            <td width="10%">Day</td>
                                            <td>
                                                <select name="" id="filter-day" class="form-control" style="width: 150px">
                                                    <option value="NULL">--Select Day--</option>
                                                    <?php foreach ($list_day as $val) { ?> 
                                                            <option value="<?php echo $val->day; ?>"><?php echo $val->day; ?></option>      
                                                    <?php } ?>
                                                </select>                                                
                                            </td>
                                            <td width="10%">Area</td>
                                            <td>
                                                <select name="" id="filter-area" class="form-control" style="width: 180px">
                                                    <option value="NULL">--Select Area--</option>
                                                    <?php foreach ($list_area as $val) { ?> 
                                                        <option value="<?php echo $val->AREA; ?>"><?php echo $val->AREA; ?></option>      
                                                    <?php } ?>
                                                </select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>A/C TYPE</td>
                                            <td><strong><?php echo $project->EARTX ?></strong>
                                            </td>
                                            <td>Date</td>
                                            <td><strong id="date"></strong></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td>Type of Insp.</td>
                                            <td><strong>D02-CHECK</strong></td>
                                            <td>MHRS</td>
                                            <td><strong id="mhrs-total"></strong></td>
                                            <td>MPWR Req</td>
                                            <td><strong id="mhrs-req"></strong></td>
                                        </tr>
                                        <tr>
                                            <td>Date Start</td>
                                            <td colspan="5"><strong>
                                                <?php 
                                                    $raw_start_date = strtotime($project->REVBD);
                                                    $start_date = date('d M Y', $raw_start_date);
                                                    echo $start_date;
                                                 ?>
                                            </strong></td>
                                        </tr>
                                        <tr>
                                            <td height="30px" colspan="5"></td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" class="text-center">Given by Prod. Controller</td>
                                            <td colspan="2" class="text-center"></td>
                                            <td colspan="2" class="text-center">Verified by Supervisor</td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" class="text-center" height="100px"></td>
                                            <td colspan="2" class="text-center" height="100px"></td>
                                            <td colspan="2" class="text-center" height="100px"></td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" class="text-center" >Name &amp; Sign</td>
                                            <td colspan="2" class="text-center" ></td>
                                            <td colspan="2" class="text-center" >Name &amp; Sign</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <!-- Grid contents -->
                            <style type="text/css">
                                #tablecontent td:hover {
                                    color: black;
                                }
                            </style>
                            <table class="table table-bordered table-hover no-footer dataTable" id="daily_list">
                                    <thead>
                                        <tr>
                                            <th width="20px">Seq</th>
                                            <th width="15%">Order</th>
                                            <th>Description</th>
                                            <th width="50px">Task Code</th>
                                            <th width="15%">Area</th>
                                            <th width="5%">Mhrs</th>
                                            <th width="10%">Type</th>
                                        </tr>
                                    </thead>
                                </table>
                            <!-- Paginator control -->
                            <div id="paginator"></div>
                        </div>
                        <!-- script src="<?php echo base_url(); ?>assets/editablegrid/js/jquery-1.11.1.min.js" ></script -->
                        <script src="<?php echo base_url(); ?>assets/jquery-ui/js/jquery-ui-1.10.4.custom.min.js"></script>
                            
                    </section>
                </div>
                <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
        </div>
        <!-- nav-tabs-custom -->
        <!-- /.col -->
    </div>
</section>

<script type="text/javascript">
    var params = {
        REVNR: <?= $id_project ?>
    }

    load_data(params)
    var daily_list = $('#daily_list').dataTable({
        paging: false,
        filter: false
    })

    $(document).ready(function(){
            getMhrs();          
    })

    function load_data(params){
        var url = '<?php echo base_url('projects/crud_daily_menu_load_all'); ?>'
        $.ajax({
                url: url,
                method: 'POST',
                data: params,
                dataType: 'text',
                success: function(data){
                    var response = $.parseJSON(data);
                    if(response.status === 'success'){
                        perfoming_datatables(response.body);
                    }else{
                        daily_list.fnClearTable();
                    }
                }
            })
    }

    function perfoming_datatables(data){
        report_table = $('#daily_list').dataTable({
            destroy: true,
            searching: false,
            paging: false,
            data: data,
            columns: [
                { title: 'Seq', data: 'SEQ' },
                { title: 'Order', data: 'AUFNR' },
                { title: 'Description', data: 'KTEXT' },
                { title: 'Task Code', data: 'ILART' },
                { title: 'Area', data: 'AREA' },
                { 
                    title: 'Mhrs', 
                    data: 'MHRS',
                    className: 'mhrs',
                    render: function(data, type, row){
                        return parseFloat(data).toFixed(2); 
                    } 
                },
                { title: 'Type', data: 'CARD_TYPE' }                
            ],
            initComplete: function(setting, josn){
                getMhrs()
            }     
        })
    };


    

    var area = $("#filter-area");
    var day = $("#filter-day");

    $("#filter-day").on("change", function(){
        if(day.val() !== 'NULL' ) {
            params["DAY"] = day.val();
            load_data(params)
        }else{
            delete params["DAY"];
            load_data(params)
        }
        getCurrentDateByStartDate(day.val());
    })

    $("#filter-area").on("change", function(){
        if(area.val() !== 'NULL' ) {
            params["AREA"] = area.val();
            load_data(params)
        }else{
            delete params["AREA"];
            load_data(params)
        }
    })

    function getMhrs(){
        var mhrs = 0.0;
        var mhrs_req = 0.0;
        var mhrs_container = $('.mhrs');
        $.each(mhrs_container, function(key, val){
            if(key === 0) { return }
            mhrs += parseFloat($(val).text());
        })

        $('#mhrs-total').html(mhrs.toFixed(2));
        $('#mhrs-req').html(parseFloat(mhrs/5).toFixed(2));
    }

    function getCurrentDateByStartDate(day){
        var start_date      = new Date('<?php echo $start_date ?>');
        var diff            = moment(start_date, "DD MM YYYY").add((day-1), 'days');
        $('#date').html(diff.format("DD MMM YYYY"));
    }

    $('#print').on('click', function(){
        window.print();
    })



</script>
