<div id="modalPrm" class="modal fade" role="dialog">
    <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Add New PRM</h4>
        </div>
        <div class="modal-body">
            <div class="form-horizontal">
                <input type="text" class="form-control" id="CRM_ID"  style="display:none">

                <div class="form-group">
                    <label class="control-label col-sm-3" for="email">Part Number</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" id="MATNR"/>
                    </div>
                </div> 
                <div class="form-group">
                    <label class="control-label col-sm-3" for="email">ALT PART NUMBER</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" id="ALT_PART_NUMBER"/>
                    </div>
                </div> 
                <div class="form-group">
                    <label class="control-label col-sm-3" for="email">Description</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" id="MAKTX"/>
                    </div>
                </div> 
                <div class="form-group">
                    <label class="control-label col-sm-3" for="email">Mat. Type</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" id="CTG"/>
                    </div>
                </div>                        
                <div class="form-group">
                    <label class="control-label col-sm-3" for="SN_QTY">SN QTY</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" id="SN_QTY" placeholder="Enter SN QTY">
                    </div>
                </div> 

                <div class="form-group">
                    <label class="control-label col-sm-3" for="POST">POS</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" id="POST" placeholder="Enter POS">
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-sm-3" for="SP_OUT_NO">SP OUT NO</label>
                    <div class="col-sm-9">
                        <input type="number" class="form-control" id="SP_OUT_NO" placeholder="Enter SP OUT NO">
                    </div>
                </div> 

                <div class="form-group">
                    <label class="control-label col-sm-3" for="SP_OUT_DATE">SP OUT DATE</label>
                    <div class="col-sm-9">
                        <input type="date" class="form-control" id="SP_OUT_DATE" placeholder="Enter SP OUT DATE">
                    </div>
                </div>   

                <div class="form-group">
                    <label class="control-label col-sm-3" for="SP_IN_NO">SP IN NO</label>
                    <div class="col-sm-9">
                        <input type="number" class="form-control" id="SP_IN_NO" placeholder="Enter SP IN NO">
                    </div>
                </div>  

                <div class="form-group">
                    <label class="control-label col-sm-3" for="SP_IN_DATE">SP IN DATE</label>
                    <div class="col-sm-9">
                        <input type="date" class="form-control" id="SP_IN_DATE" placeholder="Enter SP IN DATE">
                    </div>
                </div>     

                <div class="form-group">
                    <label class="control-label col-sm-3" for="UNIT_SEND">SP UNIT SEND</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" id="UNIT_SEND" placeholder="Enter UNIT SEND">
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-sm-3" for="UNIT_RECV">SP UNIT RECV</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" id="UNIT_RECV" placeholder="Enter UNIT RECV">
                    </div>
                </div>   

                <div class="form-group">
                    <label class="control-label col-sm-3" for="PART_LOC">PART LOCATION</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" id="PART_LOC" placeholder="Enter PART LOCATION">
                    </div>
                </div> 

                <div class="form-group">
                    <label class="control-label col-sm-3" for="REMARK">REMARK</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" id="REMARK" placeholder="Enter REMARK">
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-sm-3" for="STATUS">STATUS</label>
                    <div class="col-sm-9">
                        <select id="STATUS" class="form-control">
                            <option value="UR">UR - Und. repr</option>
                            <option value="SB">SB - Serviceabl</option>
                            <option value="RA">RA - Return as is</option>
                            <option value="CR">CR - Sent to Contract</option>
                            <option value="C">C - Close</option>
                        </select>
                    </div>
                </div>                                                                                                                                                                                                     


            </div>            
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary" id="save_prm">Save</button>
        </div>
    </div>

    </div>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        // Save Button
        $('#saveMenu').on("click", function(){
            var btn = $('#saveMenu').html();
            var url_location = '';
            if(btn === 'Save'){
                url_location = '<?php echo base_url() ?>' + 'projects/crud_prm_add';
            }else{
                url_location = '<?php echo base_url() ?>' + 'projects/crud_prm_update';
            }

            prm_data['SN_QTY'] = $('#SN_QTY').val(),
            prm_data['POST'] = $('#POST').val(),
            prm_data['SP_OUT_NO'] = $('#SP_OUT_NO').val(),
            prm_data['SP_OUT_DATE'] = $('#SP_OUT_DATE').val(),
            prm_data['SP_IN_NO'] = $('#SP_IN_NO ').val(),   
            prm_data['SP_IN_DATE'] = $('#SP_IN_DATE').val(),
            prm_data['UNIT_SEND'] = $('#UNIT_SEND').val(),
            prm_data['UNIT_RECV'] = $('#UNIT_RECV').val(),
            prm_data['PART_LOC'] = $('#PART_LOC').val(),
            prm_data['REMARK'] = $('#REMARK').val(),
            prm_data['STATUS'] = $('#STATUS').val();

            $.ajax({
                type: "POST",
                url: url_location,
                data: prm_data,
                dataType: "text",
                cache: false,
                success: function(data){
                    var msg = $.parseJSON(data);
                    if(msg.status === 'failed'){
                        swal("Error saving Record!", {
                                icon: "error"
                        });
                    }else{
                        swal("PRM has been saved successfully!", {
                                icon: "success"
                        });
                        datagrid.fetchGrid();
                        $('#modalPrm').modal('hide');
                        clear_form();
                    }
                }
            }); 
        })

    });

    function clear_form(){
        $('#SN_QTY').val('');
        $('#POST').val('');
        $('#SP_OUT_NO').val('');
        $('#SP_OUT_DATE').val('');
        $('#SP_IN_NO').val('');
        $('#SP_IN_DATE').val('');
        $('#UNIT_SEND').val('');
        $('#UNIT_RECV').val('');
        $('#PART_LOC').val('');
        $('#REMARK').val('');
        $('#STATUS').val('');
        $('#order_number_prm').val(''); 
        $('#PART_NUMBER_PRM').val(''); 
        $('#order_number_prm').parent().parent(). removeClass('has-success'); 
        $('#PART_NUMBER_PRM').parent().parent(). removeClass('has-success');
        prm_data = {};
    }
</script>