<?php
    $this->load->view('admin/projects/header');
    $MF_STATUS = array("DLVR FULL to Production" => "DLVR FULL to Production", " DLVR PARTIAL to Production" => "DLVR PARTIAL to Production", "WAITING Customer Supply" => "WAITING Customer Supply", "ROBBING Desicion" => "ROBBING Desicion", "PROVISION in Store" => "PROVISION in Store", "PRELOADED in Hangar Store" => "PRELOADED in Hangar Store", "ORDERED by Purchasing" => "ORDERED by Purchasing", "ORDERED by Loan Control" => "ORDERED by Loan Control", "ORDERED by Workshop" => "ORDERED by Workshop", "EXCHANGE in Progress" => "EXCHANGE in Progress", "Actual STOCK NIL" => "Actual STOCK NIL", "RIC/Part on Receiving Area" => "RIC/Part on Receiving Area", "NO SOURCE" => "NO SOURCE", "Waiting PN Confrimation" => "Waiting PN Confrimation", "GR OK" => "GR OK", "Need FOLLOW UP" => "Need FOLLOW UP", "PENDING/NO ACTION" => "PENDING/NO ACTION", "PR Ready" => "PR Ready", "POOLING" => "POOLING", "Shipment" => "Shipment", "Custom Process" => "Custom Process", "Need SOA" => "Need SOA", "WAITING Customer Approval" => "WAITING Customer Approval");
    $is_customer = is_customer($session['user_group']);
    $is_edit = $write == 1 ? true : false;
    $id_project = json_encode($id_project);
?>

<style>
    table > thead > tr > th.editablegrid-ALTERNATE_PART_NUMBER div a,
    table > thead > tr > th.editablegrid-MTART div a,
    table > thead > tr > th.editablegrid-IPC div a,
    table > thead > tr > th.editablegrid-STO_NUMBER div a,
    table > thead > tr > th.editablegrid-OUTBOND_DELIV div a,
    table > thead > tr > th.editablegrid-TO_NUM div a,
    table > thead > tr > th.editablegrid-MENGE div a,
    table > thead > tr > th.editablegrid-MEINS div a,
    table > thead > tr > th.editablegrid-STOCK_MANUAL div a,
    table > thead > tr > th.editablegrid-LGORT div a,
    table > thead > tr > th.editablegrid-MATERIAL_FULFILLMENT_STATUS div a,
    table > thead > tr > th.editablegrid-FULLFILLMENT_REMARK div a,
    table > thead > tr > th.editablegrid-PO_DATE div a,
    table > thead > tr > th.editablegrid-PO_NUM div a,
    table > thead > tr > th.editablegrid-AWB_NUMBER div a,
    table > thead > tr > th.editablegrid-AWB_DATE div a,
    table > thead > tr > th.editablegrid-QTY_DELIVERED div a,
    table > thead > tr > th.editablegrid-QTY_REMAIN div a,
    table > thead > tr > th.editablegrid-MATERIAL_REMARK div a    
    {
        color: #0097e6 !important;
    },

    .editablegrid-ID {
        display: none !important;
    }
    
    
</style>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <!-- Custom Tabs -->
            <div class="nav-tabs-custom">
                <?php $this->load->view('admin/projects/menu'); ?>
                <div class="tab-content">
                    <section class="content">
                        <div id="message"></div>
                        <div id="wrap">
                            <!-- <h3>JOBCARD</h3>-->
                            <!-- Feedback message zone -->
                            <div id="toolbar">
                                <input type="text" id="filter" name="filter" placeholder="Filter :type any text here"  />
                                <?php if($is_edit) { ?>
                                    <div class="pull-right form-inline">
                                        <button class="btn btn-info" id="upload" data-toggle="modal" data-target="#modalUploadFile">
                                            Import Excel
                                            <i class="fa fa-upload" aria-hidden="true"></i>
                                        </button>   
                                        <a href="<?= base_url('index.php/projects/crud_mrm_export?REVNR=' . json_decode($id_project)) ?>" target="_blank"><button class="btn btn-info">
                                            Export Excel
                                            <i class="fa fa-download" aria-hidden="true"></i>
                                        </button></a>
                                        <button class="btn btn-info" id="import" onclick="update_mrm()">
                                            Refresh 
                                            <i class="fa fa-refresh" aria-hidden="true"></i>
                                        </button>                                    
                                        <button id="addNew" data-toggle="modal" data-target="#modalOrderNumber" class="btn btn-info">
                                            <i class="fa fa-plus" aria-hidden="true"></i> 
                                            New
                                        </button>
                                    </div>
                                <?php } ?>
                            </div>
                            <!-- Grid contents -->
                            <div id="tablecontent" style="overflow-x: auto;">
                                <div class="lds-ellipsis"><div></div><div></div><div></div><div></div></div>
                            </div>
                            <!-- Paginator control -->
                            <div id="paginator"></div>
                        </div>
                        <!-- script src="<?php echo base_url(); ?>assets/editablegrid/js/jquery-1.11.1.min.js" ></script -->


                        <script src="<?php echo base_url(); ?>assets/jquery-ui/js/jquery-ui-1.10.4.custom.min.js"></script>
                        <script type="text/javascript">
                            var base_url = "<?php echo base_url(); ?>index.php/projects/crud_mrm_";

                            function highlightRow(rowId, bgColor, after)
                            {
                                var rowSelector = $("#" + rowId);
                                rowSelector.css("background-color", bgColor);
                                rowSelector.fadeTo("normal", 0.5, function () {
                                    rowSelector.fadeTo("fast", 1, function () {
                                        rowSelector.css("background-color", '');
                                    });
                                });
                            }

                            function highlight(div_id, style) {
                                highlightRow(div_id, style == "error" ? "#e5afaf" : style == "warning" ? "#ffcc00" : "#8dc70a");
                            }



                            function message(type, message) {
                                $('#message').html("<div class=\"notification  " + type + "\">" + message + "</div>").slideDown('normal').delay(1800).slideToggle('slow');
                            }

                            /**
                             updateCellValue calls the PHP script that will update the database.
                             */
                            function updateCellValue(editableGrid, rowIndex, columnIndex, oldValue, newValue, row, onResponse)
                            {
                                $.ajax({
                                    url: base_url + 'update?REVNR=' + <?= $id_project ?> ,
                                    type: 'POST',
                                    dataType: "html",
                                    data: {
                                        tablename: editableGrid.name,
                                        id: editableGrid.getValueAt(rowIndex, 0),
                                        newvalue: editableGrid.getColumnType(columnIndex) == "boolean" ? (newValue ? 1 : 0) : newValue,
                                        colname: editableGrid.getColumnName(columnIndex),
                                        coltype: editableGrid.getColumnType(columnIndex)
                                    },
                                    success: function (response)
                                    {
                                        // reset old value if failed then highlight row

                                        var success = response.search("ok") > 0;
                                        if (!success) {
                                            editableGrid.setValueAt(rowIndex, columnIndex, oldValue);
                                            display_toast('danger', 'Error to update, please check on Column ' + editableGrid.getColumnName(columnIndex) + ', Order : ' + editableGrid.getValueAt(rowIndex, 0) );
                                            // datagrid.fetchGrid();
                                        } else {
                                            display_toast('success', 'ID : ' + editableGrid.getValueAt(rowIndex, 0) + ', Column '+ editableGrid.getColumnName(columnIndex) + ' updated to ' + editableGrid.getValueAt(rowIndex, columnIndex));
                                            // datagrid.fetchGrid();
                                        }
                                    },
                                    error: function (XMLHttpRequest, textStatus, exception) {
                                        alert("Ajax failure\n" + errortext);
                                    },
                                    async: true
                                });

                            }

                            function DatabaseGrid()
                            {
                                this.editableGrid = new EditableGrid("TB_MRM", {
                                    editmode: 'static',
                                    enableSort: true,

                                    /* Comment this line if you set serverSide to true */
                                    // define the number of row visible by page
                                    /*pageSize: 50,*/

                                    /* This property enables the serverSide part */
                                    serverSide: true,

                                    // Once the table is displayed, we update the paginator state
                                    tableRendered: function () {
                                        updatePaginator(this);
                                    },
                                    tableLoaded: function () {
                                        datagrid.initializeGrid(this);
                                    },
                                    modelChanged: function (rowIndex, columnIndex, oldValue, newValue, row) {
                                        updateCellValue(this, rowIndex, columnIndex, oldValue, newValue, row);
                                    }
                                });
                                this.fetchGrid();

                                $("#filter").val(this.editableGrid.currentFilter != null ? this.editableGrid.currentFilter : "");
                                if (this.editableGrid.currentFilter != null && this.editableGrid.currentFilter.length > 0)
                                    $("#filter").addClass('filterdefined');
                                else
                                    $("#filter").removeClass('filterdefined');

                            }

                            DatabaseGrid.prototype.fetchGrid = function () {
                                // call a PHP script to get the data
                                this.editableGrid.loadJSON(base_url + "load?edit=" + "<?= $write ?>" + "&REVNR=" + <?= $id_project ?>);
                            };

                            DatabaseGrid.prototype.initializeGrid = function (grid) {

                                var self = this;

                                // render for the action column
                               grid.setCellRenderer("action", new CellRenderer({
                                    render: function (cell, ID) {
                                        cell.innerHTML += "<button class='btn btn-sm btn-danger delete' data-id='"+ID+"' onclick='mrm_delete("+ID+")'><i class='fa fa-trash-o' aria-hidden='true'></i> </button>";
                                    }
                                }));


                                grid.renderGrid("tablecontent", "testgrid");
                                $('html, body').scrollTop(1);
                                $('html, body').scrollTop(0);

                            };

                            DatabaseGrid.prototype.deleteRow = function (id)
                            {

                                var self = this;

                                if (confirm('Are you sur you want to delete the row id ' + id)) {

                                    $.ajax({
                                        url: base_url + 'delete',
                                        type: 'POST',
                                        dataType: "html",
                                        data: {
                                            tablename: self.editableGrid.name,
                                            id: id
                                        },
                                        success: function (response)
                                        {
                                            if (response == "ok") {
                                                message("success", "Row deleted");
                                                self.fetchGrid();
                                            }
                                        },
                                        error: function (XMLHttpRequest, textStatus, exception) {
                                            alert("Ajax failure\n" + errortext);
                                        },
                                        async: true
                                    });


                                }

                            };


                            DatabaseGrid.prototype.addRow = function (id)
                            {

                                var self = this;

                                $.ajax({
                                    url: base_url + 'add',
                                    type: 'POST',
                                    dataType: "html",
                                    data: {
                                        tablename: self.editableGrid.name,
                                        name: $("#name").val(),
                                        firstname: $("#firstname").val()
                                    },
                                    success: function (response)
                                    {
                                        if (response == "ok") {

                                            // hide form
                                            showAddForm();
                                            $("#name").val('');
                                            $("#firstname").val('');
                                            message("success", "Row added : reload model");
                                            self.fetchGrid();
                                        } else
                                            message("error", "Error occured");
                                    },
                                    error: function (XMLHttpRequest, textStatus, exception) {
                                        alert("Ajax failure\n" + errortext);
                                    },
                                    async: true
                                });
                            };

                            function updatePaginator(grid, divId)
                            {
                                divId = divId || "paginator";
                                var paginator = $("#" + divId).empty();
                                var nbPages = grid.getPageCount();

                                // get interval
                                var interval = grid.getSlidingPageInterval(20);
                                if (interval == null)
                                    return;

                                // get pages in interval (with links except for the current page)
                                var pages = grid.getPagesInInterval(interval, function (pageIndex, isCurrent) {
                                    if (isCurrent)
                                        return "<span id='currentpageindex'>" + (pageIndex + 1) + "</span>";
                                    return $("<a>").css("cursor", "pointer").html(pageIndex + 1).click(function (event) {
                                        grid.setPageIndex(parseInt($(this).html()) - 1);
                                    });
                                });

                                // "first" link
                                var link = $("<a class='nobg'>").html("<i class='fa fa-fast-backward'></i>");
                                if (!grid.canGoBack())
                                    link.css({opacity: 0.4, filter: "alpha(opacity=40)"});
                                else
                                    link.css("cursor", "pointer").click(function (event) {
                                        grid.firstPage();
                                    });
                                paginator.append(link);

                                // "prev" link
                                link = $("<a class='nobg'>").html("<i class='fa fa-backward'></i>");
                                if (!grid.canGoBack())
                                    link.css({opacity: 0.4, filter: "alpha(opacity=40)"});
                                else
                                    link.css("cursor", "pointer").click(function (event) {
                                        grid.prevPage();
                                    });
                                paginator.append(link);

                                // pages
                                for (p = 0; p < pages.length; p++)
                                    paginator.append(pages[p]).append(" ");

                                // "next" link
                                link = $("<a class='nobg'>").html("<i class='fa fa-forward'>");
                                if (!grid.canGoForward())
                                    link.css({opacity: 0.4, filter: "alpha(opacity=40)"});
                                else
                                    link.css("cursor", "pointer").click(function (event) {
                                        grid.nextPage();
                                    });
                                paginator.append(link);

                                // "last" link
                                link = $("<a class='nobg'>").html("<i class='fa fa-fast-forward'>");
                                if (!grid.canGoForward())
                                    link.css({opacity: 0.4, filter: "alpha(opacity=40)"});
                                else
                                    link.css("cursor", "pointer").click(function (event) {
                                        grid.lastPage();
                                    });
                                paginator.append(link);
                            }
                            ;
                        </script>
                        <script type="text/javascript">
                            var datagrid;
                            window.onload = function () {
                                datagrid = new DatabaseGrid();
                                // key typed in the filter field
                                $("#filter").keyup(function () {
                                    datagrid.editableGrid.filter($(this).val());

                                    // To filter on some columns, you can set an array of column index
                                    //datagrid.editableGrid.filter( $(this).val(), [0,3,5]);
                                });

                                $("#addbutton").click(function () {
                                    datagrid.addRow();
                                });
                            };



                            $(function () {
                                $("#dialog").dialog({
                                    autoOpen: false,
                                    modal: true,
                                    height: 600,
                                    width: 800,
                                    open: function (ev, ui) {
                                        $('#add_iframe').attr('src', $('#dialog').data('src'));
                                    },
                                    close: function (ev, ui) {
                                        location.reload();
                                    }
                                });
                            });

                        </script>
                    </section>
                </div>
                <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
        </div>
        <!-- nav-tabs-custom -->
        <!-- /.col -->
    </div>
</section>


<div id="modalOrderNumber" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Find Order Number &amp; Part Number </h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal">
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="email">Order Number</label>
                        <div class="col-sm-9">
                            <select class="form-control" id="order_number" >
                            </select>
                        </div>
                    </div>  

                    <div class="form-group">
                        <label class="control-label col-sm-3" for="email">Part Number</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="PART_NUMBER" placeholder="Enter Part Number">
                            <small id="part_number_help" class="text-danger" style="display: none;">
                                Try another! Part Number Not Found.
                            </small> 
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-offset-3 col-sm-9">
                            <a class="btn btn-primary" id="create_mrm" disabled="true">Create MRM</a>
                        </div>
                    </div>                    
                </form>
            </div>
        </div>

    </div>
</div>

<div id="modalUploadFile" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Import MRM</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" id="form-upload" enctype="multipart/form-data" >
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="email">Browse File</label>
                        <div class="col-sm-9">
                            <input type="file" class="form-control" id="material_file" placeholder="Enter Order Number" name="media">
                            <small id="order_number_help" class="text-danger" style="display: none;">
                                Try another! Order Number Not Found.
                            </small> 
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-offset-3 col-sm-9">
                            <!-- <button type="submit" class="btn btn-primary" id="upload_data"> <i id="loading" class='fa fa-circle-o-notch fa-spin' style="display: none;"></i>  Upload Data <i class="fa fa-upload" aria-hidden="true"></i></button> -->
                            <p id="total_data"></p>
                            <div id="progres">
                            <p>Total Sukses : <span id="sukses">0</span></p>
                            <p>Total Gagal : <span id="gagal">0</span></p>
                            </div>
                            <a class="btn btn-primary" id="upload_data"> <i id="loading" class='fa fa-circle-o-notch fa-spin' style="display: none;"></i>  Upload Data <i class="fa fa-upload" aria-hidden="true"></i></a>
                        </div>
                    </div>                    
                </form>
            </div>
        </div>

        <div class="modal-content" style="display: none" id="import_status_parent">
            <div class="modal-header">
                <h4 class="modal-title">Import Status</h4>
            </div>
            <div class="modal-body">
                <table class="table table-bordered table-hover dataTable" id="import_status">
                    <thead>
                        <tr>
                            <th width="60%">Order Number</th>
                            <th width="30%">Status</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>

    </div>
</div>

<div id="modalMrm" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Add New MRM</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal">
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="email">Part Number</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="MATNR"/>
                        </div>
                    </div> 
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="email">ALT PART NUMBER</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="ALT_PART_NUMBER"/>
                        </div>
                    </div> 
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="email">Description</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="MAKTX"/>
                        </div>
                    </div> 
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="email">Mat. Type</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="CTG"/>
                        </div>
                    </div>                                                             
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="email">IPC</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="IPC"/>
                        </div>
                    </div>  
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="email">STO Number</label>
                        <div class="col-sm-9">
                            <input type="number" class="form-control" id="STO_NUMBER"/>
                        </div>
                    </div> 
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="email">Qty Req Single Item</label>
                        <div class="col-sm-9">
                            <input type="number" class="form-control" id="MENGE"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="email">UOM</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="MEINS"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="email">Input Stock Manually</label>
                        <div class="col-sm-9">
                            <input type="number" class="form-control" id="STOCK_MANUAL"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="email">Storage Location</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="LGORT"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="email">Material Fulfillment Status</label>
                        <div class="col-sm-9">
                            <select id="MATERIAL_FULFILLMENT_STATUS" class="form-control">
                                <?php foreach ($MF_STATUS as $key => $value) { ?>
                                    <option value='<?php echo $key ?>'> <?php echo $value ?> </option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="email">Fulfillment Remark</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="FULLFILLMENT_REMARK"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="email">Date of PO</label>
                        <div class="col-sm-9">
                            <input type="date" class="form-control" id="PO_DATE"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="email">Purchase Order PO</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="PO_NUM"/>
                        </div>
                    </div>  
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="email">AWB Number</label>
                        <div class="col-sm-9">
                            <input type="number" class="form-control" id="AWB_NUMBER"/>
                        </div>
                    </div>   
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="email">AWB Date</label>
                        <div class="col-sm-9">
                            <input type="date" class="form-control" id="AWB_DATE"/>
                        </div>
                    </div>           
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="email">Qty Delivered</label>
                        <div class="col-sm-9">
                            <input type="number" class="form-control" id="QTY_DELIVERED"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="email">Qty Remain</label>
                        <div class="col-sm-9">
                            <input type="number" class="form-control" id="QTY_REMAIN"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="email">Material Remark</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="MATERIAL_REMARK"/>
                        </div>
                    </div> 
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="email">Outbond Delivery</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="OUTBOND_DELIV"/>
                        </div>
                    </div> 
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="email">TO Num</label>
                        <div class="col-sm-9">
                            <input type="number" class="form-control" id="TO_NUM"/>
                        </div>
                    </div>                                                                                            
                    <div class="form-group">
                        <div class="col-md-offset-3 col-sm-9">
                            <a class="btn btn-primary" id="save_mrm" required>Save</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>

    </div>
</div>



<style>
    .modal{
        overflow-y: auto !important;
    }
</style>

<script type="text/javascript">

    var mrm_data = {};

    $(document).ready(function () {

        $('#addNew').click(function () {
            clear_form();
        })


        var id_project = <?= $id_project ?> ;
        var url = '<?php echo base_url() ?>' + 'Projects/find_order_number?REVNR=' + id_project
        $('#order_number').select2({
            width: '100%',
            minimumInputLength: 6,
            triggerChange: true,
            allowClear: true,
            ajax: {
                url: url,
                minimumInputLength: 5,
                dataType: 'json',
                delay: 450,
                processResults: function (data) {
                    var response = data.body
                    var jobcard = {id: 0, text: 'Jobcard', children: []};
                    var mdr = {id: 1, text: 'MDR', children: []};

                    $.each(response, function (key, val) {
                        if (val.AUART === 'GA01') {
                            jobcard.children.push({id: val.AUFNR, text: val.AUFNR});
                        } else {
                            mdr.children.push({id: val.AUFNR, text: val.AUFNR});
                        }
                    })

                    return {
                        results: [jobcard, mdr]
                    }
                },
                cache: true
            }
        }).select2('val', []);


        $('#order_number').on("select2:select", function (e) {
            find_order_number(e.params.data.id);
        });

        $("#PART_NUMBER").focusout(function () {
            find_part_number();
        });

        $('#create_mrm').click(function () {
            var on = $('#order_number').val();
            var pn = $('#PART_NUMBER').val();
            if ((on != null) && (pn != "")) {
                $('#modalOrderNumber').modal('hide');
                $('#modalMrm').modal('show');
            } else {
                swal("Opss!", "Order Number or Part Number is Required!", "error");
            }
        })

        $('#save_mrm').click(function () {
            saveMaterial();
        })

    })

    function find_order_number(order_number) {
        var order_type = $('#order_type').val();

        if (order_number !== null) {
            var data = {
                AUFNR: order_number,
                ORDER_TYPE: order_type,
                MENU: 'MRM'
            }
            $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>" + "projects/find_order_number_data",
                data: data,
                dataType: "text",
                cache: false,
                success: function (data) {
                    var response = $.parseJSON(data);
                    if (response.status === 'success') {
                        var order = response.body;
                        mrm_data["CARD_TYPE"] = order.AUART === 'GA01' ? 'JC' : 'MDR';
                        mrm_data["AUFNR"] = order.AUFNR;
                        mrm_data["KTEXT"] = order.KTEXT;
                        mrm_data["CUST_JC_NUM"] = order.AUART === 'GA02' ? parseInt(order.JC_REFF) * 1 : "";
                    } else {
                        // handleOrderError();
                    }
                }
            })
        }
    }

    function find_part_number() {
        var part_number = $('#PART_NUMBER').val();
        var url = '<?php echo base_url() ?>' + 'projects/load_material_data'
        $.ajax({
            url: url,
            method: 'POST',
            dataType: 'text',
            data: {MATNR: part_number},
            success: function (data) {
                $('#modalMaterial').modal('hide');
                var response = $.parseJSON(data);
                if (response.status === 'success') {
                    var material = response.body;

                    check_duplicate_mrm();

                    $('#ALT_PART_NUMBER').val(material.ALT_PART_NUMBER);
                    $('#MAKTX').val(material.MAKTX);
                    $('#MATNR').val(material.MATNR);
                    $('#CTG').val(material.CTG);

                    $('#ALT_PART_NUMBER').prop('readonly', true);
                    $('#MATNR').prop('readonly', true);
                    $('#MAKTX').prop('readonly', true);
                    $('#CTG').prop('readonly', true);
                } else {
                    $('#create_mrm').attr('disabled', false)
                    $('#MATNR').val($('#PART_NUMBER').val());
                }
            }
        })
    }

    function check_duplicate_mrm() {
        var order_number = $('#order_number').val();
        var part_number = $('#PART_NUMBER').val();

        var url = '<?php echo base_url() ?>' + 'projects/check_duplicate_mrm'
        var data = {
            AUFNR: order_number,
            MATNR: part_number
        }

        $.ajax({
            url: url,
            method: 'POST',
            dataType: 'text',
            data: data,
            success: function (data) {
                var response = $.parseJSON(data);
                if (response.status === 'error') {
                    swal("Opss!", "Another MRM with this Order Number & Part Number already exists!", "error");
                    $('#create_mrm').attr('disabled', true)
                } else {
                    $('#create_mrm').attr('disabled', false)
                }
            }
        })
    }

    function saveMaterial() {
        var url = '<?php echo base_url() ?>' + 'admin/Master_data/crud_material_add';
        var data = {
            ALT_PART_NUMBER: $('#ALT_PART_NUMBER').val(),
            MAKTX: $('#MAKTX').val(),
            MATNR: $('#MATNR').val(),
            CTG: $('#CTG').val()
        }

        if ($('#MAKTX').is('[readonly]')) {
            saveMrm()
            return
        }

        $.ajax({
            url: url,
            method: 'POST',
            dataType: 'text',
            data: data,
            success: function (data) {
                var response = $.parseJSON(data);
                if (response.status === 'success') {
                    saveMrm()
                } else {
                    swal("Opss!", "Error while saving Material!", "error");
                }
            }
        })
    }

    function saveMrm() {
        mrm_data["ALT_PART_NUMBER"] = $('#ALT_PART_NUMBER').val();
        mrm_data["MAKTX"] = $('#MAKTX').val();
        mrm_data["MATNR"] = $('#MATNR').val();
        mrm_data["CTG"] = $('#CTG').val();
        mrm_data["IPC"] = $('#IPC').val();
        mrm_data["STO_NUMBER"] = $('#STO_NUMBER').val();
        mrm_data["MENGE"] = $('#MENGE').val();
        mrm_data["MEINS"] = $('#MEINS').val();
        mrm_data["STOCK_MANUAL"] = $('#STOCK_MANUAL').val();
        mrm_data["LGORT"] = $('#LGORT').val();
        mrm_data["MATERIAL_FULFILLMENT_STATUS"] = $('#MATERIAL_FULFILLMENT_STATUS').val();
        mrm_data["FULLFILLMENT_REMARK"] = $('#FULLFILLMENT_REMARK').val();
        mrm_data["PO_DATE"] = $('#PO_DATE').val();
        mrm_data["PO_NUM"] = $('#PO_NUM').val();
        mrm_data["AWB_NUMBER"] = $('#AWB_NUMBER').val();
        mrm_data["AWB_DATE"] = $('#AWB_DATE').val();
        mrm_data["QTY_DELIVERED"] = $('#QTY_DELIVERED').val();
        mrm_data["QTY_REMAIN"] = $('#QTY_REMAIN').val();
        mrm_data["MATERIAL_REMARK"] = $('#MATERIAL_REMARK').val();
        mrm_data["OUTBOND_DELIV"] = $('#OUTBOND_DELIV').val();
        mrm_data["TO_NUM"] = $('#TO_NUM').val();

        var url = '<?php echo base_url() ?>' + 'projects/crud_mrm_add';
        $.ajax({
            url: url,
            method: 'POST',
            dataType: 'text',
            data: mrm_data,
            success: function (data) {
                var response = $.parseJSON(data);
                if (response.status === 'success') {
                    swal("Good Job!", "Record has been saved!", "success");
                    datagrid.fetchGrid();
                    clear_form();
                    $('#modalMrm').modal('hide');
                }
            }
        })
    }

    function clear_form() {
        $('#IPC').val('');
        $('#STO_NUMBER').val('');
        $('#MENGE').val('');
        $('#MEINS').val('');
        $('#STOCK_MANUAL').val('');
        $('#LGORT').val('');
        $('#MATERIAL_FULFILLMENT_STATUS').val('');
        $('#FULLFILLMENT_REMARK').val('');
        $('#PO_DATE').val('');
        $('#PO_NUM').val('');
        $('#AWB_NUMBER').val('');
        $('#AWB_DATE').val('');
        $('#QTY_DELIVERED').val('');
        $('#QTY_REMAIN').val('');
        $('#MATERIAL_REMARK').val('');
        $('#OUTBOND_DELIV').val('');
        $('#TO_NUM').val('');
        $('#order_number').val(null).trigger('change');
        $('#PART_NUMBER').val('');
        $('#create_mrm').attr('disabled', true);
        crm_data = {};
    }

    function mrm_delete(element){
        var data = $(element).attr('data-id');
        var id = data.split("-");
        $('#loader').show();
        var url = '<?php echo base_url() ?>' + 'Projects/crud_mrm_update';
        swal({
                title: "Warning!",
                text: "do you want to delete this record?",
                icon: "warning",
                buttons: true,
                dangerMode: true,
         })
        .then(function(isConfirm){
            if (isConfirm) {
                $.ajax({
                    url: url,
                    method: 'POST',
                    data: { colname: 'IS_ACTIVE', id: id[1], newvalue: 0, coltype: 'string', REVNR: id[0]},
                    dataType: 'text',
                    success: function(data){
                        $('#loader').hide();
                        datagrid.fetchGrid();
                        var success = data.search("ok") > 0;
                        
                        if(success) {
                            display_toast('success', 'This record has been deleted! ');
                        } else {
                            display_toast('danger', 'Error while deleting this record! ');
                        }
                    }
                })
            }else{
                $('#loader').hide();
            }        
        })  
    }

    function update_mrm() {
        var url = '<?php echo base_url(); ?>' + 'Projects/merge_mrm/' + <?= $id_project ?>;
        if (!bw_run) {
            bw_run = true;
            swal({title: 'MRM', text: 'Loading...', icon: 'info'});
            jQuery.get(url, function (data) {
                if (data == 1) {
                    swal({title: 'MRM', text: 'Data updated', icon: 'success'});
                    datagrid.fetchGrid();
                } else {
                    swal({title: 'MRM', text: 'Data not updated', icon: 'error'});
                }
            });
        } else {
            swal({title: 'Warning', text: 'Operation already running', icon: 'warning'});
        }
    }

</script>

<script>
    $(document).ready(function(){
        // $("#upload_data").click(function(evt){
        //     $('#upload_data').html('<i id="loading" class="fa fa-circle-o-notch fa-spin"></i> Uploading Data...'); 
        //     $('#upload_data').attr('disabled', true);
        //     evt.preventDefault();
        //     var formData = new FormData($('#form-upload')[0]);
            
        //     setTimeout(function(){
        //         $.ajax({
        //             url: "<?php echo base_url('Projects/read_xlsx_file_mrm?REVNR=' . json_decode($id_project)); ?>",
        //             type: 'POST',
        //             data: formData,
        //             dataType: 'text',
        //             async: true,
        //             cache: false,
        //             contentType: false,
        //             enctype: 'multipart/form-data',
        //             processData: false,
        //             success: function (data) {
        //                 $('#upload_data').html('Upload Data <i class="fa fa-upload" aria-hidden="true"></i>'); 
        //                 $('#upload_data').attr('disabled', false);
        //                 var response = $.parseJSON(data);
        //                 if(response.status === 'success'){
        //                     datagrid.fetchGrid();
        //                     swal({
        //                         title: 'Import Success',
        //                         icon: 'success',
        //                         button: 'Ok'
        //                     })
        //                     $('#modalUploadFile').modal('hide');
        //                 } else {
        //                     swal({
        //                         title: 'Something Wrong',
        //                         icon: 'error',
        //                         button: 'Try Again'
        //                     })
        //                 }
        //             },
        //             error: function(req, status, code){
        //                 swal({
        //                     title: 'Opss!' ,
        //                     text: code + '!',
        //                     icon: 'error',
        //                 })
        //                 $('#upload_data').html('Upload Data <i class="fa fa-upload" aria-hidden="true"></i>'); 
        //                 $('#upload_data').attr('disabled', false)
        //             }
        //         });
        //     }, 1000); 
        // });

        var idx = 0;
        var size = 20;
        var sukses = parseInt($('#sukses').text());
        var gagal = parseInt($('#gagal').text());
        var total = 0;
        var time = 0;

        $(document).ready(function(){
            $('#progres').hide();
            $('#upload_data').on("click", function(){  
                $('#progres').show();

                idx = 0;
                size = 20;
                sukses = parseInt($('#sukses').text());
                gagal = parseInt($('#gagal').text());
                total = 0;
                time = 0;
                
                $('#upload_data').html('<i id="loading" class="fa fa-circle-o-notch fa-spin"></i> Uploading Data...'); 
                $('#upload_data').attr('disabled', true);
                $("#import_status tbody").html("");

                var url = URL.createObjectURL(document.getElementById('material_file').files[0]);
                var oReq = new XMLHttpRequest();
                oReq.open("GET", url, true);
                oReq.responseType = "arraybuffer";

                oReq.onload = function(e) {
                    var arraybuffer = oReq.response;

                    /* convert data to binary string */
                    var data = new Uint8Array(arraybuffer);
                    var arr = new Array();
                    for(var i = 0; i != data.length; ++i) arr[i] = String.fromCharCode(data[i]);
                    var bstr = arr.join("");
                    /* Call XLSX */
                    var workbook = XLSX.read(bstr, {type:"binary"});

                    /* DO SOMETHING WITH workbook HERE */
                    var first_sheet_name = workbook.SheetNames[0];
                    /* Get worksheet */
                    var worksheet = workbook.Sheets[first_sheet_name];
                    var dataJSON = XLSX.utils.sheet_to_json(worksheet,{range:1});
                    $('#total_data').text("Total Data : " + dataJSON.length);
                    UpdateMrmFormSheet(dataJSON)
                }

                oReq.send();
            });

            function UpdateMrmFormSheet(jc_list){
                
                size = jc_list.length < size ? jc_list.length : size;
                var jc = jc_list.slice(idx, size);
                $.each(jc, function(key, val){
                    delete jc[key].AUFNR;
                    delete jc[key].CARD_TYPE;
                    delete jc[key].MAKTX;
                    delete jc[key].KTEXT;
                    delete jc[key].ORIG_NUMBER;
                    delete jc[key].MRM_ISSUE_DATE;
                    delete jc[key].TOTAL_QTY;
                    delete jc[key].FULLFILLMENT_STATUS_DATE;
                })

                $.ajax({
                    url: "<?php echo base_url('Projects/read_xlsx_file_mrm')?>",
                    type: 'POST',
                    data: { myData: JSON.stringify(jc_list.slice(idx, size)) },
                    dataType: "json",
                    async: true,
                    error: function(request, status, error){
                        if(request.status == 500){
                            swal({
                                title: "Opss!",
                                text: error,
                                icon: "info",
                                buttons: {
                                    cancel: "Cancel",
                                    attempt: {
                                        text: "Try Again!",
                                        value: true,
                                    }
                                }
                            })
                            .then(function(value){
                                if(!value) {
                                    $('#modalUploadFile').modal('hide');
                                    $('#upload_data').html('Upload Data <i class="fa fa-upload" aria-hidden="true"></i>'); 
                                    $('#upload_data').attr('disabled', false);
                                    document.getElementById("material_file").value = "";
                                    return;
                                }
                                UpdateMrmFormSheet(jc_list); 
                            })    
                        }

                    }
                })
                .done(function(data){

                    buildImportStatus(data.result);
                    
                    $("#import_status_parent").show();
                    
                    sukses = sukses + data.result.filter(function(value){ return value.STATUS === 1 }).length;
                    gagal = gagal + data.result.filter(function(value){ return value.STATUS === 0 }).length;
                    
                    $('#gagal').text(gagal);
                    $('#sukses').text(sukses);
                    
                    setTimeout(function(){
                        if(data.status === 'success') {

                            idx =+ size;
                            size = size + 20;

                            if(jc_list.length > idx){   

                                UpdateMrmFormSheet(jc_list); 

                            } else {

                                swal({
                                    title: 'Import Success',
                                    icon: 'success',
                                    confirmButtonText: 'Ok'
                                })

                                datagrid.fetchGrid();

                                $('#upload_data').html('Upload Data <i class="fa fa-upload" aria-hidden="true"></i>'); 
                                $('#upload_data').attr('disabled', false);
                                
                            }

                        } else if (data.status === 'failed') {

                            idx = idx + data.current_error + 1;
                            if( jc_list.length > idx ) {
                                UpdateMrmFormSheet(jc_list); 
                            }
                        
                        }
                    }, 3e3);
                });        
            }

            function buildImportStatus(data){
                var tbody = $('#import_status tbody');
                $.each(data, function(key, val){
                    if(val.STATUS == 0) {
                        var content = "<tr>" +
                        "<td>" + val.ORDER + "</td>" +
                        "<td>" + setStatusIcon(val.STATUS) + "</td>" +
                        "</tr>";
                        tbody.append(content);
                    }
                })
            }

            function setStatusIcon(status){
                if(status == 1) {
                    return '<span class="label label-success" id="report-status">SUCCESS </span>'
                } else {
                    return '<span class="label label-danger" id="report-status">ERROR </span>'
                }
            }

            $('#modalUploadFile').on('hidden.bs.modal', function(){
                document.getElementById("material_file").value = "";
                $("#import_status_parent").hide();
                $("#import_status tbody").html("");
                $('#total_data').hide();
                $('#progres').hide();
            })          
                    
        }) 
    });
</script>
