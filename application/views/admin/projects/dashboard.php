
<?php $this->load->view('admin/projects/header'); ?>
<section class="content-body">
    <div class="row">
        <div class="col-md-12">
            <div class="nav-tabs-custom">
                <?php $this->load->view('admin/projects/menu'); ?>
            </div>
        </div>
    </div>
    <div class="container-full">
        <div class="col-md-12">
            <div class="box box-solid"><?php $this->load->view('admin/projects/title_dashboard'); ?></div>
        </div>
    </div>
    <div class="container-full">
        <div class="piechart">
            <div class="row">
                <div class="col-lg-6 col-xs-12">
                    <div class="box box-solid">
                        <div class="box-header with-border">
                            <h3 class="box-title">JOBCARD STATUS</h3>
                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                            </div>
                        </div>
                        <div class="box-body piechart col-md-12">
                            <canvas id="jc_by_status"  height="100"></canvas>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="col-lg-6 col-xs-12">
                    <div class="box box-solid">
                        <div class="box-header with-border">
                            <h3 class="box-title">MDR STATUS</h3>
                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                            </div>
                        </div>
                        <div class="box-body piechart col-md-12">
                            <canvas id="mdr_by_status"  height="100"></canvas>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <div class="row">
                
                <div class="col-md-6">
                    <div class="container-full">
                        <div class="box box-solid">
                            <div class="box-header with-border">
                                <h3 id="jobcard-perphase" class="box-title">PERCENTAGE JOBCARD PERPHASE</h3>
                                <div class="chart">
                                    <canvas id="jobcard_per_phase" width="100" height="10"></canvas>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="container-full">
                        <div class="box box-solid">
                            <div class="box-header with-border">
                                <h3 class="box-title">MDR OPEN &amp; CLOSE COMPARISON</h3>
                                <div class="chart">
                                    <canvas id="mdr_per_area" width="100" height="10"></canvas>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6 col-xs-12">
                    <div class="box box-solid">
                        <div class="box-header with-border">
                            <h3 class="box-title">PERCENTAGE JOBCARD STATUS PER AREA</h3>
                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                </button>
                                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                            </div>
                        </div>
                        <div class="box-body col-md-12 fonts">
                            <canvas id="jobcard_status_per_area" width="100" height="10"></canvas>
                        </div>
                        
                        <div class="clearfix"></div>
                        <!-- /.box-body -->
                    </div>
                </div>                          
                <div class="col-lg-6 col-xs-12">
                    <div class="box box-solid">
                        <div class="box-header with-border">
                            <h3 class="box-title">MATERIAL STATUS QTY</h3>
                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                </button>
                                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                            </div>
                        </div>
                        <div class="box-body col-md-12 fonts">
                            <canvas id="bar_material_status" width="100" height="10"></canvas>
                        </div>
                        
                        <div class="clearfix"></div>
                        <!-- /.box-body -->
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6 col-xs-12">
                    <div class="box box-solid">
                        <div class="box-header with-border">
                            <h3 class="box-title">MDR SENT</h3>
                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                </button>
                                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                            </div>
                        </div>
                        <div class="box-body col-md-12 fonts">
                            <canvas id="mdr_sent"></canvas>
                        </div>
                        
                        <div class="clearfix"></div>
                        <!-- /.box-body -->
                    </div>
                </div>                          
                <div class="col-lg-6 col-xs-12">
                    <div class="box box-solid">
                        <div class="box-header with-border">
                            <h3 class="box-title">MDR PROGRESS STATUS</h3>
                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                </button>
                                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                            </div>
                        </div>
                        <div class="box-body col-md-12 fonts">
                            <canvas id="mdr_progress_status"></canvas>
                        </div>
                        
                        <div class="clearfix"></div>
                        <!-- /.box-body -->
                    </div>
                </div>
            </div>            
        </div>
    </div>
</div>
</div>
</div>
</section>


<!-- <script src="<?php echo base_url(); ?>assets/bower_components/chart.js/Chart.min.js"></script> -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.min.js"></script>

<script type="text/javascript">

    $(document).ready(function(){
        get_jc_status();
        load_single_bar_data("get_jc_phase");
        load_single_bar_data("get_jc_area");
        load_single_bar_data("get_mat_qty");
        load_stack_bar_data("get_mdr_compare");
        load_stack_bar_data("get_jc_skill");
        load_pie_chart_data("get_mdr_shop");
        load_pie_chart_data("get_mdr_acc_status");
    })

    var bg_color_1 = [ "#e74c3c", "#3498db", "#2ecc71" ];   
    var bg_color_2 = [ "#f7aa00", "#cc99f9", "#ff5959", "#d1478c", "#00a8b5", "#f30e5c", "#89f8ce", "#182952", "#155e63", "#48e0e4", "#53d397", "#6900ff", "#92e6e6", "#00d1ff", "#c00000", "#ede862", "#ff467e", "#0278ae", "#4ef037", "#3f52e3", "#453246"]   
    var bg_color_3 = [ "#155e63", "#48e0e4", "#53d397", "#6900ff", "#92e6e6", "#00d1ff", "#c00000", "#ede862", "#ff467e", "#0278ae", "#4ef037", "#3f52e3", "#453246", "#f7aa00", "#cc99f9", "#ff5959", "#d1478c", "#00a8b5", "#f30e5c", "#89f8ce", "#182952"]
    var bg_color_4 = [ "#ff467e", "#0278ae", "#4ef037", "#3f52e3", "#453246", "#f7aa00", "#155e63", "#48e0e4", "#53d397",  "#cc99f9", "#ff5959", "#d1478c", "#00a8b5", "#f30e5c", "#89f8ce", "#182952", "#6900ff", "#92e6e6", "#00d1ff", "#c00000", "#ede862"]
    
    function get_jc_status(){
        var url = '<?php echo base_url('Projects/dashboard_jobcard_by_status') ?>'
        var REVNR = '<?php echo $id_project ?>';
        $.ajax({
            url: url,
            method: 'POST',
            dataType: 'text',
            data: { REVNR: REVNR },
            success: function(data){
                var response = $.parseJSON(data);
                if (response.status === 'success') {
                    
                    if (response.body.mdr_status !== undefined) {
                        buildPieChart(buildLabels(response.body.mdr_status),  buildData(response.body.mdr_status), "mdr_by_status", bg_color_1);
                    } else {
                        chart_no_data($('#mdr_by_status'));
                    }

                    if (response.body.jc_status !== undefined) {
                        buildPieChart(buildLabels(response.body.jc_status),  buildData(response.body.jc_status), "jc_by_status", bg_color_1);
                    } else {
                        chart_no_data($('#jc_by_status'));    
                    }

                }
            }
        })
    }

   function buildLabels(data){
        var labels = [];
        $.each(data, function(key, val){
            labels.push(val.LABELS);
        })
        return labels;
    }

    function buildData(data){
        var chart_data = [];    
        $.each(data, function(key, val){
            chart_data.push(parseFloat(val.QTY));
        })
        return chart_data;
    }

    function buildDataOpen(data){
            var chart_data = [];    
            $.each(data, function(key, val){
                chart_data.push(parseFloat(val.QTY_OPEN));
            })
            return chart_data;
        }

   function buildDataClose(data){
            var chart_data = [];    
            $.each(data, function(key, val){
                chart_data.push(parseFloat(val.QTY_CLOSE));
            })
            return chart_data;
        }    

    function buildPieChart(labels, data, elemnetId, bg) {
        var ctx = document.getElementById(elemnetId).getContext('2d');
        var DoughnutChart = new Chart(ctx, {
            type: 'doughnut',
            data: {
                labels: labels,     
                datasets:[{
                    data: data,
                    backgroundColor:  bg
                }]
            },
            options: DoughnutOptions
        });
    }

    var DoughnutOptions = {
            responsive: true,
            legend: {
                position: 'right',
                align: 'center',
                display: true
            },
            tooltips: {
                mode: 'label',
                callbacks: {
                    label: function(tooltipItem, data) {
                        var total = 0;
                        var lbl = data['labels'][tooltipItem['index']];
                        $.each(data['datasets'][0]['data'], function(key, val){
                            total += val;
                        })
                        return lbl + " " + get_percentage(data['datasets'][0]['data'][tooltipItem['index']], total) + '%' +" [QTY : "+data['datasets'][0]['data'][tooltipItem['index']]+"]";
                    }
                }
            },
    }

     function get_percentage(number, divider) {
        return number != 0 ? ((number / divider) * 100).toFixed(2) : 0;
    }

    function load_single_bar_data(prefix_url){
        var REVNR = '<?= $id_project ?>';
        var url = '<?= base_url('Board/') ?>' + prefix_url;
        $.ajax({
            url: url,
            method: 'GET',
            dataType: 'text',
            data: {REVNR: REVNR},
            success: function (data) {
                var response = $.parseJSON(data);
                if (response.status === 'success' && response.body.length > 0) {
                    if(prefix_url === "get_jc_phase")
                        buildSingleBarChar( buildLabels(response.body), buildData(response.body), "jobcard_per_phase", true, bg_color_2 );
                    if(prefix_url === "get_jc_area")
                        buildSingleBarChar( buildLabels(response.body), buildData(response.body), "jobcard_status_per_area", true, bg_color_3.reverse() );
                    if(prefix_url === "get_mat_qty")
                        buildSingleBarChar( buildLabels(response.body), buildData(response.body), "bar_material_status", false, bg_color_3 );
                } else {
                    if(prefix_url === "get_jc_phase")
                        chart_no_data($('#jobcard_per_phase'));
                    if(prefix_url === "get_jc_area")
                        chart_no_data($('#jobcard_status_per_area'));
                    if(prefix_url === "get_mat_qty")
                        chart_no_data($('#bar_material_status'));
                
                }
            }
        })                
    }

    function load_stack_bar_data(prefix_url){
        var REVNR = '<?= $id_project ?>';
        var url = '<?= base_url('Board/') ?>' + prefix_url;
        $.ajax({
            url: url,
            method: 'GET',
            dataType: 'text',
            data: {REVNR: REVNR},
            success: function (data) {
                var response = $.parseJSON(data);
                if (response.status === 'success' && response.body.length > 0) {
                    if(prefix_url === "get_mdr_compare")
                        buildStackBarChart(buildLabels(response.body), buildDataOpen(response.body), buildDataClose(response.body), "mdr_per_area");
                    if(prefix_url === "get_jc_skill")
                        buildStackBarChart(buildLabels(response.body), buildDataOpen(response.body), buildDataClose(response.body), "JobCardBySkill2");
                } else {
                    if(prefix_url === "get_mdr_compare")
                        chart_no_data($('#mdr_per_area'));
                    if(prefix_url === "get_jc_skill")
                        chart_no_data($('#JobCardBySkill2'));
                }
            }
        })                
    }    

    function load_pie_chart_data(prefix_url){
        var REVNR = '<?= $id_project ?>';
        var url = '<?= base_url('Board/') ?>' + prefix_url;
        $.ajax({
            url: url,
            method: 'GET',
            dataType: 'text',
            data: {REVNR: REVNR},
            success: function (data) {
                var response = $.parseJSON(data);
                if (response.status === 'success' && response.body.length > 0) {
                    if(prefix_url === "get_mdr_shop")
                        buildPieChart(buildLabels(response.body), buildData(response.body), "mdr_sent", bg_color_2.reverse())
                    if(prefix_url === "get_mdr_acc_status")
                        buildPieChart(buildLabels(response.body), buildData(response.body), "mdr_progress_status", bg_color_3.reverse())
                } else {
                    if(prefix_url === "get_mdr_shop")
                        chart_no_data($('#mdr_sent'));
                    if(prefix_url === "get_mdr_acc_status")
                        chart_no_data($('#mdr_progress_status'));
                }
            }
        })                
    }
  

    function buildSingleBarChar(labels, data, elemnetId, is_percentage, bg) {
        var ctxPerPhase2 = document.getElementById(elemnetId);
        if (ctxPerPhase2 != null) {
            ctxPerPhase2.height = 50;
            var myChart = new Chart(ctxPerPhase2, {
                type: 'bar',
                data: {
                    labels: labels,
                    datasets: [{
                        data: data,
                        backgroundColor: bg
                    }]
                },
                options: {
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero: true,
                                autoSkip: false,
                                callback: function(value, index, values) {
                                    return is_percentage ? value + " %" : value
                                }
                            }
                        }],
                        xAxes: [{
                            ticks: {
                                autoSkip: false,
                                fontSize: 8
                            }
                        }]
                    },
                    legend: {
                        display: false
                    },
                    tooltips: {
                        mode: 'label',
                        callbacks: {
                            label: function(tooltipItem, data) {
                                return is_percentage ? data['datasets'][0]['data'][tooltipItem['index']] + '%' : data['datasets'][0]['data'][tooltipItem['index']]
                            }
                        }
                    }                            
                }
            });
        }                
    }

    function buildStackBarChart(labels, dataOpen, dataClose, elemnetId){
        var ctxOpenCloseComparassion2 = document.getElementById(elemnetId);
        if (ctxOpenCloseComparassion2 != null) {
            ctxOpenCloseComparassion2.height = 50;
            var chartOpenCloseComparassion2 = new Chart(ctxOpenCloseComparassion2, {
                type: 'bar',
                data: {
                    labels: labels,
                    datasets: [{
                        label: 'Open',
                        data: dataOpen,
                        backgroundColor: 'rgba(255, 99, 132, 1)'
                    }, {
                        label: 'Close',
                        data: dataClose,
                        backgroundColor: 'rgba(54, 162, 235, 1)'
                    }]
                },
                options: {
                    scales: {
                        xAxes: [{
                            stacked: true,
                            ticks: {
                                autoSkip: false,
                                fontSize: 12
                            }
                        }],
                        yAxes: [{
                            stacked: true,
                            autoSkip: false,
                            fontSize: 6
                        }]
                    }
                }
            });
        }                
    }
    
    var dynamicColors = function() {
        var r = Math.floor(Math.random() * 255);
        var g = Math.floor(Math.random() * 255);
        var b = Math.floor(Math.random() * 255);
        return "rgb(" + r + "," + g + "," + b + ")";
    }

    function generate_bg(data) {
        var bg = [];
        $.each(data, function(val){
            bg.push(dynamicColors());
        })
        return bg;
    }


    function chart_no_data(chart){
        var content = "" +
        "<div>"+
            "<center> <span> No data to display </center>"+
        "</div>";
        chart.parent().html(content);
    }

    


</script>



