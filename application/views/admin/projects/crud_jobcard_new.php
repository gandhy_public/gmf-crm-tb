<?php 
    $this->load->view('admin/projects/header'); 
    $this->load->helper('auth');
    $is_customer = is_customer($session['user_group']);
    $is_edit = $write == 1 ? true : false;
    $id_project = json_encode($id_project);

?>
<!-- <style>
    table > thead > tr > th.editablegrid-ITVAL div a,
    table > thead > tr > th.editablegrid-SKILL div a,
    table > thead > tr > th.editablegrid-AREA div a,
    table > thead > tr > th.editablegrid-PHASE div a,
    table > thead > tr > th.editablegrid-DAY div a,
    table > thead > tr > th.editablegrid-DATECLOSE div a,
    table > thead > tr > th.editablegrid-STATUS div a,
    table > thead > tr > th.editablegrid-REMARK div a,
    table > thead > tr > th.editablegrid-DOC_SENT_STATUS div a,
    table > thead > tr > th.editablegrid-FREETEXT div a,
    table > thead > tr > th.editablegrid-DATEPROGRESS div a,
    table > thead > tr > th.editablegrid-CABINSTATUS div a 
    {
        color: #0097e6 !important;
    }
    
</style> -->
<style>
    button.delete{
    display:none;
    padding:4px 9px !important;
}
.handsontable td {
    vertical-align: middle !important;
    font-family:  Roboto,Helvetica,Arial,sans-serif;
    font-size: 12px;
}
.handsontable th {
    font-size: 12px; 
    }
    td.plum{
     background-color: plum;   
    }
td.yellow{
            background-color: yellow;
        }
        td.lime{
            background-color: lime;
        }
        td.orange{
            background-color: orange;
        }
        td.tan{
            background-color: tan;
        } td.cyan{
            background-color: cyan;
        }td.silver{
            background-color: silver;
        }td.aqua{
            background-color: aqua;
        }td.red{
            background-color: red;
        }td.pink{
            background-color: pink;
        }td.magenta{
            background-color: magenta;
        }td.skyblue{
            background-color: skyblue;
        }

    div.wtHolder{
        width: 100% !important;
    }
    button.changeType{
        position:sticky !important;
    }

        .handsontable th {
            background-color: #EEE;
            color: #222;
            text-align: center;
            font-weight: normal;
            white-space: nowrap;
            padding:8px !important;
            min-width:80px;
        }
        .handsontable th.edited {
            color: #0097e6 !important;
        }
        .edited {
            color: #0097e6 !important;
        }
        .currentRow {
          background-color: lightgoldenrodyellow !important;
        }
</style>
<!-- <script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script> -->
<!-- Main content -->
<section class="content">
    <!-- Live preview-->
    <div class="row">
        <div class="col-md-12">
            <!-- Custom Tabs -->
            <div class="nav-tabs-custom">
                <?php $this->load->view('admin/projects/menu'); ?>
                <div class="tab-content">
                    <section class="content" style="overflow: auto;">
                        <div id="message"></div>
                        <div id="wrap">
                            <!-- <h3>JOBCARD</h3>-->
                            <!-- Feedback message zone -->
                            <div id="toolbar">
                                <input type="text" id="filter" name="filter" placeholder="Filter :type any text here" />
                                <!-- Check if User Is Customer -->
                                <?php if($is_customer){?>
                                <div class="pull-right form-inline">
                                    <a href="<?php echo base_url('index.php/Projects/write_xlsx_file_jobcard?REVNR=' . json_decode($id_project) )?>" class="btn btn-info" id="download" target="_blank">Export Excel <i class="fa fa-download" aria-hidden="true"></i></a>
                                </div>
                                <?php }elseif(!$is_customer && $is_edit) { ?>
                                <div class="pull-right form-inline">
                                    <a href="<?php echo base_url('index.php/Projects/write_xlsx_file_jobcard?REVNR=' . json_decode($id_project) )?>" class="btn btn-info" id="download" target="_blank">Export Excel <i class="fa fa-download" aria-hidden="true"></i></a>
                                    <button class="btn btn-info" data-toggle="modal" data-target="#modalUploadFile">
                                        Import Excel
                                        <i class="fa fa-upload" aria-hidden="true"></i>
                                    </button>
                                </div>
                                <?php } ?>
                            </div>
                            <!-- Grid contents -->
                            <div id="tablecontentHT" class="hot handsontable htRowHeaders htColumnHeaders ht__manualColumnMove">
                                <!--  <div class="lds-ellipsis"><div></div><div></div><div></div><div></div></div> -->
                            </div>
                        </div>

                        <!-- simple form, used to add a new row -->
                        <div id="dialog" style="overflow: hidden;">
                            <iframe id="add_iframe" src="" style="border: none; width: 100%; height: 100%; overflow: hidden;"></iframe>
                        </div>
                    </section>
                </div>
                <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
        </div>
        <!-- nav-tabs-custom -->
        <!-- /.col -->
    </div>
</section>

<div id="modalUploadFile" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Import Jobcard</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" id="form-upload" enctype="multipart/form-data">
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="email">Browse File</label>
                        <div class="col-sm-9">
                            <input type="file" class="form-control" id="jobcard_file" placeholder="Enter Order Number" name="media">
                            <small id="order_number_help" class="text-danger" style="display: none;">
                                Try another! Order Number Not Found.
                            </small>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-offset-3 col-sm-9">
                            <!-- <button type="submit" class="btn btn-primary" id="upload_data"> <i id="loading" class='fa fa-circle-o-notch fa-spin' style="display: none;"></i>  Upload Data <i class="fa fa-upload" aria-hidden="true"></i></button> -->
                            <p id="total_data"></p>
                            <div id="progres">
                                <p>Total Sukses : <span id="sukses">0</span></p>
                                <p>Total Gagal : <span id="gagal">0</span></p>
                            </div>
                            <a class="btn btn-primary" id="upload_data"> <i id="loading" class='fa fa-circle-o-notch fa-spin' style="display: none;"></i> Upload Data <i class="fa fa-upload" aria-hidden="true"></i></a>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        <div class="modal-content" style="display: none" id="import_status_parent">
            <div class="modal-header">
                <h4 class="modal-title">Import Status</h4>
            </div>
            <div class="modal-body">
                <table class="table table-bordered table-hover dataTable" id="import_status">
                    <thead>
                        <tr>
                            <th width="60%">Order Number</th>
                            <th width="30%">Status</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>

    </div>
</div>

<div id="modalMdr" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">MDR List </h4>
            </div>
            <div class="modal-body">
                <table class="table table-bordered table-hover dataTable" id="mdr-list">
                    <thead>
                        <tr>
                            <th width="20px">SEQ</th>
                            <th>ORDER</th>
                            <th width="20%">DESCRIPTION</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        // var data = []; // for (var i = 0; i <=1 00000; i++) { // data.push([i, i, i, i, i]); // }
        var hot;
        var data = [];
        var header = [];
        var colname = [];
        var col = [];
        var coltype = [];
        var editable = [];

        $.ajax({
            url: '<?php echo base_url() ?>' + 'projects/crud_jobcard_load_all_new?edit=' + <?= $write ?> + '&REVNR=' + <?= $id_project ?>,
            dataType: 'json',
            type: 'GET',
            success: function(res) {
                // for(var entry in res.data){
                //     data.push(res.data[entry].values);
                // }
                // var cd =res.data.length;
                for (var entry in res.metadata) {
                    var a = {};
                    header.push(res.metadata[entry].label);
                    colname.push(res.metadata[entry].name);
                    coltype.push(res.metadata[entry].datatype);
                    data.push(res.data[entry].values);
                    var rend = res.metadata[entry].datatype;
                    var editor = res.metadata[entry].editable;
                    editable.push(res.metadata[entry].editable);
                    var source = res.metadata[entry].values;
                    if (rend == 'int') {
                        rend = "numeric";
                        a.renderer = rend;
                    } else if (rend == 'string') {
                        rend = "text";
                        a.renderer = rend;
                    } else if (rend == 'date') {
                        rend = "date";
                        a.type = 'date';
                        a.dateFormat = 'DD MMM YYYY';
                        a.correctFormat = true;
                        a.defaultDate = '1900-01-01';
                    } else {
                        a.renderer = rend;
                    }
                    if (!editor) {
                        a.editor = false;
                    }
                    if (source != null) {
                        // a.renderer='styleDropdown';
                        a.type = 'dropdown';

                        var b = [];
                        for (var c in source) {
                            b.push(source[c].value)
                        }
                        if (b != null)
                            a.source = b;



                    }
                    col.push(a);
                }

                HO(data, editable);
            }
        });
        var container = document.getElementById('tablecontentHT');
        var chot;
        function HO() {
            hot = new Handsontable(container, {
                fixedColumnsLeft: 2,
                colHeaders: header,
                columns: col,
                currentRowClassName: 'currentRow',
                manualColumnResize: true,
                width: '100%',
                filters: true,
                columnSorting: true,
                sortIndicator: true,
                dropdownMenu: [
                    'filter_by_condition',
                    '---------',
                    'filter_operators',
                    '---------',
                    'filter_by_value',
                    '---------',
                    'filter_action_bar'
                ],
                afterChange: function(change, source) {
                    if (source === 'loadData') {
                        return; 
                    }
                    var rowIdx = change[0][0];
                    var colIdx = change[0][1];
                    var oldValue = change[0][2];
                    var newValue = change[0][3];
                    var order = hot.getDataAtCell(change[0][0], 1);
                    if (oldValue == newValue)
                        return;
                    $.ajax({
                        url: '<?php echo base_url(); ?>projects/crud_jobcard_update',
                        type: 'POST',
                        dataType: "html",
                        data: {
                            id: order,
                            newvalue: coltype[colIdx] == "boolean" ? (newValue ? 1 : 0) : newValue,
                            colname: colname[colIdx],
                            coltype: coltype[colIdx]
                        },
                        success: function(response) {
                            var success = response.search("ok") > 0;
                            if (!success) {
                                hot.setDataAtCell(colIdx, colIdx, oldValue);
                                display_toast('danger', 'Error to update, please check on Column ' + hot.getColHeader(colIdx) + ', Order : ' + order);
                            } else {
                                display_toast('success', 'Order : ' + order + ', Column ' + colname[colIdx] + ' updated to ' + newValue);
                            }
                        },
                        error: function(XMLHttpRequest, textStatus, exception) {
                            alert("Ajax failure\n" + errortext);
                            hot.setDataAtCell(colIdx, colIdx, oldValue);
                        },
                        async: true
                    });
                },
                afterRender: function() {
                    $(".ht_master .wtHolder").height($(".ht_master .wtSpreader").height() + 24);
                    $(".ht_master .wtHolder").floatingScroll();
                    $("#hot-display-license-info").hide();
                    $('.fl-scrolls').css('z-index', '103');
                },
                afterGetColHeader: addInput,
                beforeOnCellMouseDown: doNotSelectColumn
            });
            hot.loadData(data);
            var ch = hot.getColHeader();
            hot.updateSettings({
                cells: styleDropdown,
                colHeaders: function(col) {
                    if (editable[col]) {
                        return '<div class="edited">' + ch[col] + '</div>';
                    } else {
                        return ch[col];
                    }

                }
            });

        }
        var status = 0;
        var status_sap = 0;

        function styleDropdown(row, col, prop) {
            var value = hot.getDataAtCell(row, col);
            var element = hot.getCell(row, col); 
            var colnameH
            colnameH = colname[col];
            if (element) {
                if ((colname[col].toLowerCase() == 'status') || (colname[col].toLowerCase() == 'status_sap')) {
                    if (typeof value == 'string' && (value.toLowerCase() == 'close' || value.toLowerCase() == 'closed')) {
                        element.className = "lime";
                        element.style.fontWeight = "bold";
                    } else if (typeof value == 'string' && value.toLowerCase() == 'open') {
                        element.className = "red";
                        element.style.fontWeight = "bold";
                    } else if (typeof value == 'string' && value.toLowerCase() == 'perform by prod') {
                        element.className = "pink";
                        element.style.fontWeight = "bold";
                    } else if (typeof value == 'string' && value.toLowerCase() == 'perform to shop') {
                        element.className = "orange";
                        element.style.fontWeight = "bold";
                    } else if (typeof value == 'string' && value.toLowerCase() == 'waiting material') {
                        element.className = "magenta";
                        element.style.fontWeight = "bold";
                    } else if (typeof value == 'string' && value.toLowerCase() == 'waiting tool') {
                        element.className = "skyblue";
                        element.style.fontWeight = "bold";
                    } else if (typeof value == 'string' && value.toLowerCase() == 'prepare for install') {
                        element.className = "plum";
                        element.style.fontWeight = "bold";
                    } else if (typeof value == 'string' && value.toLowerCase() == 'prepare for test') {
                        element.className = "aqua";
                        element.style.fontWeight = "bold";
                    } else if (typeof value == 'string' && value.toLowerCase() == 'prepare for run up') {
                        element.className = "tan";
                        element.style.fontWeight = "bold";
                    } else if (typeof value == 'string' && value.toLowerCase() == 'prepare for ndt') {
                        element.className = "silver";
                        element.style.fontWeight = "bold";
                    } else if (typeof value == 'string' && value.toLowerCase() == 'part avail') {
                        element.className = "cyan";
                        element.style.fontWeight = "bold";
                    } else if (typeof value == 'string' && value.toLowerCase() == 'progress') {
                        element.className = "yellow";
                        element.style.fontWeight = "bold";
                    } else if (typeof value == 'string' && value.toLowerCase() == 'final') {
                        element.className = "orange";
                        element.style.fontWeight = "bold";
                    } else {
                        $(element).removeAttr('style');
                    }
                    if ((colnameH.toLowerCase() == 'status')) {
                        if (value !== null) {
                            status = value.toLowerCase() === 'closed' ? 1 : 0;
                        } else {
                            status = 0;
                        }

                    }

                    if ((colnameH.toLowerCase() == 'status_sap')) {
                        if (value !== null) {
                            status_sap = value.toLowerCase() === 'closed' ? 1 : 0;
                        } else {
                            status_sap = 0;
                        }
                    }

                    if (status == 1 && status_sap == 1) {
                        $(element).parent().children().addClass("td-close");
                        // $(element).parent().children().addClass("td-close");
                        setTimeout(function() {
                            $(element).parent().children().addClass("td-close");
                        }, 1000);
                        status = 0;
                        status_sap = 0;
                    }

                    if (colnameH.toLowerCase() == 'mdr_status') {
                        if (value !== null) {
                            if (value.toLowerCase() === 'closed') {
                                $(element).parent().children().addClass("td-close");
                                setTimeout(function() {
                                    $(element).parent().children().addClass("td-close");
                                }, 1000);
                            }
                        }
                    }


                }

            }
        }

        var debounceFn = Handsontable.helper.debounce(function(colIndex, event) {
            var filtersPlugin = hot.getPlugin('filters');
            if(colIndex.length>0){
                for(var a in colIndex){
                    filtersPlugin.removeConditions(a);
                }
                for(var a in colIndex){
                    if(event.realTarget.value!="" && event.realTarget.value!="-")
                    filtersPlugin.addCondition(a, 'contains', [event.realTarget.value]);
                    if(event.realTarget.value=="-")
                    filtersPlugin.addCondition(a, 'empty',[""]);
                    filtersPlugin.filter();
                    if(hot.countRows()==0){
                    filtersPlugin.removeConditions(a);
                    filtersPlugin.filter();
                    }

                }
            }else{
            filtersPlugin.removeConditions(colIndex);
            if(event.realTarget.value!="" && event.realTarget.value!="-")
            filtersPlugin.addCondition(colIndex, 'contains', [event.realTarget.value]);
            if(event.realTarget.value=="-")
            filtersPlugin.addCondition(colIndex, 'empty',[""]);
             filtersPlugin.filter();
            }
           
        }, 200);

        var addEventListeners = function(input, colIndex) {
            input.addEventListener('keydown', function(event) {
                if(event.keyCode==13)
                debounceFn(colIndex, event);
            });
        };

        var getInitializedElements = function(colIndex) {
          
            var xin = document.getElementById("filter");
            var div = document.createElement('div');
            var input = document.createElement('input');
            var att = document.createAttribute("placeholder");
            att.value = "Find";
            input.setAttributeNode(att);
            div.className = 'filterHeader';
            div.style.width="100%";
            addEventListeners(input, colIndex);
            addEventListeners(xin, colname);
            div.appendChild(input);
            return div;
        };

        var addInput = function(col, TH) {
            // Hooks can return value other than number (for example `columnSorting` plugin use this).
            if (typeof col !== 'number') {
                return col;
            }
            if (col >= 0 && TH.childElementCount < 2) {
                TH.appendChild(getInitializedElements(col));
            }
        };

        // Deselect column after click on input.
        var doNotSelectColumn = function(event, coords) {
            if (coords.row === -1 && event.realTarget.nodeName === 'INPUT') {
                event.stopImmediatePropagation();
                this.deselectCell();
            }
        };
        $(document).scroll(function() {
            // console.log($('.ht_clone_left tbody').outheight());
            // $('.ht_clone_left tbody').height($('.ht_master tbody').height());
            // var i=0;
            // $('.ht_clone_left tbody tr').find('td:first-child').each (function() {
            //         console.log($(this).outerHeight(true),$('.ht_master tbody tr:eq('+i+') td:first-child').outerHeight(true));
            //         i++;

            //     }); 

            // console.log($('.ht_clone_top_left_corner').offset().top,$('.ht_master').offset().top);
            // $('.ht_master thead tr').find('th:first-child').each (function() {
            //     console.log($(this).outerHeight(true),$(this).html());
            // });        


            // console.log($('.ht_master tbody').height());

        });

    });
</script>