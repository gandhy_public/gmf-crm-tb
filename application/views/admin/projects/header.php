<?php 
    $this->load->helper('text_formatter'); 
    $menu_idx = get_label_menu($_SESSION['menu_tab'], str_replace("_new","",str_replace("admin/", "", $content)));
?>
<section class="content-header">
    <h1>
        <?php echo $project_name; ?>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">
            <a href="<?php echo base_url() . $_SESSION['menu_tab'][$menu_idx]['l'] . '/' . $id_project ; ?> "> 
                <?= $_SESSION['menu_tab'][$menu_idx]['m'] ?> 
            </a>
        </li>
        <li class="active"><?php echo $project_name; ?> </li> 
    </ol>
</section>
