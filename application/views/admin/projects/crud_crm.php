<?php
    $this->load->view('admin/projects/header'); 
    $this->load->view('admin/projects/modal_crm'); 
    $is_customer = is_customer($session['user_group']);
    $is_edit = $write == 1 ? true : false;
    $id_project = json_encode($id_project);
?>

<style>
    table > thead > tr > th.editablegrid-POST div a,
    table > thead > tr > th.editablegrid-SP_OUT_NO div a,
    table > thead > tr > th.editablegrid-SP_OUT_DATE div a,
    table > thead > tr > th.editablegrid-SP_IN_NO div a,
    table > thead > tr > th.editablegrid-SP_IN_DATE div a,
    table > thead > tr > th.editablegrid-UNIT_SEND div a,
    table > thead > tr > th.editablegrid-UNIT_RECV div a,
    table > thead > tr > th.editablegrid-PART_LOC div a,
    table > thead > tr > th.editablegrid-REMARK div a,
    table > thead > tr > th.editablegrid-STATU div a,
    table > thead > tr > th.editablegrid-DESCRIPTION div a
    {
        color: #0097e6 !important;
    }
</style>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <!-- Custom Tabs -->
            <div class="nav-tabs-custom">
                <?php $this->load->view('admin/projects/menu'); ?>
                <div class="tab-content">
                    <section class="content" style="overflow: auto;">
                        <div id="message"></div>
                        <div id="wrap">
                            <!-- <h3>JOBCARD</h3>-->
                            <!-- Feedback message zone -->
                            <div id="toolbar">
                                <input type="text" id="filter" name="filter" placeholder="Filter :type any text here"  />
                                <div class="pull-right form-inline">
                                    <?php if(!$is_customer) { ?>
                                        <a href="<?php echo base_url('index.php/Projects/write_xlsx_file_crm?REVNR=' . json_decode($id_project) )?>" class="btn btn-info" id="download" target="_blank">Export Excel <i class="fa fa-download" aria-hidden="true"></i></a>                                       
                                    <?php } ?>
                                    <?php if($is_edit) { ?>
                                        <button id="addNew" data-toggle="modal" data-target="#modalOrderNumber" class="btn btn-info">
                                            <i class="fa fa-plus" aria-hidden="true"></i> 
                                                New
                                        </button>                                      
                                    <?php } ?>  
                                </div> 
                            </div>
                            <!-- Grid contents -->
                            <style type="text/css">
                                #tablecontent td:hover {
                                    color: black;
                                }
                            </style>
                            <div id="tablecontent">
                                <div class="lds-ellipsis"><div></div><div></div><div></div><div></div></div>
                            </div>
                            <!-- Paginator control -->
                            <div id="paginator"></div>
                        </div>
                        <!-- script src="<?php echo base_url(); ?>assets/editablegrid/js/jquery-1.11.1.min.js" ></script -->
                        <script src="<?php echo base_url(); ?>assets/jquery-ui/js/jquery-ui-1.10.4.custom.min.js"></script>
                        <script type="text/javascript">
                            var base_url = "<?php echo base_url(); ?>projects/crud_crm_";

                            function highlightRow(rowId, bgColor, after)
                            {
                                var rowSelector = $("#" + rowId);
                                rowSelector.css("background-color", bgColor);
                                rowSelector.fadeTo("normal", 0.5, function () {
                                    rowSelector.fadeTo("fast", 1, function () {
                                        rowSelector.css("background-color", '');
                                    });
                                });
                            }

                            function highlight(div_id, style) {
                                highlightRow(div_id, style == "error" ? "#e5afaf" : style == "warning" ? "#ffcc00" : "#8dc70a");
                            }



                            function message(type, message) {
                                $('#message').html("<div class=\"notification  " + type + "\">" + message + "</div>").slideDown('normal').delay(1800).slideToggle('slow');
                            }

                            /**
                             updateCellValue calls the PHP script that will update the database.
                             */
                            function updateCellValue(editableGrid, rowIndex, columnIndex, oldValue, newValue, row, onResponse)
                            {
                                $.ajax({
                                    url: base_url + 'update',
                                    type: 'POST',
                                    dataType: "html",
                                    data: {
                                        tablename: editableGrid.name,
                                        id: editableGrid.getValueAt(rowIndex, 16),
                                        newvalue: editableGrid.getColumnType(columnIndex) == "boolean" ? (newValue ? 1 : 0) : newValue,
                                        colname: editableGrid.getColumnName(columnIndex),
                                        coltype: editableGrid.getColumnType(columnIndex)
                                    },
                                    success: function (response)
                                    {
                                        // reset old value if failed then highlight row
                                        var success = response.search("ok") > 0;
                                        if (!success) {
                                            editableGrid.setValueAt(rowIndex, columnIndex, oldValue);
                                            display_toast('danger', 'Error to update, please check on Column ' + editableGrid.getColumnName(columnIndex) + ', CRM : ' + editableGrid.getValueAt(rowIndex, 0) );
                                            // datagrid.fetchGrid();
                                        } else {
                                            display_toast('success', 'CRM : ' + editableGrid.getValueAt(rowIndex, 1) + ', Column '+ editableGrid.getColumnName(columnIndex) + ' updated to ' + editableGrid.getValueAt(rowIndex, columnIndex));
                                            // datagrid.fetchGrid();
                                        }
                                    },
                                    error: function (XMLHttpRequest, textStatus, exception) {
                                        alert("Ajax failure\n" + errortext);
                                    },
                                    async: true
                                });

                            }

                            function DatabaseGrid()
                            {
                                this.editableGrid = new EditableGrid("CRM", {
                                    enableSort: true,

                                    /* Comment this line if you set serverSide to true */
                                    // define the number of row visible by page
                                    /*pageSize: 50,*/

                                    /* This property enables the serverSide part */
                                    serverSide: true,

                                    // Once the table is displayed, we update the paginator state
                                    tableRendered: function () {
                                        updatePaginator(this);
                                    },
                                    tableLoaded: function () {
                                        datagrid.initializeGrid(this);
                                    },
                                    modelChanged: function (rowIndex, columnIndex, oldValue, newValue, row) {
                                        updateCellValue(this, rowIndex, columnIndex, oldValue, newValue, row);
                                    }
                                });
                                this.fetchGrid();

                                $("#filter").val(this.editableGrid.currentFilter != null ? this.editableGrid.currentFilter : "");
                                if (this.editableGrid.currentFilter != null && this.editableGrid.currentFilter.length > 0)
                                    $("#filter").addClass('filterdefined');
                                else
                                    $("#filter").removeClass('filterdefined');

                            }

                            DatabaseGrid.prototype.fetchGrid = function () {
                                // call a PHP script to get the data
                                this.editableGrid.loadJSON(base_url + "load?edit=" + "<?= $write ?>" + "&REVNR=" + <?= $id_project ?>);
                            };

                            DatabaseGrid.prototype.initializeGrid = function (grid) {

                                var self = this;

                                // render for the action column
                               grid.setCellRenderer("action", new CellRenderer({
                                    render: function (cell, ID) {
                                        cell.innerHTML += "<button class='btn btn-sm btn-danger delete' data-id='"+ID+"' onclick='mrm_delete("+ID+")'><i class='fa fa-trash-o' aria-hidden='true'></i> </button>";
                                    }
                                }));

                                grid.renderGrid("tablecontent", "testgrid");
                                $('html, body').scrollTop(1);
                                $('html, body').scrollTop(0);

                            };

                            DatabaseGrid.prototype.deleteRow = function (id)
                            {

                                var self = this;

                                if (confirm('Are you sur you want to delete the row id ' + id)) {

                                    $.ajax({
                                        url: base_url + 'delete',
                                        type: 'POST',
                                        dataType: "html",
                                        data: {
                                            tablename: self.editableGrid.name,
                                            id: id
                                        },
                                        success: function (response)
                                        {
                                            if (response == "ok") {
                                                message("success", "Row deleted");
                                                self.fetchGrid();
                                            }
                                        },
                                        error: function (XMLHttpRequest, textStatus, exception) {
                                            alert("Ajax failure\n" + errortext);
                                        },
                                        async: true
                                    });


                                }

                            };


                            DatabaseGrid.prototype.addRow = function (id)
                            {

                                var self = this;

                                $.ajax({
                                    url: base_url + 'add',
                                    type: 'POST',
                                    dataType: "html",
                                    data: {
                                        tablename: self.editableGrid.name,
                                        name: $("#name").val(),
                                        firstname: $("#firstname").val()
                                    },
                                    success: function (response)
                                    {
                                        if (response == "ok") {

                                            // hide form
                                            showAddForm();
                                            $("#name").val('');
                                            $("#firstname").val('');
                                            message("success", "Row added : reload model");
                                            self.fetchGrid();
                                        } else
                                            message("error", "Error occured");
                                    },
                                    error: function (XMLHttpRequest, textStatus, exception) {
                                        alert("Ajax failure\n" + errortext);
                                    },
                                    async: true
                                });
                            };

                            function updatePaginator(grid, divId)
                            {
                                divId = divId || "paginator";
                                var paginator = $("#" + divId).empty();
                                var nbPages = grid.getPageCount();

                                // get interval
                                var interval = grid.getSlidingPageInterval(20);
                                if (interval == null)
                                    return;

                                // get pages in interval (with links except for the current page)
                                var pages = grid.getPagesInInterval(interval, function (pageIndex, isCurrent) {
                                    if (isCurrent)
                                        return "<span id='currentpageindex'>" + (pageIndex + 1) + "</span>";
                                    return $("<a>").css("cursor", "pointer").html(pageIndex + 1).click(function (event) {
                                        grid.setPageIndex(parseInt($(this).html()) - 1);
                                    });
                                });

                                // "first" link
                                var link = $("<a class='nobg'>").html("<i class='fa fa-fast-backward'></i>");
                                if (!grid.canGoBack())
                                    link.css({opacity: 0.4, filter: "alpha(opacity=40)"});
                                else
                                    link.css("cursor", "pointer").click(function (event) {
                                        grid.firstPage();
                                    });
                                paginator.append(link);

                                // "prev" link
                                link = $("<a class='nobg'>").html("<i class='fa fa-backward'></i>");
                                if (!grid.canGoBack())
                                    link.css({opacity: 0.4, filter: "alpha(opacity=40)"});
                                else
                                    link.css("cursor", "pointer").click(function (event) {
                                        grid.prevPage();
                                    });
                                paginator.append(link);

                                // pages
                                for (p = 0; p < pages.length; p++)
                                    paginator.append(pages[p]).append(" ");

                                // "next" link
                                link = $("<a class='nobg'>").html("<i class='fa fa-forward'>");
                                if (!grid.canGoForward())
                                    link.css({opacity: 0.4, filter: "alpha(opacity=40)"});
                                else
                                    link.css("cursor", "pointer").click(function (event) {
                                        grid.nextPage();
                                    });
                                paginator.append(link);

                                // "last" link
                                link = $("<a class='nobg'>").html("<i class='fa fa-fast-forward'>");
                                if (!grid.canGoForward())
                                    link.css({opacity: 0.4, filter: "alpha(opacity=40)"});
                                else
                                    link.css("cursor", "pointer").click(function (event) {
                                        grid.lastPage();
                                    });
                                paginator.append(link);
                            }
                            ;
                        </script>
                        <script type="text/javascript">
                            var datagrid;
                            window.onload = function () {
                                datagrid = new DatabaseGrid();
                                // key typed in the filter field
                                $("#filter").keyup(function () {
                                    datagrid.editableGrid.filter($(this).val());

                                    // To filter on some columns, you can set an array of column index
                                    //datagrid.editableGrid.filter( $(this).val(), [0,3,5]);
                                });

                                $("#addbutton").click(function () {
                                    datagrid.addRow();
                                });
                            };

                            $(function () {
                                $("#dialog").dialog({
                                    autoOpen: false,
                                    modal: true,
                                    height: 600,
                                    width: 800,
                                    open: function (ev, ui) {
                                        $('#add_iframe').attr('src', $('#dialog').data('src'));
                                    },
                                    close: function (ev, ui) {
                                        location.reload();
                                    }
                                });
                            });

                        </script>
                        <!-- simple form, used to add a new row -->
                        <div id="dialog" style="overflow: hidden;">
                            <iframe id="add_iframe" src="" style="border: none; width: 100%; height: 100%; overflow: hidden;"></iframe>
                        </div>
                    </section>
                </div>
                <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
        </div>
        <!-- nav-tabs-custom -->
        <!-- /.col -->
    </div>
</section>


<div id="modalOrderNumber" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Find Order Number &amp; Part Number </h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal">
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="email">Order Number</label>
                        <div class="col-sm-9">
                            <select class="form-control" id="order_number" >
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-sm-3" for="email">Part Number</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="PART_NUMBER" placeholder="Enter Part Number">
                            <small id="part_number_help" class="text-danger" style="display: none;">
                                Try another! Part Number Not Found.
                            </small> 
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-sm-3" for="email">Serial Number</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="SERIAL_NUMBER" placeholder="Enter Serial Number">
                        </div>
                    </div>                      


                    <div class="form-group">
                        <div class="col-md-offset-3 col-sm-9">
                            <a class="btn btn-primary" id="create_crm" disabled="true">Create CRM</a>
                        </div>
                    </div>                    
                </form>
            </div>
        </div>

    </div>
</div>

<style>
    .modal{
        overflow-y: auto !important;
    }
</style>

<script type="text/javascript">

    var crm_data = {}

    $(document).ready(function(){

        $('#addNew').click(function(){
            clear_form();
        })

        var url = '<?php echo base_url() ?>' + 'Projects/find_order_number?REVNR=' + <?= $id_project ?>;
        $('#order_number').select2({
            width: '100%',
            minimumInputLength: 6,
            triggerChange: true,
            allowClear: true,
            ajax: {
                url: url,
                minimumInputLength: 5,
                dataType: 'json',
                delay: 450,
                processResults: function(data){
                    var response = data.body
                    var jobcard = { id: 0, text: 'Jobcard', children: [] };
                    var mdr = { id: 1, text: 'MDR', children: [] };

                    $.each(response, function(key, val){
                        if(val.AUART === 'GA01'){
                            jobcard.children.push({ id: val.AUFNR, text: val.AUFNR});
                        } else {
                            mdr.children.push({ id: val.AUFNR, text: val.AUFNR});
                        }
                    })

                    return {
                        results: [jobcard, mdr]
                    }
                },
                cache: true
            }
        }).select2('val', []);


        $('#order_number').on("select2:select", function(e) { 
            find_order_number(e.params.data.id);
        });

        $("#PART_NUMBER").focusout(function(){
            find_part_number();
        });

        $('#SERIAL_NUMBER').focusout(function(){
            check_duplicate_crm();
        })

        $('#create_crm').click(function () {
            var on = $('#order_number').val();
            var pn = $('#PART_NUMBER').val();
            var sn = $('#SERIAL_NUMBER').val();
            
            if((on != null) && (pn != "") && (sn != "")){
                $('#SN_QTY').val(sn)
                $('#SN_QTY').attr('readonly', true)
                
                $('#modalOrderNumber').modal('hide');
                $('#modalCrm').modal('show');
            }else{
                swal("Opss!", "Order Number, Serial Number & Part Number is Required!", "error");
            }
        })

        $('#save_crm').click(function(){
            saveMaterial();
        })

    })

    function find_order_number(order_number){
        var order_type = $('#order_type').val(); 

        if(order_number !== null){
            var data = {
                AUFNR: order_number,
                ORDER_TYPE : order_type,
                MENU: 'MRM'
            }
            $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>" + "projects/find_order_number_data",
                data: data,
                dataType: "text",
                cache: false,
                success: function(data){
                        var response = $.parseJSON(data);
                        if(response.status === 'success'){
                            var order = response.body; 
                            crm_data["CARD_TYPE"] = order.AUART === 'GA01' ? 'JC' : 'MDR';
                            crm_data["AUFNR"] = order.AUFNR;
                            crm_data["KTEXT"] = order.KTEXT;
                        }else{
                            // handleOrderError();
                        }
                }
            })
        }
    }

    function find_part_number(){
        var part_number = $('#PART_NUMBER').val(); 
        var url = '<?php echo base_url() ?>' + 'projects/load_material_data'
        $.ajax({
            url: url,
            method: 'POST',
            dataType: 'text',
            data: { MATNR: part_number },
            success: function(data){
                $('#modalMaterial').modal('hide');
                var response = $.parseJSON(data);
                if(response.status === 'success'){
                    var material = response.body;

                    // check_duplicate_mrm();
                    $('#create_crm').attr('disabled', false)

                    $('#ALT_PART_NUMBER').val(material.ALT_PART_NUMBER);
                    $('#MAKTX').val(material.MAKTX);
                    $('#MATNR').val(material.MATNR);
                    $('#CTG').val(material.CTG);

                    $('#ALT_PART_NUMBER').prop('readonly', true);
                    $('#MATNR').prop('readonly', true);
                    $('#MAKTX').prop('readonly', true);
                    $('#CTG').prop('readonly', true);
                }else{
                    $('#create_crm').attr('disabled', false)
                    $('#MATNR').val($('#PART_NUMBER').val());                
                }
            }
        })
    }

    function check_duplicate_crm(){
        var order_number = $('#order_number').val();
        var SN_QTY = $('#SERIAL_NUMBER').val();
        var REVNR = <?= $id_project ?>

        var url = '<?php echo base_url() ?>' + 'projects/check_duplicate_crm'
        var data = {
            AUFNR: order_number,
            SN_QTY: SN_QTY,
            REVNR: REVNR
        }

        $.ajax({
            url: url,
            method: 'POST',
            dataType: 'text',
            data: data,
            success: function(data){
                var response = $.parseJSON(data);
                if(response.status === 'error'){
                    swal("Another CRM with this Order Number & Serial Number already exists!?", {
                        buttons: {
                            cancel: "Cancel",
                            replace: {
                                text: "Replace it!",
                                value: true
                            }
                        }
                    })
                    .then(function(value){
                        if(value != true){
                            $('#SERIAL_NUMBER').val('')
                        }
                    })

                }else{
                    $('#create_crm').attr('disabled', false)
                }
            }
        })
    }      

    function saveMaterial(){
        var url = '<?php echo base_url() ?>' + 'admin/Master_data/crud_material_add';
        var data = {
            ALT_PART_NUMBER: $('#ALT_PART_NUMBER').val(),
            MAKTX: $('#MAKTX').val(),
            MATNR: $('#MATNR').val(),
            CTG: $('#CTG').val()
        }

        if ($('#MAKTX').is('[readonly]')) {
            saveCrm()
            return
        }

        $.ajax({
            url: url,
            method: 'POST',
            dataType: 'text',
            data: data,
            success: function(data){
                var response = $.parseJSON(data);
                if(response.status === 'success'){
                    saveCrm()
                }else{
                    swal("Opss!", "Error while saving Material!", "error");
                }
            }
        })
    }
    
    function saveCrm(){
        crm_data["ALT_PART_NUMBER"] = $('#ALT_PART_NUMBER').val();
        crm_data["MAKTX"] = $('#MAKTX').val();
        crm_data["MATNR"] = $('#MATNR').val();
        crm_data["CTG"] = $('#CTG').val();
        crm_data["IPC"] = $('#IPC').val();
        crm_data['SN_QTY'] = $('#SN_QTY').val(),
        crm_data['POST'] = $('#POST').val(),
        crm_data['SP_OUT_NO'] = $('#SP_OUT_NO').val(),
        crm_data['SP_OUT_DATE'] = $('#SP_OUT_DATE').val(),
        crm_data['SP_IN_NO'] = $('#SP_IN_NO ').val(),   
        crm_data['SP_IN_DATE'] = $('#SP_IN_DATE').val(),
        crm_data['UNIT_SEND'] = $('#UNIT_SEND').val(),
        crm_data['UNIT_RECV'] = $('#UNIT_RECV').val(),
        crm_data['PART_LOC'] = $('#PART_LOC').val(),
        crm_data['REMARK'] = $('#REMARK').val(),
        crm_data['STATUS'] = $('#STATUS').val();
        crm_data['REVNR'] = <?= $id_project ?>

        var url = '<?php echo base_url() ?>' + 'projects/crud_crm_add';
        $.ajax({
            url: url,
            method: 'POST',
            dataType: 'text',
            data: crm_data,
            success: function(data){
                var response = $.parseJSON(data);
                if(response.status === 'success'){
                    swal("Good Job!", "Record has been saved!", "success");
                    datagrid.fetchGrid();
                    clear_form();
                    $('#modalCrm').modal('hide');
                }
            }
        })
    }

    function clear_form(){
        $('#SN_QTY').val('');
        $('#POST').val('');
        $('#SP_OUT_NO').val('');
        $('#SP_OUT_DATE').val('');
        $('#SP_IN_NO ').val('');   
        $('#SP_IN_DATE').val('');
        $('#UNIT_SEND').val('');
        $('#UNIT_RECV').val('');
        $('#PART_LOC').val('');
        $('#REMARK').val('');
        $('#STATUS').val('');
        $('#order_number').val(null).trigger('change');
        $('#PART_NUMBER').val('');
        $('#create_crm').attr('disabled', true);
        crm_data = {};
    }

    function mrm_delete(AUFNR){
        $('#loader').show();
        var url = '<?php echo base_url() ?>' + 'Projects/crud_crm_update';
        swal({
                title: "Warning!",
                text: "do you want to delete this record?",
                icon: "warning",
                buttons: true,
                dangerMode: true,
         })
        .then(function(isConfirm){
            if (isConfirm) {
                $.ajax({
                    url: url,
                    method: 'POST',
                    data: { colname: 'IS_ACTIVE', id: AUFNR, newvalue: 0},
                    dataType: 'text',
                    success: function(data){
                        $('#loader').hide();
                        datagrid.fetchGrid();
                    }
                })
            }        
        })  
    }
    
</script>
