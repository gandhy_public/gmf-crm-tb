<?php 
    $this->load->helper('auth_helper');
    $is_customer = is_customer($session["user_group"]);
?>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12 col-lg-12">
            <div class="box">
                <div class="box-header with-border">
                    <h1 class="box-title">List Project</h1>
                    <span class="label label-info pull-right">
                        <strong id="loaded_count" >  </strong> of <strong id="total_projects"> </strong> Projects
                    </span>
                </div>
                <div class="box-body">
                    <div id="toolbar">
                        <input type="text" id="filter" name="filter" placeholder="Type and enter"/>
                        <div class="pull-right form-inline">
                            <?php if($session['user_group_level'] == 1) { ?>
                                <div class="form-group" style="margin-right: 20px">
                                    <label for="email">Location : </label>
                                    <select class="select-status select-filter form-control" id="filterByLocation">
                                        <option value="" selected>All Location</option>
                                        <option value="GAH1">GAH1</option>
                                        <option value="GAH3">GAH3</option>
                                        <option value="GAH4">GAH4</option>
                                    </select>
                                </div>
                            <?php } ?>
                            <div class="form-group">
                                <label for="email">Status : </label>
                                <select class="select-status select-filter form-control" id="filterByStatus" style="margin-left: 10px;">
                                    <option value="">All Status</option>
                                    <option value="CRTD">New</option>
                                    <option value="REL" selected>Release</option>
                                    <option value="CLSD">Close</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <ul id="list-projects" class="list-projects"></ul>
                    <!-- <button id="btn_load_more">Load More</button>` -->
                    <div class="ajax-load">
                        <!-- loading -->
                        <div class="lds-ellipsis"><div></div><div></div><div></div><div></div></div>
                    </div>                
                </div>
            </div>
        </div>
    </div>

    <div id="hiliteModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Edit or Update Hilite</h4>
            </div>
            <div class="modal-body">
                <div>
                    <div class="form-group">
                        <div class="checkbox">
                            <label><input type="checkbox" value="1" id=in_hangar> In Hangar</label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="body_hilite">Hilite : </label>
                        <textarea id="body_hilite" name="editordata" class="form-control"></textarea>
                    </div>  
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" id="save_hilite" class="btn btn-primary">Save</button>
            </div>
        </div>
    </div>
    </div>
</section>
<script>
    var is_customer = '<?= $is_customer ?>' === "1" ? true : false;
    var REVLIST = [];
    var loaded_count = 0;
    var sent = false;
    $(document).ready(function() {

        var status = $("#filterByStatus option:selected").val();
        var location = $("#filterByLocation option:selected").val();
        loadMoreData(1, status, location);

        var page = 1;
        var end = false;


        $(window).scroll(function(){
            if(sent) {            
                if($(window).scrollTop() + $(window).height() >= ($(document).height() - 5)) {
                    page++;
                    var status = $("#filterByStatus option:selected").val();
                    var location = $("#filterByLocation option:selected").val();
                    var filter = $('#filter').val();
                    loadMoreData(page, status, location, filter);
                    sent = false;
                }   
            }         
        });


        $("#filter").keyup(function () {
            var filter = $(this).val();
            if (event.keyCode === 13 || filter === '') {
                $('.ajax-load').html('<div class="lds-ellipsis"><div></div><div></div><div></div><div></div></div>');
                var status = $("#filterByStatus option:selected").val();
                var location = $("#filterByLocation option:selected").val();
                loadMoreData(1, status, location, filter);
            } 
        });

        $("#filterByStatus, #filterByLocation").change(function(){
            page = 1;
            $('.ajax-load').html('<div class="lds-ellipsis"><div></div><div></div><div></div><div></div></div>');
            var status = $("#filterByStatus option:selected").val();
            var location = $("#filterByLocation option:selected").val();
            var filter = $("#filter").val();
            loadMoreData(1, status, location, filter);
        });

        function get_project_status(value) {
            var status = '';
            switch (value.replace(/\s/g, '')) {
                case 'CRTD': status = 'NEW'; break;
                case 'CLSD': status = 'CLOSED'; break;
                case 'REL': status = 'RELEASE'; break;	
            } 
            return status;
        }

        function date_format(date){
            return moment(date, 'YYYYMMDD').format('DD MMM YYYY');
        }

        function loadMoreData(page, status, location, filter = ''){
            $.ajax({
                url: "",
                type: "POST",
                data: {
                    page: page,
                    status: status,
                    location: location,
                    filter: filter
                },
                beforeSend: function() {
                    if(page == 1) $('.list-projects').html('');
                    $('.ajax-load').show();
                },
                success: function(data) {
                    sent = true;
                    var display_btn = '<?= $is_customer ?>' === "1" ? "none" : "block";
                    var response = $.parseJSON(data);
                    if (typeof response.data.result !== 'undefined' && response.data.result.length > 0) {
                        var show = '';
                        REVLIST = [];
                        $.each(response.data.result, function(key, item) {
                            REVLIST.push(item.REVNR);
                            var btn_view_value = is_customer ? '<i class="fa fa-eye" aria-hidden="true"></i>' : item.REVNR; 
                            var url = '<?php echo base_url() ?>' + 'projects/dashboard/' + item.REVNR;
                            var url_board = '<?php echo base_url() ?>' + 'Board/index/' + item.REVNR;
                            show = '<li class="project-item">' + 
                                '<div class="project-headline">' +
                                    '<h5 class="project-name">' + item.REVTX + '</h5>' +
                                    '<a href="'+ url +'" class="project-number">'+ btn_view_value +'</a>' +
                                    '<a href="'+ url_board +'" class="project-number" style="display: ' + display_btn + '"><i class="fa fa-bar-chart" aria-hidden="true"  target="_blank" data-toggle="tooltip" title="Project Control Board"></i></a>' +
                                    '<a onclick="revh_edit(' + item.REVNR + ')" class="project-number" data-toggle="tooltip" title="Edit" data-revnr="' + item.REVNR + '" style="display: ' + display_btn + '"><i class="fa fa-pencil" aria-hidden="true"></i></a>' +
                                    '<a onclick="update_bw_by_revision(&quot;' + item.REVNR + '&quot;)" class="project-number" data-toggle="tooltip" title="Syncronize Project" data-revnr="' + item.REVNR + '" style="display: ' + display_btn + '"><i class="fa fa-refresh" aria-hidden="true"></i></a>' +
                                    '<span class="project-status '+ get_project_status(item.TXT04).toLowerCase() +'">' + get_project_status(item.TXT04) +'</span>' +
                                '</div>'+
                                '<div class="project-detail row">' +
                                    '<div class="col-md-4">' +
                                        '<p class="plant-code">'+ item.TPLNR +'</p>' +
                                        '<p class="customer-name" style="color:#605ca8"> <i class="fa fa-building" aria-hidden="true"></i> '+ item.COMPANY_NAME +'</p>' +
                                    '</div>' +
                                    '<div class="col-md-3">' +
                                        '<div class="info-area row">' +
                                            '<div class="col-md-6 left">start date :</div>' +
                                            '<div class="col-md-6 right">'+ date_format(item.REVBD) +'</div>' +
                                        '</div>' +
                                        '<div class="info-area row">' +
                                            '<div class="col-md-6 left">finish date :</div>' +
                                            '<div class="col-md-6 right">'+ date_format(item.REVED) +'</div>' +
                                        '</div>' +
                                    '</div>' +
                                    '<div class="col-md-2">' +
                                        '<div class="info-area row">' +
                                            '<div class="col-md-6 left">location :</div>' +
                                            '<div class="col-md-6 right">'+ item.VAWRK +'</div>' +
                                        '</div>' +
                                        '<div class="info-area row">' +
                                            '<div class="col-md-6 left">type :</div>' +
                                            '<div class="col-md-6 right">'+ item.REVTY +'</div>' +
                                        '</div>' +
                                    '</div>' +
                                    '<div class="col-md-3">' +
                                        '<div class="info-area row last">' +
                                            '<div class="col-md-6 left" id="jc_lbl_'+ item.REVNR +'">JC (%) :</div>' +
                                            '<div class="col-md-6 right">' +
                                                '<div class="progress"><span class="progress-bar" style="width: 0; background: #13CE66" id="jc_'+item.REVNR+'"></span></div>' +
                                            '</div>' +
                                        '</div>' +
                                        '<div class="info-area row last">' +
                                            '<div class="col-md-6 left" id="mdr_lbl_'+ item.REVNR +'">MDR (%) :</div>' +
                                            '<div class="col-md-6 right">' +
                                                '<div class="progress"><span class="progress-bar" style="width: 0; background: #FFC82C" id="mdr_'+ item.REVNR+'"></span></div>' +
                                            '</div>' +
                                        '</div>' +
                                    '</div>' +
                                '</div>' +
                            '</li>';
                            $('.ajax-load').hide();
                            $(".list-projects").append(show);
                        });
   
                        $('#total_projects').html(response.data.total_count);   

                        $.when.apply($, REVLIST).done(function(){
                            // getOrderProgress();
                        })

                         $('[data-toggle="tooltip"]').tooltip(); 

                    } else {
                        $('.ajax-load').html("<center>No data available in List</center>");
                        return this.end = true;
                    }

                    $('#loaded_count').html($('.project-item').length);
                },
                error: function() {
                    $('.ajax-load').html("<center></center>");
                }
            })
            .done(function(){
                getOrderProgress();
            })
        }
    });

    function getOrderProgress(){
        var url = '<?php echo base_url('Projects/order_progress_count') ?>';
        var data = { REVLIST: JSON.stringify(REVLIST) };

        $.ajax({
            url:url,
            method: 'POST',
            dataType: 'text',
            data: data,
            success: function(data){
                var response = $.parseJSON(data);
                if (response.status === 'success') {
                    appendOrderProgress(response.body);
                }
            }
        });
    }

    function appendOrderProgress(data) {
        $.each(data, function(key, val){
            $('#jc_'+val.REVNR).css("width", val.JC_PROGRESS + "%");
            $('#mdr_'+val.REVNR).css("width", val.MDR_PROGRESS + "%");
            $('#jc_lbl_'+val.REVNR).text('JC ( '+ val.JC_PROGRESS  +'% OF ' + val.JC_TOTAL +'  ) : ');
            $('#mdr_lbl_'+val.REVNR).text('MDR ( '+ val.MDR_PROGRESS  +'% OF ' + val.MDR_TOTAL + ') : ');
        });
    }


    function is_hidden() {
        return is_customer == 1 ? "display: none;" : "display: block"; 
    }   


    var PROJECT_ID = null;
    function revh_edit(revnr) {
        get_hilite('000'+revnr);
        PROJECT_ID = '000'+revnr;
    }

    function get_hilite(revnr){
        var url = '<?= base_url("Projects/get_hilite") ?>';
        $.ajax({
            url: url,
            method: 'POST',
            data: { REVNR: revnr },
            dataType: 'text',
            beforeSend: function(){
                $('.loader').show();
            },
            success: function(data){
                $('.loader').hide();
                var response = $.parseJSON(data);
                if(response.status === 'success') {
                    // $('#body_hilite').summernote('code', response.body.HILITE);
                    $('#body_hilite').val();
                    if(response.body.IN_HANGAR === "1") {
                        $('#in_hangar').prop('checked', true);
                    } else {
                        $('#in_hangar').prop('checked', false);
                    }
                    $('#hiliteModal').modal('show');
                } else {
                    // $('#body_hilite').summernote('reset');
                    // $('#body_hilite').summernote('insertParagraph');
                    $('#body_hilite').val('');
                    $('#in_hangar').prop('checked', false);
                    $('#hiliteModal').modal('show');
                }
            }

        })
    }

    $('#save_hilite').click(function(e){
        e.preventDefault();
        var in_hangar = $('#in_hangar').prop('checked') ? 1 : 0;
        var data = {
            REVNR: PROJECT_ID,
            HILITE: $('#body_hilite').val(),
            IN_HANGAR: in_hangar
        }
        var hilite = $('#body_hilite').val();
        var url = '<?= base_url("Projects/save_hilite") ?>';
        $.ajax({
            url: url,
            method: 'POST',
            data: data,
            dataType: 'text',
            beforeSend: function(){
                $('.loader').show();
            },
            success: function(data){
                $('.loader').hide();
                $('#hiliteModal').modal('hide');
                var response = $.parseJSON(data);
                if(response.status === 'success') {
                    swal("Good Job!", "Record has been saved!", "success");
                } else {
                    swal("Opss!", "Order Number or Part Number is Required!", "error");
                }
            }

        })

    })

    window.title = "CRM TB | Project List";
      
</script>
