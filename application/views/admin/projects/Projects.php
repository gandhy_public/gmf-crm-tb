<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require_once('assets/editablegrid/EditableGrid.php');
require_once APPPATH . 'libraries/spout/src/Spout/Autoloader/autoload.php';

use Illuminate\Database\Eloquent\Model as ELOQ;
use Box\Spout\Reader\ReaderFactory;
use Box\Spout\Common\Type;
use Box\Spout\Writer\WriterFactory;

class Projects extends CI_Controller {

    private $is_customer;

    public function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->library('menu_lib');
        $this->load->model('Project_model_ci', '', TRUE);
        $this->load->model('Project_model');
        $this->load->model('Revh_model');
        $this->load->model('Management_model');
        $this->load->model('VProject_model');
        $this->load->model('Customer_model');
        $this->load->model('Dashboard_model', '', TRUE);
        $this->load->model('Jobcard_model');
        $this->load->model('Order_model');
        $this->load->model('OrderH_model');
        $this->load->model('Proc_model');
        $this->load->model('Jobcard_model_view');
        $this->load->model('Mdr_model');
        $this->load->model('Mdr_model_view');
        $this->load->model('Mrm_model');
        $this->load->model('Mrm_model_view');
        $this->load->model('Prm_model');
        $this->load->model('Prm_model_view');
        $this->load->model('Crm_model');
        $this->load->model('Ticket_model');
        $this->load->model('Csr_model');
        $this->load->model('Csr_model_view');
        $this->load->model('Daily_menu_model_view');
        $this->load->model('Daily_menu_model_ci');
        $this->load->model('Daily_day_model');
        $this->load->model('Daily_day_model_view');
        $this->load->model('Daily_area_model');
        $this->load->model('Daily_details_model');
        $this->load->model('Maint_phase_model');
        $this->load->model('Maint_milestone_model');
        $this->load->model('Daily_day_progress_model');
        $this->load->model('Users');
        $this->load->model('User_group_model');
        $this->load->model('Phase_model');
        $this->load->model('Phase_model_view');
        $this->load->model('Material_model');
        $this->load->helper('auth_helper');
        $this->load->helper('text_formatter');

        // $this->output->enable_profiler(TRUE);
        // Cek User Seesion
        $this->data["session"] = $this->session->userdata('logged_in');
        
        $this->is_customer = is_customer($this->data["session"]["user_group"]);

        has_session($this->data["session"]);
    }

    public function callback_project($value, $row) {

        return '<a href="' . base_url() . 'admin/projects/crud_jobcard/' . $row->Id . '">' . $value . '</a>';
    }

    public function callback_jobcard_progress($value, $row) {
        $close = $this->Jobcard_model->where('id_project', '=', $row->Id)
                ->where('status', 'like', 'Close')
                ->count();
        $all = $this->Jobcard_model->where('id_project', '=', $row->Id)->count();
        $persen = round(($close / ($all ? $all : 1)) * 10000) / 100;
        return '<a href="' . base_url() . 'admin/projects/crud_jobcard/' . $row->Id . '">' . $persen . '%</a>';
    }

    public function callback_mdr_progress($value, $row) {
        $close = $this->Mdr_model->where('id_project', '=', $row->Id)
                ->where('status', 'like', 'Close')
                ->count();
        $all = $this->Mdr_model->where('id_project', '=', $row->Id)->count();
        $persen = round(($close / ($all ? $all : 1)) * 10000) / 100;
        return '<a href="' . base_url() . 'admin/projects/crud_mdr/' . $row->Id . '">' . $persen . '%</a>';
    }

    public function check_projectdates($finish_date, $start_date) {
        $parts = explode('/', $this->input->post('start_date'));
        $start_date = join('-', $parts);
        $start_date = strtotime($start_date);

        $parts2 = explode('/', $this->input->post('finish_date'));
        $finish_date = join('-', $parts2);
        $finish_date = strtotime($finish_date);

        if ($finish_date >= $start_date) {

            return true;
        } else {
            $this->form_validation->set_message('check_projectdates', "finish date should be greater than start date");
            return false;
        }
    }

    public function crud_project_customer_name_add_field_callback() {
        return $this->Customer_model->where('ID_CUSTOMER', $this->uri->segment(4))->first()->COMPANY_NAME . '<input type="hidden" name="customer_name" value="' . $this->uri->segment(4) . '" />';
    }

    public function crud_customer() {
        $data['is_grocery'] = true;
        $crud = new grocery_CRUD();
        $crud->set_table('customer');
        $crud->set_subject('Customer');
        $crud->unset_fields('ID_CUSTOMER');
        $crud->unset_columns('ID_CUSTOMER');
        $crud->unset_add();
        $crud->unset_read();
        $crud->unset_delete();
        $crud->unset_edit();
        $crud->add_action('Project', base_url() . 'assets/dist/img/icons_dashboard.png', 'admin/projects/index');
        $output = $crud->render();

        $data["session"] = $this->session->userdata('logged_in');
        $data['content'] = 'admin/projects/crud_customer';

        $this->load->view('template', array_merge($data, (array) $output));
    }

    public function encrypt_password($post_array) {
        $this->load->helper('security');
        $post_array['PASSWORD'] = do_hash($post_array['PASSWORD'], 'md5');
        $post_array['STATUS'] = 1;
        return $post_array;
    }

    public function crud_daily_day_list() {
        try {
            if ($this->uri->segment(4)) {
                $this->session->set_userdata('REVNR', $this->uri->segment(4));
            }
            $data['id_project'] = $this->session->userdata('REVNR');
            $active_prj = $this->Project_model_ci->getProjectById($data['id_project']);

            if ($active_prj) {
                $data['project_name'] = $active_prj[0]->REVTX;
            }
        } catch (Exception $e) {
            error_log($e);
        }

        $data["session"] = $this->session->userdata('logged_in');
        $data['content'] = 'admin/projects/crud_daily_day_lists';

        $this->load->view('template', $data);

        return true;
    }

    public function order_progress_count() {
        $response["status"] = NULL;
        $response["body"] = NULL;

        try {
            $data = array();
            $REVLIST = json_decode($_POST['REVLIST']);
            $order = $this->Proc_model->get_order_count($REVLIST);
            if ($order) {
                foreach ($order as $value) {
                    $jc_percentage = get_percentage($value->JC_CLOSED, $value->JC_TOTAL);
                    $mdr_percentage = get_percentage($value->MDR_CLOSED, $value->MDR_TOTAL);

                    array_push($data, array(
                        "REVNR" => $value->REVNR,
                        "JC_PROGRESS" => $jc_percentage,
                        "MDR_PROGRESS" => $mdr_percentage,
                        "JC_TOTAL" => $value->JC_TOTAL,
                        "MDR_TOTAL" => $value->MDR_TOTAL
                    ));
                }
                $response["status"] = 'success';
                $response["body"] = $data;
            } else {
                $response["status"] = 'error';
                $response["body"] = NULL;
            }

        } catch (Exception $e) {
            $response["status"] = 'failed';
            $response["body"] = NULL;
        }

        header('Content-Type: application/json');
        echo json_encode($response);
    }

    public function order_mhrs_count() {
        $response["status"] = NULL;
        $response["body"] = NULL;

        try {
            $data = array();
            $REVLIST = json_decode($_POST['REVLIST']);
            $order = $this->Project_model->get_mhrs($REVLIST);

            if ($order) {

                $sum['actual'] = array();
                $sum['plan'] = array();
                foreach ($order as $value) {
                    if (!array_key_exists($value['REVNR'], $sum['actual'])) {
                        settype($sum['actual'][$value['REVNR']], "float");
                        $sum['actual'][$value['REVNR']] = 0.0;
                    }
                    $sum['actual'][$value['REVNR']] += floatval($value->ACTUAL);


                    if (!array_key_exists($value['REVNR'], $sum['plan'])) {
                        settype($sum['plan'][$value['REVNR']], "float");
                        $sum['plan'][$value['REVNR']] = 0.0;
                    }
                    $sum['plan'][$value['REVNR']] += floatval($value->PLN);
                }

                foreach ($sum['actual'] as $key => $value) {
                    array_push($data, array(
                        'REVNR' => $key,
                        'ACTUAL' => $value,
                        'PLAN' => $sum['plan'][$key],
                        'CONSUMED' => get_percentage($value, $sum['plan'][$key])
                    ));
                }

                $response["status"] = 'success';
                $response["body"] = $data;
            } else {
                $response["status"] = 'error';
                $response["body"] = NULL;
            }
        } catch (Exception $e) {
            echo $e;
            $response["status"] = 'failed';
            $response["body"] = $e;
        }

        header('Content-Type: application/json');
        echo json_encode($response);
    }

    public function crud_daily_day_list_load() {
        $response["status"] = NULL;
        $response["body"] = NULL;

        $project_id = $this->session->userdata('REVNR');
        $report_list = $this->Daily_day_model;

        try {

            if ($project_id != '') {
                $report_list = $report_list->where('REVNR', $project_id);
            }

            $report_list = $report_list->select(
                            'TB_DAILY_DAY.ROW_NUM', 
                            'TB_DAILY_DAY.REPORT_NAME', 
                            'TB_USER.NAME', 
                            'TB_DAILY_DAY.CREATED_BY', 
                            'TB_DAILY_DAY.CREATED_AT', 
                            'TB_DAILY_DAY.STATUS', 
                            'TB_DAILY_DAY.ID'
                    )
                    ->leftJoin('TB_USER', function($leftJoin) {
                        $leftJoin->on('TB_DAILY_DAY.UPDATED_BY', '=', 'TB_USER.ID_USER');
                    })
                    ->orderBy('CREATED_AT', 'DESC')
                    ->get();

            if ($report_list) {
                if(count($report_list) > 0) { 
                    $last_status = $report_list[0]['STATUS'];
                    $response["body"]["last_status"] = $last_status;
                }
                $response["status"] = 'success';
                $response["body"]["report_list"] = $report_list;
            } else {
                $response["status"] = 'error';
                $response["body"] = NULL;
            }
        } catch (Exception $e) {
            $response["status"] = 'FAILED';
            $response["body"] = $e;
        }
        header('Content-Type: application/json');
        echo json_encode($response);
    }

    public function crud_daily_report_copy() {
        date_default_timezone_set('Asia/Jakarta');
        $response["status"] = NULL;
        $response["body"] = NULL;

        $project_id = $this->session->userdata('REVNR');
        $userdata = $this->session->userdata('logged_in');

        $report = $this->Daily_day_model;
        $new_report = $this->Daily_day_model;
        $technical_details = $this->Daily_details_model;
        


        try {
            // $report = $report->where('ID', $_POST['ID'])->first();
            $check_duplicate = $this->Daily_day_model->select("CREATED_AT")
                                    ->where('REVNR', $this->input->post('REVNR'))
                                    ->whereDate('CREATED_AT', 'like', date('Y-m-d'))
                                    ->count();

            if ($check_duplicate > 0) {
                $response["status"] = 'error';
                $response["body"] = 'Report can not be duplicate!'  ;
            } else {
                $report = $report->where('REVNR', $this->input->post('REVNR'))->orderBy('CREATED_AT', 'DESC')->first();
                if ($report) {
                    // Copy / Select Header (Data Report)    
                    $data_report = $report->attributesToArray();
                    $data_report = array_except($data_report, ['ID', 'CREATED_AT', 'CUSTOMER', 'REPORT_NAME', 'ROW_NUM', 'CREATED_BY', 'UPDATED_AT', 'UPDATED_BY', 'STATUS']);
                    $data_report['REPORT_NAME'] = $_POST['REPORT_NAME'];
                    $data_report['ID_USER'] = $userdata['id_user'];
                    $data_report['CREATED_AT'] = date("Y/m/d h:i:sa");
                    $data_report['CREATED_BY'] = $userdata['id_user'];
                    $data_report['UPDATED_AT'] = date("Y/m/d h:i:sa");
                    $data_report['UPDATED_BY'] = $userdata['id_user'];
                    $data_report['UPDATED_BY'] = $userdata['id_user'];
                    $data_report['STATUS'] = 'OPEN';
                    $new_report = $new_report->create($data_report);
    
                    if ($new_report) {
                        $new_report = $new_report->where('ID_USER', $userdata['id_user'])
                                ->where('REVNR', $project_id)
                                ->orderBy('CREATED_AT', 'DESC')
                                ->first();
    
                        $response["status"] = 'success';
                        $response["body"] = $new_report;
    
    
                        $this->crud_technical_details_copy($new_report->ID, $report->ID);
                        $this->project_phase_copy($new_report->ID, $report->ID);
                        $this->daily_day_progress_copy($new_report->ID, $report->ID, $new_report->CREATED_AT);
                    }
                } else {
                    $response["status"] = 'error';
                    $response["body"] = 'Something Wrong!';
                }
            }
        } catch (Exception $e) {
            $response["status"] = 'failed';
            $response["body"] = $e;
        }
        header('Content-Type: application/json');
        echo json_encode($response);
    }

    public function crud_daily_report_add() {
        $response["status"] = NULL;
        $response["body"] = NULL;

        $project_id = $this->session->userdata('REVNR');
        $userdata = $this->session->userdata('logged_in');
        date_default_timezone_set('Asia/Jakarta');

        $mileston = $this->Maint_milestone_model;
        $phase = $this->Maint_phase_model;

        try {
            $report = $this->Daily_day_model;
            $report->REVNR = $project_id;
            $report->ID_USER = $userdata['id_user'];
            $report->REPORT_NAME = $_POST['REPORT_NAME'];
            $report->CREATED_AT = date("Y/m/d h:i:sa");
            $report->CREATED_BY = $userdata['id_user'];
            $report->UPDATED_AT = date("Y/m/d h:i:sa");
            $report->UPDATED_BY = $userdata['id_user'];
            $report->STATUS = 'OPEN';
            $report->SOA_OPEN = 0;
            $report->SOA_CANCEL = 0;
            $report->SOA_APPROVED = 0;
            $report->WEA_OPEN = 0;
            $report->WEA_CANCEL = 0;
            $report->WEA_APPROVED = 0;
            $report->save();

            if ($report) {
                $report = $report->where('ID_USER', $userdata['id_user'])
                        ->where('REVNR', $project_id)
                        ->orderBy('CREATED_AT', 'DESC')
                        ->first();
                
                $response["status"] = 'success';
                $response["body"] = $report;
            } else {
                $response["status"] = 'error';
                $response["body"] = NULL;
            }
        } catch (Exception $e) {
            $response["status"] = 'failed';
            $response["body"] = $e;
        }
        header('Content-Type: application/json');
        echo json_encode($response);
    }

    public function crud_daily_report_close() {
        $response["status"] = NULL;
        $response["body"] = NULL;

        $data = array(
            'PRELIMINARY' => $_POST['PRELIMINARY'],
            'INSPECTION' => $_POST['INSPECTION'],
            'INSTALLATION' => $_POST['INSTALLATION'],
            'OPENING' => $_POST['OPENING'],
            'OPC' => $_POST['OPC'],
            'SERVICING' => $_POST['SERVICING'],
            'JC_OPEN' => $_POST['JC_OPEN'],
            'JC_PROGRESS' => $_POST['JC_PROGRESS'],
            'JC_CLOSED' => $_POST['JC_CLOSED'],
            'JC_TOTAL' => $_POST['JC_TOTAL'],
            'PERC_JC_OPEN' => $_POST['PERC_JC_OPEN'],
            'PERC_JC_PROGRESS' => $_POST['PERC_JC_PROGRESS'],
            'PERC_JC_CLOSED' => $_POST['PERC_JC_CLOSED'],
            'MDR_OPEN' => $_POST['MDR_OPEN'],
            'MDR_PROGRESS' => $_POST['MDR_PROGRESS'],
            'MDR_CLOSED' => $_POST['MDR_CLOSED'],
            'MDR_TOTAL' => $_POST['MDR_TOTAL'],
            'PERC_MDR_OPEN' => $_POST['PERC_MDR_OPEN'],
            'PERC_MDR_PROGRESS' => $_POST['PERC_MDR_PROGRESS'],
            'PERC_MDR_CLOSED' => $_POST['PERC_MDR_CLOSED'],
            'STATUS' => 'CLOSE'
        );

        try {
            $report = $this->Daily_day_model;
            $report->where('ID', $_POST['ID'])
                    ->where('REVNR', $_POST['REVNR'])
                    ->update($data);

            if ($report) {
                $response["status"] = 'success';
                $response["body"] = NULL;
            }
        } catch (Exception $e) {
            $response["status"] = 'failed';
            $response["body"] = $e;
        }
        header('Content-Type: application/json');
        echo json_encode($response);
    }

    public function crud_daily_day() {
        $data['content'] = 'admin/projects/crud_daily_day_lists';
        try {
            if ($this->uri->segment(3)) {
                $this->session->set_userdata('REVNR', $this->uri->segment(3));
            }

            $data['id_project'] = $this->session->userdata('REVNR');
            $active_prj = $this->Project_model->get_project_detail($this->session->userdata('REVNR'));

            if ($active_prj) {
                $data['project_name'] = $active_prj->REVTX;
            }
        } catch (Exception $e) {
            error_log($e);
        }

        try {
            if ($this->uri->segment(4)) {
                $this->session->set_userdata('report_id', $this->uri->segment(4));
                $data['content'] = 'admin/projects/crud_daily_day';
            } else {
                $this->session->unset_userdata('report_id');
                $data['content'] = 'admin/projects/crud_daily_day_lists';
            }
            $data['report_id'] = $this->session->userdata('report_id');
        } catch (Exception $e) {
            error_log($e);
        }

        $daily_area = $this->Daily_area_model->get();
        $jc_status = $this->Dashboard_model->getJobcardByStatus($data['id_project']);
        $mdr_status = $this->Dashboard_model->getMdrByStatus($data['id_project']);
        // $phase_presentace = $this->Proc_model->get_phase_progress($data['id_project']); // $this->db->query("EXEC TB_P_PAHSE_PROGRESS @REVNR = " . $data['id_project'])->result();

        $data['project_name'] = $active_prj->REVTX;
        $data['project_data'] = $active_prj;
        $data['daily_area'] = $daily_area;
        $data['jc_status'] = $jc_status;
        $data['mdr_status'] = $mdr_status;
        // $data['phase_presentace'] = $phase_presentace;

        $data["session"] = $this->session->userdata('logged_in');
        $this->load->view('template', $data);

        return true;
    }

    public function get_project_phase(){
        $response["status"] = NULL;
        $response["body"] = NULL;

        $phase = $this->Maint_phase_model;

        try {
            $REVNR = $this->input->post('REVNR');
            $ID_REPORT = $this->input->post('ID');
            $STATUS = $this->input->post('STATUS');

            if ($STATUS == 'OPEN') {
                $phase_presentace = $this->Proc_model->get_phase_progress($REVNR);
                
                foreach($phase_presentace as $value) {
                    if($value->TOTAL > 0){
                        $phase = $phase->updateOrCreate(
                                ['DAILY_DAY_ID' =>  $ID_REPORT, 'MAINT_NAME' => $value->PHASE],
                                [
                                    'MAINT_NAME' => $value->PHASE,
                                    'PROGRESS' => get_percentage($value->CLOSED, $value->TOTAL)
                                ]
                            );
                    }
                }
            }

            $phase = $phase->where("DAILY_DAY_ID", $ID_REPORT)->orderBy("ID")->get();
            if($phase) {
                $response["status"] = 'success';
                $response["body"] = $phase;
            } else {
                $response["status"] = 'error';
                $response["body"] = NULL;
            }

        } catch (Exception $e){

        }
        header('Content-Type: application/json');
        echo json_encode($response);

    }

    public function project_phase_copy($daily_day_id, $old_daily_report) {
        try {
            $phase = $this->Maint_phase_model;
            $phase = $phase->where('DAILY_DAY_ID', $old_daily_report)
                            ->orderBy('ID', 'DESC')->get();
            if ($phase) {
                foreach ($phase as $value) {
                    $new_phase = new $this->Maint_phase_model;
                    $new_phase->MAINT_NAME = $value->MAINT_NAME;
                    $new_phase->PROGRESS = $value->PROGRESS; 
                    $new_phase->REMARKS = $value->REMARKS; 
                    $new_phase->DAILY_DAY_ID = $daily_day_id;
                    $new_phase->save();
                }
            } else {
                
            }
        } catch (Exception $e) {
            echo $e;
        }
    }

    public function update_project_phase(){
        $response["status"] = NULL;
        $response["body"] = NULL;

        $phase = $this->Maint_phase_model;

        try {
            $ID = $this->input->post('pk');
            $colname = $this->input->post('name');
            $value = $this->input->post('value');

            $phase = $phase->where("ID", $ID)
                        ->update([$colname => $value]);

            if($phase) {
                $response["status"] = 'success';
                $response["body"] = $phase;
            } else {
                $response["status"] = 'error';
                $response["body"] = NULL;
            }

        } catch (Exception $e){

        }
        header('Content-Type: application/json');
        echo json_encode($response);

    }

    public function get_daily_day_progress(){
        $response["status"] = NULL;
        $response["body"] = NULL;

        $progress = $this->Daily_day_progress_model;

        try {
            $REVNR = $this->input->post('REVNR');
            $ID_REPORT = $this->input->post('ID');
            $STATUS = $this->input->post('STATUS');
            $DATE = $this->input->post('DATE');
            

            $jc_status = $this->Dashboard_model->getJobcardByStatus($REVNR);
            $mdr_status = $this->Dashboard_model->getMdrByStatus($REVNR);

            if ($STATUS == 'OPEN') {  
                if($jc_status){
                    foreach($jc_status as $value) {
                        $progress = $progress->updateOrCreate(
                                [
                                    'DAILY_DAY_ID' =>  $ID_REPORT, 
                                    'DAILY_DAY_DATE' => $DATE,
                                    'ITEM' => 'JOBCARD',
                                ],
                                [
                                    'OPEN' => $value->JC_OPEN,
                                    'CLOSE' => $value->JC_CLOSED,
                                    'PROGRESS' => $value->JC_PROGRESS,
                                    'TOTAL' => $value->JC_TOTAL,
                                    'TYPE' => 'ORDER',
                                    'ITEM' => 'JOBCARD',
                                    'DAILY_DAY_DATE' => date('Y-m-d', strtotime($DATE))
                                ]
                            );
                        
                    }
                }else{
                    $progress = $progress->updateOrCreate(
                        [
                            'DAILY_DAY_ID' =>  $ID_REPORT, 
                            'DAILY_DAY_DATE' => $DATE,
                            'ITEM' => 'JOBCARD',
                        ],
                        [
                            'OPEN' => 0,
                            'CLOSE' => 0,
                            'PROGRESS' => 0,
                            'TOTAL' => 0,
                            'TYPE' => 'ORDER',
                            'ITEM' => 'JOBCARD',
                            'DAILY_DAY_DATE' => date('Y-m-d', strtotime($DATE))
                        ]
                    );
                }
                
                if($mdr_status){
                    foreach($mdr_status as $value) {
                        $progress = $progress->updateOrCreate(
                                [
                                    'DAILY_DAY_ID' =>  $ID_REPORT, 
                                    'DAILY_DAY_DATE' => $DATE,
                                    'ITEM' => 'NRC/MDR',
                                ],
                                [
                                    'OPEN' => $value->MDR_OPEN,
                                    'CLOSE' => $value->MDR_CLOSED,
                                    'PROGRESS' => $value->MDR_PROGRESS,
                                    'TOTAL' => $value->MDR_TOTAL,
                                    'TYPE' => 'ORDER',
                                    'ITEM' => 'NRC/MDR',
                                    'DAILY_DAY_DATE' => date('Y-m-d', strtotime($DATE))
                                ]
                            );
                        
                    }
                }else{
                    $progress = $progress->updateOrCreate(
                        [
                            'DAILY_DAY_ID' =>  $ID_REPORT, 
                            'DAILY_DAY_DATE' => $DATE,
                            'ITEM' => 'NRC/MDR',
                        ],
                        [
                            'OPEN' => 0,
                            'CLOSE' => 0,
                            'PROGRESS' => 0,
                            'TOTAL' => 0,
                            'TYPE' => 'ORDER',
                            'ITEM' => 'NRC/MDR',
                            'DAILY_DAY_DATE' => date('Y-m-d', strtotime($DATE))
                        ]
                    );
                } 
            }
            
            $progress = $progress->where("DAILY_DAY_ID", $ID_REPORT)->orderBy("ID")->get();
            if($progress) {
                $response["status"] = 'success';
                $response["body"] = $progress;
            } else {
                $response["status"] = 'error';
                $response["body"] = NULL;
            }

        } catch (Exception $e){
            echo $e;
        }
        header('Content-Type: application/json');
        echo json_encode($response);        
    }

    public function daily_day_progress_copy($daily_day_id, $old_daily_report, $date) {
        try {
            $daily_date = date_create($date);
            $progress = $this->Daily_day_progress_model;
            $progress = $progress->where('DAILY_DAY_ID', $old_daily_report)
                            ->orderBy('ID', 'DESC')->get();
            if ($progress) {
                foreach ($progress as $value) {
                    $new_progress = new $this->Daily_day_progress_model;
                    $new_progress->ITEM = $value->ITEM;
                    $new_progress->OPEN = $value->OPEN;
                    $new_progress->PROGRESS = $value->PROGRESS;
                    $new_progress->CLOSE = $value->CLOSE;
                    $new_progress->TOTAL = $value->TOTAL;
                    $new_progress->TYPE = $value->TYPE;
                    $new_progress->DAILY_DAY_DATE = date_format($daily_date , "Y-m-d");
                    $new_progress->DAILY_DAY_ID = $daily_day_id;


                    $new_progress->save();
                }
            } else {
                
            }
        } catch (Exception $e) {
            echo $e;
        }
    }

    public function daily_day_after_update($post_array, $primary_key) {
        $daily_day_update = array(
            "update_date_daily" => date('Y-m-d H:i:s'),
            "updateBy_daily" => $this->session->userdata('logged_in')['id_user']
        );

        $this->db->update('daily_day', $daily_day_update, array('id' => $primary_key));

        return true;
    }

    function crud_daily_day_load() {
        $response["status"] = NULL;
        $response["body"] = NULL;

        try {

            $project_id = $this->session->userdata('REVNR');
            $report_id = $this->session->userdata('report_id');
            $daily_report = $this->Daily_day_model
                    ->where('REVNR', $project_id)
                    ->where('ID', $report_id)
                    ->first();

            if ($daily_report) {
                $response["status"] = "success";
                $response["body"] = $daily_report;
            } else {
                $response["status"] = "failed";
                $response["body"] = NULL;
            }
        } catch (Exception $e) {
            $response["status"] = "error";
            $response["body"] = $e;
        }

        header('Content-Type: application/json');
        echo json_encode($response);
    }

    public function crud_daily_report_load_by_id() {
        $response["status"] = NULL;
        $response["body"] = NULL;

        try {

            $project_id = $this->session->userdata('REVNR');
            $report_id = $_POST['ID'];
            $daily_report = $this->Daily_day_model
                    ->where('REVNR', $project_id)
                    ->where('ID', $report_id)
                    ->first();

            if ($daily_report) {
                $response["status"] = "success";
                $response["body"] = $daily_report;
            } else {
                $response["status"] = "failed";
                $response["body"] = NULL;
            }
        } catch (Exception $e) {
            $response["status"] = "error";
            $response["body"] = $e;
        }

        header('Content-Type: application/json');
        echo json_encode($response);
    }

    public function crud_daily_report_delete() {
        $response["status"] = NULL;
        $response["body"] = NULL;

        try {

            $project_id = $this->session->userdata('REVNR');
            $report_id = $_POST['ID'];

            $daily_report = $this->Daily_day_model
                    ->where('REVNR', $project_id)
                    ->where('ID', $report_id)
                    ->delete();

            if ($daily_report) {
                $response["status"] = "success";
                $response["body"] = "Record has been deleted successfully";
            } else {
                $response["status"] = "failed";
                $response["body"] = NULL;
            }
        } catch (Exception $e) {
            $response["status"] = "error";
            $response["body"] = $e;
        }

        header('Content-Type: application/json');
        echo json_encode($response);
    }

    public function crud_daily_day_save() {
        if (isset($_POST['name']) && isset($_POST['value']) && isset($_POST['pk'])) {
            try {
                $name = $_POST['name'];
                $value = $_POST['value'];
                $pk = $_POST['pk'];

                $daily_report = $this->Daily_day_model->where('ID', $pk)
                        ->update([$name => $value, 'ID_USER' => $this->session->userdata('logged_in')['id_user']]);

                if ($daily_report) {
                    echo "ok";
                } else {
                    echo "failed";
                }
            } catch (Exception $e) {

                echo $e;
            }
        }
    }

    public function crud_technical_detail() {
        $response["status"] = NULL;
        $response["body"] = NULL;

        if (isset($_POST['REVNR']) && isset($_POST['AREA'])) {
            try {
                $details = $this->Daily_details_model;
                $details->REVNR = $_POST['REVNR'];
                $details->AREA = $_POST['AREA'];
                $details->TASK = $_POST['TASK'];
                $details->FOLLOW_UP = $_POST['FOLLOW_UP'];
                $details->REMARKS = $_POST['REMARKS'];
                $details->LEVEL = $_POST['LEVEL'];
                $details->STATUS = $_POST['STATUS'];
                $details->DAILY_DAY_ID = $_POST['DAILY_DAY_ID'];
                $details->save();

                if ($details) {
                    $response["status"] = 'success';
                    $response["body"] = $details;
                } else {
                    $response["status"] = 'failed';
                    $response["body"] = NULL;
                }
            } catch (Exception $e) {
                $response["status"] = 'error';
                $response["body"] = $e;
            }
        }

        header('Content-Type: application/json');
        echo json_encode($response);
    }

    public function crud_technical_details_load_all() {
        $response["status"] = NULL;
        $response["body"] = NULL;

        try {
            if (isset($_POST['REPORT_ID'])) {
                $area = $this->Daily_area_model->get();
                $details = $this->Daily_details_model;
                $report_id = $_POST['REPORT_ID'];

                $data = array();
                foreach ($area as $key => $value) {
                    $data[$value->ID] = $details
                            ->where('AREA', $value->ID)
                            ->where('DAILY_DAY_ID', $report_id)
                            ->get();
                }
            }

            $response["status"] = 'success';
            $response["body"] = $data;
        } catch (Exception $e) {
            $response["status"] = 'error';
            $response["body"] = $e;
        }

        header('Content-Type: application/json');
        echo json_encode($response);
    }

    public function crud_technical_details_update() {
        $response["status"] = NULL;
        $response["body"] = NULL;

        if (isset($_POST['name']) && isset($_POST['value']) && isset($_POST['pk'])) {
            try {
                $name = $_POST['name'];
                $value = $_POST['value'];
                $pk = $_POST['pk'];

                $daily_details = $this->Daily_details_model->where('ID', $pk);
                $daily_details->update([$name => $value]);

                if ($daily_details) {
                    $response["status"] = 'success';
                    $response["body"] = $daily_details->get();
                } else {
                    $response["status"] = 'failed';
                    $response["body"] = NULL;
                }
            } catch (Exception $e) {

                $response["status"] = 'error';
                $response["body"] = $e;
            }
        }

        header('Content-Type: application/json');
        echo json_encode($response);
    }

    public function crud_technical_details_delete() {
        $response["status"] = NULL;
        $response["body"] = NULL;

        if (isset($_POST['ID'])) {
            try {
                $pk = $_POST['ID'];

                $daily_details = $this->Daily_details_model->where('ID', $pk);
                $daily_details->delete();

                if ($daily_details) {
                    $response["status"] = 'success';
                    $response["body"] = NULL;
                } else {
                    $response["status"] = 'failed';
                    $response["body"] = NULL;
                }
            } catch (Exception $e) {
                $response["status"] = 'error';
                $response["body"] = $e;
            }
        }

        header('Content-Type: application/json');
        echo json_encode($response);
    }    

    public function crud_technical_details_copy($daily_day_id, $old_daily_report) {
        $is_success = false;
        try {
            $technical_details = $this->Daily_details_model;
            $technical_details = $technical_details
                            ->where('REVNR', $this->session->userdata('REVNR'))
                            ->where('DAILY_DAY_ID', $old_daily_report)
                            ->orderBy('CREATED_AT', 'DESC')->get();
            if ($technical_details) {
                foreach ($technical_details as $value) {
                    $new_technical_details = new $this->Daily_details_model;
                    $new_technical_details->REVNR = $value->REVNR;
                    $new_technical_details->AREA = $value->AREA;
                    $new_technical_details->TASK = $value->TASK;
                    $new_technical_details->FOLLOW_UP = $value->FOLLOW_UP;
                    $new_technical_details->REMARKS = $value->REMARKS;
                    $new_technical_details->LEVEL = $value->LEVEL;
                    $new_technical_details->DAILY_DAY_ID = $daily_day_id;
                    $new_technical_details->save();

                    if (!$new_technical_details) {
                        $is_success = false;
                        break;
                    } else {
                        $is_success = true;
                    }
                }
                $is_success = true;
            } else {
                $is_success = false;
            }
        } catch (Exception $e) {
            echo $e;
            $is_success = false;
        }

        return $is_success;
    }

    public function view_vcustomer() {
        $data["session"] = $this->session->userdata('logged_in');
        $data['content'] = 'admin/projects/view_vcustomer';

        $this->load->view('template', $data);
    }

    public function view_vcustomer_load() {
        $data["session"] = $this->session->userdata('logged_in');

        $grid = new EditableGrid();

        $grid->addColumn('KUNNR', 'ID', 'html', NULL, false, 'KUNNR');
        $grid->addColumn('NAME1', 'Name', 'html', NULL, false);

        $active_vcustomer = $this->Customer_model;
        $totalUnfiltered = $active_vcustomer->count();
        $total = $totalUnfiltered;

        $page = 0;
        if (isset($_GET['page']) && is_numeric($_GET['page']))
            $page = (int) $_GET['page'];

        $rowByPage = 20;

        $from = ($page - 1) * $rowByPage;

        if (isset($_GET['filter']) && $_GET['filter'] != "") {
            $filter = $_GET['filter'];
            $active_vcustomer = $active_vcustomer
                    ->where('NAME1', 'like', '%' . $filter . '%')
                    ->orWhere('KUNNR', 'like', '%' . $filter . '%');
            $total = $active_vcustomer->count();
        }

        if (isset($_GET['sort']) && $_GET['sort'] != "" && in_array($_GET['sort'], $grid->getColumnFields())) {
            $$active_vcustomer = $active_vcustomer->orderBy($_GET['sort'], $_GET['asc'] == "0" ? "DESC" : "ASC");
        } else {
            $active_vcustomer = $active_vcustomer->orderBy('NAME1', 'ASC');
        }

        $active_vcustomer = $active_vcustomer->skip($from)->take($rowByPage);
        $grid->setPaginator(ceil($total / $rowByPage), (int) $total, (int) $totalUnfiltered, null);

        $grid->renderJSON($active_vcustomer->get(), false, false, !isset($_GET['data_only']));
    }

    public function view_vproject() {
        $data["session"] = $this->session->userdata('logged_in');

        $response["status"] = '';
        $response["data"] = '';
        try {
            if ($this->uri->segment(2)) {
                $this->session->set_userdata('KUNNR', $this->uri->segment(2));
            } else {
                $this->session->unset_userdata('KUNNR');
            }
            $data['id_customer'] = $this->session->userdata('KUNNR');
            $this->session->unset_userdata('REVNR');
        } catch (Exception $e) {
            error_log($e);
        }        
        
        $menu_tab = $this->Management_model->getMenuTop($data['session']['group_id']);
        $this->session->set_userdata('menu_tab',$menu_tab);
        $data['content'] = 'admin/projects/view_vproject';

        $perPage = 10;
        $page = $this->input->post("page");
        $status = $this->input->post("status");
        $filter = $this->input->post("filter");
        $location = is_admin($data["session"]["user_group"]) ? $data["session"]["work_area_id"] : $this->input->post("location");
        $customer = is_customer($data["session"]["user_group"]) ? $data["session"]["customer_id"] : $data['id_customer'];
        if (!empty($page)) {
            $projects = array();
            if (!empty($filter)) {
                $projects = $this->Project_model->get_projects_with_filter($page, $perPage, $status, $location, $filter, $customer);
            } else {
                $projects = $this->Project_model->get_projects($page, $perPage, $status, $location, $customer);
            }
            $response["status"] = "success";
            $response["data"] = $projects;
            echo json_encode($response);
        } else {
            $this->load->view('template', $data);
        }
    }

    public function view_vproject_load() {

        $data["session"] = $this->session->userdata('logged_in');

        $work_area_id = $this->session->userdata['logged_in']['work_area_id'];
        $customer_id = $this->session->userdata['logged_in']['customer_id'];
        $group_level = $this->session->userdata['logged_in']['group_level'];
        $GROUP_LINK = $group_level != 1 ? 'REVNR_CUSTOMER' : 'REVNR_LINK';

        $grid = new EditableGrid();

        $grid->addColumn('NAME1', 'Customer Name', 'string', NULL, false);
        $grid->addColumn('TPLNR', 'Aircraft Registered', 'string', NULL, false);
        $grid->addColumn($GROUP_LINK, 'Project Code', 'html', NULL, false);
        $grid->addColumn('REVTX', 'Project Name', 'string', NULL, false);
        $grid->addColumn('REVBD', 'Start Date', 'date', NULL, false, 'REVBD');
        $grid->addColumn('REVED', 'Finish Date', 'date', NULL, false, 'REVED');
        $grid->addColumn('VAWRK', 'Location', 'string', NULL, false);
        $grid->addColumn('TXT04', 'Status Project', 'string', NULL, false);
        $grid->addColumn('REVTY', 'Type Project', 'string', NULL, false);
//        $grid->addColumn('JC_PROGRESS', 'JC Progress (%)', 'string', NULL, false);
//        $grid->addColumn('MDR_PROGRESS', 'MDR Progress (%)', 'number', NULL, false);
        $grid->addColumn('action', 'Action', 'html', NULL, false, 'REVNR');

        // $active_job = $this->VProject_model->selectRaw('*,'
//                . "(((SELECT COUNT(*) FROM TB_V_JOBCARD_PROGRESS WHERE TB_V_JOBCARD_PROGRESS.REVNR = TB_V_PROJECT_LIST.REVNR AND TB_V_JOBCARD_PROGRESS.STATUS LIKE 'CLOSE%')/(CASE WHEN (SELECT COUNT(*) FROM TB_V_JOBCARD_PROGRESS WHERE TB_V_JOBCARD_PROGRESS.REVNR = TB_V_PROJECT_LIST.REVNR)=0 THEN 1 ELSE (SELECT COUNT(*) FROM TB_V_JOBCARD_PROGRESS WHERE TB_V_JOBCARD_PROGRESS.REVNR = TB_V_PROJECT_LIST.REVNR) END)*10000)/100) AS JC_PROGRESS,"
//                . "(((SELECT COUNT(*) FROM TB_V_MDR_PROGRESS WHERE TB_V_MDR_PROGRESS.REVNR = TB_V_PROJECT_LIST.REVNR AND TB_V_MDR_PROGRESS.STATUS LIKE 'CARRY OUT%')/(CASE WHEN (SELECT COUNT(*) FROM TB_V_MDR_PROGRESS WHERE TB_V_MDR_PROGRESS.REVNR = TB_V_MDR_PROGRESS.REVNR)=0 THEN 1 ELSE (SELECT COUNT(*) FROM TB_V_MDR_PROGRESS WHERE TB_V_MDR_PROGRESS.REVNR = TB_V_MDR_PROGRESS.REVNR) END)*10000)/100) AS MDR_PROGRESS,"

        $active_job = $this->Project_model->selectRaw('TPLNR, REVNR, REVTX, REVBD, REVED, VAWRK, TXT04, REVTY,'
                . ((($this->db->dbdriver == 'sqlsrv') or ( $this->db->dbdriver == 'mssql')) ? "'<a href=\"" . base_url() . "projects/crud_jobcard/' + REVNR + '\">' + REVNR + '</a>' AS REVNR_LINK," : "CONCAT('<a href=\"" . base_url() . "projects/crud_jobcard/', REVNR, '\">', REVNR, '</a>') AS REVNR_LINK,")
                . ((($this->db->dbdriver == 'sqlsrv') or ( $this->db->dbdriver == 'mssql')) ? "'<a href=\"" . base_url() . "projects/c_jobcard/' + REVNR + '\">' + REVNR + '</a>' AS REVNR_CUSTOMER" : "CONCAT('<a href=\"" . base_url() . "projects/c_jobcard/', REVNR, '\">', REVNR, '</a>') AS REVNR_CUSTOMER")
        );

        $active_job = $active_job
                ->where('TXT04', '!=', 'CLSD')
                ->whereIn('VAWRK', ['GAH1', 'GAH3', 'GAH4']);

        if ($this->session->userdata('KUNNR') != NULL) {
            $active_job = $active_job->where('KUNNR', $this->session->userdata('KUNNR'));
        }

        // if( ! empty($work_area_id) ) {
        //         $active_job = $active_job->where('VAWRK', $work_area_id);
        // }



        $totalUnfiltered = $active_job->count();
        $total = $totalUnfiltered;

        $page = 0;
        if (isset($_GET['page']) && is_numeric($_GET['page']))
            $page = (int) $_GET['page'];

        $rowByPage = 20;

        $from = ($page - 1) * $rowByPage;

        if (isset($_GET['filter']) && $_GET['filter'] != "") {
            $filter = $_GET['filter'];
            $active_job = $active_job
//                    ->where('NAME1', 'like', '%' . $filter . '%')
                    ->where('TPLNR', 'like', '%' . $filter . '%')
                    ->orwhere('REVNR', 'like', '%' . $filter . '%')
                    ->orwhere('REVTX', 'like', '%' . $filter . '%')
                    ->orwhere('REVBD', 'like', '%' . $filter . '%')
                    ->orwhere('REVED', 'like', '%' . $filter . '%')
                    ->orwhere('VAWRK', 'like', '%' . $filter . '%')
//                    ->orwhere('TXT30', 'like', '%' . $filter . '%')
                    ->orwhere('REVTY', 'like', '%' . $filter . '%');
            $total = $active_job->count();
        }

        if (isset($_GET['sort']) && $_GET['sort'] != "") {
            $active_job = $active_job->orderBy($_GET['sort'], $_GET['asc'] == "0" ? "DESC" : "ASC");
        } else {
            $active_job = $active_job->orderBy('REVNR', 'ASC');
        }

        $active_job = $active_job->skip($from)->take($rowByPage);
        $grid->setPaginator(ceil($total / $rowByPage), (int) $total, (int) $totalUnfiltered, null);
        $grid->renderJSON($active_job->get(), false, false, !isset($_GET['data_only']));
    }

    public function find_order_number_data() {
        $response["status"] = NULL;
        $response["body"] = NULL;

        if (isset($_POST['AUFNR'])) {
            try {
                $order_number = $this->Jobcard_model
                        ->where('REVNR', $this->session->userdata('REVNR'))
                        ->where('AUFNR', ($_POST['AUFNR']))
                        ->first();
                if ($order_number) {
                    $response["status"] = 'success';
                    $response["body"] = $order_number;
                }
            } catch (Exception $e) {
                error_log($e);
            }
        }

        header('Content-Type: application/json');
        echo json_encode($response);
    }

    public function find_order_number() {
        $response["status"] = NULL;
        $response["body"] = NULL;

        try {
            $order_number = $this->Jobcard_model
                    ->select("AUFNR", "AUART", "JC_REFF")
                    ->where('REVNR', $_GET['REVNR'])
                    ->where('AUFNR', 'like', ($_GET['q']) . "%")
                    ->distinct('AUFNR')
                    ->get();

            if ($order_number) {
                $response["status"] = 'success';
                $response["body"] = $order_number;
            }
        } catch (Exception $e) {
            error_log($e);
        }

        header('Content-Type: application/json');
        echo json_encode($response);
    }

    public function read_xlsx_file_jobcard() {
        $response["status"] = NULL;
        $response["body"] = NULL;
        try {
            $path = $_FILES['media']['tmp_name'];
            $reader = ReaderFactory::create(Type::XLSX);
            $reader->open($path);
            $i = 0;
            $data["error"] = array();
            $data["success"] = array();

            foreach ($reader->getSheetIterator() as $sheet) {
                foreach ($sheet->getRowIterator() as $row => $value ) {
                    if ($i == 0) {
                        $i++;
                        continue; 
                    }


                    $list = array(
                        'SKILL',
                        'AREA',
                        'PHASE',
                        'DAY',
                        'STATUS',
                        'DATECLOSE',
                        'REMARK',
                        'DOC_SENT_STATUS',
                        'FREETEXT',
                        'DATEPROGRESS',
                        'CABINSTATUS'
                    );
                    foreach ($value as $key2 => $value2) {
                        if ($key2 >= 2 and $key2 <= 12) {
                            $item[$list[$key2 - 2]] = (string) $value2 ?: null;
                        }
                    }


                    $jobcard = $this->Jobcard_model
                            ->where('REVNR',  $value[0])
                            ->where('AUFNR',  $value[1])
                            ->where('AUART', 'GA01')
                            ->update($item);
                    if ($jobcard) {
                        array_push($data["success"], $value[1]);

                        if (!empty($item['AREA'])) {
                            $mdr = $this->Mdr_model
                                ->where('JC_REFF', 'LIKE', '%' . $value[1])
                                ->update(["AREA" => $item['AREA']]);
                        }
                        
                    } else {
                        array_push($data["error"], $value[1]);
                    }

                }
            }

            if (count($data["success"]) > 0) {
                $response["status"] = 'success';
                $response["body"] = $data;
            }

        } catch (Exception $e) {
            echo $e;
            exit;
        }
        header('Content-Type: application/json');
        echo json_encode($response);
    }

    public function crud_jobcard() {
        try {
            if ($this->uri->segment(3)) {
                $this->session->set_userdata('REVNR', $this->uri->segment(3));
            }
            $data['id_project'] = $this->session->userdata('REVNR');
            $active_prj = $this->Project_model_ci->getProjectName($data['id_project']);
            if ($active_prj) {
                $data['project_name'] = $active_prj->REVTX;
            }
        } catch (Exception $e) {
            error_log($e);
        }

        try {
            if ($this->uri->segment(4)) {
                $this->session->set_userdata('jc_id', $this->uri->segment(4));
            } else {
                $this->session->unset_userdata('jc_id');
            }
            $data['jc_id'] = $this->session->userdata('jc_id');
        } catch (Exception $e) {
            error_log($e);
        }
        $this->Proc_model->merge_pmorder($this->session->userdata('REVNR'));
        $data["session"] = $this->session->userdata('logged_in');
        $data['content'] = 'admin/projects/crud_jobcard';
        $this->load->view('template', $data);
    }

    public function crud_jobcard_load() {
        $data["session"] = $this->session->userdata('logged_in');
        $phase = $this->Phase_model->get();
        $phase_arr = array();

        foreach ($phase as $value) {
            $phase_arr[$value->ID] = $value->PHASE;
        }

        $grid = new EditableGrid();

        $grid->addColumn('SEQ_NUM', 'SEQ', 'int', NULL, false);
        // $grid->addColumn('AUFNR_LINK', 'Order', 'html', NULL, false);
        $grid->addColumn('AUFNR', 'Order', 'html', NULL, false, 'AUFNR');
        $grid->addColumn('KTEXT', 'Description', 'string', NULL, false);
        $grid->addColumn('CUST_JC_NUM', 'Cust JC Num', 'string', NULL, false);
        $grid->addColumn('ILART', 'Task', 'string', NULL, false);
        $grid->addColumn('ITVAL', 'ITVAL', 'string');
        $grid->addColumn('MHRS', 'Mhrs Plan', 'string', NULL, false);
        $grid->addColumn('SKILL', 'Skill', 'string', array("A/P" => "A/P", "CBN" => "CBN", "E/A" => "E/A", "STR" => "STR", "WS" => "WS"));
        $grid->addColumn('AREA', 'Area', 'string', array("FUSELAGE" => "FUSELAGE", "COCKPIT" => "COCKPIT", "LH-WING" => "LH-WING", "RH-WING" => "RH-WING", "L/G" => "L/G", "ENG#1" => "ENG#1", "ENG#2" => "ENG#2", "ENG#3" => "ENG#3", "ENG#4" => "ENG#4", "TAIL" => "TAIL", "CABIN" => "CABIN", "FWD CARGO" => "FWD CARGO", "AFT CARGO" => "AFT CARGO", "BULK CARGO" => "BULK CARGO", "MAIN CARGO" => "MAIN CARGO", "LOW CARGO" => "LOW CARGO", "ELECT" => "ELECT", "GENERAL AREA" => "GENERAL AREA"));
        $grid->addColumn('PHASE', 'Phase', 'string', $phase_arr);
        $grid->addColumn('DAY', 'Day', 'string');
        $grid->addColumn('DATECLOSE', 'Date Closed', 'date');
        $grid->addColumn('STATUS', 'Status', 'string', array("OPEN" => "OPEN", "CLOSED" => "CLOSED", "PERFORM BY PROD" => "PERFORM BY PROD", "PERFORM TO SHOP" => "PERFORM TO SHOP", "WAITING MATERIAL" => "WAITING MATERIAL", "WAITING TOOL" => "WAITING TOOL", "PREPARE FOR INSTALL" => "PREPARE FOR INSTALL", "PREPARE FOR TEST" => "PREPARE FOR TEST", "PREPARE FOR RUN UP" => "PREPARE FOR RUN UP", "PREPARE FOR NDT" => "PREPARE FOR NDT", "PART AVAIL" => "PART AVAIL"));
        $grid->addColumn('JC_REFF_MDR_VAL', 'JC Reff MDR', 'string', NULL, false);
        $grid->addColumn('REMARK', 'Remark', 'string', array("N/A (NOT APPLICABLE)" => "N/A (NOT APPLICABLE)", "WITHDRAWN" => "WITHDRAWN", "COVER BY ANOTHER JOBCARD" => "COVER BY ANOTHER JOBCARD", "INTERUPT" => "INTERUPT", "MAP SHORTAGE" => "MAP SHORTAGE", "MAP GADC" => "MAP GADC", "MAP W. CUST. SUPPLY" => "MAP W. CUST. SUPPLY", "PART COMPLETE" => "PART COMPLETE", "PART PARTIAL" => "PART PARTIAL", "ANOTHER REASON" => "ANOTHER REASON"));
        $grid->addColumn('DOC_SENT_STATUS', 'Doc. Sent Status', 'string', array("SENT TO CABIN" => "SENT TO CABIN", "SENT TO CABIN SHOP" => "SENT TO CABIN SHOP", "SENT TO PAINTING" => "SENT TO PAINTING", "SENT TO NDT" => "SENT TO NDT", "SENT TO TBR SHOP" => "SENT TO TBR SHOP", "SENT TO SEALANT" => "SENT TO SEALANT", "SENT TO STR HANGAR" => "SENT TO STR HANGAR", "SENT TO SEAT" => "SENT TO SEAT", "SENT TO WHEEL SHOP" => "SENT TO WHEEL SHOP", "SENT TO TV ENGINE" => "SENT TO TV ENGINE", "SENT TO CLEANING" => "SENT TO CLEANING"));

        $grid->addColumn('FREETEXT', 'Free Text', 'html');
        $grid->addColumn('DATEPROGRESS', 'Date Progress', 'date');
        $grid->addColumn('MATERIAL_STATUS', 'Mat Status', 'string', NULL, false);
        $grid->addColumn('CABINSTATUS', 'Cabin Status', 'string');
        $grid->addColumn('STATUS_SAP', 'Status SAP', 'string', NULL, false);
        $grid->addColumn('action', 'Action', 'html', NULL, false, 'id');


        $active_job = $this->Jobcard_model
                ->selectRaw('*,'
                        . ((($this->db->dbdriver == 'sqlsrv') or ( $this->db->dbdriver == 'mssql')) ? "'<a href=\"" . base_url() . "projects/crud_mdr/' + REVNR + '/' + AUFNR + '/JC\">' + AUFNR + '</a>' AS AUFNR_LINK," : "CONCAT('<a href=\"" . base_url() . "admin/projects/crud_mdr/', REVNR, '/', AUFNR, '/JC\">', AUFNR, '</a>') AS AUFNR_LINK,")
                        . '(select count(*) from TB_M_PMORDER where TB_M_PMORDER.MAUFNR = TB_M_PMORDER.AUFNR) AS JC_REFF_MDR_VAL')
                ->where('REVNR', $this->session->userdata('REVNR'))
                ->where('AUART', 'GA01')
                ->orderBy('AUFNR', 'ASC');


        if ($this->session->userdata('jc_id') != NULL) {
            $active_job = $active_job->where('AUFNR', $this->session->userdata('jc_id'));
        }

        $totalUnfiltered = $active_job->count();
        $total = $totalUnfiltered;

        $page = 0;
        if (isset($_GET['page']) && is_numeric($_GET['page']))
            $page = (int) $_GET['page'];


        $rowByPage = $totalUnfiltered; // 50;

        $from = ($page - 1) * $rowByPage;

        if (isset($_GET['filter']) && $_GET['filter'] != "") {
            $filter = $_GET['filter'];
            $active_job = $active_job
                    ->where('AUFNR', 'like', '%' . $filter . '%')
                    ->orwhere('KTEXT', 'like', '%' . $filter . '%')
                    ->orwhere('CUST_JC_NUM', 'like', '%' . $filter . '%')
                    ->orwhere('AUART', 'like', '%' . $filter . '%')
                    ->orwhere('ITVAL', 'like', '%' . $filter . '%')
                    ->orwhere('ARBEI', 'like', '%' . $filter . '%')
                    ->orwhere('ILART', 'like', '%' . $filter . '%')
                    ->orwhere('AREA', 'like', '%' . $filter . '%')
                    ->orwhere('PHASE', 'like', '%' . $filter . '%')
                    ->orwhere('DAY', 'like', '%' . $filter . '%')
                    ->orwhere('STATUS', 'like', '%' . $filter . '%')
                    ->orwhere('DATECLOSE', 'like', '%' . $filter . '%')
                    ->orwhere('REMARK', 'like', '%' . $filter . '%')
                    ->orwhere('FREETEXT', 'like', '%' . $filter . '%')
                    ->orwhere('DATEPROGRESS', 'like', '%' . $filter . '%');
            $total = $active_job->count();
        }

        if (isset($_GET['sort']) && $_GET['sort'] != "") {
            $active_job = $active_job->orderBy($_GET['sort'], $_GET['asc'] == "0" ? "DESC" : "ASC");
        } else {
            $active_job = $active_job->orderBy('SEQ_NUM', 'ASC');
        }

        $active_job = $active_job->skip($from)->take($rowByPage);
        $grid->setPaginator(ceil($total / $rowByPage), (int) $total, (int) $totalUnfiltered, null);
        $grid->renderJSON($active_job->get(), false, false, !isset($_GET['data_only']));
    }

    public function crud_jobcard_load_all() {
        $data["session"] = $this->session->userdata('logged_in');
        $phase = $this->Phase_model->get();
        $phase_arr = array();
        $area = array(
            "Cockpit" => "Cockpit",
            "Fuselage" => "Fuselage",
            "LH-Wing" => "LH-Wing",
            "RH-Wing" => "RH-Wing",
            "L/G" => "L/G",
            "ENG#1" => "ENG#1",
            "ENG#2" => "ENG#2",
            "ENG#3" => "ENG#3",
            "ENG#4" => "ENG#4",
            "Tail" => "Tail",
            "Cabin" => "Cabin",
            "FWD Cargo" => "FWD Cargo",
            "AFT Cargo" => "AFT Cargo",
            "Bulk Cargo" => "Bulk Cargo",
            "Main Cargo" => "Main Cargo",
            "Elect" => "Elect",
            "General Area" => "General Area"
        );


        foreach ($phase as $value) {
            $phase_arr[$value->ID] = $value->PHASE;
        }

        $grid = new EditableGrid();

        if(!$this->is_customer){            
            $grid->addColumn('SEQ_NUM', 'SEQ', 'int', NULL, false);
            $grid->addColumn('AUFNR_LINK', 'Order', 'html', NULL, false);
            // $grid->addColumn('AUFNR', 'Order', 'html', NULL, false, 'AUFNR');
            $grid->addColumn('KTEXT', 'Description', 'html', NULL, false);
            $grid->addColumn('CUST_JC_NUM', 'Cust JC Num', 'string', NULL, false);
            $grid->addColumn('ILART', 'Task', 'string', NULL, false);
            $grid->addColumn('ITVAL', 'ITVAL', 'string');
            $grid->addColumn('MHRS', 'Mhrs Plan', 'string', NULL, false);
            $grid->addColumn('SKILL', 'Skill', 'string', array("A/P" => "A/P", "CBN" => "CBN", "E/A" => "E/A", "STR" => "STR", "WS" => "WS"));
            $grid->addColumn('AREA', 'Area', 'string', $area);
            $grid->addColumn('PHASE', 'Phase', 'string', $phase_arr);
            $grid->addColumn('DAY', 'Day', 'string');
            $grid->addColumn('STATUS', 'Status', 'string', array("OPEN" => "OPEN", "CLOSED" => "CLOSED", "PERFORM BY PROD" => "PERFORM BY PROD", "PERFORM TO SHOP" => "PERFORM TO SHOP", "WAITING MATERIAL" => "WAITING MATERIAL", "WAITING TOOL" => "WAITING TOOL", "PREPARE FOR INSTALL" => "PREPARE FOR INSTALL", "PREPARE FOR TEST" => "PREPARE FOR TEST", "PREPARE FOR RUN UP" => "PREPARE FOR RUN UP", "PREPARE FOR NDT" => "PREPARE FOR NDT", "PART AVAIL" => "PART AVAIL"));
            $grid->addColumn('DATECLOSE', 'Date Closed', 'date');
            $grid->addColumn('JC_REFF_MDR_VAL', 'MDR Issued', 'string', NULL, false);
            $grid->addColumn('REMARK', 'Remark', 'string', array("N/A (NOT APPLICABLE)" => "N/A (NOT APPLICABLE)", "WITHDRAWN" => "WITHDRAWN", "COVER BY ANOTHER JOBCARD" => "COVER BY ANOTHER JOBCARD", "INTERUPT" => "INTERUPT", "MAP SHORTAGE" => "MAP SHORTAGE", "MAP GADC" => "MAP GADC", "MAP W. CUST. SUPPLY" => "MAP W. CUST. SUPPLY", "PART COMPLETE" => "PART COMPLETE", "PART PARTIAL" => "PART PARTIAL", "ANOTHER REASON" => "ANOTHER REASON"));
            $grid->addColumn('DOC_SENT_STATUS', 'Doc. Sent Status', 'string', array("SENT TO CABIN" => "SENT TO CABIN", "SENT TO CABIN SHOP" => "SENT TO CABIN SHOP", "SENT TO PAINTING" => "SENT TO PAINTING", "SENT TO NDT" => "SENT TO NDT", "SENT TO TBR SHOP" => "SENT TO TBR SHOP", "SENT TO SEALANT" => "SENT TO SEALANT", "SENT TO STR HANGAR" => "SENT TO STR HANGAR", "SENT TO SEAT" => "SENT TO SEAT", "SENT TO WHEEL SHOP" => "SENT TO WHEEL SHOP", "SENT TO TV ENGINE" => "SENT TO TV ENGINE", "SENT TO CLEANING" => "SENT TO CLEANING"));
    
            $grid->addColumn('FREETEXT', 'Free Text', 'html');
            $grid->addColumn('DATEPROGRESS', 'Date Progress', 'date');
            $grid->addColumn('MAT_FULLFILLMENT_STATUS', 'Material Status', 'string', NULL, false);
            $grid->addColumn('CABINSTATUS', 'Cabin Status', 'string', array(
                                    "Open" => "Open", 
                                    "Waiting RO"=>"Waiting RO", 
                                    "Waiting Material" => "Waiting Material",
                                    "Progress in Hangar" => "Progress in Hangar",
                                    "Progress in W101" => "Progress in W101",
                                    "Progress in W102" => "Progress in W102",
                                    "Progress in W103" => "Progress in W103",
                                    "Progress in W401" => "Progress in W401",
                                    "Progress in W402" => "Progress in W402",
                                    "Progress in W501" => "Progress in W501",
                                    "Progress in W502" => "Progress in W502",
                                    "Close" => "Close" 
                                ));
            $grid->addColumn('STATUS_SAP', 'Status SAP', 'string', NULL, false);
            $grid->addColumn('AUFNR_DEL', 'Action', 'html', NULL, false);
        } else {
            $grid->addColumn('SEQ_NUM', 'Seq', 'integer', NULL, false);
            $grid->addColumn('AUFNR', 'Order', 'html', NULL, false);
            $grid->addColumn('CUST_JC_NUM', 'Cust JC Num', 'string', NULL, false);
            $grid->addColumn('KTEXT', 'Discrepancies', 'html', NULL, false);
            $grid->addColumn('STATUS', 'Status', 'string', NULL, false);
        }


//        $sql = "EXEC TB_P_JOBCARD_PROGRESS @REVNR = " . $this->session->userdata('REVNR');
        
        $ORDER = 'SEQ_NUM';
        $AUFNR = 0;
        $FILTER = '';

        if (isset($_GET['sort']) && $_GET['sort'] != "") {
            $ORDER = $_GET['sort'] . ($_GET['asc'] == "0" ? " DESC" : " ASC");
        }
        
        if ($this->session->userdata('jc_id')) {
            $AUFNR = $this->session->userdata('jc_id');

        }

        if (isset($_GET['filter']) && $_GET['filter'] != "") {
            $FILTER = $_GET['filter'];
        }
        
        // $active_job = $this->Proc_model->get_jc($this->session->userdata('REVNR'), $ORDER, $AUFNR, $FILTER);
        // $grid->renderJSON($active_job, false, false, true);

       if ($this->Proc_model->merge_pmorder($this->session->userdata('REVNR'))) {
            $active_job = $this->Proc_model->get_jc($this->session->userdata('REVNR'), $ORDER, $AUFNR, $FILTER);           
       }

       $grid->renderJSON($active_job, false, false, true);
    }

    function crud_jobcard_after_insert($post_array, $primary_key) {
        $jobcard_update = array(
            "id_project" => $this->session->userdata('id_project')
        );
        $this->db->update('jobcard', $jobcard_update, array('id' => $primary_key));
        return true;
    }

    function crud_jobcard_seq_add_field_callback() {
        error_reporting(E_ALL & ~E_NOTICE);
        $last_seq = 0;
        try {
            $query = $this->Jobcard_model
                            ->where('id_project', $this->session->userdata('id_project'))
                            ->orderBy('seq', 'DESC')
                            ->first()->seq;
            // $last = $query->seq;
            $last_seq = $query + 1;
        } catch (Exception $e) {
            error_log($e);
            $last_seq = 1;
        }
        return '<input type="text" value="' . $last_seq . '" name="seq">';
    }

    public function crud_jobcard_update() {
        $data["session"] = $this->session->userdata('logged_in');

        $id = strip_tags($_POST['id']);
        $coltype = strip_tags($_POST['coltype']);
        $value = ($coltype == 'html') ? $_POST['newvalue'] : strip_tags($_POST['newvalue']);
        $colname = strip_tags($_POST['colname']);

        if ($colname == "SKILL_TYPE") {
            $colname = "SKILL";
        }

        if ($colname == 'ITVAL') {
            $jc = $this->Jobcard_model->where('AUFNR', $id)->first();
            if ($jc) {
                $CUST_JC_NUM = get_cust_jc_num($jc->KTEXT);
                $colname = 'KTEXT';
                $value = "[" . $value . "]" . $CUST_JC_NUM;
            }
        }


        if ($coltype == 'date') {
            if ($value === "") {
                $value = NULL;
            } else {
                $date_info = date_parse_from_format('d/m/Y', $value);
                $value = "{$date_info['year']}-{$date_info['month']}-{$date_info['day']}";
            }
        }

        try {
            $jobcard = $this->Jobcard_model->where('AUFNR', $id)
                    ->where('REVNR', $this->session->userdata('REVNR'))
                    ->update([$colname => $value]);
            if ($jobcard) {
                echo "ok";
            } else {
                echo "error";
            }
        } catch (Exception $exc) {
            echo "error";
            echo $exc;
        } finally {

            try {
                $mdr = $this->Mdr_model->where('JC_REFF', 'LIKE', '%' . $id)
                        ->update(["AREA" => $value]);
            } catch (Exception $e) {
                echo $e;
            }

        }
    }

    public function crud_jobcard_delete() {
        $data["session"] = $this->session->userdata('logged_in');

        $id = strip_tags($_POST['id']);
        // $tablename = strip_tags($_POST['tablename']);

        try {
            $this->Jobcard_model->destroy($id);
        } catch (Exception $exc) {
            echo "error";
        } finally {
            echo "ok";
        }
    }

    public function crud_mdr() {
        try {
            if ($this->uri->segment(3)) {
                $this->session->set_userdata('REVNR', $this->uri->segment(3));
            }
            $data['id_project'] = $this->session->userdata('REVNR');
            $active_prj = $this->Project_model_ci->getProjectName($data['id_project']);
            if ($active_prj) {
                $data['project_name'] = $active_prj->REVTX;
            }
        } catch (Exception $e) {
            error_log($e);
        }

        try {
            if ($this->uri->segment(4)) {
                $this->session->set_userdata('jc_id', $this->uri->segment(4));
            } else {
                $this->session->unset_userdata('jc_id');
            }

            if ($this->uri->segment(5)) {
                $this->session->set_userdata('mdr_type', $this->uri->segment(5));
            } else {
                $this->session->unset_userdata('mdr_type');
            }

            $data['jc_id'] = $this->session->userdata('jc_id');
        } catch (Exception $e) {
            error_log($e);
        }
        $this->Proc_model->merge_pmorder($this->session->userdata('REVNR'));
        $data["session"] = $this->session->userdata('logged_in');
        $data['content'] = 'admin/projects/crud_mdr';
        $this->load->view('template', $data);
    }

    public function read_xlsx_file_mdr() {
        $response["status"] = NULL;
        $response["body"] = NULL;
        try {
            $path = $_FILES['media']['tmp_name'];
            $reader = ReaderFactory::create(Type::XLSX);
            $reader->open($path);
            $i = 0;
            $data["error"] = array();
            $data["success"] = array();

            foreach ($reader->getSheetIterator() as $sheet) {
                foreach ($sheet->getRowIterator() as $row => $value) {
                    if ($i == 0) {
                        $i++;
                        continue;
                    }

                    $list = array(
                        'SKILL',
                        'DATE_PE',
                        'STATUS',
                        'MATSTATUS',
                        'STEP1',
                        'DATE1',
                        'STEP2',
                        'DATE2',
                        'STEP3',
                        'DATE3',
                        'DATECLOSE',
                        'REMARK',
                        'DOC_SENT_STATUS',
                        'FREETEXT',
                        'DATEPROGRESS',
                        'DAY',
                        'CABINSTATUS'
                    );

                    foreach ($value as $key2 => $value2) {
                        if ($key2 >= 2 and $key2 <= 17) {
                            $item[$list[$key2 - 2]] = (string) $value2 ?: null;
                        }
                    }


                    $mdr = $this->Mdr_model
                                ->where('REVNR', $value[0])
                                ->where('AUFNR', $value[1])
                                ->where('AUART', 'GA02')
                                ->update($item);

                    
                    if ($mdr) {
                        array_push($data["success"], $value[0]);

                        if (!empty($item['STATUS'])) {
                            $this->update_status_mdr($value[1], $item['STATUS']);
                        }

                    } else {
                        array_push($data["error"], $value[0]);
                    }
                }
            }

            if (count($data["success"]) > 0) {
                $response["status"] = 'success';
                $response["body"] = $data;
            }
        } catch (Exception $e) {
            echo $e;
            exit;
        }
        header('Content-Type: application/json');
        echo json_encode($response);
    }

    public function crud_mdr_load() {
        $data["session"] = $this->session->userdata('logged_in');

        $grid = new EditableGrid();

        $grid->addColumn('SEQ_NUM', 'Seq', 'integer', NULL, false);
        $grid->addColumn('AUFNR_LINK', 'MDR Order', 'html', NULL, false);
        $grid->addColumn('MAUFNR_LINK', 'JC REFF', 'html', NULL, false);
        $grid->addColumn('CUST_JC_NUM', 'Cust JC Num', 'string', NULL, false);
        $grid->addColumn('KTEXT', 'Discrepancies', 'string');
        $grid->addColumn('AREA', 'AREA Code', 'string');
        $grid->addColumn('ERDAT', 'Created On', 'string', NULL, false);
        // $grid->addColumn('SKILL_TYPE', 'Main Skill', 'string');
        $grid->addColumn('SKILL_TYPE', 'Main Skill', 'string', array("A/P" => "A/P", "E/A" => "E/A", "CBN" => "CBN", "STR" => "STR", "TBP" => "TBP"));
        $grid->addColumn('ERNAM', 'Iss. By', 'string', NULL, false);
        $grid->addColumn('DATE_PE', 'Date from PE', 'date');
        $grid->addColumn('STATUS', 'Accomp. Status', 'string', array("CARRY OUT" => "CARRY OUT", "PERFORM BY PROD" => "PERFORM BY PROD", "PERFORM TO SHOP" => "PERFORM TO SHOP", "WAITING MATERIAL" => "WAITING MATERIAL", "WAITING TOOL" => "WAITING TOOL", "NEXT RO" => "NEXT RO", "WAITING RO" => "WAITING RO", "WAITING DEPLOYMENT" => "WAITING DEPLOYMENT", "WAITING CUST APPROVAL" => "WAITING CUST APPROVAL", "PREPARE FOR TEST" => "PREPARE FOR TEST", "PREPARE FOR RUN UP" => "PREPARE FOR RUN UP", "PREPARE FOR NDT" => "PREPARE FOR NDT", "PART AVAIL" => "PART AVAIL"));
        $grid->addColumn('MATSTATUS', 'Mat Status', 'string', array("SHORTAGE" => "SHORTAGE", "GADC" => "GADC", "WCS" => "WCS"));
        $grid->addColumn('date', 'Date', 'date');
        $grid->addColumn('step1', 'STEP1', 'string', array("A/P" => "A/P", "E/A" => "E/A", "CBN" => "CBN", "STR" => "STR", "TBRS" => "TBRS", "NDT" => "NDT", "SEAT" => "SEAT", "CBN SHOP" => "CBN SHOP", "TBP" => "TBP", "TVP" => "TVP", "CLN" => "CLN", "WHL" => "WHL", "RAHU" => "RAHU"));
        $grid->addColumn('date1', 'Date 1 ', 'date');
        $grid->addColumn('step2', 'STEP2', 'string', array("A/P" => "A/P", "E/A" => "E/A", "CBN" => "CBN", "STR" => "STR", "TBRS" => "TBRS", "NDT" => "NDT", "SEAT" => "SEAT", "CBN SHOP" => "CBN SHOP", "TBP" => "TBP", "TVP" => "TVP", "CLN" => "CLN", "WHL" => "WHL", "RAHU" => "RAHU"));
        $grid->addColumn('date2', 'Date 2', 'date');
        $grid->addColumn('step3', 'STEP3', 'string', array("A/P" => "A/P", "E/A" => "E/A", "CBN" => "CBN", "STR" => "STR", "TBRS" => "TBRS", "NDT" => "NDT", "SEAT" => "SEAT", "CBN SHOP" => "CBN SHOP", "TBP" => "TBP", "TVP" => "TVP", "CLN" => "CLN", "WHL" => "WHL", "RAHU" => "RAHU"));
        $grid->addColumn('date3', 'Date 3', 'date');
        $grid->addColumn('DATECLOSE', 'Date Close', 'date');
        $grid->addColumn('MAT_FULLFILLMENT_STATUS', 'Material Status MRM', 'string', NULL, false);
        $grid->addColumn('REMARK', 'Remark', 'string', array("SENT TO CABIN" => "SENT TO CABIN", "SENT TO CABIN SHOP" => "SENT TO CABIN SHOP", "SENT TO PAINTING" => "SENT TO PAINTING", "SENT TO NDT" => "SENT TO NDT", "SENT TO TBR SHOP" => "SENT TO TBR SHOP", "SENT TO SEALANT" => "SENT TO SEALANT", "SENT TO STR HANGAR" => "SENT TO STR HANGAR", "SENT TO SEAT" => "SENT TO SEAT", "SENT TO WHEEL SHOP" => "SENT TO WHEEL SHOP", "SENT TO TV ENGINE" => "SENT TO TV ENGINE", "SENT TO CLEANING" => "SENT TO CLEANING", "MDR BELUM KE PPC" => "MDR BELUM KE PPC", "MATERIAL" => "MATERIAL", "RO BY ->" => "RO BY ->", "WAITING JOB ->" => "WAITING JOB", "COVER BY" => "COVER BY", "INTERUPT" => "INTERUPT", "MAP SHORTAGE" => "MAP SHORTAGE", "MAP GADC" => "MAP GADC", "MAP W. CUST. SUPPLY" => "MAP W. CUST. SUPPLY", "PART COMPLETE" => "PART COMPLETE", "PART PARTIAL" => "PART PARTIAL", "ANOTHER REASON" => "ANOTHER REASON"));
        $grid->addColumn('FREETEXT', 'Free Text', 'html');
        $grid->addColumn('DATEPROGRESS', 'Date Progress', 'date');
        $grid->addColumn('DAY', 'Day', 'integer');
        $grid->addColumn('CABINSTATUS', 'Cabin Status', 'string', NULL, false);
        $grid->addColumn('action', 'Action', 'html', NULL, false, 'id');

        $active_job = $this->Mdr_model_view
                ->selectRaw('*,'
                        . 'AUFNR AS id,'
                        . ((($this->db->dbdriver == 'sqlsrv') or ( $this->db->dbdriver == 'mssql')) ? "'<a href=\"" . base_url() . "projects/crud_mrm/' + REVNR + '/' + AUFNR + '\">' + AUFNR + '</a>' AS AUFNR_LINK," : "CONCAT('<a href=\"" . base_url() . "projects/crud_mrm/', REVNR, '/', AUFNR, '\">', AUFNR, '</a>') AS AUFNR_LINK,")
                        . ((($this->db->dbdriver == 'sqlsrv') or ( $this->db->dbdriver == 'mssql')) ? "'<a href=\"" . base_url() . "projects/crud_jobcard/' + REVNR + '/' + MAUFNR + '\">' + MAUFNR + '</a>' AS MAUFNR_LINK," : "CONCAT('<a href=\"" . base_url() . "projects/crud_mrm/', REVNR, '/', MAUFNR, '\">', MAUFNR, '</a>') AS MAUFNR_LINK,")
                        . '(select AREA from TB_V_JOBCARD_PROGRESS where TB_V_MDR_PROGRESS.MAUFNR = TB_V_JOBCARD_PROGRESS.AUFNR) AS AREA_2,'
                        . '(select SKILL_TYPE from TB_V_JOBCARD_PROGRESS where TB_V_MDR_PROGRESS.MAUFNR = TB_V_JOBCARD_PROGRESS.AUFNR) AS SKILL_TYPE_2')
                ->where('REVNR', $this->session->userdata('REVNR'))
                ->orderBy('AUFNR', 'ASC');

        if ($this->session->userdata('mdr_id') != NULL) {
            $active_job = $active_job->where('AUFNR', $this->session->userdata('mdr_id'));
        }

        if ($this->session->userdata('jc_id') != NULL) {
            $active_job = $active_job->where('MAUFNR', $this->session->userdata('jc_id'));
        }

        $totalUnfiltered = $active_job->count();
        $total = $totalUnfiltered;

        $page = 0;
        if (isset($_GET['page']) && is_numeric($_GET['page']))
            $page = (int) $_GET['page'];


        $rowByPage = $totalUnfiltered; // 50;

        $from = ($page - 1) * $rowByPage;

        if (isset($_GET['filter']) && $_GET['filter'] != "") {
            $filter = $_GET['filter'];
            $active_job = $active_job
                    ->where('AUFNR', 'like', '%' . $filter . '%')
                    ->orwhere('KTEXT', 'like', '%' . $filter . '%')
                    ->orwhere('CUST_JC_NUM', 'like', '%' . $filter . '%')
                    ->orwhere('AUART', 'like', '%' . $filter . '%')
                    ->orwhere('ITVAL', 'like', '%' . $filter . '%')
                    ->orwhere('ARBEI', 'like', '%' . $filter . '%')
                    ->orwhere('ILART', 'like', '%' . $filter . '%')
                    ->orwhere('AREA', 'like', '%' . $filter . '%')
                    ->orwhere('PHASE', 'like', '%' . $filter . '%')
                    ->orwhere('DAY', 'like', '%' . $filter . '%')
                    ->orwhere('STATUS', 'like', '%' . $filter . '%')
                    ->orwhere('DATECLOSE', 'like', '%' . $filter . '%')
                    ->orwhere('REMARK', 'like', '%' . $filter . '%')
                    ->orwhere('FREETEXT', 'like', '%' . $filter . '%')
                    ->orwhere('DATEPROGRESS', 'like', '%' . $filter . '%');
            $total = $active_job->count();
        }

        if (isset($_GET['sort']) && $_GET['sort'] != "") {
            $active_job = $active_job->orderBy($_GET['sort'], $_GET['asc'] == "0" ? "DESC" : "ASC");
        } else {
            $active_job = $active_job->orderBy('SEQ_NUM', 'ASC');
        }

        $active_job = $active_job->skip($from)->take($rowByPage);
        $grid->setPaginator(ceil($total / $rowByPage), (int) $total, (int) $totalUnfiltered, null);
        $grid->renderJSON($active_job->get(), false, false, !isset($_GET['data_only']));
    }

    public function crud_mdr_load_all() {
        $data["session"] = $this->session->userdata('logged_in');

        $grid = new EditableGrid();
        if(!$this->is_customer){
            $grid->addColumn('SEQ_NUM', 'Seq', 'integer', NULL, false);
            $grid->addColumn('AUFNR_LINK', 'Order', 'html', NULL, false);
            $grid->addColumn('JC_REF_LINK', 'JC REF', 'html', NULL, false);
            $grid->addColumn('CUST_JC_NUM', 'Cust JC Num', 'string', NULL, false);
            $grid->addColumn('KTEXT', 'Discrepancies', 'html', NULL, false);
            $grid->addColumn('AREA', 'AREA', 'string', NULL, false);
            $grid->addColumn('ERDAT', 'Created On', 'string', NULL, false);
            $grid->addColumn('SKILL', 'Main Skill', 'string', array("A/P" => "A/P", "E/A" => "E/A", "CBN" => "CBN", "STR" => "STR", "TBP" => "TBP"));
            $grid->addColumn('ERNAM', 'Iss. By', 'string', NULL, false);
            $grid->addColumn('DATE_PE', 'Date from PE', 'date');
            $grid->addColumn('STATUS', 'Accomp. Status', 'string', array("CARRY OUT" => "CARRY OUT", "PERFORM BY PROD" => "PERFORM BY PROD", "PERFORM TO SHOP" => "PERFORM TO SHOP", "WAITING MATERIAL" => "WAITING MATERIAL", "WAITING TOOL" => "WAITING TOOL", "NEXT RO" => "NEXT RO", "WAITING RO" => "WAITING RO", "WAITING DEPLOYMENT" => "WAITING DEPLOYMENT", "WAITING CUST APPROVAL" => "WAITING CUST APPROVAL", "PREPARE FOR TEST" => "PREPARE FOR TEST", "PREPARE FOR RUN UP" => "PREPARE FOR RUN UP", "PREPARE FOR NDT" => "PREPARE FOR NDT", "PART AVAIL" => "PART AVAIL"));
            $grid->addColumn('MATSTATUS', 'Mat Status', 'string', array("SHORTAGE" => "SHORTAGE", "GADC" => "GADC", "WCS" => "WCS"));
            $grid->addColumn('STEP1', 'STEP1', 'string', array("A/P" => "A/P", "E/A" => "E/A", "CBN" => "CBN", "STR" => "STR", "TBRS" => "TBRS", "NDT" => "NDT", "SEAT" => "SEAT", "CBN SHOP" => "CBN SHOP", "TBP" => "TBP", "TVP" => "TVP", "CLN" => "CLN", "WHL" => "WHL", "RAHU" => "RAHU"));
            $grid->addColumn('DATE1', 'Date 1 ', 'date');
            $grid->addColumn('STEP2', 'STEP2', 'string', array("A/P" => "A/P", "E/A" => "E/A", "CBN" => "CBN", "STR" => "STR", "TBRS" => "TBRS", "NDT" => "NDT", "SEAT" => "SEAT", "CBN SHOP" => "CBN SHOP", "TBP" => "TBP", "TVP" => "TVP", "CLN" => "CLN", "WHL" => "WHL", "RAHU" => "RAHU"));
            $grid->addColumn('DATE2', 'Date 2', 'date');
            $grid->addColumn('STEP3', 'STEP3', 'string', array("A/P" => "A/P", "E/A" => "E/A", "CBN" => "CBN", "STR" => "STR", "TBRS" => "TBRS", "NDT" => "NDT", "SEAT" => "SEAT", "CBN SHOP" => "CBN SHOP", "TBP" => "TBP", "TVP" => "TVP", "CLN" => "CLN", "WHL" => "WHL", "RAHU" => "RAHU"));
            $grid->addColumn('DATE3', 'Date 3', 'date');
            $grid->addColumn('MDR_STATUS', 'Status', 'string', NULL, false);
            $grid->addColumn('DATECLOSE', 'Date Close', 'date');
            $grid->addColumn('MATERIAL_FULFILLMENT_STATUS', 'Material Status MRM', 'string', NULL, false);
            $grid->addColumn('FULLFILLMENT_STATUS_DATE', 'Fulfillment Date', 'date', NULL, false);
            $grid->addColumn('REMARK', 'Remark', 'string', array("MDR BELUM KE PPC" => "MDR BELUM KE PPC", "MATERIAL" => "MATERIAL", "RO BY ->" => "RO BY ->", "WAITING JOB ->" => "WAITING JOB", "COVER BY" => "COVER BY", "INTERUPT" => "INTERUPT", "MAP SHORTAGE" => "MAP SHORTAGE", "MAP GADC" => "MAP GADC", "MAP W. CUST. SUPPLY" => "MAP W. CUST. SUPPLY", "PART COMPLETE" => "PART COMPLETE", "PART PARTIAL" => "PART PARTIAL", "ANOTHER REASON" => "ANOTHER REASON"));
            $grid->addColumn('DOC_SENT_STATUS', 'Doc. Sent Status', 'string', array("SENT TO CABIN" => "SENT TO CABIN", "SENT TO CABIN SHOP" => "SENT TO CABIN SHOP", "SENT TO PAINTING" => "SENT TO PAINTING", "SENT TO NDT" => "SENT TO NDT", "SENT TO TBR SHOP" => "SENT TO TBR SHOP", "SENT TO SEALANT" => "SENT TO SEALANT", "SENT TO STR HANGAR" => "SENT TO STR HANGAR", "SENT TO SEAT" => "SENT TO SEAT", "SENT TO WHEEL SHOP" => "SENT TO WHEEL SHOP", "SENT TO TV ENGINE" => "SENT TO TV ENGINE", "SENT TO CLEANING" => "SENT TO CLEANING"));
            $grid->addColumn('FREETEXT', 'Free Text', 'html');
            $grid->addColumn('DATEPROGRESS', 'Date Progress', 'date');
            $grid->addColumn('DAY', 'Day', 'integer');
            $grid->addColumn('CABINSTATUS', 'Cabin Status', 'string', array(
                "Open" => "Open", 
                "Waiting RO"=>"Waiting RO", 
                "Waiting Material" => "Waiting Material",
                "Progress in Hangar" => "Progress in Hangar",
                "Progress in W101" => "Progress in W101",
                "Progress in W102" => "Progress in W102",
                "Progress in W103" => "Progress in W103",
                "Progress in W401" => "Progress in W401",
                "Progress in W402" => "Progress in W402",
                "Progress in W501" => "Progress in W501",
                "Progress in W502" => "Progress in W502",
                "Close" => "Close" 
            ));
            $grid->addColumn('AUFNR_DEL', 'Action', 'html', NULL, false);
        } else {
            $grid->addColumn('SEQ_NUM', 'Seq', 'integer', NULL, false);
            $grid->addColumn('AUFNR', 'MDR Order', 'html', NULL, false);
            $grid->addColumn('JC_REF', 'JC REFF', 'html', NULL, false);
            $grid->addColumn('KTEXT', 'Discrepancies', 'html', NULL, false);
            $grid->addColumn('MDR_STATUS', 'Status', 'string', NULL, false);
        }

        $ORDER = 'SEQ_NUM';
        $AUFNR = 0;
        $JC_REF = '';
        $FILTER = '';

        if (isset($_GET['sort']) && $_GET['sort'] != "") {
            $ORDER = $_GET['sort'] . ($_GET['asc'] == "0" ? " DESC" : " ASC");
        }
        
        if (($this->session->userdata("mdr_type") == "JC") || $this->session->userdata("mdr_type") == "jc") {
            $JC_REF = $this->session->userdata('jc_id');
        }
        
        if (($this->session->userdata("mdr_type") == "MRM") || $this->session->userdata("mdr_type") == "mrm") {
            $AUFNR = $this->session->userdata('jc_id');
        }
        
        if (($this->session->userdata("mdr_type") == "CSP") || $this->session->userdata("mdr_type") == "csp") {
            $AUFNR = $this->session->userdata('jc_id');
        }

        if (isset($_GET['filter']) && $_GET['filter'] != "") {
            $FILTER = $_GET['filter'];
        }
        
        $active_job = $this->Proc_model->get_mdr($this->session->userdata('REVNR'), $ORDER, $AUFNR, $JC_REF, $FILTER);
        $grid->renderJSON($active_job, false, false, true);

//        $sql = "EXEC TB_P_MDR_PROGRESS @REVNR = " . $this->session->userdata('REVNR');
//        
//        if ($this->session->userdata("mdr_type") == "JC") {
//            $sql .= ", @JC_REF = " . $this->session->userdata('jc_id');
//        }
//        if ($this->session->userdata("mdr_type") == "MRM") {
//            $sql .= ", @AUFNR = " . $this->session->userdata('jc_id');
//        }
//        if ($this->session->userdata("mdr_type") == "CSP") {
//            $sql .= ", @AUFNR = " . $this->session->userdata('jc_id');
//        }
//        if (isset($_GET['filter']) && $_GET['filter'] != "") {
//            $sql .= ", @FILTER = '" . $_GET['filter'] . "'";
//        }
//        
////        if ($this->Proc_model->merge_pmorder($this->session->userdata('REVNR'))) {
//            $active_job = $this->db->query($sql);
//            $grid->renderJSON($active_job->result(), false, false, true);
////        }
//
////        $active_job = $this->db->query($sql);
////
////        $grid->renderJSON($active_job->result(), false, false, true);
    }

    public function crud_mdr_add() {
        $crud = new grocery_CRUD();

        $crud->set_table('mdr');
        $crud->set_subject('Mdr');
        $crud->columns('seq', 'date_from_pe', 'accomp_status', 'mat_status', 'date', 'step1', 'date1', 'step2', 'date2', 'step3', 'date3', 'status_mdr', 'date_close', 'remark', 'free_text', 'date_progress', 'day');
        $crud->field_type('accomp_status', 'dropdown', array("Carry Out" => "Carry Out", "Perform by Prod" => "Perform by Prod", "Perform To Shop" => "Perform To Shop", "Waiting Material" => "Waiting Material", "Waiting Tool" => "Waiting Tool", "Next RO" => "Next RO", "Waiting RO" => "Waiting RO", "Waiting Deployment" => "Waiting Deployment", "Waiting Cust Approval" => "Waiting Cust Approval", "Prepare for Test" => "Prepare for Test", "Prepare for Run Up" => "Prepare for Run Up", "Prepare for NDT" => "Prepare for NDT", "Part Avail" => "Part Avail"));
        $crud->field_type('mat_status', 'dropdown', array("Shortage" => "Shortage", "GADC" => "GADC", "WCS" => "WCS"));
        $crud->field_type('step1', 'dropdown', array("A/P" => "A/P", "E/A" => "E/A", "CBN" => "CBN", "STR" => "STR", "TBRS" => "TBRS", "NDT" => "NDT", "SEAT" => "SEAT", "CBN SHOP" => "CBN SHOP", "TBP" => "TBP", "TVP" => "TVP", "CLN" => "CLN", "WHL" => "WHL", "RAHU" => "RAHU"));
        $crud->field_type('step2', 'dropdown', array("A/P" => "A/P", "E/A" => "E/A", "CBN" => "CBN", "STR" => "STR", "TBRS" => "TBRS", "NDT" => "NDT", "SEAT" => "SEAT", "CBN SHOP" => "CBN SHOP", "TBP" => "TBP", "TVP" => "TVP", "CLN" => "CLN", "WHL" => "WHL", "RAHU" => "RAHU"));
        $crud->field_type('step3', 'dropdown', array("A/P" => "A/P", "E/A" => "E/A", "CBN" => "CBN", "STR" => "STR", "TBRS" => "TBRS", "NDT" => "NDT", "SEAT" => "SEAT", "CBN SHOP" => "CBN SHOP", "TBP" => "TBP", "TVP" => "TVP", "CLN" => "CLN", "WHL" => "WHL", "RAHU" => "RAHU"));
        $crud->field_type('remark', 'dropdown', array("SENT TO CABIN" => "SENT TO CABIN", "SENT TO CABIN SHOP" => "SENT TO CABIN SHOP", "SENT TO PAINTING" => "SENT TO PAINTING", "SENT TO NDT" => "SENT TO NDT", "SENT TO TBR SHOP" => "SENT TO TBR SHOP", "SENT TO SEALANT" => "SENT TO SEALANT", "SENT TO STR HANGAR" => "SENT TO STR HANGAR", "SENT TO SEAT" => "SENT TO SEAT", "SENT TO WHEEL SHOP" => "SENT TO WHEEL SHOP", "SENT TO TV ENGINE" => "SENT TO TV ENGINE", "SENT TO CLEANING" => "SENT TO CLEANING", "MDR BELUM KE PPC" => "MDR BELUM KE PPC", "MATERIAL" => "MATERIAL", "RO BY ->" => "RO BY ->", "WAITING JOB ->" => "WAITING JOB", "COVER BY" => "COVER BY", "INTERUPT" => "INTERUPT", "MAP SHORTAGE" => "MAP SHORTAGE", "MAP GADC" => "MAP GADC", "MAP W. CUST. SUPPLY" => "MAP W. CUST. SUPPLY", "PART COMPLETE" => "PART COMPLETE", "PART PARTIAL" => "PART PARTIAL", "ANOTHER REASON" => "ANOTHER REASON"));
        $crud->set_relation('jc_reff', 'jobcard', 'jc_order');
        $crud->unset_fields('ori_taskcard', 'jc_reff', 'discrepancies', 'area_code', 'main_skill', 'iss_by', 'mdr_order', 'material_status_mrm', 'status_sap', 'cabin_status', 'id_project', 'date_created', 'id');
        $crud->callback_after_insert(array($this, 'crud_mdr_after_insert'));
        $crud->callback_add_field('seq', array($this, 'crud_mdr_seq_add_field_callback'));
        $crud->unset_back_to_list();
        $output = $crud->render();

        $data["session"] = $this->session->userdata('logged_in');

        $this->load->view('example', array_merge($data, (array) $output));
    }

    function crud_mdr_after_insert($post_array, $primary_key) {
        date_default_timezone_set('Asia/Jakarta');
        $mdr_update = array(
            "id_project" => $this->session->userdata('id_project'),
            'date_created' => date('Y-m-d H:i:s')
        );
        $this->db->update('mdr', $mdr_update, array('id' => $primary_key));
        return true;
    }

    function crud_mdr_seq_add_field_callback() {
        error_reporting(E_ALL & ~E_NOTICE);
        $last_seq = 0;
        try {
            $query = $this->Mdr_model
                            ->where('id_project', $this->session->userdata('id_project'))
                            ->orderBy('seq', 'DESC')
                            ->first()->seq;
            // $last = $query->seq;
            $last_seq = $query + 1;
        } catch (Exception $e) {
            error_log($e);
            $last_seq = 1;
        }
        return '<input type="text" value="' . $last_seq . '" name="seq">';
    }

    public function crud_mdr_update() {
        $data["session"] = $this->session->userdata('logged_in');

        $colname = strip_tags($_POST['colname']);
        $id = strip_tags($_POST['id']);
        $coltype = strip_tags($_POST['coltype']);
        $value = strip_tags($_POST['newvalue']);

        if ($colname == 'SKILL_TYPE') {
            $colname = 'SKILL';
        }

        if ($coltype == 'date') {
            if ($value === "") {
                $value = NULL;
            } else {
                $date_info = date_parse_from_format('d/m/Y', $value);
                $value = "{$date_info['year']}-{$date_info['month']}-{$date_info['day']}";
            }
        }

        try {
            $mdr = $this->Mdr_model->where('AUFNR', $id)
                    ->where('REVNR', $this->session->userdata('REVNR'))
                    ->update([$colname => $value]);
            if ($mdr) {
                echo "ok";
                if ($colname === 'STATUS') {
                    $this->update_status_mdr($id, $value);
                }
            } else {
                echo "error";
            }
        } catch (Exception $exc) {
            echo $exc;
        }
    }

    public function update_status_mdr($AUFNR, $mdr_status) {
        try {
            $open = array("WAITING RO", "WAITING MATERIAL", "NEXT RO", "WAITING CUST APPROVAL", "WAITING TOOL", "WAITING DEPLOYMENT", "PART AVAIL");
            $progress = array("PERFORM BY PROD", "PERFORM TO SHOP");
            $pending = array("PREPARE FOR TEST", "PREPARE FOR NDT", "PREPARE FOR RUN UP");
            $close = array("CARRY OUT");

            $status = '';
            if (in_array($mdr_status, $open)) {
                $status = 'OPEN';
            }
            if (in_array($mdr_status, $progress)) {
                $status = 'PROGRESS';
            }
            if (in_array($mdr_status, $pending)) {
                $status = 'PENDING';
            }
            if (in_array($mdr_status, $close)) {
                $status = 'CLOSED';
            }

            $mdr = $this->Mdr_model->where('AUFNR', $AUFNR)
                    ->where('REVNR', $this->session->userdata('REVNR'))
                    ->update(['MDR_STATUS' => $status]);
        } catch (Exception $e) {
            echo $e;
        }
    }

    public function crud_mdr_delete() {
        try {
            if ($this->uri->segment(4)) {
                $this->session->set_userdata('id_project', $this->uri->segment(4));
            }
            $data['id_project'] = $this->session->userdata('id_project');
            $active_prj = $this->VProject_model->find($data['id_project']);
            if ($active_prj) {
                $data['project_name'] = $active_prj->project_name;
            }
        } catch (Exception $e) {
            error_log($e);
        }

        $data["session"] = $this->session->userdata('logged_in');

        $id = strip_tags($_POST['id']);
        // $tablename = strip_tags($_POST['tablename']);

        try {
            $this->Mdr_model->destroy($id);
        } catch (Exception $exc) {
            echo "error";
        } finally {
            echo "ok";
        }
    }

    public function crud_mrm() {
        try {
            if ($this->uri->segment(3)) {
                $this->session->set_userdata('REVNR', $this->uri->segment(3));
            }
            $data['id_project'] = $this->session->userdata('REVNR');
            $active_prj = $this->Project_model_ci->getProjectName($data['id_project']);
            if ($active_prj) {
                $data['project_name'] = $active_prj->REVTX;
            }
        } catch (Exception $e) {
            error_log($e);
        }

        try {
            if ($this->uri->segment(4)) {
                $this->session->set_userdata('jc_id', $this->uri->segment(4));
            } else {
                $this->session->unset_userdata('jc_id');
            }
            $data['jc_id'] = $this->session->userdata('jc_id');
        } catch (Exception $e) {
            error_log($e);
        }

        try {
            if ($this->uri->segment(5)) {
                $this->session->set_userdata('mrm_type', $this->uri->segment(5));
            } else {
                $this->session->unset_userdata('mrm_type');
            }
            $data['mrm_type'] = $this->session->userdata('mrm_type');
        } catch (Exception $e) {
            error_log($e);
        }
        $this->Proc_model->merge_mrm($this->session->userdata('REVNR'));
        $data["session"] = $this->session->userdata('logged_in');
        $data['content'] = 'admin/projects/crud_mrm';
        $this->load->view('template', $data);
    }

    public function check_duplicate_mrm() {
        $response["status"] = NULL;
        $response["body"] = NULL;
        try {
            $mrm = $this->Mrm_model
                    ->where('AUFNR', $_POST['AUFNR'])
                    ->where('MATNR', $_POST['MATNR'])
                    ->count();

            if ($mrm > 0) {
                $response["status"] = 'error';
            } else {
                $response["status"] = 'success';
            }
        } catch (Exception $e) {
            
        }
        header('Content-Type: application/json');
        echo json_encode($response);
    }

    public function crud_mrm_load() {
        $data["session"] = $this->session->userdata('logged_in');

        $grid = new EditableGrid();


        $grid->addColumn('ID', 'ID', 'integer', NULL, false);
        $grid->addColumn('CARD_TYPE', 'Card Type', 'string', array("JC" => "JC", "MDR" => "MDR", "AD" => "AD"), NULL, false);
        $grid->addColumn('MATNR', 'Part Number', 'string', NULL, false);
        $grid->addColumn('ALTERNATE_PART_NUMBER', 'Alternate Part Number', 'string');
        $grid->addColumn('MAKTX', 'Material Description', 'string', NULL, false);
//        $grid->addColumn('AUFNR', 'Order Number', 'html', NULL, false, 'AUFNR');
        $grid->addColumn('AUFNR_LINK', 'Order', 'html', NULL, false);
        $grid->addColumn('KTEXT_LINK', 'Discrepancies', 'html', NULL, false);
        $grid->addColumn('MTART', 'Mat Type', 'string', array("EXP" => "EXP", "ROT" => "ROT", "REP" => "REP", "RAW" => "RAW", "CON" => "CON"));
        $grid->addColumn('IPC', 'IPC', 'string');
        $grid->addColumn('STO_NUMBER', 'STO Number', 'string');
        $grid->addColumn('OUTBOND_DELIV', 'Outbound Delivery', 'string');
        $grid->addColumn('TO_NUM', 'TO Number', 'string');
        $grid->addColumn('CUST_JC_NUM', 'Jobcard Number', 'string', NULL, false);
        $grid->addColumn('MRM_ISSUE_DATE', 'MRM Issue Date', 'date', NULL, false);
        $grid->addColumn('MENGE', 'Qty Req Single Item', 'string');
        $grid->addColumn('TOTAL_QTY', 'Total Qty Required for All Job Task', 'string', NULL, false);
        $grid->addColumn('MEINS', 'UOM', 'string', array("FT" => "FT", "E/A" => "E/A", "L" => "L", "LB" => "LB", "QT" => "QT", "ROL" => "ROL", "M" => "M"));
        $grid->addColumn('STOCK_MANUAL', 'Input Stock Manually', 'string');
        $grid->addColumn('LGORT', 'Storage Location', 'string');
        $grid->addColumn('MATERIAL_FULFILLMENT_STATUS', 'Material Fulfillment Status', 'string', array("DLVR FULL to Production" => "DLVR FULL to Production", " DLVR PARTIAL to Production" => "DLVR PARTIAL to Production", "WAITING Customer Supply" => "WAITING Customer Supply", "ROBBING Desicion" => "ROBBING Desicion", "PROVISION in Store" => "PROVISION in Store", "PRELOADED in Hangar Store" => "PRELOADED in Hangar Store", "ORDERED by Purchasing" => "ORDERED by Purchasing", "ORDERED by Loan Control" => "ORDERED by Loan Control", "ORDERED by Workshop" => "ORDERED by Workshop", "EXCHANGE in Progress" => "EXCHANGE in Progress", "Actual STOCK NIL" => "Actual STOCK NIL", "RIC/Part on Receiving Area" => "RIC/Part on Receiving Area", "NO SOURCE" => "NO SOURCE", "Waiting PN Confrimation" => "Waiting PN Confrimation", "GR OK" => "GR OK", "Need FOLLOW UP" => "Need FOLLOW UP", "PENDING/NO ACTION" => "PENDING/NO ACTION", "PR Ready" => "PR Ready", "POOLING" => "POOLING", "Shipment" => "Shipment", "Custom Process" => "Custom Process", "Need SOA" => "Need SOA", "WAITING Customer Approval" => "WAITING Customer Approval"));
        $grid->addColumn('FULLFILLMENT_STATUS_DATE', 'Fullfillment Status Date', 'date', NULL, false);
        $grid->addColumn('FULLFILLMENT_REMARK', 'Fullfillment remark', 'html');
        $grid->addColumn('PO_DATE', 'Date Of PO', 'date');
        $grid->addColumn('PO_NUM', 'Purchase Order PO', 'string');
        $grid->addColumn('AWB_NUMBER', 'AWB Number', 'string');
        $grid->addColumn('AWB_DATE', 'AWB Date', 'date');
        $grid->addColumn('QTY_DELIVERED', 'Qty Delivered', 'string');
        $grid->addColumn('QTY_REMAIN', 'Qty Remain', 'string');
        $grid->addColumn('MATERIAL_REMARK', 'Material Remark', 'html');
        $grid->addColumn('action', 'Action', 'html', NULL, false, 'ID');

        $active_job = $this->Mrm_model->selectRaw('*,'
                . "'<a href=\"#\" onclick=\"longtextswal(' + CAST(AUFNR AS VARCHAR) + ')\">' + KTEXT + '</a>' AS KTEXT_LINK,"
                . "'<a href=\"" . base_url() . "projects/' + (CASE WHEN CARD_TYPE = 'JC' THEN 'crud_jobcard/' ELSE 'crud_mdr/' END) + CAST(REVNR AS VARCHAR) + '/' + CAST(AUFNR AS VARCHAR) + '/MRM\">' + CAST(AUFNR AS VARCHAR) + '</a>' AS AUFNR_LINK")
                ->where('REVNR', $this->session->userdata('REVNR'))
                ->where('IS_ACTIVE', 1)
                ->orderBy('ID', 'ASC');

        if ($this->session->userdata('jc_id') != NULL) {
            $active_job = $active_job->where('AUFNR', $this->session->userdata('jc_id'));
        }

        $totalUnfiltered = $active_job->count();
        $total = $totalUnfiltered;

        $page = 0;
        if (isset($_GET['page']) && is_numeric($_GET['page']))
            $page = (int) $_GET['page'];


        $rowByPage = 100; // $totalUnfiltered; // 50;

        $from = ($page - 1) * $rowByPage;

        if (isset($_GET['filter']) && $_GET['filter'] != "") {
            $filter = $_GET['filter'];
            $active_job = $active_job
                    ->where(function($active_job) use ($filter) {
                $active_job->where('CARD_TYPE', 'like', '%' . $filter . '%')
                ->orWhere('MATNR', 'like', '%' . $filter . '%')
                ->orWhere('ALTERNATE_PART_NUMBER', 'like', '%' . $filter . '%')
                ->orWhere('MAKTX', 'like', '%' . $filter . '%')
                ->orWhere('AUFNR', 'like', '%' . $filter . '%')
                ->orWhere('KTEXT', 'like', '%' . $filter . '%')
                ->orWhere('MTART', 'like', '%' . $filter . '%')
                ->orWhere('IPC', 'like', '%' . $filter . '%')
                ->orWhere('STO_NUMBER', 'like', '%' . $filter . '%')
                ->orWhere('OUTBOND_DELIV', 'like', '%' . $filter . '%')
                ->orWhere('TO_NUM', 'like', '%' . $filter . '%')
                ->orWhere('CUST_JC_NUM', 'like', '%' . $filter . '%')
                ->orWhere('MRM_ISSUE_DATE', 'like', '%' . $filter . '%')
                ->orWhere('MENGE', 'like', '%' . $filter . '%')
                ->orWhere('TOTAL_QTY', 'like', '%' . $filter . '%')
                ->orWhere('MEINS', 'like', '%' . $filter . '%')
                ->orWhere('STOCK_MANUAL', 'like', '%' . $filter . '%')
                ->orWhere('LGORT', 'like', '%' . $filter . '%')
                ->orWhere('MATERIAL_FULFILLMENT_STATUS', 'like', '%' . $filter . '%')
                ->orWhere('FULLFILLMENT_STATUS_DATE', 'like', '%' . $filter . '%')
                ->orWhere('FULLFILLMENT_REMARK', 'like', '%' . $filter . '%')
                ->orWhere('PO_DATE', 'like', '%' . $filter . '%')
                ->orWhere('PO_NUM', 'like', '%' . $filter . '%')
                ->orWhere('AWB_NUMBER', 'like', '%' . $filter . '%')
                ->orWhere('AWB_DATE', 'like', '%' . $filter . '%')
                ->orWhere('QTY_DELIVERED', 'like', '%' . $filter . '%')
                ->orWhere('QTY_REMAIN', 'like', '%' . $filter . '%')
                ->orWhere('MATERIAL_REMARK', 'like', '%' . $filter . '%');
            });
            $total = $active_job->count();
        }

        if (isset($_GET['sort']) && $_GET['sort'] != "") {
            $active_job = $active_job->orderBy($_GET['sort'], $_GET['asc'] == "0" ? "DESC" : "ASC");
        } else {
            $active_job = $active_job->orderBy('AUFNR', 'ASC');
        }

        $active_job = $active_job->skip($from)->take($rowByPage);
        if ($total > 0 && $rowByPage > 0) {
            $grid->setPaginator(ceil($total / $rowByPage), (int) $total, (int) $totalUnfiltered, null);
        }
        $grid->renderJSON($active_job->get(), false, false, !isset($_GET['data_only']));
    }

    public function crud_mrm_add() {
        $response["status"] = NULL;
        $response["body"] = NULL;

        date_default_timezone_set('Asia/Jakarta');
        try {
            $mrm = $this->Mrm_model;
            $mrm->REVNR = $this->session->userdata('REVNR');
            $mrm->CARD_TYPE = $_POST['CARD_TYPE'];
            $mrm->MATNR = $_POST['MATNR'];
            $mrm->ALTERNATE_PART_NUMBER = $_POST['ALT_PART_NUMBER'];
            $mrm->MAKTX = $_POST['MAKTX'];
            $mrm->AUFNR = $_POST['AUFNR'];
            $mrm->KTEXT = $_POST['KTEXT'];
            $mrm->MTART = $_POST['CTG'];
            $mrm->IPC = $_POST['IPC'];
            $mrm->STO_NUMBER = $_POST['STO_NUMBER'];
            $mrm->OUTBOND_DELIV = $_POST['OUTBOND_DELIV'];
            $mrm->TO_NUM = $_POST['TO_NUM'];
            $mrm->CUST_JC_NUM = $_POST['CUST_JC_NUM'];
            $mrm->MRM_ISSUE_DATE = date('Y-m-d H:i:s');
            $mrm->MENGE = $_POST['MENGE'];
            // $mrm->TOTAL_QTY = $_POST['TOTAL_QTY']; by System
            $mrm->MEINS = $_POST['MEINS'];
            $mrm->STOCK_MANUAL = $_POST['STOCK_MANUAL'];
            $mrm->LGORT = $_POST['LGORT'];
            $mrm->MATERIAL_FULFILLMENT_STATUS = $_POST['MATERIAL_FULFILLMENT_STATUS'];
            $mrm->FULLFILLMENT_STATUS_DATE = date('Y-m-d H:i:s');
            $mrm->FULLFILLMENT_REMARK = $_POST['FULLFILLMENT_REMARK'];
            $mrm->PO_DATE = $_POST['PO_DATE'];
            $mrm->PO_NUM = $_POST['PO_NUM'];
            $mrm->AWB_NUMBER = $_POST['AWB_NUMBER'];
            $mrm->AWB_DATE = $_POST['AWB_DATE'];
            $mrm->QTY_DELIVERED = $_POST['QTY_DELIVERED'];
            $mrm->QTY_REMAIN = $_POST['QTY_REMAIN'];
            $mrm->MATERIAL_REMARK = $_POST['MATERIAL_REMARK'];
            $mrm->IS_ACTIVE = 1;
            $mrm->save();
            if ($mrm) {
                $mrm->update([
                    'TOTAL_QTY' => $this->Mrm_model
                        ->selectRaw('SUM(CAST(MENGE AS FLOAT)) AS MENGE')
                        ->where('MATNR', $mrm->first()->MATNR)
                        ->where('REVNR', $mrm->first()->REVNR)
                        ->first()
                        ->MENGE
                        // Mungkin bermasalah, peraiki
                ]);
                $response["status"] = "success";
                $response["body"] = NULL;
            } else {
                $response["status"] = "error";
                $response["body"] = NULL;
            }
        } catch (Exception $e) {
            error_log($e);
            echo $e;
        }

        header('Content-Type: application/json');
        echo json_encode($response);
    }

    function crud_mrm_after_insert($post_array, $primary_key) {
        date_default_timezone_set('Asia/Jakarta');
        $mrm_update = array(
            "id_project" => $this->session->userdata('id_project'),
            'date_created' => date('Y-m-d H:i:s'),
            'MRM_Issue_Date' => date('Y-m-d H:i:s')
        );
        $this->db->update('mrm', $mrm_update, array('id' => $primary_key));
        return true;
    }

    function crud_mrm_no_add_field_callback() {
        error_reporting(E_ALL & ~E_NOTICE);
        $last_no = 0;
        try {
            $query = $this->Mrm_model
                            ->where('id_project', $this->session->userdata('id_project'))
                            ->orderBy('no_', 'DESC')
                            ->first()->no_;
            // $last = $query->seq;
            $last_no = $query + 1;
        } catch (Exception $e) {
            $last_no = 1;
        }
        return '<input type="text" value="' . $last_no . '" name="no_">';
    }

    public function crud_mrm_update() {
        $data["session"] = $this->session->userdata('logged_in');

        $colname = strip_tags($_POST['colname']);
        $id = strip_tags($_POST['id']);
        $coltype = strip_tags($_POST['coltype']);
        $value = strip_tags($_POST['newvalue']);
        

        if ($coltype == 'date') {
            if ($value === "") {
                $value = NULL;
            } else {
                $date_info = date_parse_from_format('d/m/Y', $value);
                $value = "{$date_info['year']}-{$date_info['month']}-{$date_info['day']}";
            }
        }

        try {
            $mrm = $this->Mrm_model->where('ID', $id);
            if ($colname == 'MENGE') {
                $mrm->update([
                        $colname => $value
                    ]);
                $mrm->update([
                        'TOTAL_QTY' => $this->Mrm_model
                            ->selectRaw('SUM(CAST(MENGE AS FLOAT)) AS MENGE')
                            ->where('MATNR', $mrm->first()->MATNR)
                            ->where('REVNR', $mrm->first()->REVNR)
                            ->first()
                            ->MENGE
                            // Mungkin bermasalah, peraiki
                    ]);
            } else {
                $mrm->update([$colname => $value]);
                echo $mrm ? "ok" : "error";
            }
        } catch (Exception $exc) {
            echo $exc;
        } finally {
            if ($colname == 'MATERIAL_FULFILLMENT_STATUS') {

                $this->setOrderStatus($id);

                date_default_timezone_set('Asia/Jakarta');
                $this->Mrm_model
                        ->where('ID', $id)
                        ->update(['FULLFILLMENT_STATUS_DATE' => date('Y-m-d H:i:s')]);

                if ($value == 'WAITING Customer Supply') {
                    // echo $value;
                    // die();
                    $this->insert_csr_after_mrm_updated($id);
                } else {
                    $this->remove_csr_by_mrm_id($id);
                }
            }
        }
    }

    public function setOrderStatus($mrm_id) {
        date_default_timezone_set('Asia/Jakarta');
        try {
            $mrm = $this->Mrm_model->where('ID', $mrm_id)->first();
            $AUFNR = $mrm->AUFNR;
            $mrm = $this->Mrm_model->select("MATERIAL_FULFILLMENT_STATUS")->where('AUFNR', $AUFNR)->get()->toArray();
            $mrm_status = array_column($mrm, 'MATERIAL_FULFILLMENT_STATUS');
            $mrm_unique_status = array_unique($mrm_status);
            $COMPLETED = array("DLVR FULL to Production", "PRELOADED in Hangar Store");
            $order = $this->Jobcard_model->where('AUFNR', $AUFNR);

            
            if ( count($mrm_unique_status) == 1 ) {
                if( 
                    in_array('DLVR FULL to Production', array_unique($mrm_unique_status)) || 
                    in_array('PRELOADED in Hangar Store', array_unique($mrm_unique_status))
                ) {
                    $order->update([
                        'MAT_FULLFILLMENT_STATUS' => 'COMPLETED',
                        'FULLFILLMENT_STATUS_DATE' => date('Y-m-d H:i:s')
                    ]);
                } else {
                    $order->update([
                        'MAT_FULLFILLMENT_STATUS' => $mrm_unique_status[0],
                        'FULLFILLMENT_STATUS_DATE' => date('Y-m-d H:i:s')
                    ]);
                }
            } elseif ( count($mrm_unique_status) > 1 ) {
                if(count(array_diff($mrm_unique_status, $COMPLETED)) == 0){
                    $order->update([
                        'MAT_FULLFILLMENT_STATUS' => 'COMPLETED',
                        'FULLFILLMENT_STATUS_DATE' => date('Y-m-d H:i:s')
                    ]);
                } elseif( 
                    in_array('DLVR FULL to Production', array_unique($mrm_unique_status)) ||
                    in_array('PRELOADED in Hangar Store', array_unique($mrm_unique_status)) 
                    ) {
                    $order->update([
                        'MAT_FULLFILLMENT_STATUS' => 'PARTIAL',
                        'FULLFILLMENT_STATUS_DATE' => date('Y-m-d H:i:s')
                    ]);
                } else {
                    $order->update([
                        'MAT_FULLFILLMENT_STATUS' => 'NOT COMPLETE',
                        'FULLFILLMENT_STATUS_DATE' => date('Y-m-d H:i:s')
                    ]);
                }
            }

            if ($order) {
                echo "success";
            } else {
                echo "error";
            }

            // echo "<pre>";
            // print_r($mrm_unique_status);

            


            // $is_completed = 1;
            // $mrm = $this->Mrm_model->select("MATERIAL_FULFILLMENT_STATUS")->where('AUFNR', $AUFNR)->get();
            // foreach ($mrm as $value) {
            //     if ($value->MATERIAL_FULFILLMENT_STATUS != 'DLVR FULL to Production') {
            //         $is_completed = 0;
            //         break;
            //     } else {
            //         $is_completed = 1;
            //     }
            // }

            // if ($is_completed) {
            //     $order = $this->Mdr_model
            //             ->where('AUFNR', $AUFNR)
            //             ->update([
            //         'MAT_FULLFILLMENT_STATUS' => 'COMPLETED',
            //         'FULLFILLMENT_STATUS_DATE' => date('Y-m-d H:i:s')
            //     ]);

            //     if ($order) {
            //         echo "success";
            //     } else {
            //         echo "error";
            //     }
            // } else {
            //     $order = $this->Mdr_model
            //             ->where('AUFNR', $AUFNR)
            //             ->update([
            //         'MAT_FULLFILLMENT_STATUS' => 'PARTIAL',
            //         'FULLFILLMENT_STATUS_DATE' => date('Y-m-d H:i:s')
            //     ]);

            //     if ($order) {
            //         echo "success";
            //     } else {
            //         echo "error";
            //     }
            // }
        } catch (Exception $e) {
            echo $e;
        }
    }

    public function crud_mrm_delete() {
        try {
            if ($this->uri->segment(4)) {
                $this->session->set_userdata('id_project', $this->uri->segment(4));
            }
            $data['id_project'] = $this->session->userdata('id_project');
            $active_prj = $this->VProject_model->find($data['id_project']);
            if ($active_prj) {
                $data['project_name'] = $active_prj->project_name;
            }
        } catch (Exception $e) {
            error_log($e);
        }

        $data["session"] = $this->session->userdata('logged_in');

        $id = strip_tags($_POST['id']);
        // $tablename = strip_tags($_POST['tablename']);

        try {
            $this->Mrm_model->destroy($id);
        } catch (Exception $exc) {
            echo "error";
        } finally {
            echo "ok";
        }
    }

    public function crud_prm() {
        try {
            if ($this->uri->segment(4)) {
                $this->session->set_userdata('REVNR', $this->uri->segment(4));
            }
            $data['id_project'] = $this->session->userdata('REVNR');
            $active_prj = $this->Project_model_ci->getProjectName($data['id_project']);
            if ($active_prj) {
                $data['project_name'] = $active_prj->REVTX;
            }
        } catch (Exception $e) {
            error_log($e);
        }

        $data["session"] = $this->session->userdata('logged_in');
        $data['content'] = 'admin/projects/crud_prm';

        $this->load->view('template', $data);
    }

    public function check_duplicate_prm() {
        $response["status"] = NULL;
        $response["body"] = NULL;
        try {
            $crm = $this->Prm_model
                    ->where('AUFNR', $_POST['AUFNR'])
                    ->where('SN_QTY', $_POST['SN_QTY'])
                    ->where('REVNR', $_POST['REVNR'])
                    ->count();

            if ($crm > 0) {
                $response["status"] = 'error';
            } else {
                $response["status"] = 'success';
            }
        } catch (Exception $e) {
            $response["status"] = 'failed';
            $response["body"] = $e;
        }
        header('Content-Type: application/json');
        echo json_encode($response);
    }

    public function crud_prm_load() {
        $data["session"] = $this->session->userdata('logged_in');

        $grid = new EditableGrid();

        $grid->addColumn('SEQ_NUM', 'No', 'integer', NULL, false);
        $grid->addColumn('PART_NUMBER', 'Part Number', 'string', NULL, false);
        $grid->addColumn('DESCRIPTION', 'Description', 'html', NULL, false);
        $grid->addColumn('CTG', 'CTG', 'string', NULL, false);
        $grid->addColumn('SN_QTY', 'SN/Qty', 'string', NULL, false);
        $grid->addColumn('POST', 'Post', 'string');
        $grid->addColumn('AUFNR', 'ORDER REFF', 'html', NULL, false);
        $grid->addColumn('PART_STATUS', 'Part Status', 'string');
        $grid->addColumn('PL_SP_OUT_NO', 'P/L or SP (Out) No', 'string');
        $grid->addColumn('PL_SP_OUT_DATE', 'P/L or SP (Out) Date', 'date');
        $grid->addColumn('PL_SP_IN_NO', 'P/L or SP (In) No', 'string');
        $grid->addColumn('PL_SP_IN_DATE', 'P/L or SP (In) Date', 'date');
        $grid->addColumn('UNIT_SEND', 'Unit Send', 'string');
        $grid->addColumn('UNIT_RECV', 'Unit Recv.', 'string');
        $grid->addColumn('PART_LOC', 'Part Location.', 'string');
        $grid->addColumn('REMARK', 'Remark', 'html');
        $grid->addColumn('action', 'Action', 'html', NULL, false, 'ID');

        $data['id_project'] = $this->session->userdata('REVNR');
        $active_job = $this->Prm_model->selectRaw('ROW_NUMBER () OVER (ORDER BY ID ) AS SEQ_NUM, *,'
                        . "'<a href=\"" . base_url() . "projects/crud_mrm/' + REVNR + '/' + AUFNR + '\">' + AUFNR + '</a>' AS AUFNR_LINK"
                )
                ->where('REVNR', $data['id_project'])
                ->where('IS_ACTIVE', 1);

        $totalUnfiltered = $active_job->count();
        $total = $totalUnfiltered;

        $page = 0;
        if (isset($_GET['page']) && is_numeric($_GET['page']))
            $page = (int) $_GET['page'];


        $rowByPage = $totalUnfiltered; // 50;

        $from = ($page - 1) * $rowByPage;

        if (isset($_GET['filter']) && $_GET['filter'] != "") {
            $filter = $_GET['filter'];
            $active_job = $active_job
                    ->where(function($active_job) use($filter){
                        $active_job->where('PART_NUMBER', 'like', '%' . $filter . '%')
                        ->orwhere('DESCRIPTION', 'like', '%' . $filter . '%')
                        ->orwhere('CTG', 'like', '%' . $filter . '%')
                        ->orwhere('SN_QTY', 'like', '%' . $filter . '%')
                        ->orwhere('POST', 'like', '%' . $filter . '%')
                        ->orwhere('AUFNR', 'like', '%' . $filter . '%')
                        ->orwhere('PART_STATUS', 'like', '%' . $filter . '%')
                        ->orwhere('PL_SP_OUT_NO', 'like', '%' . $filter . '%')
                        ->orwhere('PL_SP_OUT_DATE', 'like', '%' . $filter . '%')
                        ->orwhere('PL_SP_IN_NO', 'like', '%' . $filter . '%')
                        ->orwhere('PL_SP_IN_DATE', 'like', '%' . $filter . '%')
                        ->orwhere('UNIT_SEND', 'like', '%' . $filter . '%')
                        ->orwhere('UNIT_RECV    ', 'like', '%' . $filter . '%')
                        ->orwhere('PART_LOC', 'like', '%' . $filter . '%')
                        ->orwhere('REMARK', 'like', '%' . $filter . '%');
                    });
            $total = $active_job->count();
        }

        if (isset($_GET['sort']) && $_GET['sort'] != "" && in_array($_GET['sort'], $grid->getColumnFields())) {
            $active_job = $active_job->orderBy($_GET['sort'], $_GET['asc'] == "0" ? "DESC" : "ASC");
        } else {
            $active_job = $active_job->orderBy('NO_', 'ASC');
        }

        $active_job = $active_job->skip($from)->take($rowByPage);

        if ($total > 0) {
            $grid->setPaginator(ceil($total / $rowByPage), (int) $total, (int) $totalUnfiltered, null);
        }

        $grid->renderJSON($active_job->get(), false, false, !isset($_GET['data_only']));
    }

    public function crud_prm_add() {
        date_default_timezone_set('Asia/Jakarta');

        $msg["status"] = NULL;
        $msg["body"] = NULL;

        $ORDER = $_POST['AUFNR'];
        $SERIAL_NUMBER = $_POST['SN_QTY'];
        $REVNR = $_POST['REVNR'];

        try {
            $prm = $this->Prm_model->updateOrCreate(
                    ['AUFNR' => $ORDER, 'SN_QTY' => $SERIAL_NUMBER, 'REVNR' => $REVNR], [
                'REVNR' => $REVNR,
                'PART_NUMBER' => $_POST['MATNR'],
                'DESCRIPTION' => $_POST['MAKTX'],
                'CTG' => $_POST['CTG'],
                'SN_QTY' => $_POST['SN_QTY'],
                'POST' => $_POST['POST'],
                'PL_SP_OUT_NO' => $_POST['SP_OUT_NO'],
                'PL_SP_OUT_DATE' => $_POST['SP_OUT_DATE'],
                'PL_SP_IN_NO' => $_POST['SP_IN_NO'],
                'PL_SP_IN_DATE' => $_POST['SP_IN_DATE'],
                'UNIT_SEND' => $_POST['UNIT_SEND'],
                'UNIT_RECV' => $_POST['UNIT_RECV'],
                'PART_LOC' => $_POST['PART_LOC'],
                'REMARK' => $_POST['REMARK'],
                'PART_STATUS' => $_POST['STATUS'],
                'AUFNR' => $_POST['AUFNR'],
                'IS_ACTIVE' => 1
                    ]
            );

            if ($prm) {
                $msg["status"] = "success";
                $msg["body"] = "Prm has been saved";
            } else {
                $msg["status"] = "error";
                $msg["body"] = "Prm has been saved";
            }
        } catch (Exception $e) {
            $msg["status"] = "failed";
            $msg["body"] = $e;
        }

        header('Content-Type: application/json');
        echo json_encode($msg);
    }

    function crud_prm_after_insert($post_array, $primary_key) {
        date_default_timezone_set('Asia/Jakarta');
        $prm_update = array(
            "REVNR" => $this->session->userdata('REVNR'),
            'DATE_CREATED' => date('Y-m-d H:i:s')
        );
        $this->db->update('PRM', $prm_update, array('ID' => $primary_key));
        return true;
    }

    function crud_prm_no_add_field_callback() {
        error_reporting(E_ALL & ~E_NOTICE);
        $last_no = 0;
        try {
            $query = $this->Prm_model
                            ->where('REVNR', $this->session->userdata('REVNR'))
                            ->orderBy('NO_', 'DESC')
                            ->first()->NO_;
            // $last = $query->seq;
            $last_no = $query + 1;
        } catch (Exception $e) {
            error_log($e);
            $last_no = 1;
        }
        return '<input type="text" value="' . $last_no . '" name="NO_">';
    }

    public function crud_prm_update() {
        try {
            if ($this->uri->segment(4)) {
                $this->session->set_userdata('id_project', $this->uri->segment(4));
            }
            $data['id_project'] = $this->session->userdata('REVNR');
            $active_prj = $this->VProject_model->find($data['id_project']);
            if ($active_prj) {
                $data['project_name'] = $active_prj->REVTX;
            }
        } catch (Exception $e) {
            error_log($e);
        }

        $data["session"] = $this->session->userdata('logged_in');

        $colname = strip_tags($_POST['colname']);
        $id = strip_tags($_POST['id']);
        $coltype = strip_tags($_POST['coltype']);
        $value = strip_tags($_POST['newvalue']);
        // $tablename = strip_tags($_POST['tablename']);

        if ($coltype == 'date') {
            if ($value === "") {
                $value = NULL;
            } else {
                $date_info = date_parse_from_format('d/m/Y', $value);
                $value = "{$date_info['year']}-{$date_info['month']}-{$date_info['day']}";
            }
        }

        try {
            $this->Prm_model->where('ID', $id)->update([$colname => $value]);
        } catch (Exception $exc) {
            echo "error";
        } finally {
            echo "ok";
        }
    }

    public function crud_prm_delete() {
        try {
            if ($this->uri->segment(4)) {
                $this->session->set_userdata('id_project', $this->uri->segment(4));
            }
            $data['id_project'] = $this->session->userdata('REVNR');
            $active_prj = $this->VProject_model->find($data['id_project']);
            if ($active_prj) {
                $data['project_name'] = $active_prj->REVTX;
            }
        } catch (Exception $e) {
            error_log($e);
        }

        $data["session"] = $this->session->userdata('logged_in');

        $id = strip_tags($_POST['id']);
        // $tablename = strip_tags($_POST['tablename']);

        try {
            $this->Prm_model->destroy($id);
        } catch (Exception $exc) {
            echo "error";
        } finally {
            echo "ok";
        }
    }

    public function crud_crm() {
        try {
            if ($this->uri->segment(4)) {
                $this->session->set_userdata('REVNR', $this->uri->segment(4));
            }
            $data['id_project'] = $this->session->userdata('REVNR');
            $active_prj = $this->Project_model_ci->getProjectName($data['id_project']);
            if ($active_prj) {
                $data['project_name'] = $active_prj->REVTX;
            }
        } catch (Exception $e) {
            error_log($e);
        }


        $data["session"] = $this->session->userdata('logged_in');
        $data['content'] = 'admin/projects/crud_crm';

        $this->load->view('template', $data);
    }

    public function check_duplicate_crm() {
        $response["status"] = NULL;
        $response["body"] = NULL;
        try {
            $crm = $this->Crm_model
                    ->where('REFF', $_POST['AUFNR'])
                    ->where('SN_QTY', $_POST['SN_QTY'])
                    ->where('REVNR', $_POST['REVNR'])
                    ->count();

            if ($crm > 0) {
                $response["status"] = 'error';
            } else {
                $response["status"] = 'success';
            }
        } catch (Exception $e) {
            $response["status"] = 'failed';
            $response["body"] = $e;
        }
        header('Content-Type: application/json');
        echo json_encode($response);
    }

    public function crud_crm_load() {
        $data["session"] = $this->session->userdata('logged_in');

        $grid = new EditableGrid();

        $grid->addColumn('SEQ_NUM', 'No', 'integer', NULL, false);
        $grid->addColumn('PART_NUMBER', 'Part Number', 'string', NULL, false);
        $grid->addColumn('DESCRIPTION', 'Description', 'string', NULL, false);
        $grid->addColumn('CTG', 'CTG', 'string', NULL, false);
        $grid->addColumn('SN_QTY', 'SN', 'string', NULL, false);
        $grid->addColumn('POST', 'Pos', 'string');
        $grid->addColumn('REFF', 'Order', 'string', NULL, false);
        $grid->addColumn('SP_OUT_NO', 'SP (Out) No', 'string');
        $grid->addColumn('SP_OUT_DATE', 'SP (Out) Date', 'date');
        $grid->addColumn('SP_IN_NO', 'SP (In) No', 'string');
        $grid->addColumn('SP_IN_DATE', 'SP (In) Date', 'date');
        $grid->addColumn('UNIT_SEND', 'Unit Send', 'string');
        $grid->addColumn('UNIT_RECV', 'Unit Recv.', 'string');
        $grid->addColumn('PART_LOC', 'Part Location.', 'string');
        $grid->addColumn('REMARK', 'Remark', 'string');
        $grid->addColumn('STATUS', 'Status', 'string', array("UR - Und. repr" => "UR - Und. repr", "SB - Serviceabl" => "SB - Serviceabl", "RA - Return as is" => "RA - Return as is", "CR - Sent to Contracr" => "CR - Sent to Contracr", "C - Close" => "C - Close"));
        $grid->addColumn('action', 'Action', 'html', NULL, false, 'ID');

        $data['id_project'] = $this->session->userdata('REVNR');
        $active_job = $this->Crm_model
            ->selectRaw("ROW_NUMBER () OVER (ORDER BY ID ) AS SEQ_NUM, *")
            ->where('REVNR', $data['id_project'])
            ->where('IS_ACTIVE', 1);

        $totalUnfiltered = $active_job->count();
        $total = $totalUnfiltered;

        $page = 0;
        if (isset($_GET['page']) && is_numeric($_GET['page']))
            $page = (int) $_GET['page'];


        $rowByPage = $totalUnfiltered; // 50;

        $from = ($page - 1) * $rowByPage;

        if (isset($_GET['filter']) && $_GET['filter'] != "") {
            $filter = $_GET['filter'];
            $active_job = $active_job
                    ->where(function($active_job) use($filter){
                        $active_job->where('PART_NUMBER', 'like', '%' . $filter . '%')
                        ->orwhere('DESCRIPTION', 'like', '%' . $filter . '%')
                        ->orwhere('CTG', 'like', '%' . $filter . '%')
                        ->orwhere('SN_QTY', 'like', '%' . $filter . '%')
                        ->orwhere('POST', 'like', '%' . $filter . '%')
                        ->orwhere('REFF', 'like', '%' . $filter . '%')
                        ->orwhere('SP_OUT_NO', 'like', '%' . $filter . '%')
                        ->orwhere('SP_OUT_DATE', 'like', '%' . $filter . '%')
                        ->orwhere('SP_IN_NO', 'like', '%' . $filter . '%')
                        ->orwhere('SP_IN_DATE', 'like', '%' . $filter . '%')
                        ->orwhere('UNIT_SEND', 'like', '%' . $filter . '%')
                        ->orwhere('UNIT_RECV', 'like', '%' . $filter . '%')
                        ->orwhere('PART_LOC', 'like', '%' . $filter . '%')
                        ->orwhere('REMARK', 'like', '%' . $filter . '%')
                        ->orwhere('STATUS', 'like', '%' . $filter . '%');
                    });
            $total = $active_job->count();
        }

        if (isset($_GET['sort']) && $_GET['sort'] != "" && in_array($_GET['sort'], $grid->getColumnFields())) {
            $active_job = $active_job->orderBy($_GET['sort'], $_GET['asc'] == "0" ? "DESC" : "ASC");
        } else {
            $active_job = $active_job->orderBy('ID', 'ASC');
        }

        $active_job = $active_job->skip($from)->take($rowByPage);
        if ($total > 0) {
            $grid->setPaginator(ceil($total / $rowByPage), (int) $total, (int) $totalUnfiltered, null);
        }
        

        $grid->renderJSON($active_job->get(), false, false, !isset($_GET['data_only']));
    }

    public function crud_crm_add() {
        $msg["status"] = NULL;
        $msg["body"] = NULL;

        $ORDER = $_POST['AUFNR'];
        $SERIAL_NUMBER = $_POST['SN_QTY'];
        $REVNR = $_POST['REVNR'];

        try {
            $crm = $this->Crm_model->updateOrCreate(
                    ['REFF' => $ORDER, 'SN_QTY' => $SERIAL_NUMBER, 'REVNR' => $REVNR], [
                'REVNR' => $REVNR,
                'PART_NUMBER' => $_POST['MATNR'],
                'DESCRIPTION' => $_POST['MAKTX'],
                'CTG' => $_POST['CTG'],
                'SN_QTY' => $_POST['SN_QTY'],
                'POST' => $_POST['POST'],
                'SP_OUT_NO' => $_POST['SP_OUT_NO'],
                'SP_OUT_DATE' => $_POST['SP_OUT_DATE'],
                'SP_IN_NO' => $_POST['SP_IN_NO'],
                'SP_IN_DATE' => $_POST['SP_IN_DATE'],
                'UNIT_SEND' => $_POST['UNIT_SEND'],
                'UNIT_RECV' => $_POST['UNIT_RECV'],
                'PART_LOC' => $_POST['PART_LOC'],
                'REMARK' => $_POST['REMARK'],
                'STATUS' => $_POST['STATUS'],
                'REFF' => $_POST['AUFNR'],
                'IS_ACTIVE' => 1
                    ]
            );

            $msg["status"] = "success";
            $msg["body"] = "User has been saved";
        } catch (Exception $e) {
            $msg["status"] = "failed";
            $msg["body"] = $e;
        }

        header('Content-Type: application/json');
        echo json_encode($msg);
    }

    function crud_crm_after_insert($post_array, $primary_key) {
        date_default_timezone_set('Asia/Jakarta');
        $prm_update = array(
            "REVNR" => $this->session->userdata('REVNR'),
            'DATE_CREATED' => date('Y-m-d H:i:s')
        );
        $this->db->update('CRM', $prm_update, array('ID' => $primary_key));
        return true;
    }

    function crud_crm_no_add_field_callback() {
        error_reporting(E_ALL & ~E_NOTICE);
        $last_no = 0;
        try {
            $query = $this->Crm_model
                            ->where('REVNR', $this->session->userdata('REVNR'))
                            ->orderBy('NO_', 'DESC')
                            ->first()->NO_;
            // $last = $query->seq;
            $last_no = $query + 1;
        } catch (Exception $e) {
            error_log($e);
            $last_no = 1;
        }
        return '<input type="text" value="' . $last_no . '" name="NO_">';
    }

    public function crud_crm_update() {
        try {
            if ($this->uri->segment(4)) {
                $this->session->set_userdata('id_project', $this->uri->segment(4));
            }
            $data['id_project'] = $this->session->userdata('REVNR');
            $active_prj = $this->VProject_model->find($data['id_project']);
            if ($active_prj) {
                $data['project_name'] = $active_prj->REVTX;
            }
        } catch (Exception $e) {
            error_log($e);
        }

        $data["session"] = $this->session->userdata('logged_in');

        $colname = strip_tags($_POST['colname']);
        $id = strip_tags($_POST['id']);
        $coltype = strip_tags($_POST['coltype']);
        $value = strip_tags($_POST['newvalue']);
        // $tablename = strip_tags($_POST['tablename']);

        if ($coltype == 'date') {
            if ($value === "") {
                $value = NULL;
            } else {
                $date_info = date_parse_from_format('d/m/Y', $value);
                $value = "{$date_info['year']}-{$date_info['month']}-{$date_info['day']}";
            }
        }

        try {
            $this->Crm_model->where('ID', $id)->update([$colname => $value]);
        } catch (Exception $exc) {
            echo "error";
        } finally {
            echo "ok";
        }
    }

    public function crud_crm_delete() {
        try {
            if ($this->uri->segment(4)) {
                $this->session->set_userdata('id_project', $this->uri->segment(4));
            }
            $data['id_project'] = $this->session->userdata('id_project');
            $active_prj = $this->VProject_model->find($data['id_project']);
            if ($active_prj) {
                $data['project_name'] = $active_prj->project_name;
            }
        } catch (Exception $e) {
            error_log($e);
        }

        $data["session"] = $this->session->userdata('logged_in');

        $id = strip_tags($_POST['id']);
        // $tablename = strip_tags($_POST['tablename']);

        try {
            $this->Crm_model->destroy($id);
        } catch (Exception $exc) {
            echo "error";
        } finally {
            echo "ok";
        }
    }

    public function insert_csr_after_mrm_updated($id_mrm) {

        // Cek Exist Record CSR 
        $is_csr_exist = $this->Csr_model->selectRaw("*")->where("ID_", $id_mrm)->count();
        if ($is_csr_exist) {
            $this->Csr_model->where('ID_', $id_mrm)->delete();
        }

        $mrm = $this->Mrm_model->selectRaw("*")->where('id', $id_mrm)->first();
        if ($mrm != null) {
            $csr = $this->Csr_model;
            $csr->REVNR = $mrm->REVNR;
            $csr->AUFNR = $mrm->AUFNR;
            $csr->MATNR = $mrm->MATNR;
            $csr->MAT_FULLFILLMENT_STATUS = $mrm->MAT_FULLFILLMENT_STATUS;
            $csr->ID_ = $mrm->ID;
            $csr->SEQ_NUM = $mrm->ID;
            $csr->REMARK = $mrm->REMARK;
            $csr->BDMNG = $mrm->MENGE;
            $csr->TOTAL_QTY = $mrm->TOTAL_QTY;
            $csr->MAKTX = $mrm->MAKTX;
            $csr->KTEXT = $mrm->KTEXT;
            $csr->CARD_TYPE = $mrm->CARD_TYPE;
            $csr->save();
        }
    }

    public function remove_csr_by_mrm_id($id_mrm) {
        $is_csr_exist = $this->Csr_model->selectRaw("*")->where("ID_", $id_mrm)->count();
        if ($is_csr_exist) {
            $this->Csr_model->where('ID_', $id_mrm)->delete();
        }
    }

    public function crud_customer_supply() {
        try {
            if ($this->uri->segment(4)) {
                $this->session->set_userdata('REVNR', $this->uri->segment(4));
            }
            $data['id_project'] = $this->session->userdata('REVNR');
            $active_prj = $this->Project_model_ci->getProjectName($data['id_project']);
            if ($active_prj) {
                $data['project_name'] = $active_prj->REVTX;
            }
        } catch (Exception $e) {
            error_log($e);
        }

        // $this->Project_model_ci->executeCSP();


        $data["session"] = $this->session->userdata('logged_in');
        $data['content'] = 'admin/projects/crud_customer_supply';

        $this->load->view('template', $data);
    }

    public function crud_customer_supply_load() {
        $data["session"] = $this->session->userdata('logged_in');
        $grid = new EditableGrid();

        $grid->addColumn('ID', 'ID', 'integer', NULL, false);
        $grid->addColumn('ID_', 'MRM', 'integer', NULL, false);
        $grid->addColumn('MATNR', 'Part Number', 'string', NULL, false);
        $grid->addColumn('MAKTX', 'Part Description', 'string', NULL, false);
        $grid->addColumn('SERIAL_NO', 'Serial No', 'string', NULL, false);
        $grid->addColumn('BDMNG', 'Qty', 'string', NULL, false);
        $grid->addColumn('TOTAL_QTY', 'Total Qty', 'string', NULL, false);
        $grid->addColumn('AUFNR', 'Order Number', 'html', NULL, false);
        // $grid->addColumn('CUST_JC_NUM', 'AAI TaskCard', 'string' ,NULL, false);
        $grid->addColumn('CARD_TYPE', 'Type', 'string', NULL, false);
        $grid->addColumn('KTEXT', 'Discrepancies', 'string', NULL, false);
        $grid->addColumn('AWB_NUM', 'AWB Number', 'string', NULL, !$this->is_customer);
        $grid->addColumn('INBOUND_NUMBER', 'Inbound Number', 'string', NULL, !$this->is_customer);
        $grid->addColumn('COMMENT_AAI', 'Customer Comment', 'string', NULL, $this->is_customer);
        $grid->addColumn('COMMENT_GMF', 'Comment GMF', 'string', NULL, !$this->is_customer);
        $grid->addColumn('STATUS', 'Status', 'string', array("OPEN" => "OPEN", "CLOSED" => "CLOSED"), NULL, $this->is_customer);

        !$this->is_customer ? $grid->addColumn('action', 'Action', 'html', NULL, false, 'ID') : NULL ;

        $data['id_project'] = $this->session->userdata('REVNR');
        $active_job = $this->Csr_model
                ->selectRaw('*,'
                        // .'(select SUM(CAST(CAST(BDMNG AS FLOAT) as DECIMAL(4, 2))) from V_MRM WHERE MATNR = V_CSR.MATNR and ORDER_NUMBER = V_CSR.ORDER_NUMBER and MATERIAL_FULFILLMENT_STATUS = V_CSR.MATERIAL_FULFILLMENT_STATUS) as TOTAL_QTY ,'
                        . "'<a href=\"" . base_url() . "projects/' + (CASE WHEN CARD_TYPE LIKE 'MDR' THEN 'crud_mdr' ELSE 'crud_jobcard' END) + '/' + REVNR + '/' + AUFNR + '/CSP\">' + AUFNR + '</a>' AS AUFNR")
                ->where('REVNR', $data['id_project']);

        $totalUnfiltered = $active_job->count();
        $total = $totalUnfiltered;

        $page = 0;
        if (isset($_GET['page']) && is_numeric($_GET['page']))
            $page = (int) $_GET['page'];


        $rowByPage = $totalUnfiltered; // 50;

        $from = ($page - 1) * $rowByPage;

        if (isset($_GET['filter']) && $_GET['filter'] != "") {
            $filter = $_GET['filter'];
            $active_job = $active_job
                    ->where(function($active_job) use ($filter){
                        $active_job->where('SEQ_NUM', 'like', '%' . $filter . '%')
                        ->orwhere('MATNR', 'like', '%' . $filter . '%')
                        ->orWhere('MATNR', 'like', '%' . $filter . '%')
                        ->orWhere('MAKTX', 'like', '%' . $filter . '%')
                        ->orWhere('SERIAL_NO', 'like', '%' . $filter . '%')
                        ->orWhere('BDMNG', 'like', '%' . $filter . '%')
                        ->orWhere('TOTAL_QTY', 'like', '%' . $filter . '%')
                        ->orWhere('AUFNR', 'like', '%' . $filter . '%')
                        ->orWhere('CARD_TYPE', 'like', '%' . $filter . '%')
                        ->orWhere('KTEXT', 'like', '%' . $filter . '%')
                        ->orWhere('AWB_NUM', 'like', '%' . $filter . '%')
                        ->orWhere('INBOUND_NUMBER', 'like', '%' . $filter . '%')
                        ->orWhere('COMMENT_AAI', 'like', '%' . $filter . '%')
                        ->orWhere('COMMENT_GMF', 'like', '%' . $filter . '%')
                        ->orWhere('STATUS', 'like', '%' . $filter . '%');
                    });
            $total = $active_job->count();
        }

        if (isset($_GET['sort']) && $_GET['sort'] != "" && in_array($_GET['sort'], $grid->getColumnFields())) {
            $active_job = $active_job->orderBy($_GET['sort'], $_GET['asc'] == "0" ? "DESC" : "ASC");
        } else {
            $active_job = $active_job->orderBy('SEQ_NUM', 'ASC');
        }

        $active_job = $active_job->skip($from)->take($rowByPage);
        if ($total > 0) {
            $grid->setPaginator(ceil($total / $rowByPage), (int) $total, (int) $totalUnfiltered, null);
        }

        $grid->renderJSON($active_job->get(), false, false, !isset($_GET['data_only']));
    }

    public function crud_customer_supply_update() {
        $data["session"] = $this->session->userdata('logged_in');

        $colname = strip_tags($_POST['colname']);
        $id = strip_tags($_POST['id']);
        $coltype = strip_tags($_POST['coltype']);
        $value = strip_tags($_POST['newvalue']);
        // $tablename = strip_tags($_POST['tablename']);
        //$data['id_project'] = $this->session->userdata('REVNR');
        //$csr = $this->Project_model_ci->getItemCSR($id, $data['id_project']);
        //$data['aufnr'] = $csr->AUFNR;
        // $data['id'] = $csr->AUFNR;

        if ($coltype == 'date') {
            if ($value === "") {
                $value = NULL;
            } else {
                $date_info = date_parse_from_format('d/m/Y', $value);
                $value = "{$date_info['year']}-{$date_info['month']}-{$date_info['day']}";
            }
        }

        try {
            $this->Csr_model
                    //->where('AUFNR', $data['aufnr'])
                    //->where('REVNR', $this->session->userdata('REVNR'))
                    ->where('ID', $id)
                    ->update([$colname => $value]);
        } catch (Exception $exc) {
            echo "error";
            echo $exc;
        } finally {
            echo "ok";
        }
    }

    public function crud_task() {
        try {
            if ($this->uri->segment(4)) {
                $this->session->set_userdata('REVNR', $this->uri->segment(4));
            }
            $data['id_project'] = $this->session->userdata('REVNR');
            $active_prj = $this->Project_model_ci->getProjectName($data['id_project']);
            if ($active_prj) {
                $data['project_name'] = $active_prj->REVTX;
            }
        } catch (Exception $e) {
            error_log($e);
        }

        $data['is_grocery'] = true;
        $crud = new grocery_CRUD();


        $crud->set_table('task');
        $crud->where('id_project', $this->session->userdata('id_project'));
        $crud->set_subject('Task');
        $crud->columns('No_', 'Subject', 'Start_Date', 'End_Date', 'Billable_Status');
        $crud->display_as('Subject', 'Name');
        $crud->unset_fields('id_project', 'date_created', 'id');
        $crud->set_field_upload('Attachment', 'assets/uploads/files');
        $crud->callback_after_insert(array($this, 'task_after_insert'));
        $crud->callback_add_field('No_', array($this, 'crud_task_no_add_field_callback'));
        $output = $crud->render();

        $data["session"] = $this->session->userdata('logged_in');
        $data['content'] = 'admin/projects/crud_task';

        $this->load->view('template', array_merge($data, (array) $output));
    }

    public function task_after_insert($post_array, $primary_key) {
        date_default_timezone_set('Asia/Jakarta');
        $task_day_update = array(
            "date_created" => date('Y-m-d H:i:s'),
            "id_project" => $this->session->userdata('id_project'),
        );

        $this->db->update('task', $task_day_update, array('id' => $primary_key));

        return true;
    }

    function crud_task_no_add_field_callback() {
        error_reporting(E_ALL & ~E_NOTICE);
        $last_no_ = 0;
        try {
            $query = $this->db->select('No_')
                    ->from('task')
                    ->where('id_project', $this->session->userdata('id_project'))
                    ->order_by('No_', 'DESC')
                    ->limit(1)
                    ->get();
            $ret = $query->row();
            $last = $ret->No_;
            $last_no = $last + 1;
        } catch (Exception $e) {
            error_log($e);
            $last_no_ = 1;
        }
        return '<input type="text" value="' . $last_no . '" name="No_">';
    }

    function crud_ticket() {

        try {
            if ($this->uri->segment(4)) {
                $this->session->set_userdata('REVNR', $this->uri->segment(4));
            }
            $data['id_project'] = $this->session->userdata('REVNR');
            $active_prj = $this->project_model_ci->getProjectName($data['id_project']);
            if ($active_prj) {
                $data['project_name'] = $active_prj->REVTX;
            }
        } catch (Exception $e) {
            error_log($e);
        }

        $data['is_grocery'] = true;
        $crud = new grocery_CRUD();

        $crud->set_table('ticket');
        $crud->set_subject('Ticket');
        $crud->where('id_project', $this->session->userdata('id_project'));
        $crud->columns('Subject', 'Ticket_Type', 'Status', 'Attachment');
        $crud->field_type('Ticket_Type', 'dropdown', array('Low' => 'Low', 'Medium' => 'Medium', 'High' => 'High'));
        $crud->field_type('Status', 'dropdown', array('New' => 'New', 'Open Replied' => 'Open Replied', 'Open' => 'Open', 'Close' => 'Close'));
        $crud->add_fields('Subject', 'Ticket_Type', 'Description', 'Attachment');
        $crud->edit_fields('Subject', 'Ticket_Type', 'Status', 'Description', 'Attachment');
        $crud->set_field_upload('Attachment', 'assets/uploads/files');
        $crud->callback_after_insert(array($this, 'ticket_after_insert'));
        $crud->callback_after_update(array($this, 'ticket_after_update'));
        $crud->callback_column('Subject', array($this, 'callback_detail_ticket'));
        // $crud->add_action('Comment', base_url() . 'assets/dist/img/icon_comments.png', 'admin/projects/comment_ticket');
        $output = $crud->render();
        $data["session"] = $this->session->userdata('logged_in');
        $data['content'] = 'admin/projects/crud_ticket';

        $this->load->view('template', array_merge($data, (array) $output));
    }

    function ticket_after_insert($post_array, $primary_key) {
        $ticket_update = array(
            "Create_Date_Ticket" => date('Y-m-d H:i:s'),
            "Created_By" => $this->session->userdata('logged_in')['id_user'],
            "Status" => "Open",
            "id_project" => $this->session->userdata('id_project'),
        );

        $this->db->update('ticket', $ticket_update, array('Id' => $primary_key));

        return true;
    }

    public function callback_detail_ticket($value, $row) {

        return '<a href="' . base_url() . 'admin/projects/comment_ticket/' . $row->Id . '">' . $value . '</a>';
    }

    function comment_ticket() {

        try {
            if ($this->uri->segment(4)) {
                $this->session->set_userdata('ticket_id', $this->uri->segment(4));
            }
            $data['ticket_id'] = $this->session->userdata('ticket_id');
            $active_prj = $this->Ticket_model->find($data['ticket_id']);
            if ($active_prj) {
                $data['Subject'] = $active_prj->Subject;
                $data['Status'] = $active_prj->Status;
                $data['Created_By'] = $active_prj->Created_By;
                $data['Ticket_Type'] = $active_prj->Ticket_Type;
                $data['Create_Date_Ticket'] = $active_prj->Create_Date_Ticket;
                $data['Status'] = $active_prj->Status;
            }
        } catch (Exception $e) {
            error_log($e);
        }


        $data["session"] = $this->session->userdata('logged_in');
        $data['content'] = 'admin/projects/comment_ticket';

        $this->load->view('template', $data);
    }

    function ticket_after_update($post_array, $primary_key) {
        $ticket_update = array(
            "Replay_At" => date('Y-m-d H:i:s'),
            "Replay_By" => $this->session->userdata('logged_in')['id_user']
        );

        $this->db->update('ticket', $ticket_update, array('id' => $primary_key));

        return true;
    }

    public function crud_estimates() {
        try {
            if ($this->uri->segment(4)) {
                $this->session->set_userdata('id_project', $this->uri->segment(4));
            }
            $data['id_project'] = $this->session->userdata('id_project');
            $active_prj = $this->VProject_model->find($data['id_project']);
            if ($active_prj) {
                $data['project_name'] = $active_prj->project_name;
            }
        } catch (Exception $e) {
            error_log($e);
        }
        $data['is_grocery'] = true;
        $crud = new grocery_CRUD();


        $crud->set_table('estimates');
        $crud->where('id_project', $this->session->userdata('id_project'));
        $crud->set_subject('Estimate');
        $crud->columns('Estimate', 'Amount', 'Start_Date', 'Expired_Date', 'Status');
        $crud->unset_fields('id_project', 'date_created', 'id');
        // $crud->callback_column('Amount', array($this, 'valueToDollar'));
        $crud->field_type('Status', 'dropdown', array('Draft' => 'Draft', 'Sent' => 'Sent', 'Expired' => 'Expired', 'Declined' => 'Declined', 'Accepted' => 'Accepted'));
        $crud->callback_after_insert(array($this, 'estimate_after_insert'));
        $crud->callback_add_field('Estimate', array($this, 'crud_estimate_no_add_field_callback'));
        // $crud->set_rules('Expired_Date', 'Expired_Date', 'trim|callback_check_estimatedates[' . $this->input->post('Start_Date') . ']');
        $output = $crud->render();

        $data["session"] = $this->session->userdata('logged_in');
        $data['content'] = 'admin/projects/crud_estimates';

        $this->load->view('template', array_merge($data, (array) $output));
    }

    public function valueToDollar($value, $row) {
        return '$' . $value;
    }

    public function estimate_after_insert($post_array, $primary_key) {
        date_default_timezone_set('Asia/Jakarta');
        $task_day_update = array(
            "date_created" => date('Y-m-d H:i:s'),
            "id_project" => $this->session->userdata('id_project'),
        );

        $this->db->update('estimates', $task_day_update, array('id' => $primary_key));

        return true;
    }

    function crud_estimate_no_add_field_callback() {
        error_reporting(E_ALL & ~E_NOTICE);
        $last_no_ = 0;
        try {
            $query = $this->db->select('Estimate')
                    ->from('estimates')
                    ->where('id_project', $this->session->userdata('id_project'))
                    ->order_by('Estimate', 'DESC')
                    ->limit(1)
                    ->get();
            $ret = $query->row();
            $last = $ret->Estimate;
            $last_no = $last + 1;
        } catch (Exception $e) {
            $last_no_ = 1;
        }
        return '<input type="text" value="' . $last_no . '" name="Estimate">';
    }

    public function check_estimatedates($Expired_Date, $Start_Date) {
        $parts = explode('/', $this->input->post('Start_Date'));
        $Start_Date = join('-', $parts);
        $Start_Date = strtotime($Start_Date);

        $parts2 = explode('/', $this->input->post('Expired_Date'));
        $Expired_Date = join('-', $parts2);
        $Expired_Date = strtotime($Expired_Date);

        if ($Expired_Date >= $Start_Date) {

            return true;
        } else {
            $this->form_validation->set_message('check_estimatedates', "Expired date should be greater than start date");
            return false;
        }
    }

    public function crud_invoices() {
        try {
            if ($this->uri->segment(4)) {
                $this->session->set_userdata('id_project', $this->uri->segment(4));
            }
            $data['id_project'] = $this->session->userdata('id_project');
            $active_prj = $this->VProject_model->find($data['id_project']);
            if ($active_prj) {
                $data['project_name'] = $active_prj->project_name;
            }
        } catch (Exception $e) {
            error_log($e);
        }
        $data['is_grocery'] = true;
        $crud = new grocery_CRUD();


        $crud->set_table('invoices');
        $crud->where('id_project', $this->session->userdata('id_project'));
        $crud->set_subject('Invoice');
        $crud->columns('Invoice', 'Amount', 'Bill_Date', 'Due_Date', 'Status');
        $crud->unset_fields('id_project', 'date_created', 'id');
        $crud->callback_column('Amount', array($this, 'valueToDollar'));
        $crud->field_type('Status', 'dropdown', array('Paid' => 'Paid', 'Unpaid' => 'Unpaid'));
        $crud->callback_after_insert(array($this, 'invoices_after_insert'));
        $crud->callback_add_field('Invoice', array($this, 'crud_invoices_no_add_field_callback'));
        $crud->set_rules('Due_Date', 'Due_Date', 'trim|callback_check_dates[' . $this->input->post('Bill_Date') . ']');
        $output = $crud->render();

        $data["session"] = $this->session->userdata('logged_in');
        $data['content'] = 'admin/projects/crud_invoices';

        $this->load->view('template', array_merge($data, (array) $output));
    }

    public function invoices_after_insert($post_array, $primary_key) {
        date_default_timezone_set('Asia/Jakarta');
        $task_day_update = array(
            "date_created" => date('Y-m-d H:i:s'),
            "id_project" => $this->session->userdata('id_project'),
        );

        $this->db->update('invoices', $task_day_update, array('id' => $primary_key));

        return true;
    }

    function crud_invoices_no_add_field_callback() {
        error_reporting(E_ALL & ~E_NOTICE);
        $last_no_ = 0;
        try {
            $query = $this->db->select('Invoice')
                    ->from('invoices')
                    ->where('id_project', $this->session->userdata('id_project'))
                    ->order_by('Invoice', 'DESC')
                    ->limit(1)
                    ->get();
            $ret = $query->row();
            $last = $ret->Invoice;
            $last_no = $last + 1;
        } catch (Exception $e) {
            $last_no_ = 1;
        }
        return '<input type="text" value="' . $last_no . '" name="Invoice">';
    }

    public function check_dates($Due_Date, $Bill_Date) {
        $parts = explode('/', $this->input->post('Bill_Date'));
        $Bill_Date = join('-', $parts);
        $Bill_Date = strtotime($Bill_Date);

        $parts2 = explode('/', $this->input->post('Due_Date'));
        $Due_Date = join('-', $parts2);
        $Due_Date = strtotime($Due_Date);

        if ($Due_Date >= $Bill_Date) {

            return true;
        } else {
            $this->form_validation->set_message('check_dates', "due date should be greater than bill date");
            return false;
        }
    }

    public function crud_files() {
        try {
            if ($this->uri->segment(4)) {
                $this->session->set_userdata('REVNR', $this->uri->segment(4));
            }
            $data['id_project'] = $this->session->userdata('REVNR');
            $active_prj = $this->project_model_ci->getProjectName($data['id_project']);
            if ($active_prj) {
                $data['project_name'] = $active_prj->REVTX;
            }
        } catch (Exception $e) {
            error_log($e);
        }
        $data['is_grocery'] = true;
        $crud = new grocery_CRUD();


        $crud->set_table('files');
        $crud->where('id_project', $this->session->userdata('id_project'));
        $crud->set_subject('File');
        $crud->columns('No_', 'Subject', 'Description', 'Attachments_file');
        $crud->unset_fields('id_project', 'date_created', 'id');
        $crud->set_field_upload('Attachments_file', 'assets/uploads/files');
        $crud->callback_after_insert(array($this, 'file_after_insert'));

        $output = $crud->render();

        $data["session"] = $this->session->userdata('logged_in');
        $data['content'] = 'admin/projects/crud_files';

        $this->load->view('template', array_merge($data, (array) $output));
    }

    public function file_after_insert($post_array, $primary_key) {
        date_default_timezone_set('Asia/Jakarta');
        $task_day_update = array(
            "date_created" => date('Y-m-d H:i:s'),
            "id_project" => $this->session->userdata('id_project'),
        );
        $this->db->update('files', $task_day_update, array('id' => $primary_key));
        return true;
    }

    public function dashboard() {
        try {
            if ($this->uri->segment(3)) {
                $this->session->set_userdata('REVNR', $this->uri->segment(3));
            }
            $data['id_project'] = $this->session->userdata('REVNR');
            $active_prj = $this->Project_model->get_project_detail($data['id_project']);
            if ($active_prj) {
                $data['project_name'] = $active_prj->REVTX;
                $data['active_prj'] = $active_prj;
            }
        } catch (Exception $e) {
            error_log($e);
            echo $e;
        }

        $data["session"] = $this->session->userdata('logged_in');
        $data['content'] = 'admin/projects/dashboard';

        $data['projects'] = true;


        $this->load->view('template', $data);
    }

    public function dashboard_jobcard_by_status() {
        $response["status"] = NULL;
        $response["body"] = NULL;

        try {

            $REVNR = $_POST['REVNR'];
            $jobcard = $this->Dashboard_model->getJobcardByStatus($REVNR);
            $mdr = $this->Dashboard_model->getMdrByStatus($REVNR);

            if ($jobcard) {

                // Modify Column to Row
                $jc_status = array();
                array_push($jc_status,
                    ((object) array("LABELS" => "OPEN", "QTY" => $jobcard[0]->JC_OPEN )),
                    ((object) array("LABELS" => "PROGRESS", "QTY" => $jobcard[0]->JC_PROGRESS )),
                    ((object) array("LABELS" => "CLOSED", "QTY" => $jobcard[0]->JC_CLOSED ))
                );

                $response["status"] = 'success';
                $response["body"]["jc_status"] = $jc_status;
            }

            if($mdr){
                // Modify Column to Row
                $mdr_status = array();
                array_push($mdr_status,
                    ((object) array("LABELS" => "OPEN", "QTY" => $mdr[0]->MDR_OPEN )),
                    ((object) array("LABELS" => "PROGRESS", "QTY" => $mdr[0]->MDR_PROGRESS )),
                    ((object) array("LABELS" => "CLOSED", "QTY" => $mdr[0]->MDR_CLOSED ))
                );

                $response["body"]["mdr_status"] = $mdr_status;
                
            }
        } catch (Exception $e) {
            
        }

        header('Content-Type: application/json');
        echo json_encode($response);
    }

    public function crud_proposal() {

        $data['is_grocery'] = true;
        $crud = new grocery_CRUD();
        $crud->set_table('proposal');
        $crud->set_subject('Proposal');
        $crud->columns('No_Proposal', 'Subject', 'Start_Date', 'Expired_Date', 'Amount', 'Status');
        $crud->unset_fields('date_created', 'Id');

        $crud->field_type('Status', 'dropdown', array('Draft' => 'Draft', 'Sent' => 'Sent', 'Accepted' => 'Accepted', 'Declined' => 'Declined'));
        $crud->callback_after_insert(array($this, 'proposal_after_insert'));
        $crud->callback_add_field('No_Proposal', array($this, 'crud_propsal_no_add_field_callback'));
        $crud->set_rules('Expired_Date', 'Expired_Date', 'trim|callback_proposal_check_dates[' . $this->input->post('Start_Date') . ']');
        $output = $crud->render();

        $data["session"] = $this->session->userdata('logged_in');
        $data['content'] = 'admin/projects/crud_proposal';
        $data['group'] = $this->User_group_model->where('ID_USER_GROUP', $data["session"]['group_id'])->first();

        $this->load->view('template', array_merge($data, (array) $output));
    }

    public function proposal_after_insert($post_array, $primary_key) {
        date_default_timezone_set('Asia/Jakarta');
        $proposal_day_update = array(
            "date_created" => date('Y-m-d H:i:s')
        );

        $this->db->update('proposal', $proposal_day_update, array('Id' => $primary_key));

        return true;
    }

    function crud_propsal_no_add_field_callback() {
        error_reporting(E_ALL & ~E_NOTICE);
        $last_no_ = 0;
        try {
            $query = $this->db->select('No_Proposal')
                    ->from('proposal')
                    ->order_by('No_Proposal', 'DESC')
                    ->limit(1)
                    ->get();
            $ret = $query->row();
            $last = $ret->No_Proposal;
            $last_no = $last + 1;
        } catch (Exception $e) {
            $last_no_ = 1;
        }
        return '<input type="text" value="' . $last_no . '" name="No_Proposal">';
    }

    public function proposal_check_dates($Due_Date, $Bill_Date) {
        $parts = explode('/', $this->input->post('Bill_Date'));
        $Bill_Date = join('-', $parts);
        $Bill_Date = strtotime($Bill_Date);

        $parts2 = explode('/', $this->input->post('Due_Date'));
        $Due_Date = join('-', $parts2);
        $Due_Date = strtotime($Due_Date);

        if ($Due_Date >= $Bill_Date) {

            return true;
        } else {
            $this->form_validation->set_message('check_dates', "due date should be greater than bill date");
            return false;
        }
    }

//    public function get_notif() {
//        $jobcard_notif['base'] = $this->Jobcard_model_view
//                ->whereRaw("((UPPER( STATUS_SAP ) IN('OPEN', 'CLOSED') OR UPPER( STATUS_SAP ) IN('OPEN', 'CLOSED') ) AND UPPER( STATUS_SAP )!= UPPER( STATUS ) ) OR(UPPER( STATUS_SAP )= 'PROGRESS'AND UPPER( STATUS ) IN('OPEN', 'CLOSED') )");
////        $jobcard_notif['count'] = $jobcard_notif['base']->count();
//        $jobcard_notif['data'] = $jobcard_notif['base']->get();
//        echo json_encode($jobcard_notif['data']);
//    }

    public function get_notif_count() {
        if ($this->session->userdata('REVNR')) {
            echo json_encode($this->Proc_model->get_notif_count($this->session->userdata('REVNR')));
        } else {
            echo json_encode($this->Proc_model->get_notif_count());
        }
    }

    public function get_notif($page = 1, $limit = 10) {
        if ($this->session->userdata('REVNR')) {
            echo json_encode($this->Proc_model->get_notif($page, $limit, $this->session->userdata('REVNR')));
        } else {
            echo json_encode($this->Proc_model->get_notif($page, $limit));
        }
    }

    public function crud_daily_menu() {
        try {
            if ($this->uri->segment(4)) {
                $this->session->set_userdata('REVNR', $this->uri->segment(4));
            }
            $data['id_project'] = $this->session->userdata('REVNR');
            $active_prj = $this->Project_model->get_project_list($data['id_project']);
            $project_area = $this->Daily_menu_model_ci->getAreaList($data['id_project']);
            $project_day = $this->Daily_menu_model_ci->getDayList($data['id_project']);
            $mhrs_query = " SELECT SUM( CAST( REPLACE(ARBEI,',','') AS FLOAT ) ) AS 'MHRS' FROM M_PMORDER WHERE REVNR = " . $this->session->userdata('REVNR') . " AND AUART = 'GA01'";
            $mhrs_count = $this->db
                            ->query($mhrs_query)
                            ->row()->MHRS;

            if ($active_prj) {
                $data['project_name'] = "DAILY MENU";
                $data['project_data'] = $active_prj;
                $data['project_area'] = $project_area;
                $data['project_day'] = $project_day;
                $data['mhrs_count'] = $mhrs_count;
            }
        } catch (Exception $e) {
            error_log($e);
        }

        $data["session"] = $this->session->userdata('logged_in');
        $data['content'] = 'admin/projects/crud_daily_menu';

        $this->load->view('template', $data);
    }

    public function crud_daily_menu_load() {
        $data["session"] = $this->session->userdata('logged_in');

        $grid = new EditableGrid();

        $grid->addColumn('SEQ', 'SEQ', 'int', NULL, false);
        $grid->addColumn('AUFNR', 'Order', 'INT', NULL, false);
        $grid->addColumn('KTEXT', 'Description', 'string', NULL, false);
        $grid->addColumn('ILART', 'Task Code', 'string', NULL, false);
        $grid->addColumn('AREA', 'Area', 'string', NULL, false);
        $grid->addColumn('MHRS', 'Mhrs', 'INT', NULL, false);
        $grid->addColumn('CARD_TYPE', 'TYPE', 'string', NULL, false);

        $active_job = $this->db->query("EXEC TB_P_DAILY_MENU @REVNR = " . $this->session->userdata('REVNR'));

        if (isset($_GET['filter']) && $_GET['filter'] != "") {
            $filter = $_GET['filter'];
            $pos = strpos($filter, '-');

            if ($filter == '-') {
                $active_job = $active_job;
            } elseif (strpos($filter, '-') == (strlen($filter) - 1)) {
                // Filter By Day
                $active_job = $this->db->query("EXEC TB_P_DAILY_MENU @REVNR = " . $this->session->userdata('REVNR') . " ,@P_DAY = "
                        . str_replace('-', '', $filter));
            } elseif ($pos == 0) {
                // Filter By Area
                $active_job = $this->db->query("EXEC TB_P_DAILY_MENU @REVNR = " . $this->session->userdata('REVNR') . " ,@P_AREA = " . str_replace('-', '', $filter));
            } else {
                // Jika URl Seperti ini 5-COPKIT
                $filter = explode('-', $filter);
                $active_job = $this->db->query("EXEC TB_P_DAILY_MENU @REVNR = " . $this->session->userdata('REVNR') . " , @P_AREA = " . $filter[0] . ", @P_DAY = " . $filter[1]);
            }
        }
        $grid->renderJSON($active_job->result(), false, false, true);
    }

    public function crud_daily_menu_load_all() {
        $response["status"] = NULL;
        $response["body"] = NULL;
        $daily_menu = $this->Proc_model;

        try {
            // $sql = "EXEC TB_P_DAILY_MENU @REVNR = " . $_POST['REVNR'];

            // if (isset($_POST['DAY']) && isset($_POST['AREA'])) {
            //     $sql .= ", @P_DAY =" . $_POST['DAY'] . ", @P_AREA = '" . $_POST['AREA'] . "'";
            // } elseif (isset($_POST['DAY'])) {
            //     $sql .= ", @P_DAY =" . $_POST['DAY'];
            // } elseif (isset($_POST['AREA'])) {
            //     $sql .= ", @P_AREA = '" . $_POST['AREA'] . "'";
            // }


            // print_r($_POST);

            $DAY = '';
            $AREA = '';

            if (isset($_POST['DAY']) && isset($_POST['AREA'])) {
                $DAY = $_POST['DAY'];
                $AREA = $_POST['AREA'];
            } elseif (isset($_POST['DAY'])) {
                $DAY = $_POST['DAY'];
            } elseif (isset($_POST['AREA'])) {
                $AREA = $_POST['AREA'];
            }


            $daily_menu = $daily_menu->get_daily_menu($_POST['REVNR'], $DAY, $AREA);

            if (count($daily_menu) > 0) {
                $response["status"] = 'success';
                $response["body"] = $daily_menu;
            } else {
                $response["status"] = 'error';
                $response["body"] = NULL;
            }
        } catch (Exception $e) {
            
        }

        header('Content-Type: application/json');
        echo json_encode($response);
    }

    public function get_mhrs_total() {
        if (isset($_GET['id']) && ( isset($_GET['day']) || isset($_GET['area']) )) {
            $id = $_GET['id'];
            $area = $_GET['area'];
            $day = $_GET['day'];

            $mhrs['total'] = $this->Daily_menu_model_ci->getSumMhrs($id, $area, $day)->total;
            $mhrs['req'] = $mhrs['total'] / 4;
            echo json_encode($mhrs);
        } else {
            echo "null";
        }
    }

    // Customer View
    public function view_customer() {
        $data["session"] = $this->session->userdata('logged_in');
        $data['content'] = 'admin/projects/view_customer';

        $this->load->view('template', $data);
    }

    public function load_material_data() {
        $response["status"] = NULL;
        $response["body"] = NULL;

        try {
            if (isset($_POST['MATNR'])) {
                $material = $this->Material_model->where('MATNR', $_POST['MATNR']);
                if ($material->count() > 0) {
                    $response["status"] = 'success';
                    $response["body"] = $material->first();
                } else {
                    $response["status"] = 'error';
                    $response["body"] = NULL;
                }
            }
        } catch (Exception $e) {
            
        }

        header('Content-Type: application/json');
        echo json_encode($response);
    }

    function crud_revh(){
    
    }

    function get_hilite(){
        $response["status"] = NULL;
        $response["body"] = NULL;

        try{
            if(!empty($this->input->post('REVNR'))){
                $REVNR = $this->input->post('REVNR');
                $hilite = $this->Revh_model
                                ->where("REVNR", $REVNR)
                                ->first();
                if ($hilite) {
                    $response["status"] = 'success';
                    $response["body"] = $hilite;
                } else {
                    $response["status"] = 'error';
                    $response["body"] = $hilite;
                } 
            }          
        } catch (Exception $e){ 
            echo $e;
        }

        header('Content-Type: application/json');
        echo json_encode($response); 
    }

    function save_hilite(){
        $response["status"] = NULL;
        $response["body"] = NULL;

        try{
            if(!empty($this->input->post('REVNR'))){
                $REVNR = $this->input->post('REVNR');
                $HILITE_BODY = $this->input->post('HILITE');
                $IN_HANGAR = $this->input->post('IN_HANGAR');
                $hilite = $this->Revh_model
                    ->updateOrCreate(
                        ['REVNR' => $REVNR ],
                        ['HILITE' => $HILITE_BODY, 'IN_HANGAR' => $IN_HANGAR ]
                    );

                if ($hilite) {
                    $response["status"] = 'success';
                    $response["body"] = NULL;
                } else {
                    $response["status"] = 'error';
                    $response["body"] = NULL;
                } 
            }          
        } catch (Exception $e){ 
            echo $e;
        }

        header('Content-Type: application/json');
        echo json_encode($response); 
    }

}
