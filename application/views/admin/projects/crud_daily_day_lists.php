<?php 
    $this->load->view('admin/projects/header'); 
    $is_customer = is_customer($session["user_group"]);
    $id_project = json_encode($id_project);
?>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <!-- Custom Tabs -->
            <div class="nav-tabs-custom">
                <?php $this->load->view('admin/projects/menu'); ?>
                <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
        </div>  
        <!-- nav-tabs-custom -->
        <!-- /.col -->
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header with-border">
                    <div class="box-tools">
                        <button class="btn btn-info" id="addNew" style="display: none;">Add New</button>
                        <button class="btn btn-info" id="copyNew" style="display: none;"><i class="fa fa-files-o" aria-hidden="true"></i> Copy Last Report</button>
                        <!-- <button class="btn btn-warning">Filter</button> -->
                    </div>
                    <!-- /.box-tools -->
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table class="table table-bordered table-hover dataTable" id="report-list">
                        <thead>
                            <tr>
                                <th width="20px"></th>
                                <th></th>
                                <th width="20%"></th>
                                <th width="15%"></th>
                                <th width="15%"></th>
                                <th width="15%"></th>
                            </tr>
                        </thead>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
    
</section>

<style>
    .box-header > .box-title, .box-header > .box-tools {
        position: relative;
        display: inline-block;
        vertical-align: middle;
        right: 0;
        top:0;
    }

</style>
<script type="text/javascript">
    var is_customer = '<?= $is_customer ?>' === "1" ? true : false;
    var report_table = $('#report-list').dataTable({
        paging: false
    });

    load_report();
    function load_report(){
        var url = '<?php echo base_url() ?>' + '/Projects/crud_daily_day_list_load'
        $.ajax({
            type: 'POST',
            url: url,
            dataType: 'text',
            data: { REVNR:  <?= $id_project ?> },
            success: function(data){
                console.log(data);
                var response = $.parseJSON(data);
                if(response.status === 'success') {
                    performing_datatables(response.body.report_list)
                    if(response.body.report_list.length > 0){
                        if(!is_customer){
                            $('#copyNew').show();
                            response.body.last_status == 'OPEN' ? $('#copyNew').hide() : $('#copyNew').show();
                        }
                    }else{
                        if(!is_customer){
                            $('#addNew').show();
                            $('#copyNew').hide();
                        }
                    }
                }
            }
        })
    }

    function performing_datatables(data){
        report_table = $('#report-list').dataTable({
            destroy: true,
            searching: true,
            data: data,
            columns: [
                { title: 'No', data: 'ROW_NUM', render(data, type, row, meta){
                    return meta.settings._iDisplayStart + meta.row + 1; 
                } },
                { title: 'REPORT', data: 'REPORT_NAME' },
                { title: 'UPDATED BY', data: 'NAME' },
                { title: 'UPDATED AT', data: 'CREATED_AT', render: function(data, type, row){
                    return moment(data, "YYYY-MM-DD hh:mm:ss").format("DD MMM YYYY HH:mm:ss");
                } },
                { title: 'STATUS', data: 'STATUS', render: function(data, type, row){
                    if(data === 'OPEN') {
                        return '<center><span class="label label-danger" id="report-status">' + data + '</center></span>'
                    } else {
                        return '<center><span class="label label-success" id="report-status">'  + data + ' </center></span>'
                    }
                } },
                { title: 'ACTION', data: 'ID', render: function(data, type, row){ 
                    if(is_customer){
                        return "<center><button class='btn btn-sm btn-info view' data-id='"+data+"'><i class='fa fa-eye' aria-hidden='true'></i> </button>"
                    } else if (row.STATUS === 'OPEN') {
                        return "<center><button class='btn btn-sm btn-info view' data-id='"+data+"'><i class='fa fa-pencil-square-o' aria-hidden='true'></i> </button>"
                    } else {
                        return "<center><button class='btn btn-sm btn-info view' data-id='"+data+"'><i class='fa fa-eye' aria-hidden='true'></i> </button>" +
                    " <button class='btn btn-sm btn-danger delete' data-id='"+data+"'> <i class='fa fa-trash-o' aria-hidden='true'></i> </button></center>" 
                    }
                } }
            ]     
        })


        $('#report-list tbody').on('click', 'button', function(){
            var isCopy = $(this).hasClass('copy') ? true : false;
            var isView = $(this).hasClass('view') ? true : false;
            var isDelete = $(this).hasClass('delete') ? true : false;
            var id = $(this).data('id');

            if (isCopy) { crud_action(id, 'copy'); return; }

            if(isView) { crud_action(id, 'load_by_id'); return; }

            if(isDelete) { crud_action(id, 'delete'); return; }

        })
    }

    function crud_action(id, action){
        $('#loader').show();
        var url = '<?php echo base_url() ?>' + 'Projects/crud_daily_report_'+action;
        var REVNR, REPORT_ID;
        if(action === 'delete'){
            swal({
                title: "Are you sure?",
                text: "Once deleted, you will not be able to recover this record!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
             })
            .then(function(isConfirm){
                if(isConfirm){delete_report(id, url)}
            })            
        }else{
            $.ajax({
                type: 'POST',
                url: url,
                data: {'ID': id},
                dataType: 'text',
                success: function(data){
                    $('#loader').hide(); 
                    var response = $.parseJSON(data);                
                    if(response.status === 'success'){
                        if(action === 'copy') { show_alert(response.body.REVNR, response.body.ID, 'Duplicate Report Success!'); return}
                        if(action === 'load_by_id') { view_details(response.body.REVNR, response.body.ID); return}
                    }
                }
            })
        }
    }

    function show_alert(REVNR, REPORT_ID, MSG){
        swal({
            title: MSG,
            icon: 'success',
            confirmButtonText: 'Ok'
        })
        .then(function(isConfirm){
            var redirect = '<?php echo base_url() ?>' + 'Projects/crud_daily_day/'+REVNR+'/'+REPORT_ID;
            window.location.href = redirect;
        })
    }

    function view_details(REVNR, REPORT_ID){
            var redirect = '<?php echo base_url() ?>' + 'Projects/crud_daily_day/'+REVNR+'/'+REPORT_ID;
            window.location.href = redirect;
    }
    
    function delete_report(id, url){
        $.ajax({
                type: 'POST',
                url: url,
                data: {'ID': id},
                dataType: 'text',
                success: function(data){
                    $('#loader').toggle(); 
                    var response = $.parseJSON(data);                
                    if(response.status === 'success'){
                        load_report();
                        swal("Report has been deleted!", {
                            icon: "success",
                        });
                    }
                }
            })
    }

    $('#addNew').on('click', function(){
        var url = '<?php echo base_url() ?>' + 'Projects/crud_daily_report_add'
        $('#loader').show();
        var REVNR = '<?php echo $project_data->REVNR ?>';
        var data = { REPORT_NAME: generate_report_name(), REVNR: REVNR };
        $.ajax({
            url: url,
            method: 'POST',
            dataType: 'text',
            data: data,
            success: function(data){
                $('#loader').hide()
                var response = $.parseJSON(data);
                if(response.status === 'success'){
                    $('#addNew').hide();
                    load_report();
                    show_alert(response.body.REVNR, response.body.ID, 'Report has been saved!');
                }
            }
        })
    })

    $('#copyNew').click(function(){
            var REVNR = '<?= $project_data->REVNR ?>'
            var url = '<?php echo base_url() ?>' + 'Projects/crud_daily_report_copy';
            var data = { REPORT_NAME: generate_report_name(), REVNR: REVNR };
            $.ajax({
                type: 'POST',
                url: url,
                dataType: 'text',
                data: data,
                success: function(data){
                    $('#loader').hide(); 
                    var response = $.parseJSON(data);                
                    if(response.status === 'success'){
                        show_alert(response.body.REVNR, response.body.ID, 'Duplicate Report Success!');
                    } else {
                        swal({
                            title: 'Opps!',
                            text: response.body,
                            icon: 'warning'
                        })
                    }
                }
            })
    })

    function generate_report_name(){
        var start_date      = moment('<?php echo $project_data->REVBD ?>');
        var current_date    = new moment();
        var diff            = Math.trunc(moment.duration(current_date.diff(start_date)).asDays()) + 1;
        return "Daily Report #"+diff;
    }
    





</script>