 <?php
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
 header("Content-Disposition: attachment; filename=$REVNR.xlsx");
 header("Pragma: no-cache");
 header("Expires: 0");


 ?>
 
 <table border="1" width="100%">
 
      <thead>
        <th>ID</th>
        <th>Order</th>
        <th>Card Type</th>
        <th>Part Number</th>
        <th>Alternate Part Number</th>
        <th>Material Description</th>
        <th>Discrepancies</th>
        <th>Mat Type</th>
        <th>IPC</th>
        <th>STO Number</th>
        <th>Outbound Delivery</th>
        <th>TO Number</th>
        <th>Jobcard Number</th>
        <th>MRM Issue Date</th>
        <th>Qty Req Single Item</th>
        <th>Total Qty Required for All Job Task</th>
        <th>UOM</th>
        <th>Input Stock Manually</th>
        <th>Storage Location</th>
        <th>Material Fulfillment Status</th>
        <th>Fullfillment Status Date</th>
        <th>Fullfillment remark</th>
        <th>Date Of PO</th>
        <th>Purchase Order PO</th>
        <th>AWB Number</th>
        <th>AWB Date</th>
        <th>Qty Delivered</th>
        <th>Qty Remain</th>
        <th>Material Remark</th>
      </thead>
      <tbody>
        <?php $no=1;foreach($data as $key) { ?>
        <tr>
          <td><?= $key->ID?></td>
          <td><?= $key->AUFNR_LINK?></td>
          <td><?= $key->CARD_TYPE?></td>
          <td><?= $key->MATNR?></td>
          <td><?= $key->ALTERNATE_PART_NUMBER?></td>
          <td><?= $key->MAKTX?></td>
          <td><?= $key->KTEXT_LINK?></td>
          <td><?= $key->MTART?></td>
          <td><?= $key->IPC?></td>
          <td><?= $key->STO_NUMBER?></td>
          <td><?= $key->OUTBOND_DELIV?></td>
          <td><?= $key->TO_NUM?></td>
          <td><?= $key->CUST_JC_NUM?></td>
          <td><?= $key->MRM_ISSUE_DATE?></td>
          <td><?= $key->MENGE?></td>
          <td><?= $key->TOTAL_QTY?></td>
          <td><?= $key->MEINS?></td>
          <td><?= $key->STOCK_MANUAL?></td>
          <td><?= $key->LGORT?></td>
          <td><?= $key->MATERIAL_FULFILLMENT_STATUS?></td>
          <td><?= $key->FULLFILLMENT_STATUS_DATE?></td>
          <td><?= $key->FULLFILLMENT_REMARK?></td>
          <td><?= $key->PO_DATE?></td>
          <td><?= $key->PO_NUM?></td>
          <td><?= $key->AWB_NUMBER?></td>
          <td><?= $key->AWB_DATE?></td>
          <td><?= $key->QTY_DELIVERED?></td>
          <td><?= $key->QTY_REMAIN?></td>
          <td><?= $key->MATERIAL_REMARK?></td>
        </tr>
        <?php } ?>
      </tbody>
 
 </table>