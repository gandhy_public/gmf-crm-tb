<?php 
    $this->load->view('admin/projects/header'); 
    $this->load->helper('auth');
    $is_customer = is_customer($session['user_group']);
    $is_edit = $write == 1 ? true : false;
    $id_project = json_encode($id_project);
    $search_value = ((!isset($_GET['search'])) || $_GET['search'] == "") ? "Search Q & A" : $_GET['search'];
?>
<style>
    table > thead > tr > th.editablegrid-ANSWER
    {
        color: #0097e6 !important;
    }
</style>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="nav-tabs-custom">
                <?php $this->load->view('admin/projects/menu'); ?>
                <div class="tab-content">
                    <section class="content" style="overflow: auto;">
                        <div class="row">
                            <div class="col-md-3">
                                <a href="<?= base_url()?>index.php/ticket/create/<?php echo json_decode($id_project)?>" class="btn btn-primary btn-block margin-bottom">Create Q & A</a>
                            </div>
                            <div class="col-md-9">
                                <div class="box box-primary">
                                    <div class="box-header with-border">
                                        <h3 class="box-title">Q & A List</h3>

                                        <div class="box-tools pull-right">
                                            <div class="has-feedback">
                                                <input type="text" class="form-control input-sm" id="search" placeholder="<?= $search_value?>">
                                                <span class="glyphicon glyphicon-search form-control-feedback"></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="box-body no-padding">
                                        <div class="table-responsive mailbox-messages">
                                            <table class="table table-hover table-striped">
                                                <tbody id="body-table">
                                                    
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="box-footer no-padding">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">
    $(function() {
        var table = loadTable();

        $('#search').on('keypress', function (e) {
            if(e.which === 13){
                var value_search = $(this).val();
                window.location.href = "<?= base_url()?>index.php/ticket/view/<?= json_decode($id_project)?>?search="+value_search;
            }
        });

        $(this).on('click', '.label', function (e) {
            var id_ticket = $(this).data('x');

            $("#status_"+id_ticket).hide();
            $("#status-edit_"+id_ticket).show();
        });

        $(this).on('change', '.status-edit', function (e) {
            var id_ticket = $(this).data('x');
            var value_edit = $(this).val();
            $.ajax({
                url: "<?= base_url('index.php/Ticket/update_status/').json_decode($id_project)?>",
                type: 'POST',
                data:{
                    status:value_edit,
                    id_ticket:id_ticket
                },
                success: function (response) {
                    if(response){
                        display_toast('success',"Success Update Data!");
                        $('#body-table').load(loadTable());
                    }else{
                        display_toast('danger',"Failed Update Data!");
                        $('#body-table').load(loadTable());
                    }
                }   
            });
        }); 

    });



    function loadTable() {
        $('#body-table').html("");
        var search = "<?= $search_value?>";
        if(search == "Search Q & A"){
            search = "";
        }else{
            search = "?search="+search;
        }
        $.ajax({
            url: "<?= base_url('index.php/Ticket/api_table/').json_decode($id_project)?>"+search,
            type: 'POST',
            success: function (response) {
                var resp = JSON.parse(response);
                var trHTML = '';
                if(resp.length == 0){
                    trHTML += 
                        '<tr>'+
                            '<td colspan="4"><center>No Data Available</center></td>'+
                        '</tr>';
                    $('#body-table').append(trHTML);
                    return;
                }

                $.each(resp, function (i, item) {

                    if(item.STATUS == "open"){
                        status = '<span class="label label-danger" data-x="'+item.ID+'">OPEN</span>';
                    }else if(item.STATUS == "process"){
                        status = '<span class="label label-warning" data-x="'+item.ID+'">PROCESS</span>';
                    }else{
                        status = '<span class="label label-success" data-x="'+item.ID+'">CLOSE</span>';
                    }

                    trHTML += 
                        '<tr>' +
                            '<td class="mailbox-name" width="25%"><b>'+item.NAME+'</b></td>'+
                            '<td class="mailbox-subject" width="45%"><a href="'+"<?= base_url()?>"+'index.php/Ticket/answer/'+item.REVNR+'/'+item.ID+'"><b>'+item.SUBJECT+'</a></b> - '+item.MESSAGE.substr(0,20)+'...</td>' +
                            '<td class="mailbox-attachment" width="10%">'+
                                '<div id="status_'+item.ID+'">'+
                                    status+
                                '</div>'+
                                '<select class="status-edit" id="status-edit_'+item.ID+'" data-x="'+item.ID+'" style="display: none">'+
                                    '<option value="" disabled selected>Select Status</option>'+
                                    '<option value="open">OPEN</option>'+
                                    '<option value="process">PROCESS</option>'+
                                    '<option value="close">CLOSE</option>'+
                                '</select>'+
                            '</td>'+
                            '<td class="mailbox-date" width="20%">'+item.CREATE_AT.substr(0,16)+'</td>'+
                        '</tr>';
                });

                $('#body-table').append(trHTML);
            }   
        });
    };   
</script>