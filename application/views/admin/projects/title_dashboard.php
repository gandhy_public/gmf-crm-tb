<?php $this->load->helper('text_formatter'); ?>
<style type="text/css">
    p{
        line-height: 0.8;
        margin-top: 15px;
    }
</style>

<div class="row">
	<div class="col-lg-12 col-xs-12">   
	  <div class="box box-solid">
		  <div class="box">
	
				<div class="col-lg-12">
					<div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
                        <p><b>A/C REG</b></p>
                        <p><b>DATE IN</b></p>
                        <p><b>DATE OUT</b></p>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
                        <p>: <?= $active_prj->TPLNR ?></p>
                        <p>: <?= raw_date_format($active_prj->REVBD) ?></p>
                        <p>: <?= raw_date_format($active_prj->REVED) ?></p>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
                        <p><b>TOTAL MAINT</b></p>
                        <p><b>DATE REPORT</b></p>
                        <p><b>DAY REPORT</b></p>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
                        <p>: <?= get_diff_days($active_prj->REVBD, $active_prj->REVED) ?></p>
                        <p>: <?= get_current_date() ?></p>
                        <p>: <?= get_diff_days($active_prj->REVBD, get_current_date())?> </p>
                    </div>


			  </div>
		   </div>
		</div>
	</div>
</div>