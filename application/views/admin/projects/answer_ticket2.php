
<?php 
    $this->load->view('admin/projects/header'); 
    $this->load->helper('auth');
    $is_customer = is_customer($session['user_group']);
    $is_edit = $write == 1 ? true : false;
    $id_project = json_encode($id_project);
?>
<style>
    table > thead > tr > th.editablegrid-ANSWER
    {
        color: #0097e6 !important;
    }
</style>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="nav-tabs-custom">
                <?php $this->load->view('admin/projects/menu'); ?>
                <div class="tab-content">
                    <section class="content" style="overflow: auto;">
                    	<div class="row">
                    		<div class="col-md-12">
					          	<div class="box box-primary">
						            <div class="box-header with-border">
						                <span class="username"><a href="javascript:;"><?= $ticket_detail[0]->NAME?></a></span>
						                <br>
						                <span class="description"><?= $ticket_detail[0]->SUBJECT?> | <?= substr($ticket_detail[0]->CREATE_AT,0,16)?></span>
						              
										<div class="box-tools">
											<a href="<?= base_url()?>index.php/ticket/view/<?= json_decode($id_project)?>" class="btn btn-primary btn-block margin-bottom">Back to List Q & A</a>
										</div>
						            </div>
						            <div class="box-body">
						              	<pre style="font-size: 15px;border: 2px solid rgba(255,255,255,0.15);background: rgba(255,255,255,.15);width: 100%;"><?= $ticket_detail[0]->MESSAGE?></pre>

						              	<?php if($ticket_detail[0]->FILE != "kosong"){?>
							            <div class="clearfix">
							            	<a href="javascript:;" data-x="<?= base_url().$ticket_detail[0]->FILE?>" class="view-image">
							            		<img src="<?= base_url().$ticket_detail[0]->FILE?>" style="width: 10%">
							            	</a>
							             </div>
							         	<?php }?>
						            </div>
						            <hr>

						            <?php foreach ($ticket_answer as $key) {?>
						            <div class="box-footer box-comments">
						              	<div class="box-comment">
						                	<div class="comment-text">
						                      	<span class="username">
						                        	<?= $key->NAME?>
						                        	<span class="text-muted pull-right"><?= substr($key->CREATE_AT,0,16)?></span>
						                      	</span><!-- /.username -->
						                  		<pre style="font-size: 15px;border: 2px solid rgba(255,255,255,0.15);background: rgba(255,255,255,.15);width: 100%;"><?= $key->MESSAGE?></pre>
						                  		<?php if($key->FILE != "kosong"){?>
						                  		<div class="clearfix">
						                  			<a href="javascript:;" data-x="<?= base_url().$key->FILE?>" class="view-image">
									            		<img src="<?= base_url().$key->FILE?>" style="width: 20%">
									            	</a>
									            </div>
									        	<?php }?>
						                	</div>
						              	</div>
						            </div>
						            <br>
						            <?php }?>

						            <div class="box-footer">
										<form id="myform" method="post">
											<div class="img-push">
												<div class="form-group">
										  			<textarea class="form-control" id="answer" rows="3" placeholder="Type your reply here..."></textarea>
										  		</div>
										  		<div class="form-group">
							                  		<i class="fa fa-paperclip"></i> Attachment File Image
							                  		<input type="file" name="file" id="file" accept="image/x-png,image/gif,image/jpeg">
							                  		<div class="progress-bar progress-bar-success myprogress" role="progressbar" style="width:0%">0%</div>
							              		</div>
							              		<br>
							              		<div class="form-group">
										  			<button type="button" id="btn_submit" class="btn btn-primary">POST</button>
										  		</div>
											</div>
										</form>
						            </div>
					          	</div>
					        </div>
                    	</div>
                    </section>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="modal fade" id="imagemodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">              
      <div class="modal-body">
      	<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <img src="" class="imagepreview" style="width: 100%;" >
      </div>
    </div>
  </div>
</div>

<script>
    $(function () {
    	$(".view-image").on("click",function(){
    		var link_image = $(this).data('x');
    		$('.imagepreview').attr('src', link_image);
			$('#imagemodal').modal('show');   
    	});

        $('#btn_submit').click(function () {
            $('.myprogress').css('width', '0');
            
            var id_ticket = "<?= $ticket_detail[0]->ID?>"
            var answer = $('#answer').val();
            var file = $('#file')[0].files[0]

            if (answer.trim() == ''){
                display_toast('danger',"Answer Q&A is required!");
                return;
            }
            

            var formData = new FormData();
            formData.append('id_ticket', id_ticket);
            formData.append('answer', answer);
            formData.append('file', file);

            $('#btn_submit').attr('disabled', 'disabled').text("POSTING...");
            $.ajax({
                url: "<?= base_url()?>index.php/ticket/proses_answer",
                data: formData,
                processData: false,
                contentType: false,
                type: 'POST',
                xhr: function () {
                    var xhr = new window.XMLHttpRequest();
                    if(file){
	                    xhr.upload.addEventListener("progress", function (evt) {
	                        if (evt.lengthComputable) {
	                            var percentComplete = evt.loaded / evt.total;
	                            percentComplete = parseInt(percentComplete * 100);
	                            $('.myprogress').text(percentComplete + '%');
	                            $('.myprogress').css('width', percentComplete + '%');
	                        }
	                    }, false);
	                    return xhr;
                	}else{
                		return xhr;
                	}
                },
                success: function (data) {
                	if(data){
                		swal({
                            title: "INFO !",
                            text: "Success answer Q & A",
                            icon: "success"
                        }).then(function(){
                			$('#btn_submit').removeAttr('disabled').text("POST");
                            location.reload();
                        });
                	}else{
                		swal({
                            title: "WARNING !",
                            text: "Failed answer Q & A, please try again!",
                            icon: "warning"
                        }).then(function(){
                			$('#btn_submit').removeAttr('disabled').text("Repost");
                        });
                	}
                    
                }
            });
        });
    });
</script>