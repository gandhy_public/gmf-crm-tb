<?php 
    $this->load->view('admin/projects/header'); 
    $this->load->helper('auth');
    $is_customer = is_customer($session['user_group']);
    $is_edit = $write == 1 ? true : false;
?>
<style>
    table > thead > tr > th.editablegrid-DATE_PE ,
    table > thead > tr > th.editablegrid-STATUS ,
    table > thead > tr > th.editablegrid-MATSTATUS ,
    table > thead > tr > th.editablegrid-STEP1 ,
    table > thead > tr > th.editablegrid-DATE1 ,
    table > thead > tr > th.editablegrid-STEP2 ,
    table > thead > tr > th.editablegrid-DATE2 ,
    table > thead > tr > th.editablegrid-STEP3 ,
    table > thead > tr > th.editablegrid-DATE3 ,
    table > thead > tr > th.editablegrid-DATECLOSE ,
    table > thead > tr > th.editablegrid-remark ,
    table > thead > tr > th.editablegrid-DOC_SENT_STATUS ,
    table > thead > tr > th.editablegrid-FREETEXT ,
    table > thead > tr > th.editablegrid-DATEPROGRESS ,
    table > thead > tr > th.editablegrid-REMARK ,
    table > thead > tr > th.editablegrid-DAY ,
    table > thead > tr > th.editablegrid-CABINSTATUS ,
    table > thead > tr > th.editablegrid-SKILL
    {
        color: #0097e6 !important;
    }
    .title{
    position: absolute;
    left: 50%;
    margin-left: -200px;
    /*margin-right:-100px;*/
    top: 10px;
    height: 40px;
    width: 100%;
    font-size: large;fontweight:bold;color:white;
    }
    .scrollbar::-webkit-scrollbar
    {
        height: 18px;
        /*background-color: #000000;*/
    }
     
    .scrollbar::-webkit-scrollbar-thumb
    {
        border-radius: 10px;
        -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,.3);
        background-color: #FFFFFF;
    }
    .highlight{
        background-color: lightgoldenrodyellow;
    }
    table.dataTable td{
        vertical-align: middle !important;
    }
    table tbody tr:hover{
        background-color: lightgoldenrodyellow !important;
    }
    table tbody td.focus{box-shadow:inset 0 0 1px 2px #3366FF !important;
        }
    table tbody td.edited{
        padding:0px !important;}

    /*.dataTables_scrollBody{
          position: fixed !important;
          /*top: 20px;
          overflow-x: scroll !important;
          bottom: 20px;
          width: 100%;

    }*/
</style>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <!-- Custom Tabs -->
            <div class="nav-tabs-custom">
                <?php $this->load->view('admin/projects/menu'); ?>
                <div class="tab-content">
                    <section class="content" style="overflow: auto;">
                        <div id="message"></div>
                        <div id="wrap">
                            <!-- <h3>JOBCARD</h3>-->
                            <!-- Feedback message zone -->
                            <div id="toolbar">
                                <div class="pull-left form-inline">
                                    &nbsp;
                                </div>
                                <!-- <input type="text" id="filter" name="filter" placeholder="Filter :type any text here"  /> -->
                                <!--                                <a id="showaddformbutton" class="button green"><i class="fa fa-plus"></i> Add new row</a>-->

                                <!-- Check if Is Customer -->
                                <?php if(!$is_customer && $is_edit) { ?>
                                <div class="pull-right form-inline">
                                    <a href="<?php echo base_url('Projects/write_xlsx_file_mdr?REVNR=' . $id_project ) ?>" class="btn btn-info" id="download">Download Excel <i class="fa fa-download" aria-hidden="true"></i></a>
                                    <button class="btn btn-info" data-toggle="modal" data-target="#modalUploadFile">
                                        Import Excel
                                        <i class="fa fa-upload" aria-hidden="true"></i>
                                    </button>
                                </div>
                                <?php } ?>
                            </div>
                            <!-- Grid contents -->
                            <!-- <div id="tablecontentx">
                                <div class="lds-ellipsis"><div></div><div></div><div></div><div></div></div>
                            </div> -->
                            <div id="tablecontentx" name="TB_M_PMORDER">
                                <table id="tbMDR" class="testgridx table table-responsive table-bordered " width="100%">
                                    <thead id="tbHeader">
                                        <tr class="trHeader"></tr>
                                    </thead>
                                </table>
                            </div>
                            <!-- Paginator control -->
                            <div id="paginator"></div>
                        </div>
                        <!-- script src="<?php echo base_url(); ?>assets/editablegrid/js/jquery-1.11.1.min.js" ></script -->
                        <script src="<?php echo base_url(); ?>assets/jquery-ui/js/jquery-ui-1.10.4.custom.min.js"></script>
                        <script type="text/javascript">
                            var base_url = "<?php echo base_url(); ?>projects/crud_mdr_";

                            function highlightRow(rowId, bgColor, after) {
                                var rowSelector = $("#" + rowId);
                                rowSelector.css("background-color", bgColor);
                                rowSelector.fadeTo("normal", 0.5, function() {
                                    rowSelector.fadeTo("fast", 1, function() {
                                        rowSelector.css("background-color", '');
                                    });
                                });
                            }

                            function highlight(div_id, style) {
                                highlightRow(div_id, style == "error" ? "#e5afaf" : style == "warning" ? "#ffcc00" : "#8dc70a");
                            }



                            function message(type, message) {
                                $('#message').html("<div class=\"notification  " + type + "\">" + message + "</div>").slideDown('normal').delay(1800).slideToggle('slow');
                            }

                            /**
                             updateCellValue calls the PHP script that will update the database.
                             */
                            function updateCellValue(editableGrid, rowIndex, columnIndex, oldValue, newValue, row, onResponse) {
                                $.ajax({
                                    url: base_url + 'update',
                                    type: 'POST',
                                    dataType: "html",
                                    data: {
                                        tablename: editableGrid.name,
                                        id: editableGrid.getValueAt(rowIndex, 1),
                                        newvalue: editableGrid.getColumnType(columnIndex) == "boolean" ? (newValue ? 1 : 0) : newValue,
                                        colname: editableGrid.getColumnName(columnIndex),
                                        coltype: editableGrid.getColumnType(columnIndex)
                                    },
                                    success: function(response) {
                                        // reset old value if failed then highlight row
                                        var success = response.search("ok") > 0;
                                        if (!success) {
                                            editableGrid.setValueAt(rowIndex, columnIndex, oldValue);
                                            display_toast('danger', 'Error to update, please check on Column ' + editableGrid.getColumnName(columnIndex) + ', Order : ' + editableGrid.getValueAt(rowIndex, 1));
                                            datagrid.fetchGrid();
                                        } else {
                                            display_toast('success', 'Order : ' + editableGrid.getValueAt(rowIndex, 1) + ', Column ' + editableGrid.getColumnName(columnIndex) + ' updated to ' + editableGrid.getValueAt(rowIndex, columnIndex));
                                            datagrid.fetchGrid();
                                        }
                                    },
                                    error: function(XMLHttpRequest, textStatus, exception) {
                                        alert("Ajax failure\n" + errortext);
                                    },
                                    async: true
                                });

                            }

                            function DatabaseGrid() {
                                this.editableGrid = new EditableGrid("V_MDR_PROGRESS", {
                                    editmode: 'static',
                                    enableSort: true,

                                    /* Comment this line if you set serverSide to true */
                                    // define the number of row visible by page
                                    /*pageSize: 50,*/

                                    /* This property enables the serverSide part */
                                    serverSide: true,

                                    // Once the table is displayed, we update the paginator state
                                    tableRendered: function() {
                                        updatePaginator(this);
                                    },
                                    tableLoaded: function() {
                                        datagrid.initializeGrid(this);
                                    },
                                    modelChanged: function(rowIndex, columnIndex, oldValue, newValue, row) {
                                        updateCellValue(this, rowIndex, columnIndex, oldValue, newValue, row);
                                    }
                                });
                                this.fetchGrid();

                                $("#filter").val(this.editableGrid.currentFilter != null ? this.editableGrid.currentFilter : "");
                                if (this.editableGrid.currentFilter != null && this.editableGrid.currentFilter.length > 0)
                                    $("#filter").addClass('filterdefined');
                                else
                                    $("#filter").removeClass('filterdefined');

                            }

                            DatabaseGrid.prototype.fetchGrid = function() {
                                // call a PHP script to get the data
                                // this.editableGrid.loadJSON("<?php echo base_url() ?>projects/crud_mdr_load_all?edit=" + "<?= $write ?>");
                            };

                            DatabaseGrid.prototype.initializeGrid = function(grid) {

                                var self = this;

                                // render for the action column
                                //                                if (grid.data.length > 0) {
                                //                                    if(grid.data[0].length > 10) {
                                //                                        grid.setCellRenderer("action", new CellRenderer({
                                //                                            render: function (cell, AUFNR) {
                                //                                                cell.innerHTML += "<button class='btn btn-sm btn-danger delete' data-id='"+AUFNR+"' onclick='jc_delete("+AUFNR+")'><i class='fa fa-trash-o' aria-hidden='true'></i> </button>";
                                //                                            }
                                //                                        }));                                        
                                //                                    }
                                //                                }

                                //                                grid.setCellRenderer("AUFNR", new CellRenderer({
                                //                                    render: function(cell, AUFNR){
                                //                                        var mrm_link = '<?php echo base_url() ?>' + 'projects/crud_mrm/' + '<?php echo $id_project ?>' + '/'+AUFNR+'/MDR';     
                                //                                        cell.innerHTML = "<a href='" + mrm_link + "'>"+ AUFNR +"</a>"
                                //                                    }
                                //                                }))
                                //
                                //                                grid.setCellRenderer("JC_REF", new CellRenderer({
                                //                                    render: function(cell, JC_REF){
                                //                                        var jc_round = JC_REF * 1;
                                //                                        var jc_link = '<?php echo base_url() ?>' + 'projects/crud_jobcard/' + '<?php echo $id_project ?>' + '/'+jc_round;     
                                //                                        cell.innerHTML = "<a href='" + jc_link + "'>"+ jc_round +"</a>"
                                //                                    }
                                //                                }))                            

                                // grid.renderGrid("tablecontent", "testgrid");
                                // $('html, body').scrollTop(1);
                                // $('html, body').scrollTop(0);

                            };

                            DatabaseGrid.prototype.deleteRow = function(id) {

                                var self = this;

                                if (confirm('Are you sur you want to delete the row id ' + id)) {

                                    $.ajax({
                                        url: base_url + 'delete',
                                        type: 'POST',
                                        dataType: "html",
                                        data: {
                                            tablename: self.editableGrid.name,
                                            id: id
                                        },
                                        success: function(response) {
                                            if (response == "ok") {
                                                message("success", "Row deleted");
                                                self.fetchGrid();
                                            }
                                        },
                                        error: function(XMLHttpRequest, textStatus, exception) {
                                            alert("Ajax failure\n" + errortext);
                                        },
                                        async: true
                                    });


                                }

                            };


                            DatabaseGrid.prototype.addRow = function(id) {

                                var self = this;

                                $.ajax({
                                    url: base_url + 'add',
                                    type: 'POST',
                                    dataType: "html",
                                    data: {
                                        tablename: self.editableGrid.name,
                                        name: $("#name").val(),
                                        firstname: $("#firstname").val()
                                    },
                                    success: function(response) {
                                        if (response == "ok") {

                                            // hide form
                                            showAddForm();
                                            $("#name").val('');
                                            $("#firstname").val('');
                                            message("success", "Row added : reload model");
                                            self.fetchGrid();
                                        } else
                                            message("error", "Error occured");
                                    },
                                    error: function(XMLHttpRequest, textStatus, exception) {
                                        alert("Ajax failure\n" + errortext);
                                    },
                                    async: true
                                });
                            };

                            function updatePaginator(grid, divId) {
                                divId = divId || "paginator";
                                var paginator = $("#" + divId).empty();
                                var nbPages = grid.getPageCount();

                                // get interval
                                var interval = grid.getSlidingPageInterval(20);
                                if (interval == null)
                                    return;

                                // get pages in interval (with links except for the current page)
                                var pages = grid.getPagesInInterval(interval, function(pageIndex, isCurrent) {
                                    if (isCurrent)
                                        return "<span id='currentpageindex'>" + (pageIndex + 1) + "</span>";
                                    return $("<a>").css("cursor", "pointer").html(pageIndex + 1).click(function(event) {
                                        grid.setPageIndex(parseInt($(this).html()) - 1);
                                    });
                                });

                                // "first" link
                                var link = $("<a class='nobg'>").html("<i class='fa fa-fast-backward'></i>");
                                if (!grid.canGoBack())
                                    link.css({
                                        opacity: 0.4,
                                        filter: "alpha(opacity=40)"
                                    });
                                else
                                    link.css("cursor", "pointer").click(function(event) {
                                        grid.firstPage();
                                    });
                                paginator.append(link);

                                // "prev" link
                                link = $("<a class='nobg'>").html("<i class='fa fa-backward'></i>");
                                if (!grid.canGoBack())
                                    link.css({
                                        opacity: 0.4,
                                        filter: "alpha(opacity=40)"
                                    });
                                else
                                    link.css("cursor", "pointer").click(function(event) {
                                        grid.prevPage();
                                    });
                                paginator.append(link);

                                // pages
                                for (p = 0; p < pages.length; p++)
                                    paginator.append(pages[p]).append(" ");

                                // "next" link
                                link = $("<a class='nobg'>").html("<i class='fa fa-forward'>");
                                if (!grid.canGoForward())
                                    link.css({
                                        opacity: 0.4,
                                        filter: "alpha(opacity=40)"
                                    });
                                else
                                    link.css("cursor", "pointer").click(function(event) {
                                        grid.nextPage();
                                    });
                                paginator.append(link);

                                // "last" link
                                link = $("<a class='nobg'>").html("<i class='fa fa-fast-forward'>");
                                if (!grid.canGoForward())
                                    link.css({
                                        opacity: 0.4,
                                        filter: "alpha(opacity=40)"
                                    });
                                else
                                    link.css("cursor", "pointer").click(function(event) {
                                        grid.lastPage();
                                    });
                                paginator.append(link);
                            };
                        </script>
                        <script type="text/javascript">
                            var datagrid;
                            window.onload = function() {
                                datagrid = new DatabaseGrid();
                                // key typed in the filter field
                                $("#filter").keyup(function() {
                                    datagrid.editableGrid.filter($(this).val());

                                    // To filter on some columns, you can set an array of column index
                                    //datagrid.editableGrid.filter( $(this).val(), [0,3,5]);
                                });

                                $("#addbutton").click(function() {
                                    datagrid.addRow();
                                });
                            };

                            $(function() {
                                $("#dialog").dialog({
                                    autoOpen: false,
                                    modal: true,
                                    height: 600,
                                    width: 800,
                                    open: function(ev, ui) {
                                        $('#add_iframe').attr('src', $('#dialog').data('src'));
                                    },
                                    close: function(ev, ui) {
                                        location.reload();
                                    }
                                });
                            });
                        </script>
                        <!-- simple form, used to add a new row -->
                        <div id="dialog" style="overflow: hidden;">
                            <iframe id="add_iframe" src="" style="border: none; width: 100%; height: 100%; overflow: hidden;"></iframe>
                        </div>
                    </section>
                </div>
                <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
        </div>
        <!-- nav-tabs-custom -->
        <!-- /.col -->
    </div>
</section>

<div id="modalUploadFile" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Import MDR</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" id="form-upload" enctype="multipart/form-data">
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="email">Browse File</label>
                        <div class="col-sm-9">
                            <input type="file" class="form-control" id="material_file" placeholder="Enter Order Number" name="media">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-offset-3 col-sm-9">
                            <button type="submit" class="btn btn-primary" id="upload_data"> <i id="loading" class='fa fa-circle-o-notch fa-spin' style="display: none;"></i> Upload Data <i class="fa fa-upload" aria-hidden="true"></i></button>
                        </div>
                    </div>
                </form>
            </div>
        </div>

    </div>
</div>

<script type="text/javascript">
    function jc_delete(AUFNR) {
        $('#loader').show();
        var url = '<?php echo base_url() ?>' + 'Projects/crud_mdr_update';
        swal({
                title: "Warning!",
                text: "do you want to delete this record?",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then(function(isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        url: url,
                        method: 'POST',
                        data: {
                            colname: 'IS_ACTIVE',
                            id: AUFNR,
                            newvalue: 0
                        },
                        dataType: 'text',
                        success: function(data) {
                            $('#loader').hide();
                            datagrid.fetchGrid();
                        }
                    })
                }
            })
    }
    $(document).ready(function() {

        $('.table').attr('style', 'margin-bottom: 0px !important;');
        $('table.testgridx').attr('style', 'margin-bottom: 0px !important;'); //.attr('margin-bottom','');
        //.attr('style', 'padding: 0px !important;');
        $('table.testgridx').attr('style', 'margin-top: 0px !important;');
        $("#form-upload").submit(function(evt) {
            $('#upload_data').attr('disabled', true)
            $('#loading').show();
            evt.preventDefault();
            var formData = new FormData($(this)[0]);
            $.ajax({
                url: "<?php echo base_url('Projects/read_xlsx_file_mdr')?>",
                type: 'POST',
                data: formData,
                dataType: 'text',
                async: false,
                cache: false,
                contentType: false,
                enctype: 'multipart/form-data',
                processData: false,
                success: function(data) {
                    // console.log(data);
                    $('#loading').hide();
                    $('#upload_data').attr('disabled', false)
                    var response = $.parseJSON(data);
                    if (response.status === 'success') {
                        datagrid.fetchGrid();
                        swal({
                            title: 'Import Success',
                            icon: 'success',
                            confirmButtonText: 'Ok'
                        })
                        $('#modalUploadFile').modal('hide');
                    } else {
                        swal({
                            title: 'Something Wrong',
                            icon: 'error',
                            confirmButtonText: 'Try Again'
                        })
                    }
                },
                error: function(req, status, code) {
                    swal({
                        title: 'Opss!',
                        text: code + '!',
                        icon: 'error',
                        confirmButtonText: 'Try Again'
                    })
                    $('#loading').hide();
                    $('#upload_data').attr('disabled', false)
                }
            });
            return false;
        });
    })
</script>
<script type="text/javascript">
    // Create Table Header
    var table;
    var arrayData = [];
    var isWrite = [];
    var TypeCell = [];
    var selectk = new Object();
    var obejct2 = {};
    var $tbHeader = $(".trHeader");
    $.ajax({
        url: "<?php echo base_url() ?>" + "projects/crud_mdr_header?edit=" + "<?= $write ?>",
        success: function(json) {
            var output = '';
            var label = '';
            var name = '';
            var xa = 0;
            for (var entry in json['metadata']) {
                // output += 'key: ' + entry + ' | value: ' + json['metadata'][entry] ;
                name = json['metadata'][entry]['name'];
                label = json['metadata'][entry]['label'];
                $tbHeader.append("<th class='editablegrid-" + name + "'>" + label + "</th>");
                // arrayData.push({
                //     data: json['metadata'][entry]['name'],
                //     orderable: true
                // });
                isWrite.push(json['metadata'][entry]['editable']);
                TypeCell.push(json['metadata'][entry]['datatype']);
                if (json['metadata'][entry]['values'] != null) {
                    // Object.assign(select, {"0":"kjkj"} );
                    // obejct2=Object.assign({0+xa: json['metadata'][entry]['values']}, select);
                    selectk[xa] = json['metadata'][entry]['values'];
                }
                xa++;
            };

            obejct2 = new Object(selectk);
            createDataTable();
            $('footer.main-footer').attr('style', 'display:none !important');
            $('.dataTables_info').addClass('col-md-4 pull-left');
            $('.dataTables_filter').addClass('col-md-4 pull-right');
            //.css("margin-up", "0px !important");//.attr('margin-bottom','');
        }
    });
    // console.log("clienta",selectk);
    // end Create Table Header
    function createDataTable() {
        // Setup - add a text input to each footer cell
        $('#tbMDR thead tr').clone(true).appendTo('#tbMDR thead');
        $('#tbMDR thead tr:eq(1) th').each(function(i) {
            // var title = $(this).text();
            $(this).html('<input type="text"  style="width:100%;" class="headFilter" placeholder="Find " />');
            $('input.headFilter', this).keypress(function(e) {

                if (e.which == 13) {
                    // console.log("bb"+table.column(i).search(),"aa"+this.value);
                    if (table.column(i).search() !== this.value && this.value != '-') {
                        // table.ajax.reload();
                        // console.log('d1');
                        table
                            .column(i)
                            .search(this.value)
                            .draw();
                    } else if (this.value == '-') {
                        // console.log('d2');
                        table.column(i).search('^\s*$', true, false).draw();
                    } else if (table.column(i).search() == "") {
                        // console.log('d3');
                        table
                            // .column(i)
                            // .search( this.value )
                            .draw();
                    }
                }
            });
        });
        var x=0;
        var poso;
        table = $('#tbMDR').DataTable({
            dom: 'Rifrt', //add resize column
            // deferRender: true,
            scroller: true,
            info: true,
            orderCellsTop: true, //order in cells top
            // fixedHeader: true,//fixed header
            fixedHeader: {
                header: true,
                headerOffset: $('header').height(),
            },
            processing: true,
            serverSide: false,
            statesave: true,
            paging: false,
            // autoWidth: true,
            // keys: true,
            fixedColumns: {
                leftColumns: 2
            },
            // paging: true,
            // scrollY: '62vh',
            scrollCollapse: true,
            scrollX: "100%",
            "sAjaxSource": "<?php echo base_url() ?>" + "projects/crud_mdr_load_all_new?edit=" + "<?= $write ?>",
            // ajax: {
            //     url: "<?php echo base_url() ?>" + "projects/crud_mdr_load_all_new?edit=" + "<?= $write ?>",
            //     type: "POST",

            // }
            // ,
            // columns: arrayData,
            columnDefs: [{
              targets: [6,9,13,15,17,19,21,25],
              // targets: [6],
              render:
              function(data){
                    var x="";
                    if(data!=null){
                        //check if valid for date
                        if(moment(data, moment.ISO_8601).isValid()){
                            // console.log('risValid');
                            if(moment(data).format('DD MMM YYYY')!='Invalid date'){
                            x= moment(data).format('DD MMM YYYY');
                            // console.log('rFormat');
                        }
                        }else{
                            x=data;
                        }
                    }
                  return x; 
                } 
            } ,{targets:0,className:"text-center"}],
            select: {
                style: 'lightgoldenrodyellow',
                selector: 'td:first-child'
            },
            language: {
                // processing: "<div class='lds-ellipsis'><div></div><div></div><div></div><div></div></div>"
                processing: display_toast('info', 'Wait! Processing Get Data')
            }
            ,
            fnRowCallback: function(nRow, aData, iDisplayIndex, iDisplayIndexFull){
                drawColor(nRow, aData, iDisplayIndex, iDisplayIndexFull);
            }
                ,

            "initComplete": function(settings, json) {
                
                $(".dataTables_scrollBody").floatingScroll();

                $(".dataTables_scrollBody").scroll(function() {
                    x= this.scrollLeft;
                    var pos = $(".fixedHeader-floating").position();
                    //set pos top floating when resize window
                    if (pos) {
                        if (pos.top != $('header').height()) {
                            $(".fixedHeader-floating").css('top', $('header').height());
                        }
                    }
                    //sest pos left floating when scroll bar
                    poso = $(this).offset();
                    if ($(".fixedHeader-floating").is(":visible")) {
                        $(".fixedHeader-floating").offset({
                            left: poso.left - 1 * (this.scrollLeft)
                        });

                    } else {
                    }
                });
                display_toast('success', 'Success Get Data');
            }


        });

        function drawColor(nRow, aData, iDisplayIndex, iDisplayIndexFull){
            if (aData[10] == 'CARRY OUT' && aData[18] == 'CLOSED') {
                        $(nRow).css({
                            'color': 'black',
                            'background-color': '#aeaeae !important'
                        });
                    } else if (aData[10] == 'WAITING MATERIAL') {
                        $(nRow).find('td:eq(10)').css({
                            'background-color': 'magenta',
                            'font-weight': '900'
                        });
                    }else if(aData[10]=='OPEN'){
                    $(nRow).find('td:eq(10)').css({
                            'background-color': 'red',
                            'font-weight': '900'
                        });
                    }else if(aData[10]=='PREPARE FOR TEST'||aData[10]=='PART AVAIL'){
                    $(nRow).find('td:eq(10)').css({
                            'background-color': 'aqua',
                            'font-weight': '900'
                        });
                    }else if(aData[10]=='PERFORM BY PROD'){
                    $(nRow).find('td:eq(10)').css({
                            'background-color': 'pink',
                            'font-weight': '900'
                        });
                    }else if(aData[10]=='PERFORM TO SHOP'){
                    $(nRow).find('td:eq(10)').css({
                            'background-color': 'orange',
                            'font-weight': '900'
                        });
                    }else if(aData[10]=='PREPARE FOR RUN UP'){
                    $(nRow).find('td:eq(10)').css({
                            'background-color': 'tan',
                            'font-weight': '900'
                        });
                    }else if(aData[10]=='PREPARE FOR NDT'){
                    $(nRow).find('td:eq(10)').css({
                            'background-color': 'silver',
                            'font-weight': '900'
                        });
                    }
        }

        function beautifyFixedHeader(event){
               
                var lastx=0;
                // var divwing=true;
                var lastevent="";
                if(event!=lastevent){
                    if ($(".fixedHeader-floating").is(":visible")) {
                        if(x!=0){
                        $(".fixedHeader-floating").offset({
                            left: poso.left - 1 * x
                        });
                        }
                        //add White Screen in Header Fixed
                        var th = $('.fixedHeader-floating');
                        var tr = $('.dataTables_scrollBody');
                        var tc = $('.nav-tabs-custom');
                        $(th).append('<div class="a1" style="background-color:white;position:fixed;left: '+$(tc).offset().left+'px;top: ' + $(th).position().top + 'px;width:' + ((1+$(tc).width()+ parseInt($(tc).css('padding')) - $(tr).width())/2) + 'px;height:' + ($(th).height()+2) + 'px;"></div>');
                        $(th).append('<div class="b1" style="background-color:white;position:fixed;left: ' + ($(tr).offset().left + $(tr).width() ) + 'px;top: ' + $(th).position().top + 'px;width:' + ((1+$(tc).width()+ parseInt($(tc).css('padding')) - $(tr).width())/2) + 'px;height:' + ($(th).height()+2) + 'px;"></div>'); 
                        $(th).append('<div style="background-color:#ecf0f5;position:fixed;left:0px;top: ' + $(th).position().top + 'px;width:' + $('.a1').offset().left + 'px;height:' + ($(th).height()+2) + 'px;"></div>');
                        $(th).append('<div style="background-color:#ecf0f5;position:fixed;left: ' + ($('.b1').offset().left + $('.b1').width()-1 ) + 'px;top: ' + $(th).position().top + 'px;width:' + (($(window).width()- ($('.b1').offset().left + $('.b1').width()) )+5)+ 'px;height:' + ($(th).height()+2) + 'px;"></div>');  
                        //end
                        lastevent=event;
                    }

                    }
        }
        $('#filter').hide();
        $('#tbMDR_filter input').unbind();
        $('#tbMDR_filter input').bind('keyup', function(e) {
            if (e.keyCode == 13) {
                // console.log('d5');
                table.search(this.value).draw();
            }
        });
        $(function() {
            // Check the initial Poistion of the Sticky Header
            var stickyHeaderTop = $('#tbMDR').offset().top-$('header').height()-25;
            var stickyBodyTop = $('#tbMDR tbody').offset().top;
            $(window).resize(function() {
                beautifyFixedHeader('resize');
            });
            $(window).scroll(function() {
                if ($(window).scrollTop() > stickyHeaderTop) {
                    $('.DTFC_LeftHeadWrapper').css('transform', 'translateY(0%)');
                    $('.DTFC_LeftHeadWrapper').css({
                        position: 'fixed',
                        top: $('header').height(),
                        zIndex: '1',
                        left: 'auto',
                        width: $('.DTFC_LeftBodyWrapper').width()
                    });
                   
                    $('.DTFC_ScrollWrapper').css({
                        height: ''
                    });
                    $('.DTFC_LeftBodyWrapper').css({
                        top: $('.DTFC_LeftHeadWrapper').height() - 2
                    });
                     $('.fl-scrolls').css({
                        zIndex :'2'
                    });
                     //add dynamic Title
                   if ($('.title').length == 0) {
                        var activeMenu = $('section.content-header ol.breadcrumb').find('li.active a')[0];
                        $("<div class='title' id='title'>MDR - " + $('.content-header h1').html() + "</div>").insertBefore(".navbar-custom-menu");
                    }
                } else {
                    $('.DTFC_LeftHeadWrapper').css({
                        position: 'relative',
                        top: '0px'
                    });
                    $('.DTFC_LeftBodyWrapper').css({
                        top: '-1px'
                    });
                    $('.fl-scrolls').css({
                        zIndex :'2'
                    });
                    //remove Dynamic Title
                      if ($('.title').length) {
                            $('.title').remove();
                        }
                }
                beautifyFixedHeader('scroll');
            });
        });
        // });
        EditDataTable(table);
    };
    $(document).keyup(function(event) {
        $('.DTFC_LeftHeadWrapper input.headFilter').one('keyup', function (e) {
            var colIndex = $(this).parent().index();
            if (e.which == 13) {
                if (table.column(colIndex).search() !== this.value && this.value != '-') {
                    // console.log('d6');
                    table
                        .column(colIndex)
                        .search(this.value)
                        .draw();
                } else if (this.value == '-') {
                    // console.log('d7');
                    table.column(colIndex).search('^\s*$', true, false).draw();
                } else if (this.value  == "") {
                    // console.log('d8');
                    table.draw();
                }
            }
        });
    });
</script>
<script>
    var base_url = "<?php echo base_url(); ?>projects/crud_mdr_";

    function EditDataTable(datatable) {
        $('#tablecontentx').tableEdit({
            inputCss: {
                padding: '0.75em'
            },
            cellType: {
                0: TypeCell
            },
            select: obejct2,
            editableCols: {
                0: isWrite
            },
            ajax: {
                url: base_url + 'update_new',
                type: 'html',
                success: function(response) {
                    // console.log("client:"+response);
                    var get = response.split("#");
                    var success = get[0].indexOf('ok') != -1;
                    // console.log(response);
                    if (!success) {
                        display_toast('danger', 'Error to update, please check on Column ' + get[4] + ', Order : ' + get[3]);
                    } else {
                        var UpdateTD = $('.last-editable-input').parent('td');
                        // console.log('client',UpdateTD);
                        if (UpdateTD.is("td")) {
                            // console.log('d9');
                            datatable.cell(UpdateTD).data(get[2]).draw();
                            $(UpdateTD).html('').append(get[2]);
                        }
                        display_toast('success', 'Order : ' + get[3] + ', Column ' + get[4] + ' updated to ' + get[2]);

                    }
                    // table.draw();
                },
                error: function(respons) {
                    // console.log("client Error");
                    display_toast('danger', 'Error to update, Server Ajax failure. Please Retry.' + get[4] + ', Order : ' + get[3]);
                    // alert("Server Ajax failure. Please Retry\n" + respons);
                }


            }
        });
    }
</script>