<ul class="nav nav-tabs">

    <?php $menu = $this->session->userdata('menu'); ?>
    <?php foreach ($_SESSION['menu_tab'] as $key => $value) { ?>
        <li <?php echo isActive($content, $value['l'] )?>>
            <a href=" <?php echo base_url() . $value['l'] . '/' . $id_project ?>"> <?php echo $value['m'] ?></a>
        </li>
    <?php } ?>
    
    <?php if ($content == 'admin/projects/crud_daily_menu') { ?>
        <li class="pull-right">
            <a id="print">
                <?php echo "<i class='fa fa-print' aria-hidden='true'></i>" ?>
            </a>
        </li>
    <?php } ?>

</ul>

<?php
    function isActive($content, $path){
        $src = 'admin/' . $path;
        if($content == $src) {
            echo 'class="active"';
        }elseif(($content == "admin/projects/crud_ticket2" || $content == "admin/projects/answer_ticket2" || $content == "admin/projects/create_ticket2") && $src == "admin/ticket/view"){
            echo 'class="active"';
        }
    }
    function tes()
    {
        echo "string";
    }
?>

