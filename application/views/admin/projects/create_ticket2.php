
<?php 
    $this->load->view('admin/projects/header'); 
    $this->load->helper('auth');
    $is_customer = is_customer($session['user_group']);
    $is_edit = $write == 1 ? true : false;
    $id_project = json_encode($id_project);
?>
<style>
    table > thead > tr > th.editablegrid-ANSWER
    {
        color: #0097e6 !important;
    }
</style>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="nav-tabs-custom">
                <?php $this->load->view('admin/projects/menu'); ?>
                <div class="tab-content">
                    <section class="content" style="overflow: auto;">
                    	<div class="row">
					        <div class="col-md-3">
					          	<a href="<?= base_url()?>index.php/ticket/view/<?= json_decode($id_project)?>" class="btn btn-primary btn-block margin-bottom">Back to List Q & A</a>
					        </div>
					        <div class="col-md-9">
					        	<form id="myform" method="post">
					          	<div class="box box-primary">
					            	<div class="box-header with-border">
					              		<h3 class="box-title">Create Q & A</h3>
					            	</div>
					            	<!-- /.box-header -->
					            	<div class="box-body">
					              		<div class="form-group">
					                		<input class="form-control" name="subject" id="subject" placeholder="Subject:">
					              		</div>
					              		<div class="form-group">
					                    	<textarea id="compose-textarea" name="message" class="form-control" style="height: 300px" placeholder="Enter your question in here..."></textarea>
					              		</div>
					              		<div class="form-group">
					                  		<i class="fa fa-paperclip"></i> Attachment File Image
					                  		<input type="file" name="file" id="file" accept="image/x-png,image/gif,image/jpeg">
					                  		<div class="progress-bar progress-bar-success myprogress" role="progressbar" style="width:0%">0%</div>
					              		</div>
				            		</div>
					            	<!-- /.box-body -->
					            	<div class="box-footer">
					              		<div class="pull-right">
					                		<button type="button" id="btn_submit" class="btn btn-primary"><i class="fa fa-envelope-o"></i> Send</button>
					              		</div>
					            	</div>
					            	<!-- /.box-footer -->
					          	</div>
					          	<!-- /. box -->
					      		</form>
					       </div>
					        <!-- /.col -->
					    </div>
                    </section>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
    $(function () {
        $('#btn_submit').click(function () {
            $('.myprogress').css('width', '0');
            //$('.msg').text('');
            var id_project = "<?= json_decode($id_project)?>"
            var subject = $('#subject').val();
            var message = $('#compose-textarea').val();
            var file = $('#file')[0].files[0]
            if (subject.trim() == ''){
                display_toast('danger',"Subject Q&A is required!");
                return;
            }
            if(message.trim() == ''){
            	display_toast('danger',"Message Q&A is required!");
                return;
            }

            var formData = new FormData();
            formData.append('id_project', id_project);
            formData.append('subject', subject);
            formData.append('message', message);
            formData.append('file', file);

            $('#btn_submit').attr('disabled', 'disabled').text("Creating Q & A ...");
            //$('.msg').text('Uploading in progress...');
            $.ajax({
                url: "<?= base_url()?>index.php/ticket/proses_create",
                data: formData,
                processData: false,
                contentType: false,
                type: 'POST',
                xhr: function () {
                    var xhr = new window.XMLHttpRequest();
                    if(file){
	                    xhr.upload.addEventListener("progress", function (evt) {
	                        if (evt.lengthComputable) {
	                            var percentComplete = evt.loaded / evt.total;
	                            percentComplete = parseInt(percentComplete * 100);
	                            $('.myprogress').text(percentComplete + '%');
	                            $('.myprogress').css('width', percentComplete + '%');
	                        }
	                    }, false);
	                    return xhr;
                	}else{
                		return xhr;
                	}
                },
                success: function (data) {
                	if(data){
                		swal({
                            title: "INFO !",
                            text: "Success create Q & A",
                            icon: "success"
                        }).then(function(){
                			$('#btn_submit').removeAttr('disabled').text("Send");
                            window.location.href = "<?= base_url()?>index.php/ticket/view/"+id_project;
                        });
                	}else{
                		swal({
                            title: "WARNING !",
                            text: "Failed create Q & A, please try again!",
                            icon: "warning"
                        }).then(function(){
                			$('#btn_submit').removeAttr('disabled').text("Resend");
                        });
                	}
                    
                }
            });
        });
    });
</script>