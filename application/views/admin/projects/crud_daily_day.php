<?php 
    date_default_timezone_set('Asia/Jakarta');
    $this->load->view('admin/projects/header'); 
    $this->load->helper('text_formatter');
    $this->load->helper('auth');
    $is_customer = is_customer($session["user_group"]);
    $id_project = json_encode($id_project);
    $report_id = json_encode($report_id);
?>
<!-- Main content -->
<div class="content" style="min-height: 0 !important; padding-bottom: 0px">
    <div class="row">
        <div class="col-md-12">
            <!-- Custom Tabs -->
            <div class="nav-tabs-custom">
                <?php $this->load->view('admin/projects/menu'); ?>
                <div class="tab-content">
                    <section class="content" style="min-height: 0 !important">
                        <a href="<?php echo base_url('projects/crud_daily_day/' . json_decode($id_project)) ?>" class="btn btn-info">Back</a>
                        <!-- <button class="btn btn-danger pull-right" id="btn-close" style="display: none; margin-left: 10px">Close This Report</button> -->
                    </section>
                </div>
                <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
        </div>
        <!-- nav-tabs-custom -->
        <!-- /.col -->
    </div>
</div>

<div class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header with-border">
                    <h1 class="box-title report-title"></h1> <span class="label" id="report-status"></span>
                </div>
                 <div class="box-body">
                    <div class="col-md-12">
                        <table class="table table-bordered table-vertical-multi">
                            <tbody>
                                <tr>
                                    <td>Customer</td>
                                    <td><strong><?php echo $project_data->COMPANY_NAME ?></strong></td>
                                    <td>A/C Reg.</td>
                                    <td><strong><?php echo $project_data->TPLNR ?></strong></td>
                                </tr>
                                <tr>
                                    <td>A/C Type</td>
                                    <td><strong><?php echo $project_data->EQART2 ?></strong></td>
                                    <td>Day / Date</td>
                                    <td><strong><?php echo get_diff_days($project_data->REVBD, get_current_date()) + 1 ?></strong></td>
                                </tr>
                                <tr>
                                    <td>Start Date</td>
                                    <td>
                                        <strong>
                                                <?= raw_date_format($project_data->REVBD)?>
                                        </strong>
                                    </td>
                                    <td>RTS plan date</td>
                                    <td> <strong> <?= raw_date_format($project_data->REVED)?> </strong> </td>
                                </tr>
                                <tr>
                                    <td>Maint. Type</td>
                                    <td><strong> <?= get_main_type($project_data->REVTX) ?>  </strong></td>
                                    <td> REVISE RTS date </td>
                                    <td>
                                        <strong>
                                            <span id="REVISE_RTS_DATE"></span>
                                        </strong>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>

                    <div class="col-md-6">
                        <table class="table table-bordered table-vertical-single" id="MAINT_PHASE">
                            <thead>
                                <tr>
                                    <th>Maintenance Phase</th>
                                    <th>%</th>
                                    <th>Remarks</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                            
                        </table>
                    </div>

                    <div class="col-md-6">
                        <table class="table table-bordered table-vertical-single ">
                            <thead>
                                <tr>
                                    <th>Maintenance Milestone</th>
                                    <th>Target</th>
                                    <th>Remarks</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>
                                        <span id="MILESTONE_TITLE_1"></span> 
                                    </td>
                                    <td>
                                        <strong>
                                            <span href="#" id="HANGAR_IN"></span>
                                        </strong>
                                    </td>
                                    <td>
                                        <strong>
                                            <span href="#" id="HANGAR_IN_REMARK"></span>
                                        </strong>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <span id="MILESTONE_TITLE_2"></span>
                                    </td>
                                    <td>
                                        <strong>
                                            <span href="#" id="AC_PRELIMINARY"></span>
                                        </strong>                                                
                                    </td>
                                    <td>
                                        <strong>
                                            <span href="#" id="AC_PRELIMINARY_REMARK"></span>
                                        </strong>                                                
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <span id="MILESTONE_TITLE_3"></span>
                                    </td>
                                    <td>
                                        <strong>
                                            <span href="#" id="AC_DOCKING"></span>
                                        </strong>                                                
                                    </td>
                                    <td>
                                        <strong>
                                            <span href="#" id="AC_DOCKING_REMARK"></span>
                                        </strong>                                                  
                                    </td>
                                </tr>
                                <tr>
                                    <td><span id="MILESTONE_TITLE_4"></span></td>
                                    <td>
                                        <strong>
                                            <span href="#" id="AC_ROLL_OUT"></span>
                                        </strong>                                                
                                    </td>
                                    <td>
                                        <strong>
                                            <span href="#" id="AC_ROLL_OUT_REMARK"></span>
                                        </strong>                                                  
                                    </td>
                                </tr>
                                <tr>
                                    <td><span id="MILESTONE_TITLE_5"></span></td>
                                    <td>
                                        <strong>
                                            <span href="#" id="AC_REDELIVERY"></span>
                                        </strong>                                                
                                    </td>
                                    <td>
                                        <strong>
                                            <span href="#" id="AC_REDELIVERY_REMARK"></span>
                                        </strong>                                                  
                                    </td>
                                </tr>
                                
                            </tbody>
                        </table>
                    </div>

                    <div class="col-md-12">            
                    </div>

                    <div class="col-md-6">
                        <table class="table table-bordered table-vertical-single " id="order_progress">
                            <thead>
                                <tr>
                                    <th> <center> ITEM </center></th>
                                    <th colspan="2"> <center> OPEN </center></th>
                                    <th colspan="2"> <center> PROGRESS </center></th>
                                    <th colspan="2"> <center> CLOSED </center></th>
                                    <th> <center> TOTAL </center></th>
                                </tr>
                            </thead>
                            <tbody>
                                                                
                            </tbody>
                        </table>
                    </div>

                    <div class="col-md-6">
                        <table class="table table-bordered table-vertical-single ">
                            <thead>
                                <tr>
                                    <th> <center> ITEM </center> </th>
                                    <th colspan="2"> <center> OPEN </center> </th>
                                    <th colspan="2"> <center> CANCEL </center> </th>
                                    <th colspan="2"> <center> APPROVED </center> </th>
                                    <th> <center> TOTAL </center> </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>SOA</td>
                                    <td>
                                        <strong>
                                            <span class="soa" id="SOA_OPEN">0</span>
                                        </strong>
                                    </td>
                                    <td>
                                        <strong>
                                            <span id="SOA_OPEN_PERC"></span>
                                        </strong>
                                    </td>
                                    <td>
                                        <strong>
                                            <span class="soa" id="SOA_APPROVED">0</span>
                                        </strong>
                                    </td>
                                    <td>
                                        <strong>
                                            <span id="SOA_APP_PERC"></span>
                                        </strong>
                                    </td>
                                    <td>
                                        <strong>
                                            <span class="soa" id="SOA_CANCEL">0</span>
                                        </strong>
                                    </td>
                                    <td>
                                        <strong>
                                            <span id="SOA_CANCEL_PERC"></span>
                                        </strong>
                                    </td>
                                    <td>
                                        <strong id="SOA_TOTAL">
                                        </strong>                                                
                                    </td>
                                </tr>
                                <tr>
                                    <td>WEA</td>
                                    <td>
                                        <strong>
                                            <span class="soa" id="WEA_OPEN">0</span>
                                        </strong>                                                
                                    </td>
                                    <td>
                                        <strong>
                                            <span id="WEA_OPEN_PERC"></span>
                                        </strong>
                                    </td>
                                    <td>
                                        <strong>
                                            <span class="soa" id="WEA_APPROVED">0</span>
                                        </strong>                                                
                                    </td>
                                    <td>
                                        <strong>
                                            <span id="WEA_APP_PERC"></span>
                                        </strong>
                                    </td>
                                    <td>
                                        <strong>
                                            <span class="soa" id="WEA_CANCEL">0</span>
                                        </strong>                                                
                                    </td>
                                    <td>
                                        <strong>
                                            <span id="WEA_CANCEL_PERC"></span>
                                        </strong>
                                    </td>
                                    <td>
                                        <strong>
                                            <span id="WEA_TOTAL"></span>
                                        </strong>
                                    </td>
                                </tr>                                   
                            </tbody>
                        </table>
                    </div>

                    <div class="col-md-12">
                    <table class="table table-bordered">
                        <tbody>
                            <tr>
                                <td width="30%"><strong> A/C Current Status </strong></td>
                                <td style="background: #e67e22"><center><strong id="CURRENT_STATUS"> </strong></center></td>
                            </tr>                            
                        </tbody>
                    </table>
                    </div>
                </div>               
            </div>
        </div>
    </div>
</div>

<div class="content" id="technical_details">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header with-border">
                    <h1 class="box-title">TECHNICAL  DETAILS</h1>
                </div>

                <br>

                <div class="col-md-12">
                    <table class="table table-bordered" style="width: 50%; margin-left: auto; margin-right: auto;">
                        <tbody>
                            <tr class="cell-green">
                                <td class="text-center"><strong>G</strong></td>
                                <td>Green‐Low Risk; on track</td>
                            </tr> 
                            <tr class="cell-yellow">
                                <td class="text-center"><strong>A</strong></td>
                                <td>Amber‐Medium Risk; on track but need immediate action and more attention</td>
                            </tr> 
                            <tr class="cell-red">
                                <td class="text-center"><strong>R</strong></td>
                                <td>Red‐High Risk; behind schedule, need extra and remedy action</td>
                            </tr>                                
                        </tbody>
                    </table>
                </div>
                
                <?php foreach ($daily_area as $value) { ?>
                    <div class="box-header with-border text-center">
                        <h3 class="box-title">
                            <?php echo $value->NAME ?>
                        </h3>
                        <!-- Hide Btn Add if is Customer -->
                        <?php if(!$is_customer) {?>
                            <a href="" class="btn btn-primary pull-right btn-add" style="margin-right: 15px; display: none;" data-toggle="modal" data-target="#AddNew" data-area="<?php echo $value->NAME?>" data-area-id="<?php echo $value->ID ?>" >
                                <i class="fa fa-plus"></i>
                            </a>
                        <?php } ?>
                    </div>
                    <div class="box-body">
                        <div class="col-md-12">
                            <table class="table table-bordered technical_table resizable" id="details_<?php echo $value->ID ?>">
                                <thead>
                                    <tr>
                                        <th width="30px">No.</th>
                                        <th width="35%">TASK AND PROGRESS</th>
                                        <th width="30%">FOLLOW UP &amp; RESULT</th>
                                        <th width="15%">REMARKS</th>
                                        <th width="10%">STATUS</th>
                                        <th width="5%">LEVEL</th>
                                        <th width="5%">ACTION</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td colspan="6"><center><strong>Loading...</strong></center></td>
                                    </tr>                                     
                                </tbody>
                            </table>
                        </div>
                    </div>                           
                <?php } ?>                
            </div>
        </div>
    </div>
</div>


<div id="AddNew" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
    <!-- Modal content-->
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title"></h4>
        </div>
        <div class="modal-body">
            <form class="form-horizontal" id="details-form">            
                <div class="form-group">
                    <label class="control-label col-sm-3" for="name">Area</label>
                    <div class="col-sm-9">
                        <span class="form-control" id="AREA" data-area="0"></span>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-sm-3" for="name">Task And Progress</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" id="TASK" name="TASK" placeholder="Enter Task And Progress">
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-sm-3" for="name">Follow Up And Result</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" id="FOLLOW_UP" name="FOLLOW_UP" placeholder="Enter Follow Up And Result">
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-sm-3" for="name">Remarks</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" id="REMARKS" name="REMARKS" placeholder="Enter Remarks">
                    </div>
                </div> 

                <div class="form-group">
                    <label class="control-label col-sm-3" for="name">Status</label>
                    <div class="col-sm-9">
                        <select class="form-control" id="STATUS" name="STATUS">
                            <option value="Open">Open</option>
                            <option value="In Progress">In Progress</option>
                            <option value="Close">Close</option>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-sm-3" for="name">Level</label>
                    <div class="col-sm-9">
                            <label class="radio-inline level-style" style="background: #ecf0f1"><input type="radio" name="LEVEL" value="W" checked="true"> </label>
                            <label class="radio-inline level-style" style="background: #2ecc71"><input type="radio" name="LEVEL" value="G"> </label>
                            <label class="radio-inline level-style" style="background: #f1c40f"><input type="radio" name="LEVEL" value="A"> </label>
                            <label class="radio-inline level-style" style="background: #e74c3c"><input type="radio" name="LEVEL" value="R"> </label>
                    </div>
                </div>                                                                                                                                            
            </form>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary" id="btn-save">Save</button>
        </div>
    </div>

    </div>
</div>

<style>
    .editable-click{
        border-bottom: 0px !important;
        cursor: pointer;
    }

    .editable-click::after{
        font-family: FontAwesome;
        content: "\f040";
        display: inline-block;
        visibility: hidden;
        padding-right: 3px;
        vertical-align: middle;
        margin-left: 5px;
        font-size: .8em;

    }

    table:hover .editable-click:after{
        visibility: visible;
    }

    .level-style{
        height: 20px;
        width: 20px;
        border-radius: 10px;
        position: relative;
    }

    .level-style > input[type="radio"] {
        position: absolute;
        top: 0;
        right: 17%;
    }
</style>

<script type="text/javascript">
    var is_customer = '<?= $is_customer ?>' === "1" ? true : false;
    var report_id = <?=$report_id ?>;
    var reportIsOpen = false;
    var daily_day_date = null;
    load_daily_report();

    function load_daily_report(){
        var url = '<?php echo base_url(); ?>' + 'projects/crud_daily_day_load';        

        $.ajax({
            type: 'POST',
            dataType: 'text',
            url: url,
            data: { REVNR: <?= $id_project ?>, REPORT_ID: report_id},
            success: function(data){
                var response = $.parseJSON(data);
                if(response.status === 'success') {
                    appendDataToView(response.body)
                    daily_day_date = response.body.CREATED_AT;
                    if ( response.body.STATUS === 'OPEN' ) {
                        reportIsOpen = true;
                        if(!is_customer){
                            setEditableReport(response.body.STATUS);
                        }
                    }
                    load_all_technical_details();
                    get_project_phase();
                    get_daily_day_progress();
                }
            }
        })
    }

    function appendDataToView(data){
        // General
        $('.report-title').html(data.REPORT_NAME);
        setStatusLabel(data.STATUS);

        $('#REVISE_RTS_DATE').html( isNotNull(data.REVISE_RTS_DATE) ?  formatDate(data.REVISE_RTS_DATE) : isEditable(data.STATUS)); 

        // Date Target
        $('#HANGAR_IN').html( isNotNull(data.HANGAR_IN) ?  formatDate(data.HANGAR_IN) : isEditable(data.STATUS)); 
        $('#AC_PRELIMINARY').html( isNotNull(data.AC_PRELIMINARY) ?  formatDate(data.AC_PRELIMINARY) : isEditable(data.STATUS));
        $('#AC_DOCKING').html( isNotNull(data.AC_DOCKING) ?  formatDate(data.AC_DOCKING) : isEditable(data.STATUS));
        $('#AC_ROLL_OUT').html( isNotNull(data.AC_ROLL_OUT) ?  formatDate(data.AC_ROLL_OUT) : isEditable(data.STATUS));
        $('#AC_REDELIVERY').html( isNotNull(data.AC_REDELIVERY) ?  formatDate(data.AC_REDELIVERY) : isEditable(data.STATUS)); 

        // Remark Options
        $('#HANGAR_IN_REMARK').html( isNotNull(data.HANGAR_IN_REMARK) ?  data.HANGAR_IN_REMARK : isEditable(data.STATUS) );
        $('#AC_PRELIMINARY_REMARK').html( isNotNull(data.AC_PRELIMINARY_REMARK) ?  data.AC_PRELIMINARY_REMARK : isEditable(data.STATUS) );
        $('#AC_DOCKING_REMARK').html( isNotNull(data.AC_DOCKING_REMARK) ?  data.AC_DOCKING_REMARK : isEditable(data.STATUS) );
        $('#AC_ROLL_OUT_REMARK').html( isNotNull(data.AC_ROLL_OUT_REMARK) ?  data.AC_ROLL_OUT_REMARK: isEditable(data.STATUS) );
        $('#AC_REDELIVERY_REMARK').html( isNotNull(data.AC_REDELIVERY_REMARK) ?  data.AC_REDELIVERY_REMARK : isEditable(data.STATUS) ); 


        $('#PRELIMINARY_REMARK').html( isNotNull(data.PRELIMINARY_REMARK) ?  data.PRELIMINARY_REMARK : isEditable(data.STATUS) ); 
        $('#OPENING_REMARK').html( isNotNull(data.OPENING_REMARK) ?  data.OPENING_REMARK : isEditable(data.STATUS) ); 
        $('#INSPECTION_REMARK').html( isNotNull(data.INSPECTION_REMARK) ?  data.INSPECTION_REMARK : isEditable(data.STATUS) ); 
        $('#SERVICING_REMARK').html( isNotNull(data.SERVICING_REMARK) ?  data.SERVICING_REMARK : isEditable(data.STATUS) ); 
        $('#INSTALLATION_REMARK').html( isNotNull(data.INSTALLATION_REMARK) ?  data.INSTALLATION_REMARK : isEditable(data.STATUS) ); 
        $('#OPC_REMARK').html( isNotNull(data.OPC_REMARK) ?  data.OPC_REMARK : isEditable(data.STATUS) ); 


        // SOA
        $('#SOA_OPEN').html( isNotNull(data.SOA_OPEN) ?  data.SOA_OPEN : isEditable(data.STATUS) );
        $('#SOA_APPROVED').html( isNotNull(data.SOA_APPROVED) ?  data.SOA_APPROVED : isEditable(data.STATUS) );
        $('#SOA_CANCEL').html( isNotNull(data.SOA_CANCEL) ?  data.SOA_CANCEL : isEditable(data.STATUS) ); 
        var soa_total = parseFloat(data.SOA_OPEN) + parseFloat(data.SOA_APPROVED) + parseFloat(data.SOA_CANCEL); 
        $('#SOA_TOTAL').html(soa_total);
        $('#SOA_OPEN_PERC').html(get_percentage(data.SOA_OPEN, soa_total) + " %")
        $('#SOA_APP_PERC').html(get_percentage(data.SOA_APPROVED, soa_total) + " %")
        $('#SOA_CANCEL_PERC').html(get_percentage(data.SOA_CANCEL, soa_total) + " %")


        // WEA
        $('#WEA_OPEN').html( isNotNull(data.WEA_OPEN) ?  data.WEA_OPEN : isEditable(data.STATUS) );
        $('#WEA_APPROVED').html( isNotNull(data.WEA_APPROVED) ?  data.WEA_APPROVED : isEditable(data.STATUS) );
        $('#WEA_CANCEL').html( isNotNull(data.WEA_CANCEL) ?  data.WEA_CANCEL : isEditable(data.STATUS) ); 
        var wea_total = parseFloat(data.WEA_OPEN) + parseFloat(data.WEA_APPROVED) + parseFloat(data.WEA_CANCEL); 
        $('#WEA_TOTAL').html(wea_total);
        $('#WEA_OPEN_PERC').html(get_percentage(data.WEA_OPEN, wea_total) + " %")
        $('#WEA_APP_PERC').html(get_percentage(data.WEA_APPROVED, wea_total) + " %")
        $('#WEA_CANCEL_PERC').html(get_percentage(data.WEA_CANCEL, wea_total) + " %")

        // CURRENT STATUS
        $('#CURRENT_STATUS').html( isNotNull(data.CURRENT_STATUS) ?  data.CURRENT_STATUS : isEditable(data.STATUS) )
        $('#MILESTONE_TITLE_1').html( isNotNull(data.MILESTONE_TITLE_1) ?  data.MILESTONE_TITLE_1 : isEditable(data.STATUS) )
        $('#MILESTONE_TITLE_2').html( isNotNull(data.MILESTONE_TITLE_2) ?  data.MILESTONE_TITLE_2 : isEditable(data.STATUS) )
        $('#MILESTONE_TITLE_3').html( isNotNull(data.MILESTONE_TITLE_3) ?  data.MILESTONE_TITLE_3 : isEditable(data.STATUS) )
        $('#MILESTONE_TITLE_4').html( isNotNull(data.MILESTONE_TITLE_4) ?  data.MILESTONE_TITLE_4 : isEditable(data.STATUS) )
        $('#MILESTONE_TITLE_5').html( isNotNull(data.MILESTONE_TITLE_5) ?  data.MILESTONE_TITLE_5 : isEditable(data.STATUS) )
        $('#MILESTONE_TITLE_6').html( isNotNull(data.MILESTONE_TITLE_6) ?  data.MILESTONE_TITLE_6 : isEditable(data.STATUS) )
        
  
        if (data.STATUS === 'CLOSE') {
            $('#PRELIMINARY').html((parseFloat(data.PRELIMINARY).toFixed(2) * 1) + " %")
            $('#OPENING').html((parseFloat(data.OPENING).toFixed(2) * 1) + " %")
            $('#INSPECTION').html((parseFloat(data.INSPECTION).toFixed(2) * 1) + " %")
            $('#SERVICING').html((parseFloat(data.SERVICING).toFixed(2) * 1) + " %")
            $('#INSTALLATION').html((parseFloat(data.INSTALLATION).toFixed(2) * 1) + " %")

            $('#JC_OPEN').html((parseFloat(data.JC_OPEN).toFixed(2) * 1 ))
            $('#PERC_JC_OPEN').html((parseFloat(data.PERC_JC_OPEN).toFixed(2) * 1 ) + " %")
            $('#JC_PROGRESS').html((parseFloat(data.JC_PROGRESS).toFixed(2) * 1 ))
            $('#PERC_JC_PROGRESS').html((parseFloat(data.PERC_JC_PROGRESS).toFixed(2) * 1 ) + " %")
            $('#JC_CLOSED').html((parseFloat(data.JC_CLOSED).toFixed(2) * 1 ))
            $('#PERC_JC_CLOSED').html((parseFloat(data.PERC_JC_CLOSED).toFixed(2) * 1 ) + " %")
            $('#JC_TOTAL').html((parseFloat(data.JC_TOTAL).toFixed(2) * 1 ))
            $('#MDR_OPEN').html((parseFloat(data.MDR_OPEN).toFixed(2) * 1 ))
            $('#PERC_MDR_OPEN').html((parseFloat(data.PERC_MDR_OPEN).toFixed(2) * 1 ) + " %")
            $('#MDR_PROGRESS').html((parseFloat(data.MDR_PROGRESS).toFixed(2) * 1 ))
            $('#PERC_MDR_PROGRESS').html((parseFloat(data.PERC_MDR_PROGRESS).toFixed(2) * 1 ) + " %")
            $('#MDR_CLOSED').html((parseFloat(data.MDR_CLOSED).toFixed(2) * 1 ))
            $('#PERC_MDR_CLOSED').html((parseFloat(data.PERC_MDR_CLOSED).toFixed(2) * 1 ) + " %")
            $('#MDR_TOTAL').html((parseFloat(data.MDR_TOTAL).toFixed(2) * 1 ))


            $('#OPC').html(data.OPC)
        } 


    }

    function setStatusLabel(status){
        $('#report-status').html(status);
        if (status === 'OPEN') {
            $('#report-status').addClass('label-danger');
            $('#report-status').removeClass('label-success');
        } else  {
            $('#report-status').addClass('label-success');
            $('#report-status').removeClass('label-danger');
        }    
    }

    function isNotNull(value){
        return value !== null; 
    }

    function isEditable(status){
        return status === 'OPEN' ? '&nbsp;' : '';
    }

    function formatDate(date){
        return moment(date).format('DD MMM YYYY')
    }

    // Bootstrap Editable Configuration
    var url = '<?php echo base_url(); ?>' + 'projects/crud_daily_day_save';
    var url_phase = '<?php echo base_url(); ?>' + 'projects/update_project_phase';
    var pk = <?= $report_id ?>;

    function setEditableRemarks(url, id, colname){
        return {
            emptytext: '',
            type: 'text',
            pk: id,
            url: url,
            title: 'Enter Remarks',
            name: colname     
        }
    }
    
    var editable_date_conf = {
        emptytext: '',
        type: 'date',
        pk: pk,
        url: url,
        title: 'Enter Date',
        viewformat: 'dd M yyyy',
    }

    var editable_remarks_conf = {
        emptytext: '',
        type: 'text',
        pk: pk,
        url: url,
        title: 'Enter Remarks',     
    }

    var editable_text_conf = {
        emptytext: '',
        type: 'text',
        pk: pk,
        url: url,
        title: 'Input',
    }

    var editable_text_soa_conf = {
        emptytext: '',
        type: 'text',
        pk: pk,
        url: url,
        title: 'Input',
        success: function(response, oldValue, newValue){
            load_daily_report();
        }
    }

    // Set Editable if Report Still Open
    function setEditableReport(status){
        if (status === 'OPEN') {

            $('#REVISE_RTS_DATE').editable(editable_date_conf).removeClass('editable-empty');

            $('#HANGAR_IN').editable(editable_date_conf).removeClass('editable-empty');
            $('#AC_PRELIMINARY').editable(editable_date_conf).removeClass('editable-empty');
            $('#AC_DOCKING').editable(editable_date_conf).removeClass('editable-empty');
            $('#AC_ROLL_OUT').editable(editable_date_conf).removeClass('editable-empty');
            $('#AC_REDELIVERY').editable(editable_date_conf).removeClass('editable-empty');

            // Select Remark
            $('#HANGAR_IN_REMARK').editable(editable_remarks_conf).removeClass('editable-empty');
            $('#AC_PRELIMINARY_REMARK').editable(editable_remarks_conf).removeClass('editable-empty');
            $('#AC_DOCKING_REMARK').editable(editable_remarks_conf).removeClass('editable-empty');
            $('#AC_ROLL_OUT_REMARK').editable(editable_remarks_conf).removeClass('editable-empty');
            $('#AC_REDELIVERY_REMARK').editable(editable_remarks_conf).removeClass('editable-empty');
            $('#PRELIMINARY_REMARK').editable(editable_remarks_conf).removeClass('editable-empty');
            $('#OPENING_REMARK').editable(editable_remarks_conf).removeClass('editable-empty');
            $('#INSPECTION_REMARK').editable(editable_remarks_conf).removeClass('editable-empty');
            $('#SERVICING_REMARK').editable(editable_remarks_conf).removeClass('editable-empty');
            $('#INSTALLATION_REMARK').editable(editable_remarks_conf).removeClass('editable-empty');
            $('#OPC_REMARK').editable(editable_remarks_conf).removeClass('editable-empty');

             $('.soa').editable(editable_text_soa_conf).removeClass('editable-empty');

            $('#CURRENT_STATUS').editable(editable_text_conf).removeClass('editable-empty');             
            $('#MILESTONE_TITLE_1').editable(editable_text_conf).removeClass('editable-empty');             
            $('#MILESTONE_TITLE_2').editable(editable_text_conf).removeClass('editable-empty');             
            $('#MILESTONE_TITLE_3').editable(editable_text_conf).removeClass('editable-empty');             
            $('#MILESTONE_TITLE_4').editable(editable_text_conf).removeClass('editable-empty');             
            $('#MILESTONE_TITLE_5').editable(editable_text_conf).removeClass('editable-empty');             
            $('#MILESTONE_TITLE_6').editable(editable_text_conf).removeClass('editable-empty');             
        } 
    }


    // Set Modal On Click New Btn
    $('.btn-add').on('click', function(){
            var areaId = $(this).data('areaId');
            var area = $(this).data('area');

            $('.modal-title').html(area + '- Add New');    
            $('#AREA').html(area); 
            $('#AREA').data("area", areaId) 
    })

    // Save & Update Technical Details
    $('#btn-save').on('click', function(){

        $(this).prop('disabled', true);

        var operation_type = $(this).text();
        var raw_data = $('#details-form').serializeArray();
        
        var form_data = {};
        form_data['REVNR'] = "<?php echo $project_data->REVNR ?>";
        form_data['AREA'] = $('#AREA').data("area");
        form_data['DAILY_DAY_ID'] = <?= $report_id ?>;

        $(raw_data).each(function(index, obj){
            form_data[obj.name] = obj.value;
        });

        if(operation_type === 'Save'){
            save_new(form_data);
        }
    })

    // Close Report


    // Add New technical Detail
    function save_new(data){
        var url = '<?php echo base_url() ?>' + 'projects/crud_technical_detail';
        $.ajax({
            type: 'POST',
            url: url,
            dataType: 'text',
            data: data,
            success: function(data){
                $('#btn-save').prop('disabled', false);
                var response = $.parseJSON(data);
                if(response.status === 'success'){
                    swal("Record has been Saved!", {
                                icon: "success"
                    });
                    $('#details-form').trigger("reset");
                    $('#AddNew').modal('hide');
                    load_all_technical_details();
                }
                
            }

        })
    }
    
    function load_all_technical_details(){
        $('.technical_table tbody').html('');
        var url = '<?php echo base_url() ?>' + 'projects/crud_technical_details_load_all';

        $.ajax({
            type: 'POST',
            url: url,
            data: { REPORT_ID:  report_id },
            dataType: 'text',
            success: function(data){
                var response = $.parseJSON(data);
                if(response.status === 'success'){
                    append_technical_details_to_table(response.body);
                }
            }
        })
    }

    function append_technical_details_to_table(data){
        $.each(data, function(key, val){
            if(val.length > 0){
                var tbody = document.createElement("tbody");
                $.each(val, function(key2, val2){

                    var tr = document.createElement("tr");
                    var td_sequence = document.createElement("td");
                    var td_task = document.createElement("td");
                    var td_follow_up = document.createElement("td");
                    var td_remarks = document.createElement("td");
                    var td_status = document.createElement("td");
                    var td_level = document.createElement("td");
                    var td_action = document.createElement("td");

                    var span_lv = document.createElement("span");
                    var span_task = document.createElement("span");
                    var span_follow_up = document.createElement("span");
                    var span_status = document.createElement("span");
                    var span_remarks = document.createElement("span");
                    var span_action = document.createElement("span");

                    var btn_delete = document.createElement("button");
                    var fa_delete = document.createElement("i");

                    td_sequence.className = "text-center";

                    span_lv.className = "choose-level";
                    span_task.className = "edit-task";
                    span_follow_up.className = "edit-follow-up";
                    span_status.className = "choose-status";
                    span_remarks.className = "edit-remark";
                    btn_delete.className = "btn btn-sm btn-danger delete";
                    fa_delete.className = "fa fa-trash";


                    span_lv.dataset.pk = val2.ID;
                    span_task.dataset.pk = val2.ID;
                    span_follow_up.dataset.pk = val2.ID;
                    span_remarks.dataset.pk = val2.ID;
                    span_status.dataset.pk = val2.ID;
                    btn_delete.dataset.pk = val2.ID;

                    span_lv.append(set_level_text(val2.LEVEL))
                    span_task.append(val2.TASK)
                    span_follow_up.append(val2.FOLLOW_UP)
                    span_status.append(val2.STATUS)
                    span_remarks.append(val2.REMARKS)
                    btn_delete.append(fa_delete);
                    



                    td_sequence.append(key2+1);    
                    td_task.append(span_task);
                    td_follow_up.append(span_follow_up);
                    td_remarks.append(span_remarks);
                    td_status.append(span_status);
                    td_level.append(span_lv);
                    td_action.append(btn_delete);
                    
                    if(!is_customer){
                        if(reportIsOpen && val2.LEVEL === 'W') {
                            $(btn_delete).show();
                        } else {
                            $(btn_delete).hide();
                        }
                    }else{
                        $(btn_delete).hide();
                    }
                    
                    
                    if(val2.STATUS === 'close') {
                        tr.className = set_level_background(val2.LEVEL) + ' cell-grey-except'
                    } else {
                        tr.className = set_level_background(val2.LEVEL);
                    }

                    tr.append(td_sequence, td_task, td_follow_up, td_remarks, td_status, td_level, td_action);
                    tbody.append(tr);
                    
                }) 

                $(`#details_${key} tbody`).replaceWith(tbody);
                $(`#details_${key}`).resizableColumns();

                if(reportIsOpen && !is_customer){ 
                    setEditableTechnical();
                    $('.btn-add').show(); 
                }
            }else{
                if(reportIsOpen) {
                    $('.btn-add').show(); 
                }
                $(`#details_${key} tbody tr td`).html("<center><strong>No data available in table</strong></center>");
            }
        })
    }

    function set_level_background(level){
        if(level === 'R'){
            return "cell-red-except";
        }else if(level === 'A'){
            return "cell-yellow-except";
        }else if (level === 'G'){
            return "cell-green-except";
        }else{
            return "";
        }
    }

    function toggle_delete_btn(status) {
        
    }

    function set_level_text(level){
        if(level === 'R'){
            return "High Risk";
        }else if(level === 'A'){
            return "Medium Risk";
        }else if (level === 'G'){
            return "Low Risk";
        }else{
            return "Normal";
        }
    }

    function setEditableTechnical(){
        var url = '<?php echo base_url() ?>' + 'projects/crud_technical_details_update'
        $('.choose-level').editable({
            source: [
                {value: 'W', text: 'Normal'},
                {value: 'G', text: 'Low Risk'},
                {value: 'A', text: 'Medium Risk'},
                {value: 'R', text: 'High Risk'},
            ],
            emptytext: '',
            type: 'select',
            name: 'LEVEL',
            url: url,
            title: 'Choose Level',
            success: function(response, newValue){

                if ( $(this).parent().parent().hasClass("cell-red-except") ) {
                    $(this).parent().parent().removeClass("cell-red-except")
                } 

                if ( $(this).parent().parent().hasClass("cell-green-except") ) {
                    $(this).parent().parent().removeClass("cell-green-except")
                } 

                if ( $(this).parent().parent().hasClass("cell-yellow-except") ) {
                    $(this).parent().parent().removeClass("cell-yellow-except")
                }
                
                if (newValue === 'W') {
                    $(this).parent().siblings(":last").children('.btn').show();
                } else {
                    $(this).parent().siblings(":last").children('.btn').hide();
                }

                $(this).parent().parent().addClass(set_level_background(response.body[0].LEVEL));
            }  
        }).removeClass('editable-empty')

        $('.edit-task').editable({
            emptytext: '',
            type: 'text',
            name: 'TASK',
            url: url,
            title: 'Edit Task',
            success: function(response, newValue){

            }
        }).removeClass('editable-empty')

        $('.edit-follow-up').editable({
            emptytext: '',
            type: 'text',
            name: 'FOLLOW_UP',
            url: url,
            title: 'Edit Follow Up & Result',
            success: function(response, newValue){

            }
        }).removeClass('editable-empty')

        $('.edit-remark').editable({
            emptytext: '',
            type: 'text',
            name: 'REMARKS',
            url: url,
            title: 'Edit Remarks',
            success: function(response, newValue){

            }
        }).removeClass('editable-empty')

        $('.choose-status').editable({
            emptytext: '',
            source: [
                {value: 'open', text: 'Open'},
                {value: 'in progress', text: 'In Progress'},
                {value: 'close', text: 'Close'}
            ],
            type: 'select',
            name: 'STATUS',
            url: url,
            title: 'Edit Remarks',
            success: function(response, newValue){
                if(newValue === 'close') {
                    $(this).parent().parent().addClass('cell-grey-except');
                } else {
                    $(this).parent().parent().removeClass('cell-grey-except');
                }
            }
        }).removeClass('editable-empty')

        $('.delete').on('click', function(){
            var pk = $(this).data('pk');
            swal({
                title: "Are you sure?",
                text: "Once deleted, you will not be able to recover this record!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
             })
            .then(function(isConfirm){
                if(isConfirm){ delete_report(pk) }
            }) 
        })

        function delete_report(id){
             $('#loader').show(); 
            var url = '<?php echo base_url('Projects/crud_technical_details_delete') ?>'
            $.ajax({
                type: 'POST',
                url: url,
                data: {'ID': id},
                dataType: 'text',
                success: function(data){
                    $('#loader').hide(); 
                    var response = $.parseJSON(data);                
                    if(response.status === 'success'){
                        load_all_technical_details();
                        swal("Report has been deleted!", {
                            icon: "success",
                        });
                    }
                }
            })
        }


    }

    function count_soa_percentage(){
        var soa_open        = $('#SOA_OPEN').text();
        var soa_approved    = $('#SOA_APPROVED').text();
        var soa_cancel      = $('#SOA_CANCEL').text();

        var total = parseFloat(soa_open) + parseFloat(soa_approved) + parseFloat(soa_cancel);

        var open_perc = (parseFloat(soa_open) / total) * 100;
        var approved_perc = (parseFloat(soa_approved) / total) * 100;
        var cancel_perc = (parseFloat(soa_cancel) / total) * 100;
    }

    function get_project_phase(){
        var url = '<?php echo base_url('Projects/get_project_phase')?>';
        var data = {
            ID : report_id,
            REVNR: '<?php echo $id_project ?>',
            STATUS: reportIsOpen ? 'OPEN' : 'CLOSE'
        }
        $.ajax({
            url: url,
            method: 'POST',
            dataType: 'text',
            data: data,
            success: function(data){
                var response = $.parseJSON(data);
                if(response.status === 'success'){
                    append_phase(response.body);
                }

            }
        })

    }

    function append_phase(data){
        var phase_table = $('#MAINT_PHASE tbody');
        phase_table.html('');
        if(data.length > 0) {
            $.each(data, function(key, val){
                var REMARKS = val.REMARKS !== null ? val.REMARKS : ''; 
                var content = "" +
                "<tr>"+
                    "<td>" + val.MAINT_NAME + "</td>" +
                    "<td>" + parseFloat(val.PROGRESS).toFixed(2) * 1 + " %</td>" +
                    "<td> <span class='phase-remark' id="+val.ID+">" + REMARKS + "</span></td>" +
                "</tr>"

                if(reportIsOpen && !is_customer){
                    setInterval(function(){
                        $('#'+val.ID).editable(setEditableRemarks(url_phase, val.ID, "REMARKS")).removeClass('editable-empty')
                    }, 500)
                }
                
                phase_table.append(content);
            })
        }
    }

    function get_daily_day_progress(){
        var date = moment(daily_day_date, "YYYY-MM-DD").format("YYYY-MM-DD");
        var url = '<?php echo base_url('Projects/get_daily_day_progress')?>';
        var data = {
            ID : report_id,
            REVNR: <?= $id_project ?>,
            STATUS: reportIsOpen ? 'OPEN' : 'CLOSE',
            DATE: date
        }
        $.ajax({
            url: url,
            method: 'POST',
            dataType: 'text',
            data: data,
            success: function(data){
                var response = $.parseJSON(data);
                append_order(response.body)
            }
        })

    }

    function append_order(data){
        var order_table = $('#order_progress tbody');
        order_table.html('');
        if(data !== null) {
            $.each(data, function(key, val){
                if(val.TYPE === 'ORDER') {
                    var content = "" +
                    "<tr>" +
                        "<td>" + val.ITEM + "</td>" +
                        "<td>" + val.OPEN + "</td>" +
                        "<td>" + get_percentage(val.OPEN, val.TOTAL) + " %</td>" +
                        "<td>" + val.PROGRESS + "</td>" +
                        "<td>" + get_percentage(val.PROGRESS, val.TOTAL) + " %</td>" +
                        "<td>" + val.CLOSE + "</td>" +
                        "<td>" + get_percentage(val.CLOSE, val.TOTAL) + " %</td>" +
                        "<td>" + val.TOTAL + "</td>" +
                    "</tr>" 
                }
                order_table.append(content);
            })
        } else {
            var content = "" +
                    "<tr>" +
                        "<td> JOBCARD </td>" +
                        "<td>0</td>" +
                        "<td>0 %</td>" +
                        "<td>0</td>" +
                        "<td>0 %</td>" +
                        "<td>0</td>" +
                        "<td>0 %</td>" +
                        "<td>0</td>" +
                    "</tr>" 
                    "<tr>" +
                        "<td> JOBCARD </td>" +
                        "<td>0</td>" +
                        "<td>0 %</td>" +
                        "<td>0</td>" +
                        "<td>0 %</td>" +
                        "<td>0</td>" +
                        "<td>0 %</td>" +
                        "<td>0</td>" +
                    "</tr>"
            order_table.append(content); 
        }
        
    }

    function get_percentage(number, divider) {
        return number != 0 ? ((number / divider) * 100).toFixed(2) : 0;
    }

    function calculateSoa(){
        var open = $('#SOA_OPEN').text();
        var approved = $('#SOA_APPROVED').text();
        var cancel = $('#SOA_CANCEL').text();
        console.log(parseInt(open) + parseInt(approved) + parseInt(cancel))
    }
    
    
</script>