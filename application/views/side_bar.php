<section class="sidebar">
    <!-- Sidebar user panel -->
    <div class="user-panel">
        <div class="pull-left image">
            <img src="<?php echo base_url(); ?>assets/dist/img/man.png" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
            <p><?php echo $session['name'] ?></p>
            <a href="#"><i class="fa fa-user"></i></a>
            <?php echo $session['user_group'] ?>
        </div>
    </div>
    <!-- /.search form -->
    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu" data-widget="tree">
        <?php
        $data_menu = json_decode($_COOKIE['menu'],true);
        $modul = array_keys($data_menu);
        for($a=0;$a<count($modul);$a++){
        ?>
        <?php if(count($data_menu[$modul[$a]]) == 1){ $menu=explode("=",$data_menu[$modul[$a]][0]['m']);?>
        <li class="#">
        <a href="<?= base_url("index.php")."/$menu[1]"?>">
        <?php } else { echo "<li class='treeview'><a href='javascript:;'>"; }?>
          <i class="<?php $logo = explode("=", $modul[$a]); echo $logo[1]?>"></i> <span><?= $logo[0]?></span>
          <?php if(count($data_menu[$modul[$a]]) != 1){?>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
            </span>
          <?php }?>
        </a>
        <?php 
          if(count($data_menu[$modul[$a]]) != 1){
            echo "<ul class='treeview-menu'>";
            for($b=0;$b<count($data_menu[$modul[$a]]);$b++){
              $menu2 = explode("=", $data_menu[$modul[$a]][$b]['m']);
        ?>
          <li>
            <a href="<?= base_url('index.php')."/$menu2[1]"?>"> 
              <i class="<?= $data_menu[$modul[$a]][$b]['l']?>"></i>
              <span><?= $menu2[0]?></span>
            </a>
          </li>
        <?php
            }
            echo "</ul>";
          }
        ?>
        </li>
        <?php
        }
        ?>
        <hr>





        <!-- <?php $menu = $this->session->userdata('menu'); ?>
        <?php foreach ($menu['side_bar'] as $value) { ?>
        <li class="#">
            <a href="<?php echo base_url() . $value['PATH']; ?>">
                <i class='<?php echo $value['MENU_ID']?>'></i> <span><?php echo $value['LABEL']?></span>
            </a>
        </li>
        <?php } ?>
        <li class="treeview">
            <a href="#"><i class="fa fa-gears"></i><span>Utility</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
            </span>
            </a>      
            <ul class="treeview-menu">
                <li>
                    <a href="<?php echo base_url(); ?>admin/user_management/crud_users">
                        <i class="fa fa-user"></i> <span>Users Management</span>
                    </a>
                </li>           
            </ul>
        </li>
        <li class="treeview">
            <a href="#"><i class="fa fa-database"></i><span>Master Data</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
            </span>
            </a>      
            <ul class="treeview-menu">
                <li>
                    <a href="<?php echo base_url(); ?>admin/master_data/crud_material">
                        <i class="fa fa-user"></i> <span>Material</span>
                    </a>
                </li>           
            </ul>        
        </li> -->
    </ul>
</section>