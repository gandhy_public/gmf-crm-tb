<section class="content-header">
    <h1>
        Synchronize Job List
    </h1>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header with-border">
                    <div class="box-tools">
                        <button class="btn btn-info" onclick="update_bw()" >Synchronize</button>
                        <button class="btn btn-default" onclick="update_bw_check()">Status Check</button>
                    </div>
                    <!-- /.box-tools -->
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table class="table table-bordered table-hover dataTable" id="material-list">
                        <thead>
                            <tr>
                                <th width="20px">No.</th>
                                <th width="10%">Project</th>
                                <th>Date Created</th>
                                <th width="20%">Exec By</th>
                                <th width="20%">Job Status</th>
                                <th width="20%">Interface Status</th>
                            </tr>
                        </thead>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- nav-tabs-custom -->
        <!-- /.col -->
    </div>
</section>

<style>
    .box-header > .box-title, .box-header > .box-tools {
        position: relative;
        display: inline-block;
        vertical-align: middle;
        right: 0;
        top:0;
    }

</style>


<script type="text/javascript">
	var report_table = $('#material-list').dataTable({
		paging: false
	});

	load_report();
	function load_report(){
		var url = '<?php echo base_url() ?>' + '/Sapi/get_job_list'
		$.ajax({
			type: 'POST',
			url: url,
			dataType: 'text',
			success: function(data){
				var response = $.parseJSON(data);
				if(response.status === 'success') {
					performing_datatables(response.body);
				}
			}
		})
	}


	function performing_datatables(data){
		report_table = $('#material-list').dataTable({
		    destroy: true,
            data: data,
            order: [[ 3, "desc"]],
		    columns: [
                { title: 'No', data: 'ID' },
                { title: 'Project', data: 'REVISION' },
		        { title: 'Date Created', data: 'DTIME', render: function(data, type, row){
                    return moment(data, "YYYY-MM-DD HH:mm").format("HH:mm, DD MMM YYYY");
                }},
		        { title: 'Execute By', data: 'EXEC_BY' },
		        { title: 'Job Status', data: 'JOBSTATUS', render: function(data, type, row){
                    
                    /* SAP Response Status
                    Running = R
                    Ready = Y
                    Schedule = P
                    Release = S
                    Cancel = A
                    Finish = F
                    Put Active = Z
                    */

                    if (data === 'F' ){
                        return '<center><span class="label label-success" id="report-status">Finish</span></center>'
                    } else if (data === 'A') {
                        return '<center><span class="label label-danger" id="report-status">Error</span></center>'
                    } else if (data === 'R' || data === null){
                        return '<center><span class="label label-warning" id="report-status">Running</span></center>'
                    }
                }},
		        { title: 'Interface Status', data: 'RFCSTATUS', render: function(data, type, row){
                    if (data === 'CREATED' ){
                        return '<center><span class="label label-warning" id="report-status">Created</span></center>'
                    } else if (data === 'CANCEL') {
                        return '<center><span class="label label-danger" id="report-status">Cancel</span></center>'
                    } else {
                        return '<center><span class="label label-success" id="report-status">Done</span></center>'
                    }
                }}
		    ]     
		})
    }
    
    
    function update_bw_by_revnr(){
        swal({
            text: 'Syncronize By Project Number',
            content: "input",
            button: {
                text: "Syncronize!",
                closeModal: false,
            },
        })
        .then(rev => {
            if (!rev) throw null;
            update_bw_by_revision(rev)
        })

    }



</script>