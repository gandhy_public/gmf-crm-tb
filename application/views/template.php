<!DOCTYPE html>
<html>
    <head>
        <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/dist/img/favicon.ico">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>CRM TB</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <?php if (isset($is_grocery)): ?>
            <?php foreach ($css_files as $file): ?>
                <link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
            <?php endforeach; ?>
            <?php foreach ($js_files as $file): ?>
                <script src="<?php echo $file; ?>"></script>
            <?php endforeach; ?>
        <?php endif; ?>

        <!-- editgrid -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/editablegrid/css/style.css" type="text/css" media="screen">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/editablegrid/css/responsive.css" type="text/css" media="screen">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/editablegrid/css/font-awesome-4.7.0/css/font-awesome.min.css" type="text/css" media="screen">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/editablegrid/css/richtext.min.css" type="text/css" media="screen">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/editablegrid/css/select2.min.css" type="text/css" media="screen">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/custom.css" type="text/css" media="screen">

        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/jquery-ui/css/smoothness/jquery-ui-1.10.4.custom.min.css" />

        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/sweetalert/sweetalert.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/sweetalert/swal-forms.css">

        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/bootstrap/dist/css/bootstrap.min.css">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/font-awesome/css/font-awesome.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/Ionicons/css/ionicons.min.css">
        <!-- jvectormap -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/jvectormap/jquery-jvectormap.css">

        <!-- Theme style -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/AdminLTE.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/loader.css">
        <!-- AdminLTE Skins. Choose a skin from the css/skins
             folder instead of downloading all of them to reduce the load. -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/datatables.net/css/buttons.dataTables.min.css">
        <!-- <link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css"> -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
        <!-- <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css"> -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/skins/_all-skins.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/bootstrap-daterangepicker/daterangepicker.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
        <!-- Bootstrap 3.3.6 -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
        <!-- Font Awesome -->
        <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css"> -->

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">


        <!-- Morris charts -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/morris.js/morris.css">
        <!-- HandsOnTable -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/handsontable/css/handsontable.full.min.css">



        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

        <!-- Google Font -->
        <link rel="stylesheet"
              href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

        <style type="text/css">
            @font-face {
                font-family: lato;
                src: url(<?php echo base_url(); ?>assets/dist/css/lato.ttf);
            }

            @font-face {
                font-family: panton;
                src: url(<?php echo base_url(); ?>assets/dist/css/panton.otf);
            }

            body * {
                font-family: panton;
            }

            a.sundaboy:link {color:#FF0000;}

            th {
                white-space: nowrap !important;
            }

            .example-modal .modal {
                position: relative;
                top: auto;
                bottom: auto;
                right: auto;
                left: auto;
                display: block;
                z-index: 1;
            }

            .example-modal .modal {
                background: transparent !important;
            }

            #tablecontent th div {
                resize: horizontal;
                overflow: hidden;
                margin: 0px;
                padding: 2px;
                display: block;
            }

            #tablecontent th {
                margin: 0px;
                padding: 0px;
            }

            #tablecontent td:hover {
                color: black;
            }

            #tablecontent thead tr:hover a {
                color: blue;
            }

            #tablecontent tr:hover {
                color: blue;
                background-color: lightgoldenrodyellow;
            }

            #tablecontent tr:hover a {
                color: red;
                font-weight: bold;
            }

            #tablecontent td {
                /* min-width: 130px; */
                max-width: 500px;
                overflow: hidden;
                /* white-space: nowrap !important; */
                white-space: normal !important; 
                vertical-align: top;
            }

            .td-close:not(:last-child) {
                background-color: #aeaeae !important;
                font-weight: bold;
                border-bottom: 1px solid #aeaeae !important;
            }

            .navbar-static-top{
                position: relative;
            }

            .modal-dialog {
                margin: 10vh auto 0px auto;
            }

            .loader {
                display: none;
                position: absolute;
                left: 50%;
                margin-left: -30px;
                top: 10px;
                z-index: -1;
                border: 4px solid #ffffff;
                border-top: 4px solid #3498db;
                border-radius: 50%;
                width: 30px;
                height: 30px;
                animation: spin 2s linear infinite;
            }

            @keyframes spin {
                0% { transform: rotate(0deg); }
                100% { transform: rotate(360deg); }
            }

            table.DTFC_Cloned{
                margin-bottom: 0px !important;
                 margin-top: 0px !important;
            }
            table.DTFC_Cloned thead,
             .testgridx thead {
              background-color: lightgrey;
            }
            .testgridx tbody{
                background-color: #fff;
            }
            .testgridx th { font-size: 12px; }
            .testgridx td { font-size: 11px; }
          

        </style>


        <!-- Custom Style -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/custom.css">



        <!-- jQuery 3 -->
        <?php if (!isset($is_grocery)): ?>
            <script src="<?php echo base_url(); ?>assets/editablegrid/js/jquery-1.12.4.min.js" ></script>
             <!-- editable js -->
            <script src="<?php echo base_url(); ?>assets/editable/js/table.js"></script>
            <script src="<?php echo base_url(); ?>assets/jquery-ui/js/jquery-ui-1.10.4.custom.min.js"></script>
            <script src="<?php echo base_url(); ?>assets/grocery_crud/texteditor/ckeditor/ckeditor.js"></script>
            <script src="<?php echo base_url(); ?>assets/grocery_crud/texteditor/ckeditor/adapters/jquery.js"></script>
            <script src="<?php echo base_url(); ?>assets/editablegrid/js/jquery.richtext.min.js"></script>
            <script src="<?php echo base_url(); ?>assets/editablegrid/js/select2.min.js"></script>
           
            <!-- editgrid js -->
            <script src="<?php echo base_url(); ?>assets/editablegrid/js/editablegrid.js"></script>
            <script src="<?php echo base_url(); ?>assets/editablegrid/js/editablegrid_charts.js"></script>
            <script src="<?php echo base_url(); ?>assets/editablegrid/js/editablegrid_charts_ofc.js"></script>
            <script src="<?php echo base_url(); ?>assets/editablegrid/js/editablegrid_editors.js"></script>
            <script src="<?php echo base_url(); ?>assets/editablegrid/js/editablegrid_renderers.js"></script>
            <script src="<?php echo base_url(); ?>assets/editablegrid/js/editablegrid_utils.js"></script>
            <script src="<?php echo base_url(); ?>assets/editablegrid/js/editablegrid_validators.js"></script>
            <!-- EditableGrid test if jQuery UI is present. If present, a datepicker is automatically used for date type -->
        <?php endif; ?>

        <script src="<?php echo base_url(); ?>assets/handsontable/js/handsontable.full.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/bower_components/fastclick/lib/fastclick.js"></script>
        <script src="<?php echo base_url(); ?>assets/dist/js/adminlte.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>

        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/floatingscroll/jquery.floatingscroll.css">
        <script src="<?php echo base_url(); ?>assets/floatingscroll/jquery.floatingscroll.min.js" ></script>

        <!-- Floating Scroll -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/floating-scroll/src/jquery.floatingscroll.css">
        <script src="<?php echo base_url(); ?>assets/dist/js/floating-scroll-bar.js" ></script>

        <!-- Sweet Alert -->
        <script src="<?php echo base_url(); ?>assets/dist/js/sweetalert.min.js" ></script>
        <script src="<?php echo base_url(); ?>assets/sweetalert/swal-forms.js" ></script>

        <!-- Div to PDF -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.5/jspdf.debug.js" integrity="sha384-CchuzHs077vGtfhGYl9Qtc7Vx64rXBXdIAZIPbItbNyWIRTdG0oYAqki3Ry13Yzu" crossorigin="anonymous"></script>

        <!-- Momment JS -->
        <script src="<?php echo base_url(); ?>assets/bower_components/moment/moment.js"></script>

        <script src="<?php echo base_url(); ?>assets/bower_components/datatables.net/js/jquery.dataTables.js"></script>
          <link href="<?php echo base_url(); ?>assets/bower_components/datatables.net/css/fixedHeader.dataTables.min.css" rel="stylesheet">
         <script src="<?php echo base_url(); ?>assets/bower_components/datatables.net/js/dataTables.fixedHeader.min.js"></script>
         <!-- <script src="<?php echo base_url(); ?>assets/bower_components/datatables.net/js/ColReorderWithResize.js"></script> -->
         <script src="<?php echo base_url(); ?>assets/bower_components/datatables.net/js/datetime.js"></script>
          <!--  <link href="<?php echo base_url(); ?>assets/bower_components/datatables.net/css/fixedColumns.dataTables.min.css" rel="stylesheet"> -->
          <script src="<?php echo base_url(); ?>assets/bower_components/datatables.net/js/dataTables.fixedColumns.min.js"></script>
       
        <!-- Bootsrap Editable Table -->
        <link href="<?php echo base_url(); ?>assets/plugins/bootstrap-editable/css/bootstrap-editable.css" rel="stylesheet">
        <script src="<?php echo base_url(); ?>assets/plugins/bootstrap-editable/js/bootstrap-editable.js"></script>
        <script src="<?php echo base_url(); ?>assets/dist/js/jQuery.resizableColumns.js"></script>

        <!-- Simple Toast -->
        <link href="<?php echo base_url(); ?>assets/dist/css/bootoast.min.css" rel="stylesheet">
        <script src="<?php echo base_url(); ?>assets/dist/js/bootoast.min.js"></script>


        <!-- include summernote css/js -->
        <link href="<?php echo base_url(); ?>assets/summernote/summernote.css" rel="stylesheet">
        <script src="<?php echo base_url(); ?>assets/summernote/summernote.js"></script>

        <!-- CKEditor -->
        <!-- <link href="<?php echo base_url(); ?>assets/summernote/summernote.css" rel="stylesheet"> -->
        <script src="<?php echo base_url(); ?>assets/ckeditor/ckeditor.js"></script>

        <!-- SHEET EXCEL -->
        <script src="<?php echo base_url(); ?>assets/dist/js/xlsx.full.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/dist/js/FileSaver.js"></script>
       

    </head>
    <body class="hold-transition skin-purple-light sidebar-mini sidebar-collapse fixed">
        <div class="wrapper">

            <header class="main-header">

                <!-- Logo -->
                <a href="#" class="logo">
                    <!-- mini logo for sidebar mini 50x50 pixels -->
                    <span class="logo-mini"><img src="<?php echo base_url(); ?>assets/dist/img/logokecil.png" width="50"/></span>
                    <!-- logo for regular state and mobile devices -->
                    <span class="logo-lg"><b><img src="<?php echo base_url(); ?>assets/dist/img/logokecil.png" width="50"/></b> <b>CRM</b> TB</span>
                </a>

                <!-- Header Navbar: style can be found in header.less -->
                <nav class="navbar navbar-static-top">
                    <!-- Sidebar toggle button-->
                    <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                        <span class="sr-only">Toggle navigation</span>
                    </a>

                    <div class="loader" id="loader"></div>

                    <!-- Navbar Right Menu -->
                    <div class="navbar-custom-menu">
                        <ul class="nav navbar-nav">
                            <!-- Messages: style can be found in dropdown.less-->

                            <!-- Notifications: style can be found in dropdown.less -->
                            <li class="dropdown notifications-menu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" onclick="get_notif()">
                                    <i class="fa fa-bell-o"></i>
                                    <span class="label label-warning" id="notif-count">&nbsp;</span>
                                </a>
                                <ul class="dropdown-menu">
                                    <li class="header">You have <strong id="notif-count-span">&nbsp;</strong> notifications</li>
                                    <li>
                                        <!-- inner menu: contains the actual data -->
                                        <ul class="menu" id="notif-detail">
                                            <center><img width="32" src="<?php echo base_url(); ?>assets/dist/img/notif-load.gif" /></center>
                                        </ul>
                                    </li>
                                    <!--<li class="footer"><a href="#">View all</a></li>-->
                                </ul>
                            </li>

                            <!-- Tasks: style can be found in dropdown.less -->

                            <!-- User Account: style can be found in dropdown.less -->
                            <li class="dropdown user user-menu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <img src="<?php echo base_url(); ?>assets/dist/img/man.png" class="user-image" alt="User Image">
                                    <span class="hidden-xs"><?php echo $session['name'] ?></span>
                                </a>
                                <ul class="dropdown-menu">
                                    <!-- User image -->
                                    <li class="user-header">
                                        <img src="<?php echo base_url(); ?>assets/dist/img/man.png" class="img-circle" alt="User Image">
                                        <p><?php echo $session['name'] ?></p>
                                        <p>
                                            <i class="fa fa-user"></i>&nbsp;
                                            <?php echo $session['user_group'] ?>
                                        </p>
                                    </li>
                                    <!-- Menu Footer-->
                                    <li class="user-footer">
                                        <div class="pull-left">
                                            <a href="#" class="btn btn-default btn-flat">Profile</a>
                                        </div>
                                        <div class="pull-right">
                                            <a href="<?php echo base_url(); ?>login/logout" class="btn btn-default btn-flat">Sign out</a>
                                        </div>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>

                </nav>
            </header>
            <!-- Left side column. contains the logo and sidebar -->
            <aside class="main-sidebar">
                <!-- sidebar: style can be found in sidebar.less -->
                <?php $this->load->view('side_bar'); ?>
                <!-- /.sidebar -->
            </aside>
            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <!-- Content Header (Page header) -->
                <?php $this->load->view($content); ?>
                <!-- /.content -->
            </div>
            <!-- /.content-wrapper -->

            <footer class="main-footer">
                <div class="pull-right hidden-xs">
                    <b>Version</b> 2.4.0
                </div>
                <strong>Copyright &copy; 2018.</strong> All rights
                reserved.
            </footer>

            <!-- Control Sidebar -->
            <aside class="control-sidebar control-sidebar-dark">
                <!-- Create the tabs -->
                <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
                    <li><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
                    <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
                </ul>
                <!-- Tab panes -->
                <div class="tab-content">
                    <!-- Home tab content -->
                    <div class="tab-pane" id="control-sidebar-home-tab">
                        <h3 class="control-sidebar-heading">Recent Activity</h3>
                        <ul class="control-sidebar-menu">
                            <li>
                                <a href="javascript:void(0)">
                                    <i class="menu-icon fa fa-birthday-cake bg-red"></i>

                                    <div class="menu-info">
                                        <h4 class="control-sidebar-subheading">Langdon's Birthday</h4>

                                        <p>Will be 23 on April 24th</p>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:void(0)">
                                    <i class="menu-icon fa fa-user bg-yellow"></i>

                                    <div class="menu-info">
                                        <h4 class="control-sidebar-subheading">Frodo Updated His Profile</h4>

                                        <p>New phone +1(800)555-1234</p>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:void(0)">
                                    <i class="menu-icon fa fa-envelope-o bg-light-blue"></i>

                                    <div class="menu-info">
                                        <h4 class="control-sidebar-subheading">Nora Joined Mailing List</h4>

                                        <p>nora@example.com</p>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:void(0)">
                                    <i class="menu-icon fa fa-file-code-o bg-green"></i>

                                    <div class="menu-info">
                                        <h4 class="control-sidebar-subheading">Cron Job 254 Executed</h4>

                                        <p>Execution time 5 seconds</p>
                                    </div>
                                </a>
                            </li>
                        </ul>
                        <!-- /.control-sidebar-menu -->

                        <h3 class="control-sidebar-heading">Tasks Progress</h3>
                        <ul class="control-sidebar-menu">
                            <li>
                                <a href="javascript:void(0)">
                                    <h4 class="control-sidebar-subheading">
                                        Custom Template Design
                                        <span class="label label-danger pull-right">70%</span>
                                    </h4>

                                    <div class="progress progress-xxs">
                                        <div class="progress-bar progress-bar-danger" style="width: 70%"></div>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:void(0)">
                                    <h4 class="control-sidebar-subheading">
                                        Update Resume
                                        <span class="label label-success pull-right">95%</span>
                                    </h4>

                                    <div class="progress progress-xxs">
                                        <div class="progress-bar progress-bar-success" style="width: 95%"></div>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:void(0)">
                                    <h4 class="control-sidebar-subheading">
                                        Laravel Integration
                                        <span class="label label-warning pull-right">50%</span>
                                    </h4>

                                    <div class="progress progress-xxs">
                                        <div class="progress-bar progress-bar-warning" style="width: 50%"></div>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:void(0)">
                                    <h4 class="control-sidebar-subheading">
                                        Back End Framework
                                        <span class="label label-primary pull-right">68%</span>
                                    </h4>

                                    <div class="progress progress-xxs">
                                        <div class="progress-bar progress-bar-primary" style="width: 68%"></div>
                                    </div>
                                </a>
                            </li>
                        </ul>
                        <!-- /.control-sidebar-menu -->

                    </div>
                    <!-- /.tab-pane -->

                    <!-- Settings tab content -->
                    <div class="tab-pane" id="control-sidebar-settings-tab">
                        <form method="post">
                            <h3 class="control-sidebar-heading">General Settings</h3>

                            <div class="form-group">
                                <label class="control-sidebar-subheading">
                                    Report panel usage
                                    <input type="checkbox" class="pull-right" checked>
                                </label>

                                <p>
                                    Some information about this general settings option
                                </p>
                            </div>
                            <!-- /.form-group -->

                            <div class="form-group">
                                <label class="control-sidebar-subheading">
                                    Allow mail redirect
                                    <input type="checkbox" class="pull-right" checked>
                                </label>

                                <p>
                                    Other sets of options are available
                                </p>
                            </div>
                            <!-- /.form-group -->

                            <div class="form-group">
                                <label class="control-sidebar-subheading">
                                    Expose author name in posts
                                    <input type="checkbox" class="pull-right" checked>
                                </label>

                                <p>
                                    Allow the user to show his name in blog posts
                                </p>
                            </div>
                            <!-- /.form-group -->

                            <h3 class="control-sidebar-heading">Chat Settings</h3>

                            <div class="form-group">
                                <label class="control-sidebar-subheading">
                                    Show me as online
                                    <input type="checkbox" class="pull-right" checked>
                                </label>
                            </div>
                            <!-- /.form-group -->

                            <div class="form-group">
                                <label class="control-sidebar-subheading">
                                    Turn off notifications
                                    <input type="checkbox" class="pull-right">
                                </label>
                            </div>
                            <!-- /.form-group -->

                            <div class="form-group">
                                <label class="control-sidebar-subheading">
                                    Delete chat history
                                    <a href="javascript:void(0)" class="text-red pull-right"><i class="fa fa-trash-o"></i></a>
                                </label>
                            </div>
                            <!-- /.form-group -->
                        </form>
                    </div>
                    <!-- /.tab-pane -->
                </div>
            </aside>
            <!-- /.control-sidebar -->
            <!-- Add the sidebar's background. This div must be placed
                 immediately after the control sidebar -->
            <div class="control-sidebar-bg"></div>

        </div>
        <!-- ./wrapper -->
        <script type="text/javascript">
//             var notif_buffer;
//             setInterval(function () {
//                 jQuery.getJSON('<?php echo base_url(); ?>projects/get_notif', {}, function (data) {
//                     if (data != notif_buffer) {
//                         $('#notif-count').html(data.length);
//                         $('#notif-count-span').html(data.length);
//                         var notif_detail = '';
//                         for (var i = 0; i < data.length; i++) {
//                             notif_detail += '<li style="font-size:11px;">';
//                             notif_detail += '<a href="<?php echo base_url() ?>projects/crud_jobcard/' + data[i].REVNR + '/' + data[i].AUFNR + '">';
//                             notif_detail += '<strong>JOBCARD ' + data[i].AUFNR + '</strong><br /><span style="font-size:8px;">Status=' + data[i].STATUS + ' &amp; SAP=' + data[i].STATUS_SAP + '</span>';
//                             notif_detail += '</a>';
//                             notif_detail += '</li>';
//                         }
//                         $('#notif-detail').html(notif_detail);
//                         notif_buffer = data;
//                     }
//                 });
//             }, 5000);
            var notif_page = 1;
            var notif_limit = 10;
            var notif_load = false;
            var bw_run = false;
            function nFormat(num_s, digits) {
                num = Math.abs(num_s);
                var si = [
                    {value: 1, symbol: ""},
                    {value: 1E3, symbol: "k"},
                    {value: 1E6, symbol: "M"},
                    {value: 1E9, symbol: "G"},
                    {value: 1E12, symbol: "T"},
                    {value: 1E15, symbol: "P"},
                    {value: 1E18, symbol: "E"}
                ];
                var rx = /\.0+$|(\.[0-9]*[1-9])0+$/;
                var i;
                for (i = si.length - 1; i > 0; i--) {
                    if (num >= si[i].value) {
                        break;
                    }
                }
                return ((num_s < 0) ? '-' : '') + (num / si[i].value).toFixed(digits).replace(rx, "$1") + si[i].symbol;
            }
            function get_notif_count() {
                jQuery.getJSON('<?php echo base_url(); ?>projects/get_notif_count', {}, function (data) {
                    for (var i = 0; i < data.length; i++) {
                        $('#notif-count').html(nFormat(data[0].CNT, 0));
                        $('#notif-count-span').html(data[0].CNT);
                    }
                    setTimeout(get_notif_count, 30000);
                });
            }
            function get_notif() {
                notif_load = true;
                var notif_detail_html = '';
                if (notif_page === 1) {
                    $('#notif-detail').html('<center><img width="32" src="<?php echo base_url(); ?>assets/dist/img/notif-load.gif" /></center>');
                } else {
                    notif_detail_html = $('#notif-detail').html();
                    $('#notif-detail').html(notif_detail_html + '<center><img width="32" src="<?php echo base_url(); ?>assets/dist/img/notif-load.gif" /></center>');
                }
                jQuery.getJSON('<?php echo base_url(); ?>projects/get_notif/' + notif_page + '/' + notif_limit, {}, function (data) {
                    var notif_detail = '';
                    for (var i = 0; i < data.length; i++) {
                        notif_detail += '<li style="font-size:11px;">';
                        notif_detail += '<a href="<?php echo base_url() ?>projects/crud_jobcard/' + data[i].REVNR + '/' + data[i].AUFNR + '">';
                        notif_detail += '<strong>JOBCARD ' + data[i].AUFNR + '</strong><br /><span style="font-size:8px;">Status=' + data[i].STATUS + ' &amp; SAP=' + data[i].STATUS_SAP + '</span>';
                        notif_detail += '</a>';
                        notif_detail += '</li>';
                    }
                    if (notif_page === 1) {
                        $('#notif-detail').html(notif_detail);
                    } else {
                        $('#notif-detail').html(notif_detail_html + notif_detail);
                    }
                    notif_page = notif_page + notif_limit;
                    notif_load = false;
                });
            }
            function longtextswal(revnr) {
                swal('Description', 'Loading...');
                jQuery.get('<?php echo base_url(); ?>Sapi/get_longtext/' + revnr, function (data) {
                    swal('Description', eval("'" + data + "'"));
                });
            }
            function update_bw() {
                swal({title: 'BG Job', text: 'Loading...', icon: 'info'});
                jQuery.get('<?php echo base_url(); ?>Sapi/create_job/', function (data) {
                    try {
                        var jdata = JSON.parse(data);
                        if (jdata.msg == 'error') {
                            swal({title: 'BG Job', text: jdata.body, icon: 'error'});
                        } else if (jdata.msg == 'success') {
                            swal({
                                    title: 'BG Job', 
                                    text: 'Job running', 
                                    icon: 'success'
                                })
                            .then(function(){
                                load_report();
                            })
                        }
                    } catch (e) {
                        console.log(e);
                        swal({title: 'BG Job', text: 'API error', icon: 'error'});
                    }
                });
            }

            function update_bw_check() {
                jQuery.get('<?php echo base_url(); ?>Sapi/read_job/', function (data) {
                    try {
                        var jdata = JSON.parse(data);
                        if(jdata.msg === 'success'){
                            if(jdata.body[0].RFCSTATUS === 'DONE'){
                                swal({title: 'Syncronized!', text: 'Done!', icon: 'success'});
                                load_report();
                            }else{
                                swal({title: 'Syncronizing...', text: 'Please Wait', icon: 'info'});
                            }
                        }else{
                            swal({title: 'No Task!', icon: 'info'});
                        }
                    } catch (e) {
                        console.log(e);
                    }
                });
            }
            function update_bw2() {
                if (!bw_run) {
                    bw_run = true;
                    swal({title: 'Project', text: 'Loading...', icon: 'info'});
                    jQuery.get('<?php echo base_url(); ?>Sapi/update_revision/', function (data) {
                        try {
                            var jdata = JSON.parse(data);
                            var fdate = jdata.E_DATA.FINISHDATE.slice(0, 4) + '-' + jdata.E_DATA.FINISHDATE.slice(4, 6) + '-' + jdata.E_DATA.FINISHDATE.slice(6, 8);
                            swal({title: 'Project', text: 'Updated at ' + fdate, icon: 'success'});
                            setTimeout(function () {
                                swal({title: 'Project Change', text: 'Loading...', icon: 'info'});
                                jQuery.get('<?php echo base_url(); ?>Sapi/update_revision_change/', function (data) {
                                    try {
                                        var jdata = JSON.parse(data);
                                        var fdate = jdata.E_DATA.FINISHDATE.slice(0, 4) + '-' + jdata.E_DATA.FINISHDATE.slice(4, 6) + '-' + jdata.E_DATA.FINISHDATE.slice(6, 8);
                                        swal({title: 'Project Change', text: 'Updated at ' + fdate, icon: 'success'});
                                        setTimeout(function () {
                                            swal({title: 'Order Header', text: 'Loading...', icon: 'info'});
                                            jQuery.get('<?php echo base_url(); ?>Sapi/update_pmorder_header/', function (data) {
                                                try {
                                                    var jdata = JSON.parse(data);
                                                    var fdate = jdata.E_DATA.FINISHDATE.slice(0, 4) + '-' + jdata.E_DATA.FINISHDATE.slice(4, 6) + '-' + jdata.E_DATA.FINISHDATE.slice(6, 8);
                                                    swal({title: 'Order Header', text: 'Updated at ' + fdate, icon: 'success'});
                                                    setTimeout(function () {
                                                        swal({title: 'Order Item', text: 'Loading...', icon: 'info'});
                                                        jQuery.get('<?php echo base_url(); ?>Sapi/update_pmorder_item/', function (data) {
                                                            try {
                                                                var jdata = JSON.parse(data);
                                                                var fdate = jdata.E_DATA.FINISHDATE.slice(0, 4) + '-' + jdata.E_DATA.FINISHDATE.slice(4, 6) + '-' + jdata.E_DATA.FINISHDATE.slice(6, 8);
                                                                swal({title: 'Order Item', text: 'Updated at ' + fdate, icon: 'success'}); // , function () {
                                                                location.reload();
                                                                bw_run = false;
                                                                // });
                                                            } catch (e) {
                                                                console.log(e);
                                                                swal({title: 'Order Item', text: 'Updated error', icon: 'error'});
                                                                bw_run = false;
                                                            }
                                                        });
                                                    }, 5000);
                                                } catch (e) {
                                                    console.log(e);
                                                    swal({title: 'Order Header', text: 'Updated error', icon: 'error'});
                                                    bw_run = false;
                                                }
                                            });
                                        }, 5000);
                                    } catch (e) {
                                        console.log(e);
                                        swal({title: 'Project', text: 'Updated error', icon: 'error'});
                                        bw_run = false;
                                    }
                                });
                            }, 5000);
                        } catch (e) {
                            console.log(e);
                            swal({title: 'Project', text: 'Updated error', icon: 'error'});
                            bw_run = false;
                        }
                    });
                } else {
                    swal({title: 'Warning', text: 'Operation already running', icon: 'warning'});
                }
            }
            $(document).ready(function () {
                $("#tablecontent").floatingScrollbar();
                $('#notif-detail').on('scroll', function () {
                    if ($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight) {
                        if (!notif_load)
                            get_notif();
                    }
                });
                // update_bw_check();
                // get_notif_count();
            //    setInterval(get_notif_count, 30000);
            });

            function display_toast(type, msg) {
                bootoast.toast({
                    message: msg,
                    type: type,
                    position: 'top-right'
                });
            }

            function update_bw_by_revision(REVNR){
                swal({title: 'BG Job', text: 'Loading...', icon: 'info'});
                var url = '<?php echo base_url(); ?>Sapi/create_job_by_revision?p3='+REVNR
                jQuery.get(url, function (data) {
                    try {
                        var jdata = JSON.parse(data);
                        if (jdata.msg == 'error') {
                            swal({title: 'BG Job', text: jdata.body, icon: 'error'});
                        } else if (jdata.msg == 'success') {
                            swal({
                                    title: 'BG Job', 
                                    text: 'Job running', 
                                    icon: 'success'
                                })
                            .then(function(){
                                load_report();
                            })
                        }
                    } catch (e) {
                        console.log(e);
                        swal({title: 'BG Job', text: 'API error', icon: 'error'});
                    }
                });
            }
        </script>
    </body>
</html>
