<?php 
    $this->load->helper('text_formatter'); 
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/bootstrap-grid.css" type="text/css" media="screen">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/main.css" type="text/css" media="screen">

        <script type="text/javascript" src="<?php echo base_url(); ?>assets/dist/js/jquery.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="<?php echo base_url(); ?>assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

        <script type="text/javascript" src="<?php echo base_url(); ?>assets/dist/js/owl.carousel.min.js"></script>  
        <script src="<?php echo base_url(); ?>assets/bower_components/moment/moment.js"></script>

        <title>Maintenance Project Controll Board</title>
    </head>
    <body>

        <div class="viewcontainer">

            <!-- Sidebar -->
            <aside class="sidebar _pl-m _pr-m">
                <div class="logo _mt-b">
                </div>

                <h2 class="_centered-text _mt-m _mb-m">Maintenance Project Control Board </h2>

                <div class="meta-info _mb-m">
                    <span class="metatitle _mb-x">Date In</span>
                    <span class="metacontent" id="date_in"><?= raw_date_format($project->REVBD) ?></span>
                </div>

                <div class="meta-info _mb-m">
                    <span class="metatitle _mb-x">A/C REG</span>
                    <span class="metacontent"><?= $project->TPLNR ?> </span>
                </div>

                <div class="meta-info _mb-m">
                    <span class="metatitle _mb-x">Date Out</span>
                    <span class="metacontent" id="date_out"><?= raw_date_format($project->REVED) ?></span>
                </div>

                <div class="meta-info _mb-m">
                    <span class="metatitle _mb-x">Total Maint</span>
                    <span class="metacontent" id="total_maint"><?= get_diff_days($project->REVBD, $project->REVED)?></span>
                </div>

                <div class="meta-info _mb-m">
                    <span class="metatitle _mb-x">Date Report</span>
                    <span class="metacontent"><?= get_current_date() ?></span>
                </div>

                <div class="meta-info _mb-m">
                    <span class="metatitle _mb-x">Day Report</span>
                    <span class="metacontent" id="day_report"><?= get_diff_days($project->REVBD, get_current_date()) ?> </span>
                </div>

                <div class="meta-info _mb-m">
                    <span class="metatitle _mb-x">Today Phase</span>
                    <span class="metacontent"> <?=  $today_phase ?> </span>
                    
                </div>
            </aside>

            <div class="content">
                <div class="container _pt-s">
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-md-4">
                            <div class="gmf-chart -dark _p-all-s _round-all-s">

                                <div class="chart-icon _red-bg">
                                    <img src="<?php echo base_url('assets/dist/img/ic_document.svg'); ?>" alt="">
                                </div>

                                <div class="chart-content">
                                    <div class="chartitle ">Actual Jobcard Close</div>
                                    <div class="chartinfo">
                                        <div class="infonumber" id="jc_close_percentage"></div>
                                        <div class="infoother" id="jc_close"></div>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-6 col-md-4">
                            <div class="gmf-chart -dark _p-all-s _round-all-s">

                                <div class="chart-icon _green-bg">
                                    <img src="<?php echo base_url('assets/dist/img/ic_document.svg') ?>" alt="">
                                </div>

                                <div class="chart-content">
                                    <div class="chartitle ">Actual MDR Close</div>
                                    <div class="chartinfo">
                                        <div class="infonumber" id="mdr_close_percentage"></div>
                                        <div class="infoother" id="mdr_close"></div>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-6 col-md-4">
                            <div class="gmf-chart -dark _p-all-s _round-all-s">

                                <div class="chart-icon _yellow-bg">
                                    <img src="<?php echo base_url('assets/dist/img/ic_document.svg') ?>" alt="">
                                </div>

                                <div class="chart-content">
                                    <div class="chartitle ">Today Target Achievement</div>
                                    <div class="chartinfo">
                                        <div class="infonumber" id="daily_tat"></div>
                                        <div class="infoother">
                                            <div>Jobcard <strong id="tat_jc"> </strong></div>
                                            <div>MDR <strong id="tat_mdr"> </strong></div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>          

                    </div>
                </div>

                <div class="container _pt-s">
                    <div class="row">
                        <!-- JOBCARD PERCENTAGE PER PHASE -->
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <div class="gmf-chart _p-all-s _round-all-s">
                                <div class="chart-content">
                                    <div class="chartitle ">JOBCARD PERCENTAGE PER PHASE</div>
                                    <div class="chartinfo">
                                        <canvas id="PerPhase2" class="chart-item"></canvas>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- MATERIAL STATUS QTY -->
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <div class="gmf-chart  _p-all-s _round-all-s">
                                <div class="chart-content">
                                    <div class="chartitle ">MATERIAL STATUS QTY</div>
                                    <div class="chartinfo">
                                        <canvas id="MaterialStatusQty2" class="chart-item"></canvas>
                                    </div>
                                </div>
                            </div>
                        </div>            
                    </div>
                </div>

                <div class="container _pt-s">
                    <div class="row">
                        <!-- JOBCARD PERCENTAGE PER AREA -->
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <div class="gmf-chart _p-all-s _round-all-s">
                                <div class="chart-content">
                                    <div class="chartitle ">JOBCARD PERCENTAGE PER AREA</div>
                                    <div class="chartinfo">
                                        <canvas id="PerArea2" class="chart-item"></canvas>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- MDR OPEN & CLOSE COMPARASSION -->
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <div class="gmf-chart _p-all-s _round-all-s">
                                <div class="chart-content">
                                    <div class="chartitle">MDR OPEN &amp; CLOSE COMPARISON</div>
                                    <div class="chartinfo">
                                        <canvas id="OpenCloseComparassion2" class="chart-item"></canvas>
                                    </div>
                                </div>
                            </div>
                        </div>            
                    </div>
                </div>

                <div class="container _pt-s">
                    <div class="row">

                        <!-- JOBCARD PERCENTAGE PER AREA -->
                        <div class="col-xs-12 col-sm-6 col-md-4">
                            <div class="gmf-chart _p-all-s _round-all-s">
                                <div class="chart-content">
                                    <div class="chartitle ">MDR SENT</div>
                                    <div class="chartinfo">
                                        <canvas id="MdrSentToShop2" class="chart-item"></canvas>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- MMDR PROGRESS STATUS -->
                        <div class="col-xs-12 col-sm-6 col-md-4">
                            <div class="gmf-chart _p-all-s _round-all-s">
                                <div class="chart-content">
                                    <div class="chartitle ">MDR PROGRESS STATUS</div>
                                    <div class="chartinfo">
                                        <canvas id="MdrProgressStatus2" class="chart-item"></canvas>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- JOBCARD BY SKILL -->
                        <div class="col-xs-12 col-sm-6 col-md-4">
                            <div class="gmf-chart _p-all-s _round-all-s">
                                <div class="chart-content">
                                    <div class="chartitle ">JOBCARD BY SKILL</div>
                                    <div class="chartinfo">
                                        <canvas id="JobCardBySkill2" class="chart-item"></canvas>
                                    </div>
                                </div>
                            </div>
                        </div>              
                    </div>
                </div>
            </div>

        </div>





        <script type="text/javascript" src="<?php echo base_url(); ?>assets/dist/js/app-stat.js"></script> 
        <script type="text/javascript">
            var tta_percentace = 0;

            var bg_color_1 = [ "#e74c3c", "#3498db", "#2ecc71" ];   
            var bg_color_2 = [ "#f7aa00", "#cc99f9", "#ff5959", "#d1478c", "#00a8b5", "#f30e5c", "#89f8ce", "#182952", "#155e63", "#48e0e4", "#53d397", "#6900ff", "#92e6e6", "#00d1ff", "#c00000", "#ede862", "#ff467e", "#0278ae", "#4ef037", "#3f52e3", "#453246"]   
            var bg_color_3 = [ "#155e63", "#48e0e4", "#53d397", "#6900ff", "#92e6e6", "#00d1ff", "#c00000", "#ede862", "#ff467e", "#0278ae", "#4ef037", "#3f52e3", "#453246", "#f7aa00", "#cc99f9", "#ff5959", "#d1478c", "#00a8b5", "#f30e5c", "#89f8ce", "#182952"]
            var bg_color_4 = [ "#ff467e", "#0278ae", "#4ef037", "#3f52e3", "#453246", "#f7aa00", "#155e63", "#48e0e4", "#53d397",  "#cc99f9", "#ff5959", "#d1478c", "#00a8b5", "#f30e5c", "#89f8ce", "#182952", "#6900ff", "#92e6e6", "#00d1ff", "#c00000", "#ede862"]
            

            $(document).ready(function () {
                load_all_data();
            })

            function load_all_data(){
                load_statistic();
                load_single_bar_data("get_jc_phase");
                load_single_bar_data("get_mat_qty");
                load_single_bar_data("get_jc_area");
                load_stack_bar_data("get_mdr_compare");
                load_stack_bar_data("get_jc_skill");
                load_pie_chart_data("get_mdr_shop");
                load_pie_chart_data("get_mdr_acc_status");


                // Reload Every 10 Minutes
                setTimeout(function() {
                    load_all_data();
                    }, 600000 );
                }

            function load_statistic() {
                var url = '<?= base_url('Board/get_projects_stats') ?>';
                var REVNR = '<?= $project->REVNR ?>';
                $.ajax({
                    url: url,
                    method: 'GET',
                    dataType: 'text',
                    data: {REVNR: REVNR},
                    success: function (data) {
                        var response = $.parseJSON(data);
                        if (response.status === 'success') {
                            appendDataHeader(response.body);
                        }
                    }
                })
            }

            function appendDataHeader(data) {

                // Today Target Achievement
                var total_maint = $('#total_maint').text();
                var day_report = $('#day_report').text();
                count_tat();
                cangeOrderColorIndicator();

                // Jobcard Close
                if (data["jc_status"] != undefined) {
                    var jc_percentage = get_percentage(data.jc_status[0].JC_CLOSED, data.jc_status[0].JC_TOTAL)
                    $('#jc_close').html(data.jc_status[0].JC_CLOSED + " OF " + data.jc_status[0].JC_TOTAL)
                    $('#jc_close_percentage').html(jc_percentage + '%')
                    $('#tat_jc').html(Math.round((data.jc_status[0].JC_TOTAL * tta_percentace) / 100))
                } else {
                    $('#jc_close').html("0 OF 0")
                    $('#jc_close_percentage').html("0%")
                }

                // MDR Close
                if (data["mdr_status"] != undefined) {
                    var mdr_percentage = get_percentage(data.mdr_status[0].MDR_CLOSED, data.mdr_status[0].MDR_TOTAL)
                    $('#mdr_close').html(data.mdr_status[0].MDR_CLOSED + " OF " + data.mdr_status[0].MDR_TOTAL)
                    $('#mdr_close_percentage').html(mdr_percentage + '%')
                    $('#tat_mdr').html(Math.round((data.mdr_status[0].MDR_TOTAL * tta_percentace) / 100))
                } else {
                    $('#mdr_close').html("0 OF 0")
                    $('#mdr_close_percentage').html("0%")
                }

                

            }

            function count_tat(){
                var start_date = moment($('#date_in').text(), 'DD MMM YYYY');
                var end_date = moment($('#date_out').text(), 'DD MMM YYYY');
                var today = moment();

                if (start_date.isAfter(today)) {
                    $('#daily_tat').html('0%');
                } else if (end_date.isAfter(today)) {
                    var current_day_diff = today.diff(start_date, "days");
                    var total_maint = end_date.diff(start_date, "days");
                    tta_percentace = ((current_day_diff / total_maint) * 100).toFixed(2);
                    $('#daily_tat').html(tta_percentace + '%') ;                   
                } else {
                    $('#daily_tat').html('100%');
                }

            }

            function load_single_bar_data(prefix_url){
                var REVNR = '<?= $project->REVNR ?>';
                var url = '<?= base_url('Board/') ?>' + prefix_url;
                $.ajax({
                    url: url,
                    method: 'GET',
                    dataType: 'text',
                    data: {REVNR: REVNR},
                    success: function (data) {
                        var response = $.parseJSON(data);
                        if (response.status === 'success') {
                            if(prefix_url === "get_jc_phase")
                                buildSingleBarChar( buildLabels(response.body), buildData(response.body), "PerPhase2" );
                            if(prefix_url === "get_mat_qty")
                                buildSingleBarChar( buildLabels(response.body), buildData(response.body), "MaterialStatusQty2" );
                            if(prefix_url === "get_jc_area")
                                buildSingleBarChar( buildLabels(response.body), buildData(response.body), "PerArea2" );
                        }
                    }
                })                
            }

            function load_stack_bar_data(prefix_url){
                var REVNR = '<?= $project->REVNR ?>';
                var url = '<?= base_url('Board/') ?>' + prefix_url;
                $.ajax({
                    url: url,
                    method: 'GET',
                    dataType: 'text',
                    data: {REVNR: REVNR},
                    success: function (data) {
                        var response = $.parseJSON(data);
                        if (response.status === 'success') {
                            if(prefix_url === "get_mdr_compare")
                                buildStackBarChart(buildLabels(response.body), buildDataOpen(response.body), buildDataClose(response.body), "OpenCloseComparassion2");
                            if(prefix_url === "get_jc_skill")
                                buildStackBarChart(buildLabels(response.body), buildDataOpen(response.body), buildDataClose(response.body), "JobCardBySkill2");
                        }
                    }
                })                
            }

            function load_pie_chart_data(prefix_url){
                var REVNR = '<?= $project->REVNR ?>';
                var url = '<?= base_url('Board/') ?>' + prefix_url;
                $.ajax({
                    url: url,
                    method: 'GET',
                    dataType: 'text',
                    data: {REVNR: REVNR},
                    success: function (data) {
                        var response = $.parseJSON(data);
                        if (response.status === 'success') {
                            if(prefix_url === "get_mdr_shop")
                                buildPieChart(buildLabels(response.body), buildData(response.body), "MdrSentToShop2")
                            if(prefix_url === "get_mdr_acc_status")
                                buildPieChart(buildLabels(response.body), buildData(response.body), "MdrProgressStatus2")
                        }
                    }
                })                
            }            

            function buildLabels(data){
                var labels = [];
                $.each(data, function(key, val){
                    labels.push(val.LABELS);
                })
                return labels;
            }

            function buildData(data){
                var chart_data = [];    
                $.each(data, function(key, val){
                    chart_data.push(parseFloat(val.QTY));
                })
                return chart_data;

            }

            function buildDataOpen(data){
                var chart_data = [];    
                $.each(data, function(key, val){
                    chart_data.push(parseFloat(val.QTY_OPEN));
                })
                return chart_data;
            }

            function buildDataClose(data){
                var chart_data = [];    
                $.each(data, function(key, val){
                    chart_data.push(parseFloat(val.QTY_CLOSE));
                })
                return chart_data;
            }


            function get_percentage(number, divider) {
                return number != 0 ? ((number / divider) * 100).toFixed(2) : 0;
            }


            // BUILD CHART
            function getRandomInt(min, max) {
                return Math.floor(Math.random() * (max - min + 1)) + min;
            }

            var defConfy = {
                beginAtZero: true,
                fontColor: 'rgba(236, 240, 241,1.0)'
            };
            var defConfx = {
                beginAtZero: true,
                fontColor: 'rgba(236, 240, 241,1.0)',
                autoSkip: false,
                fontSize: 10
            };

            var defConfy2 = {
                beginAtZero: true
            };
            var defConfx2 = {
                autoSkip: false,
                fontSize: 10
            }

            var DoughnutOptions = {
                    responsive: true,
                    legend: {
                        position: 'right',
                        align: 'center',
                        display: true,
                        labels: {
                                boxWidth: 10,
                                fontSize: 10
                        }
                    },
                    tooltips: {
                        mode: 'label',
                        callbacks: {
                            label: function(tooltipItem, data) {
                                var total = 0;
                                $.each(data['datasets'][0]['data'], function(key, val){
                                    total += val;
                                })
                                return get_percentage(data['datasets'][0]['data'][tooltipItem['index']], total) + '%';
                            }
                        }
                    }
            }    
                    
            var lblPhase = ["PRELIM", "OPN/REM", "INSP", "SERV/LUB", "INST/REST", "OPC/FUC"];
            var lblMaterialStatsus = ["NIL STOCK", "SOA", "CUST. SPLY", "ORDERED PURC", "CUSTOM", "SHIPMENT"];
            var lblPerArea = ["AREA", "COCKPIT", "FUSELAGE", "LH-WING", "RH-WING", "L/G", "ENG#1", "ENG#2", "ENG#3", "ENG#4", "TAIL", "CABIN", "FWD CARGO", "AFT CARGO", "ELECT"];
            var lblSkill = ["A/P", "CBN", "E/A", "NDT", "PAINTING", "STR", "TBR-SEAL", "TBR-SHOP"];
            var lblShop = ["TVP", "TBRS", "CBN", "CBN SHOP", "SEAT", "TBP"];
            var lblProgress = ["PERFORM BY SHOP", "WAITING RO", "MATERIAL WAITING", "PERFORM BY PROD"];            

            // Build Single Bar Char
            function buildSingleBarChar(labels, data, elemnetId) {
                var ctxPerPhase2 = document.getElementById(elemnetId);
                if (ctxPerPhase2 != null) {
                    ctxPerPhase2.height = 140;
                    var myChart = new Chart(ctxPerPhase2, {
                        type: 'bar',
                        data: {
                            labels: labels,
                            datasets: [{
                                data: data,
                                backgroundColor: bg_color_4
                            }]
                        },
                        options: {
                            scales: {
                                yAxes: [{
                                    ticks: {
                                        beginAtZero: true,
                                        autoSkip: false
                                    }
                                }],
                                xAxes: [{
                                    ticks: {
                                        autoSkip: false,
                                        fontSize: 8
                                    }
                                }]
                            },
                            legend: {
                                display: false
                            },
                            events: false,
                            tooltips: {
                                enabled: false
                            },
                            hover: {
                                animationDuration: 1
                            },
                            animation: {
                                duration: 1,
                                onComplete: function () {
                                    var chartInstance = this.chart,
                                    ctx = chartInstance.ctx;
                                    ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
                                    ctx.textAlign = 'center';
                                    ctx.textBaseline = 'bottom';

                                    this.data.datasets.forEach(function (dataset, i) {
                                        var meta = chartInstance.controller.getDatasetMeta(i);
                                        meta.data.forEach(function (bar, index) {
                                            var data = dataset.data[index];                            
                                            ctx.fillText(data, bar._model.x, bar._model.y - 5);
                                        });
                                    });
                                }
                            }
                        },
                    });
                }                
            }

            // Build Stcakbar Cart
            function buildStackBarChart(labels, dataOpen, dataClose, elemnetId){
                var ctxOpenCloseComparassion2 = document.getElementById(elemnetId);
                if (ctxOpenCloseComparassion2 != null) {
                    ctxOpenCloseComparassion2.height = 140;
                    var chartOpenCloseComparassion2 = new Chart(ctxOpenCloseComparassion2, {
                        type: 'bar',
                        data: {
                            labels: labels,
                            datasets: [{
                                label: 'Open',
                                data: dataOpen,
                                backgroundColor: 'rgba(255, 99, 132, 1)'
                            }, {
                                label: 'Close',
                                data: dataClose,
                                backgroundColor: 'rgba(54, 162, 235, 1)'
                            }]
                        },
                        options: {
                            scales: {
                                xAxes: [{
                                    stacked: true,
                                    ticks: {
                                        autoSkip: false,
                                        fontSize: 8
                                    }
                                }],
                                yAxes: [{
                                    stacked: true,
                                    autoSkip: false,
                                    fontSize: 8
                                }]
                            },
                            events: false,
                            tooltips: {
                                enabled: false
                            },
                            hover: {
                                animationDuration: 1
                            },
                            animation: {
                                duration: 1,
                                onComplete: function () {
                                    var chartInstance = this.chart,
                                    ctx = chartInstance.ctx;
                                    ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
                                    ctx.textAlign = 'center';
                                    ctx.textBaseline = 'bottom';

                                    this.data.datasets.forEach(function (dataset, i) {
                                        var meta = chartInstance.controller.getDatasetMeta(i);
                                        meta.data.forEach(function (bar, index) {
                                            var data = dataset.data[index];
                                            if(data > 0) {
                                                ctx.fillText(data, bar._model.x, bar._model.y - 5);
                                            }                            
                                        });
                                    });
                                }
                            }
                        }
                    });
                }                
            }

            // Build Pie Chart
            function buildPieChart(labels, data, elemnetId) {
                var ctx = document.getElementById(elemnetId).getContext('2d');
                var DoughnutChart = new Chart(ctx, {
                    type: 'doughnut',
                    data: {
                        labels: labels,     
                        datasets:[{
                            data: data,
                            backgroundColor: ['rgba(26, 188, 156,1.0)', 'rgba(52, 152, 219,1.0)', 'rgba(155, 89, 182,1.0)', 'rgba(241, 196, 15,1.0)', 'rgba(52, 73, 94,1.0)',  'rgba(46, 204, 113,1.0)', 'rgba(230, 126, 34,1.0)']
                        }]
                    },
                    options: DoughnutOptions
                });
            }            

            function cangeOrderColorIndicator(){
                var jc_percentage = $('#jc_close_percentage').text();
                var mdr_percentage = $('#mdr_close_percentage').text();
                var tat = $('#daily_tat').text();
                
                if (jc_percentage >= tat) {
                    $('#jc_close_percentage').css("color", "#4ef037");
                } else {
                    $('#jc_close_percentage').css("color", "red");
                }

                if (mdr_percentage >= tat) {
                    $('#mdr_close_percentage').css("color", "#4ef037");
                } else {
                    $('#mdr_close_percentage').css("color", "red");
                }
            }

        </script>
    </body>
</html>

