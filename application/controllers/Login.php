<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Login extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->helper('auth_helper');
        $this->load->database();
        $this->load->model('Users');
        $this->load->model('User_group_model');
        $this->load->model('Menu_access_view_model');
        $this->load->model('Work_area_view_model');
        $this->load->model('Management_model');

    }

    function index() {
        $data['is_error'] = 0;
        $this->load->view('login', $data);
    }

    function verifylogin() {
        //This method will have the credentials validation

        $this->form_validation->set_rules('username', 'Username', 'trim|required|xss_clean');
        $this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean|callback_check_database2');

        if ($this->form_validation->run() == FALSE) {
            //Field validation failed.  User redirected to login page
            $data['is_error'] = 1;
            $this->load->view('login', $data);
        } else {
            //Go to private area
            redirect('projects/', 'refresh');
        }
    }

    function check_database($password) {
        //Field validation succeeded.  Validate against database
        $username = $this->input->post('username');
        //query the database
        $result = $this->Users->login($username, $password);
        

        if ($result) {
            $group = $this->User_group_model->get_user_group($result->GROUP_ID);
            $work_area = $this->Work_area_view_model->where('ID_WORK_AREA', $result->WORK_AREA_ID)->first(); 
            $work_area_id = !empty($work_area) ? $work_area->ID_WORK_AREA : NULL; 
    
            $sess_array = array(
                'id_user' => $result->ID_USER,
                'name' => $result->NAME,
                'username' => $result->USERNAME,
                'group_id' => $result->GROUP_ID,
                'customer_id' => $result->CUSTOMER_ID,
                'user_group' => $group->USER_GROUP,
                'group_level' => $group->LEVEL,
                'work_area_id' => $work_area_id
            );

            

            $this->session->set_userdata('logged_in', $sess_array);
            $this->Users->update_last_log($sess_array['id_user']);
            $group = $this->User_group_model->get_user_group($sess_array['group_id']);

            if (is_customer($sess_array['user_group'])) {
                redirect('projects/'.$sess_array['customer_id']) ;
            } 
            else {
                redirect('projects');
            }

            
            
            return TRUE;
        } else {
            $this->form_validation->set_message('check_database', 'Invalid username or password');
            return false;
        }
    }

    function check_database2($password) {
        $username = $this->input->post('username');

        $cek_user = $this->Management_model->cekUserLogin($username,$password);
        
        if($cek_user['codestatus'] == 'S'){
            $update_last_login = $this->Management_model->updateLastLogin($cek_user['resultdata'][0]->user_id);
            $log = array(
                'id_user' => $cek_user['resultdata'][0]->user_id,
                'name' => $cek_user['resultdata'][0]->user_nama,
                'username' => $cek_user['resultdata'][0]->user_username,
                'group_id' => $cek_user['resultdata'][0]->user_group_id,
                'customer_id' => $cek_user['resultdata'][0]->user_customer_id,
                'user_group_id' => $cek_user['resultdata'][0]->user_role_id,
                'user_group' => $cek_user['resultdata'][0]->user_role,
                'user_group_level' => $cek_user['resultdata'][0]->user_role_level,
                'work_area_id' => $cek_user['resultdata'][0]->user_work_id,
            );

            $get_menu = $this->Management_model->getMenuSide($cek_user['resultdata'][0]->user_group_id);
            $cookie_name = "menu";
            $cookie_value = json_encode($get_menu);
            setcookie($cookie_name, $cookie_value, 2147483647, "/");
            $this->session->set_userdata('logged_in', $log);

            // echo "<pre>";
            // print_r($log);
            // die();

            // if (is_customer($log['user_group'])) {
            //     redirect('projects/'.$log['customer_id']) ;
            // } else {
            //     redirect('projects');
            // }

            redirect('projects');
            return TRUE;
        }else{
            $this->form_validation->set_message('check_database', 'Invalid username or password');
            return false;
        }
    }

    function logout() {
        $cookie_name = "menu";
        $cookie_value = null;
        setcookie($cookie_name, $cookie_value, 2147483647, "/");

        $this->session->unset_userdata('logged_in');
        $this->session->unset_userdata('menu');

        $this->session->sess_destroy();
        redirect('/', 'refresh');
    }

}
