<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Sap
 *
 * @author Miftachur Rozaq
 */
require('assets/print_r/lib/PrintrTokenizer.php');
require('assets/print_r/lib/PrintrParser.php');
require('assets/print_r/lib/StringLines.php');
require('assets/print_r/lib/ArrayExportObject.php');
require('assets/print_r/lib/ArrayExporter.php');

class Sapi extends CI_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index($AUFNR) {
        $i = file_get_contents("http://192.168.240.107/Sched_ABS/testing_sap_longtext.php?order=$AUFNR");

        $buffer = str_replace("\r\n", "", $i);

        $var = PrintrParser($buffer);
        if (is_array($var)) {
            $exporter = new ArrayExporter();
            $buffer = $exporter->export($var);
        } else {
            $buffer = var_export($var, true);
        }

        echo json_encode($buffer);
    }

}
