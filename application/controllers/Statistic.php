<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require_once('assets/editablegrid/EditableGrid.php');
require_once APPPATH . 'libraries/spout/src/Spout/Autoloader/autoload.php';

use Illuminate\Database\Eloquent\Model as ELOQ;
use Box\Spout\Reader\ReaderFactory;
use Box\Spout\Common\Type;
use Box\Spout\Writer\WriterFactory;

class Statistic extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->model('Project_model');
        $this->load->model('Jobcard_model');
        $this->load->model('Board_model');
        $this->load->library('menu_lib');
        $this->load->helper('auth_helper');
        $this->load->helper('text_formatter');

        $this->data["session"] = $this->session->userdata('logged_in');
        has_session($this->data["session"]);
    }

    public function index(){
        $data['workarea'] = $this->Project_model->get_workcenter();

        $data['content'] = 'stat/highlight';
        $this->load->view('stat/index', $data);
    }

    public function getWorkAreaWithProject(){
        $response["status"] = NULL;
        $response["body"] = NULL;

        try {
            $data = array();
            $workarea = $this->Project_model->get_workcenter();
            $projects = $this->Project_model;
            $revnr_list = array();

            if ( $workarea ) {

                    foreach ($workarea as $key => $value) {
                        $a = $projects->get_project_by_location($value->VAWRK);
                        $prj_data = array();
                        foreach ($a as $project) {
                            array_push($revnr_list, $project->REVNR);
                            $c = array(
                                'REVNR' => $project->REVNR,
                                'REVTX' => $project->REVTX,
                                'TPLNR' => $project->TPLNR,
                                'TXT04' => $project->TXT04,
                                'VAWRK' => $project->VAWRK,
                                'REVBD' => $project->REVBD,
                                'REVED' => $project->REVED
                            );
                            array_push($prj_data, $c);
                        }
                        $data[$key] = array(
                            'LOCATION' => $value->VAWRK,
                            'AC_COUNT' => $value->AC_COUNT,
                            'AC_LIST' => $prj_data
                        );
                    }

                    // $order_count = $this->Jobcard_model->get_jobcard_close($revnr_list);

                    $response["status"] = 'success';
                    $response["body"] = $data;
            }

        } catch (Exception $e) {
            
        }


        header('Content-Type: application/json');
        echo json_encode($response);
    }

    public function getProgress(){
        $response["status"] = NULL;
        $response["body"] = NULL;

        try {
            $data = array();
            $revnr_list  = json_decode($_POST['REVLIST']);
            $order_list = $this->Project_model->get_order_count($revnr_list);
            $closed_list = $this->Project_model->get_order_close_count($revnr_list);

            if ($order_list && $closed_list) {
                
                foreach ($order_list as $idx => $order) {
                    array_push($data, array(
                        "REVNR" => $order->REVNR,
                        "TOTAL" => $order->TOTAL,
                        "CLOSED" => $closed_list[$idx]->TOTAL,
                        "PERCENTAGE" => get_percentage($closed_list[$idx]->TOTAL, $order->TOTAL)
                        
                    ));
                }

                $response["status"] = 'success';
                $response["body"] = $data;                
            }

        } catch (Exception $e) {
            echo $e;
        }


        header('Content-Type: application/json');
        echo json_encode($response);
    }

    public function hangar_project(){
        try {
            if ($this->uri->segment(3)) {
                $this->session->set_userdata('VAWRK', $this->uri->segment(3));
                $data['VAWRK'] = $this->session->userdata('VAWRK');
            }
        } catch (Exception $e) {
            
        }

        $data['content'] = 'stat/hangar';
        $this->load->view('stat/index', $data);
    }

    public function getProjectByLocation(){
        $response["status"] = NULL;
        $response["body"] = NULL;
        try {
            $VAWRK = $_POST['VAWRK'];
            $projects = $this->Project_model->get_project_by_location($VAWRK);

            if ($projects) {
                $response["status"] = 'success';
                $response["body"] = $projects;                
            } else {
                $response["status"] = 'error';
                $response["body"] = NULL;                       
            }
            
        } catch (Exception $e) {
            $response["status"] = 'failed';
            $response["body"] = NULL;   
        }

        header('Content-Type: application/json');
        echo json_encode($response);
    }

    public function get_mat_shortage() {
        $response["status"] = NULL;
        $response["body"] = NULL;

        try {
            $REVLIST = json_decode($_POST['REVLIST']);
            $data = array();
            foreach ($REVLIST as $value) {
                array_push($data, array("REVNR" => $value, "MATERIAL" => $this->Board_model->get_mat_qty($value)));
            }
            $response["status"] = 'success';
            $response["body"] = $data;
        } catch (Exception $e) {
            echo $e;
        }

        header('Content-Type: application/json');
        echo json_encode($response);
    }


}