<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require_once('assets/editablegrid/EditableGrid.php');
require_once APPPATH . 'libraries/spout/src/Spout/Autoloader/autoload.php';

use Illuminate\Database\Eloquent\Model as ELOQ;

class Cron extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->model('Daily_day_model');
    }


    function close_daily_report(){
        // $today = date('Y M D');
        $daily_report = $this->Daily_day_model
                        // ->whereDate('CREATED_AT', date('Y-m-d'))
                        ->where('STATUS', 'OPEN')
                        ->update(['STATUS' => 'CLOSE']);
                        
        echo $daily_report ? "success" : "error";
    }

    function test(){
        echo "ok bro";
    }
}
