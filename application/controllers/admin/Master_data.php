<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require_once('assets/editablegrid/EditableGrid.php');
require_once APPPATH . 'libraries/spout/src/Spout/Autoloader/autoload.php';

use Illuminate\Database\Eloquent\Model as ELOQ;
use Box\Spout\Reader\ReaderFactory;
use Box\Spout\Common\Type;
use Box\Spout\Writer\WriterFactory;

class Master_data extends CI_Controller {
		public function __construct() {
			parent::__construct();
			$this->load->database();
			$this->load->library('menu_lib');
			$this->load->model('Menu_access_view_model', 'Menu');
			$this->load->model('Material_model');
			$this->load->helper('auth_helper');
			$this->load->helper('text_formatter');

			// Cek User Seesion
			$this->data["session"] = $this->session->userdata('logged_in');

			has_session($this->data["session"]);
		}

		/* Master Data Material
		*****************************************************************************************************/
		public function crud_material(){
			$data["session"] = $this->session->userdata('logged_in');
			$data['content'] = 'master_data/material';
			$this->load->view('template', $data);
		}

		public function crud_material_load_all(){
			$response["status"] = NULL;
        		$response["body"] = NULL;

			try {
				$material = $this->Material_model->get();
				if($material){
						$response["status"] = 'success';
        				$response["body"] = $material;
				}else{
						$response["status"] = 'error';
        				$response["body"] = NULL;
				}

			} catch (Exception $e) {
					$response["status"] = 'error';
    				$response["body"] = NULL;				
			}

			header('Content-Type: application/json');
        	echo json_encode($response);
		}

		public function read_xlsx_file_material(){
			$response["status"] = NULL;
        		$response["body"] = NULL;
			try {
				$path = $_FILES['media']['tmp_name'];
				$reader = ReaderFactory::create(Type::XLSX);
				$reader->open($path);
				$i = 0;
				$data["error"] = array();
				$data["success"] = array();

				foreach ($reader->getSheetIterator() as $sheet) {
					foreach ($sheet->getRowIterator() as $row => $value) {
						if($i == 0){ $i++; continue; }

						$material = $this->Material_model->firstOrNew(
							['MATNR' => $value[0] . ''],
							['ALT_PART_NUMBER' => $value[0], 'MAKTX' => $value[1], 'CTG' => $value[3]]
						);

						$material->save();						
						if($material){
							array_push($data["success"], $value[0]);
						}else{
							array_push($data["error"], $value[0]);
						}

					}				
				}

				if(count($data["success"]) > 0){
					$response["status"] = 'success';
        			$response["body"] = $data;
				}

			}catch(Exception $e){
				echo $e;
				exit;
			}
			header('Content-Type: application/json');
        		echo json_encode($response);
		}

		public function crud_material_add(){
			$response["status"] = NULL;
        		$response["body"] = NULL;

			try {
				if(isset($_POST['MATNR'])){

					$MATNR				= $_POST['MATNR'];
					$ALT_PART_NUMBER	= $_POST['ALT_PART_NUMBER'];
					$MAKTX				= $_POST['MAKTX'];
					$CTG				= $_POST['CTG'];

					$material = $this->Material_model;
					$material->MATNR = $MATNR;
					$material->ALT_PART_NUMBER = $ALT_PART_NUMBER;
					$material->MAKTX = $MAKTX;
					$material->CTG = $CTG;
					$material->save();


					if($material){
							$response["status"] = 'success';
		    				$response["body"] = NULL;
					}else{
							$response["status"] = 'error';
		    				$response["body"] = NULL;
					}

				}

			} catch (Exception $e) {
					$response["status"] = 'error';
    				$response["body"] = NULL;				
			}

			header('Content-Type: application/json');
        	echo json_encode($response);			
		}

		public function crud_material_delete(){
			$response["status"] = NULL;
        		$response["body"] = NULL;

			try {
				if(isset($_POST['MATNR'])){

					$MATNR = $_POST['MATNR'];
					$material = $this->Material_model
						->where('MATNR', $MATNR)
						->delete();

					if($material){
							$response["status"] = 'success';
		    				$response["body"] = NULL;
					}else{
							$response["status"] = 'error';
		    				$response["body"] = NULL;
					}

				}

			} catch (Exception $e) {
					$response["status"] = 'error';
    				$response["body"] = NULL;				
			}

			header('Content-Type: application/json');
        	echo json_encode($response);			
		}
}