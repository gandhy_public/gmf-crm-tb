<?php
date_default_timezone_set("Asia/Jakarta");
class Management extends CI_Controller
{
	public function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->library('menu_lib');
        $this->load->model('Project_model_ci', '', TRUE);
        $this->load->model('Project_model');
        $this->load->model('Management_model');
        $this->load->model('VProject_model');
        $this->load->model('Customer_model');
        $this->load->model('Dashboard_model', '', TRUE);
        $this->load->model('Jobcard_model');
        $this->load->model('Jobcard_model_view');
        $this->load->model('Mdr_model');
        $this->load->model('Mdr_model_view');
        $this->load->model('Mrm_model');
        $this->load->model('Mrm_model_view');
        $this->load->model('Prm_model');
        $this->load->model('Prm_model_view');
        $this->load->model('Crm_model');
        $this->load->model('Ticket_model');
        $this->load->model('Csr_model');
        $this->load->model('Csr_model_view');
        $this->load->model('Daily_menu_model_view');
        $this->load->model('Daily_menu_model_ci');
        $this->load->model('Daily_day_model');
        $this->load->model('Daily_day_model_view');
        $this->load->model('Daily_area_model');
        $this->load->model('Daily_details_model');
        $this->load->model('Users');
        $this->load->model('User_group_model');
        $this->load->model('Phase_model');
        $this->load->model('Phase_model_view');
        $this->load->model('Material_model');
        $this->load->helper('auth_helper');
        $this->load->helper('text_formatter');

        // $this->output->enable_profiler(TRUE);

        // Cek User Seesion
        $this->data["session"] = $this->session->userdata('logged_in');
        
        has_session($this->data["session"]);
    }
	
	public function user()
	{
        $sess_login = $this->session->userdata('logged_in');
        if($sess_login['user_group_level'] == 3 || $sess_login['user_group_level'] == 4){
            redirect(base_url('index.php/projects'));
        }
		$data['role']             = $this->Management_model->getRole();
        $data['customer']         = $this->Management_model->getCustomer();
        $data['group']            = $this->Management_model->getGroup();
        $data['work']             = $this->Management_model->getWork();

		//$data['projects'] = $this->Project_model->get_projects(1, 5);
		$data["session"] = $this->session->userdata('logged_in');
		$data['content'] = 'management/user';
		$data['title'] = 'User Management';
        //print('<pre>'.print_r($this->session->userdata(),TRUE).'</pre>');die();
		$this->load->view('template', $data);
	}

	public function menu()
	{
        $data_menu = $this->Management_model->getMenu();
        foreach ($data_menu as $key) {
            
            $menu_arr[$key->modul][] = [
                'menu' => $key->menu,
                'id' => $key->menu_id,
                'modul_id' => $key->modul_id,
            ];

        }
        $data['data_menu']             = $menu_arr;
        $data_tab_project = $this->Management_model->getTabProject();
        $data['tab_project'] = $data_tab_project;

		//$data['projects'] = $this->Project_model->get_projects(1, 5);
		$data["session"] = $this->session->userdata('logged_in');
		$data['content'] = 'management/menu';
		$data['title'] = 'Menu Management';
		$this->load->view('template', $data);
    }
    
    public function edit_group($id)
	{
        $view = strtolower($this->input->get('v'));
        if((!isset($view)) || (($view != "y") && ($view != "n"))){ $view = "y"; }
        $data['view'] = $view;
        if($view == "y"){
            $data['title'] = 'View Group Menu';
            $data['title_tab'] = 'View Group';
        }else{
            $data['title'] = 'Edit Group Menu';
            $data['title_tab'] = 'Edit Group';
        }

        $data_menu = $this->Management_model->getMenu();
        foreach ($data_menu as $key) {
            
            $menu_arr[$key->modul][] = [
                'menu' => $key->menu,
                'id' => $key->menu_id,
                'modul_id' => $key->modul_id,
            ];

        }
        $data['data_menu']             = $menu_arr;
        $data_tab_project = $this->Management_model->getTabProject();
        $data['tab_project'] = $data_tab_project;
        $data['id_group'] = $id;

		//$data['projects'] = $this->Project_model->get_projects(1, 5);
		$data["session"] = $this->session->userdata('logged_in');
		$data['content'] = 'management/group/edit';
		$this->load->view('template', $data);
	}

	public function create_user()
	{
		$ldap = $this->input->post('ldap');
		$nama = $this->input->post('nama');
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$role = $this->input->post('role');
		$customer = $this->input->post('customer');
		$work = $this->input->post('work');
		$group = $this->input->post('group');
        $url = base_url('index.php')."/management/user";
        

		if($ldap == "on"){
			$cek_ldap = $this->checkLdap($username,$password);
			$param = [
				'NAME' => $nama,
				'USERNAME' => $username,
				'PASSWORD' => "using ldap",
				'GROUP_ID' => $role,
				'STATUS' => '1',
				'WORK_AREA_ID' => $work,
				'GROUP_NAME_ID' => $group,
			];
			$cek_user = $this->Management_model->cekUser($username,$password);
			if(count($cek_user) > 0){
				$this->session->set_flashdata('alert',"<div class='alert alert-warning'>Data user sudah ada sebelumnya</div>");
				header("Location: $url");
			}else{
				$this->Management_model->createUser($param);

                if(isset($_POST['config'])){
                    $user_id = $this->Management_model->cekUser($param['USERNAME'],$param['PASSWORD']);
                    $user_id = $user_id[0]['ID_USER'];
                    $this->Management_model->deleteManagementTabConfig($user_id);
                    for($a=0;$a<count($_POST['config']);$a++){
                        $data = explode("_", $_POST['config'][$a]);
                        $param = [
                            'user_id' => $user_id,
                            'group_id' =>  $data[0],
                            'modul_id' =>  $data[1],
                            'menu_id' =>  $data[2],
                        ];
                        $this->Management_model->creatManagementTabConfig($param);
                    }
                }

				$this->session->set_flashdata('alert',"<div class='alert alert-success'>Sukses create new user</div>");
				header("Location: $url");
			}
		}else{
			$param = [
				'NAME' => $nama,
				'USERNAME' => $username,
				'PASSWORD' => $password,
				'CUSTOMER_ID' => $customer,
				'GROUP_ID' => $role,
				'STATUS' => '1',
                'WORK_AREA_ID' => $work,
				'GROUP_NAME_ID' => $group,
            ];
            
			$cek_user = $this->Management_model->cekUser($username,$password);
			if(count($cek_user) > 0){
				$this->session->set_flashdata('alert',"<div class='alert alert-warning'>Data user sudah ada sebelumnya</div>");
				header("Location: $url");
			}else{
				$this->Management_model->createUser($param);

                if(isset($_POST['config'])){
                    $user_id = $this->Management_model->cekUser($param['USERNAME'],$param['PASSWORD']);
                    $user_id = $user_id[0]['ID_USER'];
                    $this->Management_model->deleteManagementTabConfig($user_id);
                    for($a=0;$a<count($_POST['config']);$a++){
                        $data = explode("_", $_POST['config'][$a]);
                        $param = [
                            'user_id' => $user_id,
                            'group_id' =>  $data[0],
                            'modul_id' =>  $data[1],
                            'menu_id' =>  $data[2],
                        ];
                        $this->Management_model->creatManagementTabConfig($param);
                    }
                }
                
				$this->session->set_flashdata('alert',"<div class='alert alert-success'>Sukses create new user</div>");
				header("Location: $url");
			}
		}
	}

	public function create_group()
	{
		$url = base_url('index.php')."/management/menu";
		$group = $this->input->post('group');
        $param = [
            'name' => $group,
            'is_active' => "1",
            'create_at' => date('Y-m-d H:i:s'),
        ];
        $group_id = $this->Management_model->creatGroup($param);

        $data_modul = array_keys($this->input->post());
        if (array_search("PROJECT_tab", $data_modul)) {
        	for($b=0;$b<count($_POST['PROJECT_tab']);$b++){
                $menu_modul_top = $_POST['PROJECT_tab'][$b];
                $menu_modul_top = explode("_", $menu_modul_top);
                $menu_id_top = $menu_modul_top[0];
                $modul_id_top = $menu_modul_top[1];
                $data_insert_top[] = [
                    'group_id' => $group_id,
                    'modul_id' => $modul_id_top,
                    'menu_id' => $menu_id_top
                ];
            }
            $create_management = $this->Management_model->creatManagementTab($data_insert_top);
            unset($_POST['PROJECT_tab']);
            $modul = array_keys($_POST);
        }else{
        	$modul = array_keys($_POST);
        }

        for($a=1;$a<count($modul);$a++){
            $modul_name = $modul[$a];
            for($b=0;$b<count($_POST[$modul_name]);$b++){
                $menu_modul = $_POST[$modul_name][$b];
                $menu_modul = explode("_", $menu_modul);
                $menu_id = $menu_modul[0];
                $modul_id = $menu_modul[1];
                $data_insert[] = [
                    'group_id' => $group_id,
                    'modul_id' => $modul_id,
                    'menu_id' => $menu_id
                ];
            }
        }

        $create_management = $this->Management_model->creatManagement($data_insert);
        if($create_management){
            $this->session->set_flashdata('alert',"<div class='alert alert-success'>Sukses create management menu</div>");
        }else{
            $this->session->set_flashdata('alert',"<div class='alert alert-danger'>Failed create management menu</div>");
        }
        header("Location: $url");
	}

    public function update_group()
    {
        $url = base_url('index.php')."/management/menu";
        $group = $this->input->post('group_name');
        $id_group = $this->input->post('id_group');
        $status = $this->input->post('status');
        $param = [
            'name' => $group,
            'is_active' => $status,
            'update_at' => date('Y-m-d H:i:s'),
        ];
        $update = $this->Management_model->updateGroup($param,$id_group);
        if(!$update){
            $this->session->set_flashdata('alert',"<div class='alert alert-danger'>Failed update management menu</div>");
            header("Location: $url");
            die();
        }

        $data_modul = array_keys($this->input->post());
        if (array_search("PROJECT_tab", $data_modul)) {
            for($b=0;$b<count($_POST['PROJECT_tab']);$b++){
                $menu_modul_top = $_POST['PROJECT_tab'][$b];
                $menu_modul_top = explode("_", $menu_modul_top);
                $menu_id_top = $menu_modul_top[0];
                $modul_id_top = $menu_modul_top[1];
                $data_insert_top[] = [
                    'group_id' => $id_group,
                    'modul_id' => $modul_id_top,
                    'menu_id' => $menu_id_top
                ];
            }
            $create_management = $this->Management_model->updateManagementTab($data_insert_top,$id_group);
            unset($_POST['PROJECT_tab']);
            $modul = array_keys($_POST);
        }else{
            $modul = array_keys($_POST);
        }

        for($a=3;$a<count($modul);$a++){
            $modul_name = $modul[$a];
            for($b=0;$b<count($_POST[$modul_name]);$b++){
                $menu_modul = $_POST[$modul_name][$b];
                $menu_modul = explode("_", $menu_modul);
                $menu_id = $menu_modul[0];
                $modul_id = $menu_modul[1];
                $data_insert[] = [
                    'group_id' => $id_group,
                    'modul_id' => $modul_id,
                    'menu_id' => $menu_id
                ];
            }
        }

        $create_management = $this->Management_model->updateManagement($data_insert,$id_group);
        if($create_management){
            $this->session->set_flashdata('alert',"<div class='alert alert-success'>Sukses update management menu</div>");
        }else{
            $this->session->set_flashdata('alert',"<div class='alert alert-danger'>Failed update management menu</div>");
        }
        header("Location: $url");
    }

	public function checkLdap($username, $password)
    {
        $dn = "DC=gmf-aeroasia,DC=co,DC=id";
        $ldapconn = ldap_connect("172.16.100.46") or die ("Could not connect to LDAP server.");
        if ($ldapconn) {
            ldap_set_option(@$ldap, LDAP_OPT_PROTOCOL_VERSION, 3);
            ldap_set_option(@$ldap, LDAP_OPT_REFERRALS, 0);
            $ldapbind = ldap_bind($ldapconn, "ldap", "aeroasia");
            @$sr = ldap_search($ldapconn, $dn, "samaccountname=$username");
            @$srmail = ldap_search($ldapconn, $dn, "mail=$username@gmf-aeroasia.co.id");
            @$info = ldap_get_entries($ldapconn, @$sr);
            @$infomail = ldap_get_entries($ldapconn, @$srmail);
            @$usermail = substr(@$infomail[0]["mail"][0], 0, strpos(@$infomail[0]["mail"][0], '@'));
            @$bind = @ldap_bind($ldapconn, $info[0][dn], $password);
            /*print("<pre>".print_r(@$info,true)."</pre>");
            print("<pre>".print_r(@$infomail,true)."</pre>");
            print("<pre>".print_r(@$usermail,true)."</pre>");
            print("<pre>".print_r(@$bind,true)."</pre>");
            die();*/
            if ((@$info[0]["samaccountname"][0] == $username AND $bind) OR (@$usermail == $username AND $bind)) {
                return true;
            } else {
                return false;
            }
        } else {
            echo "LDAP Connection trouble,, please try again 2/3 time";
        }
    }

    public function list_user()
    {
        $query = $this->Management_model->get_datatables();
        
        $query = explode("FROM", $query);

        $table = explode("JOIN", $query[1]);
        $table_fix = $table[0];

        $order = explode("ORDER", $table[1]);
        $order_fix = $order[1];

        $where = explode("WHERE", $order[0]);
        if (count($where) == 2) {
            $where_fix = "WHERE (".$where[1].")";

            $sess_login = $this->session->userdata('logged_in');
            if($sess_login['user_group_level'] == 1){
                $where_fix .= " AND TB_USER_GROUP.LEVEL IN (1,2,3,4)";
            }elseif($sess_login['user_group_level'] == 2){
                $where_fix .= " AND TB_USER_GROUP.LEVEL IN (3,4)";
            }else{
                $where_fix .= " AND TB_USER_GROUP.LEVEL NOT IN (1,2,3,4)";
            }
        }else{
            $where_fix = "";

            $sess_login = $this->session->userdata('logged_in');
            if($sess_login['user_group_level'] == 1){
                $where_fix .= " WHERE TB_USER_GROUP.LEVEL IN (1,2,3,4)";
            }elseif($sess_login['user_group_level'] == 2){
                $where_fix .= " WHERE TB_USER_GROUP.LEVEL IN (3,4)";
            }else{
                $where_fix .= " WHERE TB_USER_GROUP.LEVEL NOT IN (1,2,3,4)";
            }
        }


        $start = $_REQUEST['start'];
        $end   = $_REQUEST['length']+$_REQUEST['start'];

        $sql = "WITH TblDatabases AS
                (
                SELECT 
                TB_USER_GROUP.USER_GROUP,
                TB_USER.*,
                ROW_NUMBER() OVER (ORDER $order_fix) as Row 
                FROM TB_USER
                JOIN TB_USER_GROUP ON TB_USER_GROUP.ID_USER_GROUP = TB_USER.GROUP_ID
                $where_fix
                )
                SELECT * FROM TblDatabases WHERE Row > $start and Row <= $end";
        $list = $this->Management_model->get_datatables_query($sql);

        $data = array();
        $no = $_REQUEST['start'];
        foreach ($list as $detail) {   
            if($detail->STATUS){
                $status = "<span class='label label-success'>Active</span>";
            }else{
                $status = "<span class='label label-danger'>Non-Active</span>";
            }

            if($detail->PASSWORD == "using ldap"){
                $act = "<center>
            <button class='btn btn-success btn-xs btn_edit' data-toggle='modal' title='Edit Data' data-x='".$detail->ID_USER."' data-y='".$detail->GROUP_NAME_ID."' data-ldap='yes' data-target='#modalEdit'><i class='fa fa-edit'></i></button>
            </center>";
            }else{
                $act = "<center>
            <button class='btn btn-success btn-xs btn_edit' data-toggle='modal' title='Edit Data' data-x='".$detail->ID_USER."' data-y='".$detail->GROUP_NAME_ID."' data-ldap='no' data-target='#modalEdit'><i class='fa fa-edit'></i></button>
            &nbsp&nbsp&nbsp
            <button class='btn btn-warning btn-xs btn_reset' data-toggle='modal' title='Reset Password' data-x='".$detail->ID_USER."' data-y='".$detail->GROUP_NAME_ID."' data-target='#modalPassword'><i class='fa fa-lock'></i></button>
            </center>";
            }

            $no++;
            $row = array();
            $row['no']          = "<center>".$no."</center>";
            $row['name']        = "<center>".$detail->NAME."</center>";
            $row['username']    = "<center>".$detail->USERNAME."</center>";
            $row['role']        = "<center>".$detail->USER_GROUP."</center>";
            $row['status']      = "<center>".$status."</center>";
            $row['act']         = $act;
 
            $data[] = $row;
        }
 
        $output = [
            "draw" => $_REQUEST['draw'],
            "recordsTotal" => $this->Management_model->count_all(),
            "recordsFiltered" => $this->Management_model->count_filtered(),
            "data" => $data,
            "query" => $sql,
            "where" => $where_fix,
        ];
        //output to json format
        echo json_encode($output);
    }

    public function list_role()
    {
        $query = $this->Management_model->get_datatables_role();
        $query = explode("FROM", $query);

        $order = explode("ORDER", $query[1]);
        $order_fix = $order[1];

        $table = explode("WHERE", $order[0]);
        $table_fix = $table[0];
        if (count($table) == 2) {
            $where_fix = "WHERE (".$table[1].")";
        }else{
            $where_fix = "";
        }

        $start = $_REQUEST['start'];
        $end   = $_REQUEST['length']+$_REQUEST['start'];

        $sql = "WITH TblDatabases AS
        (
        SELECT *, ROW_NUMBER() OVER (ORDER $order_fix) as Row FROM $table_fix
        $where_fix
        )
        SELECT * FROM TblDatabases WHERE Row>$start and Row<=$end";
        
        $list = $this->Management_model->get_datatables_query($sql);
        $data = array();
        $no = $_REQUEST['start'];
        foreach ($list as $item) {
            $no++;
            $row = array();
            $row['no']          = "<center>".$no."</center>";
            $row['user_group']  = "<center>".$item->USER_GROUP."</center>";
            $row['level']       = "<center>".$item->LEVEL."</center>";
            $row['act']         = "<center>
            <button class='btn btn-success btn-xs btn_edit' data-toggle='modal' title='Edit Role' data-x='".$item->ID_USER_GROUP."' data-target='#modalEditRole'><i class='fa fa-edit'></i></button>
            &nbsp&nbsp&nbsp
            <button class='btn btn-danger btn-xs btn_delete' data-toggle='modal' title='Delete Role' data-x='".$item->ID_USER_GROUP."' data-target='#modalDetailRole'><i class='fa fa-trash'></i></button>
            </center>";
            $data[] = $row;
        }
        $output = [
            "draw" => $_REQUEST['draw'],
            "recordsTotal" => $this->Management_model->count_all_role(),
            "recordsFiltered" => $this->Management_model->count_filtered_role(),
            "data" => $data,
        ];
        echo json_encode($output);
    }

    public function list_group()
    {
        $query = $this->Management_model->get_datatables_group();
        $query = explode("FROM", $query);

        $order = explode("ORDER", $query[1]);
        $order_fix = $order[1];

        $table = explode("WHERE", $order[0]);
        $table_fix = $table[0];
        if (count($table) == 2) {
            $where_fix = "WHERE (".$table[1].")";
        }else{
            $where_fix = "";
        }


        $start = $_REQUEST['start'];
        $end   = $_REQUEST['length']+$_REQUEST['start'];

        $sql = "WITH TblDatabases AS
        (
        SELECT *, ROW_NUMBER() OVER (ORDER $order_fix) as Row FROM $table_fix
        $where_fix
        )
        SELECT * FROM TblDatabases WHERE Row>$start and Row<=$end";
        
        $list = $this->Management_model->get_datatables_query($sql);
        $data = array();
        $no = $_REQUEST['start'];
        foreach ($list as $detail) {   
            if($detail->is_active){
                $status = "<span class='label label-success'>Active</span>";
            }else{
                $status = "<span class='label label-danger'>Non-Active</span>";
            }      
            $no++;
            $row = array();
            $row['no']          = "<center>".$no."</center>";
            $row['group']        = "<center>".$detail->name."</center>";
            $row['status']      = "<center>".$status."</center>";
            $row['act']         = "<center>
            <a href='".base_url('index.php/management/edit_group/'.$detail->id)."?v=n' class='btn btn-success btn-xs btn_edit' title='Edit Group'><i class='fa fa-edit'></i></a>
            &nbsp&nbsp&nbsp
            <a href='".base_url('index.php/management/edit_group/'.$detail->id)."?v=y' class='btn btn-warning btn-xs btn_detail' title='Detail Group'><i class='fa fa-eye'></i></a>
            </center>";
 
            $data[] = $row;
        }
 
        $output = [
            "draw" => $_REQUEST['draw'],
            "recordsTotal" => $this->Management_model->count_all_group(),
            "recordsFiltered" => $this->Management_model->count_filtered_group(),
            "data" => $data,
            /*"query" => $sql,
            "where" => $where_fix,*/
        ];
        //output to json format
        echo json_encode($output);
    }

    public function get_user_by_id()
    {
        $id_user = $this->input->post('id_user');

        if(!empty($id_user)) {
            $data = $this->Management_model->getUserByID($id_user);
            echo json_encode($data);
        }
    }

    public function get_group_by_id()
    {
        $id_group = $this->input->post('id_group');

        if(!empty($id_group)) {
            $data = $this->Management_model->getGroupByID($id_group);
            foreach ($data as $key) {
                $data_arr[$key->MODUL_NAME]['value'][] = $key->MENU_ID."_".$key->MODUL_ID;
                $data_arr[$key->MODUL_NAME]['name'][] = $key->GROUP_NAME;
                $data_arr[$key->MODUL_NAME]['status'][] = $key->STATUS;
            }
            echo json_encode($data_arr);
        }
    }

    public function get_group_top_by_id()
    {
        $id_group = $this->input->post('id_group');

        if(!empty($id_group)) {
            $data = $this->Management_model->getGroupTopByID($id_group);
            foreach ($data as $key) {
                $data_arr[$key->MODUL_NAME."_tab[]"]['value'][] = $key->MENU_ID."_".$key->MODUL_ID;
                $data_arr[$key->MODUL_NAME."_tab[]"]['name'][] = $key->GROUP_NAME;
            }
            echo json_encode($data_arr);
        }
    }

    public function get_menu_top()
    {
        error_reporting(0);
        $id_group = $this->input->post('id_group');

        if(!empty($id_group)) {
            $data = $this->Management_model->getMenuTop($id_group);
            if(count($data) != 0){
                echo json_encode($data);
            }else{
                echo json_encode($data);
            }
        }
    }

    public function get_menu_top_config()
    {
        error_reporting(0);
        $id_user = $this->input->post('id_user');
        $id_group = $this->input->post('id_group');

        if(!empty($id_user)) {
            $data = $this->Management_model->getMenuTopConfig($id_group,$id_user);
            if(count($data) != 0){
                echo json_encode($data);
            }else{
                echo json_encode($data);
            }
        }
    }

    public function update_user()
    {
        $url = base_url('index.php')."/management/user";
        $id_user = $this->input->post('id_user');
        $level = $this->input->post('level_user');
        $password = $this->input->post('password_user');
        $nama = $this->input->post('nama-edit');
        $username = $this->input->post('username-edit');
        $group_id = $this->input->post('role-edit');
        $customer_id = $this->input->post('customer-edit');
        $work_id = $this->input->post('work-edit');
        $group_name_id = $this->input->post('group-edit');
        $status = $this->input->post('status-edit');

        $data = array(
            'NAME'          => $nama,
            'GROUP_ID'      => $group_id,
            'GROUP_NAME_ID' => $group_name_id,
            'status'        => $status
        );

        if($level == "1"){
            $data['USERNAME'] = $username;
        }elseif ($level == "2") {
            $data['USERNAME'] = $username;
            $data['WORK_AREA_ID'] = $work_id;
        }elseif ($level == "3") {
            $data['USERNAME'] = $username;
            $data['WORK_AREA_ID'] = $work_id;
        }elseif ($level == "4") {
            $data['USERNAME'] = $username;
            $data['CUSTOMER_ID'] = $customer_id;
            unset($data['GROUP_ID']);
        }
        
        $update_user = $this->Management_model->updateUser($data, $id_user);
        if ($update_user) {
            if(isset($_POST['config'])){
                $this->Management_model->deleteManagementTabConfig($id_user);
                for($a=0;$a<count($_POST['config']);$a++){
                    $data = explode("_", $_POST['config'][$a]);
                    $param = [
                        'user_id' => $id_user,
                        'group_id' =>  $data[0],
                        'modul_id' =>  $data[1],
                        'menu_id' =>  $data[2],
                    ];
                    $this->Management_model->creatManagementTabConfig($param);
                }
            }

            $this->session->set_flashdata('alert',"<div class='alert alert-success'>success update user</div>");
            header("Location: $url");
        } else {
            $this->session->set_flashdata('alert',"<div class='alert alert-warning'>failed update user</div>");
            header("Location: $url");
        }
    }

    public function update_password()
    {
        $id_user = $this->input->post('id_user');
        $new_password = $this->input->post('password');

        $data = array(
            'PASSWORD'  => $new_password
        );

        $this->Management_model->updateUser($data, $id_user);
    }

    public function get_role_by_id()
    {
        $id_user_group = $this->input->post('id_user_group');

        if(!empty($id_user_group)) {
            $data = $this->Management_model->getRoleByID($id_user_group);
            echo json_encode($data);
        }
    }

    public function update_role()
    {
        $id_user_group = $this->input->post('id_user_group');
        $user_group = $this->input->post('user_group');
        $level = $this->input->post('level');

        $data = array(
            'USER_GROUP'    => $user_group,
            'LEVEL'         => $level,
        );

        $this->Management_model->updateRole($data, $id_user_group);
    }

    public function delete_role()
    {
        $id_user_group = $this->input->post('id_user_group');
        
        $cek = $this->Management_model->checkRoleAlreadyUsed($id_user_group);
        $response = array();
        if($cek == true) {
            $response['status'] = 'failed';
        } else {
            $this->Management_model->deleteRole($id_user_group);
            $response['status'] = 'success';
        }
        echo json_encode($response);
    }

    public function create_role()
    {
        $url = base_url('index.php')."/management/user#create_role";
        $role_name = $this->input->post('role_name');
        $level = $this->input->post('level');

        $param = [ 
                'USER_GROUP' => $role_name,
                'LEVEL' => $level
        ];

        try{

            $role = $this->User_group_model;
            $role->USER_GROUP = $role_name;
            $role->LEVEL = $level;
            $role->save();

            if ($role) {
				$this->session->set_flashdata('alert',"<div class='alert alert-success'>success create new Role</div>");
				header("Location: $url");
            } else {
                $this->session->set_flashdata('alert',"<div class='alert alert-warning'>Something Wrong </div>");
				header("Location: $url");
            }

        } catch(Exception $e) {

        }
            
    }
}
?>