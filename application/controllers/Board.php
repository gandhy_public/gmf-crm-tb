<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require_once('assets/editablegrid/EditableGrid.php');
require_once APPPATH . 'libraries/spout/src/Spout/Autoloader/autoload.php';

use Illuminate\Database\Eloquent\Model as ELOQ;
use Box\Spout\Reader\ReaderFactory;
use Box\Spout\Common\Type;
use Box\Spout\Writer\WriterFactory;

class Board extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->model('Project_model');
        $this->load->model('Dashboard_model');
        $this->load->model('Jobcard_model');
        $this->load->model('Proc_model');
        $this->load->model('Mdr_model');
        $this->load->model('Phase_model');
        $this->load->model('Board_model');
        $this->load->model('Daily_day_model');
        $this->load->library('menu_lib');
        $this->load->helper('auth_helper');
        $this->load->helper('text_formatter');
    }

    public function index() {
        if ($this->uri->segment(3)) {
            $this->session->set_userdata('REVNR', $this->uri->segment(3));
            $data['REVNR'] = $this->session->userdata('REVNR');
        }

        $data['project'] = $this->Project_model->get_projects_by_id($data['REVNR']);
        $daily_menu = $this->Daily_day_model
                    ->select("CURRENT_STATUS")
                    ->where("REVNR", $data['REVNR'])
                    ->orderBy("CREATED_AT", "DESC")
                    ->first();
        $data['today_phase'] = $daily_menu ? $daily_menu->CURRENT_STATUS : '';
        $this->load->view('board/index', $data);
    }

    public function get_projects_stats() {
        $response["status"] = NULL;
        $response["body"] = NULL;

        try {
            $response["status"] = 'success';
            $REVNR = $_GET['REVNR'];

            $jc_count_by_status = $this->Jobcard_model->get_jobcard_count_by_status($REVNR);
            $mdr_count_by_status = $this->Mdr_model->get_mdr_count_by_status($REVNR);
            // $jc_phase = $this->Phase_model->get_jobcard_phase($REVNR)

            $jobcard = $this->Dashboard_model->getJobcardByStatus($REVNR);
            $mdr = $this->Dashboard_model->getMdrByStatus($REVNR);

            if ($jobcard) {
                $response["body"]["jc_status"] = $jobcard;
            }
            if ($mdr) {
                $response["body"]["mdr_status"] = $mdr;
            }
        } catch (Exception $e) {
            echo $e;
        }

        header('Content-Type: application/json');
        echo json_encode($response);
    }
    
    public function get_jc_phase() {
        $response["status"] = NULL;
        $response["body"] = NULL;

        try {
            $jc_phase = $this->Proc_model->get_phase_progress($_GET['REVNR']);
            $data = array();    
            if ($jc_phase) {             
                foreach($jc_phase as $key => $value){
                    if($value->CLOSED > 0)
                    array_push($data, array("LABELS" => $value->PHASE, "QTY" => get_percentage($value->CLOSED, $value->TOTAL)));
                }
            }

            $response["status"] = 'success';
            $response["body"] = $data;
        } catch (Exception $e) {
            echo $e;
        }

        header('Content-Type: application/json');
        echo json_encode($response);
    }
    
    public function get_mat_qty() {
        $response["status"] = NULL;
        $response["body"] = NULL;

        try {
            $response["status"] = 'success';
            $response["body"] = $this->Board_model->get_mat_qty($_GET['REVNR']);
        } catch (Exception $e) {
            echo $e;
        }

        header('Content-Type: application/json');
        echo json_encode($response);
    }
    
    public function get_jc_area() {
        $response["status"] = NULL;
        $response["body"] = NULL;

        try {
            $response["status"] = 'success';
            $response["body"] = $this->Board_model->get_jc_area($_GET['REVNR']);
        } catch (Exception $e) {
            echo $e;
        }

        header('Content-Type: application/json');
        echo json_encode($response);
    }
    
    public function get_mdr_compare() {
        $response["status"] = NULL;
        $response["body"] = NULL;

        try {
            
            $jc_phase = $this->Proc_model->get_mdr_by_area($_GET['REVNR']);
            $data = array();    
            if ($jc_phase) {             
                foreach($jc_phase as $key => $value){
                    if($value->TOTAL > 0)
                    array_push($data, array("LABELS" => $value->AREA, "QTY_CLOSE" => $value->CLOSED, "QTY_OPEN" => $value->OPEN));
                }
            }

            $response["status"] = 'success';
            $response["body"] = $data;
        } catch (Exception $e) {
            echo $e;
        }

        header('Content-Type: application/json');
        echo json_encode($response);
    }
    
    public function get_jc_skill() {
        $response["status"] = NULL;
        $response["body"] = NULL;

        try {
            $response["status"] = 'success';
            $response["body"] = $this->Board_model->get_jc_skill($_GET['REVNR']);
        } catch (Exception $e) {
            echo $e;
        }

        header('Content-Type: application/json');
        echo json_encode($response);
    }
    
    public function get_mdr_shop() {
        $response["status"] = NULL;
        $response["body"] = NULL;

        try {
            $data = array();
            $mdr = $this->Board_model->get_mdr_shop($_GET['REVNR']);

            $data = array();    
            if ($mdr) {             
                foreach($mdr as $key => $value){
                    if($value->TOTAL > 0)
                    array_push($data, array("LABELS" => str_replace('SENT TO', '', $value->LABELS ), "QTY" => $value->TOTAL));
                }
            }

            if ( count($data) > 0 ) {
                $response["status"] = 'success';
                $response["body"] = $data;
            } else {
                $response["status"] = 'error';
                $response["body"] = NULL;
            }
        } catch (Exception $e) {
            echo $e;
        }

        header('Content-Type: application/json');
        echo json_encode($response);
    }

    public function get_mdr_acc_status() {
        $response["status"] = NULL;
        $response["body"] = NULL;

        try {
            $response["status"] = 'success';
            $response["body"] = $this->Board_model->get_mdr_acc_status($_GET['REVNR']);

        } catch (Exception $e) {
            echo $e;
        }

        header('Content-Type: application/json');
        echo json_encode($response);
    }    

}
