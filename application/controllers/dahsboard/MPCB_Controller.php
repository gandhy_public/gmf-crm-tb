<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require_once('assets/editablegrid/EditableGrid.php');

class MPCB_Controller extends CI_Controller {

	public function __construct() {
        	parent::__construct();
        	$this->load->database();
        	$this->load->model('Jobcard_model_view');
	}

	public function get_project_percentage(){
		$project_id = '00027794';

		$jobcard = $this->Jobcard_model_view->get_jobcard_close_percentage($project_id);
		$mdr = $this->Jobcard_model_view->get_mdr_close_percentage($project_id);

		$data["code"] = 200;
		$data['result'] = array();
		$obj = new stdClass(); 
		$obj->jobcard = $jobcard;
		$obj->mdr = $mdr;
		array_push($data['result'], $obj);
		header('Content-Type: application/json');
		echo json_encode($data);
	}
}