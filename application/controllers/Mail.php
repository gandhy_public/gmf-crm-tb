<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Mail extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $config = Array(
            'protocol' => 'smtp',
            'smtp_host' => 'mail.sisi.id',
            'smtp_port' => 587,
            'smtp_user' => 'okky.permana@sisi.id',
            'smtp_pass' => 'password',
            'smtp_crypto' => 'tls',
            'mailtype' => 'html'
        );
        $this->load->library('email', $config);
        $this->email->set_newline("\r\n");
    }

    public function index() {
        try {
            $this->email->clear();

            $this->email->to('okky.permana@sisi.id');
            $this->email->from('okky.permana@sisi.id');
            $this->email->subject('Here is your info ');
            $this->email->message('Hi Here is the info you requested.');
            if (!$this->email->send()) {
                echo 'Mail failed to sent';
            } else {
                echo 'Mail sent';
            }
        } catch (Exception $ex) {
            echo json_encode($ex);
        }
    }

}
