<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require_once('assets/editablegrid/EditableGrid.php');

class Dashboard extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->database();
    

        $this->data["session"] = $this->session->userdata('logged_in');
        if ($this->data["session"]["group_id"] != "1") {
            redirect('login/logout', 'refresh');
        }
    }

    public function index() {  

        $data["session"] = $this->session->userdata('logged_in');
        $data['content'] = 'manager/dashboard/index';

        $this->load->view('template', $data);
    }

    

}
