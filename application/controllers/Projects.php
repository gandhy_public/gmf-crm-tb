<?php
ini_set('max_execution_time', 9999999);
defined('BASEPATH') OR exit('No direct script access allowed');

require_once('assets/editablegrid/EditableGrid.php');
require_once APPPATH . 'libraries/spout/src/Spout/Autoloader/autoload.php';

use Illuminate\Database\Eloquent\Model as ELOQ;
use Illuminate\Database\Capsule\Manager as DB;
use Box\Spout\Reader\ReaderFactory;
use Box\Spout\Common\Type;
use Box\Spout\Writer\WriterFactory;
use Box\Spout\Writer\Style\StyleBuilder;
use Box\Spout\Writer\Style\Color;
use Box\Spout\Writer\Style\Border;
use Box\Spout\Writer\Style\BorderBuilder;


require 'vendor/autoload.php';
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Reader\Xlsx;

class Projects extends CI_Controller {

    private $is_customer;

    public function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->library('menu_lib');
        $this->load->model('Project_model_ci', '', TRUE);
        $this->load->model('Project_model');
        $this->load->model('Revh_model');
        $this->load->model('Management_model');
        $this->load->model('VProject_model');
        $this->load->model('Customer_model');
        $this->load->model('Dashboard_model', '', TRUE);
        $this->load->model('Jobcard_model');
        $this->load->model('Order_model');
        $this->load->model('OrderH_model');
        $this->load->model('Proc_model');
        $this->load->model('Jobcard_model_view');
        $this->load->model('Mdr_model');
        $this->load->model('Mdr_model_view');
        $this->load->model('Mrm_model');
        $this->load->model('Mrm_model_view');
        $this->load->model('Prm_model');
        $this->load->model('Prm_model_view');
        $this->load->model('Crm_model');
        $this->load->model('Ticket_model');
        $this->load->model('Csr_model');
        $this->load->model('Csr_model_view');
        $this->load->model('Daily_menu_model_view');
        $this->load->model('Daily_menu_model_ci');
        $this->load->model('Daily_day_model');
        $this->load->model('Daily_day_model_view');
        $this->load->model('Daily_area_model');
        $this->load->model('Daily_details_model');
        $this->load->model('Maint_phase_model');
        $this->load->model('Maint_milestone_model');
        $this->load->model('Daily_day_progress_model');
        $this->load->model('Users');
        $this->load->model('User_group_model');
        $this->load->model('Phase_model');
        $this->load->model('Phase_model_view');
        $this->load->model('Material_model');
        $this->load->helper('auth_helper');
        $this->load->helper('text_formatter');
	$this->load->model('Api_Proc_model');
        // $this->output->enable_profiler(TRUE);
        // Cek User Seesion
        $this->data["session"] = $this->session->userdata('logged_in');
        
        $this->is_customer = is_customer($this->data["session"]["user_group"]);

        has_session($this->data["session"]);

        $menu_tab = $this->session->userdata('menu_tab');
        if($menu_tab){
            $active_menu = $this->router->fetch_class() . '/' . $this->router->fetch_method();
            //sementara
            if($active_menu=="projects/crud_mdr_new"){
                $active_menu="projects/crud_mdr";
            }else if($active_menu=="projects/crud_jobcard_new"){
                $active_menu="projects/crud_jobcard";
            }
            //sementara
            $idx = array_search($active_menu, array_column($menu_tab, "l"));
            $this->is_write = $menu_tab[$idx]["w"] == 1 ? 1 : 0;
        }
    }


    public function callback_project($value, $row) {

        return '<a href="' . base_url() . 'admin/projects/crud_jobcard/' . $row->Id . '">' . $value . '</a>';
    }

    public function callback_jobcard_progress($value, $row) {
        $close = $this->Jobcard_model->where('id_project', '=', $row->Id)
                ->where('status', 'like', 'Close')
                ->count();
        $all = $this->Jobcard_model->where('id_project', '=', $row->Id)->count();
        $persen = round(($close / ($all ? $all : 1)) * 10000) / 100;
        return '<a href="' . base_url() . 'admin/projects/crud_jobcard/' . $row->Id . '">' . $persen . '%</a>';
    }

    public function callback_mdr_progress($value, $row) {
        $close = $this->Mdr_model->where('id_project', '=', $row->Id)
                ->where('status', 'like', 'Close')
                ->count();
        $all = $this->Mdr_model->where('id_project', '=', $row->Id)->count();
        $persen = round(($close / ($all ? $all : 1)) * 10000) / 100;
        return '<a href="' . base_url() . 'admin/projects/crud_mdr/' . $row->Id . '">' . $persen . '%</a>';
    }

    public function check_projectdates($finish_date, $start_date) {
        $parts = explode('/', $this->input->post('start_date'));
        $start_date = join('-', $parts);
        $start_date = strtotime($start_date);

        $parts2 = explode('/', $this->input->post('finish_date'));
        $finish_date = join('-', $parts2);
        $finish_date = strtotime($finish_date);

        if ($finish_date >= $start_date) {

            return true;
        } else {
            $this->form_validation->set_message('check_projectdates', "finish date should be greater than start date");
            return false;
        }
    }

    public function crud_project_customer_name_add_field_callback() {
        return $this->Customer_model->where('ID_CUSTOMER', $this->uri->segment(4))->first()->COMPANY_NAME . '<input type="hidden" name="customer_name" value="' . $this->uri->segment(4) . '" />';
    }

    public function crud_customer() {
        $data['is_grocery'] = true;
        $crud = new grocery_CRUD();
        $crud->set_table('customer');
        $crud->set_subject('Customer');
        $crud->unset_fields('ID_CUSTOMER');
        $crud->unset_columns('ID_CUSTOMER');
        $crud->unset_add();
        $crud->unset_read();
        $crud->unset_delete();
        $crud->unset_edit();
        $crud->add_action('Project', base_url() . 'assets/dist/img/icons_dashboard.png', 'admin/projects/index');
        $output = $crud->render();

        $data["session"] = $this->session->userdata('logged_in');
        $data['content'] = 'admin/projects/crud_customer';

        $this->load->view('template', array_merge($data, (array) $output));
    }

    public function encrypt_password($post_array) {
        $this->load->helper('security');
        $post_array['PASSWORD'] = do_hash($post_array['PASSWORD'], 'md5');
        $post_array['STATUS'] = 1;
        return $post_array;
    }

    public function crud_daily_day_list() {
        try {
            if ($this->uri->segment(4)) {
                $this->session->set_userdata('REVNR', $this->uri->segment(4));
            }
            $data['id_project'] = $this->session->userdata('REVNR');
            $active_prj = $this->Project_model_ci->getProjectById($data['id_project']);

            if ($active_prj) {
                $data['project_name'] = $active_prj[0]->REVTX;
            }
        } catch (Exception $e) {
            error_log($e);
        }

        $data["session"] = $this->session->userdata('logged_in');
        $data['content'] = 'admin/projects/crud_daily_day_lists';

        $this->load->view('template', $data);

        return true;
    }

    public function order_progress_count() {
        $response["status"] = NULL;
        $response["body"] = NULL;

        try {
            $data = array();
            $REVLIST = json_decode($_POST['REVLIST']);
            $order = $this->Proc_model->get_order_count($REVLIST);
            if ($order) {
                foreach ($order as $value) {
                    $jc_percentage = get_percentage($value->JC_CLOSED, $value->JC_TOTAL);
                    $mdr_percentage = get_percentage($value->MDR_CLOSED, $value->MDR_TOTAL);

                    array_push($data, array(
                        "REVNR" => $value->REVNR,
                        "JC_PROGRESS" => $jc_percentage,
                        "MDR_PROGRESS" => $mdr_percentage,
                        "JC_TOTAL" => $value->JC_TOTAL,
                        "MDR_TOTAL" => $value->MDR_TOTAL
                    ));
                }
                $response["status"] = 'success';
                $response["body"] = $data;
            } else {
                $response["status"] = 'error';
                $response["body"] = NULL;
            }

        } catch (Exception $e) {
            $response["status"] = 'failed';
            $response["body"] = NULL;
        }

        header('Content-Type: application/json');
        echo json_encode($response);
    }

    public function order_mhrs_count() {
        $response["status"] = NULL;
        $response["body"] = NULL;

        try {
            $data = array();
            $REVLIST = json_decode($_POST['REVLIST']);
            $order = $this->Project_model->get_mhrs($REVLIST);

            if ($order) {

                $sum['actual'] = array();
                $sum['plan'] = array();
                foreach ($order as $value) {
                    if (!array_key_exists($value['REVNR'], $sum['actual'])) {
                        settype($sum['actual'][$value['REVNR']], "float");
                        $sum['actual'][$value['REVNR']] = 0.0;
                    }
                    $sum['actual'][$value['REVNR']] += floatval($value->ACTUAL);


                    if (!array_key_exists($value['REVNR'], $sum['plan'])) {
                        settype($sum['plan'][$value['REVNR']], "float");
                        $sum['plan'][$value['REVNR']] = 0.0;
                    }
                    $sum['plan'][$value['REVNR']] += floatval($value->PLN);
                }

                foreach ($sum['actual'] as $key => $value) {
                    array_push($data, array(
                        'REVNR' => $key,
                        'ACTUAL' => $value,
                        'PLAN' => $sum['plan'][$key],
                        'CONSUMED' => get_percentage($value, $sum['plan'][$key])
                    ));
                }

                $response["status"] = 'success';
                $response["body"] = $data;
            } else {
                $response["status"] = 'error';
                $response["body"] = NULL;
            }
        } catch (Exception $e) {
            echo $e;
            $response["status"] = 'failed';
            $response["body"] = $e;
        }

        header('Content-Type: application/json');
        echo json_encode($response);
    }

    public function crud_daily_day_list_load() {
        $response["status"] = NULL;
        $response["body"] = NULL;

        $project_id = $this->input->post('REVNR');
        $report_list = $this->Daily_day_model;

        try {

            if ($project_id != '') {
                $report_list = $report_list->where('REVNR', $project_id);
            }

            $report_list = $report_list->select(
                            'TB_DAILY_DAY.ROW_NUM', 
                            'TB_DAILY_DAY.REPORT_NAME', 
                            'TB_USER.NAME', 
                            'TB_DAILY_DAY.CREATED_BY', 
                            'TB_DAILY_DAY.CREATED_AT', 
                            'TB_DAILY_DAY.STATUS', 
                            'TB_DAILY_DAY.ID'
                    )
                    ->leftJoin('TB_USER', function($leftJoin) {
                        $leftJoin->on('TB_DAILY_DAY.UPDATED_BY', '=', 'TB_USER.ID_USER');
                    })
                    ->orderBy('CREATED_AT', 'DESC')
                    ->get();

            if ($report_list) {
                if(count($report_list) > 0) { 
                    $last_status = $report_list[0]['STATUS'];
                    $response["body"]["last_status"] = $last_status;
                }
                $response["status"] = 'success';
                $response["body"]["report_list"] = $report_list;
            } else {
                $response["status"] = 'error';
                $response["body"] = NULL;
            }
        } catch (Exception $e) {
            $response["status"] = 'FAILED';
            $response["body"] = $e;
        }
        header('Content-Type: application/json');
        echo json_encode($response);
    }

    public function crud_daily_report_copy() {
        date_default_timezone_set('Asia/Jakarta');
        $response["status"] = NULL;
        $response["body"] = NULL;

        $project_id = $this->input->post('REVNR');
        $userdata = $this->session->userdata('logged_in');

        $report = $this->Daily_day_model;
        $new_report = $this->Daily_day_model;
        $technical_details = $this->Daily_details_model;
        


        try {
            // $report = $report->where('ID', $_POST['ID'])->first();
            $check_duplicate = $this->Daily_day_model->select("CREATED_AT")
                                    ->where('REVNR', $this->input->post('REVNR'))
                                    ->whereDate('CREATED_AT', 'like', date('Y-m-d'))
                                    ->count();

            if ($check_duplicate > 0) {
                $response["status"] = 'error';
                $response["body"] = 'Report can not be duplicate!'  ;
            } else {
                $report = $report->where('REVNR', $this->input->post('REVNR'))->orderBy('CREATED_AT', 'DESC')->first();
                if ($report) {
                    // Copy / Select Header (Data Report)    
                    $data_report = $report->attributesToArray();
                    $data_report = array_except($data_report, ['ID', 'CREATED_AT', 'CUSTOMER', 'REPORT_NAME', 'ROW_NUM', 'CREATED_BY', 'UPDATED_AT', 'UPDATED_BY', 'STATUS']);
                    $data_report['REPORT_NAME'] = $_POST['REPORT_NAME'];
                    $data_report['ID_USER'] = $userdata['id_user'];
                    $data_report['CREATED_AT'] = date("Y/m/d h:i:sa");
                    $data_report['CREATED_BY'] = $userdata['id_user'];
                    $data_report['UPDATED_AT'] = date("Y/m/d h:i:sa");
                    $data_report['UPDATED_BY'] = $userdata['id_user'];
                    $data_report['UPDATED_BY'] = $userdata['id_user'];
                    $data_report['STATUS'] = 'OPEN';
                    $new_report = $new_report->create($data_report);
    
                    if ($new_report) {
                        $new_report = $new_report->where('ID_USER', $userdata['id_user'])
                                ->where('REVNR', $project_id)
                                ->orderBy('CREATED_AT', 'DESC')
                                ->first();
    
                        $response["status"] = 'success';
                        $response["body"] = $new_report;
    
    
                        $this->crud_technical_details_copy($new_report->ID, $report->ID);
                        $this->project_phase_copy($new_report->ID, $report->ID);
                        $this->daily_day_progress_copy($new_report->ID, $report->ID, $new_report->CREATED_AT);
                    }
                } else {
                    $response["status"] = 'error';
                    $response["body"] = 'Something Wrong!';
                }
            }
        } catch (Exception $e) {
            $response["status"] = 'failed';
            $response["body"] = $e;
        }
        header('Content-Type: application/json');
        echo json_encode($response);
    }

    public function crud_daily_report_add() {
        $response["status"] = NULL;
        $response["body"] = NULL;

        $project_id = $this->input->post('REVNR');
        $userdata = $this->session->userdata('logged_in');
        date_default_timezone_set('Asia/Jakarta');

        $mileston = $this->Maint_milestone_model;
        $phase = $this->Maint_phase_model;

        try {
            $report = $this->Daily_day_model;
            $report->REVNR = $project_id;
            $report->ID_USER = $userdata['id_user'];
            $report->REPORT_NAME = $_POST['REPORT_NAME'];
            $report->CREATED_AT = date("Y/m/d h:i:sa");
            $report->CREATED_BY = $userdata['id_user'];
            $report->UPDATED_AT = date("Y/m/d h:i:sa");
            $report->UPDATED_BY = $userdata['id_user'];
            $report->STATUS = 'OPEN';
            $report->SOA_OPEN = 0;
            $report->SOA_CANCEL = 0;
            $report->SOA_APPROVED = 0;
            $report->WEA_OPEN = 0;
            $report->WEA_CANCEL = 0;
            $report->WEA_APPROVED = 0;
            $report->save();

            if ($report) {
                $report = $report->where('ID_USER', $userdata['id_user'])
                        ->where('REVNR', $project_id)
                        ->orderBy('CREATED_AT', 'DESC')
                        ->first();
                
                $response["status"] = 'success';
                $response["body"] = $report;
            } else {
                $response["status"] = 'error';
                $response["body"] = NULL;
            }
        } catch (Exception $e) {
            $response["status"] = 'failed';
            $response["body"] = $e;
        }
        header('Content-Type: application/json');
        echo json_encode($response);
    }

    public function crud_daily_report_close() {
        $response["status"] = NULL;
        $response["body"] = NULL;

        $data = array(
            'PRELIMINARY' => $_POST['PRELIMINARY'],
            'INSPECTION' => $_POST['INSPECTION'],
            'INSTALLATION' => $_POST['INSTALLATION'],
            'OPENING' => $_POST['OPENING'],
            'OPC' => $_POST['OPC'],
            'SERVICING' => $_POST['SERVICING'],
            'JC_OPEN' => $_POST['JC_OPEN'],
            'JC_PROGRESS' => $_POST['JC_PROGRESS'],
            'JC_CLOSED' => $_POST['JC_CLOSED'],
            'JC_TOTAL' => $_POST['JC_TOTAL'],
            'PERC_JC_OPEN' => $_POST['PERC_JC_OPEN'],
            'PERC_JC_PROGRESS' => $_POST['PERC_JC_PROGRESS'],
            'PERC_JC_CLOSED' => $_POST['PERC_JC_CLOSED'],
            'MDR_OPEN' => $_POST['MDR_OPEN'],
            'MDR_PROGRESS' => $_POST['MDR_PROGRESS'],
            'MDR_CLOSED' => $_POST['MDR_CLOSED'],
            'MDR_TOTAL' => $_POST['MDR_TOTAL'],
            'PERC_MDR_OPEN' => $_POST['PERC_MDR_OPEN'],
            'PERC_MDR_PROGRESS' => $_POST['PERC_MDR_PROGRESS'],
            'PERC_MDR_CLOSED' => $_POST['PERC_MDR_CLOSED'],
            'STATUS' => 'CLOSE'
        );

        try {
            $report = $this->Daily_day_model;
            $report->where('ID', $_POST['ID'])
                    ->where('REVNR', $_POST['REVNR'])
                    ->update($data);

            if ($report) {
                $response["status"] = 'success';
                $response["body"] = NULL;
            }
        } catch (Exception $e) {
            $response["status"] = 'failed';
            $response["body"] = $e;
        }
        header('Content-Type: application/json');
        echo json_encode($response);
    }

    public function crud_daily_day() {
        $data['content'] = 'admin/projects/crud_daily_day_lists';
        try {
            $data['id_project'] = $this->uri->segment(3) ? $this->uri->segment(3) : '0';
            $active_prj = $this->Project_model->get_project_detail($data['id_project']);
            $data['project_name'] = $active_prj ? $active_prj->REVTX : "";  
        } catch (Exception $e) {
            error_log($e);
        }

        try {
            if ($this->uri->segment(4)) {
                $data['content'] = 'admin/projects/crud_daily_day';
            } else {
                $data['content'] = 'admin/projects/crud_daily_day_lists';
            }
            $data['report_id'] = $this->uri->segment(4) ? $this->uri->segment(4) : 0 ;
        } catch (Exception $e) {
            error_log($e);
        }

        $daily_area = $this->Daily_area_model->get();
        $jc_status = $this->Dashboard_model->getJobcardByStatus($data['id_project']);
        $mdr_status = $this->Dashboard_model->getMdrByStatus($data['id_project']);
        // $phase_presentace = $this->Proc_model->get_phase_progress($data['id_project']); // $this->db->query("EXEC TB_P_PAHSE_PROGRESS @REVNR = " . $data['id_project'])->result();

        $data['project_name'] = $active_prj->REVTX;
        $data['project_data'] = $active_prj;
        $data['daily_area'] = $daily_area;
        $data['jc_status'] = $jc_status;
        $data['mdr_status'] = $mdr_status;
        // $data['phase_presentace'] = $phase_presentace;

        $data['write'] = $this->is_write;
        $data["session"] = $this->session->userdata('logged_in');
        $this->load->view('template', $data);

        return true;
    }

    public function get_project_phase(){
        $response["status"] = NULL;
        $response["body"] = NULL;

        $phase = $this->Maint_phase_model;

        try {
            $REVNR = $this->input->post('REVNR');
            $ID_REPORT = $this->input->post('ID');
            $STATUS = $this->input->post('STATUS');

            if ($STATUS == 'OPEN') {
                $phase_presentace = $this->Proc_model->get_phase_progress($REVNR);
                
                foreach($phase_presentace as $value) {
                    if($value->TOTAL > 0){
                        $phase = $phase->updateOrCreate(
                                ['DAILY_DAY_ID' =>  $ID_REPORT, 'MAINT_NAME' => $value->PHASE],
                                [
                                    'MAINT_NAME' => $value->PHASE,
                                    'PROGRESS' => get_percentage($value->CLOSED, $value->TOTAL)
                                ]
                            );
                    }
                }
            }

            $phase = $phase->where("DAILY_DAY_ID", $ID_REPORT)->orderBy("ID")->get();
            if($phase) {
                $response["status"] = 'success';
                $response["body"] = $phase;
            } else {
                $response["status"] = 'error';
                $response["body"] = NULL;
            }

        } catch (Exception $e){

        }
        header('Content-Type: application/json');
        echo json_encode($response);

    }

    public function project_phase_copy($daily_day_id, $old_daily_report) {
        try {
            $phase = $this->Maint_phase_model;
            $phase = $phase->where('DAILY_DAY_ID', $old_daily_report)
                            ->orderBy('ID', 'DESC')->get();
            if ($phase) {
                foreach ($phase as $value) {
                    $new_phase = new $this->Maint_phase_model;
                    $new_phase->MAINT_NAME = $value->MAINT_NAME;
                    $new_phase->PROGRESS = $value->PROGRESS; 
                    $new_phase->REMARKS = $value->REMARKS; 
                    $new_phase->DAILY_DAY_ID = $daily_day_id;
                    $new_phase->save();
                }
            } else {
                
            }
        } catch (Exception $e) {
            echo $e;
        }
    }

    public function update_project_phase(){
        $response["status"] = NULL;
        $response["body"] = NULL;

        $phase = $this->Maint_phase_model;

        try {
            $ID = $this->input->post('pk');
            $colname = $this->input->post('name');
            $value = $this->input->post('value');

            $phase = $phase->where("ID", $ID)
                        ->update([$colname => $value]);

            if($phase) {
                $response["status"] = 'success';
                $response["body"] = $phase;
            } else {
                $response["status"] = 'error';
                $response["body"] = NULL;
            }

        } catch (Exception $e){

        }
        header('Content-Type: application/json');
        echo json_encode($response);

    }

    public function get_daily_day_progress(){
        $response["status"] = NULL;
        $response["body"] = NULL;

        $progress = $this->Daily_day_progress_model;

        try {
            $REVNR = $this->input->post('REVNR');
            $ID_REPORT = $this->input->post('ID');
            $STATUS = $this->input->post('STATUS');
            $DATE = $this->input->post('DATE');
            

            $jc_status = $this->Dashboard_model->getJobcardByStatus($REVNR);
            $mdr_status = $this->Dashboard_model->getMdrByStatus($REVNR);

            if ($STATUS == 'OPEN') {  
                if($jc_status){
                    foreach($jc_status as $value) {
                        $progress = $progress->updateOrCreate(
                                [
                                    'DAILY_DAY_ID' =>  $ID_REPORT, 
                                    'DAILY_DAY_DATE' => $DATE,
                                    'ITEM' => 'JOBCARD',
                                ],
                                [
                                    'OPEN' => $value->JC_OPEN,
                                    'CLOSE' => $value->JC_CLOSED,
                                    'PROGRESS' => $value->JC_PROGRESS,
                                    'TOTAL' => $value->JC_TOTAL,
                                    'TYPE' => 'ORDER',
                                    'ITEM' => 'JOBCARD',
                                    'DAILY_DAY_DATE' => date('Y-m-d', strtotime($DATE))
                                ]
                            );
                        
                    }
                }else{
                    $progress = $progress->updateOrCreate(
                        [
                            'DAILY_DAY_ID' =>  $ID_REPORT, 
                            'DAILY_DAY_DATE' => $DATE,
                            'ITEM' => 'JOBCARD',
                        ],
                        [
                            'OPEN' => 0,
                            'CLOSE' => 0,
                            'PROGRESS' => 0,
                            'TOTAL' => 0,
                            'TYPE' => 'ORDER',
                            'ITEM' => 'JOBCARD',
                            'DAILY_DAY_DATE' => date('Y-m-d', strtotime($DATE))
                        ]
                    );
                }
                
                if($mdr_status){
                    foreach($mdr_status as $value) {
                        $progress = $progress->updateOrCreate(
                                [
                                    'DAILY_DAY_ID' =>  $ID_REPORT, 
                                    'DAILY_DAY_DATE' => $DATE,
                                    'ITEM' => 'NRC/MDR',
                                ],
                                [
                                    'OPEN' => $value->MDR_OPEN,
                                    'CLOSE' => $value->MDR_CLOSED,
                                    'PROGRESS' => $value->MDR_PROGRESS,
                                    'TOTAL' => $value->MDR_TOTAL,
                                    'TYPE' => 'ORDER',
                                    'ITEM' => 'NRC/MDR',
                                    'DAILY_DAY_DATE' => date('Y-m-d', strtotime($DATE))
                                ]
                            );
                        
                    }
                }else{
                    $progress = $progress->updateOrCreate(
                        [
                            'DAILY_DAY_ID' =>  $ID_REPORT, 
                            'DAILY_DAY_DATE' => $DATE,
                            'ITEM' => 'NRC/MDR',
                        ],
                        [
                            'OPEN' => 0,
                            'CLOSE' => 0,
                            'PROGRESS' => 0,
                            'TOTAL' => 0,
                            'TYPE' => 'ORDER',
                            'ITEM' => 'NRC/MDR',
                            'DAILY_DAY_DATE' => date('Y-m-d', strtotime($DATE))
                        ]
                    );
                } 
            }
            
            $progress = $progress->where("DAILY_DAY_ID", $ID_REPORT)->orderBy("ITEM")->get();
            if($progress) {
                $response["status"] = 'success';
                $response["body"] = $progress;
            } else {
                $response["status"] = 'error';
                $response["body"] = NULL;
            }

        } catch (Exception $e){
            echo $e;
        }
        header('Content-Type: application/json');
        echo json_encode($response);        
    }

    public function daily_day_progress_copy($daily_day_id, $old_daily_report, $date) {
        try {
            $daily_date = date_create($date);
            $progress = $this->Daily_day_progress_model;
            $progress = $progress->where('DAILY_DAY_ID', $old_daily_report)
                            ->orderBy('ID', 'DESC')->get();
            if ($progress) {
                foreach ($progress as $value) {
                    $new_progress = new $this->Daily_day_progress_model;
                    $new_progress->ITEM = $value->ITEM;
                    $new_progress->OPEN = $value->OPEN;
                    $new_progress->PROGRESS = $value->PROGRESS;
                    $new_progress->CLOSE = $value->CLOSE;
                    $new_progress->TOTAL = $value->TOTAL;
                    $new_progress->TYPE = $value->TYPE;
                    $new_progress->DAILY_DAY_DATE = date_format($daily_date , "Y-m-d");
                    $new_progress->DAILY_DAY_ID = $daily_day_id;


                    $new_progress->save();
                }
            } else {
                
            }
        } catch (Exception $e) {
            echo $e;
        }
    }

    public function daily_day_after_update($post_array, $primary_key) {
        $daily_day_update = array(
            "update_date_daily" => date('Y-m-d H:i:s'),
            "updateBy_daily" => $this->session->userdata('logged_in')['id_user']
        );

        $this->db->update('daily_day', $daily_day_update, array('id' => $primary_key));

        return true;
    }

    function crud_daily_day_load() {
        $response["status"] = NULL;
        $response["body"] = NULL;

        try {

            $project_id = $this->input->post('REVNR');
            $report_id = $this->input->post('REPORT_ID');
            $daily_report = $this->Daily_day_model
                    ->where('REVNR', $project_id)
                    ->where('ID', $report_id)
                    ->first();

            if ($daily_report) {
                $response["status"] = "success";
                $response["body"] = $daily_report;
            } else {
                $response["status"] = "failed";
                $response["body"] = NULL;
            }
        } catch (Exception $e) {
            $response["status"] = "error";
            $response["body"] = $e;
        }

        header('Content-Type: application/json');
        echo json_encode($response);
    }

    public function crud_daily_report_load_by_id() {
        $response["status"] = NULL;
        $response["body"] = NULL;

        try {

            $project_id = $this->session->userdata('REVNR');
            $report_id = $_POST['ID'];
            $daily_report = $this->Daily_day_model
                    ->where('REVNR', $project_id)
                    ->where('ID', $report_id)
                    ->first();

            if ($daily_report) {
                $response["status"] = "success";
                $response["body"] = $daily_report;
            } else {
                $response["status"] = "failed";
                $response["body"] = NULL;
            }
        } catch (Exception $e) {
            $response["status"] = "error";
            $response["body"] = $e;
        }

        header('Content-Type: application/json');
        echo json_encode($response);
    }

    public function crud_daily_report_delete() {
        $response["status"] = NULL;
        $response["body"] = NULL;

        try {

            $project_id = $this->session->userdata('REVNR');
            $report_id = $_POST['ID'];

            $daily_report = $this->Daily_day_model
                    ->where('REVNR', $project_id)
                    ->where('ID', $report_id)
                    ->delete();

            if ($daily_report) {
                $response["status"] = "success";
                $response["body"] = "Record has been deleted successfully";
            } else {
                $response["status"] = "failed";
                $response["body"] = NULL;
            }
        } catch (Exception $e) {
            $response["status"] = "error";
            $response["body"] = $e;
        }

        header('Content-Type: application/json');
        echo json_encode($response);
    }

    public function crud_daily_day_save() {
        if (isset($_POST['name']) && isset($_POST['value']) && isset($_POST['pk'])) {
            try {
                $name = $_POST['name'];
                $value = $_POST['value'];
                $pk = $_POST['pk'];

                $daily_report = $this->Daily_day_model->where('ID', $pk)
                        ->update([$name => $value, 'ID_USER' => $this->session->userdata('logged_in')['id_user']]);

                if ($daily_report) {
                    echo "ok";
                } else {
                    echo "failed";
                }
            } catch (Exception $e) {

                echo $e;
            }
        }
    }

    public function crud_technical_detail() {
        $response["status"] = NULL;
        $response["body"] = NULL;

        if (isset($_POST['REVNR']) && isset($_POST['AREA'])) {
            try {
                $details = $this->Daily_details_model;
                $details->REVNR = $_POST['REVNR'];
                $details->AREA = $_POST['AREA'];
                $details->TASK = $_POST['TASK'];
                $details->FOLLOW_UP = $_POST['FOLLOW_UP'];
                $details->REMARKS = $_POST['REMARKS'];
                $details->LEVEL = $_POST['LEVEL'];
                $details->STATUS = $_POST['STATUS'];
                $details->DAILY_DAY_ID = $_POST['DAILY_DAY_ID'];
                $details->save();

                if ($details) {
                    $response["status"] = 'success';
                    $response["body"] = $details;
                } else {
                    $response["status"] = 'failed';
                    $response["body"] = NULL;
                }
            } catch (Exception $e) {
                $response["status"] = 'error';
                $response["body"] = $e;
            }
        }

        header('Content-Type: application/json');
        echo json_encode($response);
    }

    public function crud_technical_details_load_all() {
        $response["status"] = NULL;
        $response["body"] = NULL;

        try {
            if (isset($_POST['REPORT_ID'])) {
                $area = $this->Daily_area_model->get();
                $details = $this->Daily_details_model;
                $report_id = $_POST['REPORT_ID'];

                $data = array();
                foreach ($area as $key => $value) {
                    $data[$value->ID] = $details
                            ->where('AREA', $value->ID)
                            ->where('DAILY_DAY_ID', $report_id)
                            ->get();
                }
            }

            $response["status"] = 'success';
            $response["body"] = $data;
        } catch (Exception $e) {
            $response["status"] = 'error';
            $response["body"] = $e;
        }

        header('Content-Type: application/json');
        echo json_encode($response);
    }

    public function crud_technical_details_update() {
        $response["status"] = NULL;
        $response["body"] = NULL;

        if (isset($_POST['name']) && isset($_POST['value']) && isset($_POST['pk'])) {
            try {
                $name = $_POST['name'];
                $value = $_POST['value'];
                $pk = $_POST['pk'];

                $daily_details = $this->Daily_details_model->where('ID', $pk);
                $daily_details->update([$name => $value]);

                if ($daily_details) {
                    $response["status"] = 'success';
                    $response["body"] = $daily_details->get();
                } else {
                    $response["status"] = 'failed';
                    $response["body"] = NULL;
                }
            } catch (Exception $e) {

                $response["status"] = 'error';
                $response["body"] = $e;
            }
        }

        header('Content-Type: application/json');
        echo json_encode($response);
    }

    public function crud_technical_details_delete() {
        $response["status"] = NULL;
        $response["body"] = NULL;

        if (isset($_POST['ID'])) {
            try {
                $pk = $_POST['ID'];

                $daily_details = $this->Daily_details_model->where('ID', $pk);
                $daily_details->delete();

                if ($daily_details) {
                    $response["status"] = 'success';
                    $response["body"] = NULL;
                } else {
                    $response["status"] = 'failed';
                    $response["body"] = NULL;
                }
            } catch (Exception $e) {
                $response["status"] = 'error';
                $response["body"] = $e;
            }
        }

        header('Content-Type: application/json');
        echo json_encode($response);
    }    

    public function crud_technical_details_copy($daily_day_id, $old_daily_report) {
        $is_success = false;
        try {
            $technical_details = $this->Daily_details_model;
            $technical_details = $technical_details
                            ->where('REVNR', $this->session->userdata('REVNR'))
                            ->where('DAILY_DAY_ID', $old_daily_report)
                            ->orderBy('CREATED_AT', 'DESC')->get();
            if ($technical_details) {
                foreach ($technical_details as $value) {
                    $new_technical_details = new $this->Daily_details_model;
                    $new_technical_details->REVNR = $value->REVNR;
                    $new_technical_details->AREA = $value->AREA;
                    $new_technical_details->TASK = $value->TASK;
                    $new_technical_details->FOLLOW_UP = $value->FOLLOW_UP;
                    $new_technical_details->REMARKS = $value->REMARKS;
                    $new_technical_details->LEVEL = $value->LEVEL;
                    $new_technical_details->STATUS = $value->STATUS;
                    $new_technical_details->DAILY_DAY_ID = $daily_day_id;
                    $new_technical_details->save();

                    if (!$new_technical_details) {
                        $is_success = false;
                        break;
                    } else {
                        $is_success = true;
                    }
                }
                $is_success = true;
            } else {
                $is_success = false;
            }
        } catch (Exception $e) {
            echo $e;
            $is_success = false;
        }

        return $is_success;
    }

    public function view_vcustomer() {
        $data["session"] = $this->session->userdata('logged_in');
        $data['content'] = 'admin/projects/view_vcustomer';

        $this->load->view('template', $data);
    }

    public function view_vcustomer_load() {
        $data["session"] = $this->session->userdata('logged_in');

        $grid = new EditableGrid();

        $grid->addColumn('ID_CUSTOMER', 'ID', 'html', NULL, false, 'ID_CUSTOMER');
        $grid->addColumn('COMPANY_NAME', 'Name', 'html', NULL, false);

        $active_vcustomer = $this->Customer_model;
        $totalUnfiltered = $active_vcustomer->count();
        $total = $totalUnfiltered;

        $page = 0;
        if (isset($_GET['page']) && is_numeric($_GET['page']))
            $page = (int) $_GET['page'];

        $rowByPage = 20;

        $from = ($page - 1) * $rowByPage;

        if (isset($_GET['filter']) && $_GET['filter'] != "") {
            $filter = $_GET['filter'];
            $active_vcustomer = $active_vcustomer
                    ->where('COMPANY_NAME', 'like', '%' . $filter . '%')
                    ->orWhere('ID_CUSTOMER', 'like', '%' . $filter . '%');
            $total = $active_vcustomer->count();
        }

        if (isset($_GET['sort']) && $_GET['sort'] != "" && in_array($_GET['sort'], $grid->getColumnFields())) {
            $$active_vcustomer = $active_vcustomer->orderBy($_GET['sort'], $_GET['asc'] == "0" ? "DESC" : "ASC");
        } else {
            $active_vcustomer = $active_vcustomer->orderBy('COMPANY_NAME', 'ASC');
        }

        $active_vcustomer = $active_vcustomer->skip($from)->take($rowByPage);
        $grid->setPaginator(ceil($total / $rowByPage), (int) $total, (int) $totalUnfiltered, null);

        $grid->renderJSON($active_vcustomer->get(), false, false, !isset($_GET['data_only']));
    }

    public function view_vproject() {
        $data["session"] = $this->session->userdata('logged_in');

        $response["status"] = '';
        $response["data"] = '';
        try {
            if ($this->uri->segment(2)) {
                $this->session->set_userdata('KUNNR', $this->uri->segment(2));
            } else {
                $this->session->unset_userdata('KUNNR');
            }
            $data['id_customer'] = $this->session->userdata('KUNNR');
            $this->session->unset_userdata('REVNR');
        } catch (Exception $e) {
            error_log($e);
        }        
        
        $menu_tab = $this->Management_model->getMenuTop($data['session']['group_id']);
        $menu_tab_config = $this->Management_model->getMenuTopConfig($data['session']['group_id'],$data['session']['id_user']);

        for($a=0;$a<count($menu_tab_config);$a++){
            for($b=0;$b<count($menu_tab);$b++){
                if(array_search($menu_tab_config[$a], $menu_tab[$b])){
                    $menu_tab[$b]['w'] = 1;
                    $b=9999;
                }else{
                    continue;
                }
            }
        }

        $this->session->set_userdata('menu_tab',$menu_tab);
        $data['content'] = 'admin/projects/view_vproject';

        $perPage = 10;
        $page = $this->input->post("page");
        $status = $this->input->post("status");
        $filter = $this->input->post("filter");
        $location = is_admin($data["session"]["user_group"]) ? $data["session"]["work_area_id"] : is_ppcplc($data["session"]["user_group"]) ? $data["session"]["work_area_id"]: $this->input->post("location");
        $customer = is_customer($data["session"]["user_group"]) ? $data["session"]["customer_id"] : $data['id_customer'];
        if (!empty($page)) {
            $projects = array();
            if (!empty($filter)) {
                $projects = $this->Project_model->get_projects_with_filter($page, $perPage, $status, $location, $filter, $customer);
            } else {
                $projects = $this->Project_model->get_projects($page, $perPage, $status, $location, $customer);
            }
            $response["status"] = "success";
            $response["data"] = $projects;
            echo json_encode($response);
        } else {
            $this->load->view('template', $data);
        }
    }

    public function view_vproject_load() {

        $data["session"] = $this->session->userdata('logged_in');

        $work_area_id = $this->session->userdata['logged_in']['work_area_id'];
        $customer_id = $this->session->userdata['logged_in']['customer_id'];
        $group_level = $this->session->userdata['logged_in']['group_level'];
        $GROUP_LINK = $group_level != 1 ? 'REVNR_CUSTOMER' : 'REVNR_LINK';

        $grid = new EditableGrid();

        $grid->addColumn('NAME1', 'Customer Name', 'string', NULL, false);
        $grid->addColumn('TPLNR', 'Aircraft Registered', 'string', NULL, false);
        $grid->addColumn($GROUP_LINK, 'Project Code', 'html', NULL, false);
        $grid->addColumn('REVTX', 'Project Name', 'string', NULL, false);
        $grid->addColumn('REVBD', 'Start Date', 'date', NULL, false, 'REVBD');
        $grid->addColumn('REVED', 'Finish Date', 'date', NULL, false, 'REVED');
        $grid->addColumn('VAWRK', 'Location', 'string', NULL, false);
        $grid->addColumn('TXT04', 'Status Project', 'string', NULL, false);
        $grid->addColumn('REVTY', 'Type Project', 'string', NULL, false);
//        $grid->addColumn('JC_PROGRESS', 'JC Progress (%)', 'string', NULL, false);
//        $grid->addColumn('MDR_PROGRESS', 'MDR Progress (%)', 'number', NULL, false);
        $grid->addColumn('action', 'Action', 'html', NULL, false, 'REVNR');

        // $active_job = $this->VProject_model->selectRaw('*,'
//                . "(((SELECT COUNT(*) FROM TB_V_JOBCARD_PROGRESS WHERE TB_V_JOBCARD_PROGRESS.REVNR = TB_V_PROJECT_LIST.REVNR AND TB_V_JOBCARD_PROGRESS.STATUS LIKE 'CLOSE%')/(CASE WHEN (SELECT COUNT(*) FROM TB_V_JOBCARD_PROGRESS WHERE TB_V_JOBCARD_PROGRESS.REVNR = TB_V_PROJECT_LIST.REVNR)=0 THEN 1 ELSE (SELECT COUNT(*) FROM TB_V_JOBCARD_PROGRESS WHERE TB_V_JOBCARD_PROGRESS.REVNR = TB_V_PROJECT_LIST.REVNR) END)*10000)/100) AS JC_PROGRESS,"
//                . "(((SELECT COUNT(*) FROM TB_V_MDR_PROGRESS WHERE TB_V_MDR_PROGRESS.REVNR = TB_V_PROJECT_LIST.REVNR AND TB_V_MDR_PROGRESS.STATUS LIKE 'CARRY OUT%')/(CASE WHEN (SELECT COUNT(*) FROM TB_V_MDR_PROGRESS WHERE TB_V_MDR_PROGRESS.REVNR = TB_V_MDR_PROGRESS.REVNR)=0 THEN 1 ELSE (SELECT COUNT(*) FROM TB_V_MDR_PROGRESS WHERE TB_V_MDR_PROGRESS.REVNR = TB_V_MDR_PROGRESS.REVNR) END)*10000)/100) AS MDR_PROGRESS,"

        $active_job = $this->Project_model->selectRaw('TPLNR, REVNR, REVTX, REVBD, REVED, VAWRK, TXT04, REVTY,'
                . ((($this->db->dbdriver == 'sqlsrv') or ( $this->db->dbdriver == 'mssql')) ? "'<a href=\"" . base_url() . "projects/crud_jobcard/' + REVNR + '\">' + REVNR + '</a>' AS REVNR_LINK," : "CONCAT('<a href=\"" . base_url() . "projects/crud_jobcard/', REVNR, '\">', REVNR, '</a>') AS REVNR_LINK,")
                . ((($this->db->dbdriver == 'sqlsrv') or ( $this->db->dbdriver == 'mssql')) ? "'<a href=\"" . base_url() . "projects/c_jobcard/' + REVNR + '\">' + REVNR + '</a>' AS REVNR_CUSTOMER" : "CONCAT('<a href=\"" . base_url() . "projects/c_jobcard/', REVNR, '\">', REVNR, '</a>') AS REVNR_CUSTOMER")
        );

        $active_job = $active_job
                ->where('TXT04', '!=', 'CLSD')
                ->whereIn('VAWRK', ['GAH1', 'GAH3', 'GAH4']);

        if ($this->session->userdata('KUNNR') != NULL) {
            $active_job = $active_job->where('KUNNR', $this->session->userdata('KUNNR'));
        }

        // if( ! empty($work_area_id) ) {
        //         $active_job = $active_job->where('VAWRK', $work_area_id);
        // }



        $totalUnfiltered = $active_job->count();
        $total = $totalUnfiltered;

        $page = 0;
        if (isset($_GET['page']) && is_numeric($_GET['page']))
            $page = (int) $_GET['page'];

        $rowByPage = 20;

        $from = ($page - 1) * $rowByPage;

        if (isset($_GET['filter']) && $_GET['filter'] != "") {
            $filter = $_GET['filter'];
            $active_job = $active_job
//                    ->where('NAME1', 'like', '%' . $filter . '%')
                    ->where('TPLNR', 'like', '%' . $filter . '%')
                    ->orwhere('REVNR', 'like', '%' . $filter . '%')
                    ->orwhere('REVTX', 'like', '%' . $filter . '%')
                    ->orwhere('REVBD', 'like', '%' . $filter . '%')
                    ->orwhere('REVED', 'like', '%' . $filter . '%')
                    ->orwhere('VAWRK', 'like', '%' . $filter . '%')
//                    ->orwhere('TXT30', 'like', '%' . $filter . '%')
                    ->orwhere('REVTY', 'like', '%' . $filter . '%');
            $total = $active_job->count();
        }

        if (isset($_GET['sort']) && $_GET['sort'] != "") {
            $active_job = $active_job->orderBy($_GET['sort'], $_GET['asc'] == "0" ? "DESC" : "ASC");
        } else {
            $active_job = $active_job->orderBy('REVNR', 'ASC');
        }

        $active_job = $active_job->skip($from)->take($rowByPage);
        $grid->setPaginator(ceil($total / $rowByPage), (int) $total, (int) $totalUnfiltered, null);
        $grid->renderJSON($active_job->get(), false, false, !isset($_GET['data_only']));
    }

    public function find_order_number_data() {
        $response["status"] = NULL;
        $response["body"] = NULL;

        if (isset($_POST['AUFNR'])) {
            try {
                $order_number = $this->Jobcard_model
                        ->where('REVNR', $this->session->userdata('REVNR'))
                        ->where('AUFNR', ($_POST['AUFNR']))
                        ->first();
                if ($order_number) {
                    $response["status"] = 'success';
                    $response["body"] = $order_number;
                }
            } catch (Exception $e) {
                error_log($e);
            }
        }

        header('Content-Type: application/json');
        echo json_encode($response);
    }

    public function find_order_number() {
        $response["status"] = NULL;
        $response["body"] = NULL;

        try {
            $order_number = $this->Jobcard_model
                    ->select("AUFNR", "AUART", "JC_REFF")
                    ->where('REVNR', $_GET['REVNR'])
                    ->where('AUFNR', 'like', ($_GET['q']) . "%")
                    ->distinct('AUFNR')
                    ->get();

            if ($order_number) {
                $response["status"] = 'success';
                $response["body"] = $order_number;
            }
        } catch (Exception $e) {
            error_log($e);
        }

        header('Content-Type: application/json');
        echo json_encode($response);
    }

    public function read_xlsx_file_jobcard() {
        $response["status"] = NULL;
        $response["body"] = NULL;
        try {
            $path = $_FILES['media']['tmp_name'];
            $reader = ReaderFactory::create(Type::XLSX);
            $reader->open($path);
            $i = 0;
            $data["error"] = array();
            $data["success"] = array();

            foreach ($reader->getSheetIterator() as $sheet) {
                foreach ($sheet->getRowIterator() as $row => $value ) {
                    if ($i == 0) {
                        $i++;
                        continue; 
                    }


                    $list = array(
                        'SKILL',
                        'AREA',
                        'PHASE',
                        'DAY',
                        'STATUS',
                        'DATECLOSE',
                        'REMARK',
                        'DOC_SENT_STATUS',
                        'FREETEXT',
                        'DATEPROGRESS',
                        'CABINSTATUS'
                    );
                    foreach ($value as $key2 => $value2) {
                        if ($key2 >= 3 and $key2 <= 13) {
                            $val = $value2 instanceof DateTime ?  $value2->format('Y-m-d ') : $value2;
                            $item[$list[$key2 - 3]] = (string) $val ?: null;
                        }
                    }
                    $jobcard = $this->Jobcard_model
                            ->where('REVNR',  $value[0])
                            ->where('AUFNR',  $value[1])
                            ->where('AUART', 'GA01')
                            ->update($item);
                    if ($jobcard) {
                        array_push($data["success"], $value[1]);

                        if (!empty($item['AREA'])) {
                            $mdr = $this->Mdr_model
                                ->where('JC_REFF', 'LIKE', '%' . $value[1])
                                ->update(["AREA" => $item['AREA']]);
                        }
                    
                    

                    } else {
                        array_push($data["error"], $value[1]);
                    }
                }
            }
            if (count($data["success"]) > 0) {
                $response["status"] = 'success';
                $response["body"] = $data;
            }

        } catch (Exception $e) {
            echo $e;
            exit;
        }
        
        // $test = array();
        // for($i = 0; $i < 10; $i++){
        //     array_push($test, $i);
        // }     

        header('Content-Type: application/json');
        echo json_encode($response);
    }

    public function read_xlsx_file_jobcard2() {

        $dataObject = json_decode($_POST['myData'],true);
        $response["status"] = NULL;
        $response["body"] = NULL;
        $data["error"] = array();
        $data["success"] = array();
        $list = array(
            null,
            'SKILL',
            'AREA',
            'PHASE',
            'DAY',
            'STATUS',
            'DATECLOSE',
            'REMARK',
            'DOCSENTSTATUS',
            'FREETEXT',
            'DATEPROGRESS',
            'CABINSTATUS'
        );
        
        
        foreach ($dataObject as $key => $value) {
            $array_key = array_keys($value);
            for($a=0;$a<count($array_key);$a++){
                $cek_array = array_search(str_replace(".", "", str_replace(" ", "", $array_key[$a])), $list);
                if($cek_array){
                    $val = $value[$array_key[$a]] instanceof DateTime ?  $value[$array_key[$a]]->format('Y-m-d ') : $value[$array_key[$a]];
                    $item[$list[$cek_array]] = (string) $val ?: null;
                }
            }
            $jobcard = $this->Jobcard_model
                    ->where('REVNR',  $value['REVISION'])
                    ->where('AUFNR',  $value['ORDER NUMBER'])
                    ->where('AUART', 'GA01')
                    ->update($item);
            if ($jobcard) {
                array_push($data["success"], $value['ORDER NUMBER']);

                if (!empty($item['AREA'])) {
                    $mdr = $this->Mdr_model
                        ->where('JC_REFF', 'LIKE', '%' . $value['ORDER NUMBER'])
                        ->update(["AREA" => $item['AREA']]);
                }
            
            

            } else {
                array_push($data["error"], $value['ORDER NUMBER']);
            }
            $item = array();
        }
        if (count($data["success"]) > 0) {
            $response["status"] = 'success';
            $response["body"] = $data;
        }
        header('Content-Type: application/json');
        echo json_encode($response);
    }

    public function read_xlsx_file_jobcard3() {
        $current_exec = 0;
        $last_exec = 0;
        $response["status"] = NULL;
        $response["body"] = NULL;
        $response["current_error"] = 0;
        $response["exec_per_row"] = [];

        try{
            $dataObject = json_decode($_POST['myData'], true);
            $data = array();

            $this->benchmark->mark('code_start');

            foreach ($dataObject as $key => $value) {
                $last_exec = $value['AUFNR'];
                $response["current_error"] = $key;

                $this->benchmark->mark('row_start');
                $jobcard = $this->Jobcard_model
                ->where('REVNR',  $value['REVNR'])
                ->where('AUFNR',  $value['AUFNR'])
                ->where('AUART', 'GA01')
                ->update($value);
                if ($jobcard) {
                    array_push($data, array("ORDER" => $value['AUFNR'], "STATUS" => 1));

                    if (!empty($value['AREA'])) {
                        $mdr = $this->Mdr_model
                            ->where('JC_REFF', 'LIKE', '%' . $value['AUFNR'])
                            ->update(["AREA" => $value['AREA']]);
                    }           
                

                } else {
                    array_push($data, array("ORDER" => $value['AUFNR'], "STATUS" => 0));
                }
                $this->benchmark->mark('row_end');
                array_push($response["exec_per_row"], $this->benchmark->elapsed_time('row_start', 'row_end'));

            }


            $this->benchmark->mark('code_end');

            
            if (count($data) > 0) {
                $response["status"] = "success";
                $response["body"] = NULL;
                $response["result"] = $data;
                $response["exec_up"] = $this->benchmark->elapsed_time('code_start', 'code_end');
            }else{
                $response["status"] = "error";
                $response["body"] = $data;
                $response["result"] = $data;
            }

        }catch(Exception $e){
            array_push($data, array("ORDER" => $last_exec, "STATUS" => 0));
            $response["status"] = "failed";
            $response["body"] = $e->getMessage();
            $response["result"] = $data;
        }

        header('Content-Type: application/json');
        echo json_encode($response);
    }

    public function import_xlsx_file_mrm($id_project) {
        $response["status"] = NULL;
        $response["body"] = NULL;
        
        try {
            $path = $_FILES['media']['tmp_name'];
            $arr_file = explode('.', $path);
            $extension = end($arr_file);

            $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
            $spreadsheet = $reader->load($path);
            $sheet_data = $spreadsheet->getActiveSheet()->toArray();
            unset($sheet_data[0]);

            $list = array(
                'MTART',
                'IPC',
                'STO_NUMBER',
                'OUTBOND_DELIV',
                'TO_NUM',
                'MENGE',
                'MEINS',
                'STOCK_MANUAL',
                'LGORT',
                'MATERIAL_FULFILLMENT_STATUS',
                'FULLFILLMENT_REMARK',
                'PO_DATE',
                'PO_NUM',
                'AWB_NUMBER',
                'AWB_DATE',
                'QTY_DELIVERED',
                'QTY_REMAIN'
            );

            $index_col = [7,8,9,10,11,14,16,17,18,19,21,22,23,24,25,26,27];

            $data = array();

            for ($i = 1; $i <= count($sheet_data); $i++) { 
                $k = 0;
                $temp = array();
                for ($j = 0; $j < count($sheet_data[$i]); $j++) { 
                    if (in_array($j, $index_col)) {
                        // $temp[$list[$k]] = $sheet_data[$i][$j] == null ? '' : $sheet_data[$i][$j];
                        $temp[$list[$k]] = $sheet_data[$i][$j] instanceof DateTime ?  $sheet_data[$i][$j]->format('Y-m-d') : $sheet_data[$i][$j];
                        $k++;
                    }
                }
                
                $this->Mrm_model
                    ->where('REVNR', $id_project)
                    ->where('AUFNR', $sheet_data[$i][1])
                    ->where('MATNR', $sheet_data[$i][3])
                    ->update($temp);
            }

            $response["status"] = 'success';
            $response["body"] = 'tes';
        } catch (Exception $e) {
            $response["status"] = 'failed';
            $response["body"] = $e;
            exit;
        }        
        echo json_encode($response);
    }

    public function read_xlsx_file_mrm() {
        $current_exec = 0;
        $last_exec = 0;
        $response["status"] = NULL;
        $response["body"] = NULL;
        $response["current_error"] = 0;
        $response["exec_per_row"] = [];

        try{
            $dataObject = json_decode($_POST['myData'], true);
            $data = array();

            $this->benchmark->mark('code_start');

            foreach ($dataObject as $key => $value) {
                $last_exec = $value['SEQ_NUM'];
                $response["current_error"] = $key;

                $this->benchmark->mark('row_start');
                $mrm = $this->Mrm_model
                    ->where('REVNR',  $value['REVNR'])
                    ->where('SEQ_NUM',  $value['SEQ_NUM'])
                    ->where('NO_',  $value['NO_'])
                    ->where('IS_ACTIVE', 1)
                    ->update($value);
                if ($mrm) {
                    array_push($data, array("ORDER" => $value['SEQ_NUM'], "STATUS" => 1));

                    if (!empty($value['MATERIAL_FULFILLMENT_STATUS'])) {
                        $this->setOrderStatus($value['SEQ_NUM'], $value['REVNR']);
                    }           
                

                } else {
                    array_push($data, array("ORDER" => $value['SEQ_NUM'], "STATUS" => 0));
                }
                $this->benchmark->mark('row_end');
                array_push($response["exec_per_row"], $this->benchmark->elapsed_time('row_start', 'row_end'));

            }


            $this->benchmark->mark('code_end');

            
            if (count($data) > 0) {
                $response["status"] = "success";
                $response["body"] = NULL;
                $response["result"] = $data;
                $response["exec_up"] = $this->benchmark->elapsed_time('code_start', 'code_end');
            }else{
                $response["status"] = "error";
                $response["body"] = $data;
                $response["result"] = $data;
            }

        }catch(Exception $e){
            array_push($data, array("ORDER" => $last_exec, "STATUS" => 0));
            $response["status"] = "failed";
            $response["body"] = $e->getMessage();
            $response["result"] = $data;
        }

        header('Content-Type: application/json');
        echo json_encode($response);
    }

    public function write_xlsx_file_jobcard(){
        $REVNR = $_GET['REVNR'];

        $jobcard = $this->Proc_model->get_jc($REVNR);
        $jobcard_list = array();

        foreach($jobcard as $value){
            if(!$this->is_customer){
                array_push(
                    $jobcard_list, 
                    array(
                        $value->REVNR, 
                        $value->AUFNR, 
                        $value->KTEXT,
                        $value->SKILL,
                        $value->AREA,
                        $value->PHASE,
                        $value->DAY,
                        $value->STATUS,
                        $value->DATECLOSE,
                        $value->REMARK,
                        $value->DOC_SENT_STATUS,
                        $value->FREETEXT,
                        $value->DATEPROGRESS,
                        $value->CABINSTATUS,
                        $value->STATUS_SAP,
                    )
                );
            }else{
                array_push(
                    $jobcard_list, 
                    array(
                        $value->SEQ_NUM, 
                        $value->AUFNR,
                        $value->CUST_JC_NUM,
                        $value->KTEXT,
                        $value->STATUS,
                    )
                );
            }
        }

        $writer = WriterFactory::create(Type::XLSX);
        $default_style = (new StyleBuilder())
                ->setFontName('Arial')
                ->setFontSize(10)
                ->setShouldWrapText(false)
                ->build();          
               
        
        $writer->setDefaultRowStyle($default_style);
        

        $file_name = "JOBCARD_" . $REVNR . "_" . date('YmdhIs') . ".xlsx";
        $writer->openToFile($file_name); 

        $data_header = array();
        if(!$this->is_customer){
            $data_header[] = "REVISION";
            $data_header[] = "ORDER NUMBER";
            $data_header[] = "DESCRIPTION";
            $data_header[] = "SKILL";
            $data_header[] = "AREA";
            $data_header[] = "PHASE";
            $data_header[] = "DAY";
            $data_header[] = "STATUS";
            $data_header[] = "DATE CLOSE";
            $data_header[] = "REMARK";
            $data_header[] = "DOC. SENT STATUS";
            $data_header[] = "FREE TEXT";
            $data_header[] = "DATE PROGRESS";
            $data_header[] = "CABIN STATUS";
            $data_header[] = "STATUS SAP";
        }else{
            $data_header[] = "Seq";
            $data_header[] = "Order";
            $data_header[] = "Cust JC Num";
            $data_header[] = "Discrepancies";
            $data_header[] = "Status";
        }
        

        $writer->addRow($data_header);
        if(!$this->is_customer){
            $query_header = array();
            $query_header[] = "REVNR";
            $query_header[] = "AUFNR";
            $query_header[] = "KTEXT";
            $query_header[] = "SKILL";
            $query_header[] = "AREA";
            $query_header[] = "PHASE";
            $query_header[] = "DAY";
            $query_header[] = "STATUS";
            $query_header[] = "DATECLOSE";
            $query_header[] = "REMARK";
            $query_header[] = "DOC_SENT_STATUS";
            $query_header[] = "FREETEXT";
            $query_header[] = "DATEPROGRESS";
            $query_header[] = "CABINSTATUS";
            $query_header[] = "STATUS_SAP";
            $writer->addRow($query_header);
        }


        $writer->addRows($jobcard_list);
        $writer->close();

        $this->load->helper('download');
        force_download($file_name, null);
        
    }

    public function crud_jobcard() {
        try {
            $data['id_project'] = $this->uri->segment(3) ? $this->uri->segment(3) : '0';
            $active_prj = $this->Project_model_ci->getProjectName($data['id_project']);
            $data['project_name'] = $active_prj ? $active_prj->REVTX : "";  
        } catch (Exception $e) {
            error_log($e);
        }

        try {
            if ($this->uri->segment(4)) {
                $this->session->set_userdata('jc_id', $this->uri->segment(4));
            } else {
                $this->session->unset_userdata('jc_id');
            }
            $data['jc_id'] = $this->session->userdata('jc_id');
        } catch (Exception $e) {
            error_log($e);
        }
        // $this->Proc_model->merge_pmorder($this->session->userdata('REVNR'));
        $data['write'] = $this->is_write;
        $data["session"] = $this->session->userdata('logged_in');
        $data['content'] = 'admin/projects/crud_jobcard';
        $this->load->view('template', $data);
    }

        public function crud_jobcard_new() {
        try {
            $data['id_project'] = $this->uri->segment(3) ? $this->uri->segment(3) : '0';
            $active_prj = $this->Project_model_ci->getProjectName($data['id_project']);
            $data['project_name'] = $active_prj ? $active_prj->REVTX : "";  
        } catch (Exception $e) {
            error_log($e);
        }

        try {
            if ($this->uri->segment(4)) {
                $this->session->set_userdata('jc_id', $this->uri->segment(4));
            } else {
                $this->session->unset_userdata('jc_id');
            }
            $data['jc_id'] = $this->session->userdata('jc_id');
        } catch (Exception $e) {
            error_log($e);
        }
        // $this->Proc_model->merge_pmorder($this->session->userdata('REVNR'));
        $data['write'] = $this->is_write;
        $data["session"] = $this->session->userdata('logged_in');
        $data['content'] = 'admin/projects/crud_jobcard_new';
        $this->load->view('template', $data);
    }

    public function crud_jobcard_load() {
        $data["session"] = $this->session->userdata('logged_in');
        $phase = $this->Phase_model->get();
        $phase_arr = array();

        foreach ($phase as $value) {
            $phase_arr[$value->ID] = $value->PHASE;
        }

        $grid = new EditableGrid();

        $grid->addColumn('SEQ_NUM', 'SEQ', 'int', NULL, false);
        // $grid->addColumn('AUFNR_LINK', 'Order', 'html', NULL, false);
        $grid->addColumn('AUFNR', 'Order', 'html', NULL, false, 'AUFNR');
        $grid->addColumn('KTEXT', 'Description', 'string', NULL, false);
        $grid->addColumn('CUST_JC_NUM', 'Cust JC Num', 'string', NULL, false);
        $grid->addColumn('ILART', 'Task', 'string', NULL, false);
        $grid->addColumn('ITVAL', 'ITVAL', 'string');
        $grid->addColumn('MHRS', 'Mhrs Plan', 'string', NULL, false);
        $grid->addColumn('SKILL', 'Skill', 'string', array(""=>"","A/P" => "A/P", "CBN" => "CBN", "E/A" => "E/A", "STR" => "STR", "WS" => "WS"));
        $grid->addColumn('AREA', 'Area', 'string', array(""=>"","FUSELAGE" => "FUSELAGE", "COCKPIT" => "COCKPIT", "LH-WING" => "LH-WING", "RH-WING" => "RH-WING", "L/G" => "L/G", "ENG#1" => "ENG#1", "ENG#2" => "ENG#2", "ENG#3" => "ENG#3", "ENG#4" => "ENG#4", "TAIL" => "TAIL", "CABIN" => "CABIN", "FWD CARGO" => "FWD CARGO", "AFT CARGO" => "AFT CARGO", "BULK CARGO" => "BULK CARGO", "MAIN CARGO" => "MAIN CARGO", "LOW CARGO" => "LOW CARGO", "ELECT" => "ELECT", "GENERAL AREA" => "GENERAL AREA"));
        $grid->addColumn('PHASE', 'Phase', 'string', $phase_arr);
        $grid->addColumn('DAY', 'Day', 'string');
        $grid->addColumn('DATECLOSE', 'Date Closed', 'date');
        $grid->addColumn('STATUS', 'Status', 'string', array(""=>"","OPEN" => "OPEN", "CLOSED" => "CLOSED", "PERFORM BY PROD" => "PERFORM BY PROD", "PERFORM TO SHOP" => "PERFORM TO SHOP", "WAITING MATERIAL" => "WAITING MATERIAL", "WAITING TOOL" => "WAITING TOOL", "PREPARE FOR INSTALL" => "PREPARE FOR INSTALL", "PREPARE FOR TEST" => "PREPARE FOR TEST", "PREPARE FOR RUN UP" => "PREPARE FOR RUN UP", "PREPARE FOR NDT" => "PREPARE FOR NDT", "PART AVAIL" => "PART AVAIL"));
        $grid->addColumn('JC_REFF_MDR_VAL', 'JC Reff MDR', 'string', NULL, false);
        $grid->addColumn('REMARK', 'Remark', 'string', array(""=>"","N/A (NOT APPLICABLE)" => "N/A (NOT APPLICABLE)", "WITHDRAWN" => "WITHDRAWN", "COVER BY ANOTHER JOBCARD" => "COVER BY ANOTHER JOBCARD", "INTERUPT" => "INTERUPT", "MAP SHORTAGE" => "MAP SHORTAGE", "MAP GADC" => "MAP GADC", "MAP W. CUST. SUPPLY" => "MAP W. CUST. SUPPLY", "PART COMPLETE" => "PART COMPLETE", "PART PARTIAL" => "PART PARTIAL", "ANOTHER REASON" => "ANOTHER REASON"));
        $grid->addColumn('DOC_SENT_STATUS', 'Doc. Sent Status', 'string', array(""=>"","SENT TO CABIN" => "SENT TO CABIN", "SENT TO CABIN SHOP" => "SENT TO CABIN SHOP", "SENT TO PAINTING" => "SENT TO PAINTING", "SENT TO NDT" => "SENT TO NDT", "SENT TO TBR SHOP" => "SENT TO TBR SHOP", "SENT TO SEALANT" => "SENT TO SEALANT", "SENT TO STR HANGAR" => "SENT TO STR HANGAR", "SENT TO SEAT" => "SENT TO SEAT", "SENT TO WHEEL SHOP" => "SENT TO WHEEL SHOP", "SENT TO TV ENGINE" => "SENT TO TV ENGINE", "SENT TO CLEANING" => "SENT TO CLEANING"));

        $grid->addColumn('FREETEXT', 'Free Text', 'html');
        $grid->addColumn('DATEPROGRESS', 'Date Progress', 'date');
        $grid->addColumn('MATERIAL_STATUS', 'Mat Status', 'string', NULL, false);
        $grid->addColumn('CABINSTATUS', 'Cabin Status', 'string');
        $grid->addColumn('STATUS_SAP', 'Status SAP', 'string', NULL, false);
        $grid->addColumn('action', 'Action', 'html', NULL, false, 'id');


        $active_job = $this->Jobcard_model
                ->selectRaw('*,'
                        . ((($this->db->dbdriver == 'sqlsrv') or ( $this->db->dbdriver == 'mssql')) ? "'<a href=\"" . base_url() . "projects/crud_mdr/' + REVNR + '/' + AUFNR + '/JC\">' + AUFNR + '</a>' AS AUFNR_LINK," : "CONCAT('<a href=\"" . base_url() . "admin/projects/crud_mdr/', REVNR, '/', AUFNR, '/JC\">', AUFNR, '</a>') AS AUFNR_LINK,")
                        . '(select count(*) from TB_M_PMORDER where TB_M_PMORDER.MAUFNR = TB_M_PMORDER.AUFNR) AS JC_REFF_MDR_VAL')
                ->where('REVNR', $this->session->userdata('REVNR'))
                ->where('AUART', 'GA01')
                ->orderBy('AUFNR', 'ASC');


        if ($this->session->userdata('jc_id') != NULL) {
            $active_job = $active_job->where('AUFNR', $this->session->userdata('jc_id'));
        }

        $totalUnfiltered = $active_job->count();
        $total = $totalUnfiltered;

        $page = 0;
        if (isset($_GET['page']) && is_numeric($_GET['page']))
            $page = (int) $_GET['page'];


        $rowByPage = $totalUnfiltered; // 50;

        $from = ($page - 1) * $rowByPage;

        if (isset($_GET['filter']) && $_GET['filter'] != "") {
            $filter = $_GET['filter'];
            $active_job = $active_job
                    ->where('AUFNR', 'like', '%' . $filter . '%')
                    ->orwhere('KTEXT', 'like', '%' . $filter . '%')
                    ->orwhere('CUST_JC_NUM', 'like', '%' . $filter . '%')
                    ->orwhere('AUART', 'like', '%' . $filter . '%')
                    ->orwhere('ITVAL', 'like', '%' . $filter . '%')
                    ->orwhere('ARBEI', 'like', '%' . $filter . '%')
                    ->orwhere('ILART', 'like', '%' . $filter . '%')
                    ->orwhere('AREA', 'like', '%' . $filter . '%')
                    ->orwhere('PHASE', 'like', '%' . $filter . '%')
                    ->orwhere('DAY', 'like', '%' . $filter . '%')
                    ->orwhere('STATUS', 'like', '%' . $filter . '%')
                    ->orwhere('DATECLOSE', 'like', '%' . $filter . '%')
                    ->orwhere('REMARK', 'like', '%' . $filter . '%')
                    ->orwhere('FREETEXT', 'like', '%' . $filter . '%')
                    ->orwhere('DATEPROGRESS', 'like', '%' . $filter . '%');
            $total = $active_job->count();
        }

        if (isset($_GET['sort']) && $_GET['sort'] != "") {
            $active_job = $active_job->orderBy($_GET['sort'], $_GET['asc'] == "0" ? "DESC" : "ASC");
        } else {
            $active_job = $active_job->orderBy('SEQ_NUM', 'ASC');
        }

        $active_job = $active_job->skip($from)->take($rowByPage);
        $grid->setPaginator(ceil($total / $rowByPage), (int) $total, (int) $totalUnfiltered, null);
        $grid->renderJSON($active_job->get(), false, false, !isset($_GET['data_only']));
    }

    public function crud_jobcard_load_all() {
        $data["session"] = $this->session->userdata('logged_in');
        $REVNR = $this->input->get('REVNR');
        $is_edit = $_GET['edit'] == 1 ? true : false;
        $phase = $this->Phase_model->get();
        $phase_arr = array();
        $area = array(
            "Cockpit" => "Cockpit",
            "Fuselage" => "Fuselage",
            "LH-Wing" => "LH-Wing",
            "RH-Wing" => "RH-Wing",
            "L/G" => "L/G",
            "ENG#1" => "ENG#1",
            "ENG#2" => "ENG#2",
            "ENG#3" => "ENG#3",
            "ENG#4" => "ENG#4",
            "Tail" => "Tail",
            "Cabin" => "Cabin",
            "FWD Cargo" => "FWD Cargo",
            "AFT Cargo" => "AFT Cargo",
            "Bulk Cargo" => "Bulk Cargo",
            "Main Cargo" => "Main Cargo",
            "Elect" => "Elect",
            "General Area" => "General Area"
        );


        foreach ($phase as $value) {
            $phase_arr[$value->ID] = $value->PHASE;
        }

        $grid = new EditableGrid();

        if(!$this->is_customer){            
            $grid->addColumn('SEQ_NUM', 'SEQ', 'int', NULL, false);
            $grid->addColumn('AUFNR_LINK', 'Order', 'html', NULL, false);
            $grid->addColumn('KTEXT_LINK', 'Description', 'html', NULL, false);
            $grid->addColumn('CUST_JC_NUM', 'Cust JC Num', 'string', NULL, false);
            $grid->addColumn('ILART', 'Task', 'string', NULL, false);
            $grid->addColumn('ITVAL', 'ITVAL', 'string', NULL, $is_edit);
            $grid->addColumn('MHRS', 'Mhrs Plan', 'string', NULL, false);
            $grid->addColumn('SKILL', 'Skill', 'string', array(""=> "","A/P" => "A/P", "CBN" => "CBN", "E/A" => "E/A", "STR" => "STR", "WS" => "WS"), $is_edit);
            $grid->addColumn('AREA', 'Area', 'string', $area, $is_edit);
            $grid->addColumn('PHASE', 'Phase', 'string', $phase_arr, $is_edit);
            $grid->addColumn('DAY', 'Day', 'string', NULL, $is_edit);
            $grid->addColumn('STATUS', 'Status', 'string', array(""=>"","OPEN" => "OPEN", "CLOSED" => "CLOSED", "PERFORM BY PROD" => "PERFORM BY PROD", "PERFORM TO SHOP" => "PERFORM TO SHOP", "WAITING MATERIAL" => "WAITING MATERIAL", "WAITING TOOL" => "WAITING TOOL", "PREPARE FOR INSTALL" => "PREPARE FOR INSTALL", "PREPARE FOR TEST" => "PREPARE FOR TEST", "PREPARE FOR RUN UP" => "PREPARE FOR RUN UP", "PREPARE FOR NDT" => "PREPARE FOR NDT", "PART AVAIL" => "PART AVAIL"), $is_edit);
            $grid->addColumn('DATECLOSE', 'Date Closed', 'date', NULL, $is_edit);
            $grid->addColumn('JC_REFF_MDR_VAL', 'MDR Issued', 'html', NULL, false);
            // $grid->addColumn('JC_REFF_MDR_VAL', 'MDR Issued', 'string', NULL, false);
            $grid->addColumn('REMARK', 'Remark', 'string', array(""=>"","N/A (NOT APPLICABLE)" => "N/A (NOT APPLICABLE)", "WITHDRAWN" => "WITHDRAWN", "COVER BY ANOTHER JOBCARD" => "COVER BY ANOTHER JOBCARD", "INTERUPT" => "INTERUPT", "MAP SHORTAGE" => "MAP SHORTAGE", "MAP GADC" => "MAP GADC", "MAP W. CUST. SUPPLY" => "MAP W. CUST. SUPPLY", "PART COMPLETE" => "PART COMPLETE", "PART PARTIAL" => "PART PARTIAL", "ANOTHER REASON" => "ANOTHER REASON"), $is_edit);
            $grid->addColumn('DOC_SENT_STATUS', 'Doc. Sent Status', 'string', array(""=>"","SENT TO CABIN" => "SENT TO CABIN", "SENT TO CABIN SHOP" => "SENT TO CABIN SHOP", "SENT TO PAINTING" => "SENT TO PAINTING", "SENT TO NDT" => "SENT TO NDT", "SENT TO TBR SHOP" => "SENT TO TBR SHOP", "SENT TO SEALANT" => "SENT TO SEALANT", "SENT TO STR HANGAR" => "SENT TO STR HANGAR", "SENT TO SEAT" => "SENT TO SEAT", "SENT TO WHEEL SHOP" => "SENT TO WHEEL SHOP", "SENT TO TV ENGINE" => "SENT TO TV ENGINE", "SENT TO CLEANING" => "SENT TO CLEANING"), $is_edit);
    
            $grid->addColumn('FREETEXT', 'Free Text', 'html', NULL, $is_edit);
            $grid->addColumn('DATEPROGRESS', 'Date Progress', 'date', NULL, $is_edit);
            $grid->addColumn('MAT_FULLFILLMENT_STATUS', 'Material Status', 'string', NULL, false);
            $grid->addColumn('CABINSTATUS', 'Cabin Status', 'string', array(
                                    ""=>"",
                                    "Open" => "Open", 
                                    "Waiting RO"=>"Waiting RO", 
                                    "Waiting Material" => "Waiting Material",
                                    "Progress in Hangar" => "Progress in Hangar",
                                    "Progress in W101" => "Progress in W101",
                                    "Progress in W102" => "Progress in W102",
                                    "Progress in W103" => "Progress in W103",
                                    "Progress in W401" => "Progress in W401",
                                    "Progress in W402" => "Progress in W402",
                                    "Progress in W501" => "Progress in W501",
                                    "Progress in W502" => "Progress in W502",
                                    "Close" => "Close" 
                                ), $is_edit);
            $grid->addColumn('STATUS_SAP', 'Status SAP', 'string', NULL, false);

            if($data['session']['user_group_level'] == 1 || $data['session']['user_group_level'] == 2){
                if ($is_edit) {
                    $grid->addColumn('AUFNR_DEL', 'Action', 'html', NULL, false);
                }
            }
        } else {
            $grid->addColumn('SEQ_NUM', 'Seq', 'integer', NULL, false);
            $grid->addColumn('AUFNR_LINK', 'Order', 'html', NULL, false);
            $grid->addColumn('CUST_JC_NUM', 'Cust JC Num', 'string', NULL, false);
            $grid->addColumn('KTEXT_LINK', 'Discrepancies', 'html', NULL, false);
            $grid->addColumn('STATUS', 'Status', 'string', NULL, false);
        }


//        $sql = "EXEC TB_P_JOBCARD_PROGRESS @REVNR = " . $this->session->userdata('REVNR');
        
        $ORDER = 'SEQ_NUM';
        $AUFNR = 0;
        $FILTER = '';

        if (isset($_GET['sort']) && $_GET['sort'] != "") {
            $ORDER = $_GET['sort'] . ($_GET['asc'] == "0" ? " DESC" : " ASC");
        }
        
        if ($this->session->userdata('jc_id')) {
            $AUFNR = $this->session->userdata('jc_id');

        }

        if (isset($_GET['filter']) && $_GET['filter'] != "") {
            $FILTER = $_GET['filter'];
        }
        
        // $active_job = $this->Proc_model->get_jc($this->session->userdata('REVNR'), $ORDER, $AUFNR, $FILTER);
        // $grid->renderJSON($active_job, false, false, true);

    //    if ($this->Proc_model->merge_pmorder($REVNR)) {
            $active_job = $this->Proc_model->get_jc($REVNR, $ORDER, $AUFNR, $FILTER);           
    //    }

       $grid->renderJSON($active_job, false, false, true);
    }

    public function crud_jobcard_load_all_new() {
        $data["session"] = $this->session->userdata('logged_in');
        $REVNR = $this->input->get('REVNR');
        $is_edit = $_GET['edit'] == 1 ? true : false;
        $phase = $this->Phase_model->get();
        $phase_arr = array();
        $area = array(
            "Cockpit" => "Cockpit",
            "Fuselage" => "Fuselage",
            "LH-Wing" => "LH-Wing",
            "RH-Wing" => "RH-Wing",
            "L/G" => "L/G",
            "ENG#1" => "ENG#1",
            "ENG#2" => "ENG#2",
            "ENG#3" => "ENG#3",
            "ENG#4" => "ENG#4",
            "Tail" => "Tail",
            "Cabin" => "Cabin",
            "FWD Cargo" => "FWD Cargo",
            "AFT Cargo" => "AFT Cargo",
            "Bulk Cargo" => "Bulk Cargo",
            "Main Cargo" => "Main Cargo",
            "Elect" => "Elect",
            "General Area" => "General Area"
        );


        foreach ($phase as $value) {
            $phase_arr[$value->ID] = $value->PHASE;
        }

        $grid = new EditableGrid();

        if(!$this->is_customer){            
            $grid->addColumn('SEQ_NUM', 'SEQ', 'int', NULL, false);
            $grid->addColumn('AUFNR_LINK', 'Order', 'html', NULL, false);
            $grid->addColumn('KTEXT_LINK', 'Description', 'html', NULL, false);
            $grid->addColumn('CUST_JC_NUM', 'Cust JC Num', 'string', NULL, false);
            $grid->addColumn('ILART', 'Task', 'string', NULL, false);
            $grid->addColumn('ITVAL', 'ITVAL', 'string', NULL, $is_edit);
            $grid->addColumn('MHRS', 'Mhrs Plan', 'string', NULL, false);
            $grid->addColumn('SKILL', 'Skill', 'string', array(""=> "","A/P" => "A/P", "CBN" => "CBN", "E/A" => "E/A", "STR" => "STR", "WS" => "WS"), $is_edit);
            $grid->addColumn('AREA', 'Area', 'string', $area, $is_edit);
            $grid->addColumn('PHASE', 'Phase', 'string', $phase_arr, $is_edit);
            $grid->addColumn('DAY', 'Day', 'string', NULL, $is_edit);
            $grid->addColumn('STATUS', 'Status', 'string', array(""=>"","OPEN" => "OPEN", "CLOSED" => "CLOSED", "PERFORM BY PROD" => "PERFORM BY PROD", "PERFORM TO SHOP" => "PERFORM TO SHOP", "WAITING MATERIAL" => "WAITING MATERIAL", "WAITING TOOL" => "WAITING TOOL", "PREPARE FOR INSTALL" => "PREPARE FOR INSTALL", "PREPARE FOR TEST" => "PREPARE FOR TEST", "PREPARE FOR RUN UP" => "PREPARE FOR RUN UP", "PREPARE FOR NDT" => "PREPARE FOR NDT", "PART AVAIL" => "PART AVAIL"), $is_edit);
            $grid->addColumn('DATECLOSE', 'Date Closed', 'date', NULL, $is_edit);
            $grid->addColumn('JC_REFF_MDR_VAL', 'MDR Issued', 'html', NULL, false);
            // $grid->addColumn('JC_REFF_MDR_VAL', 'MDR Issued', 'string', NULL, false);
            $grid->addColumn('REMARK', 'Remark', 'string', array(""=>"","N/A (NOT APPLICABLE)" => "N/A (NOT APPLICABLE)", "WITHDRAWN" => "WITHDRAWN", "COVER BY ANOTHER JOBCARD" => "COVER BY ANOTHER JOBCARD", "INTERUPT" => "INTERUPT", "MAP SHORTAGE" => "MAP SHORTAGE", "MAP GADC" => "MAP GADC", "MAP W. CUST. SUPPLY" => "MAP W. CUST. SUPPLY", "PART COMPLETE" => "PART COMPLETE", "PART PARTIAL" => "PART PARTIAL", "ANOTHER REASON" => "ANOTHER REASON"), $is_edit);
            $grid->addColumn('DOC_SENT_STATUS', 'Doc. Sent Status', 'string', array(""=>"","SENT TO CABIN" => "SENT TO CABIN", "SENT TO CABIN SHOP" => "SENT TO CABIN SHOP", "SENT TO PAINTING" => "SENT TO PAINTING", "SENT TO NDT" => "SENT TO NDT", "SENT TO TBR SHOP" => "SENT TO TBR SHOP", "SENT TO SEALANT" => "SENT TO SEALANT", "SENT TO STR HANGAR" => "SENT TO STR HANGAR", "SENT TO SEAT" => "SENT TO SEAT", "SENT TO WHEEL SHOP" => "SENT TO WHEEL SHOP", "SENT TO TV ENGINE" => "SENT TO TV ENGINE", "SENT TO CLEANING" => "SENT TO CLEANING"), $is_edit);
    
            $grid->addColumn('FREETEXT', 'Free Text', 'html', NULL, $is_edit);
            $grid->addColumn('DATEPROGRESS', 'Date Progress', 'date', NULL, $is_edit);
            $grid->addColumn('MAT_FULLFILLMENT_STATUS', 'Material Status', 'string', NULL, false);
            $grid->addColumn('CABINSTATUS', 'Cabin Status', 'string', array(
                                    ""=>"",
                                    "Open" => "Open", 
                                    "Waiting RO"=>"Waiting RO", 
                                    "Waiting Material" => "Waiting Material",
                                    "Progress in Hangar" => "Progress in Hangar",
                                    "Progress in W101" => "Progress in W101",
                                    "Progress in W102" => "Progress in W102",
                                    "Progress in W103" => "Progress in W103",
                                    "Progress in W401" => "Progress in W401",
                                    "Progress in W402" => "Progress in W402",
                                    "Progress in W501" => "Progress in W501",
                                    "Progress in W502" => "Progress in W502",
                                    "Close" => "Close" 
                                ), $is_edit);
            $grid->addColumn('STATUS_SAP', 'Status SAP', 'string', NULL, false);

            if($data['session']['user_group_level'] == 1 || $data['session']['user_group_level'] == 2){
                if ($is_edit) {
                    $grid->addColumn('AUFNR_DEL', 'Action', 'html', NULL, false);
                }
            }
        } else {
            $grid->addColumn('SEQ_NUM', 'Seq', 'integer', NULL, false);
            $grid->addColumn('AUFNR_LINK', 'Order', 'html', NULL, false);
            $grid->addColumn('CUST_JC_NUM', 'Cust JC Num', 'string', NULL, false);
            $grid->addColumn('KTEXT_LINK', 'Discrepancies', 'html', NULL, false);
            $grid->addColumn('STATUS', 'Status', 'string', NULL, false);
        }


//        $sql = "EXEC TB_P_JOBCARD_PROGRESS @REVNR = " . $this->session->userdata('REVNR');
        
        $ORDER = 'SEQ_NUM';
        $AUFNR = 0;
        $FILTER = '';

        if (isset($_GET['sort']) && $_GET['sort'] != "") {
            $ORDER = $_GET['sort'] . ($_GET['asc'] == "0" ? " DESC" : " ASC");
        }
        
        if ($this->session->userdata('jc_id')) {
            $AUFNR = $this->session->userdata('jc_id');

        }

        if (isset($_GET['filter']) && $_GET['filter'] != "") {
            $FILTER = $_GET['filter'];
        }
        
        // $active_job = $this->Proc_model->get_jc($this->session->userdata('REVNR'), $ORDER, $AUFNR, $FILTER);
        // $grid->renderJSON($active_job, false, false, true);

    //    if ($this->Proc_model->merge_pmorder($REVNR)) {
        
            $active_job = $this->Proc_model->get_jc($REVNR, $ORDER, $AUFNR, $FILTER,true);          
            // print_r($active_job);die; 
    //    }

       $grid->renderJSON($active_job, false, false, true);
    }

    function crud_jobcard_after_insert($post_array, $primary_key) {
        $jobcard_update = array(
            "id_project" => $this->session->userdata('id_project')
        );
        $this->db->update('jobcard', $jobcard_update, array('id' => $primary_key));
        return true;
    }

    function crud_jobcard_seq_add_field_callback() {
        error_reporting(E_ALL & ~E_NOTICE);
        $last_seq = 0;
        try {
            $query = $this->Jobcard_model
                            ->where('id_project', $this->session->userdata('id_project'))
                            ->orderBy('seq', 'DESC')
                            ->first()->seq;
            // $last = $query->seq;
            $last_seq = $query + 1;
        } catch (Exception $e) {
            error_log($e);
            $last_seq = 1;
        }
        return '<input type="text" value="' . $last_seq . '" name="seq">';
    }

    public function crud_jobcard_update() {
        $data["session"] = $this->session->userdata('logged_in');

        $id = strip_tags($_POST['id']);
        $coltype = strip_tags($_POST['coltype']);
        $value = ($coltype == 'html') ? $_POST['newvalue'] : strip_tags($_POST['newvalue']);
        $colname = strip_tags($_POST['colname']);

        if ($colname == "SKILL_TYPE") {
            $colname = "SKILL";
        }

        if ($colname == 'ITVAL') {
            $jc = $this->Jobcard_model->where('AUFNR', $id)->first();
            if ($jc) {
                $CUST_JC_NUM = get_cust_jc_num($jc->KTEXT);
                $colname = 'KTEXT';
                $value = "[" . $value . "]" . $CUST_JC_NUM;
            }
        }


        if ($coltype == 'date') {
            if ($value === "") {
                $value = NULL;
            } else {
                $date_info = date_parse_from_format('d/m/Y', $value);
                $value = "{$date_info['year']}-{$date_info['month']}-{$date_info['day']}";
            }
        }

        try {
            $jobcard = $this->Jobcard_model->where('AUFNR', $id)
                    ->where('REVNR', $this->session->userdata('REVNR'))
                    ->update([$colname => $value]);
            if ($jobcard) {
                echo "ok";

                if ($colname == 'AREA') {
                    $mdr = $this->Mdr_model->where('JC_REFF', 'LIKE', '%' . $id)
                    ->update(["AREA" => $value]);
                }

            } else {
                echo "error";
            }
        } catch (Exception $exc) {
            echo "error";
            echo $exc;
        } 
    }

    public function crud_jobcard_delete() {
        $data["session"] = $this->session->userdata('logged_in');

        $id = strip_tags($_POST['id']);
        // $tablename = strip_tags($_POST['tablename']);

        try {
            $this->Jobcard_model->destroy($id);
        } catch (Exception $exc) {
            echo "error";
        } finally {
            echo "ok";
        }
    }


    public function import_csv_jobcard(){
        $response["status"] = NULL;
        $response["body"] = NULL;
        $post = $this->input->post();
      

        header('Content-Type: application/json');
        echo json_encode($post);
       // echo json_encode($response);
    }

    public function crud_mdr_new() {
        try {
            if ($this->uri->segment(3)) {
                $this->session->set_userdata('REVNR', $this->uri->segment(3));
            }
            $data['id_project'] = $this->session->userdata('REVNR');
            $active_prj = $this->Project_model_ci->getProjectName($data['id_project']);
            if ($active_prj) {
                $data['project_name'] = $active_prj->REVTX;
            }
        } catch (Exception $e) {
            error_log($e);
        }

        try {
            if ($this->uri->segment(4)) {
                $this->session->set_userdata('jc_id', $this->uri->segment(4));
            } else {
                $this->session->unset_userdata('jc_id');
            }

            if ($this->uri->segment(5)) {
                $this->session->set_userdata('mdr_type', $this->uri->segment(5));
            } else {
                $this->session->unset_userdata('mdr_type');
            }

            $data['jc_id'] = $this->session->userdata('jc_id');
        } catch (Exception $e) {
            error_log($e);
        }
        $this->Proc_model->merge_pmorder($this->session->userdata('REVNR'));
        $data['write'] = $this->is_write;
        $data["session"] = $this->session->userdata('logged_in');
        $data['content'] = 'admin/projects/crud_mdr_new';
        //print('<pre>'.print_r($data,TRUE).'</pre>');die();
        $this->load->view('template', $data);
    }

    public function crud_mdr() {
        try {
            $data['id_project'] = $this->uri->segment(3) ? $this->uri->segment(3) : '0';
            $active_prj = $this->Project_model_ci->getProjectName($data['id_project']);
            $data['project_name'] = $active_prj ? $active_prj->REVTX : "";  
        } catch (Exception $e) {
            error_log($e);
        }

        try {
            if ($this->uri->segment(4)) {
                $this->session->set_userdata('jc_id', $this->uri->segment(4));
            } else {
                $this->session->unset_userdata('jc_id');
            }

            if ($this->uri->segment(5)) {
                $this->session->set_userdata('mdr_type', $this->uri->segment(5));
            } else {
                $this->session->unset_userdata('mdr_type');
            }

            $data['jc_id'] = $this->session->userdata('jc_id');
        } catch (Exception $e) {
            error_log($e);
        }
        $this->Proc_model->merge_pmorder($this->session->userdata('REVNR'));
        $data['write'] = $this->is_write;
        $data["session"] = $this->session->userdata('logged_in');
        $data['content'] = 'admin/projects/crud_mdr';
        //print('<pre>'.print_r($data,TRUE).'</pre>');die();
        $this->load->view('template', $data);
    }

    public function read_xlsx_file_mdr() {
        $response["status"] = NULL;
        $response["body"] = NULL;
        try {
            $path = $_FILES['media']['tmp_name'];
            $reader = ReaderFactory::create(Type::XLSX);
            $reader->open($path);
            $i = 0;
            $data["error"] = array();
            $data["success"] = array();

            foreach ($reader->getSheetIterator() as $sheet) {
                foreach ($sheet->getRowIterator() as $row => $value) {
                    if ($i == 0) {
                        $i++;
                        continue;
                    }

                    $list = array(
                        'SKILL',
                        'DATE_PE',
                        'STATUS',
                        'MATSTATUS',
                        'STEP1',
                        'DATE1',
                        'STEP2',
                        'DATE2',
                        'STEP3',
                        'DATE3',
                        'DATECLOSE',
                        'REMARK',
                        'DOC_SENT_STATUS',
                        'FREETEXT',
                        'DATEPROGRESS',
                        'DAY',
                        'CABINSTATUS'
                    );

                    foreach ($value as $key2 => $value2) {
                        if ($key2 >= 3 and $key2 <= 18) {
                            $val = $value2 instanceof DateTime ?  $value2->format('Y-m-d ') : $value2;
                            $item[$list[$key2 - 3]] = (string) $val ?: null;
                        }
                    }


                    $mdr = $this->Mdr_model
                                ->where('REVNR', $value[0])
                                ->where('AUFNR', $value[1])
                                ->where('AUART', 'GA02')
                                ->update($item);

                    
                    if ($mdr) {
                        array_push($data["success"], $value[0]);

                        if (!empty($item['STATUS'])) {
                            $this->update_status_mdr($value[1], $item['STATUS']);
                        }

                    } else {
                        array_push($data["error"], $value[0]);
                    }
                }
            }

            if (count($data["success"]) > 0) {
                $response["status"] = 'success';
                $response["body"] = $data;
            }
        } catch (Exception $e) {
            echo $e;
            exit;
        }
        header('Content-Type: application/json');
        echo json_encode($response);
    }

    public function read_xlsx_file_mdr2() {
        $current_exec = 0;
        $last_exec = 0;
        $response["status"] = NULL;
        $response["body"] = NULL;
        $response["current_error"] = 0;
        $response["exec_per_row"] = [];

        try{
            $dataObject = json_decode($_POST['myData'], true);
            $data = array();

            $this->benchmark->mark('code_start');

            foreach ($dataObject as $key => $value) {
                $last_exec = $value['AUFNR'];
                $response["current_error"] = $key;

                $this->benchmark->mark('row_start');
                $jobcard = $this->Jobcard_model
                ->where('REVNR',  $value['REVNR'])
                ->where('AUFNR',  $value['AUFNR'])
                ->where('AUART', 'GA02')
                ->update($value);
                if ($jobcard) {
                    array_push($data, array("ORDER" => $value['AUFNR'], "STATUS" => 1));

                    if (!empty($value['AREA'])) {
                        $mdr = $this->Mdr_model
                            ->where('JC_REFF', 'LIKE', '%' . $value['AUFNR'])
                            ->update(["AREA" => $value['AREA']]);
                    }           
                

                } else {
                    array_push($data, array("ORDER" => $value['AUFNR'], "STATUS" => 0));
                }
                $this->benchmark->mark('row_end');
                array_push($response["exec_per_row"], $this->benchmark->elapsed_time('row_start', 'row_end'));

            }


            $this->benchmark->mark('code_end');

            
            if (count($data) > 0) {
                $response["status"] = "success";
                $response["body"] = NULL;
                $response["result"] = $data;
                $response["exec_up"] = $this->benchmark->elapsed_time('code_start', 'code_end');
            }else{
                $response["status"] = "error";
                $response["body"] = $data;
                $response["result"] = $data;
            }

        }catch(Exception $e){
            array_push($data, array("ORDER" => $last_exec, "STATUS" => 0));
            $response["status"] = "failed";
            $response["body"] = $e->getMessage();
            $response["result"] = $data;
        }

        header('Content-Type: application/json');
        echo json_encode($response);
    }

    public function write_xlsx_file_mdr(){
        $REVNR = $_GET['REVNR'];

        $mdr = $this->Proc_model->get_mdr($REVNR);
        $mdr_list = array();

        foreach($mdr as $value){
            if(!$this->is_customer){
                array_push(
                $mdr_list, 
                array(
                    $value->REVNR, 
                    $value->AUFNR, 
                    $value->KTEXT,
                    $value->SKILL,
                    $value->DATE_PE,
                    $value->STATUS,
                    $value->MATSTATUS,
                    $value->STEP1,
                    $value->DATE1,
                    $value->STEP2,
                    $value->DATE2,
                    $value->STEP3,
                    $value->DATE3,
                    $value->DATECLOSE,
                    $value->REMARK,
                    $value->DOC_SENT_STATUS,
                    $value->FREETEXT,
                    $value->DATEPROGRESS,
                    $value->DAY,
                    $value->CABINSTATUS
                ));
            }else{
                array_push(
                $mdr_list, 
                array(
                    $value->SEQ_NUM, 
                    $value->AUFNR, 
                    $value->JC_REF,
                    $value->KTEXT,
                    $value->MDR_STATUS,
                ));
            }
        }

        $writer = WriterFactory::create(Type::XLSX);
        $default_style = (new StyleBuilder())
                ->setFontName('Arial')
                ->setFontSize(10)
                ->setShouldWrapText(false)
                ->build();          
               
        
        $writer->setDefaultRowStyle($default_style);
        

        $file_name = "MDR_" . $REVNR . "_" . date('YmdhIs') . ".xlsx";
        $writer->openToFile($file_name); 

        $data_header = array();

        if(!$this->is_customer){
            $data_header[] = "REVISION";
            $data_header[] = "ORDER NUMBER";
            $data_header[] = "DESCRIPTION";
            $data_header[] = "SKILL";
            $data_header[] = "DATE PE";
            $data_header[] = "STATUS";
            $data_header[] = "MATERIAL STATUS";
            $data_header[] = "STEP1";
            $data_header[] = "DATE1";
            $data_header[] = "STEP2";
            $data_header[] = "DATE2";
            $data_header[] = "STEP3";
            $data_header[] = "DATE3";
            $data_header[] = "DATECLOSE";
            $data_header[] = "REMARK";
            $data_header[] = "DOC. SENT STATUS";
            $data_header[] = "FREETEXT";
            $data_header[] = "DATEPROGRESS";
            $data_header[] = "DAY";
            $data_header[] = "CABINSTATUS";
        }else{
            $data_header[] = "Seq";
            $data_header[] = "MDR Order";
            $data_header[] = "JC REFF";
            $data_header[] = "Discrepancies";
            $data_header[] = "Status";
        }


        $writer->addRow($data_header);

        if(!$this->is_customer){
            $query_header = array();
            $query_header[] = "REVNR";
            $query_header[] = "AUFNR";
            $query_header[] = "KTEXT";
            $query_header[] = "SKILL";
            $query_header[] = "DATE_PE";
            $query_header[] = "STATUS";
            $query_header[] = "MATSTATUS";
            $query_header[] = "STEP1";
            $query_header[] = "DATE1";
            $query_header[] = "STEP2";
            $query_header[] = "DATE2";
            $query_header[] = "STEP3";
            $query_header[] = "DATE3";
            $query_header[] = "DATECLOSE";
            $query_header[] = "REMARK";
            $query_header[] = "DOC_SENT_STATUS";
            $query_header[] = "FREETEXT";
            $query_header[] = "DATEPROGRESS";
            $query_header[] = "DAY";
            $query_header[] = "CABINSTATUS";
            $writer->addRow($query_header);
        }


        $writer->addRows($mdr_list);
        $writer->close();

        $this->load->helper('download');
        force_download($file_name, null);
        
    }

    public function crud_mdr_load() {
        $data["session"] = $this->session->userdata('logged_in');

        $grid = new EditableGrid();

        $grid->addColumn('SEQ_NUM', 'Seq', 'integer', NULL, false);
        $grid->addColumn('AUFNR_LINK', 'MDR Order', 'html', NULL, false);
        $grid->addColumn('MAUFNR_LINK', 'JC REFF', 'html', NULL, false);
        $grid->addColumn('CUST_JC_NUM', 'Cust JC Num', 'string', NULL, false);
        $grid->addColumn('KTEXT', 'Discrepancies', 'string');
        $grid->addColumn('AREA', 'AREA Code', 'string');
        $grid->addColumn('ERDAT', 'Created On', 'string', NULL, false);
        // $grid->addColumn('SKILL_TYPE', 'Main Skill', 'string');
        $grid->addColumn('SKILL_TYPE', 'Main Skill', 'string', array(""=>"","A/P" => "A/P", "E/A" => "E/A", "CBN" => "CBN", "STR" => "STR", "TBP" => "TBP"));
        $grid->addColumn('ERNAM', 'Iss. By', 'string', NULL, false);
        $grid->addColumn('DATE_PE', 'Date from PE', 'date');
        $grid->addColumn('STATUS', 'Accomp. Status', 'string', array(""=>"","CARRY OUT" => "CARRY OUT", "PERFORM BY PROD" => "PERFORM BY PROD", "PERFORM TO SHOP" => "PERFORM TO SHOP", "WAITING MATERIAL" => "WAITING MATERIAL", "WAITING TOOL" => "WAITING TOOL", "NEXT RO" => "NEXT RO", "WAITING RO" => "WAITING RO", "WAITING DEPLOYMENT" => "WAITING DEPLOYMENT", "WAITING CUST APPROVAL" => "WAITING CUST APPROVAL", "PREPARE FOR TEST" => "PREPARE FOR TEST", "PREPARE FOR RUN UP" => "PREPARE FOR RUN UP", "PREPARE FOR NDT" => "PREPARE FOR NDT", "PART AVAIL" => "PART AVAIL"));
        $grid->addColumn('MATSTATUS', 'Mat Status', 'string', array(""=>"","SHORTAGE" => "SHORTAGE", "GADC" => "GADC", "WCS" => "WCS"));
        $grid->addColumn('date', 'Date', 'date');
        $grid->addColumn('step1', 'STEP1', 'string', array(""=>"","A/P" => "A/P", "E/A" => "E/A", "CBN" => "CBN", "STR" => "STR", "TBRS" => "TBRS", "NDT" => "NDT", "SEAT" => "SEAT", "CBN SHOP" => "CBN SHOP", "TBP" => "TBP", "TVP" => "TVP", "CLN" => "CLN", "WHL" => "WHL", "RAHU" => "RAHU"));
        $grid->addColumn('date1', 'Date 1 ', 'date');
        $grid->addColumn('step2', 'STEP2', 'string', array(""=>"","A/P" => "A/P", "E/A" => "E/A", "CBN" => "CBN", "STR" => "STR", "TBRS" => "TBRS", "NDT" => "NDT", "SEAT" => "SEAT", "CBN SHOP" => "CBN SHOP", "TBP" => "TBP", "TVP" => "TVP", "CLN" => "CLN", "WHL" => "WHL", "RAHU" => "RAHU"));
        $grid->addColumn('date2', 'Date 2', 'date');
        $grid->addColumn('step3', 'STEP3', 'string', array(""=>"","A/P" => "A/P", "E/A" => "E/A", "CBN" => "CBN", "STR" => "STR", "TBRS" => "TBRS", "NDT" => "NDT", "SEAT" => "SEAT", "CBN SHOP" => "CBN SHOP", "TBP" => "TBP", "TVP" => "TVP", "CLN" => "CLN", "WHL" => "WHL", "RAHU" => "RAHU"));
        $grid->addColumn('date3', 'Date 3', 'date');
        $grid->addColumn('DATECLOSE', 'Date Close', 'date');
        $grid->addColumn('MAT_FULLFILLMENT_STATUS', 'Material Status MRM', 'string', NULL, false);
        $grid->addColumn('REMARK', 'Remark', 'string', array(""=>"","SENT TO CABIN" => "SENT TO CABIN", "SENT TO CABIN SHOP" => "SENT TO CABIN SHOP", "SENT TO PAINTING" => "SENT TO PAINTING", "SENT TO NDT" => "SENT TO NDT", "SENT TO TBR SHOP" => "SENT TO TBR SHOP", "SENT TO SEALANT" => "SENT TO SEALANT", "SENT TO STR HANGAR" => "SENT TO STR HANGAR", "SENT TO SEAT" => "SENT TO SEAT", "SENT TO WHEEL SHOP" => "SENT TO WHEEL SHOP", "SENT TO TV ENGINE" => "SENT TO TV ENGINE", "SENT TO CLEANING" => "SENT TO CLEANING", "MDR BELUM KE PPC" => "MDR BELUM KE PPC", "MATERIAL" => "MATERIAL", "RO BY ->" => "RO BY ->", "WAITING JOB ->" => "WAITING JOB", "COVER BY" => "COVER BY", "INTERUPT" => "INTERUPT", "MAP SHORTAGE" => "MAP SHORTAGE", "MAP GADC" => "MAP GADC", "MAP W. CUST. SUPPLY" => "MAP W. CUST. SUPPLY", "PART COMPLETE" => "PART COMPLETE", "PART PARTIAL" => "PART PARTIAL", "ANOTHER REASON" => "ANOTHER REASON"));
        $grid->addColumn('FREETEXT', 'Free Text', 'html');
        $grid->addColumn('DATEPROGRESS', 'Date Progress', 'date');
        $grid->addColumn('DAY', 'Day', 'integer');
        $grid->addColumn('CABINSTATUS', 'Cabin Status', 'string', NULL, false);
        $grid->addColumn('action', 'Action', 'html', NULL, false, 'id');

        $active_job = $this->Mdr_model_view
                ->selectRaw('*,'
                        . 'AUFNR AS id,'
                        . ((($this->db->dbdriver == 'sqlsrv') or ( $this->db->dbdriver == 'mssql')) ? "'<a href=\"" . base_url() . "projects/crud_mrm/' + REVNR + '/' + AUFNR + '\">' + AUFNR + '</a>' AS AUFNR_LINK," : "CONCAT('<a href=\"" . base_url() . "projects/crud_mrm/', REVNR, '/', AUFNR, '\">', AUFNR, '</a>') AS AUFNR_LINK,")
                        . ((($this->db->dbdriver == 'sqlsrv') or ( $this->db->dbdriver == 'mssql')) ? "'<a href=\"" . base_url() . "projects/crud_jobcard/' + REVNR + '/' + MAUFNR + '\">' + MAUFNR + '</a>' AS MAUFNR_LINK," : "CONCAT('<a href=\"" . base_url() . "projects/crud_mrm/', REVNR, '/', MAUFNR, '\">', MAUFNR, '</a>') AS MAUFNR_LINK,")
                        . '(select AREA from TB_V_JOBCARD_PROGRESS where TB_V_MDR_PROGRESS.MAUFNR = TB_V_JOBCARD_PROGRESS.AUFNR) AS AREA_2,'
                        . '(select SKILL_TYPE from TB_V_JOBCARD_PROGRESS where TB_V_MDR_PROGRESS.MAUFNR = TB_V_JOBCARD_PROGRESS.AUFNR) AS SKILL_TYPE_2')
                ->where('REVNR', $this->session->userdata('REVNR'))
                ->orderBy('AUFNR', 'ASC');

        if ($this->session->userdata('mdr_id') != NULL) {
            $active_job = $active_job->where('AUFNR', $this->session->userdata('mdr_id'));
        }

        if ($this->session->userdata('jc_id') != NULL) {
            $active_job = $active_job->where('MAUFNR', $this->session->userdata('jc_id'));
        }

        $totalUnfiltered = $active_job->count();
        $total = $totalUnfiltered;

        $page = 0;
        if (isset($_GET['page']) && is_numeric($_GET['page']))
            $page = (int) $_GET['page'];


        $rowByPage = $totalUnfiltered; // 50;

        $from = ($page - 1) * $rowByPage;

        if (isset($_GET['filter']) && $_GET['filter'] != "") {
            $filter = $_GET['filter'];
            $active_job = $active_job
                    ->where('AUFNR', 'like', '%' . $filter . '%')
                    ->orwhere('KTEXT', 'like', '%' . $filter . '%')
                    ->orwhere('CUST_JC_NUM', 'like', '%' . $filter . '%')
                    ->orwhere('AUART', 'like', '%' . $filter . '%')
                    ->orwhere('ITVAL', 'like', '%' . $filter . '%')
                    ->orwhere('ARBEI', 'like', '%' . $filter . '%')
                    ->orwhere('ILART', 'like', '%' . $filter . '%')
                    ->orwhere('AREA', 'like', '%' . $filter . '%')
                    ->orwhere('PHASE', 'like', '%' . $filter . '%')
                    ->orwhere('DAY', 'like', '%' . $filter . '%')
                    ->orwhere('STATUS', 'like', '%' . $filter . '%')
                    ->orwhere('DATECLOSE', 'like', '%' . $filter . '%')
                    ->orwhere('REMARK', 'like', '%' . $filter . '%')
                    ->orwhere('FREETEXT', 'like', '%' . $filter . '%')
                    ->orwhere('DATEPROGRESS', 'like', '%' . $filter . '%');
            $total = $active_job->count();
        }

        if (isset($_GET['sort']) && $_GET['sort'] != "") {
            $active_job = $active_job->orderBy($_GET['sort'], $_GET['asc'] == "0" ? "DESC" : "ASC");
        } else {
            $active_job = $active_job->orderBy('SEQ_NUM', 'ASC');
        }

        $active_job = $active_job->skip($from)->take($rowByPage);
        $grid->setPaginator(ceil($total / $rowByPage), (int) $total, (int) $totalUnfiltered, null);
        $grid->renderJSON($active_job->get(), false, false, !isset($_GET['data_only']));
    }

    public function crud_mdr_header(){
        $is_edit = $_GET['edit'] == 1 ? true : false;
        $data["session"] = $this->session->userdata('logged_in');

        $grid = new EditableGrid();
       if(!$this->is_customer){
            $grid->addColumn('SEQ_NUM', 'Seq', 'integer', NULL, false);
            $grid->addColumn('AUFNR_LINK', 'Order', 'html', NULL, false);
            $grid->addColumn('JC_REF_LINK', 'JC REF', 'html', NULL, false);
            $grid->addColumn('CUST_JC_NUM', 'Cust JC Num', 'string', NULL, false);
            $grid->addColumn('KTEXT_LINK', 'Discrepancies', 'html', NULL, false);
            $grid->addColumn('AREA', 'AREA', 'string', NULL, false);
            $grid->addColumn('ERDAT', 'Created On', 'string', NULL, false);
            $grid->addColumn('SKILL', 'Main Skill', 'string', array(""=>"","A/P" => "A/P", "E/A" => "E/A", "CBN" => "CBN", "STR" => "STR", "TBP" => "TBP"), $is_edit);
            $grid->addColumn('ERNAM', 'Iss. By', 'string', NULL, false);
            $grid->addColumn('DATE_PE', 'Date from PE', 'date', NULL, $is_edit);
            $grid->addColumn('STATUS', 'Accomp. Status', 'string', array(""=>"","OPEN" => "OPEN", "CARRY OUT" => "CARRY OUT", "PERFORM BY PROD" => "PERFORM BY PROD", "PERFORM TO SHOP" => "PERFORM TO SHOP", "WAITING MATERIAL" => "WAITING MATERIAL", "WAITING TOOL" => "WAITING TOOL", "NEXT RO" => "NEXT RO", "WAITING RO" => "WAITING RO", "WAITING DEPLOYMENT" => "WAITING DEPLOYMENT", "WAITING CUST APPROVAL" => "WAITING CUST APPROVAL", "PREPARE FOR TEST" => "PREPARE FOR TEST", "PREPARE FOR RUN UP" => "PREPARE FOR RUN UP", "PREPARE FOR NDT" => "PREPARE FOR NDT", "PART AVAIL" => "PART AVAIL"), $is_edit);
            $grid->addColumn('MATSTATUS', 'Mat Status', 'string', array(""=>"","SHORTAGE" => "SHORTAGE", "GADC" => "GADC", "WCS" => "WCS"), $is_edit);
            $grid->addColumn('STEP1', 'STEP1', 'string', array(""=>"","A/P" => "A/P", "E/A" => "E/A", "CBN" => "CBN", "STR" => "STR", "TBRS" => "TBRS", "NDT" => "NDT", "SEAT" => "SEAT", "CBN SHOP" => "CBN SHOP", "TBP" => "TBP", "TVP" => "TVP", "CLN" => "CLN", "WHL" => "WHL", "RAHU" => "RAHU"), $is_edit);
            $grid->addColumn('DATE1', 'Date 1 ', 'date', NULL, $is_edit);
            $grid->addColumn('STEP2', 'STEP2', 'string', array(""=>"","A/P" => "A/P", "E/A" => "E/A", "CBN" => "CBN", "STR" => "STR", "TBRS" => "TBRS", "NDT" => "NDT", "SEAT" => "SEAT", "CBN SHOP" => "CBN SHOP", "TBP" => "TBP", "TVP" => "TVP", "CLN" => "CLN", "WHL" => "WHL", "RAHU" => "RAHU"), $is_edit);
            $grid->addColumn('DATE2', 'Date 2', 'date', NULL, $is_edit);
            $grid->addColumn('STEP3', 'STEP3', 'string', array(""=>"","A/P" => "A/P", "E/A" => "E/A", "CBN" => "CBN", "STR" => "STR", "TBRS" => "TBRS", "NDT" => "NDT", "SEAT" => "SEAT", "CBN SHOP" => "CBN SHOP", "TBP" => "TBP", "TVP" => "TVP", "CLN" => "CLN", "WHL" => "WHL", "RAHU" => "RAHU"), $is_edit);
            $grid->addColumn('DATE3', 'Date 3', 'date', NULL, $is_edit);
            $grid->addColumn('MDR_STATUS', 'Status', 'string', NULL, false);
            $grid->addColumn('DATECLOSE', 'Date Close', 'date', NULL, $is_edit);
            $grid->addColumn('MATERIAL_FULFILLMENT_STATUS', 'Material Status MRM', 'string', NULL, false);
            $grid->addColumn('FULLFILLMENT_STATUS_DATE', 'Fulfillment Date', 'date', NULL, false);
            $grid->addColumn('REMARK', 'Remark', 'string', array(""=>"","MDR BELUM KE PPC" => "MDR BELUM KE PPC", "MATERIAL" => "MATERIAL", "RO BY ->" => "RO BY ->", "WAITING JOB ->" => "WAITING JOB", "COVER BY" => "COVER BY", "INTERUPT" => "INTERUPT", "MAP SHORTAGE" => "MAP SHORTAGE", "MAP GADC" => "MAP GADC", "MAP W. CUST. SUPPLY" => "MAP W. CUST. SUPPLY", "PART COMPLETE" => "PART COMPLETE", "PART PARTIAL" => "PART PARTIAL", "ANOTHER REASON" => "ANOTHER REASON"), $is_edit);
            $grid->addColumn('DOC_SENT_STATUS', 'Doc. Sent Status', 'string', array(""=>"","SENT TO CABIN" => "SENT TO CABIN", "SENT TO CABIN SHOP" => "SENT TO CABIN SHOP", "SENT TO PAINTING" => "SENT TO PAINTING", "SENT TO NDT" => "SENT TO NDT", "SENT TO TBR SHOP" => "SENT TO TBR SHOP", "SENT TO SEALANT" => "SENT TO SEALANT", "SENT TO STR HANGAR" => "SENT TO STR HANGAR", "SENT TO SEAT" => "SENT TO SEAT", "SENT TO WHEEL SHOP" => "SENT TO WHEEL SHOP", "SENT TO TV ENGINE" => "SENT TO TV ENGINE", "SENT TO CLEANING" => "SENT TO CLEANING"), $is_edit);
            $grid->addColumn('FREETEXT', 'Free Text', 'html', NULL, $is_edit);
            $grid->addColumn('DATEPROGRESS', 'Date Progress', 'date', NULL, $is_edit);
            $grid->addColumn('DAY', 'Day', 'integer', NULL, $is_edit);
            $grid->addColumn('CABINSTATUS', 'Cabin Status', 'string', array(
                ""=>"",
                "Open" => "Open", 
                "Waiting RO"=>"Waiting RO", 
                "Waiting Material" => "Waiting Material",
                "Progress in Hangar" => "Progress in Hangar",
                "Progress in W101" => "Progress in W101",
                "Progress in W102" => "Progress in W102",
                "Progress in W103" => "Progress in W103",
                "Progress in W401" => "Progress in W401",
                "Progress in W402" => "Progress in W402",
                "Progress in W501" => "Progress in W501",
                "Progress in W502" => "Progress in W502",
                "Close" => "Close" 
            ), $is_edit);
            if($data['session']['user_group_level'] == 1 || $data['session']['user_group_level'] == 2){
                if ($is_edit) {
                    $grid->addColumn('AUFNR_DEL', 'Action', 'html', NULL, false);
                }
            }
        } else {
            $grid->addColumn('SEQ_NUM', 'Seq', 'integer', NULL, false);
            $grid->addColumn('AUFNR_LINK', 'MDR Order', 'html', NULL, false);
            $grid->addColumn('JC_REF', 'JC REFF', 'html', NULL, false);
            $grid->addColumn('KTEXT_LINK', 'Discrepancies', 'html', NULL, false);
            $grid->addColumn('MDR_STATUS', 'Status', 'string', NULL, false);
        }

        $ORDER = 'SEQ_NUM';
        $AUFNR = 0;
        $JC_REF = '';
        $FILTER = '';

        if (isset($_GET['sort']) && $_GET['sort'] != "") {
            $ORDER = $_GET['sort'] . ($_GET['asc'] == "0" ? " DESC" : " ASC");
        }
        
        if (($this->session->userdata("mdr_type") == "JC") || $this->session->userdata("mdr_type") == "jc") {
            $JC_REF = $this->session->userdata('jc_id');
        }
        
        if (($this->session->userdata("mdr_type") == "MRM") || $this->session->userdata("mdr_type") == "mrm") {
            $AUFNR = $this->session->userdata('jc_id');
        }
        
        if (($this->session->userdata("mdr_type") == "CSP") || $this->session->userdata("mdr_type") == "csp") {
            $AUFNR = $this->session->userdata('jc_id');
        }

        if (($this->session->userdata("mdr_type") == "PRM") || $this->session->userdata("mdr_type") == "prm") {
            $AUFNR = $this->session->userdata('jc_id');
        }

        if (($this->session->userdata("mdr_type") == "CRM") || $this->session->userdata("mdr_type") == "crm") {
            $AUFNR = $this->session->userdata('jc_id');
        }

        if (isset($_GET['filter']) && $_GET['filter'] != "") {
            $FILTER = $_GET['filter'];
        }
        $active_job=[];
        // $active_job = $this->Proc_model->get_mdr($this->session->userdata('REVNR'), $ORDER, $AUFNR, $JC_REF, $FILTER);

        // for($a=0;$a<count($active_job);$a++){
        //     if(empty($active_job[$a]->STEP1)){
        //         $active_job[$a]->SKILL = "";
        //     }elseif(empty($active_job[$a]->STEP2)){
        //         $active_job[$a]->SKILL = $active_job[$a]->STEP1;
        //     }elseif(empty($active_job[$a]->STEP3)){
        //         $active_job[$a]->SKILL = $active_job[$a]->STEP2;
        //     }else{
        //         $active_job[$a]->SKILL = $active_job[$a]->STEP3;
        //     }

        //     if((!empty($active_job[$a]->DATE1)) && (strtotime(date('Y-m-d')) <= strtotime($active_job[$a]->DATE1))){
        //         $active_job[$a]->SKILL = $active_job[$a]->STEP1;
        //     }elseif((!empty($active_job[$a]->DATE2)) && (strtotime(date('Y-m-d')) <= strtotime($active_job[$a]->DATE2))){                
        //         $active_job[$a]->SKILL = $active_job[$a]->STEP2;
        //     }elseif((!empty($active_job[$a]->DATE3)) && (strtotime(date('Y-m-d')) <= strtotime($active_job[$a]->DATE3))){
        //         $active_job[$a]->SKILL = $active_job[$a]->STEP3;
        //     }
        // }
        $grid->renderJSON($active_job, false, false, true);
    }	    
    public function crud_mdr_load_all_new() {
        $is_edit = $_GET['edit'] == 1 ? true : false;
        $data["session"] = $this->session->userdata('logged_in');
   

        $ORDER = 'SEQ_NUM';
        $AUFNR = 0;
        $JC_REF = '';
        $FILTER = '';

        if (isset($_GET['sort']) && $_GET['sort'] != "") {
            $ORDER = $_GET['sort'] . ($_GET['asc'] == "0" ? " DESC" : " ASC");
        }
        
        if (($this->session->userdata("mdr_type") == "JC") || $this->session->userdata("mdr_type") == "jc") {
            $JC_REF = $this->session->userdata('jc_id');
        }
        
        if (($this->session->userdata("mdr_type") == "MRM") || $this->session->userdata("mdr_type") == "mrm") {
            $AUFNR = $this->session->userdata('jc_id');
        }
        
        if (($this->session->userdata("mdr_type") == "CSP") || $this->session->userdata("mdr_type") == "csp") {
            $AUFNR = $this->session->userdata('jc_id');
        }

        if (($this->session->userdata("mdr_type") == "PRM") || $this->session->userdata("mdr_type") == "prm") {
            $AUFNR = $this->session->userdata('jc_id');
        }

        if (($this->session->userdata("mdr_type") == "CRM") || $this->session->userdata("mdr_type") == "crm") {
            $AUFNR = $this->session->userdata('jc_id');
        }

        if (isset($_GET['filter']) && $_GET['filter'] != "") {
            $FILTER = $_GET['filter'];
        }
        
        // $active_job = $this->Proc_model->get_mdr($this->session->userdata('REVNR'), $ORDER, $AUFNR, $JC_REF, $FILTER);
        // $this->db->query();
        //$this->db->query();
        $listmdr= $this->Api_Proc_model->get_mdr($this->session->userdata('REVNR'), $ORDER, $AUFNR, $JC_REF, $FILTER,FALSE);
        // $total= $this->Api_Proc_model->get_mdr($this->session->userdata('REVNR'), $ORDER, $AUFNR, $JC_REF, $FILTER,TRUE);
        // print_r($listmdr);die;
        $array = array("size" => "XL", "color" => "gold");
        // print_r($listmdr);die;
        $filter= count($listmdr);
       // $objcet=json_decode(json_encode($listmdr));
        // print_r($listmdr);die;
        for($a=0;$a<count($listmdr);$a++){
            if(empty($listmdr[$a]['STEP1'])){
               $listmdr[$a]['SKILL'] = "";
            }elseif(empty($$listmdr[$a]['STEP2'])){
                $listmdr[$a]['SKILL'] = $listmdr[$a]['STEP1'];
            }elseif(empty($listmdr[$a]['STEP3'])){
                $listmdr[$a]['SKILL'] = $listmdr[$a]['STEP2'];
            }else{
                $listmdr[$a]['SKILL'] = $listmdr[$a]['STEP3'];
            }
            if((!empty($listmdr[$a]['DATE1'])) && (strtotime(date('Y-m-d')) <= strtotime($listmdr[$a]['DATE1']))){
                $listmdr[$a]['SKILL'] = $listmdr[$a]['STEP1'];
            }elseif((!empty($listmdr[$a]['DATE2'])) && (strtotime(date('Y-m-d')) <= strtotime($listmdr[$a]['DATE2']))){                
                $listmdr[$a]['SKILL'] = $listmdr[$a]['STEP2'];
            }elseif((!empty($listmdr[$a]['DATE3'])) && (strtotime(date('Y-m-d')) <= strtotime($listmdr[$a]['DATE3']))){
                $listmdr[$a]['SKILL'] = $listmdr[$a]['STEP3'];
            }
        }
        $rec = array();
        $k=0;
        if (isset($listmdr) && is_array($listmdr)) {
        foreach ($listmdr as $item) {
            $rec['aaData'][$k] = array_values($item);
            $k++;
        }
        }
        echo json_encode($rec);
    }    
    
    public function crud_mdr_load_all() {
        $is_edit = $_GET['edit'] == 1 ? true : false;
        $data["session"] = $this->session->userdata('logged_in');

        $grid = new EditableGrid();
        if(!$this->is_customer){
            $grid->addColumn('SEQ_NUM', 'Seq', 'integer', NULL, false);
            $grid->addColumn('AUFNR_LINK', 'Order', 'html', NULL, false);
            $grid->addColumn('JC_REF_LINK', 'JC REF', 'html', NULL, false);
            $grid->addColumn('CUST_JC_NUM', 'Cust JC Num', 'string', NULL, false);
            $grid->addColumn('KTEXT_LINK', 'Discrepancies', 'html', NULL, false);
            $grid->addColumn('AREA', 'AREA', 'string', NULL, false);
            $grid->addColumn('ERDAT', 'Created On', 'string', NULL, false);
            $grid->addColumn('SKILL', 'Main Skill', 'string', array(""=>"","A/P" => "A/P", "E/A" => "E/A", "CBN" => "CBN", "STR" => "STR", "TBP" => "TBP"), $is_edit);
            $grid->addColumn('ERNAM', 'Iss. By', 'string', NULL, false);
            $grid->addColumn('DATE_PE', 'Date from PE', 'date', NULL, $is_edit);
            $grid->addColumn('STATUS', 'Accomp. Status', 'string', array(""=>"","OPEN" => "OPEN", "CARRY OUT" => "CARRY OUT", "PERFORM BY PROD" => "PERFORM BY PROD", "PERFORM TO SHOP" => "PERFORM TO SHOP", "WAITING MATERIAL" => "WAITING MATERIAL", "WAITING TOOL" => "WAITING TOOL", "NEXT RO" => "NEXT RO", "WAITING RO" => "WAITING RO", "WAITING DEPLOYMENT" => "WAITING DEPLOYMENT", "WAITING CUST APPROVAL" => "WAITING CUST APPROVAL", "PREPARE FOR TEST" => "PREPARE FOR TEST", "PREPARE FOR RUN UP" => "PREPARE FOR RUN UP", "PREPARE FOR NDT" => "PREPARE FOR NDT", "PART AVAIL" => "PART AVAIL"), $is_edit);
            $grid->addColumn('MATSTATUS', 'Mat Status', 'string', array(""=>"","SHORTAGE" => "SHORTAGE", "GADC" => "GADC", "WCS" => "WCS"), $is_edit);
            $grid->addColumn('STEP1', 'STEP1', 'string', array(""=>"","A/P" => "A/P", "E/A" => "E/A", "CBN" => "CBN", "STR" => "STR", "TBRS" => "TBRS", "NDT" => "NDT", "SEAT" => "SEAT", "CBN SHOP" => "CBN SHOP", "TBP" => "TBP", "TVP" => "TVP", "CLN" => "CLN", "WHL" => "WHL", "RAHU" => "RAHU"), $is_edit);
            $grid->addColumn('DATE1', 'Date 1 ', 'date', NULL, $is_edit);
            $grid->addColumn('STEP2', 'STEP2', 'string', array(""=>"","A/P" => "A/P", "E/A" => "E/A", "CBN" => "CBN", "STR" => "STR", "TBRS" => "TBRS", "NDT" => "NDT", "SEAT" => "SEAT", "CBN SHOP" => "CBN SHOP", "TBP" => "TBP", "TVP" => "TVP", "CLN" => "CLN", "WHL" => "WHL", "RAHU" => "RAHU"), $is_edit);
            $grid->addColumn('DATE2', 'Date 2', 'date', NULL, $is_edit);
            $grid->addColumn('STEP3', 'STEP3', 'string', array(""=>"","A/P" => "A/P", "E/A" => "E/A", "CBN" => "CBN", "STR" => "STR", "TBRS" => "TBRS", "NDT" => "NDT", "SEAT" => "SEAT", "CBN SHOP" => "CBN SHOP", "TBP" => "TBP", "TVP" => "TVP", "CLN" => "CLN", "WHL" => "WHL", "RAHU" => "RAHU"), $is_edit);
            $grid->addColumn('DATE3', 'Date 3', 'date', NULL, $is_edit);
            $grid->addColumn('MDR_STATUS', 'Status', 'string', NULL, false);
            $grid->addColumn('DATECLOSE', 'Date Close', 'date', NULL, $is_edit);
            $grid->addColumn('MATERIAL_FULFILLMENT_STATUS', 'Material Status MRM', 'string', NULL, false);
            $grid->addColumn('FULLFILLMENT_STATUS_DATE', 'Fulfillment Date', 'date', NULL, false);
            $grid->addColumn('REMARK', 'Remark', 'string', array(""=>"","MDR BELUM KE PPC" => "MDR BELUM KE PPC", "MATERIAL" => "MATERIAL", "RO BY ->" => "RO BY ->", "WAITING JOB ->" => "WAITING JOB", "COVER BY" => "COVER BY", "INTERUPT" => "INTERUPT", "MAP SHORTAGE" => "MAP SHORTAGE", "MAP GADC" => "MAP GADC", "MAP W. CUST. SUPPLY" => "MAP W. CUST. SUPPLY", "PART COMPLETE" => "PART COMPLETE", "PART PARTIAL" => "PART PARTIAL", "ANOTHER REASON" => "ANOTHER REASON"), $is_edit);
            $grid->addColumn('DOC_SENT_STATUS', 'Doc. Sent Status', 'string', array(""=>"","SENT TO CABIN" => "SENT TO CABIN", "SENT TO CABIN SHOP" => "SENT TO CABIN SHOP", "SENT TO PAINTING" => "SENT TO PAINTING", "SENT TO NDT" => "SENT TO NDT", "SENT TO TBR SHOP" => "SENT TO TBR SHOP", "SENT TO SEALANT" => "SENT TO SEALANT", "SENT TO STR HANGAR" => "SENT TO STR HANGAR", "SENT TO SEAT" => "SENT TO SEAT", "SENT TO WHEEL SHOP" => "SENT TO WHEEL SHOP", "SENT TO TV ENGINE" => "SENT TO TV ENGINE", "SENT TO CLEANING" => "SENT TO CLEANING"), $is_edit);
            $grid->addColumn('FREETEXT', 'Free Text', 'html', NULL, $is_edit);
            $grid->addColumn('DATEPROGRESS', 'Date Progress', 'date', NULL, $is_edit);
            $grid->addColumn('DAY', 'Day', 'integer', NULL, $is_edit);
            $grid->addColumn('CABINSTATUS', 'Cabin Status', 'string', array(
                ""=>"",
                "Open" => "Open", 
                "Waiting RO"=>"Waiting RO", 
                "Waiting Material" => "Waiting Material",
                "Progress in Hangar" => "Progress in Hangar",
                "Progress in W101" => "Progress in W101",
                "Progress in W102" => "Progress in W102",
                "Progress in W103" => "Progress in W103",
                "Progress in W401" => "Progress in W401",
                "Progress in W402" => "Progress in W402",
                "Progress in W501" => "Progress in W501",
                "Progress in W502" => "Progress in W502",
                "Close" => "Close" 
            ), $is_edit);
            if($data['session']['user_group_level'] == 1 || $data['session']['user_group_level'] == 2){
                if ($is_edit) {
                    $grid->addColumn('AUFNR_DEL', 'Action', 'html', NULL, false);
                }
            }
        } else {
            $grid->addColumn('SEQ_NUM', 'Seq', 'integer', NULL, false);
            $grid->addColumn('AUFNR_LINK', 'MDR Order', 'html', NULL, false);
            $grid->addColumn('JC_REF', 'JC REFF', 'html', NULL, false);
            $grid->addColumn('KTEXT_LINK', 'Discrepancies', 'html', NULL, false);
            $grid->addColumn('MDR_STATUS', 'Status', 'string', NULL, false);
        }

        $ORDER = 'SEQ_NUM';
        $AUFNR = 0;
        $JC_REF = '';
        $FILTER = '';

        if (isset($_GET['sort']) && $_GET['sort'] != "") {
            $ORDER = $_GET['sort'] . ($_GET['asc'] == "0" ? " DESC" : " ASC");
        }
        
        if (($this->session->userdata("mdr_type") == "JC") || $this->session->userdata("mdr_type") == "jc") {
            $JC_REF = $this->session->userdata('jc_id');
        }
        
        if (($this->session->userdata("mdr_type") == "MRM") || $this->session->userdata("mdr_type") == "mrm") {
            $AUFNR = $this->session->userdata('jc_id');
        }
        
        if (($this->session->userdata("mdr_type") == "CSP") || $this->session->userdata("mdr_type") == "csp") {
            $AUFNR = $this->session->userdata('jc_id');
        }

        if (($this->session->userdata("mdr_type") == "PRM") || $this->session->userdata("mdr_type") == "prm") {
            $AUFNR = $this->session->userdata('jc_id');
        }

        if (($this->session->userdata("mdr_type") == "CRM") || $this->session->userdata("mdr_type") == "crm") {
            $AUFNR = $this->session->userdata('jc_id');
        }

        if (isset($_GET['filter']) && $_GET['filter'] != "") {
            $FILTER = $_GET['filter'];
        }
        
        $active_job = $this->Proc_model->get_mdr($this->session->userdata('REVNR'), $ORDER, $AUFNR, $JC_REF, $FILTER);

        for($a=0;$a<count($active_job);$a++){
            if(empty($active_job[$a]->STEP1)){
                $active_job[$a]->SKILL = "";
            }elseif(empty($active_job[$a]->STEP2)){
                $active_job[$a]->SKILL = $active_job[$a]->STEP1;
            }elseif(empty($active_job[$a]->STEP3)){
                $active_job[$a]->SKILL = $active_job[$a]->STEP2;
            }else{
                $active_job[$a]->SKILL = $active_job[$a]->STEP3;
            }

            if((!empty($active_job[$a]->DATE1)) && (strtotime(date('Y-m-d')) <= strtotime($active_job[$a]->DATE1))){
                $active_job[$a]->SKILL = $active_job[$a]->STEP1;
            }elseif((!empty($active_job[$a]->DATE2)) && (strtotime(date('Y-m-d')) <= strtotime($active_job[$a]->DATE2))){                
                $active_job[$a]->SKILL = $active_job[$a]->STEP2;
            }elseif((!empty($active_job[$a]->DATE3)) && (strtotime(date('Y-m-d')) <= strtotime($active_job[$a]->DATE3))){
                $active_job[$a]->SKILL = $active_job[$a]->STEP3;
            }
        }

        $grid->renderJSON($active_job, false, false, true);
    }

    public function crud_mdr_add() {
        $crud = new grocery_CRUD();

        $crud->set_table('mdr');
        $crud->set_subject('Mdr');
        $crud->columns('seq', 'date_from_pe', 'accomp_status', 'mat_status', 'date', 'step1', 'date1', 'step2', 'date2', 'step3', 'date3', 'status_mdr', 'date_close', 'remark', 'free_text', 'date_progress', 'day');
        $crud->field_type('accomp_status', 'dropdown', array(""=>"","Carry Out" => "Carry Out", "Perform by Prod" => "Perform by Prod", "Perform To Shop" => "Perform To Shop", "Waiting Material" => "Waiting Material", "Waiting Tool" => "Waiting Tool", "Next RO" => "Next RO", "Waiting RO" => "Waiting RO", "Waiting Deployment" => "Waiting Deployment", "Waiting Cust Approval" => "Waiting Cust Approval", "Prepare for Test" => "Prepare for Test", "Prepare for Run Up" => "Prepare for Run Up", "Prepare for NDT" => "Prepare for NDT", "Part Avail" => "Part Avail"));
        $crud->field_type('mat_status', 'dropdown', array(""=>"","Shortage" => "Shortage", "GADC" => "GADC", "WCS" => "WCS"));
        $crud->field_type('step1', 'dropdown', array(""=>"","A/P" => "A/P", "E/A" => "E/A", "CBN" => "CBN", "STR" => "STR", "TBRS" => "TBRS", "NDT" => "NDT", "SEAT" => "SEAT", "CBN SHOP" => "CBN SHOP", "TBP" => "TBP", "TVP" => "TVP", "CLN" => "CLN", "WHL" => "WHL", "RAHU" => "RAHU"));
        $crud->field_type('step2', 'dropdown', array(""=>"","A/P" => "A/P", "E/A" => "E/A", "CBN" => "CBN", "STR" => "STR", "TBRS" => "TBRS", "NDT" => "NDT", "SEAT" => "SEAT", "CBN SHOP" => "CBN SHOP", "TBP" => "TBP", "TVP" => "TVP", "CLN" => "CLN", "WHL" => "WHL", "RAHU" => "RAHU"));
        $crud->field_type('step3', 'dropdown', array(""=>"","A/P" => "A/P", "E/A" => "E/A", "CBN" => "CBN", "STR" => "STR", "TBRS" => "TBRS", "NDT" => "NDT", "SEAT" => "SEAT", "CBN SHOP" => "CBN SHOP", "TBP" => "TBP", "TVP" => "TVP", "CLN" => "CLN", "WHL" => "WHL", "RAHU" => "RAHU"));
        $crud->field_type('remark', 'dropdown', array(""=>"","SENT TO CABIN" => "SENT TO CABIN", "SENT TO CABIN SHOP" => "SENT TO CABIN SHOP", "SENT TO PAINTING" => "SENT TO PAINTING", "SENT TO NDT" => "SENT TO NDT", "SENT TO TBR SHOP" => "SENT TO TBR SHOP", "SENT TO SEALANT" => "SENT TO SEALANT", "SENT TO STR HANGAR" => "SENT TO STR HANGAR", "SENT TO SEAT" => "SENT TO SEAT", "SENT TO WHEEL SHOP" => "SENT TO WHEEL SHOP", "SENT TO TV ENGINE" => "SENT TO TV ENGINE", "SENT TO CLEANING" => "SENT TO CLEANING", "MDR BELUM KE PPC" => "MDR BELUM KE PPC", "MATERIAL" => "MATERIAL", "RO BY ->" => "RO BY ->", "WAITING JOB ->" => "WAITING JOB", "COVER BY" => "COVER BY", "INTERUPT" => "INTERUPT", "MAP SHORTAGE" => "MAP SHORTAGE", "MAP GADC" => "MAP GADC", "MAP W. CUST. SUPPLY" => "MAP W. CUST. SUPPLY", "PART COMPLETE" => "PART COMPLETE", "PART PARTIAL" => "PART PARTIAL", "ANOTHER REASON" => "ANOTHER REASON"));
        $crud->set_relation('jc_reff', 'jobcard', 'jc_order');
        $crud->unset_fields('ori_taskcard', 'jc_reff', 'discrepancies', 'area_code', 'main_skill', 'iss_by', 'mdr_order', 'material_status_mrm', 'status_sap', 'cabin_status', 'id_project', 'date_created', 'id');
        $crud->callback_after_insert(array($this, 'crud_mdr_after_insert'));
        $crud->callback_add_field('seq', array($this, 'crud_mdr_seq_add_field_callback'));
        $crud->unset_back_to_list();
        $output = $crud->render();

        $data["session"] = $this->session->userdata('logged_in');

        $this->load->view('example', array_merge($data, (array) $output));
    }

    function crud_mdr_after_insert($post_array, $primary_key) {
        date_default_timezone_set('Asia/Jakarta');
        $mdr_update = array(
            "id_project" => $this->session->userdata('id_project'),
            'date_created' => date('Y-m-d H:i:s')
        );
        $this->db->update('mdr', $mdr_update, array('id' => $primary_key));
        return true;
    }

    function crud_mdr_seq_add_field_callback() {
        error_reporting(E_ALL & ~E_NOTICE);
        $last_seq = 0;
        try {
            $query = $this->Mdr_model
                            ->where('id_project', $this->session->userdata('id_project'))
                            ->orderBy('seq', 'DESC')
                            ->first()->seq;
            // $last = $query->seq;
            $last_seq = $query + 1;
        } catch (Exception $e) {
            error_log($e);
            $last_seq = 1;
        }
        return '<input type="text" value="' . $last_seq . '" name="seq">';
    }

    public function crud_mdr_update() {
        $data["session"] = $this->session->userdata('logged_in');

        $colname = strip_tags($_POST['colname']);
        $id = strip_tags($_POST['id']);
        $coltype = strip_tags($_POST['coltype']);
        $value = strip_tags($_POST['newvalue']);

        if ($colname == 'SKILL_TYPE') {
            $colname = 'SKILL';
        }

        if ($coltype == 'date') {
            if ($value === "") {
                $value = NULL;
            } else {
                $date_info = date_parse_from_format('d/m/Y', $value);
                $value = "{$date_info['year']}-{$date_info['month']}-{$date_info['day']}";
            } 	
        }

        try {
            $mdr = $this->Mdr_model->where('AUFNR', $id)
                    ->where('REVNR', $this->session->userdata('REVNR'))
                    ->update([$colname => $value]);
            if ($mdr) {
                echo "ok";
                if ($colname === 'STATUS') {
                    $this->update_status_mdr($id, $value);
                }
            } else {
                echo "error";
            }
        } catch (Exception $exc) {
            echo $exc;
        }
    }

 public function crud_mdr_update_new() {
        $data["session"] = $this->session->userdata('logged_in');

        $colname = strip_tags($_POST['colname']);
        $id = strip_tags($_POST['id']);
        $coltype = strip_tags($_POST['coltype']);
        $value = strip_tags($_POST['newvalue']);
        $oldvalue = strip_tags($_POST['oldvalue']);
   
        if ($colname == 'SKILL_TYPE') {
            $colname = 'SKILL';
        }

        if ($coltype == 'date') {
            if ($value === "") {
                $value = NULL;
            } else {
                $date_info = date_parse_from_format('d/m/Y', $value);
                $value = "{$date_info['year']}-{$date_info['month']}-{$date_info['day']}";
            }
        }

        try {
            $mdr = $this->Mdr_model->where('AUFNR', $id)
                    ->where('REVNR', $this->session->userdata('REVNR'))
                    ->update([$colname => $value]);
            if ($mdr) {
                echo "ok#".$oldvalue."#".$value."#".$id."#".$colname;
                if ($colname === 'STATUS') {
                    $this->update_status_mdr($id, $value);
                }
            } else {
                echo "error#".$oldvalue."#".$value."#".$id."#".$colname;
            }
        } catch (Exception $exc) {
            echo $exc;
        }
    }



    public function update_status_mdr($AUFNR, $mdr_status) {
        try {
            $open = array("OPEN", "WAITING RO", "WAITING MATERIAL", "NEXT RO", "WAITING CUST APPROVAL", "WAITING TOOL", "WAITING DEPLOYMENT", "PART AVAIL");
            $progress = array("PERFORM BY PROD", "PERFORM TO SHOP");
            $pending = array("PREPARE FOR TEST", "PREPARE FOR NDT", "PREPARE FOR RUN UP");
            $close = array("CARRY OUT");

            $status = '';
            if (in_array($mdr_status, $open)) {
                $status = 'OPEN';
            }
            if (in_array($mdr_status, $progress)) {
                $status = 'PROGRESS';
            }
            if (in_array($mdr_status, $pending)) {
                $status = 'PENDING';
            }
            if (in_array($mdr_status, $close)) {
                $status = 'CLOSED';
            }

            $mdr = $this->Mdr_model->where('AUFNR', $AUFNR)
                    ->where('REVNR', $this->session->userdata('REVNR'))
                    ->update(['MDR_STATUS' => $status]);
        } catch (Exception $e) {
            echo $e;
        }
    }

    public function crud_mdr_delete() {
        try {
            if ($this->uri->segment(4)) {
                $this->session->set_userdata('id_project', $this->uri->segment(4));
            }
            $data['id_project'] = $this->session->userdata('id_project');
            $active_prj = $this->VProject_model->find($data['id_project']);
            if ($active_prj) {
                $data['project_name'] = $active_prj->project_name;
            }
        } catch (Exception $e) {
            error_log($e);
        }

        $data["session"] = $this->session->userdata('logged_in');

        $id = strip_tags($_POST['id']);
        // $tablename = strip_tags($_POST['tablename']);

        try {
            $this->Mdr_model->destroy($id);
        } catch (Exception $exc) {
            echo "error";
        } finally {
            echo "ok";
        }
    }

    public function get_mdr_by_jc(){        
        $response["status"] = NULL;
        $response["body"] = NULL;

        try{
            $JC_REF = $this->input->post('JC_REFF');
            $active_job = $this->Proc_model->get_mdr($this->session->userdata('REVNR'), 'SEQ_NUM', 0, $JC_REF, '');
            if($active_job){
                $response["status"] = 'success';
                $response["body"] = $active_job;
            }
        }catch(Exception $e){

        }

        header('Content-Type: application/json');
        echo json_encode($response);
    }

    public function crud_mrm() {
        try {
            $data['id_project'] = $this->uri->segment(3) ? $this->uri->segment(3) : '0';
            $active_prj = $this->Project_model_ci->getProjectName($data['id_project']);
            $data['project_name'] = $active_prj ? $active_prj->REVTX : "";  
        } catch (Exception $e) {
            error_log($e);
        }

        try {
            if ($this->uri->segment(4)) {
                $this->session->set_userdata('jc_id', $this->uri->segment(4));
            } else {
                $this->session->unset_userdata('jc_id');
            }
            $data['jc_id'] = $this->session->userdata('jc_id');
        } catch (Exception $e) {
            error_log($e);
        }

        try {
            if ($this->uri->segment(5)) {
                $this->session->set_userdata('mrm_type', $this->uri->segment(5));
            } else {
                $this->session->unset_userdata('mrm_type');
            }
            $data['mrm_type'] = $this->session->userdata('mrm_type');
        } catch (Exception $e) {
            error_log($e);
        }
//        $this->Proc_model->merge_mrm($this->session->userdata('REVNR'));
        $data['write'] = $this->is_write;
        $data["session"] = $this->session->userdata('logged_in');
        $data['content'] = 'admin/projects/crud_mrm';
        $this->load->view('template', $data);
    }
    
    public function merge_mrm($REVNR) {
        if ($this->Proc_model->merge_mrm($REVNR)) {
            echo 1;
        } else {
            echo 0;
        }
    }

    public function check_duplicate_mrm() {
        $response["status"] = NULL;
        $response["body"] = NULL;
        try {
            $mrm = $this->Mrm_model
                    ->where('AUFNR', $_POST['AUFNR'])
                    ->where('MATNR', $_POST['MATNR'])
                    ->where('IS_ACTIVE', 1)
                    ->count();

            if ($mrm > 0) {
                $response["status"] = 'error';
            } else {
                $response["status"] = 'success';
            }
        } catch (Exception $e) {
            
        }
        header('Content-Type: application/json');
        echo json_encode($response);
    }

    public function crud_mrm_load() {
        $is_edit = $_GET['edit'] == 1 ? true : false;
        $data["session"] = $this->session->userdata('logged_in');
        $grid = new EditableGrid();
        $REVNR = $this->input->get('REVNR');


        $grid->addColumn('ID', 'ID', 'integer', NULL, false);
        // $grid->addColumn('SEQ_NUM', 'SEQ', 'integer', NULL, false);
        $grid->addColumn('MATNR', 'Part Number', 'string', NULL, false);
        $grid->addColumn('ALTERNATE_PART_NUMBER', 'Alternate Part Number', 'string', NULL, $is_edit);
        $grid->addColumn('MAKTX', 'Material Description', 'string', NULL, false);
        $grid->addColumn('MTART', 'Mat Type', 'string', array(""=>"","EXP" => "EXP", "ROT" => "ROT", "REP" => "REP", "RAW" => "RAW", "CON" => "CON"), $is_edit);
        $grid->addColumn('IPC', 'IPC', 'string', NULL, $is_edit);
        $grid->addColumn('MRM_ISSUE_DATE', 'MRM Issue Date', 'date', NULL, false);
        $grid->addColumn('AUFNR_LINK', 'Order', 'html', NULL, false);
        $grid->addColumn('KTEXT_LINK', 'Discrepancies', 'html', NULL, false);
        $grid->addColumn('CARD_TYPE', 'Card Type', 'string', array(""=>"","JC" => "JC", "MDR" => "MDR", "AD" => "AD"), NULL, false);
        $grid->addColumn('CUST_JC_NUM', 'Originating Number', 'string', NULL, false);
        $grid->addColumn('STO_NUMBER', 'STO Number', 'string', NULL, $is_edit);
        $grid->addColumn('OUTBOND_DELIV', 'Outbound Delivery', 'string', NULL, $is_edit);
        $grid->addColumn('TO_NUM', 'TO Number', 'string', NULL, $is_edit);
        $grid->addColumn('MENGE', 'Qty Req Single Item', 'string', NULL, $is_edit);
        $grid->addColumn('TOTAL_QTY', 'Total Qty Required for All Job Task', 'string', NULL, false);
        $grid->addColumn('MEINS', 'UOM', 'string', array(""=>"","FT" => "FT", "E/A" => "E/A", "L" => "L", "LB" => "LB", "QT" => "QT", "ROL" => "ROL", "M" => "M"), $is_edit);
        $grid->addColumn('STOCK_MANUAL', 'Input Stock Manually', 'string', NULL, $is_edit);
        $grid->addColumn('LGORT', 'Storage Location', 'string', NULL, $is_edit);
        $grid->addColumn('MATERIAL_FULFILLMENT_STATUS', 'Material Fulfillment Status', 'string', array(""=>"","DLVR FULL to Production" => "DLVR FULL to Production", " DLVR PARTIAL to Production" => "DLVR PARTIAL to Production", "WAITING Customer Supply" => "WAITING Customer Supply", "ROBBING Desicion" => "ROBBING Desicion", "PROVISION in Store" => "PROVISION in Store", "PRELOADED in Hangar Store" => "PRELOADED in Hangar Store", "ORDERED by Purchasing" => "ORDERED by Purchasing", "ORDERED by Loan Control" => "ORDERED by Loan Control", "ORDERED by Workshop" => "ORDERED by Workshop", "EXCHANGE in Progress" => "EXCHANGE in Progress", "Actual STOCK NIL" => "Actual STOCK NIL", "RIC/Part on Receiving Area" => "RIC/Part on Receiving Area", "NO SOURCE" => "NO SOURCE", "Waiting PN Confrimation" => "Waiting PN Confrimation", "GR OK" => "GR OK", "Need FOLLOW UP" => "Need FOLLOW UP", "PENDING/NO ACTION" => "PENDING/NO ACTION", "PR Ready" => "PR Ready", "POOLING" => "POOLING", "Shipment" => "Shipment", "Custom Process" => "Custom Process", "Need SOA" => "Need SOA", "WAITING Customer Approval" => "WAITING Customer Approval"), $is_edit);
        $grid->addColumn('FULLFILLMENT_STATUS_DATE', 'Fullfillment Status Date', 'date', NULL, false);
        $grid->addColumn('FULLFILLMENT_REMARK', 'Fullfillment remark', 'html', NULL, $is_edit);
        $grid->addColumn('PO_DATE', 'Date Of PO', 'date', NULL, $is_edit);
        $grid->addColumn('PO_NUM', 'Purchase Order PO', 'string', NULL, $is_edit);
        $grid->addColumn('AWB_NUMBER', 'AWB Number', 'string', NULL, $is_edit);
        $grid->addColumn('AWB_DATE', 'AWB Date', 'date', NULL, $is_edit);
        $grid->addColumn('QTY_DELIVERED', 'Qty Delivered', 'string', NULL, $is_edit);
        $grid->addColumn('QTY_REMAIN', 'Qty Remain', 'string', NULL, $is_edit);
        $grid->addColumn('MATERIAL_REMARK', 'Material Remark', 'html', NULL, $is_edit);
       
//        $grid->addColumn('AUFNR', 'Order Number', 'html', NULL, false, 'AUFNR');
        if($data['session']['user_group_level'] == 1 || $data['session']['user_group_level'] == 2){
            if ($is_edit) {
                $grid->addColumn('AUFNR_DEL', 'Action', 'html', NULL, false);
            }
        }

        $active_job = $this->Mrm_model->selectRaw('*,'
                . "'<a href=\"#\" onclick=\"longtextswal(' + CAST(AUFNR AS VARCHAR) + ')\">' + KTEXT + '</a>' AS KTEXT_LINK,"
                . "'<a href=\"" . base_url() . "projects/' + (CASE WHEN CARD_TYPE = 'JC' THEN 'crud_jobcard/' ELSE 'crud_mdr/' END) + CAST(REVNR AS VARCHAR) + '/' + CAST(AUFNR AS VARCHAR) + '/MRM\">' + CAST(AUFNR AS VARCHAR) + '</a>' AS AUFNR_LINK")
                ->where('REVNR', $REVNR)
                ->where('IS_ACTIVE', 1)
                ->orderBy('ID', 'ASC');

        if ($this->session->userdata('jc_id') != NULL) {
            $active_job = $active_job->where('AUFNR', $this->session->userdata('jc_id'));
        }

        $totalUnfiltered = $active_job->count();
        $total = $totalUnfiltered;

        $page = 0;
        if (isset($_GET['page']) && is_numeric($_GET['page']))
            $page = (int) $_GET['page'];


        $rowByPage = 100; // $totalUnfiltered; // 50;

        $from = ($page - 1) * $rowByPage;

        if (isset($_GET['filter']) && $_GET['filter'] != "") {
            $filter = $_GET['filter'];
            $active_job = $active_job
                    ->where(function($active_job) use ($filter) {
                $active_job->where('CARD_TYPE', 'like', '%' . $filter . '%')
                ->orWhere('MATNR', 'like', '%' . $filter . '%')
                ->orWhere('ALTERNATE_PART_NUMBER', 'like', '%' . $filter . '%')
                ->orWhere('MAKTX', 'like', '%' . $filter . '%')
                ->orWhere('AUFNR', 'like', '%' . $filter . '%')
                ->orWhere('KTEXT', 'like', '%' . $filter . '%')
                ->orWhere('MTART', 'like', '%' . $filter . '%')
                ->orWhere('IPC', 'like', '%' . $filter . '%')
                ->orWhere('STO_NUMBER', 'like', '%' . $filter . '%')
                ->orWhere('OUTBOND_DELIV', 'like', '%' . $filter . '%')
                ->orWhere('TO_NUM', 'like', '%' . $filter . '%')
                ->orWhere('CUST_JC_NUM', 'like', '%' . $filter . '%')
                ->orWhere('MRM_ISSUE_DATE', 'like', '%' . $filter . '%')
                ->orWhere('MENGE', 'like', '%' . $filter . '%')
                ->orWhere('TOTAL_QTY', 'like', '%' . $filter . '%')
                ->orWhere('MEINS', 'like', '%' . $filter . '%')
                ->orWhere('STOCK_MANUAL', 'like', '%' . $filter . '%')
                ->orWhere('LGORT', 'like', '%' . $filter . '%')
                ->orWhere('MATERIAL_FULFILLMENT_STATUS', 'like', '%' . $filter . '%')
                ->orWhere('FULLFILLMENT_STATUS_DATE', 'like', '%' . $filter . '%')
                ->orWhere('FULLFILLMENT_REMARK', 'like', '%' . $filter . '%')
                ->orWhere('PO_DATE', 'like', '%' . $filter . '%')
                ->orWhere('PO_NUM', 'like', '%' . $filter . '%')
                ->orWhere('AWB_NUMBER', 'like', '%' . $filter . '%')
                ->orWhere('AWB_DATE', 'like', '%' . $filter . '%')
                ->orWhere('QTY_DELIVERED', 'like', '%' . $filter . '%')
                ->orWhere('QTY_REMAIN', 'like', '%' . $filter . '%')
                ->orWhere('MATERIAL_REMARK', 'like', '%' . $filter . '%');
            });
            $total = $active_job->count();
        }

        if (isset($_GET['sort']) && $_GET['sort'] != "") {
            $ORDER = $_GET['sort'] . ($_GET['asc'] == "0" ? " DESC" : " ASC");
        } else {
            $ORDER = 'SEQ_NUM' . ' ASC';
        }

        
        $active_job = $active_job->skip($from)->take($rowByPage);
        
        if ($total > 0 && $rowByPage > 0) {
            $grid->setPaginator(ceil($total / $rowByPage), (int) $total, (int) $totalUnfiltered, null);
        }

        $active_job = $this->Proc_model->get_mrm($REVNR, $ORDER, $this->session->userdata('jc_id'), $_GET['filter'], $from, $rowByPage);
        $grid->renderJSON($active_job, false, false, true);
    }


    public function crud_mrm_add() {
        $response["status"] = NULL;
        $response["body"] = NULL;

        date_default_timezone_set('Asia/Jakarta');
        $last_id = $this->Mrm_model->where('REVNR', $this->session->userdata('REVNR'))->max('SEQ_NUM') + 1;

        try {
            $mrm = $this->Mrm_model;
            $mrm->SEQ_NUM = $last_id;
            $mrm->REVNR = $this->session->userdata('REVNR');
            $mrm->CARD_TYPE = $_POST['CARD_TYPE'];
            $mrm->MATNR = $_POST['MATNR'];
            $mrm->ALTERNATE_PART_NUMBER = $_POST['ALT_PART_NUMBER'];
            $mrm->MAKTX = $_POST['MAKTX'];
            $mrm->AUFNR = $_POST['AUFNR'];
            $mrm->KTEXT = $_POST['KTEXT'];
            $mrm->MTART = $_POST['CTG'];
            $mrm->IPC = $_POST['IPC'];
            $mrm->STO_NUMBER = $_POST['STO_NUMBER'];
            $mrm->OUTBOND_DELIV = $_POST['OUTBOND_DELIV'];
            $mrm->TO_NUM = $_POST['TO_NUM'];
            $mrm->CUST_JC_NUM = $_POST['CUST_JC_NUM'];
            $mrm->MRM_ISSUE_DATE = date('Y-m-d H:i:s');
            $mrm->MENGE = $_POST['MENGE'];
            // $mrm->TOTAL_QTY = $_POST['TOTAL_QTY']; by System
            $mrm->MEINS = $_POST['MEINS'];
            $mrm->STOCK_MANUAL = $_POST['STOCK_MANUAL'];
            $mrm->LGORT = $_POST['LGORT'];
            $mrm->MATERIAL_FULFILLMENT_STATUS = $_POST['MATERIAL_FULFILLMENT_STATUS'];
            $mrm->FULLFILLMENT_STATUS_DATE = date('Y-m-d H:i:s');
            $mrm->FULLFILLMENT_REMARK = $_POST['FULLFILLMENT_REMARK'];
            $mrm->PO_DATE = !empty($_POST['PO_DATE']) ? $_POST['PO_DATE'] : null;
            $mrm->PO_NUM = $_POST['PO_NUM'];
            $mrm->AWB_NUMBER = $_POST['AWB_NUMBER'];
            $mrm->AWB_DATE = !empty($_POST['AWB_DATE']) ? $_POST['AWB_DATE'] : null;
            $mrm->QTY_DELIVERED = $_POST['QTY_DELIVERED'];
            $mrm->QTY_REMAIN = $_POST['QTY_REMAIN'];
            $mrm->MATERIAL_REMARK = $_POST['MATERIAL_REMARK'];
            $mrm->IS_ACTIVE = 1;
            $mrm->save();
            if ($mrm) {
                $mrm
                        ->where('MATNR', $mrm->first()->MATNR)
                        ->where('REVNR', $mrm->first()->REVNR)
                        ->update([
                        'TOTAL_QTY' => $this->Mrm_model
                            ->selectRaw('SUM(CAST(MENGE AS FLOAT)) AS MENGE')
                            ->where('MATNR', $mrm->first()->MATNR)
                            ->where('REVNR', $mrm->first()->REVNR)
                            ->first()
                            ->MENGE
                            // Mungkin bermasalah, peraiki
                    ]);
                $response["status"] = "success";
                $response["body"] = NULL;
            } else {
                $response["status"] = "error";
                $response["body"] = NULL;
            }
        } catch (Exception $e) {
            error_log($e);
            echo $e;
        }

        header('Content-Type: application/json');
        echo json_encode($response);
    }

    function crud_mrm_after_insert($post_array, $primary_key) {
        date_default_timezone_set('Asia/Jakarta');
        $mrm_update = array(
            "id_project" => $this->session->userdata('id_project'),
            'date_created' => date('Y-m-d H:i:s'),
            'MRM_Issue_Date' => date('Y-m-d H:i:s')
        );
        $this->db->update('mrm', $mrm_update, array('id' => $primary_key));
        return true;
    }

    function crud_mrm_no_add_field_callback() {
        error_reporting(E_ALL & ~E_NOTICE);
        $last_no = 0;
        try {
            $query = $this->Mrm_model
                            ->where('id_project', $this->session->userdata('id_project'))
                            ->orderBy('no_', 'DESC')
                            ->first()->no_;
            // $last = $query->seq;
            $last_no = $query + 1;
        } catch (Exception $e) {
            $last_no = 1;
        }
        return '<input type="text" value="' . $last_no . '" name="no_">';
    }

    public function crud_mrm_update() {
        $data["session"] = $this->session->userdata('logged_in');
        $id_project = $this->input->get('REVNR');

        $colname = strip_tags($_POST['colname']);
        $id = strip_tags($_POST['id']);
        $coltype = strip_tags($_POST['coltype']);
        $value = strip_tags($_POST['newvalue']);
        

        if ($coltype == 'date') {
            if ($value === "") {
                $value = NULL;
            } else {
                $date_info = date_parse_from_format('d/m/Y', $value);
                $value = "{$date_info['year']}-{$date_info['month']}-{$date_info['day']}";
            }
        }

        try {
            $mrm = $this->Mrm_model->where('ID', $id)->where('REVNR', $id_project);
            if ($colname == 'MENGE') {
                $mrm->update([
                        $colname => $value
                    ]);
                $mrm
                        ->where('MATNR', $mrm->first()->MATNR)
                        ->where('REVNR', $mrm->first()->REVNR)
                        ->update([
                        'TOTAL_QTY' => $this->Mrm_model
                            ->selectRaw('SUM(CAST(MENGE AS FLOAT)) AS MENGE')
                            ->where('MATNR', $mrm->first()->MATNR)
                            ->where('REVNR', $mrm->first()->REVNR)
                            ->first()
                            ->MENGE
                            // Mungkin bermasalah, peraiki
                    ]);  
            } else {
                $mrm->update([$colname => $value]);
            }

            if($mrm){
                echo "ok";

                if ($colname == 'MATERIAL_FULFILLMENT_STATUS') {

                    $this->setOrderStatus($id, $id_project);
    
                    date_default_timezone_set('Asia/Jakarta');
                    $this->Mrm_model
                            ->where('ID', $id)
                            ->update(['FULLFILLMENT_STATUS_DATE' => date('Y-m-d H:i:s')]);
    
                    if ($value == 'WAITING Customer Supply') {
                        $this->insert_csr_after_mrm_updated($id, $id_project);
                    } else {
                        $this->remove_csr_by_mrm_id($id, $id_project);
                    }
                }
            }else{
                echo "error";
            }
        } catch (Exception $exc) {
            echo $exc;
        } finally {
            
        }
    }

    public function setOrderStatus($mrm_id, $id_project) {
        date_default_timezone_set('Asia/Jakarta');
        try {
            $mrm = $this->Mrm_model->where('SEQ_NUM', $mrm_id)->where('REVNR', $id_project)->first();
            // $mrm = $this->Mrm_model->where('NO_', $mrm_id)->where('REVNR', $id_project)->first();
            $AUFNR = $mrm->AUFNR;
            $mrm = $this->Mrm_model->select("MATERIAL_FULFILLMENT_STATUS")->where('AUFNR', $AUFNR)->get()->toArray();
            $mrm_status = array_column($mrm, 'MATERIAL_FULFILLMENT_STATUS');
            $mrm_unique_status = array_unique($mrm_status);
            $COMPLETED = array("DLVR FULL to Production", "PRELOADED in Hangar Store");
            $order = $this->Jobcard_model->where('AUFNR', $AUFNR);

            
            if ( count($mrm_unique_status) == 1 ) {
                if( 
                    in_array('DLVR FULL to Production', array_unique($mrm_unique_status)) || 
                    in_array('PRELOADED in Hangar Store', array_unique($mrm_unique_status))
                ) {
                    $order->update([
                        'MAT_FULLFILLMENT_STATUS' => 'COMPLETED',
                        'FULLFILLMENT_STATUS_DATE' => date('Y-m-d H:i:s')
                    ]);
                } else {
                    $order->update([
                        'MAT_FULLFILLMENT_STATUS' => $mrm_unique_status[0],
                        'FULLFILLMENT_STATUS_DATE' => date('Y-m-d H:i:s')
                    ]);
                }
            } elseif ( count($mrm_unique_status) > 1 ) {
                if(count(array_diff($mrm_unique_status, $COMPLETED)) == 0){
                    $order->update([
                        'MAT_FULLFILLMENT_STATUS' => 'COMPLETED',
                        'FULLFILLMENT_STATUS_DATE' => date('Y-m-d H:i:s')
                    ]);
                } elseif( 
                    in_array('DLVR FULL to Production', array_unique($mrm_unique_status)) ||
                    in_array('PRELOADED in Hangar Store', array_unique($mrm_unique_status)) 
                    ) {
                    $order->update([
                        'MAT_FULLFILLMENT_STATUS' => 'PARTIAL',
                        'FULLFILLMENT_STATUS_DATE' => date('Y-m-d H:i:s')
                    ]);
                } else {
                    $order->update([
                        'MAT_FULLFILLMENT_STATUS' => 'NOT COMPLETE',
                        'FULLFILLMENT_STATUS_DATE' => date('Y-m-d H:i:s')
                    ]);
                }
            }

            // if ($order) {
            //     echo "success";
            // } else {
            //     echo "error";
            // }

            
        } catch (Exception $e) {
            echo $e;
        }
    }

    public function crud_mrm_delete() {
        try {
            if ($this->uri->segment(4)) {
                $this->session->set_userdata('id_project', $this->uri->segment(4));
            }
            $data['id_project'] = $this->session->userdata('id_project');
            $active_prj = $this->VProject_model->find($data['id_project']);
            if ($active_prj) {
                $data['project_name'] = $active_prj->project_name;
            }
        } catch (Exception $e) {
            error_log($e);
        }

        $data["session"] = $this->session->userdata('logged_in');

        $id = strip_tags($_POST['ID']);
        $REVNR = strip_tags($_POST['REVNR']);
        // $tablename = strip_tags($_POST['tablename']);

        try {
            $this->Mrm_model->destroy($id);
        } catch (Exception $exc) {
            echo "error";
        } finally {
            echo "ok";
        }
    }

    public function crud_mrm_export()
    {
        $REVNR = $this->input->get('REVNR');
        $AUFNR = $this->session->userdata('jc_id');
        $data_mrm_all = $this->Proc_model->get_mrm_all($REVNR, $AUFNR);

        $jobcard_list = array();

        foreach($data_mrm_all as $value){
            array_push(
                $jobcard_list, 
                array(
                     $value->ID,
                     $value->NO_,
                     $value->REVNR,
                     $value->AUFNR_LINK,
                     $value->CARD_TYPE,
                     $value->MATNR,
                     $value->ALTERNATE_PART_NUMBER,
                     $value->MAKTX,
                     $value->KTEXT_LINK,
                     $value->MTART,
                     $value->IPC,
                     $value->STO_NUMBER,
                     $value->OUTBOND_DELIV,
                     $value->TO_NUM,
                     $value->CUST_JC_NUM,
                     $value->MRM_ISSUE_DATE,
                     $value->MENGE,
                     $value->TOTAL_QTY,
                     $value->MEINS,
                     $value->STOCK_MANUAL,
                     $value->LGORT,
                     $value->MATERIAL_FULFILLMENT_STATUS,
                     $value->FULLFILLMENT_STATUS_DATE,
                     $value->FULLFILLMENT_REMARK,
                     $value->PO_DATE,
                     $value->PO_NUM,
                     $value->AWB_NUMBER,
                     $value->AWB_DATE,
                     $value->QTY_DELIVERED,
                     $value->QTY_REMAIN,
                     $value->MATERIAL_REMARK,
                ));
        }

        $writer = WriterFactory::create(Type::XLSX);
        $default_style = (new StyleBuilder())
                ->setFontName('Arial')
                ->setFontSize(10)
                ->setShouldWrapText(false)
                ->build();          
               
        
        $writer->setDefaultRowStyle($default_style);
        

        $file_name = "MRM_" . $REVNR . "_" . date('Ymd') . ".xlsx";
        $writer->openToFile($file_name); 

        $data_header = array();
        $data_header[] = "ID";
        $data_header[] = "RSPOS";
        $data_header[] = "Revision";
        $data_header[] = "Order";
        $data_header[] = "Card Type";
        $data_header[] = "Part Number";
        $data_header[] = "Alternate Part Number";
        $data_header[] = "Material Description";
        $data_header[] = "Discrepancies";
        $data_header[] = "Mat Type";
        $data_header[] = "IPC";
        $data_header[] = "STO Number";
        $data_header[] = "Outbound Delivery";
        $data_header[] = "TO Number";
        $data_header[] = "Jobcard Number";
        $data_header[] = "MRM Issue Date";
        $data_header[] = "Qty Req Single Item";
        $data_header[] = "Total Qty Required for All Job Task";
        $data_header[] = "UOM";
        $data_header[] = "Input Stock Manually";
        $data_header[] = "Storage Location";
        $data_header[] = "Material Fulfillment Status";
        $data_header[] = "Fullfillment Status Date";
        $data_header[] = "Fullfillment remark";
        $data_header[] = "Date Of PO";
        $data_header[] = "Purchase Order PO";
        $data_header[] = "AWB Number";
        $data_header[] = "AWB Date";
        $data_header[] = "Qty Delivered";
        $data_header[] = "Qty Remain";
        $data_header[] = "Material Remark";


        $query_header = array();
        $query_header[] = "SEQ_NUM";
        $query_header[] = "NO_";
        $query_header[] = "REVNR";
        $query_header[] = "AUFNR";
        $query_header[] = "CARD_TYPE";
        $query_header[] = "MATNR";
        $query_header[] = "ALTERNATE_PART_NUMBER";
        $query_header[] = "MAKTX";
        $query_header[] = "KTEXT";
        $query_header[] = "MTART";
        $query_header[] = "IPC";
        $query_header[] = "STO_NUMBER";
        $query_header[] = "OUTBOND_DELIV";
        $query_header[] = "TO_NUM";
        $query_header[] = "ORIG_NUMBER";
        $query_header[] = "MRM_ISSUE_DATE";
        $query_header[] = "MENGE";
        $query_header[] = "TOTAL_QTY";
        $query_header[] = "MEINS";
        $query_header[] = "STOCK_MANUAL";
        $query_header[] = "LGORT";
        $query_header[] = "MATERIAL_FULFILLMENT_STATUS";
        $query_header[] = "FULLFILLMENT_STATUS_DATE";
        $query_header[] = "FULLFILLMENT_REMARK";
        $query_header[] = "PO_DATE";
        $query_header[] = "PO_NUM";
        $query_header[] = "AWB_NUMBER";
        $query_header[] = "AWB_DATE";
        $query_header[] = "QTY_DELIVERED";
        $query_header[] = "QTY_REMAIN";
        $query_header[] = "MATERIAL_REMARK";

        $writer->addRow($data_header);
        $writer->addRow($query_header);


        $writer->addRows($jobcard_list);
        $writer->close();

        $this->load->helper('download');
        force_download($file_name, null);
    }

    public function crud_prm() {
        try {
            $data['id_project'] = $this->uri->segment(3) ? $this->uri->segment(3) : '0';
            $active_prj = $this->Project_model_ci->getProjectName($data['id_project']);
            $data['project_name'] = $active_prj ? $active_prj->REVTX : "";  
        } catch (Exception $e) {
            error_log($e);
        }

        $data['write'] = $this->is_write;
        $data["session"] = $this->session->userdata('logged_in');
        $data['content'] = 'admin/projects/crud_prm';

        $this->load->view('template', $data);
    }

    public function write_xlsx_file_prm()
    {
        $REVNR = $_GET['REVNR'];
        $active_job = $this->Prm_model
                ->selectRaw('ROW_NUMBER () OVER (ORDER BY ID ) AS SEQ_NUM, *')           
                ->where('REVNR', $REVNR)
                ->where('IS_ACTIVE', 1);
        $csp = $active_job->get();
        $csp_list = array();
        foreach ($csp as $value) {
            array_push(
                $csp_list, 
                array(
                    $value->SEQ_NUM, 
                    $value->PART_NUMBER, 
                    $value->DESCRIPTION,
                    $value->CTG,
                    $value->SN_QTY,
                    $value->POST,
                    $value->AUFNR,
                    $value->PART_STATUS,
                    $value->PL_SP_OUT_NO,
                    $value->PL_SP_OUT_DATE,
                    $value->PL_SP_IN_NO,
                    $value->PL_SP_IN_DATE,
                    $value->UNIT_SEND,
                    $value->UNIT_RECV,
                    $value->PART_LOC,
                    $value->REMARK,
                )
            );
        }

        $writer = WriterFactory::create(Type::XLSX);
        $default_style = (new StyleBuilder())
                ->setFontName('Arial')
                ->setFontSize(10)
                ->setShouldWrapText(false)
                ->build();
        $writer->setDefaultRowStyle($default_style);

        $file_name = "PRM_" . $REVNR . "_" . date('YmdhIs') . ".xlsx";
        $writer->openToFile($file_name); 

        $data_header = array();
        $data_header[] = "No";
        $data_header[] = "Part Number";
        $data_header[] = "Description";
        $data_header[] = "CTG";
        $data_header[] = "Serial / Qty";
        $data_header[] = "Pos";
        $data_header[] = "Order Reff";
        $data_header[] = "Part Status";
        $data_header[] = "P/L or SP (Out) No";
        $data_header[] = "P/L or SP (Out) Date";
        $data_header[] = "P/L or SP (In) No";
        $data_header[] = "P/L or SP (In) Date";
        $data_header[] = "Unit Send";
        $data_header[] = "Unit Recv.";
        $data_header[] = "Part Location";
        $data_header[] = "Remark";

        $writer->addRow($data_header);
        $writer->addRows($csp_list);
        $writer->close();
 
        $this->load->helper('download');
        force_download($file_name, null);
    }

    public function check_duplicate_prm() {
        $response["status"] = NULL;
        $response["body"] = NULL;
        try {
            $crm = $this->Prm_model
                    ->where('AUFNR', $_POST['AUFNR'])
                    ->where('SN_QTY', $_POST['SN_QTY'])
                    ->where('REVNR', $_POST['REVNR'])
                    ->where('IS_ACTIVE', 1)
                    ->count();

            if ($crm > 0) {
                $response["status"] = 'error';
            } else {
                $response["status"] = 'success';
            }
        } catch (Exception $e) {
            $response["status"] = 'failed';
            $response["body"] = $e;
        }
        header('Content-Type: application/json');
        echo json_encode($response);
    }

    public function crud_prm_load() {
        $is_edit = $_GET['edit'] == 1 ? true : false;
        $data["session"] = $this->session->userdata('logged_in');
        $REVNR = $this->input->get('REVNR');

        $grid = new EditableGrid();

        $grid->addColumn('SEQ_NUM', 'No', 'integer', NULL, false);
        $grid->addColumn('PART_NUMBER', 'Part Number', 'string', NULL, false);
        $grid->addColumn('DESCRIPTION', 'Description', 'html', NULL, $is_edit);
        $grid->addColumn('CTG', 'CTG', 'string', NULL, false);
        $grid->addColumn('SN_QTY', 'SN/Qty', 'string', NULL, false);
        $grid->addColumn('POS', 'Post', 'string', NULL, $is_edit);
        $grid->addColumn('AUFNR_LINK', 'ORDER REFF', 'html', NULL, false);
        $grid->addColumn('PART_STATUS', 'Part Status', 'string', NULL, $is_edit);
        $grid->addColumn('PL_SP_OUT_NO', 'P/L or SP (Out) No', 'string', NULL, $is_edit);
        $grid->addColumn('PL_SP_OUT_DATE', 'P/L or SP (Out) Date', 'date', NULL, $is_edit);
        $grid->addColumn('PL_SP_IN_NO', 'P/L or SP (In) No', 'string', NULL, $is_edit);
        $grid->addColumn('PL_SP_IN_DATE', 'P/L or SP (In) Date', 'date', NULL, $is_edit);
        $grid->addColumn('UNIT_SEND', 'Unit Send', 'string', NULL, $is_edit);
        $grid->addColumn('UNIT_RECV', 'Unit Recv.', 'string', NULL, $is_edit);
        $grid->addColumn('PART_LOC', 'Part Location.', 'string', NULL, $is_edit);
        $grid->addColumn('REMARK', 'Remark', 'html', NULL, $is_edit);
        if($is_edit){
            $grid->addColumn('action', 'Action', 'html', NULL, false, 'ID');
        }
        $active_job = $this->Prm_model->selectRaw('ROW_NUMBER () OVER (ORDER BY ID ) AS SEQ_NUM, *,'
                        . "CASE WHEN CARD_TYPE LIKE 'MDR' THEN"
                        . "'<a href=\"" . base_url() . "projects/crud_mdr/' + REVNR + '/' + AUFNR + '/PRM\">' + AUFNR + '</a>'"
                        . "WHEN CARD_TYPE LIKE 'JC' THEN"
                        . "'<a href=\"" . base_url() . "projects/crud_jobcard/' + REVNR + '/' + AUFNR + '/PRM\">' + AUFNR + '</a>' END AS AUFNR_LINK"
                )
                ->where('REVNR', $REVNR)
                ->where('IS_ACTIVE', 1);

        $totalUnfiltered = $active_job->count();
        $total = $totalUnfiltered;

        $page = 0;
        if (isset($_GET['page']) && is_numeric($_GET['page']))
            $page = (int) $_GET['page'];


        $rowByPage = $totalUnfiltered; // 50;

        $from = ($page - 1) * $rowByPage;

        if (isset($_GET['filter']) && $_GET['filter'] != "") {
            $filter = $_GET['filter'];
            $active_job = $active_job
                    ->where(function($active_job) use($filter){
                        $active_job->where('PART_NUMBER', 'like', '%' . $filter . '%')
                        ->orwhere('DESCRIPTION', 'like', '%' . $filter . '%')
                        ->orwhere('CTG', 'like', '%' . $filter . '%')
                        ->orwhere('SN_QTY', 'like', '%' . $filter . '%')
                        ->orwhere('POST', 'like', '%' . $filter . '%')
                        ->orwhere('AUFNR', 'like', '%' . $filter . '%')
                        ->orwhere('PART_STATUS', 'like', '%' . $filter . '%')
                        ->orwhere('PL_SP_OUT_NO', 'like', '%' . $filter . '%')
                        ->orwhere('PL_SP_OUT_DATE', 'like', '%' . $filter . '%')
                        ->orwhere('PL_SP_IN_NO', 'like', '%' . $filter . '%')
                        ->orwhere('PL_SP_IN_DATE', 'like', '%' . $filter . '%')
                        ->orwhere('UNIT_SEND', 'like', '%' . $filter . '%')
                        ->orwhere('UNIT_RECV    ', 'like', '%' . $filter . '%')
                        ->orwhere('PART_LOC', 'like', '%' . $filter . '%')
                        ->orwhere('REMARK', 'like', '%' . $filter . '%');
                    });
            $total = $active_job->count();
        }

        if (isset($_GET['sort']) && $_GET['sort'] != "" && in_array($_GET['sort'], $grid->getColumnFields())) {
            $active_job = $active_job->orderBy($_GET['sort'], $_GET['asc'] == "0" ? "DESC" : "ASC");
        } else {
            $active_job = $active_job->orderBy('NO_', 'ASC');
        }

        $active_job = $active_job->skip($from)->take($rowByPage);

        if ($total > 0) {
            $grid->setPaginator(ceil($total / $rowByPage), (int) $total, (int) $totalUnfiltered, null);
        }

        $grid->renderJSON($active_job->get(), false, false, !isset($_GET['data_only']));
    }

    public function crud_prm_add() {
        date_default_timezone_set('Asia/Jakarta');

        $msg["status"] = NULL;
        $msg["body"] = NULL;

        $ORDER = $_POST['AUFNR'];
        $SERIAL_NUMBER = $_POST['SN_QTY'];
        $REVNR = $_POST['REVNR'];

        try {
            $prm = $this->Prm_model->updateOrCreate(
                    ['AUFNR' => $ORDER, 'SN_QTY' => $SERIAL_NUMBER, 'REVNR' => $REVNR], 
                    [
                        'REVNR' => $REVNR,
                        'PART_NUMBER' => $_POST['MATNR'],
                        'DESCRIPTION' => $_POST['MAKTX'],
                        'CTG' => $_POST['CTG'],
                        'SN_QTY' => $_POST['SN_QTY'],
                        'POST' => $_POST['POST'],
                        'PL_SP_OUT_NO' => $_POST['SP_OUT_NO'],
                        'PL_SP_OUT_DATE' => !empty($_POST['SP_OUT_DATE']) ? $_POST['SP_OUT_DATE'] : null,
                        'PL_SP_IN_NO' => $_POST['SP_IN_NO'],
                        'PL_SP_IN_DATE' => !empty($_POST['SP_IN_DATE']) ? $_POST['SP_IN_DATE'] : null,
                        'UNIT_SEND' => $_POST['UNIT_SEND'],
                        'UNIT_RECV' => $_POST['UNIT_RECV'],
                        'PART_LOC' => $_POST['PART_LOC'],
                        'REMARK' => $_POST['REMARK'],
                        'PART_STATUS' => $_POST['STATUS'],
                        'AUFNR' => $_POST['AUFNR'],
                        'CARD_TYPE' => $_POST['CARD_TYPE'],
                        'IS_ACTIVE' => 1
                    ]
            );

            if ($prm) {
                $msg["status"] = "success";
                $msg["body"] = "Prm has been saved";
            } else {
                $msg["status"] = "error";
                $msg["body"] = "Prm has been saved";
            }
        } catch (Exception $e) {
            $msg["status"] = "failed";
            $msg["body"] = $e;
        }

        header('Content-Type: application/json');
        echo json_encode($msg);
    }

    function crud_prm_after_insert($post_array, $primary_key) {
        date_default_timezone_set('Asia/Jakarta');
        $prm_update = array(
            "REVNR" => $this->session->userdata('REVNR'),
            'DATE_CREATED' => date('Y-m-d H:i:s')
        );
        $this->db->update('PRM', $prm_update, array('ID' => $primary_key));
        return true;
    }

    function crud_prm_no_add_field_callback() {
        error_reporting(E_ALL & ~E_NOTICE);
        $last_no = 0;
        try {
            $query = $this->Prm_model
                            ->where('REVNR', $this->session->userdata('REVNR'))
                            ->orderBy('NO_', 'DESC')
                            ->first()->NO_;
            // $last = $query->seq;
            $last_no = $query + 1;
        } catch (Exception $e) {
            error_log($e);
            $last_no = 1;
        }
        return '<input type="text" value="' . $last_no . '" name="NO_">';
    }

    public function crud_prm_update() {
        try {
            if ($this->uri->segment(4)) {
                $this->session->set_userdata('id_project', $this->uri->segment(4));
            }
            $data['id_project'] = $this->session->userdata('REVNR');
            $active_prj = $this->VProject_model->find($data['id_project']);
            if ($active_prj) {
                $data['project_name'] = $active_prj->REVTX;
            }
        } catch (Exception $e) {
            error_log($e);
        }

        $data["session"] = $this->session->userdata('logged_in');

        $colname = strip_tags($_POST['colname']);
        $id = strip_tags($_POST['id']);
        $coltype = strip_tags($_POST['coltype']);
        $value = strip_tags($_POST['newvalue']);
        // $tablename = strip_tags($_POST['tablename']);

        if ($coltype == 'date') {
            if ($value === "") {
                $value = NULL;
            } else {
                $date_info = date_parse_from_format('d/m/Y', $value);
                $value = "{$date_info['year']}-{$date_info['month']}-{$date_info['day']}";
            }
        }

        try {
            $this->Prm_model->where('ID', $id)->update([$colname => $value]);
        } catch (Exception $exc) {
            echo "error";
        } finally {
            echo "ok";
        }
    }

    public function crud_prm_delete() {
        try {
            if ($this->uri->segment(4)) {
                $this->session->set_userdata('id_project', $this->uri->segment(4));
            }
            $data['id_project'] = $this->session->userdata('REVNR');
            $active_prj = $this->VProject_model->find($data['id_project']);
            if ($active_prj) {
                $data['project_name'] = $active_prj->REVTX;
            }
        } catch (Exception $e) {
            error_log($e);
        }

        $data["session"] = $this->session->userdata('logged_in');

        $id = strip_tags($_POST['id']);
        // $tablename = strip_tags($_POST['tablename']);

        try {
            $this->Prm_model->destroy($id);
        } catch (Exception $exc) {
            echo "error";
        } finally {
            echo "ok";
        }
    }

    public function crud_crm() {
        try {
            $data['id_project'] = $this->uri->segment(3) ? $this->uri->segment(3) : '0';
            $active_prj = $this->Project_model_ci->getProjectName($data['id_project']);
            $data['project_name'] = $active_prj ? $active_prj->REVTX : "";  
        } catch (Exception $e) {
            error_log($e);
        }

        $data['write'] = $this->is_write;
        $data["session"] = $this->session->userdata('logged_in');
        $data['content'] = 'admin/projects/crud_crm';

        $this->load->view('template', $data);
    }

    public function write_xlsx_file_crm()
    {
        $REVNR = $_GET['REVNR'];
        $active_job = $this->Crm_model
                ->selectRaw('ROW_NUMBER () OVER (ORDER BY ID ) AS SEQ_NUM, *')           
                ->where('REVNR', $REVNR)
                ->where('IS_ACTIVE', 1);
        $csp = $active_job->get();
        $csp_list = array();
        foreach ($csp as $value) {
            array_push(
                $csp_list, 
                array(
                    $value->SEQ_NUM, 
                    $value->PART_NUMBER, 
                    $value->DESCRIPTION,
                    $value->CTG,
                    $value->SN_QTY,
                    $value->POST,
                    $value->REFF,
                    $value->SP_OUT_NO,
                    $value->SP_OUT_DATE,
                    $value->SP_IN_NO,
                    $value->SP_IN_DATE,
                    $value->UNIT_SEND,
                    $value->UNIT_RECV,
                    $value->PART_LOC,
                    $value->REMARK,
                    $value->STATUS,
                )
            );
        }

        $writer = WriterFactory::create(Type::XLSX);
        $default_style = (new StyleBuilder())
                ->setFontName('Arial')
                ->setFontSize(10)
                ->setShouldWrapText(false)
                ->build();
        $writer->setDefaultRowStyle($default_style);

        $file_name = "CRM_" . $REVNR . "_" . date('YmdhIs') . ".xlsx";
        $writer->openToFile($file_name); 

        $data_header = array();
        $data_header[] = "No";
        $data_header[] = "Part Number";
        $data_header[] = "Description";
        $data_header[] = "CTG";
        $data_header[] = "Serial No";
        $data_header[] = "Pos";
        $data_header[] = "Order Number";
        $data_header[] = "SP (Out) No";
        $data_header[] = "SP (Out) Date";
        $data_header[] = "SP (In) No";
        $data_header[] = "SP (In) Date";
        $data_header[] = "Unit Send";
        $data_header[] = "Unit Recv.";
        $data_header[] = "Part Location";
        $data_header[] = "Remark";
        $data_header[] = "Status";

        $writer->addRow($data_header);
        $writer->addRows($csp_list);
        $writer->close();

        $this->load->helper('download');
        force_download($file_name, null);
    }

    public function check_duplicate_crm() {
        $response["status"] = NULL;
        $response["body"] = NULL;
        try {
            $crm = $this->Crm_model
                    ->where('REFF', $_POST['AUFNR'])
                    ->where('SN_QTY', $_POST['SN_QTY'])
                    ->where('REVNR', $_POST['REVNR'])
                    ->where('IS_ACTIVE', 1)
                    ->count();

            if ($crm > 0) {
                $response["status"] = 'error';
            } else {
                $response["status"] = 'success';
            }
        } catch (Exception $e) {
            $response["status"] = 'failed';
            $response["body"] = $e;
        }
        header('Content-Type: application/json');
        echo json_encode($response);
    }

    public function crud_crm_load() {
        $is_edit = $_GET['edit'] == 1 ? true : false;
        $data["session"] = $this->session->userdata('logged_in');
        $REVNR = $this->input->get('REVNR');

        $grid = new EditableGrid();

        $grid->addColumn('SEQ_NUM', 'No', 'integer', NULL, false);
        $grid->addColumn('PART_NUMBER', 'Part Number', 'string', NULL, false);
        $grid->addColumn('DESCRIPTION', 'Description', 'string', NULL, $is_edit);
        $grid->addColumn('CTG', 'CTG', 'string', NULL, $is_edit);
        $grid->addColumn('SN_QTY', 'SN', 'string', NULL, false);
        $grid->addColumn('POST', 'Pos', 'string', NULL, $is_edit);
        $grid->addColumn('AUFNR_LINK', 'Order', 'html', NULL, false);
        $grid->addColumn('SP_OUT_NO', 'SP (Out) No', 'string', NULL, $is_edit);
        $grid->addColumn('SP_OUT_DATE', 'SP (Out) Date', 'date', NULL, $is_edit);
        $grid->addColumn('SP_IN_NO', 'SP (In) No', 'string', NULL, $is_edit);
        $grid->addColumn('SP_IN_DATE', 'SP (In) Date', 'date', NULL, $is_edit);
        $grid->addColumn('UNIT_SEND', 'Unit Send', 'string', NULL, $is_edit);
        $grid->addColumn('UNIT_RECV', 'Unit Recv.', 'string', NULL, $is_edit);
        $grid->addColumn('PART_LOC', 'Part Location.', 'string', NULL, $is_edit);
        $grid->addColumn('REMARK', 'Remark', 'string', NULL, $is_edit);
        $grid->addColumn('STATUS', 'Status', 'string', array(""=>"","UR - Und. repr" => "UR - Und. repr", "SB - Serviceabl" => "SB - Serviceabl", "RA - Return as is" => "RA - Return as is", "CR - Sent to Contracr" => "CR - Sent to Contracr", "C - Close" => "C - Close"), $is_edit);
        
        if($is_edit){
            $grid->addColumn('action', 'Action', 'html', NULL, false, 'ID');
        }

        $active_job = $this->Crm_model
            ->selectRaw('ROW_NUMBER () OVER (ORDER BY ID ) AS SEQ_NUM, *,'
            . "CASE WHEN CARD_TYPE LIKE 'MDR' THEN"
            . "'<a href=\"" . base_url() . "projects/crud_mdr/' + REVNR + '/' + REFF + '/CRM\">' + REFF + '</a>'"
            . "WHEN CARD_TYPE LIKE 'JC' THEN"
            . "'<a href=\"" . base_url() . "projects/crud_jobcard/' + REVNR + '/' + REFF + '/CRM\">' + REFF + '</a>' END AS AUFNR_LINK")           
            ->where('REVNR', $REVNR)
            ->where('IS_ACTIVE', 1);

        $totalUnfiltered = $active_job->count();
        $total = $totalUnfiltered;

        $page = 0;
        if (isset($_GET['page']) && is_numeric($_GET['page']))
            $page = (int) $_GET['page'];


        $rowByPage = $totalUnfiltered; // 50;

        $from = ($page - 1) * $rowByPage;

        if (isset($_GET['filter']) && $_GET['filter'] != "") {
            $filter = $_GET['filter'];
            $active_job = $active_job
                    ->where(function($active_job) use($filter){
                        $active_job->where('PART_NUMBER', 'like', '%' . $filter . '%')
                        ->orwhere('DESCRIPTION', 'like', '%' . $filter . '%')
                        ->orwhere('CTG', 'like', '%' . $filter . '%')
                        ->orwhere('SN_QTY', 'like', '%' . $filter . '%')
                        ->orwhere('POST', 'like', '%' . $filter . '%')
                        ->orwhere('REFF', 'like', '%' . $filter . '%')
                        ->orwhere('SP_OUT_NO', 'like', '%' . $filter . '%')
                        ->orwhere('SP_OUT_DATE', 'like', '%' . $filter . '%')
                        ->orwhere('SP_IN_NO', 'like', '%' . $filter . '%')
                        ->orwhere('SP_IN_DATE', 'like', '%' . $filter . '%')
                        ->orwhere('UNIT_SEND', 'like', '%' . $filter . '%')
                        ->orwhere('UNIT_RECV', 'like', '%' . $filter . '%')
                        ->orwhere('PART_LOC', 'like', '%' . $filter . '%')
                        ->orwhere('REMARK', 'like', '%' . $filter . '%')
                        ->orwhere('STATUS', 'like', '%' . $filter . '%');
                    });
            $total = $active_job->count();
        }

        if (isset($_GET['sort']) && $_GET['sort'] != "" && in_array($_GET['sort'], $grid->getColumnFields())) {
            $active_job = $active_job->orderBy($_GET['sort'], $_GET['asc'] == "0" ? "DESC" : "ASC");
        } else {
            $active_job = $active_job->orderBy('ID', 'ASC');
        }

        $active_job = $active_job->skip($from)->take($rowByPage);
        if ($total > 0) {
            $grid->setPaginator(ceil($total / $rowByPage), (int) $total, (int) $totalUnfiltered, null);
        }
        

        $grid->renderJSON($active_job->get(), false, false, !isset($_GET['data_only']));
    }

    public function crud_crm_add() {
        $msg["status"] = NULL;
        $msg["body"] = NULL;

        $ORDER = $_POST['AUFNR'];
        $SERIAL_NUMBER = $_POST['SN_QTY'];
        $REVNR = $_POST['REVNR'];

        try {
            $crm = $this->Crm_model->updateOrCreate(
                    ['REFF' => $ORDER, 'SN_QTY' => $SERIAL_NUMBER, 'REVNR' => $REVNR], [
                'REVNR' => $REVNR,
                'PART_NUMBER' => $_POST['MATNR'],
                'DESCRIPTION' => $_POST['MAKTX'],
                'CTG' => $_POST['CTG'],
                'SN_QTY' => $_POST['SN_QTY'],
                'POST' => $_POST['POST'],
                'SP_OUT_NO' => $_POST['SP_OUT_NO'],
                'SP_OUT_DATE' => !empty($_POST['SP_OUT_DATE']) ? $_POST['SP_OUT_DATE'] : null,
                'SP_IN_NO' => $_POST['SP_IN_NO'],
                'SP_IN_DATE' => !empty($_POST['SP_IN_DATE']) ? $_POST['SP_IN_DATE'] : null,
                'UNIT_SEND' => $_POST['UNIT_SEND'],
                'UNIT_RECV' => $_POST['UNIT_RECV'],
                'PART_LOC' => $_POST['PART_LOC'],
                'REMARK' => $_POST['REMARK'],
                'STATUS' => $_POST['STATUS'],
                'REFF' => $_POST['AUFNR'],
                'CARD_TYPE' => $_POST['CARD_TYPE'],
                'IS_ACTIVE' => 1
                    ]
            );

            $msg["status"] = "success";
            $msg["body"] = "User has been saved";
        } catch (Exception $e) {
            $msg["status"] = "failed";
            $msg["body"] = $e;
        }

        header('Content-Type: application/json');
        echo json_encode($msg);
    }

    function crud_crm_after_insert($post_array, $primary_key) {
        date_default_timezone_set('Asia/Jakarta');
        $prm_update = array(
            "REVNR" => $this->session->userdata('REVNR'),
            'DATE_CREATED' => date('Y-m-d H:i:s')
        );
        $this->db->update('CRM', $prm_update, array('ID' => $primary_key));
        return true;
    }

    function crud_crm_no_add_field_callback() {
        error_reporting(E_ALL & ~E_NOTICE);
        $last_no = 0;
        try {
            $query = $this->Crm_model
                            ->where('REVNR', $this->session->userdata('REVNR'))
                            ->orderBy('NO_', 'DESC')
                            ->first()->NO_;
            // $last = $query->seq;
            $last_no = $query + 1;
        } catch (Exception $e) {
            error_log($e);
            $last_no = 1;
        }
        return '<input type="text" value="' . $last_no . '" name="NO_">';
    }

    public function crud_crm_update() {
        try {
            if ($this->uri->segment(4)) {
                $this->session->set_userdata('id_project', $this->uri->segment(4));
            }
            $data['id_project'] = $this->session->userdata('REVNR');
            $active_prj = $this->VProject_model->find($data['id_project']);
            if ($active_prj) {
                $data['project_name'] = $active_prj->REVTX;
            }
        } catch (Exception $e) {
            error_log($e);
        }

        $data["session"] = $this->session->userdata('logged_in');

        $colname = strip_tags($_POST['colname']);
        $id = strip_tags($_POST['id']);
        $coltype = strip_tags($_POST['coltype']);
        $value = strip_tags($_POST['newvalue']);
        // $tablename = strip_tags($_POST['tablename']);

        if ($coltype == 'date') {
            if ($value === "") {
                $value = NULL;
            } else {
                $date_info = date_parse_from_format('d/m/Y', $value);
                $value = "{$date_info['year']}-{$date_info['month']}-{$date_info['day']}";
            }
        }

        try {
            $this->Crm_model->where('ID', $id)->update([$colname => $value]);
        } catch (Exception $exc) {
            echo "error";
        } finally {
            echo "ok";
        }
    }

    public function crud_crm_delete() {
        try {
            if ($this->uri->segment(4)) {
                $this->session->set_userdata('id_project', $this->uri->segment(4));
            }
            $data['id_project'] = $this->session->userdata('id_project');
            $active_prj = $this->VProject_model->find($data['id_project']);
            if ($active_prj) {
                $data['project_name'] = $active_prj->project_name;
            }
        } catch (Exception $e) {
            error_log($e);
        }

        $data["session"] = $this->session->userdata('logged_in');

        $id = strip_tags($_POST['id']);
        // $tablename = strip_tags($_POST['tablename']);

        try {
            $this->Crm_model->destroy($id);
        } catch (Exception $exc) {
            echo "error";
        } finally {
            echo "ok";
        }
    }

    public function insert_csr_after_mrm_updated($id_mrm, $id_project) {
        $last_id = 0;
        // $last_csr = $this->Csr_model->where('REVNR', $id_project)->orderBy('SEQ_NUM', 'DESC')->first();
        $last_csr = $this->Csr_model->where('REVNR', $id_project)->orderBy('SEQ_NUM', 'DESC')->first();
        if($last_csr){
            $last_id = $last_csr->SEQ_NUM;
        }

        // Cek Exist Record CSR 
        $is_csr_exist = $this->Csr_model->selectRaw("*")
                                ->where("SEQ_NUM", $id_mrm)
                                ->where("REVNR", $id_project)
                                ->count();
        if ($is_csr_exist) {
            $this->Csr_model
                ->where('SEQ_NUM', $id_mrm)
                ->where("REVNR", $id_project)
                ->delete();
        }

        $mrm = $this->Mrm_model->selectRaw("*")
            ->where('SEQ_NUM', $id_mrm)
            ->where("REVNR", $id_project)
            ->first();    

        if ($mrm != null) {
            $csr = $this->Csr_model;
            $csr->REVNR = $mrm->REVNR;
            $csr->AUFNR = $mrm->AUFNR;
            $csr->MATNR = $mrm->MATNR;
            $csr->MAT_FULLFILLMENT_STATUS = $mrm->MAT_FULLFILLMENT_STATUS;
            $csr->MRM_ID = $mrm->SEQ_NUM;
            $csr->SEQ_NUM = $last_id + 1;
            $csr->REMARK = $mrm->REMARK;
            $csr->BDMNG = $mrm->MENGE;
            $csr->TOTAL_QTY = $mrm->TOTAL_QTY;
            $csr->MAKTX = $mrm->MAKTX;
            $csr->KTEXT = $mrm->KTEXT;
            $csr->CARD_TYPE = $mrm->CARD_TYPE;
            $csr->save();
        }
    }

    public function remove_csr_by_mrm_id($id_mrm, $id_project) {
        $this->Csr_model
            ->where("MRM_ID", $id_mrm)
            ->where("REVNR", $id_project)
            ->delete();
    }

    public function crud_customer_supply() {
        try {
            $data['id_project'] = $this->uri->segment(3) ? $this->uri->segment(3) : '0';
            $active_prj = $this->Project_model_ci->getProjectName($data['id_project']);
            $data['project_name'] = $active_prj ? $active_prj->REVTX : '';
        } catch (Exception $e) {
            error_log($e);
        }

        // $this->Project_model_ci->executeCSP();

        $data['write'] = $this->is_write;
        $data["session"] = $this->session->userdata('logged_in');
        $data['content'] = 'admin/projects/crud_customer_supply';

        $this->load->view('template', $data);
    }

    public function crud_customer_supply_load() {
        $is_edit = $_GET['edit'] == 1 ? true : false;
        $data["session"] = $this->session->userdata('logged_in');
        $grid = new EditableGrid();
        $REVNR = $this->input->get('REVNR');

        $grid->addColumn('SEQ_NUM', 'ID', 'integer', NULL, false);
        $grid->addColumn('MRM_ID', 'MRM', 'integer', NULL, false);
        $grid->addColumn('MATNR', 'Part Number', 'string', NULL, false);
        $grid->addColumn('MAKTX', 'Part Description', 'string', NULL, false);
        $grid->addColumn('SERIAL_NO', 'Serial No', 'string', NULL, false);
        $grid->addColumn('BDMNG', 'Qty', 'string', NULL, false);
        $grid->addColumn('TOTAL_QTY', 'Total Qty', 'string', NULL, false);
        $grid->addColumn('AUFNR', 'Order Number', 'html', NULL, false);
        $grid->addColumn('CARD_TYPE', 'Type', 'string', NULL, false);
        $grid->addColumn('KTEXT', 'Discrepancies', 'string', NULL, false);
        $grid->addColumn('AWB_NUM', 'AWB Number', 'string', NULL, true);
        $grid->addColumn('INBOUND_NUMBER', 'Inbound Number', 'string', NULL, !$this->is_customer);
        $grid->addColumn('COMMENT_AAI', 'Customer Comment', 'string', NULL, $this->is_customer);
        $grid->addColumn('COMMENT_GMF', 'Comment GMF', 'string', NULL, !$this->is_customer);
        
        if(!$this->is_customer){
            $grid->addColumn('STATUS', 'Status', 'string', array("OPEN" => "OPEN", "CLOSED" => "CLOSED"));
        }else{
            $grid->addColumn('STATUS', 'Status', 'string', array("OPEN" => "OPEN", "CLOSED" => "CLOSED"), NULL, false);
        }

        // !$this->is_customer ? $grid->addColumn('action', 'Action', 'html', NULL, false) : NULL ;

        $active_job = $this->Csr_model
                ->selectRaw('*,'
                        // .'(select SUM(CAST(CAST(BDMNG AS FLOAT) as DECIMAL(4, 2))) from V_MRM WHERE MATNR = V_CSR.MATNR and ORDER_NUMBER = V_CSR.ORDER_NUMBER and MATERIAL_FULFILLMENT_STATUS = V_CSR.MATERIAL_FULFILLMENT_STATUS) as TOTAL_QTY ,'
                        . "'<a href=\"" . base_url() . "projects/' + (CASE WHEN CARD_TYPE LIKE 'MDR' THEN 'crud_mdr' ELSE 'crud_jobcard' END) + '/' + REVNR + '/' + AUFNR + '/CSP\">' + AUFNR + '</a>' AS AUFNR,"
                        . "(SELECT A.SEQ_NUM FROM TB_MRM LEFT JOIN (SELECT ID,ROW_NUMBER () OVER (PARTITION BY REVNR ORDER BY ID ASC) AS SEQ_NUM FROM TB_MRM WHERE REVNR = TB_CSR.REVNR) A ON TB_MRM.ID = A.ID WHERE TB_MRM.ID = TB_CSR.MRM_ID) AS FAKE_MRM_ID"
                        // . "'<button class=\"btn btn-sm btn-danger delete\"  onclick=\"mrm_delete('+ nchar(39) + REVNR + '-' + SEQ_NUM + nchar(39) + ')\"><i class=\"fa fa-trash-o\" aria-hidden=\"true\"></i> </button>' AS action"
                        )
                ->where('REVNR', $REVNR);

        $totalUnfiltered = $active_job->count();
        $total = $totalUnfiltered;

        $page = 0;
        if (isset($_GET['page']) && is_numeric($_GET['page']))
            $page = (int) $_GET['page'];


        $rowByPage = $totalUnfiltered; // 50;

        $from = ($page - 1) * $rowByPage;

        if (isset($_GET['filter']) && $_GET['filter'] != "") {
            $filter = $_GET['filter'];
            $active_job = $active_job
                    ->where(function($active_job) use ($filter){
                        $active_job->where('SEQ_NUM', 'like', '%' . $filter . '%')
                        ->orwhere('MATNR', 'like', '%' . $filter . '%')
                        ->orWhere('MRM_ID', 'like', '%' . $filter . '%')
                        ->orWhere('MAKTX', 'like', '%' . $filter . '%')
                        ->orWhere('SERIAL_NO', 'like', '%' . $filter . '%')
                        ->orWhere('BDMNG', 'like', '%' . $filter . '%')
                        ->orWhere('TOTAL_QTY', 'like', '%' . $filter . '%')
                        ->orWhere('AUFNR', 'like', '%' . $filter . '%')
                        ->orWhere('CARD_TYPE', 'like', '%' . $filter . '%')
                        ->orWhere('KTEXT', 'like', '%' . $filter . '%')
                        ->orWhere('AWB_NUM', 'like', '%' . $filter . '%')
                        ->orWhere('INBOUND_NUMBER', 'like', '%' . $filter . '%')
                        ->orWhere('COMMENT_AAI', 'like', '%' . $filter . '%')
                        ->orWhere('COMMENT_GMF', 'like', '%' . $filter . '%')
                        ->orWhere('STATUS', 'like', '%' . $filter . '%');
                    });
            $total = $active_job->count();
        }

        if (isset($_GET['sort']) && $_GET['sort'] != "" && in_array($_GET['sort'], $grid->getColumnFields())) {
            $active_job = $active_job->orderBy($_GET['sort'], $_GET['asc'] == "0" ? "DESC" : "ASC");
        } else {
            $active_job = $active_job->orderBy('SEQ_NUM', 'ASC');
        }

        $active_job = $active_job->skip($from)->take($rowByPage);
        if ($total > 0) {
            $grid->setPaginator(ceil($total / $rowByPage), (int) $total, (int) $totalUnfiltered, null);
        }

        $grid->renderJSON($active_job->get(), false, false, !isset($_GET['data_only']));
    }

    public function write_xlsx_file_csp()
    {
        $REVNR = $_GET['REVNR'];
        $active_job = $this->Csr_model
                ->selectRaw('*,'
                        . "AUFNR,"
                        . "(SELECT A.SEQ_NUM FROM TB_MRM LEFT JOIN (SELECT ID,ROW_NUMBER () OVER (PARTITION BY REVNR ORDER BY ID ASC) AS SEQ_NUM FROM TB_MRM WHERE REVNR = TB_CSR.REVNR) A ON TB_MRM.ID = A.ID WHERE TB_MRM.ID = TB_CSR.MRM_ID) AS FAKE_MRM_ID"
                        )
                ->where('REVNR', $REVNR)
                ->orderBy('SEQ_NUM', 'ASC');
        $csp = $active_job->get();
        $csp_list = array();
        foreach ($csp as $value) {
            array_push(
                $csp_list, 
                array(
                    $value->SEQ_NUM, 
                    $value->MRM_ID, 
                    $value->MATNR,
                    $value->MAKTX,
                    $value->SERIAL_NO,
                    $value->BDMNG,
                    $value->TOTAL_QTY,
                    $value->AUFNR,
                    $value->CARD_TYPE,
                    $value->KTEXT,
                    $value->AWB_NUM,
                    $value->INBOUND_NUMBER,
                    $value->COMMENT_AAI,
                    $value->COMMENT_GMF,
                    $value->STATUS,
                )
            );
        }

        $writer = WriterFactory::create(Type::XLSX);
        $default_style = (new StyleBuilder())
                ->setFontName('Arial')
                ->setFontSize(10)
                ->setShouldWrapText(false)
                ->build();
        $writer->setDefaultRowStyle($default_style);

        $file_name = "CSP_" . $REVNR . "_" . date('YmdhIs') . ".xlsx";
        $writer->openToFile($file_name); 

        $data_header = array();
        $data_header[] = "ID";
        $data_header[] = "MRM";
        $data_header[] = "Part Number";
        $data_header[] = "Part Description";
        $data_header[] = "Serial No";
        $data_header[] = "Qty";
        $data_header[] = "Total Qty";
        $data_header[] = "Order Number";
        $data_header[] = "Type";
        $data_header[] = "Discrepancies";
        $data_header[] = "AWB Number";
        $data_header[] = "Inbound Number";
        $data_header[] = "Customer Comment";
        $data_header[] = "Comment GMF";
        $data_header[] = "Status";

        $writer->addRow($data_header);
        $writer->addRows($csp_list);
        $writer->close();

        $this->load->helper('download');
        force_download($file_name, null);
    }

    public function crud_customer_supply_update() {
        $data["session"] = $this->session->userdata('logged_in');

        $colname = strip_tags($_POST['colname']);
        $id = strip_tags($_POST['id']);
        $coltype = strip_tags($_POST['coltype']);
        $value = strip_tags($_POST['newvalue']);
        // $tablename = strip_tags($_POST['tablename']);
        //$data['id_project'] = $this->session->userdata('REVNR');
        //$csr = $this->Project_model_ci->getItemCSR($id, $data['id_project']);
        //$data['aufnr'] = $csr->AUFNR;
        // $data['id'] = $csr->AUFNR;

        if ($coltype == 'date') {
            if ($value === "") {
                $value = NULL;
            } else {
                $date_info = date_parse_from_format('d/m/Y', $value);
                $value = "{$date_info['year']}-{$date_info['month']}-{$date_info['day']}";
            }
        }

        try {
            $csr = $this->Csr_model
                    //->where('AUFNR', $data['aufnr'])
                    ->where('REVNR', $this->session->userdata('REVNR'))
                    ->where('SEQ_NUM', $id)
                    ->update([$colname => $value]);
        } catch (Exception $exc) {
            echo "error";
            echo $exc;
        } finally {
            echo "ok";
        }
    }

    public function crud_task() {
        try {
            if ($this->uri->segment(4)) {
                $this->session->set_userdata('REVNR', $this->uri->segment(4));
            }
            $data['id_project'] = $this->session->userdata('REVNR');
            $active_prj = $this->Project_model_ci->getProjectName($data['id_project']);
            if ($active_prj) {
                $data['project_name'] = $active_prj->REVTX;
            }
        } catch (Exception $e) {
            error_log($e);
        }

        $data['is_grocery'] = true;
        $crud = new grocery_CRUD();


        $crud->set_table('task');
        $crud->where('id_project', $this->session->userdata('id_project'));
        $crud->set_subject('Task');
        $crud->columns('No_', 'Subject', 'Start_Date', 'End_Date', 'Billable_Status');
        $crud->display_as('Subject', 'Name');
        $crud->unset_fields('id_project', 'date_created', 'id');
        $crud->set_field_upload('Attachment', 'assets/uploads/files');
        $crud->callback_after_insert(array($this, 'task_after_insert'));
        $crud->callback_add_field('No_', array($this, 'crud_task_no_add_field_callback'));
        $output = $crud->render();

        $data["session"] = $this->session->userdata('logged_in');
        $data['content'] = 'admin/projects/crud_task';

        $this->load->view('template', array_merge($data, (array) $output));
    }

    public function task_after_insert($post_array, $primary_key) {
        date_default_timezone_set('Asia/Jakarta');
        $task_day_update = array(
            "date_created" => date('Y-m-d H:i:s'),
            "id_project" => $this->session->userdata('id_project'),
        );

        $this->db->update('task', $task_day_update, array('id' => $primary_key));

        return true;
    }

    function crud_task_no_add_field_callback() {
        error_reporting(E_ALL & ~E_NOTICE);
        $last_no_ = 0;
        try {
            $query = $this->db->select('No_')
                    ->from('task')
                    ->where('id_project', $this->session->userdata('id_project'))
                    ->order_by('No_', 'DESC')
                    ->limit(1)
                    ->get();
            $ret = $query->row();
            $last = $ret->No_;
            $last_no = $last + 1;
        } catch (Exception $e) {
            error_log($e);
            $last_no_ = 1;
        }
        return '<input type="text" value="' . $last_no . '" name="No_">';
    }

    function crud_ticket() {

        try {
            if ($this->uri->segment(4)) {
                $this->session->set_userdata('REVNR', $this->uri->segment(4));
            }
            $data['id_project'] = $this->session->userdata('REVNR');
            $active_prj = $this->project_model_ci->getProjectName($data['id_project']);
            if ($active_prj) {
                $data['project_name'] = $active_prj->REVTX;
            }
        } catch (Exception $e) {
            error_log($e);
        }

        $data['is_grocery'] = true;
        $crud = new grocery_CRUD();

        $crud->set_table('ticket');
        $crud->set_subject('Ticket');
        $crud->where('id_project', $this->session->userdata('id_project'));
        $crud->columns('Subject', 'Ticket_Type', 'Status', 'Attachment');
        $crud->field_type('Ticket_Type', 'dropdown', array('Low' => 'Low', 'Medium' => 'Medium', 'High' => 'High'));
        $crud->field_type('Status', 'dropdown', array('New' => 'New', 'Open Replied' => 'Open Replied', 'Open' => 'Open', 'Close' => 'Close'));
        $crud->add_fields('Subject', 'Ticket_Type', 'Description', 'Attachment');
        $crud->edit_fields('Subject', 'Ticket_Type', 'Status', 'Description', 'Attachment');
        $crud->set_field_upload('Attachment', 'assets/uploads/files');
        $crud->callback_after_insert(array($this, 'ticket_after_insert'));
        $crud->callback_after_update(array($this, 'ticket_after_update'));
        $crud->callback_column('Subject', array($this, 'callback_detail_ticket'));
        // $crud->add_action('Comment', base_url() . 'assets/dist/img/icon_comments.png', 'admin/projects/comment_ticket');
        $output = $crud->render();
        $data["session"] = $this->session->userdata('logged_in');
        $data['content'] = 'admin/projects/crud_ticket';

        $this->load->view('template', array_merge($data, (array) $output));
    }

    function ticket_after_insert($post_array, $primary_key) {
        $ticket_update = array(
            "Create_Date_Ticket" => date('Y-m-d H:i:s'),
            "Created_By" => $this->session->userdata('logged_in')['id_user'],
            "Status" => "Open",
            "id_project" => $this->session->userdata('id_project'),
        );

        $this->db->update('ticket', $ticket_update, array('Id' => $primary_key));

        return true;
    }

    public function callback_detail_ticket($value, $row) {

        return '<a href="' . base_url() . 'admin/projects/comment_ticket/' . $row->Id . '">' . $value . '</a>';
    }

    function comment_ticket() {

        try {
            if ($this->uri->segment(4)) {
                $this->session->set_userdata('ticket_id', $this->uri->segment(4));
            }
            $data['ticket_id'] = $this->session->userdata('ticket_id');
            $active_prj = $this->Ticket_model->find($data['ticket_id']);
            if ($active_prj) {
                $data['Subject'] = $active_prj->Subject;
                $data['Status'] = $active_prj->Status;
                $data['Created_By'] = $active_prj->Created_By;
                $data['Ticket_Type'] = $active_prj->Ticket_Type;
                $data['Create_Date_Ticket'] = $active_prj->Create_Date_Ticket;
                $data['Status'] = $active_prj->Status;
            }
        } catch (Exception $e) {
            error_log($e);
        }


        $data["session"] = $this->session->userdata('logged_in');
        $data['content'] = 'admin/projects/comment_ticket';

        $this->load->view('template', $data);
    }

    function ticket_after_update($post_array, $primary_key) {
        $ticket_update = array(
            "Replay_At" => date('Y-m-d H:i:s'),
            "Replay_By" => $this->session->userdata('logged_in')['id_user']
        );

        $this->db->update('ticket', $ticket_update, array('id' => $primary_key));

        return true;
    }

    public function crud_estimates() {
        try {
            if ($this->uri->segment(4)) {
                $this->session->set_userdata('id_project', $this->uri->segment(4));
            }
            $data['id_project'] = $this->session->userdata('id_project');
            $active_prj = $this->VProject_model->find($data['id_project']);
            if ($active_prj) {
                $data['project_name'] = $active_prj->project_name;
            }
        } catch (Exception $e) {
            error_log($e);
        }
        $data['is_grocery'] = true;
        $crud = new grocery_CRUD();


        $crud->set_table('estimates');
        $crud->where('id_project', $this->session->userdata('id_project'));
        $crud->set_subject('Estimate');
        $crud->columns('Estimate', 'Amount', 'Start_Date', 'Expired_Date', 'Status');
        $crud->unset_fields('id_project', 'date_created', 'id');
        // $crud->callback_column('Amount', array($this, 'valueToDollar'));
        $crud->field_type('Status', 'dropdown', array('Draft' => 'Draft', 'Sent' => 'Sent', 'Expired' => 'Expired', 'Declined' => 'Declined', 'Accepted' => 'Accepted'));
        $crud->callback_after_insert(array($this, 'estimate_after_insert'));
        $crud->callback_add_field('Estimate', array($this, 'crud_estimate_no_add_field_callback'));
        // $crud->set_rules('Expired_Date', 'Expired_Date', 'trim|callback_check_estimatedates[' . $this->input->post('Start_Date') . ']');
        $output = $crud->render();

        $data["session"] = $this->session->userdata('logged_in');
        $data['content'] = 'admin/projects/crud_estimates';

        $this->load->view('template', array_merge($data, (array) $output));
    }

    public function valueToDollar($value, $row) {
        return '$' . $value;
    }

    public function estimate_after_insert($post_array, $primary_key) {
        date_default_timezone_set('Asia/Jakarta');
        $task_day_update = array(
            "date_created" => date('Y-m-d H:i:s'),
            "id_project" => $this->session->userdata('id_project'),
        );

        $this->db->update('estimates', $task_day_update, array('id' => $primary_key));

        return true;
    }

    function crud_estimate_no_add_field_callback() {
        error_reporting(E_ALL & ~E_NOTICE);
        $last_no_ = 0;
        try {
            $query = $this->db->select('Estimate')
                    ->from('estimates')
                    ->where('id_project', $this->session->userdata('id_project'))
                    ->order_by('Estimate', 'DESC')
                    ->limit(1)
                    ->get();
            $ret = $query->row();
            $last = $ret->Estimate;
            $last_no = $last + 1;
        } catch (Exception $e) {
            $last_no_ = 1;
        }
        return '<input type="text" value="' . $last_no . '" name="Estimate">';
    }

    public function check_estimatedates($Expired_Date, $Start_Date) {
        $parts = explode('/', $this->input->post('Start_Date'));
        $Start_Date = join('-', $parts);
        $Start_Date = strtotime($Start_Date);

        $parts2 = explode('/', $this->input->post('Expired_Date'));
        $Expired_Date = join('-', $parts2);
        $Expired_Date = strtotime($Expired_Date);

        if ($Expired_Date >= $Start_Date) {

            return true;
        } else {
            $this->form_validation->set_message('check_estimatedates', "Expired date should be greater than start date");
            return false;
        }
    }

    public function crud_invoices() {
        try {
            if ($this->uri->segment(4)) {
                $this->session->set_userdata('id_project', $this->uri->segment(4));
            }
            $data['id_project'] = $this->session->userdata('id_project');
            $active_prj = $this->VProject_model->find($data['id_project']);
            if ($active_prj) {
                $data['project_name'] = $active_prj->project_name;
            }
        } catch (Exception $e) {
            error_log($e);
        }
        $data['is_grocery'] = true;
        $crud = new grocery_CRUD();


        $crud->set_table('invoices');
        $crud->where('id_project', $this->session->userdata('id_project'));
        $crud->set_subject('Invoice');
        $crud->columns('Invoice', 'Amount', 'Bill_Date', 'Due_Date', 'Status');
        $crud->unset_fields('id_project', 'date_created', 'id');
        $crud->callback_column('Amount', array($this, 'valueToDollar'));
        $crud->field_type('Status', 'dropdown', array('Paid' => 'Paid', 'Unpaid' => 'Unpaid'));
        $crud->callback_after_insert(array($this, 'invoices_after_insert'));
        $crud->callback_add_field('Invoice', array($this, 'crud_invoices_no_add_field_callback'));
        $crud->set_rules('Due_Date', 'Due_Date', 'trim|callback_check_dates[' . $this->input->post('Bill_Date') . ']');
        $output = $crud->render();

        $data["session"] = $this->session->userdata('logged_in');
        $data['content'] = 'admin/projects/crud_invoices';

        $this->load->view('template', array_merge($data, (array) $output));
    }

    public function invoices_after_insert($post_array, $primary_key) {
        date_default_timezone_set('Asia/Jakarta');
        $task_day_update = array(
            "date_created" => date('Y-m-d H:i:s'),
            "id_project" => $this->session->userdata('id_project'),
        );

        $this->db->update('invoices', $task_day_update, array('id' => $primary_key));

        return true;
    }

    function crud_invoices_no_add_field_callback() {
        error_reporting(E_ALL & ~E_NOTICE);
        $last_no_ = 0;
        try {
            $query = $this->db->select('Invoice')
                    ->from('invoices')
                    ->where('id_project', $this->session->userdata('id_project'))
                    ->order_by('Invoice', 'DESC')
                    ->limit(1)
                    ->get();
            $ret = $query->row();
            $last = $ret->Invoice;
            $last_no = $last + 1;
        } catch (Exception $e) {
            $last_no_ = 1;
        }
        return '<input type="text" value="' . $last_no . '" name="Invoice">';
    }

    public function check_dates($Due_Date, $Bill_Date) {
        $parts = explode('/', $this->input->post('Bill_Date'));
        $Bill_Date = join('-', $parts);
        $Bill_Date = strtotime($Bill_Date);

        $parts2 = explode('/', $this->input->post('Due_Date'));
        $Due_Date = join('-', $parts2);
        $Due_Date = strtotime($Due_Date);

        if ($Due_Date >= $Bill_Date) {

            return true;
        } else {
            $this->form_validation->set_message('check_dates', "due date should be greater than bill date");
            return false;
        }
    }

    public function crud_files() {
        try {
            if ($this->uri->segment(4)) {
                $this->session->set_userdata('REVNR', $this->uri->segment(4));
            }
            $data['id_project'] = $this->session->userdata('REVNR');
            $active_prj = $this->project_model_ci->getProjectName($data['id_project']);
            if ($active_prj) {
                $data['project_name'] = $active_prj->REVTX;
            }
        } catch (Exception $e) {
            error_log($e);
        }
        $data['is_grocery'] = true;
        $crud = new grocery_CRUD();


        $crud->set_table('files');
        $crud->where('id_project', $this->session->userdata('id_project'));
        $crud->set_subject('File');
        $crud->columns('No_', 'Subject', 'Description', 'Attachments_file');
        $crud->unset_fields('id_project', 'date_created', 'id');
        $crud->set_field_upload('Attachments_file', 'assets/uploads/files');
        $crud->callback_after_insert(array($this, 'file_after_insert'));

        $output = $crud->render();

        $data["session"] = $this->session->userdata('logged_in');
        $data['content'] = 'admin/projects/crud_files';

        $this->load->view('template', array_merge($data, (array) $output));
    }

    public function file_after_insert($post_array, $primary_key) {
        date_default_timezone_set('Asia/Jakarta');
        $task_day_update = array(
            "date_created" => date('Y-m-d H:i:s'),
            "id_project" => $this->session->userdata('id_project'),
        );
        $this->db->update('files', $task_day_update, array('id' => $primary_key));
        return true;
    }

    public function dashboard() {
        try {
            if ($this->uri->segment(3)) {
                $this->session->set_userdata('REVNR', $this->uri->segment(3));
            }
            $data['id_project'] = $this->session->userdata('REVNR');
            $active_prj = $this->Project_model->get_project_detail($data['id_project']);
            if ($active_prj) {
                $data['project_name'] = $active_prj->REVTX;
                $data['active_prj'] = $active_prj;
            }
        } catch (Exception $e) {
            error_log($e);
            echo $e;
        }

        $data["session"] = $this->session->userdata('logged_in');
        $data['content'] = 'admin/projects/dashboard';

        $data['projects'] = true;


        $this->load->view('template', $data);
    }

    public function dashboard_jobcard_by_status() {
        $response["status"] = NULL;
        $response["body"] = NULL;

        try {

            $REVNR = $_POST['REVNR'];
            $jobcard = $this->Dashboard_model->getJobcardByStatus($REVNR);
            $mdr = $this->Dashboard_model->getMdrByStatus($REVNR);

            if ($jobcard) {

                // Modify Column to Row
                $jc_status = array();
                array_push($jc_status,
                    ((object) array("LABELS" => "OPEN", "QTY" => $jobcard[0]->JC_OPEN )),
                    ((object) array("LABELS" => "PROGRESS", "QTY" => $jobcard[0]->JC_PROGRESS )),
                    ((object) array("LABELS" => "CLOSED", "QTY" => $jobcard[0]->JC_CLOSED ))
                );

                $response["status"] = 'success';
                $response["body"]["jc_status"] = $jc_status;
            }

            if($mdr){
                // Modify Column to Row
                $mdr_status = array();
                array_push($mdr_status,
                    ((object) array("LABELS" => "OPEN", "QTY" => $mdr[0]->MDR_OPEN )),
                    ((object) array("LABELS" => "PROGRESS", "QTY" => $mdr[0]->MDR_PROGRESS )),
                    ((object) array("LABELS" => "CLOSED", "QTY" => $mdr[0]->MDR_CLOSED ))
                );

                $response["body"]["mdr_status"] = $mdr_status;
                
            }
        } catch (Exception $e) {
            
        }

        header('Content-Type: application/json');
        echo json_encode($response);
    }

    public function crud_proposal() {

        $data['is_grocery'] = true;
        $crud = new grocery_CRUD();
        $crud->set_table('proposal');
        $crud->set_subject('Proposal');
        $crud->columns('No_Proposal', 'Subject', 'Start_Date', 'Expired_Date', 'Amount', 'Status');
        $crud->unset_fields('date_created', 'Id');

        $crud->field_type('Status', 'dropdown', array('Draft' => 'Draft', 'Sent' => 'Sent', 'Accepted' => 'Accepted', 'Declined' => 'Declined'));
        $crud->callback_after_insert(array($this, 'proposal_after_insert'));
        $crud->callback_add_field('No_Proposal', array($this, 'crud_propsal_no_add_field_callback'));
        $crud->set_rules('Expired_Date', 'Expired_Date', 'trim|callback_proposal_check_dates[' . $this->input->post('Start_Date') . ']');
        $output = $crud->render();

        $data["session"] = $this->session->userdata('logged_in');
        $data['content'] = 'admin/projects/crud_proposal';
        $data['group'] = $this->User_group_model->where('ID_USER_GROUP', $data["session"]['group_id'])->first();

        $this->load->view('template', array_merge($data, (array) $output));
    }

    public function proposal_after_insert($post_array, $primary_key) {
        date_default_timezone_set('Asia/Jakarta');
        $proposal_day_update = array(
            "date_created" => date('Y-m-d H:i:s')
        );

        $this->db->update('proposal', $proposal_day_update, array('Id' => $primary_key));

        return true;
    }

    function crud_propsal_no_add_field_callback() {
        error_reporting(E_ALL & ~E_NOTICE);
        $last_no_ = 0;
        try {
            $query = $this->db->select('No_Proposal')
                    ->from('proposal')
                    ->order_by('No_Proposal', 'DESC')
                    ->limit(1)
                    ->get();
            $ret = $query->row();
            $last = $ret->No_Proposal;
            $last_no = $last + 1;
        } catch (Exception $e) {
            $last_no_ = 1;
        }
        return '<input type="text" value="' . $last_no . '" name="No_Proposal">';
    }

    public function proposal_check_dates($Due_Date, $Bill_Date) {
        $parts = explode('/', $this->input->post('Bill_Date'));
        $Bill_Date = join('-', $parts);
        $Bill_Date = strtotime($Bill_Date);

        $parts2 = explode('/', $this->input->post('Due_Date'));
        $Due_Date = join('-', $parts2);
        $Due_Date = strtotime($Due_Date);

        if ($Due_Date >= $Bill_Date) {

            return true;
        } else {
            $this->form_validation->set_message('check_dates', "due date should be greater than bill date");
            return false;
        }
    }

//    public function get_notif() {
//        $jobcard_notif['base'] = $this->Jobcard_model_view
//                ->whereRaw("((UPPER( STATUS_SAP ) IN('OPEN', 'CLOSED') OR UPPER( STATUS_SAP ) IN('OPEN', 'CLOSED') ) AND UPPER( STATUS_SAP )!= UPPER( STATUS ) ) OR(UPPER( STATUS_SAP )= 'PROGRESS'AND UPPER( STATUS ) IN('OPEN', 'CLOSED') )");
////        $jobcard_notif['count'] = $jobcard_notif['base']->count();
//        $jobcard_notif['data'] = $jobcard_notif['base']->get();
//        echo json_encode($jobcard_notif['data']);
//    }

    public function get_notif_count() {
        if ($this->session->userdata('REVNR')) {
            echo json_encode($this->Proc_model->get_notif_count($this->session->userdata('REVNR')));
        } else {
            echo json_encode($this->Proc_model->get_notif_count());
        }
    }

    public function get_notif($page = 1, $limit = 10) {
        if ($this->session->userdata('REVNR')) {
            echo json_encode($this->Proc_model->get_notif($page, $limit, $this->session->userdata('REVNR')));
        } else {
            echo json_encode($this->Proc_model->get_notif($page, $limit));
        }
    }

    public function crud_daily_menu() {
        try {
            $data['id_project'] = $this->uri->segment(3) ? $this->uri->segment(3) : '0';
            $active_prj = $this->Project_model->get_project_list($data['id_project']);
            $project_area = $this->Daily_menu_model_ci->getAreaList($data['id_project']);
            $project_day = $this->Daily_menu_model_ci->getDayList($data['id_project']);
            // $mhrs_query = " SELECT SUM( CAST( REPLACE(ARBEI,',','') AS FLOAT ) ) AS 'MHRS' FROM M_PMORDER WHERE REVNR = " . $this->session->userdata('REVNR') . " AND AUART = 'GA01'";
            // $mhrs_count = $this->db
            //                 ->query($mhrs_query)
            //                 ->row()->MHRS;

            if ($active_prj) {
                $data['project_name'] = "DAILY MENU";
                $data['project_data'] = $active_prj;
                $data['project_area'] = $project_area;
                $data['project_day'] = $project_day;
                // $data['mhrs_count'] = 10;
            }
        } catch (Exception $e) {
            error_log($e);
        }

        $data['write'] = $this->is_write;
        $data["session"] = $this->session->userdata('logged_in');
        $data['content'] = 'admin/projects/crud_daily_menu';

        $this->load->view('template', $data);
    }

    public function crud_daily_menu_load() {
        $data["session"] = $this->session->userdata('logged_in');

        $grid = new EditableGrid();

        $grid->addColumn('SEQ', 'SEQ', 'int', NULL, false);
        $grid->addColumn('AUFNR', 'Order', 'INT', NULL, false);
        $grid->addColumn('KTEXT', 'Description', 'string', NULL, false);
        $grid->addColumn('ILART', 'Task Code', 'string', NULL, false);
        $grid->addColumn('AREA', 'Area', 'string', NULL, false);
        $grid->addColumn('MHRS', 'Mhrs', 'INT', NULL, false);
        $grid->addColumn('CARD_TYPE', 'TYPE', 'string', NULL, false);

        $active_job = $this->db->query("EXEC TB_P_DAILY_MENU @REVNR = " . $this->session->userdata('REVNR'));

        if (isset($_GET['filter']) && $_GET['filter'] != "") {
            $filter = $_GET['filter'];
            $pos = strpos($filter, '-');

            if ($filter == '-') {
                $active_job = $active_job;
            } elseif (strpos($filter, '-') == (strlen($filter) - 1)) {
                // Filter By Day
                $active_job = $this->db->query("EXEC TB_P_DAILY_MENU @REVNR = " . $this->session->userdata('REVNR') . " ,@P_DAY = "
                        . str_replace('-', '', $filter));
            } elseif ($pos == 0) {
                // Filter By Area
                $active_job = $this->db->query("EXEC TB_P_DAILY_MENU @REVNR = " . $this->session->userdata('REVNR') . " ,@P_AREA = " . str_replace('-', '', $filter));
            } else {
                // Jika URl Seperti ini 5-COPKIT
                $filter = explode('-', $filter);
                $active_job = $this->db->query("EXEC TB_P_DAILY_MENU @REVNR = " . $this->session->userdata('REVNR') . " , @P_AREA = " . $filter[0] . ", @P_DAY = " . $filter[1]);
            }
        }
        $grid->renderJSON($active_job->result(), false, false, true);
    }

    public function crud_daily_menu_load_all() {
        $response["status"] = NULL;
        $response["body"] = NULL;
        $daily_menu = $this->Proc_model;

        try {
            // $sql = "EXEC TB_P_DAILY_MENU @REVNR = " . $_POST['REVNR'];

            // if (isset($_POST['DAY']) && isset($_POST['AREA'])) {
            //     $sql .= ", @P_DAY =" . $_POST['DAY'] . ", @P_AREA = '" . $_POST['AREA'] . "'";
            // } elseif (isset($_POST['DAY'])) {
            //     $sql .= ", @P_DAY =" . $_POST['DAY'];
            // } elseif (isset($_POST['AREA'])) {
            //     $sql .= ", @P_AREA = '" . $_POST['AREA'] . "'";
            // }


            // print_r($_POST);

            $DAY = '';
            $AREA = '';

            if (isset($_POST['DAY']) && isset($_POST['AREA'])) {
                $DAY = $_POST['DAY'];
                $AREA = $_POST['AREA'];
            } elseif (isset($_POST['DAY'])) {
                $DAY = $_POST['DAY'];
            } elseif (isset($_POST['AREA'])) {
                $AREA = $_POST['AREA'];
            }


            $daily_menu = $daily_menu->get_daily_menu($_POST['REVNR'], $DAY, $AREA);

            if (count($daily_menu) > 0) {
                $response["status"] = 'success';
                $response["body"] = $daily_menu;
            } else {
                $response["status"] = 'error';
                $response["body"] = NULL;
            }
        } catch (Exception $e) {
            
        }

        header('Content-Type: application/json');
        echo json_encode($response);
    }

    public function get_mhrs_total() {
        if (isset($_GET['id']) && ( isset($_GET['day']) || isset($_GET['area']) )) {
            $id = $_GET['id'];
            $area = $_GET['area'];
            $day = $_GET['day'];

            $mhrs['total'] = $this->Daily_menu_model_ci->getSumMhrs($id, $area, $day)->total;
            $mhrs['req'] = $mhrs['total'] / 4;
            echo json_encode($mhrs);
        } else {
            echo "null";
        }
    }

    // Customer View
    public function view_customer() {
        $data["session"] = $this->session->userdata('logged_in');
        $data['content'] = 'admin/projects/view_customer';

        $this->load->view('template', $data);
    }

    public function load_material_data() {
        $response["status"] = NULL;
        $response["body"] = NULL;

        try {
            if (isset($_POST['MATNR'])) {
                $search = $_POST['MATNR'];
                if($search=='N/A'){
                    $search='-';
                }
                // if($_POST['MATNR']!='N/A'){
                $material = $this->Material_model->where('MATNR', $search);
                if ($material->count() > 0) {
                    $response["status"] = 'success';
                    $response["body"] = $material->first();
                } else {
                    $response["status"] = 'error';
                    $response["body"] = NULL;
                }
            // }else{
            //          $response["status"] = 'success';
            //         $response["body"] = $material->first();
            //     }
            }
        } catch (Exception $e) {
            
        }

        header('Content-Type: application/json');
        echo json_encode($response);
    }

    function crud_revh(){
    
    }

    function get_hilite(){
        $response["status"] = NULL;
        $response["body"] = NULL;

        try{
            if(!empty($this->input->post('REVNR'))){
                $REVNR = $this->input->post('REVNR');
                $hilite = $this->Revh_model
                                ->where("REVNR", $REVNR)
                                ->first();
                if ($hilite) {
                    $response["status"] = 'success';
                    $response["body"] = $hilite;
                } else {
                    $response["status"] = 'error';
                    $response["body"] = $hilite;
                } 
            }          
        } catch (Exception $e){ 
            echo $e;
        }

        header('Content-Type: application/json');
        echo json_encode($response); 
    }

    function save_hilite(){
        $response["status"] = NULL;
        $response["body"] = NULL;

        try{
            if(!empty($this->input->post('REVNR'))){
                $REVNR = $this->input->post('REVNR');
                $HILITE_BODY = $this->input->post('HILITE');
                $IN_HANGAR = $this->input->post('IN_HANGAR');
                $hilite = $this->Revh_model
                    ->updateOrCreate(
                        ['REVNR' => $REVNR ],
                        ['HILITE' => $HILITE_BODY, 'IN_HANGAR' => $IN_HANGAR ]
                    );

                if ($hilite) {
                    $response["status"] = 'success';
                    $response["body"] = NULL;
                } else {
                    $response["status"] = 'error';
                    $response["body"] = NULL;
                } 
            }          
        } catch (Exception $e){ 
            echo $e;
        }

        header('Content-Type: application/json');
        echo json_encode($response); 
    }

}
