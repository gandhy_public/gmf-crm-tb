<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Sap
 *
 * @author Miftachur Rozaq
 */
require_once 'assets/pasa/saprfc.php';

class Sapi extends CI_Controller {

    public function __construct() {
        parent::__construct();

//        $this->login_prod = array(
//            "MSHOST" => "sapgmfpsg.gmf-aeroasia.co.id",
//            "GROUP" => "SPACE",
//            "CLIENT" => "500",
//            "USER" => "RFCPRD1",
//            "PASSWD" => "Activate1",
//            "R3NAME" => "PSG"
//        );

        $this->login_dev = array(
            "ASHOST" => "sapgmfdsg.gmf-aeroasia.co.id",
            "SYSNR" => "02",
            "CLIENT" => "150",
            "USER" => "G580422",
            "PASSWD" => "Welcome01"
        );

        $this->login_bw = array(
            "ASHOST" => "172.16.102.172",
            "SYSNR" => "34",
            "CLIENT" => "500",
            "USER" => "N317405",
            "PASSWD" => "Activate101",
            "R3NAME" => "PBW"
        );

//        $this->sap = new saprfc(array(
//            "logindata" => $this->login,
//            "show_errors" => FALSE,
//            "debug" => FALSE));
//        $this->rfc = saprfc_open($this->login);
    }

    public function update_revision() {
        $this->sap = new saprfc(array(
            "logindata" => $this->login_bw,
            "show_errors" => FALSE,
            "debug" => FALSE));
        $result = $this->sap->callFunction("ZDFFM_TDE_REVISION", array(
            array("IMPORT", "1_DATA", array(
                ''
            )),
            array("EXPORT", "E_DATA", array())
        ));
        if ($this->sap->getStatus() == SAPRFC_OK) {
            echo json_encode($result);
//            echo 'SAPRFC_OK';
        } else {
            $this->sap->printStatus();
        }
        $this->sap->logoff();
    }

    public function update_pmorder_item() {
        $this->sap = new saprfc(array(
            "logindata" => $this->login_bw,
            "show_errors" => FALSE,
            "debug" => FALSE));
        $result = $this->sap->callFunction("ZDFFM_TDE_PMORDER_ITEM", array(
            array("IMPORT", "1_DATA", array()),
            array("EXPORT", "E_DATA", array())
        ));
        if ($this->sap->getStatus() == SAPRFC_OK) {
//            echo json_encode($result);
            echo 'SAPRFC_OK';
        } else {
            $this->sap->printStatus();
        }
        $this->sap->logoff();
    }

    public function update_pmorder_header() {
        $this->sap = new saprfc(array(
            "logindata" => $this->login_bw,
            "show_errors" => FALSE,
            "debug" => FALSE));
        $result = $this->sap->callFunction("ZDFFM_TDE_PMORDER_HEADER", array(
            array("IMPORT", "1_DATA", array()),
            array("EXPORT", "E_DATA", array())
        ));
        if ($this->sap->getStatus() == SAPRFC_OK) {
//            echo json_encode($result);
            echo 'SAPRFC_OK';
        } else {
            $this->sap->printStatus();
        }
        $this->sap->logoff();
    }

//    public function update_revision($p1 = '', $p2 = '') {
//        error_reporting(0);
//        if ($p1 and $p2) {
//            // RESERVED
//        } else {
//            $p1 = $p2 = date('Ymd');
//        }
//        echo file_get_contents("http://192.168.240.107/Sched_ABS/testing_sap_bw_sql.php?f=ZDFFM_TDE_REVISION&p1=$p1&p2=$p2");
//    }
//
//    public function update_revision_change($p1 = '', $p2 = '') {
////        error_reporting(0);
//        if ($p1 and $p2) {
//            // RESERVED
//        } else {
//            $p1 = $p2 = date('Ymd');
//        }
//        echo file_get_contents("http://192.168.240.107/Sched_ABS/testing_sap_bw_sql.php?t=RFC REVISION&f=ZDFFM_TDE_REVISION__CHANGE_DATE&p1=$p1&p2=$p2");
////        echo file_get_contents("http://192.168.240.107/Sched_ABS/testing_sap_bw_sql.php?f=ZDFFM_TDE_REVISION_CHANGE_DATE&p1=$p1&p2=$p2");
//    }
//
//    public function update_pmorder_header($p1 = '', $p2 = '') {
//        error_reporting(0);
//        if ($p1 and $p2) {
//            // RESERVED
//        } else {
//            $p1 = $p2 = date('Ymd');
//        }
//        echo file_get_contents("http://192.168.240.107/Sched_ABS/testing_sap_bw_sql.php?f=ZDFFM_TDE_PMORDER_HEADER&p1=$p1&p2=$p2");
//    }
//
//    public function update_pmorder_item($p1 = '', $p2 = '') {
//        error_reporting(0);
//        if ($p1 and $p2) {
//            // RESERVED
//        } else {
//            $p1 = $p2 = date('Ymd');
//        }
//        echo file_get_contents("http://192.168.240.107/Sched_ABS/testing_sap_bw_sql.php?f=ZDFFM_TDE_PMORDER_ITEM&p1=$p1&p2=$p2");
//    }
//
//    public function get_longtext($AUFNR, $TEXTONLY = 1, $NL2BR = 0, $ERR = 0) {
//        error_reporting(0);
//        $json = '';
//        try {
//            $json = file_get_contents("http://192.168.240.107/Sched_ABS/testing_sap_longtext.php?order=$AUFNR");
//        } catch (Exception $ex) {
//            if ($ERR) {
//                echo json_encode($ex);
//            }
//        } finally {
//            if ($json and $TEXTONLY) {
//                $arout = array();
//                $raw = json_decode($json);
//                foreach ($raw as $value) {
//                    array_push($arout, $value->TDLINE);
//                }
//                echo implode(($NL2BR ? '<br />' : '\n'), $arout);
//            } else {
//                echo $json;
//            }
//        }
//    }

//    public function get_revision() {
//        $result = $this->sap->callFunction(
//                "ZFM_TDI_GET_REVISION_R", array(
//            array("IMPORT", "GV_REVBD_LOW", "20170101"),
//            array("IMPORT", "GV_REVBD_HIGH", "20170102"),
//            array("TABLE", "GI_HEADER", array())
//                )
//        );
//        if ($this->sap->getStatus() == SAPRFC_OK) {
//            echo json_encode($this->sap->getInterface());
//        } else {
//            $this->sap->printStatus();
//        }
//        $this->sap->logoff();
//    }
//    public function get_revision() {
//        if (!$this->rfc) {
//            echo "RFC connection failed";
//            exit;
//        }
//        $fce = saprfc_function_discover($this->rfc, "ZFM_TDI_GET_REVISION_R");
//        if (!$fce) {
//            echo "Discovering interface of function module failed";
//            exit;
//        }
//
//        saprfc_table_init($fce, "GI_HEADER");
//        saprfc_table_init($fce, "GI_HEADER2");
//
//        saprfc_table_append($fce, "ZFM_TDI_GET_REVISION_R", array(
//            "GV_REVBD_LOW" => "20170101",
//            "GV_REVBD_HIGH" => "20170102"
//        ));
//        $rfc_rc = saprfc_call_and_receive($fce);
//        if ($rfc_rc != SAPRFC_OK) {
//            if ($this->rfc == SAPRFC_EXCEPTION) {
//                echo ("Exception raised: " . saprfc_exception($fce));
//            } else {
//                echo (saprfc_error($fce));
//            }
//            exit;
//        }
//        $rows = saprfc_table_rows($fce, "GI_HEADER");
//        for ($i = 1; $i <= $rows; $i++) {
//            $GI_HEADER[] = saprfc_table_read($fce, "GI_HEADER", $i);
//        }
//        saprfc_function_free($fce);
//        saprfc_close($this->rfc);
//
//        echo json_encode($GI_HEADER);
//    }
//    public function get_longtext($AUFNR, $ID = 'KOPF', $OBJECT = 'AUFK') {
//        if (!$this->rfc) {
//            echo "RFC connection failed";
//            exit;
//        } //Discover interface for function module RFC_READ_TEXT 
//        $fce = saprfc_function_discover($this->rfc, "RFC_READ_TEXT");
//        if (!$fce) {
//            echo "Discovering interface of function module failed";
//            exit;
//        }
//        //Set import parameters. You can use function saprfc_optional() to mark parameter as optional. 
//        //Fill internal tables 
//        saprfc_table_init($fce, "MESSAGES");
//        saprfc_table_init($fce, "OBJECTLINKS");
//        saprfc_table_init($fce, "TEXT_LINES");
//        $pm_order = '500' . str_pad($AUFNR, 12, "0", STR_PAD_LEFT);
//        saprfc_table_append($fce, "TEXT_LINES", array("MANDT" => "500",
//            "TDOBJECT" => $OBJECT,
//            "TDNAME" => $pm_order,
//            "TDID" => $ID,
//            "TDSPRAS" => "EN",
//            "COUNTER" => "",
//            "TDFORMAT" => "",
//            "TDLINE" => ""));
//        $rfc_rc = saprfc_call_and_receive($fce);
//        if ($rfc_rc != SAPRFC_OK) {
//            if ($this->rfc == SAPRFC_EXCEPTION) {
//                echo ("Exception raised: " . saprfc_exception($fce));
//            } else {
//                echo (saprfc_error($fce));
//            }
//            exit;
//        }
//        $rows = saprfc_table_rows($fce, "TEXT_LINES");
//        for ($i = 1; $i <= $rows; $i++) {
//            $TEXT_LINES[] = saprfc_table_read($fce, "TEXT_LINES", $i);
//        } //Debug info // saprfc_function_debug_info($fce); 
//        saprfc_function_free($fce);
//        saprfc_close($this->rfc);
//
////        return $TEXT_LINES;
//        echo json_encode($TEXT_LINES);
//    }
//    public function get_longtext2($AUFNR, $ID = 'KOPF', $OBJECT = 'AUFK') {
//        $pm_order = $this->login['CLIENT'] . str_pad($AUFNR, 12, "0", STR_PAD_LEFT);
////        $result = $this->sap->callFunction(
////                "RFC_READ_TEXT", array(
////            array("IMPORT", "MANDT", "500"),
////            array("IMPORT", "TDOBJECT", "$OBJECT"),
////            array("IMPORT", "TDNAME", "$pm_order"),
////            array("IMPORT", "TDID", "$ID"),
////            array("IMPORT", "TDSPRAS", "EN"),
////            array("IMPORT", "COUNTER", ""),
////            array("IMPORT", "TDFORMAT", ""),
////            array("IMPORT", "TDLINE", ""),
////            array("TABLE", "TEXT_LINES", array())
////                )
////        );
//        $result = $this->sap->callFunction(
//                "READ_TEXT", array(
//            array("IMPORT", "OBJECT", "$OBJECT"),
//            array("IMPORT", "NAME", "$pm_order"),
//            array("IMPORT", "ID", "$ID"),
//            array("IMPORT", "LANGUAGE", "EN"),
//            array("TABLE", "LINES", array())
//                )
//        );
//        if ($this->sap->getStatus() == SAPRFC_OK) {
//            echo json_encode($result);
//        } else {
//            $this->sap->printStatus();
//        }
//        $this->sap->logoff();
//    }
}
