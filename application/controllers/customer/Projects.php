<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require_once('assets/editablegrid/EditableGrid.php');

class Projects extends CI_Controller {

	public function __construct(){
		 parent::__construct();
       		$this->load->database();
       		$this->load->model('Project_model_ci', '', TRUE);
       		$this->load->model('Dashboard_model', '', TRUE);
       		$this->load->model('Project_model', '', TRUE);
       		$this->load->model('Jobcard_model_view', '', TRUE);
       		$this->load->model('User_group_model');
       		$this->load->model('Mdr_model_view');
        	$this->load->model('Menu_access_view_model', 'Menu');
        	$this->load->helper('auth_helper');
        	$this->load->library('menu_lib');

		// Cek User Seesion
		$this->data["session"] = $this->session->userdata('logged_in');

		has_session($this->data["session"]);

		$GROUP_ID = $this->data["session"]["group_id"];
		$user_menu['side_bar'] = $this->Menu->getMenuByGroup($GROUP_ID, 'S');
		$user_menu['tab_menu'] = $this->menu_lib->project_tab_menu_by_group($GROUP_ID);
		// $user_menu['side_bar'] = $this->menu_lib->project_side_bar_by_group($GROUP_ID);

		$this->session->set_userdata('menu', $user_menu);
	}

	public function dashboard(){
		try {
			if ($this->uri->segment(4)) {
				$this->session->set_userdata('REVNR', $this->uri->segment(4));
			}
			$data['id_project'] = $this->session->userdata('REVNR');
			$active_prj = $this->Project_model_ci->getProjectName($data['id_project']);
			if ($active_prj) {
				$data['project_name'] = $active_prj->REVTX;
			}
		} catch (Exception $e) {
			error_log($e);
		}
		$data["session"] = $this->session->userdata('logged_in');
		$data['content'] = 'customer/Dashboard';

		 $data['group'] = $this->User_group_model->where('ID_USER_GROUP', $data["session"]['group_id'])->first();

        	// Availiable Menu
        	$data['project_tab'] = $this->Menu->getAccessedMenu($data["session"]['group_id'], 'T');
		$this->load->view('template', $data);	
	}


	// START : Job Card
	public function jobcard(){
		try {
			if ($this->uri->segment(3)) {
				$this->session->set_userdata('REVNR', $this->uri->segment(3));
			}
			$data['id_project'] = $this->session->userdata('REVNR');
			$active_prj = $this->Project_model_ci->getProjectName($data['id_project']);
			if ($active_prj) {
				$data['project_name'] = $active_prj->REVTX;
			}
		} catch (Exception $e) {
			error_log($e);
		}
		$data["session"] = $this->session->userdata('logged_in');
		$data['content'] = 'customer/view_jobcard';
		$this->load->view('template', $data);	
	}


	public function load_jobcard() {
		$data["session"] = $this->session->userdata('logged_in');

        	$grid = new EditableGrid();
        
	        $grid->addColumn('AUFNR', 'Order', 'html', NULL, false);
	        $grid->addColumn('KTEXT', 'Description', 'string', NULL, false);
	        $grid->addColumn('CUS_JC_NUM_REAL', 'Cust JC Num', 'string', NULL, false);
	        $grid->addColumn('STATUS', 'STATUS', 'string', NULL, false);

	        $active_job = $this->Jobcard_model_view
                ->selectRaw('*,'
                        . 'AUFNR AS id,'
                        . 'SUBSTRING(KTEXT, '
                        . ((($this->db->dbdriver == 'sqlsrv') or ( $this->db->dbdriver == 'mssql')) ? "CHARINDEX('[',KTEXT)" : "LOCATE('[',KTEXT)") . '+1, ABS('
                        . ((($this->db->dbdriver == 'sqlsrv') or ( $this->db->dbdriver == 'mssql')) ? "CHARINDEX(']',KTEXT)" : "LOCATE(']',KTEXT)") . '-'
                        . ((($this->db->dbdriver == 'sqlsrv') or ( $this->db->dbdriver == 'mssql')) ? "CHARINDEX('[',KTEXT)" : "LOCATE('[',KTEXT)") . '-1)) AS ITVAL_REAL,'
                        . 'SUBSTRING(KTEXT, '
                        . ((($this->db->dbdriver == 'sqlsrv') or ( $this->db->dbdriver == 'mssql')) ? "CHARINDEX(']',KTEXT)" : "LOCATE(']',KTEXT)") . '+1, ABS('
                        . ((($this->db->dbdriver == 'sqlsrv') or ( $this->db->dbdriver == 'mssql')) ? "CHARINDEX(':',KTEXT)" : "LOCATE(':',KTEXT)") . '-'
                        . ((($this->db->dbdriver == 'sqlsrv') or ( $this->db->dbdriver == 'mssql')) ? "CHARINDEX(']',KTEXT)" : "LOCATE(']',KTEXT)") . '-1)) AS CUS_JC_NUM_REAL,'
                        . ((($this->db->dbdriver == 'sqlsrv') or ( $this->db->dbdriver == 'mssql')) ? "'<a href=\"" . base_url() . "projects/crud_mdr/' + REVNR + '/' + AUFNR + '\">' + AUFNR + '</a>' AS AUFNR_LINK," : "CONCAT('<a href=\"" . base_url() . "admin/projects/crud_mdr/', REVNR, '/', AUFNR, '\">', AUFNR, '</a>') AS AUFNR_LINK,")
                        . '(select count(*) from V_MDR_PROGRESS where V_MDR_PROGRESS.MAUFNR = V_JOBCARD_PROGRESS.AUFNR) AS JC_REFF_MDR_VAL')
                ->where('REVNR', $this->session->userdata('REVNR'))
                ->where('AUART', 'GA01');

	        if ($this->session->userdata('jc_id') != NULL) {
	            $active_job = $active_job->where('AUFNR', $this->session->userdata('jc_id'));
	        }

	        $totalUnfiltered = $active_job->count();
	        $total = $totalUnfiltered;

	        $page = 0;
	        if (isset($_GET['page']) && is_numeric($_GET['page']))
	            $page = (int) $_GET['page'];


	        $rowByPage = $totalUnfiltered; // 50;

	        $from = ($page - 1) * $rowByPage;

	        if (isset($_GET['filter']) && $_GET['filter'] != "") {
	            $filter = $_GET['filter'];
	            $active_job = $active_job
	                    ->where('AUFNR', 'like', '%' . $filter . '%')
	                    ->orwhere('KTEXT', 'like', '%' . $filter . '%')
	                    ->orwhere('CUST_JC_NUM', 'like', '%' . $filter . '%')              
	                    ->orwhere('STATUS', 'like', '%' . $filter . '%');
	            $total = $active_job->count();
	        }

	        if (isset($_GET['sort']) && $_GET['sort'] != "") {
	            $active_job = $active_job->orderBy($_GET['sort'], $_GET['asc'] == "0" ? "DESC" : "ASC");
	        } else {
	            $active_job = $active_job->orderBy('AUFNR', 'ASC');
	        }

	        $active_job = $active_job->skip($from)->take($rowByPage);
	        $grid->setPaginator(ceil($total / $rowByPage), (int) $total, (int) $totalUnfiltered, null);
	        $grid->renderJSON($active_job->get(), false, false, !isset($_GET['data_only']));
	}

	// END : Jobcard

	// START: mdr
	public function mdr(){
		try {
	            if ($this->uri->segment(3)) {
	                $this->session->set_userdata('REVNR', $this->uri->segment(3));
	            }
	            $data['id_project'] = $this->session->userdata('REVNR');
	            $active_prj = $this->Project_model_ci->getProjectName($data['id_project']);
	            if ($active_prj) {
	                $data['project_name'] = $active_prj->REVTX;
	            }
	        } catch (Exception $e) {
	            error_log($e);
	        }

	        try {
	            if ($this->uri->segment(4)) {
	                $this->session->set_userdata('jc_id', $this->uri->segment(4));
	            } else {
	                $this->session->unset_userdata('jc_id');
	            }

	            if ($this->uri->segment(5)) {
	                $this->session->set_userdata('mdr_id', $this->uri->segment(5));
	            } else {
	                $this->session->unset_userdata('mdr_id');
	            }    

	            $data['jc_id'] = $this->session->userdata('jc_id');
	        } catch (Exception $e) {
	            error_log($e);
	        }

	        $data["session"] = $this->session->userdata('logged_in');
	        $data['content'] = 'customer/view_mdr';

	        $this->load->view('template', $data);	
	}

	public function load_mdr() {
	        $data["session"] = $this->session->userdata('logged_in');

	        $grid = new EditableGrid();

	        $grid->addColumn('SEQ_NUM', 'Seq', 'integer', NULL, false);
	        $grid->addColumn('AUFNR', 'Order', 'html', NULL, false);
	        $grid->addColumn('MAUFNR_LINK', 'JC REFF', 'html', NULL, false);
	        $grid->addColumn('CUST_JC_NUM', 'Task Card', 'string', NULL, false);
	        $grid->addColumn('KTEXT', 'Discrepancies', 'string', NULL, false);
	        $grid->addColumn('STATUS', 'Status', 'string', NULL, false);

	        $active_job = $this->Mdr_model_view
	                ->selectRaw('*,'
	                        . 'AUFNR AS id,'
	                        . ((($this->db->dbdriver == 'sqlsrv') or ( $this->db->dbdriver == 'mssql')) ? "'<a href=\"" . base_url() . "projects/crud_mrm/' + REVNR + '/' + AUFNR + '\">' + AUFNR + '</a>' AS AUFNR_LINK," : "CONCAT('<a href=\"" . base_url() . "projects/crud_mrm/', REVNR, '/', AUFNR, '\">', AUFNR, '</a>') AS AUFNR_LINK,")
	                        . ((($this->db->dbdriver == 'sqlsrv') or ( $this->db->dbdriver == 'mssql')) ? "'<a href=\"" . base_url() . "projects/crud_jobcard/' + REVNR + '/' + MAUFNR + '\">' + MAUFNR + '</a>' AS MAUFNR_LINK," : "CONCAT('<a href=\"" . base_url() . "projects/crud_mrm/', REVNR, '/', MAUFNR, '\">', MAUFNR, '</a>') AS MAUFNR_LINK,")
	                        . '(select AREA from V_JOBCARD_PROGRESS where V_MDR_PROGRESS.MAUFNR = V_JOBCARD_PROGRESS.AUFNR) AS AREA,'
	                        . '(select SKILL_TYPE from V_JOBCARD_PROGRESS where V_MDR_PROGRESS.MAUFNR = V_JOBCARD_PROGRESS.AUFNR) AS SKILL_TYPE')
	                ->where('REVNR', $this->session->userdata('REVNR'))
	                ->orderBy('AUFNR', 'ASC');

	       if ($this->session->userdata('mdr_id') != NULL) {
	            $active_job = $active_job->where('AUFNR', $this->session->userdata('mdr_id'));
	        } 

	        if ($this->session->userdata('jc_id') != NULL) {
	            $active_job = $active_job->where('MAUFNR', $this->session->userdata('jc_id'));
	        }

	        $totalUnfiltered = $active_job->count();
	        $total = $totalUnfiltered;

	        $page = 0;
	        if (isset($_GET['page']) && is_numeric($_GET['page']))
	            $page = (int) $_GET['page'];


	        $rowByPage = $totalUnfiltered; // 50;

	        $from = ($page - 1) * $rowByPage;

	        if (isset($_GET['filter']) && $_GET['filter'] != "") {
	            $filter = $_GET['filter'];
	            $active_job = $active_job
	                    ->where('AUFNR', 'like', '%' . $filter . '%')
	                    ->orwhere('KTEXT', 'like', '%' . $filter . '%')
	                    ->orwhere('CUST_JC_NUM', 'like', '%' . $filter . '%')
	                    ->orwhere('AUART', 'like', '%' . $filter . '%')
	                    ->orwhere('ITVAL', 'like', '%' . $filter . '%')
	                    ->orwhere('ARBEI', 'like', '%' . $filter . '%')
	                    ->orwhere('ILART', 'like', '%' . $filter . '%')
	                    ->orwhere('AREA', 'like', '%' . $filter . '%')
	                    ->orwhere('PHASE', 'like', '%' . $filter . '%')
	                    ->orwhere('DAY', 'like', '%' . $filter . '%')
	                    ->orwhere('STATUS', 'like', '%' . $filter . '%')
	                    ->orwhere('DATECLOSE', 'like', '%' . $filter . '%')
	                    ->orwhere('REMARK', 'like', '%' . $filter . '%')
	                    ->orwhere('FREETEXT', 'like', '%' . $filter . '%')
	                    ->orwhere('DATEPROGRESS', 'like', '%' . $filter . '%');
	            $total = $active_job->count();
	        }

	        if (isset($_GET['sort']) && $_GET['sort'] != "") {
	            $active_job = $active_job->orderBy($_GET['sort'], $_GET['asc'] == "0" ? "DESC" : "ASC");
	        } else {
	            $active_job = $active_job->orderBy('SEQ_NUM', 'ASC');
	        }

	        $active_job = $active_job->skip($from)->take($rowByPage);
	        $grid->setPaginator(ceil($total / $rowByPage), (int) $total, (int) $totalUnfiltered, null);
	        $grid->renderJSON($active_job->get(), false, false, !isset($_GET['data_only']));
    	}

	// END : mdr


	// START : mrm
	public function mrm() {
	try {
	    if ($this->uri->segment(3)) {
	        $this->session->set_userdata('REVNR', $this->uri->segment(3));
	    }
	    $data['id_project'] = $this->session->userdata('REVNR');
	    $active_prj = $this->Project_model_ci->getProjectName($data['id_project']);
	    if ($active_prj) {
	        $data['project_name'] = $active_prj->REVTX;
	    }
	} catch (Exception $e) {
	    error_log($e);
	}

	try {
	    if ($this->uri->segment(4)) {
	        $this->session->set_userdata('jc_id', $this->uri->segment(4));
	    } else {
	        $this->session->unset_userdata('jc_id');
	    }
	    $data['jc_id'] = $this->session->userdata('jc_id');
	} catch (Exception $e) {
	    error_log($e);
	}

	try {
	    if ($this->uri->segment(5)) {
	        $this->session->set_userdata('mrm_id', $this->uri->segment(5));
	    } else {
	        $this->session->unset_userdata('mrm_id');
	    }
	    $data['mrm_id'] = $this->session->userdata('mrm_id');
	} catch (Exception $e) {
	    error_log($e);
	}


	$data["session"] = $this->session->userdata('logged_in');
	$data['content'] = 'customer/view_mrm';

	$this->load->view('template', $data);
	}

	public function load_mrm() {
		$data["session"] = $this->session->userdata('logged_in');

		$grid = new EditableGrid();


		$grid->addColumn('SEQ_NUM', 'Seq', 'integer', NULL, false);
		$grid->addColumn('MATNR', 'Part Number', 'string', NULL, false);
		// $grid->addColumn('MATNR', 'Alternate Part Number', 'string');
		$grid->addColumn('MAKTX', 'Material Description', 'html', NULL, false);
		$grid->addColumn('KTEXT', 'Discrepancies', 'string');
		$grid->addColumn('MTART', 'Mat Type', 'string', array("EXP" => "EXP", "ROT" => "ROT", "REP" => "REP", "RAW" => "RAW", "CON" => "CON"));
		$grid->addColumn('IPC', 'IPC', 'string');
		$grid->addColumn('AUFNR_LINK', 'Order Number', 'html', NULL, false);
		$grid->addColumn('STO_NUM', 'STO Number', 'string');
		$grid->addColumn('OUTBOND_DELIV', 'Outbound Delivery', 'string');
		$grid->addColumn('TO_NUM', 'TO Number', 'string');
		$grid->addColumn('CUST_JC_NUM', 'Jobcard Number', 'string');
		$grid->addColumn('CARD_TYPE', 'Card Type', 'string', array("JC" => "JC", "MDR" => "MDR", "AD" => "AD"));
		$grid->addColumn('MRM_Issue_Date', 'MRM Issue Date', 'datetime', NULL, false);
		$grid->addColumn('MENGE', 'Qty Req Single Item', 'string');
		$grid->addColumn('TOTAL_QTY', 'Total Qty Required for All Job Task', 'string', NULL, false);
		$grid->addColumn('MEINS', 'UOM', 'string', array("FT" => "FT", "E/A" => "E/A", "L" => "L", "LB" => "LB", "QT" => "QT", "ROL" => "ROL", "M" => "M"));
		$grid->addColumn('STOCK_MANUAL', 'Input Stock Manually', 'string');
		$grid->addColumn('LGORT', 'Storage Location', 'string');
		$grid->addColumn('MAT_FULLFILLMENT_STATUS', 'Material Fulfillment Status', 'string', array("DLVR FULL to Production" => "DLVR FULL to Production", " DLVR PARTIAL to Production" => "DLVR PARTIAL to Production", "WAITING Customer Supply" => "WAITING Customer Supply", "ROBBING Desicion" => "ROBBING Desicion", "PROVISION in Store" => "PROVISION in Store", "PRELOADED in Hangar Store" => "PRELOADED in Hangar Store", "ORDERED by Purchasing" => "ORDERED by Purchasing", "ORDERED by Loan Control" => "ORDERED by Loan Control", "ORDERED by Workshop" => "ORDERED by Workshop", "EXCHANGE in Progress" => "EXCHANGE in Progress", "Actual STOCK NIL" => "Actual STOCK NIL", "RIC/Part on Receiving Area" => "RIC/Part on Receiving Area", "NO SOURCE" => "NO SOURCE", "Waiting PN Confrimation" => "Waiting PN Confrimation", "GR OK" => "GR OK", "Need FOLLOW UP" => "Need FOLLOW UP", "PENDING/NO ACTION" => "PENDING/NO ACTION", "PR Ready" => "PR Ready", "POOLING" => "POOLING", "Shipment" => "Shipment", "Custom Process" => "Custom Process", "Need SOA" => "Need SOA", "WAITING Customer Approval" => "WAITING Customer Approval"));
		$grid->addColumn('FULLFILLMENT_STATUS_DATE', 'Fullfillment Status Date', 'date');
		$grid->addColumn('FULLFILLMENT_REMARK', 'Fullfillment remark', 'html');
		$grid->addColumn('PO_DATE', 'Date Of PO', 'date');
		$grid->addColumn('PO_NUM', 'Purchase Order PO', 'string');
		$grid->addColumn('AWB_NUM', 'AWB Number', 'string');
		$grid->addColumn('AWB_DATE', 'AWB Date', 'date');
		$grid->addColumn('QTY_DELIV', 'Qty Delivered', 'string');
		$grid->addColumn('QTY_REMAIN', 'Qty Remain', 'string');
		$grid->addColumn('MATERIAL_REMARK', 'Material Remark', 'html');
		$grid->addColumn('action', 'Action', 'html', NULL, false, 'id');

		$active_job = $this->Mrm_model_view->selectRaw('*,'
		                . 'ID AS id,'
		                . "CASE WHEN CARD_TYPE LIKE 'MDR' 
		                    THEN 
		                        '<a href=\"" . base_url() . "projects/crud_mdr/' + REVNR + '/' + MAUFNR + '/' + AUFNR + '\">' + AUFNR + '</a>'  
		                    ELSE '<a href=\"" . base_url() ."projects/crud_jobcard/' + REVNR + '/' + AUFNR + '/ \">' + AUFNR + ' </a>' 
		                    END AS AUFNR_LINK,"
		                . 'SUBSTRING(KTEXT, 6, 15) AS CUST_JC_NUM,'
		                . '(select SUM(MENGE) from M_PMORDER WHERE MATNR = V_MRM.MATNR) as TOTAL_QTY'
		        )
		        ->where('REVNR', $this->session->userdata('REVNR'))
		        ->orderBy('AUFNR', 'ASC');

		if ($this->session->userdata('jc_id') != NULL) {
		    $active_job = $active_job->where('AUFNR', $this->session->userdata('jc_id'));
		}

		$totalUnfiltered = $active_job->count();
		$total = $totalUnfiltered;

		$page = 0;
		if (isset($_GET['page']) && is_numeric($_GET['page']))
		    $page = (int) $_GET['page'];


		$rowByPage = $totalUnfiltered; // 50;

		$from = ($page - 1) * $rowByPage;

		if (isset($_GET['filter']) && $_GET['filter'] != "") {
		    $filter = $_GET['filter'];
		    $active_job = $active_job
		            ->where('MATNR', 'like', '%' . $filter . '%')
		            ->orwhere('MAKTX', 'like', '%' . $filter . '%')
		            ->orwhere('MTART', 'like', '%' . $filter . '%')
		            ->orwhere('IPC', 'like', '%' . $filter . '%')
		            ->orwhere('AUFNR', 'like', '%' . $filter . '%')
		            ->orwhere('STO_NUM', 'like', '%' . $filter . '%')
		            ->orwhere('OUTBOND_DELIV', 'like', '%' . $filter . '%')
		            ->orwhere('TO_NUM', 'like', '%' . $filter . '%')
		            ->orwhere('CARD_TYPE', 'like', '%' . $filter . '%')
		            ->orwhere('MENGE', 'like', '%' . $filter . '%')
		            ->orwhere('MEINS', 'like', '%' . $filter . '%')
		            ->orwhere('LGORT', 'like', '%' . $filter . '%')
		            ->orwhere('MAT_FULLFILLMENT_STATUS', 'like', '%' . $filter . '%')
		            ->orwhere('FULLFILLMENT_STATUS_DATE', 'like', '%' . $filter . '%')
		            ->orwhere('FULLFILLMENT_REMARK', 'like', '%' . $filter . '%')
		            ->orwhere('PO_DATE', 'like', '%' . $filter . '%')
		            ->orwhere('PO_NUM', 'like', '%' . $filter . '%')
		            ->orwhere('AWB_NUM', 'like', '%' . $filter . '%')
		            ->orwhere('AWB_DATE', 'like', '%' . $filter . '%')
		            ->orwhere('QTY_DELIV', 'like', '%' . $filter . '%')
		            ->orwhere('QTY_REMAIN', 'like', '%' . $filter . '%')
		            ->orwhere('MATERIAL_REMARK', 'like', '%' . $filter . '%')
		    ;
		    $total = $active_job->count();
		}

		if (isset($_GET['sort']) && $_GET['sort'] != "") {
		    $active_job = $active_job->orderBy($_GET['sort'], $_GET['asc'] == "0" ? "DESC" : "ASC");
		} else {
		    $active_job = $active_job->orderBy('SEQ_NUM', 'ASC');
		}

		$active_job = $active_job->skip($from)->take($rowByPage);
		$grid->setPaginator(ceil($total / $rowByPage), (int) $total, (int) $totalUnfiltered, null);
		$grid->renderJSON($active_job->get(), false, false, !isset($_GET['data_only']));		
	}	
	// END : mrm


	// START : CSP
	public function csp() {
	        try {
	            if ($this->uri->segment(4)) {
	                $this->session->set_userdata('REVNR', $this->uri->segment(4));
	            }
	            $data['id_project'] = $this->session->userdata('REVNR');
	            $active_prj = $this->Project_model_ci->getProjectName($data['id_project']);
	            if ($active_prj) {
	                $data['project_name'] = $active_prj->REVTX;
	            }
	        } catch (Exception $e) {
	            error_log($e);
	        }

	        $this->Project_model_ci->executeCSP();


	        $data["session"] = $this->session->userdata('logged_in');
	        $data['content'] = 'admin/projects/crud_customer_supply';

	        $this->load->view('template', $data);
    	}

    public function load_csp() {
        $data["session"] = $this->session->userdata('logged_in');

        $grid = new EditableGrid();

        $grid->addColumn('SEQ_NUM', 'MRM', 'integer',NULL, false);
        $grid->addColumn('MATNR', 'Part Number', 'string',NULL, false);
        $grid->addColumn('MAKTX', 'Part Description', 'string',NULL, false);
        $grid->addColumn('SERIAL_NO', 'Serial No', 'string' );
        $grid->addColumn('BDMNG', 'Qty', 'string' ,NULL, false);
        $grid->addColumn('TOTAL_QTY', 'Total Qty', 'string' ,NULL, false);
        $grid->addColumn('AUFNR_LINK', 'Order Number', 'html' ,NULL, false);
       // $grid->addColumn('CUST_JC_NUM', 'AAI TaskCard', 'string' ,NULL, false);
        $grid->addColumn('CARD_TYPE', 'Type', 'string' ,NULL, false);
        $grid->addColumn('KTEXT', 'Discrepancies', 'string' ,NULL, false);
        $grid->addColumn('AWB_NUM', 'AWB Number', 'string' ,NULL, false);
        $grid->addColumn('COMMENT_AAI', 'Comment AAI', 'string');
        $grid->addColumn('COMMENT_GMF', 'Comment GMF', 'string');
        $grid->addColumn('STATUS', 'Status', 'string', array("OPEN" => "OPEN", "CLOSED" => "CLOSED"));
        
        $grid->addColumn('action', 'Action', 'html', NULL, false, 'id');

        $data['id_project'] = $this->session->userdata('REVNR');
        $active_job = $this->Csr_model_view->selectRaw('*,'
                    .'(select SUM(CAST(CAST(BDMNG AS FLOAT) as DECIMAL(4, 2))) from V_MRM WHERE MATNR = V_CSR.MATNR and AUFNR = V_CSR.AUFNR and MAT_FULLFILLMENT_STATUS = V_CSR.MAT_FULLFILLMENT_STATUS) as TOTAL_QTY ,'
                    . ((($this->db->dbdriver == 'sqlsrv') or ( $this->db->dbdriver == 'mssql')) ? 
                    "'<a href=\"" . base_url() . "projects/crud_mrm/' + REVNR + '/' + AUFNR + '\">' + AUFNR + '</a>' AS AUFNR_LINK" : 
                    "CONCAT('<a href=\"" . base_url() . "projects/crud_mrm/', REVNR, '/', AUFNR, '\">', AUFNR, '</a>') AS AUFNR_LINK"))
                ->where('REVNR', $data['id_project']);    

        $totalUnfiltered = $active_job->count();
        $total = $totalUnfiltered;

        $page = 0;
        if (isset($_GET['page']) && is_numeric($_GET['page']))
            $page = (int) $_GET['page'];


        $rowByPage = $totalUnfiltered; // 50;

        $from = ($page - 1) * $rowByPage;

        if (isset($_GET['filter']) && $_GET['filter'] != "") {
            $filter = $_GET['filter'];
            $active_job = $active_job
                    ->where('SEQ_NUM', 'like', '%' . $filter . '%')
                    ->orwhere('MATNR', 'like', '%' . $filter . '%')
                    ;
            $total = $active_job->count();
        }

        if (isset($_GET['sort']) && $_GET['sort'] != "" && in_array($_GET['sort'], $grid->getColumnFields())) {
            $active_job = $active_job->orderBy($_GET['sort'], $_GET['asc'] == "0" ? "DESC" : "ASC");
        } else {
            $active_job = $active_job->orderBy('SEQ_NUM', 'ASC');
        }

        $active_job = $active_job->skip($from)->take($rowByPage);
        $grid->setPaginator(ceil($total / $rowByPage), (int) $total, (int) $totalUnfiltered, null);

        $grid->renderJSON($active_job->get(), false, false, !isset($_GET['data_only']));
    }	
	// END: CSP
}