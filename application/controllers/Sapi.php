<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Sap
 *
 * @author Miftachur Rozaq
 */
class Sapi extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->database();
        $this->load->model('RFCLog_model');
        $this->load->helper('text_formatter');

        ini_set("default_socket_timeout", 600);
    }


    public function download(){
        $data["session"] = $this->session->userdata('logged_in');
        $data['content'] = 'download';
        $this->load->view('template', $data);
    }

    public function create_job($n = 'ZDIFO_TDE01_RFC', $v = 'PM_TB') {
        $data["session"] = $this->session->userdata('logged_in');
        $CREATED_BY = $data["session"]["name"];
               
        $dot['msg'] = NULL;
        $dot['body'] = NULL;
        $rl = $this->RFCLog_model
                ->where('JOBNAME', $n)
                ->where('VARIANT', $v)
                ->where('RFCSTATUS', 'CREATED');
        if ($rl->count() == 0) {
            $din = file_get_contents("http://192.168.240.107/sched_abs/create_bgjob.php?n=$n&v=$v");
            if ($din) {
                $din_json = json_decode($din);
                if ($din_json) {
                    $wl = $this->RFCLog_model
                            ->create([
                        'JOBNAME' => $n,
                        'VARIANT' => $v,
                        'RFCSTATUS' => 'CREATED',
                        'JOBCOUNT' => $din_json->JOBCOUNT,
                        'EXEC_BY' => $CREATED_BY,
                        'REVISION' => 'ALL REVISION'
                    ]);
                    $dot['msg'] = 'success';
                    $dot['body'] = $wl;
                }
            }
        } else {
            $dot['msg'] = 'error';
            $dot['body'] = 'Job already running';
        }
        echo json_encode($dot);

    }

    public function create_job_by_revision($n = 'ZDIFO_TDE01_RFC_REVNR', $v = 'PM_TB_REVISION') {
        $data["session"] = $this->session->userdata('logged_in');
        $CREATED_BY = $data["session"]["name"];
        $P3 = $this->input->get('p3');              
        $dot['msg'] = NULL;
        $dot['body'] = NULL;
        $rl = $this->RFCLog_model
                ->where('JOBNAME', $n)
                ->where('VARIANT', $v)
                ->where('RFCSTATUS', 'CREATED');
        if ($rl->count() == 0) {            
            $din = file_get_contents("http://192.168.240.107/sched_abs/create_bgjob2.php?p3=$P3");
            if ($din) {
                $din_json = json_decode($din);
                if ($din_json) {
                    $wl = $this->RFCLog_model
                            ->create([
                        'JOBNAME' => $n,
                        'VARIANT' => $v,
                        'RFCSTATUS' => 'CREATED',
                        'JOBCOUNT' => $din_json->JOBCOUNT,
                        'EXEC_BY' => $CREATED_BY,
                        'REVISION' => $P3
                    ]);
                    $dot['msg'] = 'success';
                    $dot['body'] = $wl;
                }
            }
        } else {
            $dot['msg'] = 'error';
            $dot['body'] = 'Job already running';
        }
        echo json_encode($dot);

    }

    public function read_job() {
        $dot['msg'] = NULL;
        $dot['body'] = NULL;
        $ids = array();
        $rl = $this->RFCLog_model->where('RFCSTATUS', 'CREATED')->get();
//        $rl_datas = $rl-get();
        foreach ($rl as $rl_data) {
            $din = file_get_contents("http://192.168.240.107/sched_abs/read_bgjob.php?n=$rl_data->JOBNAME&c=$rl_data->JOBCOUNT");
            if ($din) {
                $din_json = json_decode($din);
                if ($din_json) {
                    $dot['msg'] = 'success';
//                    $dot['body'][$rl_data->ID] = $din_json;
                    array_push($ids, $rl_data->ID);
                    if ($din_json->JOBSTATUS == 'F') {
                        $this->RFCLog_model->where('ID', $rl_data->ID)->update([
                            'RFCSTATUS' => 'DONE',
                            'JOBSTATUS' => $din_json->JOBSTATUS,
                            'ENDTIME' => raw_datetime_format($din_json->JOBENDDATE . $din_json->JOBENDTIME)
                        ]);
                    } elseif ($din_json->JOBSTATUS == 'A') {
                        $this->RFCLog_model->where('ID', $rl_data->ID)->update([
                            'RFCSTATUS' => 'CANCEL',
                            'JOBSTATUS' => $din_json->JOBSTATUS,
                            'ENDTIME' => raw_datetime_format($din_json->JOBENDDATE . $din_json->JOBENDTIME)
                        ]);
                    }
                }
            }
        }
        if ($ids) {
            $dot['body'] = $this->RFCLog_model->whereIn('ID', $ids)->get();
        }
        echo json_encode($dot);
    }

    public function update_revision($p1 = '', $p2 = '') {
//        error_reporting(0);
        if ($p1 and $p2) {
            // RESERVED
        } else {
//            $p1 = date('Ymd', strtotime("-1 days"));
            $p1 = $p2 = date('Ymd');
        }
        echo file_get_contents("http://192.168.240.107/Sched_ABS/testing_sap_bw_sql.php?f=ZDFFM_TDE_REVISION&p1=$p1&p2=$p2");
    }

    public function update_revision_change($p1 = '', $p2 = '') {
//        error_reporting(0);
        if ($p1 and $p2) {
            // RESERVED
        } else {
//            $p1 = date('Ymd', strtotime("-1 days"));
            $p1 = $p2 = date('Ymd');
        }
//        echo file_get_contents("http://192.168.240.107/Sched_ABS/testing_sap_bw_sql.php?t=RFC REVISION&f=ZDFFM_TDE_REVISION__CHANGE_DATE&p1=$p1&p2=$p2");
        echo file_get_contents("http://192.168.240.107/Sched_ABS/testing_sap_bw_sql.php?f=ZDFFM_TDE_REVISION_CHANGE_DATE&p1=$p1&p2=$p2");
    }

    public function update_pmorder_header($p1 = '', $p2 = '') {
//        error_reporting(0);
        if ($p1 and $p2) {
            // RESERVED
        } else {
//            $p1 = date('Ymd', strtotime("-1 days"));
            $p1 = $p2 = date('Ymd');
        }
        echo file_get_contents("http://192.168.240.107/Sched_ABS/testing_sap_bw_sql.php?f=ZDFFM_TDE_PMORDER_HEADER&p1=$p1&p2=$p2");
    }

    public function update_pmorder_item($p1 = '', $p2 = '') {
//        error_reporting(0);
        if ($p1 and $p2) {
            // RESERVED
        } else {
//            $p1 = date('Ymd', strtotime("-1 days"));
            $p1 = $p2 = date('Ymd');
        }
        echo file_get_contents("http://192.168.240.107/Sched_ABS/testing_sap_bw_sql.php?f=ZDFFM_TDE_PMORDER_ITEM&p1=$p1&p2=$p2");
    }

    public function get_longtext($AUFNR, $TEXTONLY = 1, $NL2BR = 0, $ERR = 0) {
        error_reporting(0);
        $json = '';
        try {
            $json = file_get_contents("http://192.168.240.107/Sched_ABS/testing_sap_longtext.php?order=$AUFNR");
        } catch (Exception $ex) {
            if ($ERR) {
                echo json_encode($ex);
            }
        } finally {
            if ($json and $TEXTONLY) {
                $arout = array();
                $raw = json_decode($json);
                foreach ($raw as $value) {
                    array_push($arout, $value->TDLINE);
                }
                echo implode(($NL2BR ? '<br />' : '\n'), $arout);
            } else {
                echo $json;
            }
        }
    }

    public function get_job_list(){
        $response["status"] = NULL;
        $response["body"] = NULL;

			try {
				$job_list = $this->RFCLog_model->orderBy('DTIME', 'DESC')->get();
				if($job_list){
						$response["status"] = 'success';
        				$response["body"] = $job_list;
				}else{
						$response["status"] = 'error';
        				$response["body"] = NULL;
				}

			} catch (Exception $e) {
					$response["status"] = 'error';
    				$response["body"] = NULL;				
			}

			header('Content-Type: application/json');
        	echo json_encode($response);
    }

}
