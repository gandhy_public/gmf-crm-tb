<?php
ini_set('max_execution_time', 9999999);
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set("Asia/Jakarta");

require_once('assets/editablegrid/EditableGrid.php');

class Ticket extends CI_Controller
{
	public function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->library('menu_lib');
        $this->load->model('Project_model_ci', '', TRUE);
        $this->load->model('Project_model');
        $this->load->model('Revh_model');
        $this->load->model('Management_model');
        $this->load->model('VProject_model');
        $this->load->model('Customer_model');
        $this->load->model('Dashboard_model', '', TRUE);
        $this->load->model('Jobcard_model');
        $this->load->model('Order_model');
        $this->load->model('OrderH_model');
        $this->load->model('Proc_model');
        $this->load->model('Jobcard_model_view');
        $this->load->model('Mdr_model');
        $this->load->model('Mdr_model_view');
        $this->load->model('Mrm_model');
        $this->load->model('Mrm_model_view');
        $this->load->model('Prm_model');
        $this->load->model('Prm_model_view');
        $this->load->model('Crm_model');
        $this->load->model('Ticket_model');
        $this->load->model('Csr_model');
        $this->load->model('Csr_model_view');
        $this->load->model('Daily_menu_model_view');
        $this->load->model('Daily_menu_model_ci');
        $this->load->model('Daily_day_model');
        $this->load->model('Daily_day_model_view');
        $this->load->model('Daily_area_model');
        $this->load->model('Daily_details_model');
        $this->load->model('Maint_phase_model');
        $this->load->model('Maint_milestone_model');
        $this->load->model('Daily_day_progress_model');
        $this->load->model('Users');
        $this->load->model('User_group_model');
        $this->load->model('Phase_model');
        $this->load->model('Phase_model_view');
        $this->load->model('Material_model');
        $this->load->helper('auth_helper');
        $this->load->helper('text_formatter');
		$this->load->model('Api_Proc_model');
        // $this->output->enable_profiler(TRUE);
        // Cek User Seesion
        $this->data["session"] = $this->session->userdata('logged_in');
        
        $this->is_customer = is_customer($this->data["session"]["user_group"]);

        has_session($this->data["session"]);

        $menu_tab = $this->session->userdata('menu_tab');
        if($menu_tab){
            $active_menu = $this->router->fetch_class() . '/' . $this->router->fetch_method();
            //sementara
            if($active_menu=="projects/crud_mdr_new"){
                $active_menu="projects/crud_mdr";
            }
            //sementara
            $idx = array_search($active_menu, array_column($menu_tab, "l"));
            $this->is_write = $menu_tab[$idx]["w"] == 1 ? 1 : 0;
        }
    }
	
	function view($revnr = null)
	{
		if($revnr == null){
			echo ("<script language='JavaScript'>
			    window.alert('Number REVNR is required');
			    window.location.href='".base_url('projects')."';
			    </script>");
		}
		try {
            $data['id_project'] = $this->uri->segment(3) ? $this->uri->segment(3) : '0';
            $active_prj = $this->Project_model_ci->getProjectName($data['id_project']);
            $data['project_name'] = $active_prj ? $active_prj->REVTX : "";  
        } catch (Exception $e) {
            error_log($e);
        }

        try {
            if ($this->uri->segment(4)) {
                $this->session->set_userdata('jc_id', $this->uri->segment(4));
            } else {
                $this->session->unset_userdata('jc_id');
            }

            if ($this->uri->segment(5)) {
                $this->session->set_userdata('mdr_type', $this->uri->segment(5));
            } else {
                $this->session->unset_userdata('mdr_type');
            }

            $data['jc_id'] = $this->session->userdata('jc_id');
        } catch (Exception $e) {
            error_log($e);
        }
        $this->Proc_model->merge_pmorder($this->session->userdata('REVNR'));
        $data['write'] = $this->is_write;
        $data["session"] = $this->session->userdata('logged_in');
        $data['content'] = 'admin/projects/crud_ticket2';
        //print('<pre>'.print_r($data,TRUE).'</pre>');die();
        $this->load->view('template', $data);
	}

	function create($revnr = null)
	{
		if($revnr == null){
			echo ("<script language='JavaScript'>
			    window.alert('Number REVNR is required');
			    window.location.href='".base_url('projects')."';
			    </script>");
		}
		try {
            $data['id_project'] = $this->uri->segment(3) ? $this->uri->segment(3) : '0';
            $active_prj = $this->Project_model_ci->getProjectName($data['id_project']);
            $data['project_name'] = $active_prj ? $active_prj->REVTX : "";  
        } catch (Exception $e) {
            error_log($e);
        }

        try {
            if ($this->uri->segment(4)) {
                $this->session->set_userdata('jc_id', $this->uri->segment(4));
            } else {
                $this->session->unset_userdata('jc_id');
            }

            if ($this->uri->segment(5)) {
                $this->session->set_userdata('mdr_type', $this->uri->segment(5));
            } else {
                $this->session->unset_userdata('mdr_type');
            }

            $data['jc_id'] = $this->session->userdata('jc_id');
        } catch (Exception $e) {
            error_log($e);
        }
        $this->Proc_model->merge_pmorder($this->session->userdata('REVNR'));
        $data['write'] = $this->is_write;
        $data["session"] = $this->session->userdata('logged_in');
        $data['content'] = 'admin/projects/create_ticket2';
        //print('<pre>'.print_r($data,TRUE).'</pre>');die();
        $this->load->view('template', $data);
	}

	function answer($revnr = null,$id_ticket)
	{
		if($revnr == null){
			echo ("<script language='JavaScript'>
			    window.alert('Number REVNR is required');
			    window.location.href='".base_url('projects')."';
			    </script>");
		}
		try {
            $data['id_project'] = $this->uri->segment(3) ? $this->uri->segment(3) : '0';
            $active_prj = $this->Project_model_ci->getProjectName($data['id_project']);
            $data['project_name'] = $active_prj ? $active_prj->REVTX : "";  
        } catch (Exception $e) {
            error_log($e);
        }

        try {
            if ($this->uri->segment(4)) {
                $this->session->set_userdata('jc_id', $this->uri->segment(4));
            } else {
                $this->session->unset_userdata('jc_id');
            }

            if ($this->uri->segment(5)) {
                $this->session->set_userdata('mdr_type', $this->uri->segment(5));
            } else {
                $this->session->unset_userdata('mdr_type');
            }

            $data['jc_id'] = $this->session->userdata('jc_id');
        } catch (Exception $e) {
            error_log($e);
        }
        $this->Proc_model->merge_pmorder($this->session->userdata('REVNR'));
        $data['write'] = $this->is_write;
        $data["session"] = $this->session->userdata('logged_in');
        $data['content'] = 'admin/projects/answer_ticket2';
        $data['ticket_detail'] = $this->Ticket_model->getTicketDetail($revnr,$id_ticket);
        $data['ticket_answer'] = $this->Ticket_model->getTicketAnswer($id_ticket);
        //print('<pre>'.print_r($data['ticket_answer'],TRUE).'</pre>');die();
        $this->load->view('template', $data);
	}

	public function api_table($revnr)
	{
		$data = $this->Ticket_model->getTicket($revnr);
		echo json_encode($data);
	}

	public function proses_create()
	{
		$id_project = $this->input->post('id_project');
		$subject = $this->input->post('subject');
		$message = $this->input->post('message');
		$file = "kosong";
		$date = date('Y-m-d H:i:s');
		$by = $this->data['session']['id_user'];
		$status = "open";


		$insert_ticket = $this->Ticket_model->insertTicket($id_project,$subject,$message,$file,$date,$by,$status);
		if($_FILES){
			$path = "assets/uploads/ticket/"; //set your folder path
		    //set the valid file extensions 
		    $valid_formats = array("jpg", "png", "gif", "bmp", "jpeg", "GIF", "JPG", "PNG"); //add the formats you want to upload
		    
		    $name = $_FILES['file']['name']; //get the name of the file
		    
		    $size = $_FILES['file']['size']; //get the size of the file

		    	$master = explode(".", $name);
		    	$ext = $master[count($master)-1];
		    	$txt = $master[0];

		        if (in_array($ext, $valid_formats)) { 
		            if ($size > 0) { // check if the file size is more than 2 mb
		                $file_name = str_replace(" ", "_", $txt)."&".$id_project."&".$insert_ticket; //get the file name
		                $tmp = $_FILES['file']['tmp_name'];
		                //$upload = move_uploaded_file($tmp, $path . $file_name.'.'.$ext);
		                //print('<pre>'.print_r($upload,TRUE).'</pre>');die();
		                if (move_uploaded_file($tmp, $path . $file_name.'.'.$ext)) { //check if it the file move successfully.
		                    $this->Ticket_model->updateTicket($id_project,$insert_ticket,$path.$file_name.'.'.$ext);
		                    echo TRUE;
		                } else {
		                    echo FALSE;
		                }
		            } else {
		                echo FALSE;
		            }
		        } else {
		            echo FALSE;
		        }
		    
		    exit;
		}else{
			echo TRUE;
		}
	}

	public function proses_answer()
	{
		$id_ticket = $this->input->post('id_ticket');
		$answer = $this->input->post('answer');
		$file = "kosong";
		$date = date('Y-m-d H:i:s');
		$by = $this->data['session']['id_user'];

		$insert_answer = $this->Ticket_model->insertTicketAnswer($id_ticket,$answer,$file,$date,$by);

		if($_FILES){
			$path = "assets/uploads/ticket/"; //set your folder path
		    //set the valid file extensions 
		    $valid_formats = array("jpg", "png", "gif", "bmp", "jpeg", "GIF", "JPG", "PNG"); //add the formats you want to upload
		    
		    $name = $_FILES['file']['name']; //get the name of the file
		    
		    $size = $_FILES['file']['size']; //get the size of the file

		        $master = explode(".", $name);
		    	$ext = $master[count($master)-1];
		    	$txt = $master[0];

		        if (in_array($ext, $valid_formats)) { //if the file is valid go on.
		            if ($size > 0) { // check if the file size is more than 2 mb
		                $file_name = str_replace(" ", "_", $txt)."&".$id_ticket."&".$insert_answer; //get the file name
		                $tmp = $_FILES['file']['tmp_name'];
		                if (move_uploaded_file($tmp, $path . $file_name.'.'.$ext)) { //check if it the file move successfully.
		                    $this->Ticket_model->updateTicketAnswer($insert_answer,$id_ticket,$path.$file_name.'.'.$ext);
		                    echo TRUE;
		                } else {
		                    echo FALSE;
		                }
		            } else {
		                echo FALSE;
		            }
		        } else {
		            echo FALSE;
		        }
		    
		    exit;
		}else{
			echo TRUE;
		}
	}

	public function update_status($revnr)
	{
		$id_ticket = $this->input->post('id_ticket');
		$status = $this->input->post('status');

		$update_status = $this->Ticket_model->updateTicketStatus($revnr,$id_ticket,$status);
		if($update_status){
			echo TRUE;
		}else{
			echo FALSE;
		}
	}
}
?>