<?php 
	function get_itval($ktext){
	        $start = strrpos($ktext, "[")+1;
	        $itval = substr($ktext, $start, strrpos($ktext, "]")-1);
	        return $itval;
	        
	}

	function get_cust_jc_num($ktext){
		$start = strrpos($ktext, "]")+1;
		return substr($ktext, $start);
	}

	function get_percentage($number, $divider){
		return $number != 0 ? round(($number / $divider) * 100, 2) : 0;
	}

	function get_projec_status($status) {
		switch ($status) {
		 	case 'CRTD':
		 		return 'NEW';
		 		break;

			case 'CLSD':
		 		return 'CLOSED';
		 		break;

		 	case 'REL':
		 		return 'RELEASE';
		 		break;	
		 } 
	}

	function raw_date_format($date){
		$raw_start_date = strtotime($date);
                return date('d M Y', $raw_start_date);
	}

	function raw_datetime_format($date){
		$raw_start_date = strtotime($date);
                return date('Y-M-d H:s', $raw_start_date);
	}

	function get_diff_days($start_date, $end_date) {
		$raw_start_date = new DateTime($start_date);
		$raw_end_date = new DateTime($end_date);
		$diff = $raw_end_date->diff($raw_start_date)->format("%a");

		return $diff + 1;
	}

	function get_current_date(){
		date_default_timezone_set('Asia/Jakarta');
		return date('d M Y');
	}

	function check_duplicate_report ($raw_report_date) {
		date_default_timezone_set('Asia/Jakarta');
		$current_date = date('d M Y');
		$report_date = date('d M Y', $raw_report_date);
		
	}

	function get_main_type($REVTX) {
		$word = "CHECK";
		$idx = strpos($REVTX, $word) + strlen($word);
		return strpos($REVTX, $word) !== false ? substr($REVTX, 0, $idx) : "";
	}

	function get_label_menu($menu, $path) {
		if($path == "projects/crud_ticket2"){
			$path = "ticket/view";
		}elseif($path == "projects/answer_ticket2"){
			$path = "ticket/view";
		}elseif($path == "projects/create_ticket2"){
			$path = "ticket/view";
		}
		foreach($menu as $key => $value) {
			if($value['l'] == $path) {
				return $key;
			} /*else if ($value['l'] == "projects/crud_daily_day") {
				return $key;
			}*/
		}

		return null;
	}
?>