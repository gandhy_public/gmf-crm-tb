<?php 

if (!defined('BASEPATH'))
        exit('No direct script access allowed');
class menu_lib 
{
	
	public function __construct()
	{
		$this->CI =& get_instance();
	}

	public function project_tab_menu_by_group($group_id){
		$menu = array();
		try{
			# $query = $this->CI->db->query("SELECT * FROM V_MENU_ACCESS WHERE USER_GROUP_ID = " . $group_id . " AND TYPE = 'T' ORDER BY MENU_ID");
                        $query = $this->CI->db->query("
                            select
                                    TB_MENU.*,
                                    TB_MENU_ACCESS.MENU_ACCESS_ID,
                                    TB_MENU_ACCESS.USER_GROUP_ID,
                                    TB_MENU_ACCESS.READ_ONLY,
                                    TB_MENU_PARENT.LABEL AS PARENT_LABEL,
                                    TB_MENU_PARENT.PATH AS PARENT_PATH
                            from
                                    [TB_MENU_ACCESS]
                            left join [TB_MENU] on
                                            [TB_MENU_ACCESS].[MENU_ID] = [TB_MENU].[MENU_ID]
                                    left join [TB_USER_GROUP] on
                                                    [TB_USER_GROUP].[ID_USER_GROUP] = [TB_MENU_ACCESS].[USER_GROUP_ID]
                                            left join [TB_MENU] as [TB_MENU_PARENT] on
                                                            [TB_MENU].[PARENT] = [TB_MENU_PARENT].[MENU_ID]
                                                    where
                                                            [TB_MENU_ACCESS].[USER_GROUP_ID] = $group_id
                                                            and [TB_MENU].[TYPE] = 'T'
                                                    order by
                                                            [MENU_ID] asc
                                ");
			foreach ($query->result() as $row) {
				array_push($menu, $row);
			}
			return $menu;			
		} catch (Exception $e) {
			return false;
			error_log($e);
		}
	}

	// public function project_side_bar_by_group($group_id){
	// 	$menu = array();
	// 	try{
	// 		$query = $this->CI->db->query("SELECT * FROM V_MENU_ACCESS WHERE USER_GROUP_ID = " . $group_id . " AND TYPE = 'S'");
	// 		foreach ($query->result() as $row) {
	// 			array_push($menu, $row);
	// 		}
	// 		return $menu;			
	// 	} catch (Exception $e) {
	// 		return false;
	// 		error_log($e);
	// 	}
	// }
}

?>