<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use Illuminate\Database\Eloquent\Model as Eloquent;

class Work_area_model extends Eloquent {

    protected $table = 'TB_WORK_AREA';
    public $timestamps = false;

}
