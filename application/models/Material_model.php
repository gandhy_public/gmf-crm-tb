<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use Illuminate\Database\Eloquent\Model as Eloquent;

class Material_model extends Eloquent {

    protected $table = 'TB_M_MATERIAL';
    public $timestamps = false;
    protected $guarded = array();

}
