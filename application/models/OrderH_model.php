<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use Illuminate\Database\Eloquent\Model as Eloquent;

class OrderH_model extends Eloquent {

    protected $table = 'M_PMORDERH';
    public $timestamps = false;
    protected $guarded = array();

    public function getKeyName(){
        return "AUFNR";
    }

}