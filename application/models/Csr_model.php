<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use Illuminate\Database\Eloquent\Model as Eloquent;

class Csr_model extends Eloquent {

    protected $table = 'TB_CSR';
    public $timestamps = false;

}
