<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use Illuminate\Database\Eloquent\Model as Eloquent;

class Menu_access_model extends Eloquent {

    protected $table = 'TB_MENU_ACCESS';
    public $timestamps = false;

}