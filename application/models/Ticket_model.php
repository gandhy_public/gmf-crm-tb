<?php
class Ticket_model extends CI_Model {

    protected $tb_ticket = 'TB_TICKET';
    protected $tb_ticket_answer = 'TB_TICKET_ANSWER';

    public function insertTicket($id_project,$subject,$message,$file,$date,$by,$status)
    {
    	$data = [
    		'REVNR' => $id_project,
    		'SUBJECT' => $subject,
    		'MESSAGE' => $message,
    		'FILE' => $file,
    		'CREATE_AT' => $date,
    		'CREATE_BY' => $by,
    		'STATUS' => $status
    	];
    	$this->db->insert($this->tb_ticket, $data);
		$insert_id = $this->db->insert_id();

		return  $insert_id;
    }

    public function insertTicketAnswer($id_ticket,$answer,$file,$date,$by)
    {
    	$data = [
    		'ID_TICKET' => $id_ticket,
    		'MESSAGE' => $answer,
    		'FILE' => $file,
    		'CREATE_AT' => $date,
    		'CREATE_BY' => $by
    	];
    	$this->db->insert($this->tb_ticket_answer, $data);
		$insert_id = $this->db->insert_id();

		return  $insert_id;
    }

    public function updateTicket($id_project,$id_ticket,$file)
    {
    	$this->db->set('FILE', $file);
		$this->db->where('ID', $id_ticket);
		$this->db->where('REVNR', $id_project);
		return $this->db->update($this->tb_ticket);
    }

    public function updateTicketAnswer($id_answer,$id_ticket,$file)
    {
    	$this->db->set('FILE', $file);
		$this->db->where('ID_TICKET', $id_ticket);
		$this->db->where('ID', $id_answer);
		return $this->db->update($this->tb_ticket_answer);
    }

    public function updateTicketStatus($id_project,$id_ticket,$status)
    {
    	$this->db->set('STATUS', $status);
		$this->db->where('ID', $id_ticket);
		$this->db->where('REVNR', $id_project);
		return $this->db->update($this->tb_ticket);
    }

    public function getTicket($id_project)
    {
    	$query = $this->db->select("REVNR,SUBJECT,MESSAGE,FILE,CREATE_BY,ID,CREATE_AT,$this->tb_ticket.STATUS,NAME")
    					->from($this->tb_ticket)
    					->join("TB_USER","$this->tb_ticket.CREATE_BY=TB_USER.ID_USER")
    					->where("REVNR",$id_project)
    					->order_by("STATUS","DESC")
    					->order_by("ID","DESC");
    	if(isset($_GET['search'])){
    		$this->db->like('SUBJECT', $_GET['search']);
    		$this->db->or_like('MESSAGE', $_GET['search']);
    		$this->db->or_like('CREATE_AT', $_GET['search']);
    		$this->db->or_like("$this->tb_ticket.STATUS", $_GET['search']);
    		$this->db->or_like('NAME', $_GET['search']);
    	}
    	return $query->get()->result();
    }

    public function getTicketDetail($id_project,$id_ticket)
    {
    	$query = $this->db->select("REVNR,SUBJECT,MESSAGE,FILE,CREATE_BY,ID,CREATE_AT,$this->tb_ticket.STATUS,NAME")
    					->from($this->tb_ticket)
    					->join("TB_USER","$this->tb_ticket.CREATE_BY=TB_USER.ID_USER")
    					->where("REVNR",$id_project)
    					->where("ID",$id_ticket);
    	return $query->get()->result();
    }

    public function getTicketAnswer($id_ticket)
    {
    	$query = $this->db->select("MESSAGE,FILE,CREATE_BY,ID,CREATE_AT,NAME")
    					->from($this->tb_ticket_answer)
    					->join("TB_USER","$this->tb_ticket_answer.CREATE_BY=TB_USER.ID_USER")
    					->where("ID_TICKET",$id_ticket);
    	return $query->get()->result();
    }

}