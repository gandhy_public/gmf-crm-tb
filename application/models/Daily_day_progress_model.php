<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use Illuminate\Database\Eloquent\Model as Eloquent;

class Daily_day_progress_model extends Eloquent {

    protected $table = 'TB_DAILY_DAY_PROGRESS';
    public $timestamps = false;
    protected $guarded = array();
    public $primaryKey  = 'ID';

}
