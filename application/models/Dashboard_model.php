<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use Illuminate\Database\Eloquent\Model as Eloquent;
use Illuminate\Database\Capsule\Manager as DB;

Class Dashboard_model extends Eloquent {


    // protected $table = 'TB_DAILY_DAY';
    // public $timestamps = false;
    // protected $guarded = array();


///JOBCARD
    function getJobcardByStatus($id_project) {
        // return DB::select("
        //     WITH A AS 
        //     (
        //         SELECT * FROM 
        //         ( VALUES ( 'OPEN' ), ( 'PROGRESS' ), ( 'CLOSED' ) ) AS STS ( NAME )
        //     ) SELECT 
        //     NAME AS LABELS,
        //     (SELECT COUNT(DISTINCT AUFNR) FROM TB_M_PMORDER WHERE REVNR = '$id_project' AND IS_ACTIVE = 1 AND AUART = 'GA01' AND STATUS LIKE NAME) AS QTY
        //     FROM A
        // ");

        return DB::select("
        WITH A AS (
            SELECT
                    M_PMORDERH.REVNR,
                    M_PMORDERH.AUFNR,
                    M_PMORDERH.AUART,
                    TB_M_PMORDER.STATUS
            FROM
                    dbo.M_PMORDERH 
                    LEFT JOIN dbo.TB_M_PMORDER ON TB_M_PMORDER.AUFNR = M_PMORDERH.AUFNR
            WHERE
                    (TB_M_PMORDER.IS_ACTIVE = 1 OR TB_M_PMORDER.IS_ACTIVE IS NULL)
                    AND M_PMORDERH.AUART = 'GA01'
                    AND M_PMORDERH.REVNR = $id_project),
						B AS (
							SELECT
                    REVNR,
                    COUNT(AUFNR) AS JC_TOTAL
							FROM
											A
							WHERE
											AUART = 'GA01'
							GROUP BY
											REVNR
						),				

            C AS (
            SELECT
                    REVNR,
                    COUNT(AUFNR) AS JC_CLOSED
            FROM
                    A
            WHERE
                    AUART = 'GA01'
                    AND STATUS = 'CLOSED'
            GROUP BY
                    REVNR ),
            D AS (
            SELECT
                    REVNR,
                    COUNT(AUFNR) AS JC_PROGRESS
            FROM
                    A
            WHERE
                    AUART = 'GA01'
                    AND (STATUS <> 'OPEN' AND STATUS <> 'CLOSED')
            GROUP BY
                    REVNR ),
            E AS (
            SELECT
                    REVNR,
                    COUNT(AUFNR) AS JC_OPEN
            FROM
                    A
            WHERE
                    AUART = 'GA01'
                                            AND STATUS = 'OPEN'
            GROUP BY
                    REVNR ) SELECT
                    B.REVNR,
                    COALESCE( C.JC_CLOSED,
                    0 ) AS JC_CLOSED,
                    COALESCE( D.JC_PROGRESS,
                    0 ) AS JC_PROGRESS,
                    COALESCE( E.JC_OPEN,
                    0 ) AS JC_OPEN,
										COALESCE( B.JC_TOTAL,
                    0 ) AS JC_TOTAL
            FROM
                    B
            LEFT JOIN D ON
                    B.REVNR = D.REVNR
            LEFT JOIN E ON
                    B.REVNR = E.REVNR
						LEFT JOIN C ON
                    B.REVNR = C.REVNR				
										       
        ");
    }

    function getMdrByStatus($id_project) {
        return DB::select("
        WITH A AS (
            SELECT
                    M_PMORDERH.REVNR,
                    M_PMORDERH.AUFNR,
                    M_PMORDERH.AUART,
                    TB_M_PMORDER.MDR_STATUS
            FROM
                    dbo.M_PMORDERH 
                    LEFT JOIN dbo.TB_M_PMORDER ON TB_M_PMORDER.AUFNR = M_PMORDERH.AUFNR
            WHERE
                    (TB_M_PMORDER.IS_ACTIVE = 1 OR TB_M_PMORDER.IS_ACTIVE IS NULL)
                    AND M_PMORDERH.AUART = 'GA02'
                    AND M_PMORDERH.REVNR = $id_project),
						B AS (
							SELECT
                    REVNR,
                    COUNT(AUFNR) AS MDR_TOTAL
							FROM
											A
							WHERE
											AUART = 'GA02'
							GROUP BY
											REVNR
						),				

            C AS (
            SELECT
                    REVNR,
                    COUNT(AUFNR) AS MDR_CLOSED
            FROM
                    A
            WHERE
                    AUART = 'GA02'
                    AND MDR_STATUS = 'CLOSED'
            GROUP BY
                    REVNR ),
            D AS (
            SELECT
                    REVNR,
                    COUNT(AUFNR) AS MDR_PROGRESS
            FROM
                    A
            WHERE
                    AUART = 'GA02'
                    AND MDR_STATUS = 'PROGRESS' 
            GROUP BY
                    REVNR ),
            E AS (
            SELECT
                    REVNR,
                    COUNT(AUFNR) AS MDR_OPEN
            FROM
                    A
            WHERE
                    AUART = 'GA02'
                    AND (MDR_STATUS = 'OPEN' OR MDR_STATUS = 'PENDING')
            GROUP BY
                    REVNR ) SELECT
                    B.REVNR,
                    COALESCE( C.MDR_CLOSED,
                    0 ) AS MDR_CLOSED,
                    COALESCE( D.MDR_PROGRESS,
                    0 ) AS MDR_PROGRESS,
                    COALESCE( E.MDR_OPEN,
                    0 ) AS MDR_OPEN,
										COALESCE( B.MDR_TOTAL,
                    0 ) AS MDR_TOTAL
            FROM
                    B
            LEFT JOIN D ON
                    B.REVNR = D.REVNR
            LEFT JOIN E ON
                    B.REVNR = E.REVNR
						LEFT JOIN C ON
                    B.REVNR = C.REVNR				
										
        ");
    }    

//     function getTotalJCClosed($id_project) {
//         $query = $this->db->select('*')
//                 ->from('TB_M_PMORDER')
//                 ->where( 'REVNR', $id_project)
//                 ->where( 'STATUS', "CLOSED")
//                 ->get();
//         return $query->num_rows($id_project);
//     }

//     function getTotalJCProgress($id_project) {
//         $status = array('OPEN', 'CLOSED');
//         $query = $this->db->select('*')
//                  ->from('TB_M_PMORDER')
//                  ->where( 'REVNR', $id_project)
//                 ->where_not_in('STATUS', $status)
//                 ->get();
//         return $query->num_rows();
//     }

//      function getTotalJC($id_project) {
//         $query = $this->db->select('*')
//                 ->from('TB_M_PMORDER')
//                 ->where( 'REVNR', $id_project)
//                 ->get();
//         return $query->num_rows();
//     }

// ///MDR
//     function getTotalMDROpen($id_project) {
//         $where = "REVNR = '$id_project' AND STATUS = 'OPEN' OR STATUS is null";
//         $query = $this->db->select('*')
//                 ->from('TB_M_PMORDER')
//                 ->where($where)
//                 ->get();
//         return $query->num_rows();
//     }

//     function getTotalMDRClosed($id_project) {
//         $query = $this->db->select('*')
//                 ->from('TB_M_PMORDER')
//                 ->where( 'REVNR', $id_project)
//                 ->where( 'STATUS', "CLOSED")
//                 ->get();
//         return $query->num_rows();
//     }

//     function getTotalMDRProgress($id_project) {
//         $status = array('OPEN', 'CLOSED');
//         $query = $this->db->select('*')
//                  ->from('TB_M_PMORDER')
//                  ->where( 'REVNR', $id_project)
//                 ->where_not_in('STATUS', $status)
//                 ->get();
//         return $query->num_rows();
//     }

//      function getTotalMDR($id_project) {
//         $query = $this->db->select('*')
//                 ->from('TB_M_PMORDER')
//                 ->where( 'REVNR', $id_project)
//                 ->get();
//         return $query->num_rows();
//     }

//     ///======



//       function getTotalMDROpen_excel() {
//         $query = $this->db->select('*')
//                 ->from('MDR')
//                 ->where('status', "Open")

//                 ->get();
//         return $query->num_rows();
//     }

//     function getTotalJChangar() {
//         $query = $this->db->select('*')
//                 ->from('ORDER_LIST')
//                 ->where('order_type', "GA01")
//                 ->where('PROGRESS_STATUS_ID', "2")
//                 ->get();
//         return $query->num_rows();
//     }

//     function getTotalMDRhangar() {
//         $query = $this->db->select('*')
//                 ->from('ORDER_LIST')
//                 ->where('order_type', "GA02")
//                 ->where('PROGRESS_STATUS_ID', "2")
//                 ->get();
//         return $query->num_rows();
//     }

//     function getTotalJCwsss() {
//         $query = $this->db->select('*')
//                 ->from('ORDER_LIST')
//                 ->where('order_type', "GA01")
//                 ->where('PROGRESS_STATUS_ID', "3")
//                 ->get();
//         return $query->num_rows();
//     }

//     function getTotalMDRwsss() {
//         $query = $this->db->select('*')
//                 ->from('ORDER_LIST')
//                 ->where('order_type', "GA02")
//                 ->where('PROGRESS_STATUS_ID', "3")
//                 ->get();
//         return $query->num_rows();
//     }

//     function getTotalJCwsse() {
//         $query = $this->db->select('*')
//                 ->from('ORDER_LIST')
//                 ->where('order_type', "GA01")
//                 ->where('PROGRESS_STATUS_ID', "4")
//                 ->get();
//         return $query->num_rows();
//     }

//     function getTotalMDRwsse() {
//         $query = $this->db->select('*')
//                 ->from('ORDER_LIST')
//                 ->where('order_type', "GA02")
//                 ->where('PROGRESS_STATUS_ID', "4")
//                 ->get();
//         return $query->num_rows();
//     }

//     function getTotalJCwscb() {
//         $query = $this->db->select('*')
//                 ->from('ORDER_LIST')
//                 ->where('order_type', "GA01")
//                 ->where('PROGRESS_STATUS_ID', "5")
//                 ->get();
//         return $query->num_rows();
//     }

//     function getTotalMDRwscb() {
//         $query = $this->db->select('*')
//                 ->from('ORDER_LIST')
//                 ->where('order_type', "GA02")
//                 ->where('PROGRESS_STATUS_ID', "5")
//                 ->get();
//         return $query->num_rows();
//     }

//     function getTotalJCwsls() {
//         $query = $this->db->select('*')
//                 ->from('ORDER_LIST')
//                 ->where('order_type', "GA01")
//                 ->where('PROGRESS_STATUS_ID', "6")
//                 ->get();
//         return $query->num_rows();
//     }

//     function getTotalMDRwsls() {
//         $query = $this->db->select('*')
//                 ->from('ORDER_LIST')
//                 ->where('order_type', "GA02")
//                 ->where('PROGRESS_STATUS_ID', "6")
//                 ->get();
//         return $query->num_rows();
//     }



//     function getTotalClose() {
//         $query = $this->db->select('*')
//                 ->from('ORDER_LIST')
//                 ->where('PROGRESS_STATUS_ID', "7")
//                 ->get();
//         return $query->num_rows();
//     }

//     function getTotalMDRclose() {
//         $query = $this->db->select('*')
//                 ->from('ORDER_LIST')
//                 ->where('order_type', "GA02")
//                 ->where('PROGRESS_STATUS_ID', "7")
//                 ->get();
//         return $query->num_rows();
//     }

//     function getTotalMDRclose_excel() {
//         $query = $this->db->select('*')
//                 ->from('mdr')
//                 ->where('status', "close")

//                 ->get();
//         return $query->num_rows();
//     }

//     function getTotalJCclose() {
//         $query = $this->db->select('*')
//                 ->from('ORDER_LIST')
//                 ->where('order_type', "GA01")
//                 ->where('PROGRESS_STATUS_ID', "7")
//                 ->get();
//         return $query->num_rows();
//     }

//     function getTotalOpen() {
//         $query = $this->db->select('*')
//                 ->from('ORDER_LIST')
//                 ->where('PROGRESS_STATUS_ID', "1")
//                 ->get();
//         return $query->num_rows();
//     }

//     function getTotalProgress() {
//         $status = array('2', '3', '4', '5', '6');
//         $query = $this->db->select('*')
//                 ->from('ORDER_LIST')
//                 ->where_in('PROGRESS_STATUS_ID', $status)
//                 ->get();
//         return $query->num_rows();
//     }

    

    
//     function getTotalMDR_excel_close_all($row){

//         $query = $this->db->select('*')
//                 ->from('mdr')
//                 ->where('id_project',$row)
//                 ->where('status', "close")
//                 ->get();
//         return $query->num_rows();
//     }

//     function getTotalJOBCARD_excel_all($row){

//         $query = $this->db->select('*')
//                 ->from('jobcard')
//                 ->where('id_project',$row)
//                 ->get();
//         return $query->num_rows();
//     }

//     function getTotalJOBCARD_excel_close_all($row){

//         $query = $this->db->select('*')
//                 ->from('jobcard')
//                 ->where('id_project',$row)
//                 ->where('status', "close")
//                 ->get();
//         return $query->num_rows();
//     }

//     function getTimelineHangar1(){
//         $query = $this->db->select('a.*, b.REVISION, b.PROGRESS_STATUS_ID')
//                 ->from('project_panning a')
//                 ->join('ORDER_LIST b on b.REVISION = a.REVISION')
//                 ->where('b.PROGRESS_STATUS_ID', "2")
//                 ->get();
//         return $query->results();
//     }

//     //for model ticket
//     function getListticket() {
//         $query = $this->db->query("SELECT * from ticket");

//         if ($query->num_rows() > 0) {
//             $no = 1;
//             $ret = "";
//             foreach ($query->result() as $row) {

//                 $color = "";
//                 $ret .= "
//                     <tr>
//                        <td style='background:$color'>$no</td>
//                        <td>$row->subject</td>
//                         <td>$row->department</td>
//                          <td>$row->id_project</td>
//                          <td>$row->service</td>
//                          <td>$row->priority</td>
//                              <td>$row->status</td>
//                        <td>$row->last_replay</td>

//                     </tr>
//                     ";
//                 $no++;
//             }
//             return $ret;
//         } else {
//             return "";
//         }
//     }

//     //for model project
//     function getListproject1() {
//         $query = $this->db->query("SELECT customer_name,aircraft_registered,project_name,start_date,finish_date,location,status_project,type_project from project");

//         if ($query->num_rows() > 0) {
//             $no = 1;
//             $ret = "";
//             foreach ($query->result() as $row) {

//                 $color = "";
//                 $ret .= "
//                     <tr>
//                        <td>$no</td>
//                        <td>$row->customer_name</td>
//                         <td>$row->aircraft_registered</td>
//                          <td>$row->project_name</td>

//                          <td>$row->start_date</td>
//                          <td>$row->finish_date</td>
//                          <td>$row->location</td>
//                           <td>$row->status_project</td>
//                            <td>$row->type_project</td>

//                     </tr>
//                     ";
//                 $no++;
//             }
//             return $ret;
//         } else {
//             return "";
//         }
//     }

//     function getlistProject() {

//         $query = $this->db->select('customer_name,aircraft_registered,project_name,description,start_date,finish_date,location,status_project')
//                 ->from('project')
//                 ->get();

//         return $query->result();
//     }

//     //for model production_planning
//  function getListDataProject() {
//         $query = $this->db->query("select REVISION,ORDER_TYPE,DESCRIPTION,PLANT from ORDER_LIST group by REVISION");

//         if ($query->num_rows() > 0) {
//             $no = 1;
//             $ret = "";
//             foreach ($query->result() as $row) {

//                 $color = "";
//                 $ret .= "
//             <tr>
//                         <td style='background:$color'>$no</td>
//             <td>
//                             <a class='my-modal' data-target='.mymodal' data-cache='false' data-toggle='modal' data-href= " . base_url() . "index.php/administrator/production_planning/modal_planning/" . $row->REVISION . " href='javascript:void(0);'>$row->REVISION</a>
//             </td>
//             <td>$row->ORDER_TYPE</td>
//                         <td>$row->DESCRIPTION</td>
//             <td>$row->PLANT</td>
//             <td><a class='btn btn-primary my-modal' data-target='.mymodal' data-cache='false' data-toggle='modal' data-href= " . base_url() . "index.php/administrator/production_planning/modal_planning/" . $row->REVISION . " href='javascript:void(0);'><i class='fa fa-pencil' aria-hidden='true'></i> Set Planning</a></td>
//                     </tr>
//         ";
//                 $no++;
//             }
//             return $ret;
//         } else {
//             return "";
//         }
//     }

//     function getListArea() {
//         $query = $this->db->select('AREA, PHASE')
//                 ->from("area")
//                 ->get();
//         return $query->result();
//     }

//     function getProdPlanning($revision) {
//         $query = $this->db->select('*')
//                 ->from("project_planning")
//                 ->where("REVISION", $revision)
//                 ->get();
//         return $query->result();
//     }

//     function cekPlanning($revision) {
//         $query = $this->db->select('STATUS_PLANNING')
//                 ->from("ORDER_LIST")
//                 ->where("REVISION", $revision)
//                 ->get();
//         return $query->row();
//     }

//     function getIDPlant($satuan = NULL) {
//         $query = $this->db->query("SELECT * from m_plant");
//         $ret = "<select id='id_plant' class='form-control' required>"
//                 . "<option>-- Pilih Plant --</option>";
//         if ($query->num_rows() > 0) {
//             foreach ($query->result() as $row) {
//                 if ($satuan == $row->ID_PLANT) {
//                     $slct = "selected";
//                 } else {
//                     $slct = "";
//                 }
//                 $ret .= "<option $slct value='$row->PLANT'>$row->PLANT</option>";
//             }
//         }
//         $ret .="</select>";
//         return $ret;
//     }

//     function getIDProject($satuan = NULL) {
//         $query = $this->db->query("SELECT * FROM ORDER_LIST GROUP BY REVISION");
//         $ret = "<select id='id_project' class='form-control' required>"
//                 . "<option>-- Pilih Project --</option>";
//         if ($query->num_rows() > 0) {
//             foreach ($query->result() as $row) {
//                 if ($satuan == $row->REVISION) {
//                     $slct = "selected";
//                 } else {
//                     $slct = "";
//                 }
//                 $ret .= "<option $slct value='$row->REVISION'>$row->REVISION</option>";
//             }
//         }
//         $ret .="</select>";

//         return $ret;
//     }


//     // model for jobcard perphase dan area
//     //  function getJobcardPhase(){
//     //     $query = $this->db->select('*')
//     //             ->from('v_jc_per_phase')
//     //             ->get();
//     //     $data['data'] = array();        
//     //     foreach ($query->result() as $row ) {
//     //         array_push($data['data'], $row);
//     //     }        
//     //     return $data['data'];      
//     // }

//     function getPhaseInspOpen($parameter){
//       $query = $this->db->select('open_')
//           ->from('v_jobcard_phase')
//           ->where('phase', $parameter)
//           ->get();
//       return $query->row();
//     }

//     function getPhaseInspClose($parameter){
//       $query = $this->db->select('close_')
//           ->from('v_jobcard_phase')
//           ->where('phase', $parameter)
//           ->get();
//       return $query->row();
//     }

//      function getPhaseInstOpen($parameter){
//       $query = $this->db->select('open_')
//           ->from('v_jobcard_phase')
//           ->where('phase', $parameter)
//           ->get();
//       return $query->row();
//     }

//     function getPhaseInstClose($parameter){
//       $query = $this->db->select('close_')
//           ->from('v_jobcard_phase')
//           ->where('phase', $parameter)
//           ->get();
//       return $query->row();
//     }

//        function getPhaseOpcOpen($parameter){
//       $query = $this->db->select('open_')
//           ->from('v_jobcard_phase')
//           ->where('phase', $parameter)
//           ->get();
//       return $query->row();
//     }

//      function getPhaseOpcClose($parameter){
//       $query = $this->db->select('close_')
//           ->from('v_jobcard_phase')
//           ->where('phase', $parameter)
//           ->get();
//       return $query->row();
//     }

//        function getPhaseLubOpen($parameter){
//       $query = $this->db->select('open_')
//           ->from('v_jobcard_phase')
//           ->where('phase', $parameter)
//           ->get();
//       return $query->row();
//     }

//     function getPhaseLubClose($parameter){
//       $query = $this->db->select('close_')
//           ->from('v_jobcard_phase')
//           ->where('phase', $parameter)
//           ->get();
//       return $query->row();
//     }

//     function getPhaseAFTOpen($parameter){
//       $query = $this->db->select('open_')
//           ->from('v_jobcard_area')
//           ->where('area', $parameter)
//           ->get();
//       return $query->row();
//     }

//     function getPhaseAFTClose($parameter){
//       $query = $this->db->select('close_')
//           ->from('v_jobcard_area')
//           ->where('area', $parameter)
//           ->get();
//       return $query->row();
//     }

//     function getPhaseCabinOpen($parameter){
//       $query = $this->db->select('open_')
//           ->from('v_jobcard_area')
//           ->where('area', $parameter)
//           ->get();
//       return $query->row();
//     }

//     function getPhaseCabinClose($parameter){
//       $query = $this->db->select('close_')
//           ->from('v_jobcard_area')
//           ->where('area', $parameter)
//           ->get();
//       return $query->row();
//     }

//      function getPhaseELECTOpen($parameter){
//       $query = $this->db->select('open_')
//           ->from('v_jobcard_area')
//           ->where('area', $parameter)
//           ->get();
//       return $query->row();
//     }

//      function getPhaseELECTClose($parameter){
//       $query = $this->db->select('close_')
//           ->from('v_jobcard_area')
//           ->where('area', $parameter)
//           ->get();
//       return $query->row();
//     }

//     function getPhaseENG1Open($parameter){
//       $query = $this->db->select('open_')
//           ->from('v_jobcard_area')
//           ->where('area', $parameter)
//           ->get();
//       return $query->row();
//     }

//     function getPhaseENG1Close($parameter){
//       $query = $this->db->select('close_')
//           ->from('v_jobcard_area')
//           ->where('area', $parameter)
//           ->get();
//       return $query->row();
//     }

//      function getPhaseFUSELAGEOpen($parameter){
//       $query = $this->db->select('open_')
//           ->from('v_jobcard_area')
//           ->where('area', $parameter)
//           ->get();
//       return $query->row();
//     }

//     function getPhaseFUSELAGEClose($parameter){
//       $query = $this->db->select('close_')
//           ->from('v_jobcard_area')
//           ->where('area', $parameter)
//           ->get();
//       return $query->row();
//     }

//     function getPhaseLHWINGOpen($parameter){
//       $query = $this->db->select('open_')
//           ->from('v_jobcard_area')
//           ->where('area', $parameter)
//           ->get();
//       return $query->row();
//     }

//     function getPhaseLHWINClose($parameter){
//       $query = $this->db->select('close_')
//           ->from('v_jobcard_area')
//           ->where('area', $parameter)
//           ->get();
//       return $query->row();
//     }

//     //mdr per area
//      function getMDRCabinOpen($parameter){
//       $query = $this->db->select('open_')
//           ->from('v_mdr_area')
//           ->where('area', $parameter)
//           ->get();
//       return $query->row();
//     }

//     function getMDRCabinClose($parameter){
//       $query = $this->db->select('close_')
//           ->from('v_mdr_area')
//           ->where('area', $parameter)
//           ->get();
//       return $query->row();
//     }

//      function getMDRENG1Open($parameter){
//       $query = $this->db->select('open_')
//           ->from('v_mdr_area')
//           ->where('area', $parameter)
//           ->get();
//       return $query->row();
//     }

//     function getMDRENG1Close($parameter){
//       $query = $this->db->select('close_')
//           ->from('v_mdr_area')
//           ->where('area', $parameter)
//           ->get();
//       return $query->row();
//     }

//      function getMDRENG3Open($parameter){
//       $query = $this->db->select('open_')
//           ->from('v_mdr_area')
//           ->where('area', $parameter)
//           ->get();
//       return $query->row();
//     }

//     function getMDRENG3Close($parameter){
//       $query = $this->db->select('close_')
//           ->from('v_mdr_area')
//           ->where('area', $parameter)
//           ->get();
//       return $query->row();
//     }

//     function getMDRENG4Open($parameter){
//       $query = $this->db->select('open_')
//           ->from('v_mdr_area')
//           ->where('area', $parameter)
//           ->get();
//       return $query->row();
//     }

//     function getMDRENG4Close($parameter){
//       $query = $this->db->select('close_')
//           ->from('v_mdr_area')
//           ->where('area', $parameter)
//           ->get();
//       return $query->row();
//     }

//     function getMDRFUSELAGEOpen($parameter){
//       $query = $this->db->select('open_')
//           ->from('v_mdr_area')
//           ->where('area', $parameter)
//           ->get();
//       return $query->row();
//     }

//     function getMDRFUSELAGEClose($parameter){
//       $query = $this->db->select('close_')
//           ->from('v_mdr_area')
//           ->where('area', $parameter)
//           ->get();
//       return $query->row();
//     }

//     function getMDRGENERALOpen($parameter){
//       $query = $this->db->select('open_')
//           ->from('v_mdr_area')
//           ->where('area', $parameter)
//           ->get();
//       return $query->row();
//     }

//     function getMDRGENERALClose($parameter){
//       $query = $this->db->select('close_')
//           ->from('v_mdr_area')
//           ->where('area', $parameter)
//           ->get();
//       return $query->row();
//     }

//     function getJC_AP_Open($parameter){
//       $query = $this->db->select('open_')
//           ->from('v_jobcard_skill')
//           ->where('skill', $parameter)
//           ->get();
//       return $query->row();
//     }

//       function getJC_AP_Close($parameter){
//       $query = $this->db->select('close_')
//           ->from('v_jobcard_skill')
//           ->where('skill', $parameter)
//           ->get();
//       return $query->row();
//     }

//       function getJC_CBN_Open($parameter){
//       $query = $this->db->select('open_')
//           ->from('v_jobcard_skill')
//           ->where('skill', $parameter)
//           ->get();
//       return $query->row();
//     }

//       function getJC_CBN_Close($parameter){
//       $query = $this->db->select('close_')
//           ->from('v_jobcard_skill')
//           ->where('skill', $parameter)
//           ->get();
//       return $query->row();
//     }

//     function getJC_EA_Open($parameter){
//       $query = $this->db->select('open_')
//           ->from('v_jobcard_skill')
//           ->where('skill', $parameter)
//           ->get();
//       return $query->row();
//     }

//       function getJC_EA_Close($parameter){
//       $query = $this->db->select('close_')
//           ->from('v_jobcard_skill')
//           ->where('skill', $parameter)
//           ->get();
//       return $query->row();
//     }

//     function getJC_TBP_Open($parameter){
//       $query = $this->db->select('open_')
//           ->from('v_jobcard_skill')
//           ->where('skill', $parameter)
//           ->get();
//       return $query->row();
//     }

//       function getJC_TBP_Close($parameter){
//       $query = $this->db->select('close_')
//           ->from('v_jobcard_skill')
//           ->where('skill', $parameter)
//           ->get();
//       return $query->row();
//     }

//     function create(){
//     $this->db->insert("customer",array("COMPANY_NAME"=>""));
//     return $this->db->insert_id();
//   }


//   function read(){
//     $this->db->order_by("ID_CUSTOMER","desc");
//     $query=$this->db->get("customer");
//     return $query->result_array();
//   }


//   function update($id,$value,$modul){
//     $this->db->where(array("ID_CUSTOMER"=>$id));
//     $this->db->update("customer",array($modul=>$value));
//   }

//   function delete($ID_CUSTOMER){
//     $this->db->where("ID_CUSTOMER",$ID_CUSTOMER);
//     $this->db->delete("customer");
//   }

//   function getJobcard_editgrid($id_project){
//         $sql = "select * from jobcard where id_project = '$id_project'";
//         return $this->db->query($sql)->result();
//  }

//   function getTotalJobcard($id_project) {
//           $sql = "select * from jobcard where id_project='$id_project'";
//           return $this->db->query($sql)->num_rows();
//   }

//   function getMDR_editgrid($id_project){
//         $sql = "select * from mdr where id_project = '$id_project'";
//         return $this->db->query($sql)->result();

//  }

//   function getTotal_MDR($id_project) {
//           $sql = "select * from mdr where id_project='$id_project'";
//           return $this->db->query($sql)->num_rows();
//   }

//   public function get_area_jobcard()
//         {
//             $this->db->order_by('area', 'asc');
//             return $this->db->get('jobcard')->result();
//         }

//   function getMRM_editgrid($id_project){
//         $sql = "select * from mrm where id_project = '$id_project'";
//         return $this->db->query($sql)->result();
//         // $query = $this->db->select('seq, order, description')
//         //         ->from("jobcard")
//         //         ->get();
//         // return $query->result();
//   }

//    function getTotal_MRM1($id_project) {
//         $query = $this->db->select('*')
//                 ->from('mrm')
//                 ->where('id_project',$id_project)

//                 ->get();
//         return $query->num_rows();
//     }

//    function getTotal_MRM($id_project) {
//           $sql = "select * from mrm where id_project='$id_project'";
//           return $this->db->query($sql)->num_rows();
//   }

}

?>
