<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use Illuminate\Database\Eloquent\Model as Eloquent;

class Mrm_model_view extends Eloquent {

    protected $table = 'V_MRM';
    public $timestamps = false;

}
