<?php
class Management_model extends CI_Model{

	private $tb_user = "TB_USER";
	private $tb_role = "TB_USER_GROUP";
	private $tb_group = "TB_GROUP_NAME";
	private $tb_customer = "CUSTOMER";
    private $tb_work = "TB_WORK_AREA";

	private $tb_menu = "TB_MN_MENU";
    private $tb_tab_menu = "TB_MN_MENU_TOP";
	private $tb_modul = "TB_MN_MODUL";
	private $tb_management = "TB_MN_MANAGE_MENU";
    private $tb_management_tab = "TB_MN_MANAGE_MENU_TOP";
    private $tb_management_tab_config = "TB_MN_MANAGE_MENU_TOP_CONFIG";

	var $column_order = array(null,"TB_USER.NAME","TB_USER.USERNAME","TB_USER_GROUP.USER_GROUP","TB_USER.STATUS");
    var $column_search = array("TB_USER.NAME","TB_USER.USERNAME","TB_USER_GROUP.USER_GROUP","TB_USER.STATUS");    
    var $order = array("TB_USER.ID_USER" => 'ASC');

    var $column_order_group = array(null,"name","is_active");
    var $column_search_group = array("name","is_active");    
    var $order_group = array("id" => 'ASC');

    var $column_order_role = array(null,"USER_GROUP", "LEVEL");
    var $column_search_role = array("USER_GROUP"); 
    var $order_role = array("ID_USER_GROUP" => 'ASC');

    public function query($sql)
    {
    	return $this->db->query($sql)->result_array();
    }

    public function getRole()
    {
        $sess_login = $this->session->userdata('logged_in');
        if($sess_login['user_group_level'] == 2){
            $query = $this->db->select('*')
                    ->from($this->tb_role)
                    ->where_in('LEVEL',[3,4])
                    ->get();
            return $query->result();        
            //return $this->db->get_where("$this->tb_role",array('LEVEL IN' => (3,4)))->result();
        }else{
            return $this->db->get("$this->tb_role")->result();
        }
    }

    public function getGroup()
    {
    	return $this->db->get("$this->tb_group")->result();
    }

    public function getCustomer()
    {
    	return $this->db->get("$this->tb_customer")->result();
    }

    public function getWork()
    {
        return $this->db->get("$this->tb_work")->result();
    }

    public function getTabProject()
    {
    	$data = $this->db->select("$this->tb_tab_menu.name as menu,$this->tb_tab_menu.id as menu_id,$this->tb_modul.name as modul,$this->tb_modul.id as modul_id")
    					->from($this->tb_tab_menu)
    					->join($this->tb_modul,"$this->tb_modul.id=$this->tb_tab_menu.modul_id")
    					->where("$this->tb_tab_menu.is_active","1")
                        ->where("$this->tb_tab_menu.modul_id","2")
    					->get();
    	return $data->result();
    }

    public function getMenu()
    {
        $data = $this->db->select("$this->tb_menu.name as menu,$this->tb_menu.id as menu_id,$this->tb_modul.name as modul,$this->tb_modul.id as modul_id")
                        ->from($this->tb_menu)
                        ->join($this->tb_modul,"$this->tb_modul.id=$this->tb_menu.modul_id")
                        ->where("$this->tb_menu.is_active","1")
                        ->get();
        return $data->result();
    }


    public function createUser($param)
    {
		return $this->db->insert($this->tb_user, $param);
    }

    public function getUserById($id_user) {
        // return $this->db->query("SELECT * FROM $this->tb_user WHERE ID_USER = $id_user")->result();
        // $this->db->where('ID_USER', $id_user);
        $query = $this->db->select("$this->tb_role.LEVEL,$this->tb_user.*")
                    ->from($this->tb_user)
                    ->join($this->tb_role,"$this->tb_user.GROUP_ID = $this->tb_role.ID_USER_GROUP")
                    ->where("$this->tb_user.ID_USER", $id_user)
                    ->get();
        return $query->result();
        // $this->db->join($this->tb_customer, "$this->tb_user.CUSTOMER_ID");
        // $data = $this->db->get($this->tb_user);
        // return $data->result();
    }

    public function cekUser($username,$password)
    {
        $query = $this->db->get_where($this->tb_user, array('USERNAME' => $username,'PASSWORD' => $password));
        return $query->result_array();
    }

    public function creatGroup($param)
    {
		$this->db->insert($this->tb_group, $param);

		$create_at = $param['create_at'];
		$select_id = $this->db->select('id')
							->from($this->tb_group)
							->where('create_at',"$create_at")
							->get()
							->result_array();
		$select_id = $select_id[0]['id'];
		return $select_id;
    }

    public function updateGroup($param,$id)
    {
        $this->db->set($param);
        $this->db->where('id', $id);
        return $this->db->update($this->tb_group);
    }

    public function creatManagement($param)
    {
    	for($a=0;$a<count($param);$a++){
    		$data = [
    			'group_id' => $param[$a]['group_id'],
    			'modul_id' => $param[$a]['modul_id'],
    			'menu_id' => $param[$a]['menu_id'],
    		];
    		$this->db->insert($this->tb_management, $data);
    	}
    	return true;
    }

    public function updateManagement($param,$id)
    {
        $this->db->where('group_id', $id);
        $this->db->delete($this->tb_management);

        for($a=0;$a<count($param);$a++){
            $data = [
                'group_id' => $param[$a]['group_id'],
                'modul_id' => $param[$a]['modul_id'],
                'menu_id' => $param[$a]['menu_id'],
            ];
            $this->db->insert($this->tb_management, $data);
        }
        return true;
    }

    public function creatManagementTab($param)
    {
        for($a=0;$a<count($param);$a++){
            $data = [
                'group_id' => $param[$a]['group_id'],
                'modul_id' => $param[$a]['modul_id'],
                'menu_id' => $param[$a]['menu_id'],
            ];
            $this->db->insert($this->tb_management_tab, $data);
        }
        return true;
    }

    public function creatManagementTabConfig($param)
    {
        $this->db->insert($this->tb_management_tab_config, $param);
    }

    public function deleteManagementTabConfig($user_id)
    {
        $this->db->delete($this->tb_management_tab_config, array('user_id' => $user_id));
    }


    public function updateManagementTab($param,$id)
    {
        $this->db->where('group_id', $id);
        $this->db->delete($this->tb_management_tab);

        for($a=0;$a<count($param);$a++){
            $data = [
                'group_id' => $param[$a]['group_id'],
                'modul_id' => $param[$a]['modul_id'],
                'menu_id' => $param[$a]['menu_id'],
            ];
            $this->db->insert($this->tb_management_tab, $data);
        }
        return true;
    }

    public function updateUser($data, $id)
    {
		$this->db->where('ID_USER', $id);
	    return $this->db->update($this->tb_user, $data);
    }

    public function checkLdap($username, $password)
    {
        $dn = "DC=gmf-aeroasia,DC=co,DC=id";
        $ldapconn = ldap_connect("192.168.240.57") or die ("Could not connect to LDAP server.");
        if ($ldapconn) {
            ldap_set_option(@$ldap, LDAP_OPT_PROTOCOL_VERSION, 3);
            ldap_set_option(@$ldap, LDAP_OPT_REFERRALS, 0);
            $ldapbind = ldap_bind($ldapconn, "ldap", "aeroasia");
            @$sr = ldap_search($ldapconn, $dn, "samaccountname=$username");
            @$srmail = ldap_search($ldapconn, $dn, "mail=$username@gmf-aeroasia.co.id");
            @$info = ldap_get_entries($ldapconn, @$sr);
            @$infomail = ldap_get_entries($ldapconn, @$srmail);
            @$usermail = substr(@$infomail[0]["mail"][0], 0, strpos(@$infomail[0]["mail"][0], '@'));
            @$bind = @ldap_bind($ldapconn, $info[0][dn], $password);
            /*print("<pre>".print_r(@$info,true)."</pre>");
            print("<pre>".print_r(@$infomail,true)."</pre>");
            print("<pre>".print_r(@$usermail,true)."</pre>");
            print("<pre>".print_r(@$bind,true)."</pre>");
            die();*/
            if ((@$info[0]["samaccountname"][0] == $username AND $bind) OR (@$usermail == $username AND $bind)) {
                return true;
            } else {
                return false;
            }
        } else {
            echo "LDAP Connection trouble,, please try again 2/3 time";
        }
    }

    public function cekUserLogin($username,$password)
    {
        try {
            $cekUserUsingLdap = $this->db->select("PASSWORD")
                                        ->from($this->tb_user)
                                        ->where("USERNAME",$username)
                                        ->get()->result_array();
            
            if(count($cekUserUsingLdap) > 0 ) {
                if($cekUserUsingLdap[0]['PASSWORD'] == 'using ldap'){
                    $cek_ldap = $this->checkLdap($username,$password);
                    if(!$cek_ldap) throw new Exception("Data User LDAP Tidak Ada!", 1);
                    $password = $cekUserUsingLdap[0]['PASSWORD'];
                }
            }else{
                throw new Exception("Data User Tidak Ada!", 1);
            }                            

            $select = [
                "$this->tb_user.ID_USER AS user_id",
                "$this->tb_user.USERNAME AS user_username",
                "$this->tb_user.NAME AS user_nama",
                "$this->tb_user.STATUS AS user_aktif",
                "$this->tb_role.ID_USER_GROUP AS user_role_id",
                "$this->tb_role.USER_GROUP AS user_role",
                "$this->tb_role.LEVEL AS user_role_level",
                "$this->tb_group.id AS user_group_id",
                "$this->tb_group.name AS user_group",
                "$this->tb_group.is_active AS user_group_aktif",
                "$this->tb_user.CUSTOMER_ID AS user_customer_id",
                "$this->tb_user.WORK_AREA_ID AS user_work_id"
            ];
            $cek_user = $this->db->select(implode(",", $select))
                                ->from($this->tb_user)
                                ->join($this->tb_role,"$this->tb_role.ID_USER_GROUP=$this->tb_user.GROUP_ID")
                                ->join($this->tb_group,"$this->tb_group.id=$this->tb_user.GROUP_NAME_ID")
                                ->where(array(
                                    'USERNAME'=>$username,
                                    'PASSWORD'=>$password,
                                    "$this->tb_user.STATUS"=>1,
                                    "$this->tb_group.is_active"=>1
                                ))
                                ->get()->result();
            if(!$cek_user)throw new Exception("Data user tidak ada!", 1);
            return [
                'codestatus' => 'S',
                'message' => 'Sukses',
                'resultdata' => [
                    $cek_user[0]
                ]
            ];
        } catch (Exception $e) {
            return [
                'codestatus' => 'E',
                'message' => $e->getMessage(),
                'resultdata' => []
            ];
        }
    }

    public function updateLastLogin($id)
    {
        $date = date('Y-m-d H:i:s');
        $this->db->set('LAST_LOGIN', "$date");
        $this->db->where('ID_USER', "$id");
        return $this->db->update("$this->tb_user");
    }

    public function getMenuSide($group_id)
    {
        $select = [
            "$this->tb_group.name AS GROUP_NAME",
            "$this->tb_modul.name AS MODUL_NAME",
            "$this->tb_modul.logo AS MODUL_LOGO",
            "$this->tb_menu.name AS MENU_NAME",
            "$this->tb_menu.logo AS MENU_LOGO",
            "$this->tb_menu.url AS MENU_URL",
        ];
        $menu = $this->db->select(implode(",", $select))
                        ->from($this->tb_management)
                        ->join($this->tb_group,"$this->tb_group.id=$this->tb_management.group_id")
                        ->join($this->tb_modul,"$this->tb_modul.id=$this->tb_management.modul_id")
                        ->join($this->tb_menu,"$this->tb_menu.id=$this->tb_management.menu_id")
                        ->where("$this->tb_management.group_id",$group_id)
                        ->get()->result();
        $no=0;
        foreach ($menu as $key) {
            
            $menu_arr[$key->MODUL_NAME."=".$key->MODUL_LOGO][] = [
                'm' => $key->MENU_NAME."=".$key->MENU_URL,
                'l' => $key->MENU_LOGO,
            ];

        }
        return $menu_arr;
    }

    public function getMenuTop($group_id)
    {
        $select = [
            "$this->tb_group.name AS GROUP_NAME",
            "$this->tb_modul.name AS MODUL_NAME",
            "$this->tb_modul.logo AS MODUL_LOGO",
            "$this->tb_modul.id AS MODUL_ID",
            "$this->tb_tab_menu.name AS MENU_NAME",
            "$this->tb_tab_menu.logo AS MENU_LOGO",
            "$this->tb_tab_menu.url AS MENU_URL",
            "$this->tb_tab_menu.id AS MENU_ID",
        ];
        $menu = $this->db->select(implode(",", $select))
                        ->from($this->tb_management_tab)
                        ->join($this->tb_group,"$this->tb_group.id=$this->tb_management_tab.group_id")
                        ->join($this->tb_modul,"$this->tb_modul.id=$this->tb_management_tab.modul_id")
                        ->join($this->tb_tab_menu,"$this->tb_tab_menu.id=$this->tb_management_tab.menu_id")
                        ->where("$this->tb_management_tab.group_id",$group_id)
                        ->get()->result();
        $no=0;
        foreach ($menu as $key) {
            
            $menu_arr[] = [
                'm' => $key->MENU_NAME,
                'l' => $key->MENU_URL,
                'c' => $key->MODUL_ID."_".$key->MENU_ID,
                'w' => 0
            ];

        }
        return $menu_arr;
    }

    public function getMenuTopConfig($group_id,$user_id)
    {
        $select = [
            "$this->tb_management_tab_config.modul_id AS MODUL_ID",
            "$this->tb_management_tab_config.menu_id AS MENU_ID",
        ];
        $menu = $this->db->select(implode(",", $select))
                        ->from($this->tb_management_tab_config)
                        ->where("$this->tb_management_tab_config.group_id",$group_id)
                        ->where("$this->tb_management_tab_config.user_id",$user_id)
                        ->get()->result();
        $no=0;
        $menu_arr = [];
        foreach ($menu as $key) {
            
            $menu_arr[] = $key->MODUL_ID."_".$key->MENU_ID;

        }
        return $menu_arr;
    }


// ========================================================

	private function _get_datatables_query()
    {
    	$this->db->select("$this->tb_role.USER_GROUP,$this->tb_user.*");
		$this->db->from($this->tb_user);
		$this->db->join($this->tb_role, "$this->tb_role.ID_USER_GROUP = $this->tb_user.GROUP_ID");
 
        $i = 0;
     	foreach ($this->column_search as $item){
            if($_REQUEST['search']['value']) // if datatable send POST for search
            {
                 
                if($i===0) // first loop
                {
                    //$this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_REQUEST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_REQUEST['search']['value']);
                }

                //if(count($this->column_search) - 1 == $i) //last loop
                    //$this->db->group_end(); //close bracket
            }
            $i++;
        }
        
        if(isset($_REQUEST['order'])) // here order processing
        {
        	if($_REQUEST['order']['0']['column'] == 0){
        		$order = $this->order;
	            $this->db->order_by(key($order), $order[key($order)]);
        	}else{
        		$this->db->order_by($this->column_order[$_REQUEST['order']['0']['column']], strtoupper($_REQUEST['order']['0']['dir']));
        	}
        } 
        else if(isset($this->order))
        {
        	$order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    private function _get_datatables_group_query()
    {
		$this->db->from($this->tb_group);
 
        $i = 0;
     	foreach ($this->column_search_group as $item){
            if($_REQUEST['search']['value']) // if datatable send POST for search
            {
                 
                if($i===0) // first loop
                {
                    //$this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_REQUEST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_REQUEST['search']['value']);
                }
 
                //if(count($this->column_search) - 1 == $i) //last loop
                    //$this->db->group_end(); //close bracket
            }
            $i++;
        }
        
        if(isset($_REQUEST['order'])) // here order processing
        {
        	if($_REQUEST['order']['0']['column'] == 0){
        		$order = $this->order_group;
	            $this->db->order_by(key($order), $order[key($order)]);
        	}else{
        		$this->db->order_by($this->column_order_group[$_REQUEST['order']['0']['column']], strtoupper($_REQUEST['order']['0']['dir']));
        	}
        } 
        else if(isset($this->order_group))
        {
        	$order = $this->order_group;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function _get_datatables_role_query()
    {
        $this->db->from($this->tb_role);
 
        $i = 0;
     	foreach ($this->column_search_role as $item){
            if($_REQUEST['search']['value']) // if datatable send POST for search
            {
                 
                if($i===0) // first loop
                {
                    //$this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_REQUEST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_REQUEST['search']['value']);
                }
 
                //if(count($this->column_search) - 1 == $i) //last loop
                    //$this->db->group_end(); //close bracket
            }
            $i++;
        }
        
        if(isset($_REQUEST['order'])) // here order processing
        {
        	if($_REQUEST['order']['0']['column'] == 0){
        		$order = $this->order_role;
	            $this->db->order_by(key($order), $order[key($order)]);
        	}else{
        		$this->db->order_by($this->column_order_role[$_REQUEST['order']['0']['column']], strtoupper($_REQUEST['order']['0']['dir']));
        	}
        } 
        else if(isset($this->order_role))
        {
        	$order = $this->order_role;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

	function get_datatables()
    {
        $this->_get_datatables_query();
        $sess_login = $this->session->userdata('logged_in');

        if($_REQUEST['length'] != -1)
        $query = $this->db->get();
        return $this->db->last_query();
    } 

    function get_datatables_group()
    {
        $this->_get_datatables_group_query();

        if($_REQUEST['length'] != -1)
        $query = $this->db->get();
        return $this->db->last_query();
    } 

    function get_datatables_role()
    {
        $this->_get_datatables_role_query();

        if($_REQUEST['length'] != -1)
        $query = $this->db->get();
        return $this->db->last_query();
    } 

    function get_datatables_query($sql)
    {
        $query = $this->db->query($sql);
        return $query->result();
    }    
 
    function count_filtered()
    {
        $this->_get_datatables_query();
        
        $query = $this->db->get();
        return $query->num_rows();
    }

    function count_filtered_group()
    {
        $this->_get_datatables_group_query();
        
        $query = $this->db->get();
        return $query->num_rows();
    }

    function count_filtered_role()
    {
        $this->_get_datatables_role_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function count_all()
    {
    	$this->db->from($this->tb_user);
    	
        return $this->db->count_all_results();
    }

    public function count_all_group()
    {
    	$this->db->from($this->tb_group);
        return $this->db->count_all_results();
    }

    public function count_all_role()
    {
        $this->db->from($this->tb_role);
        return $this->db->count_all_results();
    }

    public function getRoleById($id_user_group) {
        return $this->db->from($this->tb_role)->where('ID_USER_GROUP', $id_user_group)->get()->result();
    }

    public function updateRole($data, $id)
    {
		$this->db->where('ID_USER_GROUP', $id);
	    $this->db->update($this->tb_role, $data);
    }

    public function deleteRole($id)
    {
		$this->db->where('ID_USER_GROUP', $id);
	    $this->db->delete($this->tb_role);
    }

    public function checkRoleAlreadyUsed($id)
    {
		$this->db->where('GROUP_ID', $id);
        $data = $this->db->get($this->tb_user);
        
        if($data->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function getGroupById($id) {
        $sql = "SELECT
                    GROUP_ID,
                    MODUL_ID,
                    MENU_ID,
                    $this->tb_group.NAME AS GROUP_NAME,
                    $this->tb_modul.NAME AS MODUL_NAME,
                    $this->tb_group.is_active AS STATUS
                FROM
                    $this->tb_management
                    JOIN $this->tb_modul ON $this->tb_management.MODUL_ID = $this->tb_modul.ID 
                    JOIN $this->tb_group ON $this->tb_management.GROUP_ID = $this->tb_group.ID
                WHERE
                    GROUP_ID = $id";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function getGroupTopById($id) {
        $sql = "SELECT
                    GROUP_ID,
                    MODUL_ID,
                    MENU_ID,
                    $this->tb_group.NAME AS GROUP_NAME,
                    $this->tb_modul.NAME AS MODUL_NAME
                FROM
                    $this->tb_management_tab
                    JOIN $this->tb_modul ON $this->tb_management_tab.MODUL_ID = $this->tb_modul.ID 
                    JOIN $this->tb_group ON $this->tb_management_tab.GROUP_ID = $this->tb_group.ID
                WHERE
                    GROUP_ID = $id";
        $query = $this->db->query($sql);
        return $query->result();
    }

}
