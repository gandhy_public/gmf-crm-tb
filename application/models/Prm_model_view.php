<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use Illuminate\Database\Eloquent\Model as Eloquent;

class Prm_model_view extends Eloquent {

    protected $table = 'V_PRM';
    public $timestamps = false;

}
