
<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use Illuminate\Database\Eloquent\Model as Eloquent;

class Jobcard_model_view extends Eloquent {

	protected $table = 'TB_V_JOBCARD_PROGRESS';
	public $timestamps = false;

    	public function get_jobcard_close_percentage($project_id){		
		$jobcard_total = $this->where('REVNR', $project_id)
		                      ->count();

		$jobcard_closed_total = $this->where('REVNR', $project_id)
		                             ->where('STATUS', 'CLOSED')
		                             ->count();
		
		$closed_persentase = ( $jobcard_closed_total /  $jobcard_total) * 100;

		$data = array(
			'TOTAL' => $jobcard_total,
			'CLOSED' => $jobcard_closed_total,
			'PERSENTASE' => number_format($closed_persentase, 2)
		);

		return $data;
	}

	public function get_mdr_close_percentage($project_id){		
		$jobcard_total = $this->where('REVNR', $project_id)
		                      ->count();

		$jobcard_closed_total = $this->where('REVNR', $project_id)
		                             ->where('STATUS', 'CLOSED')
		                             ->count();
		
		$closed_persentase = ( $jobcard_closed_total /  $jobcard_total) * 100;

		$data = array(
			'TOTAL' => $jobcard_total,
			'CLOSED' => $jobcard_closed_total,
			'PERSENTASE' => number_format($closed_persentase, 2)
		);

		return $data;
	}

	public function get_today_target_achievement($project_id){		
		$jobcard_total = $this->where('REVNR', $project_id)
		                      ->count();

		$jobcard_closed_total = $this->where('REVNR', $project_id)
		                             ->where('STATUS', 'CLOSED')
		                             ->count();
		
		$closed_persentase = ( $jobcard_closed_total /  $jobcard_total) * 100;

		$data = array(
			'TOTAL' => $jobcard_total,
			'CLOSED' => $jobcard_closed_total,
			'PERSENTASE' => number_format($closed_persentase, 2)
		);

		return $data;
	}		


}
