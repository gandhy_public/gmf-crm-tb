<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use Illuminate\Database\Eloquent\Model as Eloquent;

class Mdr_model_view extends Eloquent {

    protected $table = 'TB_V_MDR_PROGRESS';
    public $timestamps = false;

    function get_mdr_count_by_status($REVNR){
    	$open = $this->selectRaw("'OPEN' AS STATUS, count(*) as TOTAL")
    			->where('REVNR', $REVNR)
    			->where('STATUS', 'LIKE', 'OPEN');

    	$close = $this->selectRaw("'CLOSED' AS STATUS, count(*) as TOTAL")
			->where('REVNR', $REVNR)
			->where('STATUS', 'LIKE', 'CLOSED');

    	$progress = $this->selectRaw("'PROGRESS' AS STATUS, count(*) as TOTAL")
			->where('REVNR', $REVNR)
			->where(function($query){
					$query->whereNotIn('STATUS', ['OPEN', 'CLOSED'])
						->orWhereNull('STATUS');
				});
			

	$status_count = $this->selectRaw("'TOTAL' AS STATUS, count(*) as TOTAL")
			->where('REVNR', $REVNR)
			->union($open)
			->union($close)
			->union($progress)
			->get();			 			    			
    	return $status_count;
    }

}
