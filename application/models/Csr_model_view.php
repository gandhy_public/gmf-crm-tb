<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use Illuminate\Database\Eloquent\Model as Eloquent;

class Csr_model_view extends Eloquent {

    protected $table = 'V_CSR';
    public $timestamps = false;

}
