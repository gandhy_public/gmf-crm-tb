<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use Illuminate\Database\Eloquent\Model as Eloquent;

class Daily_menu_model_view extends Eloquent {

    protected $table = 'V_DAILY_MENU';
    public $timestamps = false;

}
