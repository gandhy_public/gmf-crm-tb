<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use Illuminate\Database\Eloquent\Model as Eloquent;

class RFCLog_model extends Eloquent {

    protected $table = 'TB_RFC_LOG';
    public $timestamps = false;
//    protected $guarded = array();

    protected $fillable = array('JOBNAME', 'JOBCOUNT', 'VARIANT', 'RFCSTATUS', 'EXEC_BY', 'REVISION');

//    public $primaryKey  = 'REVNR';
}
