<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use Illuminate\Database\Eloquent\Model as Eloquent;

class Menu_access_view_model extends Eloquent {

    protected $table = 'TB_V_MENU_ACCESS';
    public $timestamps = false;

    public function getAccessedMenu($USER_GROUP_ID, $TYPE){
    	return $this->where('USER_GROUP_ID', $USER_GROUP_ID)
                     ->where('TYPE', $TYPE)
                     ->orderBy('MENU_ID')->get();
    }

    public function getMenuByGroup($USER_GROUP_ID, $TYPE){
    	$menu = $this
	    	->where('USER_GROUP_ID', $USER_GROUP_ID)
	    	->where('TYPE', $TYPE)
            ->orderBy('MENU_ID');
		 return $menu->get();
    }
}
