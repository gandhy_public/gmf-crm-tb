<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use Illuminate\Database\Eloquent\Model as Eloquent;

class Work_area_view_model extends Eloquent {

    protected $table = 'TB_V_WORK_AREA';
    public $timestamps = false;

}
