<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use Illuminate\Database\Eloquent\Model as Eloquent;

class Daily_details_model extends Eloquent {

    protected $table = 'TB_DAILY_DETAILS';
    public $timestamps = false;

}
