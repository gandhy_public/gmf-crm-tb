<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use Illuminate\Database\Eloquent\Model as Eloquent;

class Maint_phase_model extends Eloquent {

    protected $table = 'TB_MAINT_PHASE';
    public $timestamps = false;
    protected $guarded = array();
    public $primaryKey  = 'ID';

}
