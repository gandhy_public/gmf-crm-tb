<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use Illuminate\Database\Eloquent\Model as Eloquent;

class Phase_model_view extends Eloquent {

    protected $table = 'TB_V_PHASE_PROGRESS';
    public $timestamps = false;
}
