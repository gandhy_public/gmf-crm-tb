<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use Illuminate\Database\Eloquent\Model as Eloquent;

class Order_model extends Eloquent {

    protected $table = 'M_PMORDER';
    public $timestamps = false;
    protected $guarded = array();

    public function getKeyName(){
        return "AUFNR";
    }

}