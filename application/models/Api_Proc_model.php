<?php

class Api_Proc_model extends CI_Model {
    private $column_order = [];
    private $column_search = [];
    private $order = [];
    function __construct() {
        parent::__construct();  // call model constructor    
    }

	function get_mdr($REVNR, $ORDER = 'SEQ_NUM', $AUFNR = 0, $JC_REF = '', $FILTER = '',$total=FALSE) {
        // if(isset($_REQUEST['order'])) {
        //     echo "<script>console.log("+$_REQUEST['order']+")</scrip>";
        // }
        $arrOrder=[$ORDER];
        $this->order=['mdr'=>$arrOrder];
        $text="ERDAT,ERNAM,JC_REF,JC_REF_LINK,CUST_JC_NUM,REVNR,
                    AREA,
                    KTEXT_LINK,
                    KTEXT,
                    SKILL,
                    DATE_PE,
                    STATUS,
                    MATSTATUS,
                    REMARK,
                    DATECLOSE,
                    [FREETEXT],
                    DATEPROGRESS,
                    DAY,
                    CABINSTATUS,
                    STEP1,
                    DATE1,
                    STEP2,
                    DATE2,
                    STEP3,
                    DATE3,
                    DOC_SENT_STATUS,
                    MATERIAL_FULFILLMENT_STATUS,
                    FULLFILLMENT_STATUS_DATE,
                    AUFNR,
                    AUFNR_LINK,
                    MDR_STATUS,
                    IS_ACTIVE";
        $arrcolumnO=explode(",",str_replace("\r\n",'', str_replace(" ",'',"null,".$text)));
        $arrcolumnS=explode(",",str_replace("\r\n",'', str_replace(" ",'',$text)));
        $this->column_order=array_filter($this->column_order);
        $this->column_order=['mdr'=>$arrcolumnO];
        $this->column_search=['mdr'=>$arrcolumnS];
        // $param = $this->_get_datatables('mdr');
        // echo json_encode($column_order) ;die;   
        // print_r($column_order);die;
        //   WITH TEMP AS (
        $sql= "
          
            WITH B AS (
            SELECT
                    DISTINCT
                    CASE
                            WHEN LEN(TB.ERDAT) = 8 THEN SUBSTRING( TB.ERDAT, 7, 2 ) + '-' + SUBSTRING( TB.ERDAT, 5, 2 ) + '-' + SUBSTRING( TB.ERDAT, 1, 4 )
                            ELSE NULL
                    END AS ERDAT,
                    TB.EXT5,
                    TB.ERNAM,
                    cast( TB.JC_REFF as bigint ) AS JC_REF,
                    CASE 
                    WHEN CHARINDEX( ':', TB2.KTEXT ) > 0  
                            THEN 
                    SUBSTRING (
                            TB2.KTEXT, CHARINDEX( ']', TB2.KTEXT ) + 1,
                            ABS(CHARINDEX( ':', TB2.KTEXT ) - CHARINDEX( ']', TB2.KTEXT ) - 1 ))
                            ELSE '' END AS 'CUST_JC_NUM',
                    TB.REVNR,
                    TB.AREA,
                    TB.KTEXT,
                    TB.SKILL,
                    TB.DATE_PE,
                    TB.STATUS,
                    TB.MATSTATUS,
                    TB.REMARK,
                    TB.DATECLOSE,
                    TB.[FREETEXT],
                    TB.DATEPROGRESS,
                    TB.DAY,
                    TB.CABINSTATUS,
                    TB.STEP1,
                    TB.DATE1,
                    TB.STEP2,
                    TB.DATE2,
                    TB.STEP3,
                    TB.DATE3,
                    TB.DOC_SENT_STATUS,
                    TB.MAT_FULLFILLMENT_STATUS AS MATERIAL_FULFILLMENT_STATUS,
                    TB.FULLFILLMENT_STATUS_DATE,
                    TB.AUFNR,
                    TB.MDR_STATUS,
                    TB.IS_ACTIVE
            FROM
                    TB_M_PMORDER TB
            LEFT JOIN TB_M_PMORDER TB2 ON
                    CAST(TB2.AUFNR AS BIGINT) = CAST(TB.JC_REFF AS BIGINT)
                    AND TB2.REVNR = TB.REVNR
                    AND TB2.AUART = 'GA01'
            LEFT JOIN TB_MRM MRM ON
                    MRM.AUFNR = TB.AUFNR
            WHERE
                    TB.REVNR = '$REVNR'
                    AND ( $AUFNR = 0
                    OR $AUFNR = TB.AUFNR )
                    AND ( '$JC_REF' = ''
                    OR TB.JC_REFF LIKE '%' + '$JC_REF' + '%' )
                    AND TB.AUART = 'GA02'
                    AND TB.IS_ACTIVE = 1
                    AND LEN(TB.RSPOS) <= 1
                    AND LEN(TB.APLZL_V) <= 1
                    AND LEN(TB.AUFPL) <= 1
                    AND ( ( '$FILTER' = ''
                    OR TB.AUFNR LIKE '%' + '$FILTER' + '%' )
                    OR ( '$FILTER' = ''
                    OR TB.JC_REFF LIKE '%' + '$FILTER' + '%' )
                    OR ( '$FILTER' = ''
                    OR CUST_JC_NUM LIKE '%' + '$FILTER' + '%' )
                    OR ( '$FILTER' = ''
                    OR TB.KTEXT LIKE '%' + '$FILTER' + '%' )
                    OR ( '$FILTER' = ''
                    OR TB.AREA LIKE '%' + '$FILTER' + '%' )
                    OR ( '$FILTER' = ''
                    OR TB.ERDAT LIKE '%' + '$FILTER' + '%' )
                    OR ( '$FILTER' = ''
                    OR TB.SKILL LIKE '%' + '$FILTER' + '%' )
                    OR ( '$FILTER' = ''
                    OR TB.ERNAM LIKE '%' + '$FILTER' + '%' )
                    OR ( '$FILTER' = ''
                    OR TB.DATE_PE LIKE '%' + '$FILTER' + '%' )
                    OR ( '$FILTER' = ''
                    OR TB.STATUS LIKE '%' + '$FILTER' + '%' )
                    OR ( '$FILTER' = ''
                    OR TB.MATSTATUS LIKE '%' + '$FILTER' + '%' )
                    OR ( '$FILTER' = ''
                    OR TB.STEP1 LIKE '%' + '$FILTER' + '%' )
                    OR ( '$FILTER' = ''
                    OR TB.DATE1 LIKE '%' + '$FILTER' + '%' )
                    OR ( '$FILTER' = ''
                    OR TB.STEP2 LIKE '%' + '$FILTER' + '%' )
                    OR ( '$FILTER' = ''
                    OR TB.DATE2 LIKE '%' + '$FILTER' + '%' )
                    OR ( '$FILTER' = ''
                    OR TB.STEP3 LIKE '%' + '$FILTER' + '%' )
                    OR ( '$FILTER' = ''
                    OR TB.DATE3 LIKE '%' + '$FILTER' + '%' )
                    OR ( '$FILTER' = ''
                    OR TB.MDR_STATUS LIKE '%' + '$FILTER' + '%' )
                    OR ( '$FILTER' = ''
                    OR TB.DATECLOSE LIKE '%' + '$FILTER' + '%' )
                    OR ( '$FILTER' = ''
                    OR TB.FULLFILLMENT_STATUS_DATE LIKE '%' + '$FILTER' + '%' )
                    OR ( '$FILTER' = ''
                    OR TB.MAT_FULLFILLMENT_STATUS LIKE '%' + '$FILTER' + '%' )
                    OR ( '$FILTER' = ''
                    OR TB.REMARK LIKE '%' + '$FILTER' + '%' )
                    OR ( '$FILTER' = ''
                    OR TB.DOC_SENT_STATUS LIKE '%' + '$FILTER' + '%' )
                    OR ( '$FILTER' = ''
                    OR TB.[FREETEXT] LIKE '%' + '$FILTER' + '%' ) ) 
                    ) SELECT
                    CAST(ROUND(EXT5, 0) AS NUMERIC(12,0)) AS SEQ_NUM,
                     AUFNR AS AUFNR_LINK,
                       CONCAT('<a href=','". base_url() ."','projects/crud_jobcard/',REVNR,'/',CAST(JC_REF AS VARCHAR),'/jc>',CAST(JC_REF AS VARCHAR),'</a>') AS JC_REF_LINK,
                    CUST_JC_NUM,
                    CONCAT('<a href=\"javascript:void(0);\" onclick=\"longtextswal(',AUFNR,')\">',KTEXT,'</a>') AS KTEXT_LINK,
                     AREA,
                    ERDAT,
                     SKILL,
                    ERNAM,
                    DATE_PE,
                    STATUS,
                    '<a href=\"" . base_url() . "projects/crud_mrm/' + REVNR + '/' + AUFNR + '/MDR\">' + MATSTATUS + '</a>'
                    AS MATSTATUS,
                    STEP1,
                    DATE1,
                    STEP2,
                    DATE2,
                    STEP3,
                    DATE3,
                    MDR_STATUS,
                    DATECLOSE,
                    MATERIAL_FULFILLMENT_STATUS,
                    FULLFILLMENT_STATUS_DATE,
                    REMARK,
                    DOC_SENT_STATUS,
                    [FREETEXT],
                    DATEPROGRESS,
                     DAY,
                    CABINSTATUS,
                    '<button class=\"btn btn-sm btn-danger delete\" data-id=\"' + AUFNR + '\" onclick=\"jc_delete(' + AUFNR + ')\"><i class=\"fa fa-trash-o\" aria-hidden=\"true\"></i> </button>' AS AUFNR_DEL
            FROM
                    B
            ORDER BY $ORDER";
        //              ),
        // X AS ( 
        //   SELECT 
        //   ROW_NUMBER ( ) OVER ( ORDER BY ".$param['order']." ) AS ROWID,
        //   * 
        //   FROM TEMP ".$param['where']."
        // ) 
        // SELECT * FROM X 
        // WHERE ROWID > ".$param['start']." AND ROWID <= ".$param['end']."";

         if($total){
            $sql = explode("WHERE ROWID >", $sql);
            $sql = $sql[0];
            return $this->db->query($sql)->num_rows();
        }
        // elseif($filter){
        //     $sql = explode("WHERE ROWID >", $sql);
        //     $sql = $sql[0];
        //     return $this->db->query($sql)->num_rows();
        // }
        else{
            // print_r($sql);die;
            return $this->db->query($sql)->result_array();

        }

    }
    private function _get_datatables($type) {
        
        $column_order = $this->column_order[$type];
        $column_search = $this->column_search[$type];
        $order = $this->order[$type];
        $start=[];$end=[];$where=[];
        // print_r($column_order);die;
        $i = 0;$a = 0;
        if(isset($_REQUEST['search'])||isset($_REQUEST['order'])||isset($_REQUEST['start'])||isset($_REQUEST['length'])){
        if($_REQUEST['search']['value']){
          foreach ($column_search as $item){
            if($_REQUEST['search']['value']) {  
              if($i===0) {
                //$this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                $this->db->like($item, $_REQUEST['search']['value']);
              } else {
                $this->db->or_like($item, $_REQUEST['search']['value']);
              }
              //if(count($this->column_search) - 1 == $i) //last loop
              //$this->db->group_end(); //close bracket
            }
            $i++;
          }
        }else{
          $like_arr = [];
          foreach ($_REQUEST['columns'] as $key) {
            if($key['search']['value']){
              $index = $column_search[$a-2];
              $like_arr[$index] = $key['search']['value'];
              /*if($a===0) {
                $this->db->like($column_search[$a-2], $key['search']['value']);
              } else {
                $this->db->or_like($column_search[$a-2], $key['search']['value']);
              }*/
            }
            $a++;
          }
          $this->db->like($like_arr);
        }
        
        if(isset($_REQUEST['order'])) {
          if($_REQUEST['order']['0']['column'] == 0) {
            // $this->db->order_by(key($order), $order[key($order)]);
          }else{
            $this->db->order_by($column_order[$_REQUEST['order']['0']['column']], strtoupper($_REQUEST['order']['0']['dir']));
          }
        } else if(isset($order)) {
          $this->db->order_by(key($order), $order[key($order)]);
        }

        $result =  $this->db->get_compiled_select();

        $query = explode("WHERE", $result);
        if(count($query) == 1){
            $pecah = explode("ORDER BY", $result);
            $where = "";
        }else{
            $pecah = explode("ORDER BY", $query[1]);
            $where = "WHERE ".$pecah[0];
        }
        // print_r($query);die;
        if(count($pecah[1])>0)
        $order = $pecah[1];
        // print_r($pecah[1]);die;
        $start = $_REQUEST['start'];
        $end   = $_REQUEST['length'] + $_REQUEST['start'];
     }
        return array(
          'start'           => $start,
          'end'             => $end,
          'order'           => $order,
          'where'           => $where
        );
    }
}
