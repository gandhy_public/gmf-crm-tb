<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use Illuminate\Database\Eloquent\Model as Eloquent;

class Mrm_model extends Eloquent {

    protected $table = 'TB_MRM';
    // protected $fillable = array();
    public $timestamps = false;
}
