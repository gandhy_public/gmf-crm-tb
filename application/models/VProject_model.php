<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use Illuminate\Database\Eloquent\Model as Eloquent;

class VProject_model extends Eloquent {

    protected $table = 'TB_V_PROJECT_LIST';
    public $timestamps = false;

}