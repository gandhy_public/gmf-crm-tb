<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use Illuminate\Database\Eloquent\Model as Eloquent;
use Illuminate\Database\Capsule\Manager as DB;

class Proc_model extends Eloquent {

    function base_url() {
        $domain = $_SERVER['HTTP_HOST'] . $_SERVER['SCRIPT_NAME'];
        $domain = preg_replace('/index.php.*/', '', $domain);
        if (!empty($_SERVER['HTTPS'])) {
            return 'https://' . $domain;
        } else {
            return 'http://' . $domain;
        }
    }

    function check_date() {
        $tbls = ['M_PMORDER', 'M_PMORDERH', 'M_POOLSTOCK', 'M_POOUTSTANDING', 'M_REVISION', 'M_REVISIONDETAIL', 'M_REVISIONDEVIATION', 'M_SALESBILLING', 'M_SALESORDER', 'M_STATUSPM', 'M_STOCK', 'M_WORKCENTER'];
        $clms = ['INSERT_DATE', 'UPDATE_DATE'];
        foreach ($tbls as $tbl) {
            foreach ($clms as $clm) {
                DB::statement("
                    IF NOT EXISTS (
                        SELECT
                            *
                        FROM
                            INFORMATION_SCHEMA.COLUMNS
                        WHERE
                            TABLE_NAME = '$tbl'
                            AND COLUMN_NAME = '$clm'
                    ) BEGIN ALTER TABLE
                        dbo.$tbl ADD $clm datetime DEFAULT (
                            GETDATE()
                        ) NOT NULL
                    END
                ");
            }
        }
    }

    function merge_pmorder($REVNR) {
        $where = '';
        if (is_array($REVNR)) {
            $REVNR_ARR = implode("','", $REVNR);
            $where = "AND dbo.M_PMORDERH.REVNR IN ('$REVNR_ARR')";
        } elseif ($REVNR != NULL) {
            $where = "AND dbo.M_PMORDERH.REVNR = '$REVNR'";
        }
        return DB::statement("
        WITH A AS (
                SELECT
                        DISTINCT dbo.M_PMORDERH.INSERT_DATE,
                        dbo.M_PMORDERH.EXT5,
                        dbo.M_PMORDERH.AUART,
                        dbo.M_PMORDERH.AUFNR,
                        dbo.M_PMORDERH.ERDAT,
                        dbo.M_PMORDERH.ERNAM,
                        dbo.M_PMORDERH.ILART,
                        dbo.M_PMORDERH.KTEXT,
                        dbo.M_PMORDERH.PHAS0,
                        dbo.M_PMORDERH.PHAS1,
                        dbo.M_PMORDERH.PHAS2,
                        dbo.M_PMORDERH.PHAS3,
                        dbo.M_PMORDERH.REVNR,
                        dbo.M_PMORDERH.[SMR_DEFORD] AS JC_REFF,
                        '' AS AUFPL,
                        '' AS APLZL_V,
                        '' AS RSPOS,
                        1 AS IS_ACTIVE
                FROM
                        dbo.M_PMORDERH
                WHERE
                        dbo.M_PMORDERH.AUART IN ( 'GA01', 'GA02' )
                        $where
                        ),
                B AS (
                SELECT
                        ROW_NUMBER() OVER( PARTITION BY AUFNR
                ORDER BY
                        INSERT_DATE DESC ) AS SEQ,
                        EXT5,
                        AUART,
                        AUFNR,
                        ERDAT,
                        ERNAM,
                        ILART,
                        KTEXT,
                        PHAS0,
                        PHAS1,
                        PHAS2,
                        PHAS3,
                        REVNR,
                        JC_REFF,
                        AUFPL,
                        APLZL_V,
                        RSPOS,
                        IS_ACTIVE
                FROM
                        A ),
                C AS (
                SELECT
                        *
                FROM
                        B
                WHERE
                        SEQ = 1 ) MERGE INTO
                        dbo.TB_M_PMORDER
                                USING C ON
                        C.AUFNR = dbo.TB_M_PMORDER.AUFNR
                        AND C.AUFPL = dbo.TB_M_PMORDER.AUFPL
                        AND C.APLZL_V = dbo.TB_M_PMORDER.APLZL_V
                        AND C.RSPOS = dbo.TB_M_PMORDER.RSPOS
                        WHEN MATCHED THEN UPDATE
                                SET
                                        EXT5 = C.EXT5,
                                        AUART = C.AUART,
                                        AUFNR = C.AUFNR,
                                        ERDAT = C.ERDAT,
                                        ERNAM = C.ERNAM,
                                        ILART = C.ILART,
                                        KTEXT = C.KTEXT,
                                        PHAS0 = C.PHAS0,
                                        PHAS1 = C.PHAS1,
                                        PHAS2 = C.PHAS2,
                                        PHAS3 = C.PHAS3,
                                        REVNR = C.REVNR,
                                        JC_REFF = C.JC_REFF
                                        --			IS_ACTIVE = C.IS_ACTIVE,
                                        --			STATUS = C.STATUS
                                        WHEN NOT MATCHED THEN INSERT
                                                ( AUART,
                                                EXT5,
                                                AUFNR,
                                                ERDAT,
                                                ERNAM,
                                                ILART,
                                                KTEXT,
                                                PHAS0,
                                                PHAS1,
                                                PHAS2,
                                                PHAS3,
                                                REVNR,
                                                JC_REFF,
                                                AUFPL,
                                                APLZL_V,
                                                RSPOS,
                                                IS_ACTIVE )
                                        VALUES ( C.AUART,
                                        C.EXT5,
                                        C.AUFNR,
                                        C.ERDAT,
                                        C.ERNAM,
                                        C.ILART,
                                        C.KTEXT,
                                        C.PHAS0,
                                        C.PHAS1,
                                        C.PHAS2,
                                        C.PHAS3,
                                        C.REVNR,
                                        C.JC_REFF,
                                        C.AUFPL,
                                        C.APLZL_V,
                                        C.RSPOS,
                                        C.IS_ACTIVE );
        ");
    }


    function merge_mrm($REVNR) {
        return DB::statement("
        WITH AA
        AS (SELECT DISTINCT AUFNR,
                                AUART,
                                REVNR,
                                CASE
                                WHEN AUART = 'GA01' THEN 'JC'
                                WHEN AUART = 'GA02' THEN 'MDR'
                                END AS CARD_TYPE
                FROM   M_PMORDER
                WHERE  REVNR = '$REVNR'
                        AND AUART IN ( 'GA01', 'GA02' )),
        BB
        AS (SELECT DISTINCT A.CARD_TYPE,
                                A.REVNR,
                                A.AUART,
                                M.AUFNR,
                                Cast (Replace(M.BDMNG, ',', '') AS FLOAT) AS 'BDMNG',
                                M.RSPOS,
                                M.MATNR,
                                M.MAKTX,
                                M.MEINS,
                                M.MTART, 
																M.INACT
                FROM   AA A
                        LEFT JOIN M_PMORDER M
                        ON A.AUFNR = M.AUFNR
                WHERE  M.MATNR != ''
                        AND M.SHKZG = 'H'),
        CC
        AS (SELECT B.MATNR,
                        Sum (B.BDMNG) AS TOTAL
                FROM   BB B
                WHERE  B.REVNR = '$REVNR'
                        AND B.AUART IN ( 'GA01', 'GA02' )
                        AND ISNUMERIC(B.BDMNG) = 1
                GROUP  BY B.MATNR),
        DD
        AS (SELECT ROW_NUMBER ()
                        OVER (
                        PARTITION BY B.AUFNR, B.REVNR, B.RSPOS
                        ORDER BY RSPOS DESC ) AS SEQ,
                        B.RSPOS                   AS NO_,
                        B.MATNR,
                        MAT.ALT_PART_NUMBER       AS ALTERNATE_PART_NUMBER,
                        B.MAKTX,
                        B.MTART,
                        NULL                      AS IPC,
                        B.AUFNR,
                        NULL                      AS STO_NUMBER,
                        NULL                      AS OUTBOND_DELIV,
                        NULL                      AS TO_NUM,
                        CASE
                        WHEN B.CARD_TYPE = 'MDR' THEN Cast (OH.[SMR_DEFORD] AS BIGINT
                                                        )
                        ELSE B.AUFNR
                        END                       AS JOBCARD_NUMBER,
                        B.CARD_TYPE,
                        NULL                      AS MRM_ISSUE_DATE,
                        B.BDMNG                   AS MENGE,
                        C.TOTAL                   AS TOTAL_QTY,
                        B.MEINS,
                        NULL                      AS STOCK_MANUAL,
                        NULL                      AS LGORT,
                        NULL                      AS MATERIAL_FULFILLMENT_STATUS,
                        NULL                      AS FULLFILLMENT_STATUS_DATE,
                        NULL                      AS FULLFILLMENT_REMARK,
                        NULL                      AS PO_DATE,
                        NULL                      AS PO_NUM,
                        NULL                      AS AWB_NUMBER,
                        NULL                      AS AWB_DATE,
                        NULL                      AS QTY_DELIVERED,
                        NULL                      AS QTY_REMAIN,
                        NULL                      AS MATERIAL_REMARK,
                        B.REVNR,
                        NULL                      AS DATE_CREATED,
                        CASE
                        WHEN B.CARD_TYPE = 'MDR' THEN Cast (OH.[SMR_DEFORD] AS BIGINT)
                        ELSE B.AUFNR
                        END                       AS CUST_JC_NUM,
												OH.KTEXT,
                        1                         AS IS_ACTIVE,
												B.INACT
                FROM   BB B
                        LEFT JOIN CC C
                        ON B.MATNR = C.MATNR
                        LEFT JOIN TB_M_MATERIAL MAT
                        ON B.MATNR = MAT.MATNR
                        LEFT JOIN M_PMORDERH OH
                        ON B.AUFNR = OH.AUFNR),
        EE
        AS (SELECT *,
                        ROW_NUMBER ()
                        OVER (
                        PARTITION BY REVNR
                        ORDER BY AUFNR, NO_ ASC ) AS SEQ_NUM
                FROM   DD
                WHERE  SEQ = 1)
                MERGE INTO dbo.TB_MRM USING EE ON EE.AUFNR = dbo.TB_MRM.AUFNR 
                        AND EE.REVNR = dbo.TB_MRM.REVNR 
                        AND EE.NO_ = dbo.TB_MRM.NO_ 
                WHEN MATCHED THEN
                UPDATE 
                        SET NO_ = EE.NO_,
                        MATNR = EE.MATNR,
                        MAKTX = EE.MAKTX,
                        MTART = EE.MTART,
                        AUFNR = EE.AUFNR,
                        JOBCARD_NUMBER = EE.JOBCARD_NUMBER,
                        CARD_TYPE = EE.CARD_TYPE,
                        MEINS = EE.MEINS,
                        REVNR = EE.REVNR,
                        KTEXT = EE.KTEXT,
                        CUST_JC_NUM = EE.CUST_JC_NUM,
                        INACT = EE.INACT
                WHEN NOT MATCHED THEN
                INSERT (
                                SEQ_NUM,
                                NO_,
                                MATNR,
                                ALTERNATE_PART_NUMBER,
                                MAKTX,
                                MTART,
                                IPC,
                                AUFNR,
                                STO_NUMBER,
                                OUTBOND_DELIV,
                                TO_NUM,
                                JOBCARD_NUMBER,
                                CARD_TYPE,
                                MRM_ISSUE_DATE,
                                MENGE,
                                TOTAL_QTY,
                                MEINS,
                                STOCK_MANUAL,
                                LGORT,
                                MATERIAL_FULFILLMENT_STATUS,
                                FULLFILLMENT_STATUS_DATE,
                                FULLFILLMENT_REMARK,
                                PO_DATE,
                                PO_NUM,
                                AWB_NUMBER,
                                AWB_DATE,
                                QTY_DELIVERED,
                                QTY_REMAIN,
                                MATERIAL_REMARK,
                                REVNR,
                                DATE_CREATED,
                                KTEXT,
                                CUST_JC_NUM,
                                IS_ACTIVE,
                                INACT 
                        )
                VALUES
                        (
                                EE.SEQ_NUM,
                                EE.NO_,
                                EE.MATNR,
                                EE.ALTERNATE_PART_NUMBER,
                                EE.MAKTX,
                                EE.MTART,
                                EE.IPC,
                                EE.AUFNR,
                                EE.STO_NUMBER,
                                EE.OUTBOND_DELIV,
                                EE.TO_NUM,
                                EE.JOBCARD_NUMBER,
                                EE.CARD_TYPE,
                                EE.MRM_ISSUE_DATE,
                                EE.MENGE,
                                EE.TOTAL_QTY,
                                EE.MEINS,
                                EE.STOCK_MANUAL,
                                EE.LGORT,
                                EE.MATERIAL_FULFILLMENT_STATUS,
                                EE.FULLFILLMENT_STATUS_DATE,
                                EE.FULLFILLMENT_REMARK,
                                EE.PO_DATE,
                                EE.PO_NUM,
                                EE.AWB_NUMBER,
                                EE.AWB_DATE,
                                EE.QTY_DELIVERED,
                                EE.QTY_REMAIN,
                                EE.MATERIAL_REMARK,
                                EE.REVNR,
                                EE.DATE_CREATED,
                                EE.KTEXT,
                                EE.CUST_JC_NUM,
                                EE.IS_ACTIVE,
                                EE.INACT 
                        );
                
        ");
    }


    function update_customer() {
        return DB::statement("
            WITH A AS ( SELECT
                    DISTINCT KUNNR, NAME1
                FROM
                    dbo.M_SALESORDER ) MERGE INTO
                    db_crmapps.dbo.CUSTOMER
                        USING A ON
                    A.KUNNR = db_crmapps.dbo.CUSTOMER.ID_CUSTOMER
                    WHEN MATCHED THEN UPDATE
                    SET
                        COMPANY_NAME = A.NAME1
                        WHEN NOT MATCHED THEN INSERT
                            ( ID_CUSTOMER, COMPANY_NAME )
                        VALUES ( A.KUNNR, A.NAME1 );
        ");
    }

    function update_pmorderh($REVNR = NULL) {
        if ($REVNR == NULL) {
            return DB::statement("
                WITH A AS ( SELECT
                        DISTINCT AUFNR, REVNR, KTEXT, ILART, ERNAM, ERDAT, PHAS0, PHAS1, PHAS2, PHAS3, IDAT2
                    FROM
                        dbo.M_PMORDER ) MERGE INTO
                        db_crmapps.dbo.M_PMORDERH
                            USING A ON
                        A.AUFNR = db_crmapps.dbo.M_PMORDERH.AUFNR
                        WHEN MATCHED THEN UPDATE
                        SET
                            AUFNR = A.AUFNR,
                            REVNR = A.REVNR,
                            KTEXT = A.KTEXT,
                            ILART = A.ILART,
                            ERNAM = A.ERNAM,
                            ERDAT = A.ERDAT,
                            PHAS0 = A.PHAS0,
                            PHAS1 = A.PHAS1,
                            PHAS2 = A.PHAS2,
                            PHAS3 = A.PHAS3,
                            IDAT2 = A.IDAT2;
            ");
        } else {
            return DB::statement("
                WITH A AS ( SELECT
                        DISTINCT AUFNR, REVNR, KTEXT, ILART, ERNAM, ERDAT, PHAS0, PHAS1, PHAS2, PHAS3, IDAT2
                    FROM
                        dbo.M_PMORDER WHERE REVNR = '$REVNR') MERGE INTO
                        db_crmapps.dbo.M_PMORDERH
                            USING A ON
                        A.AUFNR = db_crmapps.dbo.M_PMORDERH.AUFNR
                        WHEN MATCHED THEN UPDATE
                        SET
                            AUFNR = A.AUFNR,
                            REVNR = A.REVNR,
                            KTEXT = A.KTEXT,
                            ILART = A.ILART,
                            ERNAM = A.ERNAM,
                            ERDAT = A.ERDAT,
                            PHAS0 = A.PHAS0,
                            PHAS1 = A.PHAS1,
                            PHAS2 = A.PHAS2,
                            PHAS3 = A.PHAS3,
                            IDAT2 = A.IDAT2;
            ");
        }
    }

    function get_notif($page = 1, $limit = 10, $REVNR = NULL) {
        $where = '';
        if ($REVNR != NULL) {
            $where = "AND REVNR = '$REVNR'";
        }
        return DB::select("
            WITH A AS (
            SELECT
                    AUFNR,
                    REVNR,
                    STATUS,
                    PHAS0,
                    PHAS1,
                    PHAS2,
                    PHAS3
            FROM
                    TB_M_PMORDER
            WHERE
                    AUART = 'GA01'
                    AND LEN(REVNR) > 1
                    AND LEN(RSPOS) <= 1
                    AND LEN(APLZL_V) <= 1
                    AND LEN(AUFPL) <= 1
                    $where
            GROUP BY
                    AUFNR,
                    REVNR,
                    STATUS,
                    PHAS0,
                    PHAS1,
                    PHAS2,
                    PHAS3 ),
            B AS (
            SELECT
                    AUFNR,
                    REVNR,
                    STATUS,
                    CASE
                            WHEN PHAS3 LIKE 'X'
                            OR PHAS2 LIKE 'X' THEN 'CLOSED'
                            WHEN PHAS1 LIKE 'X' THEN 'PROGRESS'
                            WHEN PHAS0 LIKE 'X' THEN 'OPEN'
                    END AS STATUS_SAP
            FROM
                    A ),
            C AS (
            SELECT
                    *
            FROM
                    B
            WHERE
                    ( UPPER( STATUS_SAP ) IN( 'OPEN',
                    'CLOSED' )
                    AND UPPER( STATUS_SAP )!= UPPER( STATUS ) )
                    OR( UPPER( STATUS_SAP )= 'PROGRESS'
                    AND UPPER( STATUS ) IN( 'OPEN',
                    'CLOSED' ) )),
            D AS (
            SELECT
                    ROW_NUMBER() OVER (
            ORDER BY
                    AUFNR ) AS SEQ,
                    *
            FROM
                    C ) SELECT
                    SEQ,
                    AUFNR,
                    REVNR,
                    STATUS,
                    STATUS_SAP
            FROM
                    D
            WHERE
                    SEQ >= $page
                    AND SEQ < ( $limit + $page );
        ");
    }

    function get_notif_count($REVNR = NULL) {
        $where = '';
        if ($REVNR != NULL) {
            $where = "AND REVNR = '$REVNR'";
        }
        return DB::select("
            WITH A AS (
            SELECT
                    AUFNR,
                    REVNR,
                    STATUS,
                    PHAS0,
                    PHAS1,
                    PHAS2,
                    PHAS3
            FROM
                    TB_M_PMORDER
            WHERE
                    AUART = 'GA01'
                    AND LEN(REVNR) > 1
                    AND LEN(RSPOS) <= 1
                    AND LEN(APLZL_V) <= 1
                    AND LEN(AUFPL) <= 1
                    $where
            GROUP BY
                    AUFNR,
                    REVNR,
                    STATUS,
                    PHAS0,
                    PHAS1,
                    PHAS2,
                    PHAS3 ),
            B AS (
            SELECT
                    AUFNR,
                    REVNR,
                    STATUS,
                    CASE
                            WHEN PHAS3 LIKE 'X'
                            OR PHAS2 LIKE 'X' THEN 'CLOSED'
                            WHEN PHAS1 LIKE 'X' THEN 'PROGRESS'
                            WHEN PHAS0 LIKE 'X' THEN 'OPEN'
                    END AS STATUS_SAP
            FROM
                    A )
            SELECT
                    COUNT(AUFNR) AS CNT
            FROM
                    B
            WHERE
                    ( UPPER( STATUS_SAP ) IN( 'OPEN',
                    'CLOSED' )
                    AND UPPER( STATUS_SAP )!= UPPER( STATUS ) )
                    OR( UPPER( STATUS_SAP )= 'PROGRESS'
                    AND UPPER( STATUS ) IN( 'OPEN',
                    'CLOSED' ) );
        ");
    }

    function get_phase_progress($REVNR = NULL) {
        return DB::select("
        WITH TOTAL AS (
                SELECT
                        PHASE,
                        COUNT(PHASE) AS TOTAL
                FROM
                        TB_M_PMORDER
                        WHERE REVNR = $REVNR
                        AND IS_ACTIVE = 1
                        AND PHASE IS NOT NULL
                        AND PHASE <> ''
                        GROUP BY PHASE
                ),
                [OPEN] AS (
                SELECT
                        PHASE,
                        COUNT(PHASE) AS [OPEN]
                FROM
                        TB_M_PMORDER
                        WHERE REVNR = $REVNR
                        AND IS_ACTIVE = 1
                        AND PHASE IS NOT NULL
                        AND PHASE <> ''
                        AND STATUS = 'OPEN'
                        GROUP BY PHASE
                ),
                [CLOSED] AS (
                SELECT
                        PHASE,
                        COUNT(PHASE) AS [CLOSED]
                FROM
                        TB_M_PMORDER
                        WHERE REVNR = $REVNR
                        AND IS_ACTIVE = 1
                        AND PHASE IS NOT NULL
                        AND PHASE <> ''
                        AND STATUS = 'CLOSED'
                        GROUP BY PHASE
                ),
                [PROGRESS] AS (
                SELECT
                        PHASE,
                        COUNT(PHASE) AS [PROGRESS]
                FROM
                        TB_M_PMORDER
                        WHERE REVNR = $REVNR
                        AND IS_ACTIVE = 1
                        AND PHASE IS NOT NULL
                        AND PHASE <> ''
                        AND (STATUS <> 'CLOSED' AND STATUS <> 'OPEN')
                        GROUP BY PHASE
                )
                
                SELECT 
                        TOTAL.PHASE, 
                        COALESCE(TOTAL.TOTAL, 0) AS TOTAL, 
                        COALESCE([OPEN].[OPEN], 0) AS [OPEN],
                        COALESCE([CLOSED].[CLOSED], 0) AS [CLOSED],
                        COALESCE([PROGRESS].[PROGRESS], 0) AS [PROGRESS]
                        FROM TOTAL 
                LEFT JOIN [OPEN] ON [OPEN].PHASE = TOTAL.PHASE
                LEFT JOIN [CLOSED] ON [CLOSED].PHASE = TOTAL.PHASE
                LEFT JOIN [PROGRESS] ON [PROGRESS].PHASE = TOTAL.PHASE
                
        ");
    }

    function get_jc_progress($REVNR) {
        $REVNR_ARR = implode("','", $REVNR);
        return DB::select("
            WITH A AS (
            SELECT
                    DISTINCT REVNR,
                    AUFNR,
                    AUART,
                    STATUS,
                    MDR_STATUS
            FROM
                    dbo.TB_M_PMORDER
            WHERE
                    IS_ACTIVE = 1
                    AND LEN(RSPOS) <= 1
                    AND LEN(AUFPL) <= 1
                    AND LEN(APLZL_V) <= 1
                    AND AUART IN ( 'GA01',
                    'GA02' )
                    AND REVNR IN ('$REVNR_ARR') ),
            B AS (
            SELECT
                    REVNR,
                    COUNT(AUFNR) AS JC_TOTAL
            FROM
                    A
            WHERE
                    AUART = 'GA01'
            GROUP BY
                    REVNR ),
            C AS (
            SELECT
                    REVNR,
                    COUNT(AUFNR) AS JC_CLOSED
            FROM
                    A
            WHERE
                    AUART = 'GA01'
                    AND STATUS = 'CLOSED'
            GROUP BY
                    REVNR ),
            D AS (
            SELECT
                    REVNR,
                    COUNT(AUFNR) AS MDR_TOTAL
            FROM
                    A
            WHERE
                    AUART = 'GA02'
            GROUP BY
                    REVNR ),
            E AS (
            SELECT
                    REVNR,
                    COUNT(AUFNR) AS MDR_CLOSED
            FROM
                    A
            WHERE
                    AUART = 'GA02'
                    AND MDR_STATUS = 'CLOSED'
            GROUP BY
                    REVNR ) SELECT
                    B.REVNR,
                    COALESCE( B.JC_TOTAL,
                    0 ) AS JC_TOTAL,
                    COALESCE( C.JC_CLOSED,
                    0 ) AS JC_CLOSED,
                    COALESCE( D.MDR_TOTAL,
                    0 ) AS MDR_TOTAL,
                    COALESCE( E.MDR_CLOSED,
                    0 ) AS MDR_CLOSED
            FROM
                    B
            LEFT JOIN C ON
                    B.REVNR = C.REVNR
            LEFT JOIN D ON
                    B.REVNR = D.REVNR
            LEFT JOIN E ON
                    B.REVNR = E.REVNR
        ");
    }

    function get_jc($REVNR, $ORDER = 'SEQ_NUM', $AUFNR = 0, $FILTER = '',$fdate=false) {
        return DB::select("
            WITH P AS (
                SELECT
                    DISTINCT TB_M_PMORDER.AUFNR,
                    TB_M_PMORDER.EXT5,
                    TB_M_PMORDER.KTEXT,
                    TB_M_PMORDER.REVNR,
                    TB_M_PMORDER.AUART,
                    TB_M_PMORDER.ILART,
                    TB_M_PMORDER.SKILL AS SKILL,
                    TB_M_PMORDER.PHASE,
                    TB_M_PMORDER.STATUS AS STATUS,
                    TB_M_PMORDER.AREA,
                    TB_M_PMORDER.DAY,
                    TB_M_PMORDER.DATECLOSE,
                    TB_M_PMORDER.DATEPROGRESS,
                    TB_M_PMORDER.REMARK,
                    TB_M_PMORDER.[FREETEXT],
                    TB_M_PMORDER.CABINSTATUS,
                    TB_M_PMORDER.DOC_SENT_STATUS,
                    TB_M_PMORDER.MAT_FULLFILLMENT_STATUS,
                    TB_M_PMORDER.JC_REFF,
                        CASE 
                        WHEN 
                            CHARINDEX( ':', TB_M_PMORDER.KTEXT ) > 0  
                        THEN 
                            SUBSTRING (TB_M_PMORDER.KTEXT, CHARINDEX( ']', TB_M_PMORDER.KTEXT ) + 1,ABS(CHARINDEX( ':', TB_M_PMORDER.KTEXT ) - CHARINDEX( ']', KTEXT ) - 1 ))
                        ELSE ''
                        END AS 'CUST_JC_NUM',
                    SUBSTRING ( TB_M_PMORDER.KTEXT,CHARINDEX( '[',TB_M_PMORDER.KTEXT ) + 1,ABS( CHARINDEX( ']', KTEXT ) - CHARINDEX( '[', KTEXT ) - 1 )) AS ITVAL,
                    IS_ACTIVE,
                    CASE 
                    WHEN SUBSTRING(TXT.SYSTEMSTATUS, 0, 9) LIKE 'REL PCNF' 
                            THEN 'PROGRESS'
                    WHEN SUBSTRING(TXT.SYSTEMSTATUS, 0, 9) LIKE 'REL CNF' 
                            THEN 'FINAL'
                    WHEN SUBSTRING(TXT.SYSTEMSTATUS, 0, 5) LIKE 'TECO' 
                            THEN 'CLOSED'
                    WHEN (SUBSTRING(TXT.SYSTEMSTATUS, 0, 4) LIKE 'REL') 
                            OR (SUBSTRING(TXT.SYSTEMSTATUS, 0, 5) LIKE 'CRTD')
                            THEN 'OPEN'
                END AS STATUS_SAP
                FROM
                        TB_M_PMORDER
                LEFT JOIN M_ORDERTEXT TXT ON TB_M_PMORDER.AUFNR = TXT.AUFNR
                WHERE
                    AUART = 'GA01' AND 
                    REVNR = '$REVNR' AND 
                    LEN(RSPOS) <= 1 AND 
                    LEN(APLZL_V) <= 1 AND 
                    LEN(AUFPL) <= 1
            ),
            MDR AS (
                SELECT
                    ( JC_REFF * 1 ) AS JC_REFF,
                    COUNT( DISTINCT AUFNR ) AS REFF_COUNT
                FROM
                    TB_M_PMORDER
                WHERE
                    AUART = 'GA02' AND 
                    REVNR = '$REVNR'
                GROUP BY
                    JC_REFF 
             ),
            W AS (
                SELECT
                    DISTINCT 
                        M_PMORDER.AUFNR,
                        M_PMORDER.VORNR,
                        M_PMORDER.ARBEI
                FROM
                    M_PMORDERH 
                LEFT JOIN  
                    M_PMORDER ON M_PMORDERH.AUFNR = M_PMORDER.AUFNR
                WHERE
                    M_PMORDERH.AUART = 'GA01' AND 
                    M_PMORDERH.REVNR = '$REVNR' 
            ),
            L AS (
                SELECT
                    AUFNR,
                    SUM ( CAST ( REPLACE( ARBEI, ',', '' ) AS FLOAT )) AS 'MHRS'
                FROM
                    W
                GROUP BY AUFNR 
            ),
            Y AS (
                SELECT
                    P.*,
                    L.MHRS,
                    MDR.REFF_COUNT AS JC_REFF_MDR_VAL
                FROM
                    P
                LEFT JOIN 
                    L ON P.AUFNR = L.AUFNR
                LEFT JOIN 
                    MDR ON MDR.JC_REFF = P.AUFNR
                WHERE
                    P.REVNR = '$REVNR' AND 
                    ( 
                        $AUFNR = 0 OR 
                        $AUFNR = P.AUFNR 
                    ) AND 
                    P.AUART = 'GA01' AND 
                    P.IS_ACTIVE = 1 AND 
                    ( 
                        ( P.AUFNR LIKE '%' + '$FILTER' + '%' )
                        OR ( '$FILTER' = ''
                        OR P.KTEXT LIKE '%' + '$FILTER' + '%' )
                        OR ( '$FILTER' = ''
                        OR P.CUST_JC_NUM LIKE '%' + '$FILTER' + '%' )
                        OR ( '$FILTER' = ''
                        OR P.ILART LIKE '%' + '$FILTER' + '%' )
                        OR ( '$FILTER' = ''
                        OR P.ITVAL LIKE '%' + '$FILTER' + '%' )
                        OR ( '$FILTER' = ''
                        OR P.AREA LIKE '%' + '$FILTER' + '%' )
                        OR ( '$FILTER' = ''
                        OR P.SKILL LIKE '%' + '$FILTER' + '%' )
                        OR ( '$FILTER' = ''
                        OR P.PHASE LIKE '%' + '$FILTER' + '%' )
                        OR ( '$FILTER' = ''
                        OR P.[DAY] LIKE '%' + '$FILTER' + '%' )
                        OR ( '$FILTER' = ''
                        OR P.REMARK LIKE '%' + '$FILTER' + '%' )
                        OR ( '$FILTER' = ''
                        OR P.[FREETEXT] LIKE '%' + '$FILTER' + '%' )
                        OR ( '$FILTER' = ''
                        OR P.DOC_SENT_STATUS LIKE '%' + '$FILTER' + '%' )
                        OR ( '$FILTER' = ''
                        OR P.DATEPROGRESS LIKE '%' + '$FILTER' + '%' )
                        OR ( '$FILTER' = ''
                        OR P.MAT_FULLFILLMENT_STATUS LIKE '%' + '$FILTER' + '%' )
                        OR ( '$FILTER' = ''
                        OR P.CABINSTATUS LIKE '%' + '$FILTER' + '%' )
                        OR ( '$FILTER' = ''
                        OR P.STATUS_SAP LIKE '%' + '$FILTER' + '%' )
                        OR ( '$FILTER' = ''
                        OR P.STATUS LIKE '%' + '$FILTER' + '%' ) 
                    ) 
            ) 

            SELECT
                CAST(ROUND(EXT5, 0) AS NUMERIC(12,0)) AS SEQ_NUM,
                AUFNR,
                '<button class=\"btn btn-sm btn-danger delete\" data-id=\"' + AUFNR + '\" onclick=\"jc_delete(' + AUFNR + ')\"><i class=\"fa fa-trash-o\" aria-hidden=\"true\"></i> </button>' AS AUFNR_DEL,
                '<a href=\"" . $this->base_url() . "projects/crud_mdr/' + REVNR + '/' + AUFNR + '/jc\">' + AUFNR + '</a>' AS AUFNR_LINK,
                '<a style=\"cursor: pointer;\" onclick=\"longtextswal(' + AUFNR + ')\">' + KTEXT + '</a>' AS KTEXT_LINK,
                KTEXT,
                REVNR,
                AUART,
                ILART,
                SKILL,
                PHASE,
                STATUS,
                AREA,
                DAY,".
                ( ($fdate==true) ?
                    "CONVERT(varchar,DATECLOSE,106)"
                :
                     "DATECLOSE"
                 ).
                " as DATECLOSE ,".
                ( ($fdate==true) ?
                    "CONVERT(varchar,DATEPROGRESS,106)"
                :
                     "DATEPROGRESS"
                 ).
                " as
                DATEPROGRESS,
                REMARK,
                [FREETEXT],
                CABINSTATUS,
                DOC_SENT_STATUS,
                MAT_FULLFILLMENT_STATUS,
                JC_REFF,
                CUST_JC_NUM,
                ITVAL,
                IS_ACTIVE,
                STATUS_SAP,
                CAST(ROUND(MHRS,2) AS VARCHAR) AS MHRS,
                '<a style=\"cursor: pointer;\" onclick=get_mdr(' + AUFNR + ')  >' + CONVERT(VARCHAR(12), JC_REFF_MDR_VAL) + '</a>' AS JC_REFF_MDR_VAL
            FROM
                Y
            ORDER BY
                        $ORDER
        ");
    }

    function get_mrm($REVNR, $ORDER = 'SEQ_NUM', $AUFNR = '', $FILTER = '', $PAGE = 1, $LIMIT = 100) {
        return DB::select("
            WITH A AS (
                SELECT
                    ID AS Real_ID,
                    SEQ_NUM AS ID,
                    NO_,
                    MATNR,
                    ALTERNATE_PART_NUMBER,
                    MAKTX,
                    MTART,
                    IPC,
                    AUFNR,
                    STO_NUMBER,
                    OUTBOND_DELIV,
                    TO_NUM,
                    JOBCARD_NUMBER,
                    CARD_TYPE,
                    MRM_ISSUE_DATE,
                    CASE 
                        WHEN MENGE = '' 
                        THEN CAST ( CONVERT ( DECIMAL ( 10, 2 ), 0 ) AS nvarchar )
                        ELSE CAST ( CONVERT ( DECIMAL ( 10, 2 ), MENGE ) AS nvarchar )
                    END AS MENGE,
                    MEINS,
                    STOCK_MANUAL,
                    LGORT,
                    MATERIAL_FULFILLMENT_STATUS,
                    FULLFILLMENT_STATUS_DATE,
                    FULLFILLMENT_REMARK,
                    PO_DATE,
                    PO_NUM,
                    AWB_NUMBER,
                    AWB_DATE,
                    QTY_DELIVERED,
                    QTY_REMAIN,
                    MATERIAL_REMARK,
                    REVNR,
                    DATE_CREATED,
                    KTEXT,
                    CUST_JC_NUM, 
                    IS_ACTIVE
                FROM
                    TB_MRM
                WHERE
                    REVNR = '$REVNR'
                    AND ( '$AUFNR' = ''
                    OR '$AUFNR' = AUFNR )
                    AND IS_ACTIVE = 1
                    AND (INACT IS NULL OR INACT <> 'X')
                    
            ),
            B AS (
                SELECT
                    MATNR,
                    SUM ( CAST ( REPLACE( MENGE,',','' ) AS FLOAT ) ) AS TOTAL_QTY
                FROM
                    A
                GROUP BY
                    MATNR 
            ),
            C AS (
                SELECT
                    ROW_NUMBER ( ) OVER ( PARTITION BY A.REVNR ORDER BY A.ID ASC ) AS SEQ_NUM,
                    A.Real_ID,
                    A.ID,
                    A.NO_,
                    A.MATNR,
                    A.ALTERNATE_PART_NUMBER,
                    A.MAKTX,
                    A.MTART,
                    A.IPC,
                    A.AUFNR,
                    A.STO_NUMBER,
                    A.OUTBOND_DELIV,
                    A.TO_NUM,
                    A.JOBCARD_NUMBER,
                    A.CARD_TYPE,
                    A.MRM_ISSUE_DATE,
                    A.MENGE,
                    A.MEINS,
                    A.STOCK_MANUAL,
                    A.LGORT,
                    A.MATERIAL_FULFILLMENT_STATUS,
                    A.FULLFILLMENT_STATUS_DATE,
                    A.FULLFILLMENT_REMARK,
                    A.PO_DATE,
                    A.PO_NUM,
                    A.AWB_NUMBER,
                    A.AWB_DATE,
                    A.QTY_DELIVERED,
                    A.QTY_REMAIN,
                    A.MATERIAL_REMARK,
                    A.REVNR,
                    A.DATE_CREATED,
                    A.KTEXT,
                    A.CUST_JC_NUM,
                    A.IS_ACTIVE,
                    Cast(CONVERT(DECIMAL(10,2), B.TOTAL_QTY) as nvarchar) AS TOTAL_QTY
                FROM
                    A
                LEFT JOIN B ON A.MATNR = B.MATNR 
            ),
            D AS ( 
                SELECT
                    ROW_NUMBER ( ) OVER ( ORDER BY REVNR, ID ASC ) AS PAGE_NUM,
                    SEQ_NUM,
                    ID,
                    NO_,
                    MATNR,
                    ALTERNATE_PART_NUMBER,
                    MAKTX,
                    MTART,
                    IPC,
                    AUFNR,
                    STO_NUMBER,
                    OUTBOND_DELIV,
                    TO_NUM,
                    JOBCARD_NUMBER,
                    CARD_TYPE,
                    MRM_ISSUE_DATE,
                    MENGE,
                    MEINS,
                    STOCK_MANUAL,
                    LGORT,
                    MATERIAL_FULFILLMENT_STATUS,
                    FULLFILLMENT_STATUS_DATE,
                    FULLFILLMENT_REMARK,
                    PO_DATE,
                    PO_NUM,
                    AWB_NUMBER,
                    AWB_DATE,
                    QTY_DELIVERED,
                    QTY_REMAIN,
                    MATERIAL_REMARK,
                    REVNR,
                    DATE_CREATED,
                    KTEXT,
                    CUST_JC_NUM,
                    IS_ACTIVE,
                    TOTAL_QTY,                    
                    '<a href=\"#\" onclick=\"longtextswal(' + CAST(AUFNR AS VARCHAR) + ')\">' + KTEXT + '</a>' AS KTEXT_LINK,
                    '<a href=\"" . base_url() . "projects/' + (CASE WHEN CARD_TYPE = 'JC' THEN 'crud_jobcard/' ELSE 'crud_mdr/' END) + CAST(REVNR AS VARCHAR) + '/' + CAST(AUFNR AS VARCHAR) + '/MRM\">' + CAST(AUFNR AS VARCHAR) + '</a>' AS AUFNR_LINK,
                    '<button class=\"btn btn-sm btn-danger delete\" data-id=\"' + CONCAT(REVNR,'-',Real_ID) + '\" onclick=\"mrm_delete(this)\"><i class=\"fa fa-trash-o\" aria-hidden=\"true\"></i> </button>' AS AUFNR_DEL
                FROM
                    C
                WHERE 
                    -- AND 
                    ( 
                        (AUFNR LIKE '%' + '$FILTER' + '%' )
                        OR ( '$FILTER' = '' OR ID LIKE '%' + '$FILTER' + '%' )
                        OR ( '$FILTER' = '' OR MATNR LIKE '%' + '$FILTER' + '%' )
                        OR ( '$FILTER' = '' OR ALTERNATE_PART_NUMBER LIKE '%' + '$FILTER' + '%' )
                        OR ( '$FILTER' = '' OR MAKTX LIKE '%' + '$FILTER' + '%' )
                        OR ( '$FILTER' = '' OR MTART LIKE '%' + '$FILTER' + '%' )
                        OR ( '$FILTER' = '' OR IPC LIKE '%' + '$FILTER' + '%' )
                        OR ( '$FILTER' = '' OR AUFNR LIKE '%' + '$FILTER' + '%' )
                        OR ( '$FILTER' = '' OR STO_NUMBER LIKE '%' + '$FILTER' + '%' )
                        OR ( '$FILTER' = '' OR OUTBOND_DELIV LIKE '%' + '$FILTER' + '%' )
                        OR ( '$FILTER' = '' OR TO_NUM LIKE '%' + '$FILTER' + '%' )
                        OR ( '$FILTER' = '' OR JOBCARD_NUMBER LIKE '%' + '$FILTER' + '%' )
                        OR ( '$FILTER' = '' OR CARD_TYPE LIKE '%' + '$FILTER' + '%' )
                        OR ( '$FILTER' = '' OR MRM_ISSUE_DATE LIKE '%' + '$FILTER' + '%' )
                        OR ( '$FILTER' = '' OR MENGE LIKE '%' + '$FILTER' + '%' )
                        OR ( '$FILTER' = '' OR MEINS LIKE '%' + '$FILTER' + '%' )
                        OR ( '$FILTER' = '' OR STOCK_MANUAL LIKE '%' + '$FILTER' + '%' )
                        OR ( '$FILTER' = '' OR LGORT LIKE '%' + '$FILTER' + '%' )
                        OR ( '$FILTER' = '' OR MATERIAL_FULFILLMENT_STATUS LIKE '%' + '$FILTER' + '%' )
                        OR ( '$FILTER' = '' OR FULLFILLMENT_STATUS_DATE LIKE '%' + '$FILTER' + '%' )
                        OR ( '$FILTER' = '' OR FULLFILLMENT_REMARK LIKE '%' + '$FILTER' + '%' )
                        OR ( '$FILTER' = '' OR PO_DATE LIKE '%' + '$FILTER' + '%' )
                        OR ( '$FILTER' = '' OR PO_NUM LIKE '%' + '$FILTER' + '%' )
                        OR ( '$FILTER' = '' OR AWB_NUMBER LIKE '%' + '$FILTER' + '%' )
                        OR ( '$FILTER' = '' OR AWB_DATE LIKE '%' + '$FILTER' + '%' )
                        OR ( '$FILTER' = '' OR QTY_DELIVERED LIKE '%' + '$FILTER' + '%' )
                        OR ( '$FILTER' = '' OR QTY_REMAIN LIKE '%' + '$FILTER' + '%' )
                        OR ( '$FILTER' = '' OR MATERIAL_REMARK LIKE '%' + '$FILTER' + '%' )
                        OR ( '$FILTER' = '' OR REVNR LIKE '%' + '$FILTER' + '%' )
                        OR ( '$FILTER' = '' OR DATE_CREATED LIKE '%' + '$FILTER' + '%' )
                        OR ( '$FILTER' = '' OR KTEXT LIKE '%' + '$FILTER' + '%' )
                        OR ( '$FILTER' = '' OR CUST_JC_NUM LIKE '%' + '$FILTER' + '%' )
                    )
            )

            SELECT * FROM D               
            WHERE
                PAGE_NUM >= $PAGE AND 
                PAGE_NUM < ( $LIMIT + $PAGE )
            ORDER BY $ORDER
            ");
    }

    function get_mrm_all($REVNR) {
        return DB::select( DB::raw("
            WITH A AS (
                SELECT
                    SEQ_NUM AS ID,
                    NO_,
                    MATNR,
                    ALTERNATE_PART_NUMBER,
                    MAKTX,
                    MTART,
                    IPC,
                    AUFNR,
                    STO_NUMBER,
                    OUTBOND_DELIV,
                    TO_NUM,
                    JOBCARD_NUMBER,
                    CARD_TYPE,
                    MRM_ISSUE_DATE,
                    CASE 
                    WHEN MENGE = '' 
                    THEN CAST ( CONVERT ( DECIMAL ( 10, 2 ), 0 ) AS nvarchar )
                    ELSE CAST ( CONVERT ( DECIMAL ( 10, 2 ), MENGE ) AS nvarchar )
                        END AS MENGE,
                    MEINS,
                    STOCK_MANUAL,
                    LGORT,
                    MATERIAL_FULFILLMENT_STATUS,
                    FULLFILLMENT_STATUS_DATE,
                    FULLFILLMENT_REMARK,
                    PO_DATE,
                    PO_NUM,
                    AWB_NUMBER,
                    AWB_DATE,
                    QTY_DELIVERED,
                    QTY_REMAIN,
                    MATERIAL_REMARK,
                    REVNR,
                    DATE_CREATED,
                    KTEXT,
                    CUST_JC_NUM, 
                    IS_ACTIVE
                FROM
                    TB_MRM
                WHERE
                    REVNR = '$REVNR'
                    AND IS_ACTIVE = 1 
            ),
            B AS (
                SELECT
                    MATNR,
                    SUM ( CAST ( REPLACE( MENGE,',','' ) AS FLOAT ) ) AS TOTAL_QTY
                FROM
                    A
                GROUP BY
                    MATNR 
            ),
            C AS (
                SELECT
                    ROW_NUMBER ( ) OVER ( PARTITION BY A.REVNR ORDER BY A.ID ASC ) AS SEQ_NUM,
                    A.ID,
                    A.NO_,
                    A.MATNR,
                    A.ALTERNATE_PART_NUMBER,
                    A.MAKTX,
                    A.MTART,
                    A.IPC,
                    A.AUFNR,
                    A.STO_NUMBER,
                    A.OUTBOND_DELIV,
                    A.TO_NUM,
                    A.JOBCARD_NUMBER,
                    A.CARD_TYPE,
                    A.MRM_ISSUE_DATE,
                    A.MENGE,
                    A.MEINS,
                    A.STOCK_MANUAL,
                    A.LGORT,
                    A.MATERIAL_FULFILLMENT_STATUS,
                    A.FULLFILLMENT_STATUS_DATE,
                    A.FULLFILLMENT_REMARK,
                    A.PO_DATE,
                    A.PO_NUM,
                    A.AWB_NUMBER,
                    A.AWB_DATE,
                    A.QTY_DELIVERED,
                    A.QTY_REMAIN,
                    A.MATERIAL_REMARK,
                    A.REVNR,
                    A.DATE_CREATED,
                    A.KTEXT,
                    A.CUST_JC_NUM,
                    A.IS_ACTIVE,
                    Cast(CONVERT(DECIMAL(10,2), B.TOTAL_QTY) as nvarchar) AS TOTAL_QTY
                FROM
                    A
                LEFT JOIN B ON A.MATNR = B.MATNR 
            ),
            D AS ( 
                SELECT
                    ROW_NUMBER ( ) OVER ( ORDER BY REVNR, ID ASC ) AS PAGE_NUM,
                    SEQ_NUM,
                    ID,
                    NO_,
                    MATNR,
                    ALTERNATE_PART_NUMBER,
                    MAKTX,
                    MTART,
                    IPC,
                    AUFNR,
                    STO_NUMBER,
                    OUTBOND_DELIV,
                    TO_NUM,
                    JOBCARD_NUMBER,
                    CARD_TYPE,
                    MRM_ISSUE_DATE,
                    MENGE,
                    MEINS,
                    STOCK_MANUAL,
                    LGORT,
                    MATERIAL_FULFILLMENT_STATUS,
                    FULLFILLMENT_STATUS_DATE,
                    FULLFILLMENT_REMARK,
                    PO_DATE,
                    PO_NUM,
                    AWB_NUMBER,
                    AWB_DATE,
                    QTY_DELIVERED,
                    QTY_REMAIN,
                    MATERIAL_REMARK,
                    REVNR,
                    DATE_CREATED,
                    KTEXT,
                    CUST_JC_NUM,
                    IS_ACTIVE,
                    TOTAL_QTY,                    
                    KTEXT AS KTEXT_LINK,
                    CAST(AUFNR AS VARCHAR) AS AUFNR_LINK,
                    '<button class=\"btn btn-sm btn-danger delete\" data-id=\"' + CONCAT(REVNR,'-',ID) + '\" onclick=\"mrm_delete(this)\"><i class=\"fa fa-trash-o\" aria-hidden=\"true\"></i> </button>' AS AUFNR_DEL
                FROM
                    C
            )

            SELECT * FROM D
            "));
    }

    function get_mdr($REVNR, $ORDER = 'SEQ_NUM', $AUFNR = 0, $JC_REF = '', $FILTER = '') {
        return DB::select("
            WITH B AS (
            SELECT
                    DISTINCT
                    CASE
                            WHEN LEN(TB.ERDAT) = 8 THEN SUBSTRING( TB.ERDAT, 7, 2 ) + '-' + SUBSTRING( TB.ERDAT, 5, 2 ) + '-' + SUBSTRING( TB.ERDAT, 1, 4 )
                            ELSE NULL
                    END AS ERDAT,
                    TB.EXT5,
                    TB.ERNAM,
                    cast( TB.JC_REFF as bigint ) AS JC_REF,
                    CASE 
                    WHEN CHARINDEX( ':', TB2.KTEXT ) > 0  
                            THEN 
                    SUBSTRING (
                            TB2.KTEXT, CHARINDEX( ']', TB2.KTEXT ) + 1,
                            ABS(CHARINDEX( ':', TB2.KTEXT ) - CHARINDEX( ']', TB2.KTEXT ) - 1 ))
                            ELSE '' END AS 'CUST_JC_NUM',
                    TB.REVNR,
                    TB.AREA,
                    TB.KTEXT,
                    TB.SKILL,
                    TB.DATE_PE,
                    TB.STATUS,
                    TB.MATSTATUS,
                    TB.REMARK,
                    TB.DATECLOSE,
                    TB.[FREETEXT],
                    TB.DATEPROGRESS,
                    TB.DAY,
                    TB.CABINSTATUS,
                    TB.STEP1,
                    TB.DATE1,
                    TB.STEP2,
                    TB.DATE2,
                    TB.STEP3,
                    TB.DATE3,
                    TB.DOC_SENT_STATUS,
                    TB.MAT_FULLFILLMENT_STATUS AS MATERIAL_FULFILLMENT_STATUS,
                    TB.FULLFILLMENT_STATUS_DATE,
                    TB.AUFNR,
                    TB.MDR_STATUS,
                    TB.IS_ACTIVE
            FROM
                    TB_M_PMORDER TB
            LEFT JOIN TB_M_PMORDER TB2 ON
                    CAST(TB2.AUFNR AS BIGINT) = CAST(TB.JC_REFF AS BIGINT)
                    AND TB2.REVNR = TB.REVNR
                    AND TB2.AUART = 'GA01'
            LEFT JOIN TB_MRM MRM ON
                    MRM.AUFNR = TB.AUFNR
            WHERE
                    TB.REVNR = '$REVNR'
                    AND ( $AUFNR = 0
                    OR $AUFNR = TB.AUFNR )
                    AND ( '$JC_REF' = ''
                    OR TB.JC_REFF LIKE '%' + '$JC_REF' + '%' )
                    AND TB.AUART = 'GA02'
                    AND TB.IS_ACTIVE = 1
                    AND LEN(TB.RSPOS) <= 1
                    AND LEN(TB.APLZL_V) <= 1
                    AND LEN(TB.AUFPL) <= 1
                    AND ( ( '$FILTER' = ''
                    OR TB.AUFNR LIKE '%' + '$FILTER' + '%' )
                    OR ( '$FILTER' = ''
                    OR TB.JC_REFF LIKE '%' + '$FILTER' + '%' )
                    OR ( '$FILTER' = ''
                    OR CUST_JC_NUM LIKE '%' + '$FILTER' + '%' )
                    OR ( '$FILTER' = ''
                    OR TB.KTEXT LIKE '%' + '$FILTER' + '%' )
                    OR ( '$FILTER' = ''
                    OR TB.AREA LIKE '%' + '$FILTER' + '%' )
                    OR ( '$FILTER' = ''
                    OR TB.ERDAT LIKE '%' + '$FILTER' + '%' )
                    OR ( '$FILTER' = ''
                    OR TB.SKILL LIKE '%' + '$FILTER' + '%' )
                    OR ( '$FILTER' = ''
                    OR TB.ERNAM LIKE '%' + '$FILTER' + '%' )
                    OR ( '$FILTER' = ''
                    OR TB.DATE_PE LIKE '%' + '$FILTER' + '%' )
                    OR ( '$FILTER' = ''
                    OR TB.STATUS LIKE '%' + '$FILTER' + '%' )
                    OR ( '$FILTER' = ''
                    OR TB.MATSTATUS LIKE '%' + '$FILTER' + '%' )
                    OR ( '$FILTER' = ''
                    OR TB.STEP1 LIKE '%' + '$FILTER' + '%' )
                    OR ( '$FILTER' = ''
                    OR TB.DATE1 LIKE '%' + '$FILTER' + '%' )
                    OR ( '$FILTER' = ''
                    OR TB.STEP2 LIKE '%' + '$FILTER' + '%' )
                    OR ( '$FILTER' = ''
                    OR TB.DATE2 LIKE '%' + '$FILTER' + '%' )
                    OR ( '$FILTER' = ''
                    OR TB.STEP3 LIKE '%' + '$FILTER' + '%' )
                    OR ( '$FILTER' = ''
                    OR TB.DATE3 LIKE '%' + '$FILTER' + '%' )
                    OR ( '$FILTER' = ''
                    OR TB.MDR_STATUS LIKE '%' + '$FILTER' + '%' )
                    OR ( '$FILTER' = ''
                    OR TB.DATECLOSE LIKE '%' + '$FILTER' + '%' )
                    OR ( '$FILTER' = ''
                    OR TB.FULLFILLMENT_STATUS_DATE LIKE '%' + '$FILTER' + '%' )
                    OR ( '$FILTER' = ''
                    OR TB.MAT_FULLFILLMENT_STATUS LIKE '%' + '$FILTER' + '%' )
                    OR ( '$FILTER' = ''
                    OR TB.REMARK LIKE '%' + '$FILTER' + '%' )
                    OR ( '$FILTER' = ''
                    OR TB.DOC_SENT_STATUS LIKE '%' + '$FILTER' + '%' )
                    OR ( '$FILTER' = ''
                    OR TB.[FREETEXT] LIKE '%' + '$FILTER' + '%' ) ) 
                    ) SELECT
                    CAST(ROUND(EXT5, 0) AS NUMERIC(12,0)) AS SEQ_NUM,
                    ERDAT,
                    ERNAM,
                    JC_REF,
                    '<a href=\"" . $this->base_url() . "projects/crud_jobcard/' + REVNR + '/' + CAST(JC_REF AS VARCHAR) + '/jc\">' + CAST(JC_REF AS VARCHAR) + '</a>' AS JC_REF_LINK,
                    CUST_JC_NUM,
                    REVNR,
                    AREA,
                    '<a href=\"#\" onclick=\"longtextswal(' + AUFNR + ')\">' + KTEXT + '</a>' AS KTEXT_LINK,
                    KTEXT,
                    SKILL,
                    DATE_PE,
                    STATUS,
                    MATSTATUS,
                    REMARK,
                    DATECLOSE,
                    [FREETEXT],
                    DATEPROGRESS,
                    DAY,
                    CABINSTATUS,
                    STEP1,
                    DATE1,
                    STEP2,
                    DATE2,
                    STEP3,
                    DATE3,
                    DOC_SENT_STATUS,
                    MATERIAL_FULFILLMENT_STATUS,
                    FULLFILLMENT_STATUS_DATE,
                    AUFNR,
                    '<button class=\"btn btn-sm btn-danger delete\" data-id=\"' + AUFNR + '\" onclick=\"jc_delete(' + AUFNR + ')\"><i class=\"fa fa-trash-o\" aria-hidden=\"true\"></i> </button>' AS AUFNR_DEL,
                    '<a href=\"" . $this->base_url() . "projects/crud_mrm/' + REVNR + '/' + AUFNR + '/MDR\">' + AUFNR + '</a>' AS AUFNR_LINK,
                    MDR_STATUS,
                    IS_ACTIVE
            FROM
                    B
            ORDER BY
                    $ORDER
        ");
    }

    function get_order_count($REVNR) {
        $REVNR_ARR = implode("','", $REVNR);
        return DB::select("
            WITH A AS (
                SELECT
                        M_PMORDERH.REVNR,
                        M_PMORDERH.AUFNR,
                        M_PMORDERH.AUART,
                        TB_M_PMORDER.STATUS,
                        TB_M_PMORDER.MDR_STATUS
                FROM
                        dbo.M_PMORDERH 
                        LEFT JOIN dbo.TB_M_PMORDER ON TB_M_PMORDER.AUFNR = M_PMORDERH.AUFNR
                WHERE
                        (TB_M_PMORDER.IS_ACTIVE = 1 OR TB_M_PMORDER.IS_ACTIVE IS NULL)
                        AND M_PMORDERH.AUART IN ( 'GA01', 'GA02' )
                        AND M_PMORDERH.REVNR IN ('$REVNR_ARR') ),
                B AS (
                SELECT
                        REVNR,
                        COUNT(AUFNR) AS JC_TOTAL
                FROM
                        A
                WHERE
                        AUART = 'GA01'
                GROUP BY
                        REVNR ),
                C AS (
                SELECT
                        REVNR,
                        COUNT(AUFNR) AS JC_CLOSED
                FROM
                        A
                WHERE
                        AUART = 'GA01'
                        AND STATUS = 'CLOSED'
                GROUP BY
                        REVNR ),
                D AS (
                SELECT
                        REVNR,
                        COUNT(AUFNR) AS MDR_TOTAL
                FROM
                        A
                WHERE
                        AUART = 'GA02'
                GROUP BY
                        REVNR ),
                E AS (
                SELECT
                        REVNR,
                        COUNT(AUFNR) AS MDR_CLOSED
                FROM
                        A
                WHERE
                        AUART = 'GA02'
                        AND MDR_STATUS = 'CLOSED'
                GROUP BY
                        REVNR ) SELECT
                        B.REVNR,
                        COALESCE( B.JC_TOTAL,
                        0 ) AS JC_TOTAL,
                        COALESCE( C.JC_CLOSED,
                        0 ) AS JC_CLOSED,
                        COALESCE( D.MDR_TOTAL,
                        0 ) AS MDR_TOTAL,
                        COALESCE( E.MDR_CLOSED,
                        0 ) AS MDR_CLOSED
                FROM
                        B
                LEFT JOIN C ON
                        B.REVNR = C.REVNR
                LEFT JOIN D ON
                        B.REVNR = D.REVNR
                LEFT JOIN E ON
                        B.REVNR = E.REVNR
            ");
    }


    function get_mdr_by_area($REVNR){
        return DB::select("
            WITH [TOTAL] AS (
                SELECT
                        AREA,
                        COUNT( AREA ) AS TOTAL 
                FROM
                        TB_M_PMORDER 
                WHERE
                        REVNR = $REVNR
                        AND AUART = 'GA02'
                        AND IS_ACTIVE = 1
                        AND AREA IS NOT NULL
                        GROUP BY AREA
                ),
                
                [CLOSED] AS (
                        SELECT
                                AREA,
                                COUNT( AREA ) AS CLOSED 
                        FROM
                                TB_M_PMORDER 
                        WHERE
                                REVNR = $REVNR
                                AND AUART = 'GA02'
                                AND IS_ACTIVE = 1
                                AND AREA IS NOT NULL
                                AND MDR_STATUS = 'CLOSED'
                                GROUP BY AREA
                ),
                
                [OPEN] AS (
                        SELECT
                                AREA,
                                COUNT( AREA ) AS [OPEN] 
                        FROM
                                TB_M_PMORDER 
                        WHERE
                                REVNR = $REVNR
                                AND AUART = 'GA02'
                                AND IS_ACTIVE = 1
                                AND AREA IS NOT NULL
                                AND MDR_STATUS <> 'CLOSED'
                                GROUP BY AREA
                )	
                        
                        SELECT 
                                [TOTAL].AREA, 
                                COALESCE([TOTAL].TOTAL, 0) AS [TOTAL], 
                                COALESCE([CLOSED].CLOSED, 0) AS [CLOSED],
                                COALESCE([OPEN].[OPEN], 0) AS [OPEN]
                                FROM 
                        [TOTAL]
                        LEFT JOIN [CLOSED] ON CLOSED.AREA = [TOTAL].AREA
                        LEFT JOIN [OPEN] ON [OPEN].AREA = [TOTAL].AREA
            ");
    }

    function get_daily_menu($REVNR, $DAY = NULL, $AREA = NULL) {
        $where = "";

        if(!empty($DAY) && !empty($AREA)){
            $where = "WHERE DAY = " . $DAY;
            $where .= " AND AREA = '" . $AREA . "'"; 

        } elseif (!empty($DAY)) {
            $where = "WHERE DAY = " . $DAY;
        } elseif (!empty($AREA)) {
            $where = "WHERE AREA = '" . $AREA . "'";
        }



        return DB::select("
            WITH Z AS (
            SELECT DISTINCT
                AUFNR,
                REVNR,
                KTEXT,
                ILART,
                ERNAM,
                ERDAT,
                PHAS0,
                PHAS1,
                PHAS2,
                PHAS3,
            
                CASE 
                    WHEN AUART LIKE 'GA01' 
                    THEN 'JC'
                    ELSE 	'MDR'
                END AS CARD_TYPE,	
                CASE	
                    WHEN PHAS3 LIKE 'X' OR PHAS2 LIKE 'X' 
                        THEN'CLOSED' 
                    WHEN PHAS1 LIKE 'X' 
                        THEN 'PROGRESS' 
                    WHEN PHAS0 LIKE 'X' 
                        THEN 'OPEN' 
                END AS STATUS_SAP,
                SUBSTRING(
                    KTEXT,
                    CHARINDEX ( ']', KTEXT ) + 1,
                    ABS( CHARINDEX ( ':', KTEXT ) - CHARINDEX ( ']', KTEXT ) - 1 ) 
                ) AS CUST_JC_NUM,
                SUBSTRING(
                    KTEXT,
                    CHARINDEX ( '[', KTEXT ) + 1,
                    ABS( CHARINDEX ( ']', KTEXT ) - CHARINDEX ( '[', KTEXT ) - 1 ) 
                ) AS ITVAL 
            FROM
                M_PMORDERH 
            WHERE
                REVNR = $REVNR
                
                ),
                P AS ( SELECT DISTINCT 
                    TB_M_PMORDER.AUFNR, 
                    TB_M_PMORDER.SKILL AS SKILL,
                    TB_M_PMORDER.PHASE,
                    TB_M_PMORDER.STATUS AS STATUS,
                    TB_M_PMORDER.AREA,
                    TB_M_PMORDER.DAY,
                    TB_M_PMORDER.DATECLOSE,
                    TB_M_PMORDER.DATEPROGRESS,
                    TB_M_PMORDER.REMARK,
                    TB_M_PMORDER.[FREETEXT],
                    TB_M_PMORDER.CABINSTATUS,
                    TB_M_PMORDER.MAT_FULLFILLMENT_STATUS AS MAT_FULLFILLMENT_STATUS 
                    FROM TB_M_PMORDER WHERE TB_M_PMORDER.IS_ACTIVE = 1),
                X AS (
                SELECT
                    Z.*,
                    P.SKILL AS SKILL,
                    P.PHASE,
                    P.AREA,
                    P.DAY,
                    P.DATECLOSE,
                    P.DATEPROGRESS,
                    P.STATUS AS STATUS,
                    P.REMARK,
                    P.CABINSTATUS,
                    P.[FREETEXT],
                    P.MAT_FULLFILLMENT_STATUS AS MAT_FULLFILLMENT_STATUS
                FROM
                    Z
                    LEFT JOIN P ON Z.AUFNR = P.AUFNR 
                ),
                W AS ( SELECT DISTINCT AUFNR, VORNR, ARBEI FROM M_PMORDER ),
                L AS ( SELECT AUFNR, SUM ( CAST ( REPLACE( ARBEI, ',', '' ) AS FLOAT )) AS 'MHRS' FROM W GROUP BY AUFNR ),
                Y AS ( SELECT X.*, L.MHRS FROM X INNER JOIN L ON X.AUFNR = L.AUFNR ) 
            SELECT
                ROW_NUMBER() OVER (ORDER BY AREA, AUFNR) AS SEQ,
                *
            FROM
                Y
                $where
                ORDER BY AREA, AUFNR ASC        
        ");
    }

    function get_last_id_($column, $table, $revnr){
        return DB::select("SELECT COALESCE(MAX( $column ), 0)  FROM $table WHERE REVNR = $revnr");
    }


    


}
