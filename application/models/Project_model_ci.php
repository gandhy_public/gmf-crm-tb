<?php

Class Project_model_ci extends CI_Model {

    var $V_PROJECT = 'M_REVISION';
    var $M_PMORDER = 'TB_M_PMORDER';
    var $V_CUSTOMER = 'TB_V_CUSTOMER';
    var $V_JOBCARD_PROGRESS = 'TB_V_JOBCARD_PROGRESS';
    var $V_MDR_PROGRESS = 'TB_V_MDR_PROGRESS';
    var $V_MRM = 'TB_V_MRM';
    

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    function executeCSP() {
        $sql = "EXECUTE m_csrInsert";
        return $this->db->query($sql)->result();
    }

    //To Get Project List
    function getProjectList() {
        $sql = "SELECT DISTINCT area FROM " . $this->V_PROJECT;
        return $this->db->query($sql)->result();
    }

    //To Get Project Name
    function getProjectName($REVNR) {
        $query = $this->db->select("*")
                ->from($this->V_PROJECT)
                ->where("REVNR", $REVNR)
                ->get();
        return $query->row();
    }

    //To Get AUFNR JOBCARD
    function getItemJobCard($SEQ, $REVNR) {
        $query = $this->db->select("*")
                ->from($this->V_JOBCARD_PROGRESS)
                ->where("SEQ_NUM", $SEQ)
                ->where("REVNR", $REVNR)
                ->get();
        return $query->row();
    }

    //To Get AUFNR MDR
    function getItemJobMDR($SEQ, $REVNR) {
        $query = $this->db->select("*")
                ->from($this->V_MDR_PROGRESS)
                ->where("SEQ_NUM", $SEQ)
                ->where("REVNR", $REVNR)
                ->get();
        return $query->row();
    }

    //To Get AUFNR MRM
    function getItemJobMRM($SEQ, $REVNR) {
        $query = $this->db->select("*")
                ->from($this->V_MRM)
                ->where("SEQ_NUM", $SEQ)
                ->where("REVNR", $REVNR)
                ->get();
        return $query->row();
    }

    //To Get Customer List
    function getCustomerList() {
        $sql = "SELECT * FROM " . $this->V_CUSTOMER;
        return $this->db->query($sql)->result();
    }

    //To Get Data Jobcard refer to Project Number
    function getJobcard($revnr) {
        $sql = "SELECT * FROM " . $this->V_M_PMORDER . " WHERE REVNR = '$revnr' AND AUART = 'GA01' ORDER BY AUFNR ASC";
        return $this->db->query($sql)->result();
    }

    function getProjectById($revnr) {
        $sql = "SELECT * FROM " . $this->M_PMORDER . " where REVNR = " . $revnr;
        return $this->db->query($sql)->result();
    }

}

?>