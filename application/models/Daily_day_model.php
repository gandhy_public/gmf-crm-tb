<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use Illuminate\Database\Eloquent\Model as Eloquent;
use Illuminate\Database\Capsule\Manager as DB;

class Daily_day_model extends Eloquent {

    protected $table = 'TB_DAILY_DAY';
    public $timestamps = false;
    protected $guarded = array();
    // public $primaryKey  = 'REVNR';


}
