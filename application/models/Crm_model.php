<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use Illuminate\Database\Eloquent\Model as Eloquent;

class Crm_model extends Eloquent {

    protected $table = 'TB_CRM';
    public $timestamps = false;
    protected $guarded = array();

}
