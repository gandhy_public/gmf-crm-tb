<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use Illuminate\Database\Eloquent\Model as Eloquent;

class Jobcard_model extends Eloquent {

    protected $table = 'TB_M_PMORDER';
    // protected $table = 'M_PMORDER';
    public $timestamps = false;
     protected $guarded = array();

    function get_jobcard_count_by_status($REVNR) {
        $open = $this->selectRaw("'OPEN' AS STATUS, count(DISTINCT AUFNR) as TOTAL")
                ->where('REVNR', $REVNR)
                ->where('STATUS', 'LIKE', 'OPEN')
                ->where('AUART', 'GA01');

        $close = $this->selectRaw("'CLOSED' AS STATUS, count(DISTINCT AUFNR) as TOTAL")
                ->where('REVNR', $REVNR)
                ->where('STATUS', 'LIKE', 'CLOSED')
                ->where('AUART', 'GA01');

        $progress = $this->selectRaw("'PROGRESS' AS STATUS, count(DISTINCT AUFNR) as TOTAL")
                ->where('REVNR', $REVNR)
                ->where(function($query) {
            $query->whereNotIn('STATUS', ['OPEN', 'CLOSED'])
            ->where('AUART', 'GA01');
        });

        $status_count = $this->selectRaw("'TOTAL' AS STATUS, count(DISTINCT AUFNR) as TOTAL")
                ->where('REVNR', $REVNR)
                ->where('AUART', 'GA01')
                ->union($open)
                ->union($close)
                ->union($progress)
                ->orderBy('STATUS')
                ->get();
        return $status_count;
    }

    function get_closed_jobcard($REVNR){
        $close = $this->selectRaw("'CLOSED' AS STATUS, count(DISTINCT AUFNR) as CLOSED")
                ->where('REVNR', $REVNR)
                ->where('STATUS', 'LIKE', 'CLOSED')
                ->where('AUART', 'GA01')
                ->get();           
        return $close;        
    }



    

}
