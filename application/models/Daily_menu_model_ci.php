<?php

Class Daily_menu_model_ci extends CI_Model {
	
    var $V_DAILY_MENU = 'V_DAILY_MENU';
	var $M_PMORDER = 'TB_M_PMORDER';

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	function getAreaList($id_project) {
		$sql = "SELECT DISTINCT AREA FROM ". $this->M_PMORDER ." WHERE REVNR = " . $id_project . " AND AREA IS NOT NULL" ;
        return $this->db->query($sql)->result();
    }

	function getDayList($id_project) {
		$sql = "SELECT DISTINCT day FROM " . $this->M_PMORDER . " WHERE day IS NOT NULL AND REVNR = " .$id_project ;
        return $this->db->query($sql)->result();
    }

    function getSumMhrs($id_project, $area = null, $day = null){
        // $sql = "SELECT SUM(CAST(mhrs AS DECIMAL(4,2))) as total FROM " . $this->M_PMORDER . " WHERE REVNR = " .$id_project ;
    	$sql = "SELECT AUFNR, SUM( CAST( ARBEI AS FLOAT ) ) AS 'MHRS' FROM " . $this->M_PMORDER . " GROUP BY AUFNR";

    	if($area != null){
    		$sql .= "AND area like '%". $area ."%'";
    		echo $area;
    	}

    	if($day != null) {
    		$sql .= "AND day like '%". $day ."%'";
    	}

        return $this->db->query($sql)->row();
    }
}

?>