<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use Illuminate\Database\Eloquent\Model as Eloquent;

class Revh_model extends Eloquent {

    protected $table = 'TB_M_REVH';
    public $timestamps = false;
    protected $guarded = array();
    protected $fillable = array();
    public $primaryKey  = 'REVNR';

}
