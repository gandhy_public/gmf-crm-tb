<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use Illuminate\Database\Eloquent\Model as Eloquent;

class Mdr_model extends Eloquent {

    protected $table = 'TB_M_PMORDER';
    public $timestamps = false;

    function get_mdr_count_by_status($REVNR){
    	$open = $this->selectRaw("'OPEN' AS STATUS, count(DISTINCT AUFNR) as TOTAL")
    			->where('REVNR', $REVNR)
    			->where('MDR_STATUS', 'LIKE', 'OPEN')
    			->where('AUART', 'GA02');

    	$close = $this->selectRaw("'CLOSED' AS STATUS, count(DISTINCT AUFNR) as TOTAL")
			->where('REVNR', $REVNR)
			->where('MDR_STATUS', 'LIKE', 'CLOSED')
			->where('AUART', 'GA02');

    	$progress = $this->selectRaw("'PROGRESS' AS STATUS, count(DISTINCT AUFNR) as TOTAL")
			->where('REVNR', $REVNR)
			->where('AUART', 'GA02')
			->where(function($query){
					$query->whereNotIn('MDR_STATUS', ['OPEN', 'CLOSED'])
						->orWhereNull('STATUS');
			});
			

	$status_count = $this->selectRaw("'TOTAL' AS STATUS, count(DISTINCT AUFNR) as TOTAL")
			->where('REVNR', $REVNR)
			->where('AUART', 'GA02')
			->union($open)
			->union($close)
			->union($progress)
			->get();			 			    			
    	return $status_count;
    }

}
