<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use Illuminate\Database\Eloquent\Model as Eloquent;

class Project_model extends Eloquent {

    protected $table = 'M_REVISION';
    public $timestamps = false;

    function get_projects($page, $perPage, $status, $location, $cust) {
        $data = NULL;

        $project  =  $this->select(
                'REVTX', 
                'M_REVISION.REVNR', 
                'M_REVISION.TPLNR', 
                'REVTY', 
                'REVBD', 
                'REVED', 
                'VAWRK', 
                'TXT04',
                'V_Customer.COMPANY_NAME'
            )
            ->leftJoin('TB_M_PMORDER', 'M_REVISION.AUFNR', '=', 'TB_M_PMORDER.AUFNR')
            ->leftJoin('V_Customer', 'V_Customer.ID_CUSTOMER', '=', 'M_REVISION.PARNR_TPLNR');

        if(!empty($cust)){
            $project->where('M_REVISION.PARNR_TPLNR', $cust);
        }

        if(!empty($status)){
            $project->where('TXT04', 'like', '%' .$status. '%' );
        }
        
        if(!empty($location)){
            $project->where('VAWRK', $location );
        } else {
            $project->whereIn('VAWRK', ['GAH1', 'GAH3', 'GAH4']);
        }

        $data["total_count"] = $project->count();

        $data["result"] = $project->orderBy('M_REVISION.REVED', 'DESC')
                ->forPage($page, $perPage)->get();


        return $data;    
    }

    function get_projects_with_filter($page, $perPage, $status, $location, $filter, $cust) {
        $data = NULL;
        $project = $this->select(
                'REVTX', 
                'M_REVISION.REVNR', 
                'M_REVISION.TPLNR', 
                'REVTY', 
                'REVBD', 
                'REVED', 
                'VAWRK', 
                'TXT04',
                'V_Customer.COMPANY_NAME'
        )
            ->leftJoin('TB_M_PMORDER', 'M_REVISION.AUFNR', '=', 'TB_M_PMORDER.AUFNR')
            ->leftJoin('V_Customer', 'V_Customer.ID_CUSTOMER', '=', 'M_REVISION.PARNR_TPLNR')
            ->whereIn('VAWRK', ['GAH1', 'GAH3', 'GAH4'])
            ->where(function($query) use ($filter) {
                $query->where('REVTX', 'like', '%'.$filter.'%');
                $query->orWhere('M_REVISION.REVNR', 'like', '%'.$filter.'%');
                $query->orWhere('M_REVISION.TPLNR', 'like', '%'.$filter.'%');
                $query->orWhere('V_Customer.COMPANY_NAME', 'like', '%'.$filter.'%');
                $query->orWhere('REVTY', 'like', '%'.$filter.'%');
                $query->orWhere('REVBD', 'like', '%'.$filter.'%');
                $query->orWhere('REVED', 'like', '%'.$filter.'%');
                $query->orWhere('VAWRK', 'like', '%'.$filter.'%');
                $query->orWhere('TXT04', 'like', '%'.$filter.'%');
            });

        if(!empty($cust)){
            $project->where('M_REVISION.PARNR_TPLNR', $cust);
        }

        if(!empty($status)){
            $project->where('TXT04', 'like', '%' .$status. '%' );
        }
        
        if(!empty($location)){
            $project->where('VAWRK', $location );
        } else {
            $project->whereIn('VAWRK', ['GAH1', 'GAH3', 'GAH4']);
        }

        $data["total_count"] = $project->count();

        $data["result"] = $project->orderBy('M_REVISION.REVED', 'DESC')
                ->forPage($page, $perPage)->get();

        return $data;
    }

    function get_project_list($REVNR){
        $project = $this->where('REVNR', $REVNR)->get();
        return $project;
    }

    function get_project_detail($REVNR) {

        $project = $this->select('*')
            ->leftJoin('V_Customer', 'V_Customer.ID_CUSTOMER', '=', 'M_REVISION.PARNR_TPLNR')
            ->where('M_REVISION.REVNR', $REVNR);
        return $project->first();
    }

    function get_projects_by_id($REVNR){
        $project = $this->where('REVNR', $REVNR)->first();
        return $project;
    }

    function get_workcenter(){
    	$project = $this->selectRaw('VAWRK, COUNT(VAWRK) AS AC_COUNT')
    			->whereIn('VAWRK', ['GAH1', 'GAH3', 'GAH4'])
    			->where('TXT04', 'REL')
    			->groupBy('VAWRK')
    			->get();
    	return $project;
    }

    function get_project_by_location($location){
        $project = $this->selectRaw('M_REVISION.*, TB_M_REVH.HILITE, TB_M_REVH.IN_HANGAR')
                ->leftJoin('TB_M_REVH', 'TB_M_REVH.REVNR', 'M_REVISION.REVNR')
                ->where('VAWRK', $location)
                ->where('TXT04', 'REL')
                ->orderBy('VAWRK', 'ASC')
                ->orderBy('REVBD', 'DESC')
                ->get();
        return $project;                
    }

    function get_order_count($REVNR) {
        $close = $this->selectRaw("
                    M_REVISION.REVNR, 
                    ( SELECT COUNT( DISTINCT AUFNR ) FROM TB_M_PMORDER WHERE IS_ACTIVE = 1 AND REVNR = M_REVISION.REVNR ) AS TOTAL   
                ")
                ->whereIn('REVNR', $REVNR)
                ->groupBy('REVNR')
                ->orderBy('REVNR')
                ->get();               
        return $close;  
    }

    function get_order_close_count($REVNR) {
        $close = $this->selectRaw("
                    M_REVISION.REVNR, 
                    ( SELECT COUNT( DISTINCT AUFNR ) FROM TB_M_PMORDER WHERE IS_ACTIVE = 1 AND STATUS='CLOSED'  AND REVNR = M_REVISION.REVNR ) AS TOTAL   
                ")
                ->whereIn('REVNR', $REVNR)
                ->groupBy('REVNR')
                ->orderBy('REVNR')
                ->get();               
        return $close;  
    }

    function get_jc_progress($REVNR) {
        $close = $this->selectRaw("
                    M_REVISION.REVNR, 
                    ( SELECT COUNT( DISTINCT AUFNR ) FROM TB_M_PMORDER WHERE IS_ACTIVE = 1 AND REVNR = M_REVISION.REVNR  AND AUART = 'GA01') AS JC_TOTAL,
                    ( SELECT COUNT( DISTINCT AUFNR ) FROM TB_M_PMORDER WHERE IS_ACTIVE = 1 AND REVNR = M_REVISION.REVNR AND STATUS = 'CLOSED' AND AUART = 'GA01') AS JC_CLOSED,
                    ( SELECT COUNT( DISTINCT AUFNR ) FROM TB_M_PMORDER WHERE IS_ACTIVE = 1 AND REVNR = M_REVISION.REVNR  AND AUART = 'GA02') AS MDR_TOTAL,
                    ( SELECT COUNT( DISTINCT AUFNR ) FROM TB_M_PMORDER WHERE IS_ACTIVE = 1 AND REVNR = M_REVISION.REVNR AND MDR_STATUS = 'CLOSED' AND AUART = 'GA02') AS MDR_CLOSED   
                ")
                ->whereIn('REVNR', $REVNR)
                ->groupBy('REVNR')
                ->orderBy('REVNR')
                ->get();               
        return $close;  
    }

    function get_mhrs($REVNR) {
        $mhrs = $this->selectRaw("
                TB_M_PMORDER.REVNR,
                TB_M_PMORDER.AUFNR,
                TB_M_PMORDER.VORNR,
                CAST(REPLACE(TB_M_PMORDER.ARBEI, ',', '') AS FLOAT) AS 'PLN',
                CAST(REPLACE(TB_M_PMORDER.ISMNW, ',', '') AS FLOAT) AS 'ACTUAL'")
            ->distinct("
                TB_M_PMORDER.REVNR,
                TB_M_PMORDER.AUFNR,
                TB_M_PMORDER.VORNR,
                CAST(REPLACE(TB_M_PMORDER.ARBEI, ',', '') AS FLOAT) AS 'PLN',
                CAST(REPLACE(TB_M_PMORDER.ISMNW, ',', '') AS FLOAT) AS 'ACTUAL'")
            ->leftJoin('TB_M_PMORDER', function($leftJoin){
                    $leftJoin->on('TB_M_PMORDER.REVNR', '=', 'M_REVISION.REVNR')
                    ->where('IS_ACTIVE', 1)
                    ->where('AUART', 'GA01');
            })
            ->whereIn('M_REVISION.REVNR', $REVNR)
            ->get();
            return $mhrs;
    }


}
