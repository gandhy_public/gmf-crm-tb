<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use Illuminate\Database\Eloquent\Model as Eloquent;

class Prm_model extends Eloquent {

    protected $table = 'TB_PRM';
    public $timestamps = false;
    protected $guarded = array();

}
