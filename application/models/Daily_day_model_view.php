<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use Illuminate\Database\Eloquent\Model as Eloquent;

class Daily_day_model_view extends Eloquent {
	
    protected $table = 'V_LIST_DAILY_DAY';
    public $timestamps = false;

}
