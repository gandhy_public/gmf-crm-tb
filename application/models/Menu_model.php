<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use Illuminate\Database\Eloquent\Model as Eloquent;

class Menu_model extends Eloquent {

    protected $table = 'TB_MENU';
    public $timestamps = false;

}