<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use Illuminate\Database\Eloquent\Model as Eloquent;
use Illuminate\Database\Capsule\Manager as DB;

class Board_model extends Eloquent {

    function get_jc_phase($REVNR) {
        return DB::select("
                SELECT
                        P.PHASE AS 'LABELS',
                        COUNT( DISTINCT AUFNR ) AS QTY 
                FROM TB_M_PMORDER ORD
                        LEFT JOIN TB_M_PHASE P ON  P.ID = ORD.PHASE
                        WHERE REVNR = '$REVNR'
                        AND AUART LIKE 'GA01'
                        AND IS_ACTIVE = '1'
                        AND ORD.PHASE IS NOT NULL
                        GROUP BY P.PHASE
	
        ");
    }

    function get_mat_qty($REVNR) {
        return DB::select("
                SELECT
                        MATERIAL_FULFILLMENT_STATUS AS LABELS,
                        COUNT ( MATERIAL_FULFILLMENT_STATUS ) AS QTY 
                FROM
                        TB_MRM 
                WHERE
                        REVNR = '$REVNR' 
                        AND MATERIAL_FULFILLMENT_STATUS IS NOT NULL 
                        AND IS_ACTIVE = '1' 
                GROUP BY
                        MATERIAL_FULFILLMENT_STATUS
        ");
    }

    function get_jc_area($REVNR) {
        return DB::select("
                SELECT
                        AREA AS 'LABELS',
                        COUNT( DISTINCT TB_M_PMORDER.AUFNR ) AS 'QTY'
                FROM
                        dbo.TB_M_PMORDER 
                WHERE
                        dbo.TB_M_PMORDER.AUART = 'GA01' 
                        AND dbo.TB_M_PMORDER.IS_ACTIVE = '1' 
                        AND dbo.TB_M_PMORDER.REVNR = '$REVNR'
                        AND AREA IS NOT NULL
                        GROUP BY AREA        
        
        ");
    }

    function get_mdr_compare($REVNR) {
        return DB::select("
            WITH A AS ( SELECT
                    *
            FROM
                    (
            VALUES ( 'FUSELAGE' ), ( 'COCKPIT' ), ( 'LH-WING' ), ( 'RH-WING' ), ( 'L/G' ), ( 'ENG#1' ), ( 'ENG#2' ), ( 'ENG#3' ), ( 'ENG#4' ), ( 'TAIL' ), ( 'CABIN' ), ( 'FWD CARGO' ), ( 'AFT CARGO' ), ( 'BULK CARGO' ), ( 'MAIN CARGO' ), ( 'LOW CARGO' ), ( 'ELECT' ), ( 'GENERAL AREA' )) AS AREA( AREA )),
            B AS ( SELECT
                    A.AREA, ( SELECT
                            COUNT( DISTINCT MDRO.AUFNR )
                    FROM
                            dbo.TB_M_PMORDER MDRO
                    WHERE
                            MDRO.AREA = A.AREA
                            AND MDRO.AUART = 'GA02'
                            AND MDRO.IS_ACTIVE = '1'
                            AND MDRO.REVNR = '$REVNR'
                            AND ( MDRO.MDR_STATUS = 'OPEN'
                            OR MDRO.MDR_STATUS = 'PROGRESS' )) AS MDR_OPEN, ( SELECT
                            COUNT( DISTINCT MDRO.AUFNR )
                    FROM
                            dbo.TB_M_PMORDER MDRO
                    WHERE
                            MDRO.AREA = A.AREA
                            AND MDRO.AUART = 'GA02'
                            AND MDRO.IS_ACTIVE = '1'
                            AND MDRO.REVNR = '$REVNR'
                            AND MDRO.MDR_STATUS = 'CLOSED' ) AS MDR_CLOSE
            FROM
                    A ) SELECT
                    A.AREA AS LABELS,
                    COALESCE(( SELECT
                            SUM( B.MDR_OPEN )
                    FROM
                            B
                    WHERE
                            B.AREA = A.AREA ), 0 ) AS QTY_OPEN,
                    COALESCE(( SELECT
                            SUM( B.MDR_CLOSE )
                    FROM
                            B
                    WHERE
                            B.AREA = A.AREA ), 0 ) AS QTY_CLOSE
            FROM
                    A
        ");
    }

    function get_jc_skill($REVNR) {
        return DB::select("
            WITH A AS ( SELECT
                    *
            FROM
                    (
            VALUES('A/P'), ('CBN'), ('E/A'), ('STR'), ('WS')) AS LABELS(LABELS)) SELECT
                    A.LABELS,
                    ( SELECT
                            COUNT( DISTINCT TB_M_PMORDER.AUFNR )
                    FROM
                            dbo.TB_M_PMORDER
                    WHERE
                            dbo.TB_M_PMORDER.SKILL = A.LABELS
                            AND dbo.TB_M_PMORDER.STATUS <> 'CLOSED'
                            AND dbo.TB_M_PMORDER.AUART = 'GA01'
                            AND dbo.TB_M_PMORDER.IS_ACTIVE = '1'
                            AND dbo.TB_M_PMORDER.REVNR = '$REVNR' ) AS QTY_OPEN,
                    ( SELECT
                            COUNT( DISTINCT TB_M_PMORDER.AUFNR )
                    FROM
                            dbo.TB_M_PMORDER
                    WHERE
                            dbo.TB_M_PMORDER.SKILL = A.LABELS
                            AND dbo.TB_M_PMORDER.STATUS = 'CLOSE%'
                            AND dbo.TB_M_PMORDER.AUART = 'GA01'
                            AND dbo.TB_M_PMORDER.IS_ACTIVE = '1'
                            AND dbo.TB_M_PMORDER.REVNR = '$REVNR' ) AS QTY_CLOSE
            FROM
                    A
        ");
    }

    function get_mdr_shop($REVNR) {
        return DB::select("
                SELECT 
                        CONCAT('(' ,COUNT ( DISTINCT AUFNR ), ') ', DOC_SENT_STATUS) AS LABELS,
                        COUNT(DOC_SENT_STATUS) AS TOTAL
                FROM TB_M_PMORDER 
                        WHERE REVNR = $REVNR
                        AND IS_ACTIVE = 1
                        AND AUART = 'GA02'
                        AND DOC_SENT_STATUS IS NOT NULL
                        GROUP BY DOC_SENT_STATUS

        ");
    }

    function get_mdr_acc_status($REVNR) {
        return DB::select("
                SELECT
                    CONCAT('(' ,COUNT ( DISTINCT AUFNR ), ') ', STATUS) AS LABELS,
                    COUNT(DISTINCT AUFNR) AS QTY
                FROM
                TB_M_PMORDER 
                    WHERE REVNR = '$REVNR'
                    AND AUART = 'GA02'
                    AND IS_ACTIVE = 1
                    AND STATUS IS NOT NULL
                    GROUP BY STATUS

            ");
    }

}
