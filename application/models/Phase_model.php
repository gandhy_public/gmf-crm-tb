<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use Illuminate\Database\Eloquent\Model as Eloquent;
use Illuminate\Database\Capsule\Manager as DB;

class Phase_model extends Eloquent {

    protected $table = 'TB_M_PHASE';
    public $timestamps = false;

    function get_jobcard_phase($REVNR) {
        $close = $this->selectRaw("TB_M_PHASE.PHASE, COUNT(DISTINCT TB_M_PMORDER.AUFNR) AS COUNT")
                ->leftJoin("TB_M_PMORDER", function($join) {
                    $join->on('TB_M_PHASE.ID', '=', 'TB_M_PMORDER.PHASE')
                    ->where('TB_M_PMORDER.AUART', 'GA01');
                })
                ->where('TB_M_PMORDER.REVNR', $REVNR)
                ->groupBy('TB_M_PHASE.PHASE')
                ->get();
        return $close;
    }

}
