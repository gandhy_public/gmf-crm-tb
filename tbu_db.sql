-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 01, 2018 at 04:31 PM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 7.1.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tbu_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `area`
--

CREATE TABLE `area` (
  `ID_AREA` int(10) NOT NULL,
  `AREA` varchar(50) NOT NULL,
  `PHASE` varchar(50) NOT NULL,
  `UID` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `area`
--

INSERT INTO `area` (`ID_AREA`, `AREA`, `PHASE`, `UID`) VALUES
(1, 'Hangar', 'Removal', 0),
(2, 'Hangar', 'Inspection', 0),
(3, 'Hangar', 'Rectification', 0),
(4, 'Hangar', 'Installation', 0),
(5, 'Hangar', 'RTS', 0),
(6, 'WSSS', 'Rectification', 0),
(7, 'WSSE', 'Rectification', 0),
(8, 'WSCB', 'Rectification', 0),
(9, 'WSLS & WSSW', 'Rectification', 0);

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE `customer` (
  `ID_CUSTOMER` int(11) NOT NULL,
  `COMPANY_NAME` varchar(100) NOT NULL,
  `COMPANY_EMAIL` varchar(100) NOT NULL,
  `COMPANY_ADDRESS` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`ID_CUSTOMER`, `COMPANY_NAME`, `COMPANY_EMAIL`, `COMPANY_ADDRESS`) VALUES
(2, 'PT AirSwiss', 'airasia@gmail.com', 'sdsdsds'),
(3, 'PT Lion', 'lion@gmail.com', '<p>\r\n	ssdsdsd</p>\r\n'),
(4, 'PT OD', 'od@gmail.com', '<p>\r\n	oshagsas</p>\r\n'),
(5, 'PT AirGo', 'airgo@go.co.id', '<p>\r\n	sdskjoffg</p>\r\n'),
(19, '', '', ''),
(20, '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `daily_day`
--

CREATE TABLE `daily_day` (
  `Id` int(11) NOT NULL,
  `create_date_daily` date NOT NULL,
  `area` varchar(255) NOT NULL,
  `task_progress` longtext,
  `followup_result` longtext,
  `remarks` longtext,
  `update_date_daily` datetime DEFAULT NULL,
  `createdBy_daily` varchar(255) DEFAULT NULL,
  `updateBy_daily` varchar(255) DEFAULT NULL,
  `id_project` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `daily_day`
--

INSERT INTO `daily_day` (`Id`, `create_date_daily`, `area`, `task_progress`, `followup_result`, `remarks`, `update_date_daily`, `createdBy_daily`, `updateBy_daily`, `id_project`) VALUES
(43, '2018-02-08', 'ENGINE', '<p>\r\n	<span style=\"color: rgb(0, 0, 0); font-family: arial; font-size: 13px; white-space: pre-wrap; background-color: rgb(255, 0, 0);\">1.REF MDR 802116679 : ENG #2 HIGH OIL CONSUMPTION</span></p>\r\n<p>\r\n	<span style=\"background-color: rgb(255, 0, 0); color: rgb(0, 0, 0); font-family: arial; font-size: 13px; white-space: pre-wrap;\">2. ENG #3 REPLACEMENT</span></p>\r\n', '<p>\r\n	<span style=\"color: rgb(0, 0, 0); font-family: Arial; font-size: 13px; font-style: italic; white-space: pre-wrap; background-color: rgb(255, 0, 0);\">REPLACE GASKET ON &#39;C&#39; SUMP OIL SUPPLY TUBE, AND RADIAL STATIC OIL SEAL ON AFT BEARING</span></p>\r\n<p>\r\n	<span style=\"background-color: rgb(255, 0, 0); color: rgb(0, 0, 0); font-family: Arial; font-size: 13px; font-style: italic; white-space: pre-wrap;\">SHIPMENT, AWB 297-66633744</span></p>\r\n<p>\r\n	<span style=\"background-color: rgb(255, 255, 0); color: rgb(0, 0, 0); font-family: Arial; font-size: 13px; font-style: italic; white-space: pre-wrap;\">REMOVE DONE, CONTINUE SENT TO SHOP FOR REPAIR SCHEMA</span></p>\r\n', '<p>\r\n	<span style=\"color: rgb(0, 0, 0); font-family: Arial; font-size: 13px; text-align: center; white-space: pre-wrap; background-color: rgb(255, 0, 0);\">ETA 6 FEB</span></p>\r\n', '2018-02-26 18:09:04', 'admin', '1', 4),
(46, '2018-02-08', 'WING & FLT CONTROL', '', 'jdbdfndfj', '2/01/2018', '0000-00-00 00:00:00', 'admin', '', 4),
(47, '2018-02-08', 'FWD & AFT CARGO', '<p>\r\n	SASA</p>\r\n', NULL, '<p>\r\n	DSDSDS</p>\r\n', '2018-02-26 18:16:40', NULL, '1', 4),
(48, '2018-02-09', 'WING & FLT CONTROL', '', NULL, '<p>\r\n	DSD</p>\r\n', '2018-02-26 18:16:48', NULL, '1', 4);

-- --------------------------------------------------------

--
-- Table structure for table `daily_report`
--

CREATE TABLE `daily_report` (
  `ID` int(10) NOT NULL,
  `ORDER_NO` varchar(50) NOT NULL,
  `PLAN_TARGET_TEXT` varchar(200) NOT NULL,
  `HIGHLIGHT_TEXT` varchar(200) NOT NULL,
  `CREATED_AT` datetime NOT NULL,
  `USER` varchar(50) NOT NULL,
  `DELETED` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `daily_report`
--

INSERT INTO `daily_report` (`ID`, `ORDER_NO`, `PLAN_TARGET_TEXT`, `HIGHLIGHT_TEXT`, `CREATED_AT`, `USER`, `DELETED`) VALUES
(96, '000800028308', 'Repair', '-', '2018-01-26 03:00:02', 'admin', 0),
(97, '000800028311', 'Repair', '-', '2018-01-26 03:00:02', 'admin', 0),
(98, '000800028315', 'Repair', '-', '2018-01-26 03:00:02', 'admin', 0),
(99, '000800028330', 'Repair', 'Oke', '2018-01-26 03:09:53', 'admin', 0),
(100, '000800028331', 'Repair', 'Oke', '2018-01-26 03:09:53', 'admin', 0),
(101, '000800028449', 'Test', 'q', '2018-01-26 03:19:49', 'admin', 0),
(102, '000800028651', 'Repair', '1', '2018-01-26 03:29:59', 'admin', 0),
(103, '000800028652', 'Repair', '2', '2018-01-26 03:29:59', 'admin', 0),
(104, '000800028653', 'Repair', '3', '2018-01-26 03:29:59', 'admin', 0),
(105, '000800028668', 'Repair', '4', '2018-01-26 03:29:59', 'admin', 0),
(106, '000800028669', 'Test', '1', '2018-01-26 03:56:13', 'admin', 0),
(107, '000800028672', 'Test', '2', '2018-01-26 03:56:13', 'admin', 0),
(108, '000800028674', 'Test', '3', '2018-01-26 03:56:13', 'admin', 0),
(109, '000800028682', 'Test', '4', '2018-01-26 03:56:13', 'admin', 0),
(110, '000800028685', 'Test', '5', '2018-01-26 03:56:13', 'admin', 0),
(111, '000800028698', 'Repair', '10', '2018-01-26 04:14:53', 'admin', 0),
(112, '000800028699', 'Repair', '11', '2018-01-26 04:14:53', 'admin', 0),
(113, '000800028700', 'Repair', '12', '2018-01-26 04:14:53', 'admin', 0),
(114, '000800028701', 'fsafs', 'fafas', '2018-01-26 07:33:21', 'admin', 0),
(115, '000800028702', 'fsaf', 'fsaf', '2018-01-26 07:33:21', 'admin', 0),
(116, '000800028703', 'fasfsa', 'fasffsa', '2018-01-26 07:33:21', 'admin', 0),
(117, '000800028704', 'wqdqd', 'dwwqdq', '2018-01-26 07:33:52', 'admin', 0),
(118, '000800028717', 'wqdqd', 'qwdqd', '2018-01-26 07:33:52', 'admin', 0),
(119, '000800028718', 'qwdqwd', 'dqwdqwd', '2018-01-26 07:33:52', 'admin', 0),
(120, '000800028719', 'dwdqd', 'qdqdwq', '2018-01-26 07:34:49', 'admin', 0),
(121, '000800028727', 'wqdqd', 'wqdqd', '2018-01-26 07:34:49', 'admin', 0),
(122, '000800028728', 'qdqwd', 'wdwqd', '2018-01-26 07:34:49', 'admin', 0),
(123, '000800028729', 'dwqdqd', 'qwdqwdwq', '2018-01-26 07:34:49', 'admin', 0),
(124, '000800028730', 'dwdqwd', 'dqw', '2018-01-26 07:34:49', 'admin', 0),
(125, '000800028731', 'sas', 'sasa', '2018-01-26 07:42:27', 'admin', 0),
(126, '000800028732', 'scasca', 'csacsa', '2018-01-26 08:15:10', 'admin', 0),
(127, '000800028733', 'bdf', 'dbdfbd', '2018-01-26 08:16:42', 'admin', 0),
(128, '000800028734', 'fbdf', 'bdfbdbfdb', '2018-01-26 08:16:42', 'admin', 0),
(129, '000800028735', 'csacsac', 'cacsacas', '2018-01-26 08:19:46', 'admin', 0),
(130, '000800028738', 'dwqdq', 'dwqd', '2018-01-26 08:21:00', 'admin', 0),
(131, '000800028739', 'dwq', 'dwdqwwq', '2018-01-26 08:21:00', 'admin', 0),
(132, '000800028747', 'fwew', 'ffewf', '2018-01-26 08:21:00', 'admin', 0),
(133, '000800028748', 'fewfw', 'fewfwfw', '2018-01-26 08:21:00', 'admin', 0),
(134, '000800028741', 'csac', 'sacas', '2018-01-26 08:49:44', 'admin', 0),
(135, '000800028744', 'csacs', 'casca', '2018-01-26 08:55:01', 'admin', 0),
(136, '000800028745', 'xsa', 'xaxsax', '2018-01-26 09:01:00', 'admin', 0),
(137, '000800028749', 'sx', 'asxsaxsaxsa', '2018-01-26 09:01:00', 'admin', 0),
(138, '000800028751', 'sacsacc', 'sacasc', '2018-01-26 09:11:42', 'admin', 0),
(139, '000800028752', 'csacasc', 'ascasccsa', '2018-01-26 09:11:42', 'admin', 0),
(140, '000800028753', 'Repair', '5', '2018-01-26 09:40:14', 'admin', 0),
(141, '000800028754', 'Repair', '10', '2018-01-26 09:40:14', 'admin', 0),
(142, '000800028755', 'Repair', '7', '2018-01-26 09:40:14', 'admin', 0),
(143, '000800028759', 'ddd', 'ddd', '2018-01-26 10:32:14', 'admin', 0),
(144, '000800028757', 'ggk', 'fafas', '2018-01-26 10:34:12', 'admin', 0),
(145, '000800028760', 'fhhf', 'tut', '2018-01-26 10:35:31', 'admin', 0),
(146, '000800028761', 'fad', 'asdadd', '2018-01-29 05:16:20', 'admin', 0);

-- --------------------------------------------------------

--
-- Table structure for table `detail_main_project`
--

CREATE TABLE `detail_main_project` (
  `ID_PHASE` int(10) NOT NULL,
  `ID_PROJECT` varchar(10) NOT NULL,
  `AREA` varchar(30) NOT NULL,
  `PHASE` varchar(30) NOT NULL,
  `ID_TIMELINE` int(6) NOT NULL,
  `REMARK` varchar(30) NOT NULL,
  `PRGRESS_STATUS` varchar(30) NOT NULL,
  `MHRS_PLAN` varchar(10) NOT NULL,
  `MHRS_ACT` varchar(10) NOT NULL,
  `MVT_STATUS` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `detail_main_project`
--

INSERT INTO `detail_main_project` (`ID_PHASE`, `ID_PROJECT`, `AREA`, `PHASE`, `ID_TIMELINE`, `REMARK`, `PRGRESS_STATUS`, `MHRS_PLAN`, `MHRS_ACT`, `MVT_STATUS`) VALUES
(1, '001', 'Hangar', 'Removal', 1, '', '', '', '', 0),
(2, '001', 'Hangar', 'Inspection', 1, '', '', '', '', 0),
(3, '001', 'Hangar', 'Rectification', 1, '', '', '', '', 0),
(4, '001', 'Hangar', 'Installation', 1, '', '', '', '', 0),
(5, '001', 'Hangar', 'RTS', 1, '', '', '', '', 0),
(6, '001', 'WSSS', 'Rectification', 1, '', '', '', '', 0),
(7, '001', 'WSSE', 'Rectification', 1, '', '', '', '', 0),
(8, '001', 'WSCB', 'Rectification', 1, '', '', '', '', 0),
(9, '001', 'WSLS & WSSW', 'Rectification', 1, '', '', '', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `info`
--

CREATE TABLE `info` (
  `GENERAL_INFO` varchar(50) NOT NULL,
  `AREA` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `jobcard`
--

CREATE TABLE `jobcard` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `seq` int(11) NOT NULL,
  `order` int(11) NOT NULL,
  `description` varchar(350) DEFAULT '',
  `cus_jc_num` varchar(255) DEFAULT NULL,
  `task` varchar(255) DEFAULT NULL,
  `itval` varchar(255) DEFAULT NULL,
  `mhrs_plan` varchar(255) DEFAULT NULL,
  `skill` varchar(255) DEFAULT NULL,
  `area` varchar(255) DEFAULT NULL,
  `phase` varchar(255) DEFAULT NULL,
  `day` int(11) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `date_close` date DEFAULT NULL,
  `jc_reff_mdr` varchar(255) DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `free_text` varchar(255) DEFAULT NULL,
  `date_progress` date DEFAULT NULL,
  `mat_status` varchar(255) DEFAULT NULL,
  `cabin_status` varchar(255) DEFAULT NULL,
  `id_project` int(11) DEFAULT NULL,
  `date_created` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jobcard`
--

INSERT INTO `jobcard` (`id`, `seq`, `order`, `description`, `cus_jc_num`, `task`, `itval`, `mhrs_plan`, `skill`, `area`, `phase`, `day`, `status`, `date_close`, `jc_reff_mdr`, `remark`, `free_text`, `date_progress`, `mat_status`, `cabin_status`, `id_project`, `date_created`) VALUES
(4, 1, 802078752, '[RTN]4N-12-016-00-01: LEADING EDGE FLAP PDU - LEFT WING', '4N-11-000-00-01', 'CHK', 'RTN', '12,00', 'TBP', 'FUSELAGE', 'ggf', 13, 'Waiting Material', '2018-03-11', '14', 'JSSUUS', 'PRELIM', '2018-03-12', '', '', 4, '2018-03-01 16:38:51'),
(5, 2, 802078753, '[RTN]4N-11-001-01-01: CHECK CONDITION OF INTERIOR PLACARDS', '4N-11-001-01-01', 'CHK', 'RTN', '12,00', 'CBN', 'CABIN', 'INSP', 1, 'Waiting Tool', '0000-00-00', '4', 'SENT TO CABIN', 'SENT TO CABIN 27 JAN', '0000-00-00', '', '', 4, '2018-03-01 16:38:51'),
(6, 3, 802078754, '[RTN]4N-12-004-00-01: ENG 1 IDG OIL CHANGE', '4N-12-004-00-01', 'SVC', 'RTN', '2,40', 'A/P', 'ENG#1', 'INST/REST', 5, 'Open', '0000-00-00', '0', '', '', '0000-00-00', '', '', 4, '2018-03-01 16:38:51'),
(7, 4, 802078755, '[RTN]4N-12-004-00-02: ENG 2 IDG OIL CHANGE', '4N-12-004-00-02', 'SVC', 'RTN', '2,40', 'A/P', 'CABIN', 'INSP', 24, 'Open', '0000-00-00', '', 'PART COMPLETE', 'DOC AND PART DI RAK', '0000-00-00', '', '', 4, '2018-03-01 16:38:51'),
(8, 5, 802078756, '[RTN]4N-12-004-00-03: ENG 3 IDG OIL CHANGE', '4N-12-004-00-03', 'SVC', 'RTN', '2,40', 'A/P', 'CABIN', 'INSP', 24, 'Open', '0000-00-00', '', 'PART COMPLETE', 'DOC AND PART DI RAK', '0000-00-00', '', '', 4, '2018-03-01 16:38:51'),
(9, 6, 802078757, '[RTN]4N-12-004-00-04: ENG 4 IDG OIL CHANGE', '4N-12-004-00-04', 'SVC', 'RTN', '2,40', 'A/P', '', '', 8, 'Close', '2018-02-08', '', '', '', '0000-00-00', '', '', 4, '2018-03-01 16:38:51'),
(10, 7, 802078758, '[RTN]4N-12-008-00-01: DOOR MOUNTED GIRT BAR FITTINGS', '4N-12-008-00-01', 'LUB', 'RTN', '2,30', 'CBN', 'CABIN', 'INST/REST', 25, 'Open', '0000-00-00', '', '', '', '0000-00-00', '', '', 4, '2018-03-01 16:38:51'),
(11, 8, 802078759, '[RTN]4N-12-010-00-01: ACCESS PANEL ZIPPERS', '4N-12-010-00-01', 'LUB', 'RTN', '1,20', 'A/P', 'AFT CARGO', 'SERV/LUB', 24, 'Open', '0000-00-00', '', '', '', '0000-00-00', '', '', 4, '2018-03-01 16:38:51'),
(12, 9, 802078780, '[RTN]4N-12-012-00-01: FLIGHT ATTENDANT SEATS', '4N-12-012-00-01', 'LUB', 'RTN', '3,00', 'CBN', 'CABIN', 'INST/REST', 25, 'Open', NULL, NULL, 'SENT TO CABIN', 'SENT TO CABIN 27 JAN', NULL, NULL, NULL, 4, NULL),
(13, 10, 802078781, '[RTN]4N-12-016-00-01: LEADING EDGE FLAP PDU - LEFT WING', '4N-12-014-00-01', 'LUB', 'RTN', '0,80', 'A/P', 'CABIN', 'INST/REST', 25, 'Close', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, NULL),
(14, 11, 802078782, '[RTN]4N-12-016-00-01: LEADING EDGE FLAP PDU - LEFT WING', '4N-12-016-00-01', 'SVC', 'RTN', '1,50', 'CBN', 'AFT CARGO', 'INSP', 24, 'Close', NULL, NULL, 'PART COMPLETE', 'DOC AND PART DI RAK', NULL, NULL, NULL, 4, NULL),
(15, 12, 802078783, '[RTN]4N-12-016-00-02: LEADING EDGE FLAP PDU - RIGHT WING', '4N-12-016-00-02', 'SVC', 'RTN', '1,50', 'TBP', 'AFT CARGO', 'INSP', 24, 'Waiting Material', NULL, NULL, 'PART COMPLETE', 'DOC AND PART DI RAK', NULL, NULL, NULL, 4, NULL),
(16, 13, 802078784, '[RTN]4N-12-018-00-01: T.E. FLAP TRANSMISSION - LEFT WING', '4N-12-018-00-01', 'SVC', 'RTN', '6,00', 'A/P', NULL, NULL, 23, 'Open', NULL, NULL, 'PART COMPLETE', 'DOC AND PART DI RAK', NULL, NULL, NULL, 4, NULL),
(17, 14, 802078785, '[RTN]4N-12-018-00-02: T.E. FLAP TRANSMISSION - RIGHT WING', '4N-12-018-00-02', 'SVC', 'RTN', '6,00', 'A/P', NULL, NULL, 23, 'Open', NULL, NULL, 'PART COMPLETE', 'DOC AND PART DI RAK', NULL, NULL, NULL, 4, NULL),
(18, 15, 802078786, '[RTN]4N-12-022-00-01: LEFT WING T.E. CONTROL CABLES', '4N-12-022-00-01', 'SVC', 'RTN', '5,10', 'A/P', 'LH-WING', 'INST/REST', 18, 'Close', '2018-02-08', NULL, NULL, NULL, NULL, NULL, NULL, 4, NULL),
(19, 16, 802078787, '[RTN]4N-12-022-00-02: RIGHT WING T.E. CONTROL CABLES', '4N-12-022-00-02', 'SVC', 'RTN', '5,10', 'A/P', 'RH-WING', 'INST/REST', 18, 'Open', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, NULL),
(20, 17, 802078788, '[RTN]4N-12-024-00-01: ELEVATOR CABLES', '4N-12-024-00-01', 'SVC', 'RTN', '3,60', 'A/P', 'TAIL', 'INST/REST', 9, 'Open', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, NULL),
(21, 18, 802078789, '[RTN]4N-12-026-00-01: RUDDER CABLES', '4N-12-026-00-01', 'SVC', 'RTN', '4,20', 'A/P', 'TAIL', 'INST/REST', 9, 'Close', '2018-02-08', NULL, NULL, NULL, NULL, NULL, NULL, 4, NULL),
(22, 19, 802078790, '[RTN]4N-12-028-00-01: FLIGHT CONTROL CABLES - LEFT', '4N-12-028-00-01', 'SVC', 'RTN', '9,00', 'A/P', 'LH-WING', 'INST/REST', 18, 'Open', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, NULL),
(23, 20, 802078791, '[RTN]4N-12-028-00-02: FLIGHT CONTROL CABLES - RIGHT', '4N-12-028-00-02', 'SVC', 'RTN', '9,00', 'A/P', 'RH-WING', 'INST/REST', 18, 'Progress', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, NULL),
(24, 21, 802078792, '[RTN]4N-12-029-00-01: LEFT AILERON&SPOILER/SPEEDBRAKE CABLES', '4N-12-029-00-01', 'SVC', 'RTN', '9,00', 'A/P', 'LH-WING', 'INST/REST', 18, 'Waiting Tool', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, NULL),
(25, 22, 802078793, '[RTN]4N-12-029-00-02: RIGHT AILERON&SPOILER/SPEEDBRAKE CABLES', '4N-12-029-00-02', 'SVC', 'RTN', '9,00', 'A/P', 'RH-WING', 'INST/REST', 18, 'Open', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, NULL),
(26, 23, 802078794, '[RTN]4N-12-030-00-01: AILERON HINGES&ACTUATORS - L WING', '4N-12-030-00-01', 'LUB', 'RTN', '1,50', 'A/P', 'LH-WING', 'INST/REST', 9, 'Open', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, NULL),
(27, 24, 802078795, '[RTN]4N-12-030-00-02: AILERON HINGES&ACTUATORS - R WING', '4N-12-030-00-02', 'LUB', 'RTN', '1,50', 'A/P', 'RH-WING', 'INST/REST', 9, 'Open', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, NULL),
(28, 25, 802078796, '[RTN]4N-12-032-00-01: AILERON PROGRAMMER MECHANISM', '4N-12-032-00-01', 'LUB', 'RTN', '1,50', 'A/P', 'RH-WING', 'INST/REST', 9, 'Open', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, NULL),
(29, 26, 802078972, '[RTN]4N-21-026-15-01: FORWARD OVERBOARD VALVE SCREEN', '4N-21-026-15-01', 'RST', 'RTN', '2,10', 'E/A', 'ELECT', 'SERV/LUB', 6, 'Close', '2018-02-03', NULL, NULL, NULL, NULL, NULL, NULL, 4, NULL),
(30, 27, 802078973, '[RTN]4N-21-028-00-01: LOWER LOBE CARGO COMP COND AIR SYS', '4N-21-028-00-01', 'OPC', 'RTN', '0,60', 'E/A', 'ELECT', 'OPC/FUC', 2, 'Close', '2018-01-31', NULL, NULL, NULL, NULL, NULL, NULL, 4, NULL),
(31, 28, 802078974, '[RTN]4N-21-031-01-01: PRESS CONTR SYS OUTFLOW VALVE&S/W', '4N-21-031-01-01', 'OPC', 'RTN', '0,30', 'E/A', 'ELECT', 'OPC/FUC', 2, 'Close', '2018-01-30', NULL, NULL, NULL, NULL, NULL, NULL, 4, NULL),
(32, 29, 802078975, '[RTN]4N-21-031-03-01: PRESS CONT SYS OUTFLOW VALVE DOORS', '4N-21-031-03-01', 'DVI', 'RTN', '0,30', 'E/A', 'ELECT', 'INSP', 6, 'Close', '2018-02-03', NULL, NULL, NULL, NULL, NULL, NULL, 4, NULL),
(33, 30, 802078976, '[RTN]4N-21-031-06-01: PRESSURIZATION OUTFLOW VALVES', '4N-21-031-06-01', 'RST', 'RTN', '1,50', 'E/A', 'ELECT', 'SERV/LUB', 6, 'Close', '2018-02-03', NULL, NULL, NULL, NULL, NULL, NULL, 4, NULL),
(34, 31, 802078977, '[RTN]4N-21-032-01-01: CABIN PRESS RELIEF VALVES', '4N-21-032-01-01', 'FNC', 'RTN', '6,00', 'E/A', 'ELECT', 'OPC/FUC', 4, 'Open', NULL, '2', NULL, NULL, NULL, NULL, NULL, 4, NULL),
(35, 32, 802078978, '[RTN]4N-21-032-02-01: CABIN PRESS RELIEF VALVE FILTERS', '4N-21-032-02-01', 'DIS', 'RTN', '0,60', 'E/A', NULL, NULL, 17, 'Open', NULL, NULL, NULL, 'JOBCARD ADA DI MATERIAL', NULL, NULL, NULL, 4, NULL),
(36, 33, 802078979, '[RTN]4N-21-032-03-01: CABIN NEGATIVE PRESS RELIEF VALVE', '4N-21-032-03-01', 'FNC', 'RTN', '0,60', 'E/A', 'ELECT', 'OPC/FUC', 4, 'Open', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, NULL),
(37, 34, 802078980, '[RTN]4N-21-033-01-01: PRESS CONT ANALOG SENSOR', '4N-21-033-01-01', 'FNC', 'RTN', '0,30', 'E/A', 'ELECT', 'OPC/FUC', 4, 'Close', '2018-02-01', NULL, NULL, NULL, NULL, NULL, NULL, 4, NULL),
(38, 35, 802079622, '[RTN]4N-52-440-00-01: NO. 1 LEFT MAIN DECK ENTRY DOOR STOPS', '4N-52-440-00-01', 'DVI', 'RTN', '0,3', 'A/P', 'FUSELAGE', NULL, 5, 'Close', '2018-02-02', '0', NULL, NULL, NULL, NULL, NULL, 4, NULL),
(39, 36, 802079640, '[RTN]4N-52-450-00-09: NO. 4 RIGHT MAIN DECK ENTRY DOOR (NOT APPLICABLE TO PRODUCTION FREIGHTER MODEL)', '4N-52-450-00-09', 'DVI', 'RTN', '3,00', 'A/P', 'FUSELAGE', NULL, 9, 'Close', '2018-02-06', '0', NULL, NULL, NULL, NULL, NULL, 4, NULL),
(40, 37, 802079641, '[RTN]4N-52-450-00-10: NO. 5 RIGHT MAIN DECK ENTRY DOOR (NOT APPLICABLE TO PRODUCTION FREIGHTER MODEL)', '4N-52-450-00-10', 'DVI', 'RTN', '3,00', 'A/P', 'FUSELAGE', NULL, 8, 'Close', '2018-02-05', '0', NULL, NULL, NULL, NULL, NULL, 4, NULL),
(41, 38, 802079642, '[RTN]4N-52-470-00-01: UPPER DECK EMERGENCY DOOR - LEFT', '4N-52-470-00-01', 'DVI', 'RTN', '3,00', 'TBP', 'FUSELAGE', NULL, 9, 'Open', NULL, '0', NULL, NULL, NULL, NULL, NULL, 4, NULL),
(42, 39, 802079643, '[RTN]4N-52-470-00-02: UPPER DECK EMERGENCY DOOR - RIGHT', '4N-52-470-00-02', 'DVI', 'RTN', '3,00', 'A/P', 'FUSELAGE', '', 9, 'Open', '0000-00-00', '0', '', '', '0000-00-00', '', '', 4, NULL),
(43, 40, 802079799, '[RTN]4N-54-062-01-01: NACELLE STRUT #1 ACCESS DOORS', '4N-54-062-01-01', 'FNC', 'RTN', '0,90', 'A/P', 'ENG#1', 'OPC/FUC', 5, 'Close', '2018-02-02', '0', '', '', '0000-00-00', '', '', 4, NULL),
(44, 41, 802080208, '[RTN]4N-54-062-01-01: NACELLE STRUT #1 ACCESS DOORS', '4Q-28-115-02-01', 'CHK', 'RTN', '10,00', 'A/P', 'ENG#1', '', 3, 'Open', '0000-00-00', '0', '', 'PRELIM', '0000-00-00', '', 'ui', 4, NULL),
(45, 42, 802080300, '[RTN]4N-54-062-01-01: NACELLE STRUT #1 ACCESS DOORS', '747-54-340', 'CHK', 'ADD', '4,00', 'A/P', 'ENG#1', '', 9, 'Open', '2018-02-13', '0', '', 'PRELIM', '0000-00-00', '23', '', 4, NULL),
(46, 43345, 2147483647, 'cbxncxcxcxcxc', '', '', '', '', '', '', '', 4, '', '0000-00-00', '', '', '', '0000-00-00', '', '', 4, NULL),
(47, 6444, 87633467, 'gghgfv', 'fdfd', 'fdf', '', '', '', '', '', 0, '', '0000-00-00', '', '', '', '0000-00-00', '', '', 4, NULL),
(48, 43346, 666, '[RTN]4N-54-062-01-01: NACELLE STRUT #1 ACCESS DOORS', '467676767', 'SO', '', '', '', '', '', 0, '', '0000-00-00', '', '', '', '0000-00-00', '', '', 4, NULL),
(49, 123, 808213123, 'Fajar 123', '', '', '', '', '', '', '', 0, '', '0000-00-00', '', '', '', '0000-00-00', '', '', NULL, NULL),
(51, 2147483647, 803123122, 'Teh Fifa eaaa...', '', '', '', '', '', '', '', 0, '', '0000-00-00', '', '', '', '0000-00-00', '', '', NULL, NULL),
(66, 367, 34657, 'dfghgfhk', 'ghfj', '', '', '', '', '', '', 0, '', '0000-00-00', '', '', '', '0000-00-00', '', '', 5, '2018-03-01 10:27:01'),
(67, 4356, 4678, 'gdfhg', 'fghjk', '', '', '', '', '', '', 0, '', '0000-00-00', '', '', '', '0000-00-00', '', '', 5, '2018-03-01 10:27:02'),
(68, 321, 808321312, 'Fajar Ganteng', '', '', '', '', '', '', '', 0, '', '0000-00-00', '', '', '', '0000-00-00', '', '', 4, '2018-03-01 16:34:31'),
(69, 76767, 544, '6568', 'gfgh', '', '', '', '', '', '', 0, '', '0000-00-00', '', '', '', '0000-00-00', '', '', 4, '2018-03-01 16:35:45'),
(70, 7878, 5656, '7878', '34567', '', '', '', '', '', '', 0, '', '0000-00-00', '', '', '', '0000-00-00', '', '', 4, '2018-03-01 16:35:45'),
(71, 6878, 456, '4', '', '', '', '', '', '', '', 0, '', '0000-00-00', '', '', '', '0000-00-00', '', '', 4, '2018-03-01 16:36:27'),
(72, 8989, 4657890, '', '', '', '', '', '', '', '', 0, '', '0000-00-00', '', '', '', '0000-00-00', '', '', 4, '2018-03-01 16:36:27');

-- --------------------------------------------------------

--
-- Table structure for table `mdr`
--

CREATE TABLE `mdr` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `seq` int(11) NOT NULL,
  `mdr_order` int(11) NOT NULL,
  `jc_reff` varchar(255) DEFAULT NULL,
  `ori_taskcard` varchar(255) DEFAULT NULL,
  `discrepancies` mediumtext,
  `area_code` varchar(255) DEFAULT NULL,
  `main_skill` varchar(255) DEFAULT NULL,
  `iss_by` varchar(255) DEFAULT NULL,
  `date_from_pe` varchar(255) DEFAULT NULL,
  `accomp_status` varchar(255) DEFAULT NULL,
  `mat_status` varchar(255) DEFAULT NULL,
  `date` varchar(255) DEFAULT NULL,
  `step1` varchar(255) DEFAULT NULL,
  `date1` varchar(255) DEFAULT NULL,
  `step2` varchar(255) DEFAULT NULL,
  `date2` varchar(255) DEFAULT NULL,
  `step3` varchar(255) DEFAULT NULL,
  `date3` varchar(255) DEFAULT NULL,
  `status_mdr` varchar(255) DEFAULT NULL,
  `date_close` varchar(255) DEFAULT NULL,
  `material_status_mrm` varchar(255) DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `free_text` varchar(255) DEFAULT NULL,
  `date_progress` varchar(255) DEFAULT NULL,
  `day` varchar(255) DEFAULT NULL,
  `cabin_status` varchar(255) DEFAULT NULL,
  `id_project` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mdr`
--

INSERT INTO `mdr` (`id`, `seq`, `mdr_order`, `jc_reff`, `ori_taskcard`, `discrepancies`, `area_code`, `main_skill`, `iss_by`, `date_from_pe`, `accomp_status`, `mat_status`, `date`, `step1`, `date1`, `step2`, `date2`, `step3`, `date3`, `status_mdr`, `date_close`, `material_status_mrm`, `remark`, `free_text`, `date_progress`, `day`, `cabin_status`, `id_project`) VALUES
(1, 1, 802111663, '802086071', 'OPN-002', 'SCREW AT BODY FAIRING BAD CONDITION 500 EA', 'FUSELAGE', 'A/P', 'Mr Cristian Adi Putra Utomo', '30-Jan-18', 'Waiting Material', 'Shortage', '30-Jan-18', 'A/P', NULL, NULL, NULL, NULL, NULL, 'Open', NULL, NULL, 'MAP SHORTAGE', '30 Jan', NULL, NULL, NULL, 4),
(2, 2, 802111940, '802086129', 'GMF-002', 'REF DMI, 708B RETENTION BAR KNOB IS BROKEN', 'GENERAL AREA', 'E/A', 'Mr Yuda Nur Reza', NULL, 'Waiting RO', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Open', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4),
(3, 3, 802111941, '802086129', 'GMF-002', 'REF DMI, LAVATORY DC22 NO WATER (WATER TAB LEAKING FROM CEILING),\nLAVATORY INOP.', 'GENERAL AREA', 'E/A', 'Mr Yuda Nur Reza', NULL, 'Waiting RO', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Open', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4),
(4, 4, 802111942, '802086129', 'GMF-002', 'LH BODY GEAR DOOR INBOARD RUBBER SEAL TEAR OFF', 'GENERAL AREA', 'A/P', 'Mr Andi Makkarumpa Arsyad', '30-Jan-18', 'Waiting Material', 'Shortage', '30-Jan-18', 'A/P', NULL, 'E/A', NULL, NULL, NULL, 'Open', NULL, NULL, 'MAP SHORTAGE', '30 Jan', NULL, NULL, NULL, 4),
(5, 5, 802111943, '802086129', 'GMF-002', 'RH BODY GEAR DOOR INBORD BONDING JUMPER BROKEN 1EA', 'GENERAL AREA', 'E/A', 'Mr Andi Makkarumpa Arsyad', '30-Jan-18', 'Carry Out', NULL, NULL, 'A/P', '2-Feb-18', 'E/A', '3-Feb-18', NULL, NULL, 'Close', '3-Feb-18', NULL, NULL, NULL, NULL, '6', NULL, 4),
(6, 6, 802111944, '802086129', 'GMF-002', 'MLG BRAKE INDICATOR PIN NEAR LIMIT WHEEL #3', 'GENERAL AREA', 'A/P', 'Mr Andi Makkarumpa Arsyad', '30-Jan-18', 'Carry Out', NULL, NULL, 'A/P', '30-Jan-18', NULL, NULL, NULL, NULL, 'Close', '30-Jan-18', NULL, NULL, NULL, NULL, '2', NULL, 4),
(7, 7, 802111945, '802086129', 'GMF-002', 'MLG BRAKE INDICATOR PIN NEAR LIMIT WHEEL #6', 'GENERAL AREA', 'A/P', 'Mr Andi Makkarumpa Arsyad', '30-Jan-18', 'Carry Out', NULL, NULL, 'A/P', '30-Jan-18', NULL, NULL, NULL, NULL, 'Close', '30-Jan-18', NULL, NULL, NULL, NULL, '2', NULL, 4),
(8, 8, 802111946, '802086129', 'GMF-002', 'MLG BRAKE INDICATOR PIN NEAR LIMIT WHEEL #9', 'GENERAL AREA', 'A/P', 'Mr Andi Makkarumpa Arsyad', '30-Jan-18', 'Carry Out', NULL, NULL, 'A/P', '30-Jan-18', NULL, NULL, NULL, NULL, 'Close', '30-Jan-18', NULL, NULL, NULL, NULL, '2', NULL, 4),
(9, 9, 802111947, '802086129', 'GMF-002', 'MLG BRAKE INDICATOR PIN NEAR LIMIT WHEEL #11', 'GENERAL AREA', 'A/P', 'Mr Andi Makkarumpa Arsyad', '30-Jan-18', 'Carry Out', NULL, NULL, 'A/P', '30-Jan-18', NULL, NULL, NULL, NULL, 'Close', '30-Jan-18', NULL, NULL, NULL, NULL, '2', NULL, 4),
(10, 10, 802111949, '802086129', 'GMF-002', 'MLG BRAKE INDICATOR PIN NEAR LIMIT WHEEL #15', 'GENERAL AREA', 'A/P', 'Mr Andi Makkarumpa Arsyad', '30-Jan-18', 'Carry Out', NULL, NULL, 'A/P', '30-Jan-18', NULL, NULL, NULL, NULL, 'Close', '30-Jan-18', NULL, NULL, NULL, NULL, '2', NULL, 4),
(11, 11, 802112372, '802079143', '4N-25-040-01-01', 'WASTE FLAPPER DOOR AT LAV.BS1 FOUND PEEL OFF', 'CABIN', '0', 'Mr Kevin Bayu Putra S', NULL, 'Waiting RO', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Open', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4),
(12, 12, 802112374, '802079143', '4N-25-040-01-01', 'WASTE FLAPPER DOOR AT LAV.BS2T FOUND PEEL OFF', 'CABIN', '0', 'Mr Kevin Bayu Putra S', NULL, 'Waiting RO', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Open', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4),
(13, 13, 802112730, '802079400', '4N-32-802-01-01', 'NOSE GEAR RH & LH BRAKE PAD AND IT\'S FASTENER WORN OUT', 'L/G', 'A/P', 'Mr Muhammad Yunus Lubis', '30-Jan-18', 'Waiting Material', 'WCS', '5-Feb-18', 'A/P', NULL, 'STR', NULL, 'A/P', NULL, 'Open', NULL, NULL, 'MAP W. CUST. SUPPLY', '5 Feb', NULL, NULL, NULL, 4),
(14, 14, 802112732, '802079403', '4N-32-808-01-01', 'LH NOSE L/G WHEEL TYRE WAS DEEP CUT', 'L/G', 'A/P', 'Mr Ukon Dulfakor', '30-Jan-18', 'Perform by Prod', NULL, NULL, 'A/P', NULL, NULL, NULL, NULL, NULL, 'Progress', NULL, NULL, 'PART COMPLETE', 'PART DI LAYOUT DOC DI PLANBOARD', NULL, NULL, NULL, 4),
(15, 15, 802112735, '802079403', '4N-32-808-01-01', 'RH NOSE L/G WHEEL TYRE WAS DEEP CUT', 'L/G', 'A/P', 'Mr Ukon Dulfakor', '30-Jan-18', 'Perform by Prod', NULL, NULL, 'A/P', NULL, NULL, NULL, NULL, NULL, 'Progress', NULL, NULL, 'PART COMPLETE', 'PART DI LAYOUT DOC DI PLANBOARD', NULL, NULL, NULL, 4),
(16, 16, 802112781, '802079403', '4N-32-808-01-01', 'LH NOSE L/G STEERING ACTUATOR WAS HYDRAULIC LEAK FROM PISTON', 'L/G', 'A/P', 'Mr Ukon Dulfakor', '31-Jan-18', 'Waiting Material', 'WCS', '31-Jan-18', 'A/P', NULL, 'A/P', NULL, NULL, NULL, 'Open', NULL, NULL, 'MAP W. CUST. SUPPLY', '31 Jan', NULL, NULL, NULL, 4),
(17, 17, 802112966, '802080087', '4N-71-804-01-01', 'ENG#1 NOSE COWL PAINT PEEL OFF', 'ENG#4', '0', 'Mr Bintang Al Amin Wibisono', NULL, 'Waiting RO', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Open', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4),
(18, 18, 802112969, '802080087', '4N-71-804-01-01', 'ENG#1 NOSE COWL AFT SIDE LIP RIVET LOOSE 4EA', 'ENG#4', 'STR', 'Mr Bintang Al Amin Wibisono', '3-Feb-18', 'Perform by Prod', NULL, NULL, 'STR', NULL, NULL, NULL, NULL, NULL, 'Progress', NULL, NULL, 'SENT TO STR HANGAR', '3 Feb', NULL, NULL, NULL, 4),
(19, 19, 802112972, '802080103', '4N-71-812-01-01', 'ENG#1 LH AND RH T/R STICKER HOIST POINT PEEL OFF', 'ENG#1', '0', 'Mr Bintang Al Amin Wibisono', NULL, 'Waiting RO', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Open', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4),
(20, 20, 802112973, '802080103', '4N-71-812-01-01', 'ENG#1 LH AND RH T/R STICKER LOCKOUT PEEL OFF', 'ENG#1', '0', 'Mr Bintang Al Amin Wibisono', NULL, 'Waiting RO', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Open', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4),
(21, 21, 802113667, '802079954', '4N-57-570-00-03', 'RH I/B AILERON ACTOATOR SIGN LEAK', 'RH-WING', 'A/P', 'Mr Cristian Adi Putra Utomo', '31-Jan-18', 'Waiting Material', 'WCS', '31-Jan-18', 'A/P', NULL, NULL, NULL, NULL, NULL, 'Open', NULL, NULL, 'MAP W. CUST. SUPPLY', '31 Jan', NULL, NULL, NULL, 4),
(22, 22, 802113668, '802079953', '4N-57-570-00-02', 'LH I/B AILERON SIGN LEAK  FROM PISTON', 'LH-WING', 'A/P', 'Mr Cristian Adi Putra Utomo', '31-Jan-18', 'Waiting Material', 'WCS', '1-Feb-18', 'A/P', NULL, NULL, NULL, NULL, NULL, 'Open', NULL, NULL, 'MAP W. CUST. SUPPLY', '1 Feb', NULL, NULL, NULL, 4),
(23, 23, 802113670, '802079953', '4N-57-570-00-02', 'LH RATIO CHANGER ACTUATOR SIGN LEAK FROM PISTON', 'LH-WING', 'A/P', 'Mr Cristian Adi Putra Utomo', '31-Jan-18', 'Waiting Material', 'WCS', '31-Jan-18', 'A/P', NULL, 'A/P', NULL, NULL, NULL, 'Open', NULL, NULL, 'MAP W. CUST. SUPPLY', '31 Jan', NULL, NULL, NULL, 4),
(24, 24, 802114463, '802079584', '4N-52-021-03-01', 'VELCRO TAPE OF ACOUSTIC PAD POST CREW O/H HATCH BAD CONDITION', 'CABIN', '0', 'Mr Tio Andira Lupita', NULL, 'Waiting RO', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Open', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4),
(25, 25, 802114465, '802079584', '4N-52-021-03-01', 'PUSH BUTTON RELEASE OF EXTERNAL HANDLE O/H HATCH PAINT PEEL OFF', 'CABIN', '0', 'Mr Tio Andira Lupita', NULL, 'Waiting RO', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Open', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4),
(26, 26, 802120423, '802080351', 'SR-AAJ-57-335-R', 'RH WING T/E I/B MID FLAP AT WBL 445 CLOSURE RIB WAS CRACK REF. SR-AAJ-\n57-335-R.', 'FUSELAGE', 'STR', 'Mr Ismail', '3-Feb-18', 'Perform by Prod', NULL, NULL, 'STR', NULL, 'STR', NULL, 'STR', NULL, 'Progress', NULL, NULL, 'SENT TO STR HANGAR', '6 Feb', NULL, NULL, NULL, 4),
(27, 27, 802120435, '802080343', 'SR-AAJ-53-036-R', 'FASTENER REPAIR BETWEEN BS.140 BS.160 BETWEEN S.24 L - S.25 L REF.SR-\nAAJ-53-036-R.', 'FUSELAGE', 'NDT', 'Mr Ismail', '5-Feb-18', 'Prepare NDT', NULL, NULL, 'STR', '6-Feb-18', 'NDT', NULL, 'TBP', NULL, 'Pending', NULL, NULL, 'SENT TO NDT', '6 Feb', NULL, NULL, NULL, 4),
(28, 28, 802120436, '802079977', '4N-57-610-00-08', 'RH WING O/B T/E FLAP DRIVE ARM POS #1 FROM O/B SIDE FOUND OVERPLAY', 'RH-WING', 'A/P', 'Mr Kurniawan Agus Sri Sunaryo', '3-Feb-18', 'Perform by Prod', NULL, NULL, 'A/P', NULL, 'TBRS', NULL, 'A/P', NULL, 'Progress', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4),
(29, 29, 802120438, '802080344', 'SR-AAJ-53-037-R', 'FASTENER REPAIR BETWEEN BS.160 - BS.180 BETWEEN S.24 L - S.25 L REF.SR-\nAAJ-53-035-R.', 'FUSELAGE', 'NDT', 'Mr Ismail', '5-Feb-18', 'Prepare NDT', NULL, NULL, 'STR', '6-Feb-18', 'NDT', NULL, 'TBP', NULL, 'Pending', NULL, NULL, 'SENT TO NDT', '6 Feb', NULL, NULL, NULL, 4),
(30, 30, 802120471, '802079835', '4N-54-440-01-03', 'ENG#3 UPPER PYLON FORWARD FAIRING #3 SEAL PLATE BENT', 'ENG#3', '0', 'Mr Muhammad Mirza Fadhlih', NULL, 'Waiting RO', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Open', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4),
(31, 31, 802120482, '802079835', '4N-54-440-01-03', 'ENG#3 UPPER PYLON FORWARD FAIRING #3 ERRODED', 'ENG#3', '0', 'Mr Muhammad Mirza Fadhlih', NULL, 'Waiting RO', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Open', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4),
(32, 32, 802122382, '802079901', '4N-55-818-01-01', 'RH T/E HORSTAB PANEL PLATE NUT  WAS BROKEN 3 EA', 'TAIL', 'STR', 'Mr Edi Suratno', '3-Feb-18', 'Carry Out', NULL, NULL, 'STR', '6-Feb-18', NULL, NULL, NULL, NULL, 'Close', '6-Feb-18', NULL, NULL, NULL, NULL, NULL, NULL, 4),
(33, 33, 802127403, '802079908', '4N-55-832-01-01', 'LH I/B AND O/B UPPER & LOWER TIP NEED SEALANT', 'TAIL', '0', 'Mr Badik Utomo', NULL, 'Waiting RO', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Open', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4),
(34, 34, 802127404, '802079908', '4N-55-832-01-01', 'RH I/B ELEVATOR SEAL WAS TORN', 'TAIL', '0', 'Mr Muhammad Yunus Lubis', NULL, 'Waiting RO', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Open', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4),
(38, 35, 76, '076', NULL, NULL, NULL, NULL, NULL, NULL, 'Waiting Tool', 'Shortage', NULL, NULL, NULL, NULL, NULL, 'NDT', NULL, NULL, NULL, NULL, 'RO BY ->', NULL, NULL, NULL, NULL, 4),
(39, 1, 446576456, '876gr', NULL, '<p>\r\n	gfgfg</p>\r\n', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 7);

-- --------------------------------------------------------

--
-- Table structure for table `mrm`
--

CREATE TABLE `mrm` (
  `no` int(11) NOT NULL,
  `Part_Number` varchar(255) DEFAULT NULL,
  `Alternate_Part_Number` varchar(255) DEFAULT NULL,
  `Material_DESCRIPTION` longtext,
  `Mat_Type` varchar(255) DEFAULT NULL,
  `IPC` varchar(255) DEFAULT NULL,
  `Order_Number` varchar(255) DEFAULT NULL,
  `STO_Number` varchar(255) DEFAULT NULL,
  `Outbound_Delivery` varchar(255) DEFAULT NULL,
  `TO_Number` varchar(255) DEFAULT NULL,
  `Issued_by` varchar(255) DEFAULT NULL,
  `jobcard_number` varchar(255) DEFAULT NULL,
  `Card_Type` varchar(255) DEFAULT NULL,
  `MRM_Issue_Date` datetime DEFAULT NULL,
  `Qty_Required_persingle_item_PN` varchar(255) DEFAULT NULL,
  `TotalQty_Required_for_all_job_task` varchar(255) DEFAULT NULL,
  `UOM` varchar(255) DEFAULT NULL,
  `Input_Stock_Manually` varchar(255) DEFAULT NULL,
  `Storage_Location` varchar(255) DEFAULT NULL,
  `Material_Fulfillment_status` longtext,
  `Fullfillment_status_date` varchar(255) DEFAULT NULL,
  `Fullfillment_remark` varchar(255) DEFAULT NULL,
  `Date_Of_PO` varchar(255) DEFAULT NULL,
  `Purchase_Order_PO` varchar(255) DEFAULT NULL,
  `AWB_Number` varchar(255) DEFAULT NULL,
  `AWB_Date` varchar(255) DEFAULT NULL,
  `Qty_Delivered` varchar(255) DEFAULT NULL,
  `Qty_Remain` varchar(255) DEFAULT NULL,
  `Material_Remark` longtext,
  `id_project` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mrm`
--

INSERT INTO `mrm` (`no`, `Part_Number`, `Alternate_Part_Number`, `Material_DESCRIPTION`, `Mat_Type`, `IPC`, `Order_Number`, `STO_Number`, `Outbound_Delivery`, `TO_Number`, `Issued_by`, `jobcard_number`, `Card_Type`, `MRM_Issue_Date`, `Qty_Required_persingle_item_PN`, `TotalQty_Required_for_all_job_task`, `UOM`, `Input_Stock_Manually`, `Storage_Location`, `Material_Fulfillment_status`, `Fullfillment_status_date`, `Fullfillment_remark`, `Date_Of_PO`, `Purchase_Order_PO`, `AWB_Number`, `AWB_Date`, `Qty_Delivered`, `Qty_Remain`, `Material_Remark`, `id_project`) VALUES
(79, 'M83248-1-905', 'AS3208-05:81205', 'PACKING-PREFORMED', 'EXP', NULL, '802079328', '601436104', NULL, 'OK', NULL, '4N-29-011-14-04', 'JC', NULL, '2,00', '3,00', 'EA', '2', 'OKE', 'DLVR FULL to Production', '01/02/2018', 'QI', '14-Dec-17', '460005747', '607-79204565', '19/01/2018', '2', '0,00', 'ATTACH MDR', 4),
(80, 'M83248-1-126', NULL, 'O\'RING', 'EXP', 'M2WA', '802079328', NULL, NULL, NULL, NULL, '4N-29-011-14-04', 'JC', NULL, '1,00', '2,00', 'EA', '1', 'G1', 'DLVR FULL to Production', '01/02/2018', 'MRO SUPPLY', NULL, NULL, NULL, NULL, '1', '0,00', 'ATTACH MDR', 4),
(81, '203218', NULL, 'FILTER, OIL', 'EXP', 'M2WA', '802079328', NULL, NULL, NULL, NULL, '4N-29-011-14-04', 'JC', NULL, '1,00', '2,00', 'EA', '1', 'G1', 'DLVR FULL to Production', '01/02/2018', 'MRO SUPPLY', NULL, NULL, NULL, NULL, '1', '0,00', 'ATTACH MDR', 4),
(82, '63B10463-20', NULL, 'KIT, FILTER RESERVOIR PRESS MODULE LH RH MID', 'EXP', 'M2WA', '802079341', NULL, NULL, NULL, NULL, '4N-29-017-01-01', 'JC', NULL, '1,00', '4,00', 'EA', '1', 'G1', 'DLVR FULL to Production', '29/01/2018', 'MRO SUPPLY', NULL, NULL, NULL, NULL, '1', '0,00', 'ATTACH JC', 4),
(83, '63B10463-20', NULL, 'KIT, FILTER RESERVOIR PRESS MODULE LH RH MID', 'EXP', 'M2WA', '802079342', NULL, NULL, NULL, NULL, '4N-29-017-01-02', 'JC', NULL, '1,00', '4,00', 'EA', '1', 'G1', 'DLVR FULL to Production', '29/01/2018', 'MRO SUPPLY', NULL, NULL, NULL, NULL, '1', '0,00', 'ATTACH JC', 4),
(84, '63B10463-20', NULL, 'KIT, FILTER RESERVOIR PRESS MODULE LH RH MID', 'EXP', 'M2WA', '802079343', '601413760', '81562894', 'GLS', NULL, '4N-29-017-01-03', 'JC', NULL, '1,00', '4,00', 'EA', '1', 'G3', 'DLVR FULL to Production', '29/01/2018', 'MRO SUPPLY', NULL, NULL, NULL, NULL, '1', '0,00', 'ATTACH JC', 4),
(85, '63B10463-20', NULL, 'KIT, FILTER RESERVOIR PRESS MODULE LH RH MID', 'EXP', 'M2WA', '802079344', '601414650', '81566155', 'GLS', NULL, '4N-29-017-01-04', 'JC', NULL, '1,00', '4,00', 'EA', '1', 'G3', 'DLVR FULL to Production', '29/01/2018', 'MRO SUPPLY', NULL, NULL, NULL, NULL, '1', '0,00', 'ATTACH JC', 4),
(86, '65-90305-33', NULL, 'KIT,  FILTER HYDRAULIC RESERVOIR FILLING SYST', 'EXP', 'M2WA', '802079345', '601413721', '81562883', '1630702', NULL, '4N-29-018-06-01', 'JC', NULL, '1,00', '1,00', 'EA', '1', 'G4', 'DLVR FULL to Production', '30/01/2018', 'MRO SUPPLY', NULL, NULL, NULL, NULL, '1', '0,00', 'ATTACH JC', 4),
(87, 'NAS1611-211', NULL, 'O\'RING', 'EXP', 'M2WA', '802079346', NULL, NULL, NULL, NULL, '4N-29-021-02-02', 'JC', NULL, '1,00', '1,00', 'EA', '1', 'G1', 'DLVR FULL to Production', '29/01/2018', 'MRO SUPPLY', NULL, NULL, NULL, NULL, '1', '0,00', 'ATTACH JC', 4),
(88, '7553848', NULL, 'ELEMENT ASSY, FILTER', 'EXP', 'M2WA', '802079346', NULL, NULL, NULL, NULL, '4N-29-021-02-02', 'JC', NULL, '1,00', '1,00', 'EA', '1', 'G1', 'DLVR FULL to Production', '29/01/2018', 'MRO SUPPLY', NULL, NULL, NULL, NULL, '1', '0,00', 'ATTACH JC', 4),
(89, 'NAS1611-012', 'NAS1611-012A:80205', 'PACKING', 'EXP', 'M2WA', '802079346', NULL, NULL, NULL, NULL, '4N-29-021-02-02', 'JC', NULL, '1,00', '1,00', 'EA', '1', 'G1', 'DLVR FULL to Production', '29/01/2018', 'MRO SUPPLY', NULL, NULL, NULL, NULL, '1', '0,00', 'ATTACH JC', 4),
(90, 'NAS1611-226', NULL, 'O\'RING', 'EXP', 'M2WA', '802079393', '601414653', '81566156', 'OK', NULL, '4N-32-042-11-01', 'JC', NULL, '1,00', '2,00', 'EA', '1', 'G2', 'DLVR FULL to Production', '31/01/2018', 'MRO SUPPLY', NULL, NULL, NULL, NULL, '1', '0,00', 'ATTACH JC, PART AND JC ON G12', 4),
(91, 'MS28783-4', NULL, 'RETAINER', 'EXP', 'M2WA', '802079393', '601413668', '81562878', '1628473', NULL, '4N-32-042-11-01', 'JC', NULL, '2,00', '4,00', 'EA', '2', 'G2', 'DLVR FULL to Production', '31/01/2018', 'MRO SUPPLY', NULL, NULL, NULL, NULL, '2', '0,00', 'ATTACH JC, PART AND JC ON G12', 4),
(92, 'MS28782-14', NULL, 'RING, BACKUP', 'EXP', 'M2WA', '802079393', '601413667', '81562877', '1628474', NULL, '4N-32-042-11-01', 'JC', NULL, '2,00', '4,00', 'EA', '2', 'G2', 'DLVR FULL to Production', '31/01/2018', 'MRO SUPPLY', NULL, NULL, NULL, NULL, '2', '0,00', 'ATTACH JC, PART AND JC ON G12', 4),
(93, 'NAS1611-116', NULL, 'PACKING', 'EXP', 'M2WA', '802079393', '601413803', '81562900', '1628477', NULL, '4N-32-042-11-01', 'JC', NULL, '1,00', '2,00', 'EA', '1', 'G2', 'DLVR FULL to Production', '31/01/2018', 'MRO SUPPLY', NULL, NULL, NULL, NULL, '1', '0,00', 'ATTACH JC, PART AND JC ON G12', 4),
(94, '7553575', NULL, 'FILTER ELEMENT, OIL', 'EXP', NULL, '802079393', NULL, NULL, NULL, NULL, '4N-32-042-11-01', 'JC', NULL, '1,00', '1,00', 'EA', NULL, NULL, 'RIC / Part on Receiving Area', NULL, 'Sirli. QI 25/01. MRIR 200009412 (Part Not Receive)', '15/01/2018', '450047336', '160-40119564', '21/01/2018', NULL, '1,00', NULL, 4),
(95, 'MS28783-4', NULL, 'RETAINER', 'EXP', 'M2WA', '802079399', '601413668', '81562878', '1628473', NULL, '4N-32-053-08-01', 'JC', NULL, '2,00', '4,00', 'EA', '2', 'G2', 'DLVR FULL to Production', '01/02/2018', 'MRO SUPPLY', NULL, NULL, NULL, NULL, '2', '0,00', 'ATTACH JC', 4),
(96, 'NAS1611-116', NULL, 'PACKING', 'EXP', 'M2WA', '802079399', '601413803', '81562900', '1628477', NULL, '4N-32-053-08-01', 'JC', NULL, '1,00', '2,00', 'EA', '1', 'G2', 'DLVR FULL to Production', '01/02/2018', 'MRO SUPPLY', NULL, NULL, NULL, NULL, '1', '0,00', 'ATTACH JC', 4),
(97, 'MS28782-14', NULL, 'RING, BACKUP', 'EXP', 'M2WA', '802079399', '601413667', '81562877', '1628474', NULL, '4N-32-053-08-01', 'JC', NULL, '2,00', '4,00', 'EA', '2', 'G2', 'DLVR FULL to Production', '01/02/2018', 'MRO SUPPLY', NULL, NULL, NULL, NULL, '2', '0,00', 'ATTACH JC', 4),
(98, 'NAS1611-226', NULL, 'O\'RING', 'EXP', 'M2WA', '802079399', '601414653', '81566156', 'OK', NULL, '4N-32-053-08-01', 'JC', NULL, '1,00', '2,00', 'EA', '1', 'G2', 'DLVR FULL to Production', '01/02/2018', 'MRO SUPPLY', NULL, NULL, NULL, NULL, '1', '0,00', 'ATTACH JC', 4),
(99, '11-10107', '7553575', 'FILTER ELEMENT, OIL', 'EXP', 'M2WA', '802079399', NULL, NULL, NULL, NULL, '4N-32-053-08-01', 'JC', NULL, '1,00', '1,00', 'EA', '1', 'G2', 'DLVR FULL to Production', '01/02/2018', 'Sirliyani. QI', NULL, NULL, NULL, NULL, '1', '0,00', 'ATTACH JC', 4),
(100, '2873-550', 'AS1895-7-550', 'SEAL', 'EXP', 'M2WA', '802079448', NULL, NULL, NULL, NULL, '4N-36-010-01-01', 'JC', NULL, '1,00', '4,00', 'EA', '1', 'G1', 'DLVR FULL to Production', '31/01/2018', '44,21$ from Wencor / MRO supply and buy from Wencor', NULL, NULL, NULL, NULL, '1', '0,00', 'ATTACH JC', 4),
(101, '2873-750', 'AS1895-7-750:U1653', 'SEAL', 'EXP', NULL, '802079448', '601406357', '81578739', '1634519', NULL, '4N-36-010-01-01', 'JC', NULL, '1,00', '4,00', 'EA', '1', 'G-1', 'DLVR FULL to Production', '31/01/2018', 'KLX by Iga', '15/01/2018', '450047356', '160-40118282', '21/01/2018', '1', '0,00', 'ATTACH JC', 4),
(102, '2873-550', 'AS1895-7-550', 'SEAL', 'EXP', 'M2WA', '802079449', NULL, NULL, NULL, NULL, '4N-36-010-01-02', 'JC', NULL, '1,00', '4,00', 'EA', '1', 'G1', 'DLVR FULL to Production', '31/01/2018', '44,21$ from Wencor / MRO supply and buy from Wencor', NULL, NULL, NULL, NULL, '1', '0,00', 'ATTACH JC', 4),
(103, '2873-750', 'AS1895-7-750:U1653', 'SEAL', 'EXP', NULL, '802079449', '601406357', '81578739', '1634519', NULL, '4N-36-010-01-02', 'JC', NULL, '1,00', '4,00', 'EA', '1', 'G1', 'DLVR FULL to Production', '31/01/2018', 'KLX by Iga', '15/01/2018', '450047356', '160-40118282', '21/01/2018', '1', '0,00', 'ATTACH JC', 4),
(104, '2873-750', 'AS1895-7-550', 'SEAL', 'EXP', 'M2WA', '802079450', '601413796', '81578741', 'OK', NULL, '4N-36-010-01-03', 'JC', NULL, '1,00', '4,00', 'EA', NULL, 'G2', 'DLVR FULL to Production', '27/01/2018', '79,14$ from Wencor / MRO supply and buy from Wencor', NULL, NULL, NULL, NULL, '1', '0,00', 'ATTACH JC, PART AND JC ON G12', 4),
(105, '2873-550', 'AS1895-7-550', 'SEAL', 'EXP', 'M2WA', '802079450', NULL, NULL, NULL, NULL, '4N-36-010-01-03', 'JC', NULL, '1,00', '4,00', 'EA', '1', 'G2', 'DLVR FULL to Production', '27/01/2018', '44,21$ from Wencor / MRO supply and buy from Wencor', NULL, NULL, NULL, NULL, '1', '0,00', 'ATTACH JC, PART AND JC ON G12', 4),
(106, '2873-750', 'AS1895-7-750:U1653', 'SEAL', 'EXP', NULL, '802079451', NULL, NULL, NULL, NULL, '4N-36-010-01-04', 'JC', NULL, '1,00', '4,00', 'EA', NULL, 'G2', 'DLVR FULL to Production', '01/02/2018', 'KLX by Iga', '15/01/2018', '450047356', '160-40118282', '21/01/2018', '1', '0,00', 'ATTACH JC, PART AND JC ON G12', 4),
(107, '2873-550', 'AS1895-7-550', 'SEAL', 'EXP', 'M2WA', '802079451', NULL, NULL, NULL, NULL, '4N-36-010-01-04', 'JC', NULL, '1,00', '4,00', 'EA', '1', 'G4', 'DLVR FULL to Production', '01/02/2018', '44,21$ from Wencor / MRO supply and buy from Wencor', NULL, NULL, NULL, NULL, '1', '0,00', 'ATTACH JC, PART AND JC ON G12', 4),
(108, 'R1131-908', 'BACP11K8', 'PACKING', 'EXP', 'M2WA', '802079455', '601413747', '81562888', 'OK', NULL, '4N-36-010-05-01', 'JC', NULL, '4,00', '4,00', 'EA', '4', 'G2', 'PRELOADED in Hangar Store', '27/01/2018', 'MRO SUPPLY', NULL, NULL, NULL, NULL, NULL, '4,00', NULL, 4),
(109, '69494J112', NULL, 'PACKING', 'EXP', 'M2WA', '802079458', '601419484', '81562922', 'GLS', NULL, '4N-36-011-04-01', 'JC', NULL, '1,00', '12,00', 'EA', '1', 'G2', 'DLVR FULL to Production', '29/01/2018', 'MRO SUPPLY', NULL, NULL, NULL, NULL, '1', '0,00', 'ATTACH JC', 4),
(110, '69494J908', 'MS9385-08', 'PACKING', 'EXP', 'M2WA', '802079458', '601413745', '81562887', '1628491', NULL, '4N-36-011-04-01', 'JC', NULL, '1,00', '7,00', 'EA', '1', 'G2', 'DLVR FULL to Production', '29/01/2018', 'MRO SUPPLY', NULL, NULL, NULL, NULL, '1', '0,00', 'ATTACH JC', 4),
(111, '69494J910', NULL, 'O\'RING', 'EXP', NULL, '802079458', '601413746', '81573325', '1628492', NULL, '4N-36-011-04-01', 'JC', NULL, '1,00', '16,00', 'EA', '1', 'G2', 'DLVR FULL to Production', '29/01/2018', 'MRO SUPPLY', '17/01/2018', '450047474', '180-42433086', '21-Dec-18', '1', '0,00', 'ATTACH JC', 4),
(112, '69494J908', 'MS9385-08', 'PACKING', 'EXP', 'M2WA', '802079459', '601413745', '81562887', '1628491', NULL, '4N-36-011-04-02', 'JC', NULL, '2,00', '7,00', 'EA', '2', 'G2', 'DLVR FULL to Production', '29/01/2018', 'MRO SUPPLY', NULL, NULL, NULL, NULL, '2', '0,00', 'ATTACH JC', 4),
(113, '69494J910', NULL, 'O\'RING', 'EXP', NULL, '802079459', '601413746', '81573325', '1628492', NULL, '4N-36-011-04-02', 'JC', NULL, '1,00', '16,00', 'EA', '1', 'G2', 'DLVR FULL to Production', '29/01/2018', 'MRO SUPPLY', '17/01/2018', '450047474', '180-42433086', '21-Dec-18', '1', '0,00', 'ATTACH JC', 4),
(114, '69494J112', NULL, 'PACKING', 'EXP', 'M2WA', '802079459', '601419484', '81562922', 'GLS', NULL, '4N-36-011-04-02', 'JC', NULL, '1,00', '12,00', 'EA', '1', 'G2', 'DLVR FULL to Production', '29/01/2018', 'MRO SUPPLY', NULL, NULL, NULL, NULL, '1', '0,00', 'ATTACH JC', 4),
(115, '69494J910', NULL, 'O\'RING', 'EXP', NULL, '802079460', '601413746', '81573325', '1628492', NULL, '4N-36-011-04-03', 'JC', NULL, '1,00', '16,00', 'EA', '1', 'G2', 'DLVR FULL to Production', '29/01/2018', 'MRO SUPPLY', '17/01/2018', '450047474', '180-42433086', '21-Dec-18', '1', '0,00', 'ATTACH JC', 4),
(116, '69494J908', 'MS9385-08', 'PACKING', 'EXP', 'M2WA', '802079460', '601413745', '81562887', '1628491', NULL, '4N-36-011-04-03', 'JC', NULL, '2,00', '7,00', 'EA', '2', 'G2', 'DLVR FULL to Production', '29/01/2018', 'MRO SUPPLY', NULL, NULL, NULL, NULL, '2', '0,00', 'ATTACH JC', 4),
(117, '69494J112', NULL, 'PACKING', 'EXP', 'M2WA', '802079460', '601419484', '81562922', 'GLS', NULL, '4N-36-011-04-03', 'JC', NULL, '1,00', '12,00', 'EA', '1', 'G2', 'DLVR FULL to Production', '29/01/2018', 'MRO SUPPLY', NULL, NULL, NULL, NULL, '1', '0,00', 'ATTACH JC', 4),
(118, '69494J908', 'MS9385-08', 'PACKING', 'EXP', 'M2WA', '802079461', '601413745', '81562887', '1628491', NULL, '4N-36-011-04-04', 'JC', NULL, '2,00', '7,00', 'EA', '2', 'G2', 'DLVR FULL to Production', '29/01/2018', 'MRO SUPPLY', NULL, NULL, NULL, NULL, '2', '0,00', 'ATTACH JC', 4),
(119, '4', '6634', '<p>\r\n	43434</p>\r\n', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '3434', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4);

-- --------------------------------------------------------

--
-- Table structure for table `m_plant`
--

CREATE TABLE `m_plant` (
  `ID_PLANT` int(3) NOT NULL,
  `PLANT` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_plant`
--

INSERT INTO `m_plant` (`ID_PLANT`, `PLANT`) VALUES
(1, 'Hangar 1 (GAH1**C*)'),
(2, 'Hangar 3 (GAH3**C*)'),
(3, 'Hangar 4 (GAH4**C*)'),
(4, 'Hangar 4 (GAH4**C*)'),
(5, 'Seat Shop 2 (W102)'),
(6, 'Monument 1 (W401)'),
(7, 'Laundry & Sewing (W501 & W502)');

-- --------------------------------------------------------

--
-- Table structure for table `order_list`
--

CREATE TABLE `order_list` (
  `REVISION` varchar(8) NOT NULL,
  `ORDER_NO` varchar(12) NOT NULL,
  `ORDER_TYPE` varchar(4) NOT NULL,
  `DESCRIPTION` varchar(200) NOT NULL,
  `BSC_START` date NOT NULL,
  `BSC_END` date NOT NULL,
  `FUNCT_LOC` varchar(30) NOT NULL,
  `DESC_FUNC_LOC` varchar(200) NOT NULL,
  `MAIN_PLANT` varchar(20) NOT NULL,
  `PLANT` varchar(50) NOT NULL,
  `MATERIAL` varchar(100) NOT NULL,
  `MATERIAL_DESC` varchar(200) NOT NULL,
  `SYSTEM_STATUS` varchar(30) NOT NULL,
  `EWH_AMOUNT` varchar(30) NOT NULL,
  `MOVEMENT_LOC` varchar(20) NOT NULL,
  `PROGRESS_STATUS_ID` int(11) NOT NULL,
  `STATUS_PLANNING` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `order_list`
--

INSERT INTO `order_list` (`REVISION`, `ORDER_NO`, `ORDER_TYPE`, `DESCRIPTION`, `BSC_START`, `BSC_END`, `FUNCT_LOC`, `DESC_FUNC_LOC`, `MAIN_PLANT`, `PLANT`, `MATERIAL`, `MATERIAL_DESC`, `SYSTEM_STATUS`, `EWH_AMOUNT`, `MOVEMENT_LOC`, `PROGRESS_STATUS_ID`, `STATUS_PLANNING`) VALUES
('', '000800028308', 'GA01', 'PORTABLE HALON FIRE EXTINGUISHERS', '0000-00-00', '0000-00-00', 'PK-GIE.24', '', '', '', '', '', '', '', 'Hangar 1 (GAH1**C*)', 1, 0),
('', '000800028311', 'GA01', 'PORTABLE HALON FIRE EXTINGUISHERS', '0000-00-00', '0000-00-00', 'PK-GIE.24', '', '', '', '', '', '', '', 'Hangar 3 (GAH3**C*)', 1, 0),
('', '000800028315', 'GA01', 'PORTABLE HALON FIRE EXTINGUISHERS', '0000-00-00', '0000-00-00', '', '', '', '', '', '', '', '', '', 1, 0),
('', '000800028330', 'GA01', 'PORTABLE HALON FIRE EXTINGUISHERS', '0000-00-00', '0000-00-00', '', '', '', '', '', '', '', '', 'Seat Shop 1 (W101)', 1, 0),
('', '000800028331', 'GA01', 'PORTABLE HALON FIRE EXTINGUISHERS', '0000-00-00', '0000-00-00', '', '', '', '', '', '', '', '', 'Monument 1 (W401)', 1, 0),
('', '000800028449', 'GA01', '', '0000-00-00', '0000-00-00', '', '', '', '', '', '', '', '', '', 1, 0),
('', '000800028651', 'GA01', '', '0000-00-00', '0000-00-00', 'PK-GIA', '', '', '', '', '', '', '', '', 1, 0),
('', '000800028652', 'GA01', '', '0000-00-00', '0000-00-00', 'PK-GIA', '', '', '', '', '', '', '', '', 4, 0),
('', '000800028653', 'GA01', '', '0000-00-00', '0000-00-00', 'PK-GIA', '', '', '', '', '', '', '', '', 5, 0),
('00002594', '000800028668', 'GA01', 'testing', '0000-00-00', '0000-00-00', 'PK-GHW', '', '', '', '', '', '', '', '', 5, 1),
('00002595', '000800028669', 'GA01', 'testing', '0000-00-00', '0000-00-00', 'PK-GHW', '', '', '', '', '', '', '', '', 5, 0),
('', '000800028672', 'GA01', 'd7 notification', '0000-00-00', '0000-00-00', 'PK-GHW', '', '', '', '', '', '', '', '', 6, 0),
('00002597', '000800028674', 'GA02', 'd7 notification', '0000-00-00', '0000-00-00', 'PK-GHW', '', '', '', '', '', '', '', '', 6, 1),
('', '000800028682', 'GA02', '', '0000-00-00', '0000-00-00', 'PK-GEI', '', '', '', 'GE90-85BG11:062W0', 'ENGINE GE90LVE ,*V, TRIM AIR', '', '', '', 6, 0),
('', '000800028685', 'GA01', '', '0000-00-00', '0000-00-00', '', '', '', '', '', '', '', '', '', 6, 0),
('', '000800028698', 'GA01', '', '0000-00-00', '0000-00-00', '', '', '', '', '', '', '', '', '', 6, 0),
('', '000800028699', 'GA02', '', '0000-00-00', '0000-00-00', '', '', '', '', 'GE90-85BG11:062W0', 'ENGINE GE90LVE ,*V, TRIM AIR', '', '', '', 6, 0),
('', '000800028700', 'GA01', '', '0000-00-00', '0000-00-00', '', '', '', '', 'GE90-85BG11:062W0', 'ENGINE GE90LVE ,*V, TRIM AIR', '', '', '', 5, 0),
('', '000800028701', 'GA01', '', '0000-00-00', '0000-00-00', '', '', '', '', 'GE90-85BG11:062W0', 'ENGINE GE90LVE ,*V, TRIM AIR', '', '', '', 6, 0),
('', '000800028702', 'GA01', '', '0000-00-00', '0000-00-00', '', '', '', '', 'GE90-85BG11:062W0', 'ENGINE GE90LVE ,*V, TRIM AIR', '', '', '', 5, 0),
('', '000800028703', 'GA01', '', '0000-00-00', '0000-00-00', '', '', '', '', 'GE90-85BG11:062W0', 'ENGINE GE90LVE ,*V, TRIM AIR', '', '', '', 4, 0),
('50000308', '000800028704', 'GA01', 'HPT ROTOR MODULE FINAL BALANCE', '0000-00-00', '0000-00-00', '', '', '', '', '000000000100016057', 'SHAFT-LPT ASSY', '', '', '', 4, 0),
('00002601', '000800028717', 'GA01', 'OIl replacement', '0000-00-00', '0000-00-00', 'PK-GIA', '', '', '', '', '', '', '', '', 1, 1),
('00002601', '000800028718', 'GA02', 'Prepare for Brake Application Test.', '0000-00-00', '0000-00-00', 'PK-GIA', '', '', '', '', '', '', '', '', 2, 1),
('00002601', '000800028719', 'GA02', '', '0000-00-00', '0000-00-00', 'PK-GIA', '', '', '', '', '', '', '', '', 3, 1),
('50000316', '000800028727', 'GA01', 'Hello 1', '0000-00-00', '0000-00-00', '', '', '', '', '000000000100016048', 'STATOR ASSY-CPRSR FRONT', '', '', '', 6, 0),
('', '000800028728', 'GA01', 'Hello 1', '0000-00-00', '0000-00-00', '', '', '', '', '000000000100016040', 'rotable washer', '', '', '', 6, 0),
('50000316', '000800028729', 'GA01', 'Hello 1', '0000-00-00', '0000-00-00', '', '', '', '', '000000000100016059', 'GEARBOX-INLET NO3 BEARING ASSY', '', '', '', 3, 0),
('50000316', '000800028730', 'GA01', 'Hello 1', '0000-00-00', '0000-00-00', '', '', '', '', '000000000100016067', 'EXCITER-IGNITION UNIT', '', '', '', 3, 0),
('50000316', '000800028731', 'GA01', 'Hello 1', '0000-00-00', '0000-00-00', '', '', '', '', '000000000100016067', 'EXCITER-IGNITION UNIT', '', '', '', 3, 0),
('50000316', '000800028732', 'GA01', 'Hello 1', '0000-00-00', '0000-00-00', '', '', '', '', '000000000100016048', 'STATOR ASSY-CPRSR FRONT', '', '', '', 3, 0),
('50000316', '000800028733', 'GA01', 'Hello 1', '0000-00-00', '0000-00-00', '', '', '', '', '000000000100016067', 'EXCITER-IGNITION UNIT', '', '', '', 4, 0),
('50000316', '000800028734', 'GA01', 'adasd', '0000-00-00', '0000-00-00', '', '', '', '', '000000000100016067', 'EXCITER-IGNITION UNIT', '', '', '', 4, 0),
('50000316', '000800028735', 'GA01', 'MAT for AD/SB', '0000-00-00', '0000-00-00', '', '', '', '', '000000000100016064', 'PROBE-PT25', '', '', '', 4, 0),
('50000316', '000800028738', 'GA01', 'COUNTER 1', '0000-00-00', '0000-00-00', '', '', '', '', '000000000100016052', 'COMBUSTION ASSY', '', '', '', 4, 0),
('', '000800028739', 'GA01', 'reference order', '0000-00-00', '0000-00-00', 'PK-GXA', '', '', '', '000000000100016067', 'EXCITER-IGNITION UNIT', '', '', '', 2, 0),
('00002606', '000800028741', 'GA01', 'OIl replacement', '0000-00-00', '0000-00-00', 'PK-GAA', '', '', '', '', '', '', '', '', 2, 0),
('00002608', '000800028744', 'GA02', 'OIl replacement', '0000-00-00', '0000-00-00', 'PK-GAA', '', '', '', '', '', '', '', '', 1, 1),
('00002609', '000800028745', 'GA02', 'OIl replacement', '0000-00-00', '0000-00-00', 'PK-GAA', '', '', '', '', '', '', '', '', 1, 0),
('00002610', '000800028747', 'GA01', 'OIl replacement', '0000-00-00', '0000-00-00', 'PK-GAA', '', '', '', '', '', '', '', '', 7, 0),
('00002611', '000800028748', 'GA01', 'OIl replacement', '0000-00-00', '0000-00-00', 'PK-GAA', '', '', '', '', '', '', '', '', 7, 0),
('00002612', '000800028749', 'GA01', 'OIl replacement', '0000-00-00', '0000-00-00', 'PK-GAA', '', '', '', '', '', '', '', '', 7, 0),
('00002613', '000800028751', 'GA01', 'OIl replacement', '0000-00-00', '0000-00-00', 'PK-GAA', '', '', '', '', '', '', '', '', 7, 1),
('', '000800028752', 'GA01', '', '0000-00-00', '0000-00-00', 'PK-GAA', '', '', '', '', '', '', '', '', 7, 0),
('00002614', '000800028753', 'GA01', 'OIl replacement', '0000-00-00', '0000-00-00', 'PK-GHW', '', '', '', '', '', '', '', '', 5, 0),
('00002615', '000800028754', 'GA02', 'OIl replacement', '0000-00-00', '0000-00-00', 'PK-GHW', '', '', '', '', '', '', '', '', 5, 0),
('00002616', '000800028755', 'GA02', 'OIl replacement', '0000-00-00', '0000-00-00', 'PK-GHW', '', '', '', '', '', '', '', '', 5, 0),
('00002617', '000800028757', 'GA02', 'OIl replacement', '0000-00-00', '0000-00-00', 'PK-GHW', '', '', '', '', '', '', '', '', 4, 0),
('00002618', '000800028759', 'GA02', 'OIl replacement', '0000-00-00', '0000-00-00', 'PK-GHW', '', '', '', '', '', '', '', '', 4, 0),
('00002619', '000800028760', 'GA02', 'OIl replacement', '0000-00-00', '0000-00-00', 'PK-GHW', '', '', '', '', '', '', '', '', 4, 0),
('00002620', '000800028761', 'GA01', 'OIl replacement', '0000-00-00', '0000-00-00', 'PK-GHW', '', '', '', '', '', '', '', '', 7, 0),
('00002621', '000800028764', 'GA02', 'OIl replacement', '0000-00-00', '0000-00-00', 'PK-GHW', '', '', '', '', '', '', '', '', 7, 0),
('00002622', '000800028766', 'GA02', 'OIl replacement', '0000-00-00', '0000-00-00', 'PK-GHX', '', '', '', '', '', '', '', '', 7, 0),
('00002623', '000800028769', 'GA02', 'OIl replacement', '0000-00-00', '0000-00-00', 'PK-GAE', '', '', '', '', '', '', '', '', 7, 0),
('00002624', '000800028770', 'GA02', 'OIl replacement', '0000-00-00', '0000-00-00', 'PK-GAE', '', '', '', '', '', '', '', '', 7, 0),
('', '000800028775', 'GA01', '', '0000-00-00', '0000-00-00', '', '', '', '', '', '', '', '', '', 0, 0),
('', '000800028776', 'GA01', '', '0000-00-00', '0000-00-00', '', '', '', '', '', '', '', '', '', 0, 0),
('', '000800028781', 'GA01', 'OIl replacement', '0000-00-00', '0000-00-00', 'PK-GHX', '', '', '', '', '', '', '', '', 0, 0),
('', '000800028783', 'GA01', 'OIl replacement', '0000-00-00', '0000-00-00', 'PK-GHX', '', '', '', '', '', '', '', '', 0, 0),
('00002625', '000800028786', 'GA01', 'notification', '0000-00-00', '0000-00-00', 'PK-GHX', '', '', '', '', '', '', '', '', 0, 0),
('00002627', '000800028790', 'GA01', 'OIl replacement', '0000-00-00', '0000-00-00', 'PK-GAD', '', '', '', '', '', '', '', '', 0, 0),
('00002628', '000800028793', 'GA01', 'OIl replacement', '0000-00-00', '0000-00-00', 'PK-GAF', '', '', '', '', '', '', '', '', 0, 0),
('00002629', '000800028795', 'GA01', 'OIl replacement', '0000-00-00', '0000-00-00', 'PK-GIF', '', '', '', '', '', '', '', '', 0, 0),
('00002630', '000800028797', 'GA01', 'OIl replacement', '0000-00-00', '0000-00-00', 'PK-GAA', '', '', '', '', '', '', '', '', 0, 0),
('00002631', '000800028798', 'GA01', 'OIl replacement', '0000-00-00', '0000-00-00', 'PK-GAA', '', '', '', '', '', '', '', '', 0, 0),
('00002632', '000800028799', 'GA01', 'OIl replacement', '0000-00-00', '0000-00-00', 'PK-GAA', '', '', '', '', '', '', '', '', 0, 0),
('50000318', '000800028800', 'GA01', 'Hello 1', '0000-00-00', '0000-00-00', '', '', '', '', '000000000100016040', 'rotable washer', '', '', '', 0, 0),
('50000318', '000800028801', 'GA01', 'Hello 1', '0000-00-00', '0000-00-00', '', '', '', '', '000000000100016059', 'GEARBOX-INLET NO3 BEARING ASSY', '', '', '', 0, 0),
('50000318', '000800028802', 'GA01', 'Hello 1', '0000-00-00', '0000-00-00', '', '', '', '', '000000000100016067', 'EXCITER-IGNITION UNIT', '', '', '', 0, 0),
('50000318', '000800028803', 'GA01', 'Hello 1', '0000-00-00', '0000-00-00', '', '', '', '', '000000000100016067', 'EXCITER-IGNITION UNIT', '', '', '', 0, 0),
('50000318', '000800028804', 'GA01', 'Hello 1', '0000-00-00', '0000-00-00', '', '', '', '', '000000000100016048', 'STATOR ASSY-CPRSR FRONT', '', '', '', 0, 0),
('50000318', '000800028805', 'GA01', 'Hello 1', '0000-00-00', '0000-00-00', '', '', '', '', '000000000100016067', 'EXCITER-IGNITION UNIT', '', '', '', 0, 0),
('50000318', '000800028806', 'GA01', 'adasd', '0000-00-00', '0000-00-00', '', '', '', '', '000000000100016067', 'EXCITER-IGNITION UNIT', '', '', '', 0, 0),
('50000318', '000800028807', 'GA01', 'MAT for AD/SB', '0000-00-00', '0000-00-00', '', '', '', '', '000000000100016064', 'PROBE-PT25', '', '', '', 0, 0),
('00002633', '000800028810', 'GA01', 'OIl replacement', '0000-00-00', '0000-00-00', 'PK-GAD', '', '', '', '', '', '', '', '', 0, 0),
('00002634', '000800028811', 'GA01', 'OIl replacement', '0000-00-00', '0000-00-00', 'PK-GIF', '', '', '', '', '', '', '', '', 0, 0),
('00002635', '000800028812', 'GA01', 'OIl replacement', '0000-00-00', '0000-00-00', 'PK-GAF', '', '', '', '', '', '', '', '', 0, 0),
('00002636', '000800028813', 'GA01', 'OIl replacement', '0000-00-00', '0000-00-00', 'PK-GHA', '', '', '', '', '', '', '', '', 0, 0),
('', '000800028814', 'GA01', '', '0000-00-00', '0000-00-00', '', '', '', '', '000000000100016064', 'PROBE-PT25', '', '', '', 0, 0),
('', '000800028815', 'GA01', '', '0000-00-00', '0000-00-00', '', '', '', '', '', '', '', '', '', 0, 0),
('', '000800028816', 'GA01', '', '0000-00-00', '0000-00-00', '', '', '', '', '000000000100016064', 'PROBE-PT25', '', '', '', 0, 0),
('', '000800028817', 'GA01', '', '0000-00-00', '0000-00-00', '', '', '', '', '', '', '', '', '', 0, 0),
('', '000800028818', 'GA01', '', '0000-00-00', '0000-00-00', '', '', '', '', '', '', '', '', '', 0, 0),
('50000316', '000800028824', 'GA01', 'FRAME-FAN_DISASSEMBLY', '0000-00-00', '0000-00-00', '', '', '', '', '000000000100016040', 'rotable washer', '', '', '', 0, 0),
('', '000800028825', 'GA01', '', '0000-00-00', '0000-00-00', '', '', '', '', '000000000100016040', 'rotable washer', '', '', '', 0, 0),
('00002637', '000800028828', 'GA01', 'OIl replacement', '0000-00-00', '0000-00-00', 'PK-GAA', '', '', '', '', '', '', '', '', 0, 0),
('00002638', '000800028829', 'GA01', 'OIl replacement', '0000-00-00', '0000-00-00', 'PK-GAA', '', '', '', '', '', '', '', '', 0, 0),
('00002639', '000800028830', 'GA01', 'OIl replacement', '0000-00-00', '0000-00-00', 'PK-GAA', '', '', '', '', '', '', '', '', 0, 0),
('00002640', '000800028831', 'GA01', 'OIl replacement', '0000-00-00', '0000-00-00', 'PK-GAA', '', '', '', '', '', '', '', '', 0, 0),
('00002641', '000800028832', 'GA01', 'OIl replacement', '0000-00-00', '0000-00-00', 'PK-GAA', '', '', '', '', '', '', '', '', 0, 0),
('00002642', '000800028833', 'GA01', 'OIl replacement', '0000-00-00', '0000-00-00', 'PK-GAA', '', '', '', '', '', '', '', '', 0, 0),
('00002643', '000800028834', 'GA01', 'OIl replacement', '0000-00-00', '0000-00-00', 'PK-GAA', '', '', '', '', '', '', '', '', 0, 0),
('00002644', '000800028835', 'GA01', 'OIl replacement', '0000-00-00', '0000-00-00', 'PK-GAA', '', '', '', '', '', '', '', '', 0, 0),
('00002645', '000800028836', 'GA01', 'OIl replacement', '0000-00-00', '0000-00-00', 'PK-GAA', '', '', '', '', '', '', '', '', 0, 0),
('00002646', '000800028837', 'GA01', 'OIl replacement', '0000-00-00', '0000-00-00', 'PK-GAA', '', '', '', '', '', '', '', '', 0, 0),
('00002647', '000800028838', 'GA01', 'OIl replacement', '0000-00-00', '0000-00-00', 'PK-GAA', '', '', '', '', '', '', '', '', 0, 0),
('00002648', '000800028839', 'GA01', 'OIl replacement', '0000-00-00', '0000-00-00', 'PK-GAA', '', '', '', '', '', '', '', '', 0, 0),
('00002649', '000800028840', 'GA01', 'OIl replacement', '0000-00-00', '0000-00-00', 'PK-GAA', '', '', '', '', '', '', '', '', 0, 0),
('00002650', '000800028843', 'GA01', 'OIl replacement', '0000-00-00', '0000-00-00', 'PK-GAA', '', '', '', '', '', '', '', '', 0, 0),
('00002651', '000800028845', 'GA01', 'OIl replacement', '0000-00-00', '0000-00-00', 'PK-GAA', '', '', '', '', '', '', '', '', 0, 0),
('00002652', '000800028847', 'GA01', 'OIl replacement', '0000-00-00', '0000-00-00', 'PK-GAE', '', '', '', '', '', '', '', '', 0, 0),
('00002653', '000800028848', 'GA01', 'OIl replacement', '0000-00-00', '0000-00-00', 'PK-GAE', '', '', '', '', '', '', '', '', 0, 0),
('00002654', '000800028849', 'GA01', 'OIl replacement', '0000-00-00', '0000-00-00', 'PK-GAE', '', '', '', '', '', '', '', '', 0, 0),
('00002655', '000800028850', 'GA01', 'OIl replacement', '0000-00-00', '0000-00-00', 'PK-GAE', '', '', '', '', '', '', '', '', 0, 0),
('50000316', '000800028853', 'GA01', 'TL FOR GROUP COUNTER VALUE CHECK 2', '0000-00-00', '0000-00-00', '', '', '', '', '000000000100016056', 'LPT MODULE', '', '', '', 0, 0),
('00002656', '000800028854', 'GA01', 'OIl replacement', '0000-00-00', '0000-00-00', 'PK-GAA', '', '', '', '', '', '', '', '', 0, 0),
('50000316', '000800028855', 'GA01', 'GEARBOX ASSY-TRANSFER', '0000-00-00', '0000-00-00', '', '', '', '', '000000000100016054', 'ROTOR ASSY-HP TURBINE', '', '', '', 0, 0),
('00002657', '000800028856', 'GA01', 'OIl replacement', '0000-00-00', '0000-00-00', 'PK-GAA', '', '', '', '', '', '', '', '', 0, 0),
('00002658', '000800028858', 'GA01', 'OIl replacement', '0000-00-00', '0000-00-00', 'PK-GAA', '', '', '', '', '', '', '', '', 0, 0),
('00002659', '000800028859', 'GA01', 'OIl replacement', '0000-00-00', '0000-00-00', 'PK-GAA', '', '', '', '', '', '', '', '', 0, 0),
('50000315', '000800028860', 'GA01', 'INSTAL AIR DUCTOIL SEAL', '0000-00-00', '0000-00-00', '', '', '', '', '000000000100016040', 'rotable washer', '', '', '', 0, 0),
('50000315', '000800028862', 'GA01', 'TL FOR GROUP COUNTER VALUE CHECK 2', '0000-00-00', '0000-00-00', '', '', '', '', '000000000100016056', 'LPT MODULE', '', '', '', 0, 0),
('00002660', '000800028863', 'GA01', 'OIl replacement', '0000-00-00', '0000-00-00', 'PK-GHX', '', '', '', '', '', '', '', '', 0, 0),
('50000315', '000800028865', 'GA01', 'CODE BASED TL', '0000-00-00', '0000-00-00', '', '', '', '', '000000000100016064', 'PROBE-PT25', '', '', '', 0, 0),
('50000315', '000800028872', 'GA01', 'HPT ROTOR MODULE FINAL BALANCE', '0000-00-00', '0000-00-00', '', '', '', '', '000000000100018604', 'SLIDE RAFT', '', '', '', 0, 0),
('50000315', '000800028874', 'GA01', 'HPT ROTOR MODULE FINAL BALANCE', '0000-00-00', '0000-00-00', '', '', '', '', '000000000100016057', 'SHAFT-LPT ASSY', '', '', '', 0, 0),
('50000315', '000800028875', 'GA01', 'CODE BASED TL', '0000-00-00', '0000-00-00', '', '', '', '', '000000000100019215', 'SLIDE RAFT', '', '', '', 0, 0),
('50000315', '000800028876', 'GA01', 'CODE BASED TL', '0000-00-00', '0000-00-00', '', '', '', '', '000000000100018598', 'SLIDE RAFT', '', '', '', 0, 0),
('50000312', '000800028878', 'GA01', 'COUNTER 1', '0000-00-00', '0000-00-00', '', '', '', '', '000000000100016067', 'EXCITER-IGNITION UNIT', '', '', '', 0, 0),
('50000314', '000800028879', 'GA01', 'COUNTER 1', '0000-00-00', '0000-00-00', '', '', '', '', '000000000100016067', 'EXCITER-IGNITION UNIT', '', '', '', 0, 0),
('50000320', '000800028881', 'GA01', 'COUNTER 1', '0000-00-00', '0000-00-00', '', '', '', '', '000000000100016067', 'EXCITER-IGNITION UNIT', '', '', '', 0, 0),
('50000320', '000800028882', 'GA01', 'INSTAL AIR DUCTOIL SEAL', '0000-00-00', '0000-00-00', '', '', '', '', '000000000100016078', 'SHROUD-CENTERING', '', '', '', 0, 0),
('50000320', '000800028883', 'GA01', 'FRAME-FAN_DISASSEMBLY', '0000-00-00', '0000-00-00', '', '', '', '', '000000000100016040', 'rotable washer', '', '', '', 0, 0),
('50000320', '000800028884', 'GA01', 'INSTAL AIR DUCTOIL SEAL', '0000-00-00', '0000-00-00', '', '', '', '', '000000000100016094', 'NOZZLE SEG ASSY-LPT STG 2', '', '', '', 0, 0),
('50000320', '000800028885', 'GA01', 'HPT ROTOR MODULE FINAL BALANCE', '0000-00-00', '0000-00-00', '', '', '', '', '000000000100016057', 'SHAFT-LPT ASSY', '', '', '', 0, 0),
('50000312', '000800028886', 'GA01', 'COUNTER 28/11', '0000-00-00', '0000-00-00', '', '', '', '', '000000000100016057', 'SHAFT-LPT ASSY', '', '', '', 0, 0),
('', '000800028887', 'GA01', 'Final process 1', '0000-00-00', '0000-00-00', '', '', '', '', '000000000100016082', 'ENGINE ASSY - TURBOFAN CFM56 3B', '', '', '', 0, 0),
('50000320', '000800028888', 'GA01', 'COUNTER 1', '0000-00-00', '0000-00-00', '', '', '', '', '000000000100016067', 'EXCITER-IGNITION UNIT', '', '', '', 0, 0),
('50000320', '000800028889', 'GA01', 'COUNTER 1', '0000-00-00', '0000-00-00', '', '', '', '', '000000000100016094', 'NOZZLE SEG ASSY-LPT STG 2', '', '', '', 0, 0),
('50000312', '000800028890', 'GA01', 'Final process 1', '0000-00-00', '0000-00-00', '', '', '', '', '000000000100016082', 'ENGINE ASSY - TURBOFAN CFM56 3B', '', '', '', 0, 0),
('50000320', '000800028891', 'GA01', 'CODE BASED TL', '0000-00-00', '0000-00-00', '', '', '', '', '000000000100016064', 'PROBE-PT25', '', '', '', 0, 0),
('50000320', '000800028893', 'GA01', 'COUNTER 28/11', '0000-00-00', '0000-00-00', '', '', '', '', '000000000100016057', 'SHAFT-LPT ASSY', '', '', '', 0, 0),
('50000320', '000800028894', 'GA01', 'COUNTER 28/11', '0000-00-00', '0000-00-00', '', '', '', '', '000000000100016057', 'SHAFT-LPT ASSY', '', '', '', 0, 0),
('50000315', '000800028895', 'GA01', 'TEST', '0000-00-00', '0000-00-00', '', '', '', '', '000000000100018646', 'SLIDE RAFT', '', '', '', 0, 0),
('', '000800028918', 'GA01', '', '0000-00-00', '0000-00-00', '', '', '', '', '000000000100016057', 'SHAFT-LPT ASSY', '', '', '', 0, 0),
('', '000800028919', 'GA01', '', '0000-00-00', '0000-00-00', '', '', '', '', '000000000100016057', 'SHAFT-LPT ASSY', '', '', '', 0, 0),
('', '000800028920', 'GA01', '', '0000-00-00', '0000-00-00', 'PK-GHW', '', '', '', '000000000100016057', 'SHAFT-LPT ASSY', '', '', '', 0, 0),
('50000320', '000800028921', 'GA01', 'COUNTER 1', '0000-00-00', '0000-00-00', '', '', '', '', '000000000100016052', 'COMBUSTION ASSY', '', '', '', 0, 0),
('50000320', '000800028922', 'GA01', 'GEARBOX ASSY-TRANSFER', '0000-00-00', '0000-00-00', '', '', '', '', '000000000100016054', 'ROTOR ASSY-HP TURBINE', '', '', '', 0, 0),
('50000320', '000800028923', 'GA01', 'HPT ROTOR MODULE FINAL BALANCE', '0000-00-00', '0000-00-00', '', '', '', '', '000000000100016057', 'SHAFT-LPT ASSY', '', '', '', 0, 0),
('50000320', '000800028925', 'GA01', 'COUNTER 28/11', '0000-00-00', '0000-00-00', '', '', '', '', '000000000100016052', 'COMBUSTION ASSY', '', '', '', 0, 0),
('50000320', '000800028926', 'GA01', 'HELLO 1', '0000-00-00', '0000-00-00', '', '', '', '', '000000000100016059', 'GEARBOX-INLET NO3 BEARING ASSY', '', '', '', 0, 0),
('50000323', '000800028928', 'GA01', 'COUNTER 1', '0000-00-00', '0000-00-00', '', '', '', '', '000000000100016067', 'EXCITER-IGNITION UNIT', '', '', '', 0, 0),
('50000323', '000800028929', 'GA01', 'NRT TL', '0000-00-00', '0000-00-00', '', '', '', '', '000000000100016082', 'ENGINE ASSY - TURBOFAN CFM56 3B', '', '', '', 0, 0),
('50000323', '000800028934', 'GA01', 'CM-CR testing 21', '0000-00-00', '0000-00-00', '', '', '', '', '000000000100016052', 'COMBUSTION ASSY', '', '', '', 0, 0),
('50000323', '000800028935', 'GA01', 'CM-CR testing 21', '0000-00-00', '0000-00-00', '', '', '', '', '000000000100016082', 'ENGINE ASSY - TURBOFAN CFM56 3B', '', '', '', 0, 0),
('50000323', '000800028936', 'GA01', 'CODE BASED TL', '0000-00-00', '0000-00-00', '', '', '', '', '000000000100016064', 'PROBE-PT25', '', '', '', 0, 0),
('50000326', '000800028937', 'GA01', 'CM-CR testing 21', '0000-00-00', '0000-00-00', '', '', '', '', '000000000100016082', 'ENGINE ASSY - TURBOFAN CFM56 3B', '', '', '', 0, 0),
('50000326', '000800028945', 'GA01', 'MAT for AD/SB', '0000-00-00', '0000-00-00', '', '', '', '', '000000000100016064', 'PROBE-PT25', '', '', '', 0, 0),
('', '000800028947', 'GA01', 'CODE BASED TL', '0000-00-00', '0000-00-00', '', '', '', '', '000000000100016064', 'PROBE-PT25', '', '', '', 0, 0),
('', '000800028948', 'GA01', 'HPT ROTOR MODULE FINAL BALANCE', '0000-00-00', '0000-00-00', '', '', '', '', '000000000100016057', 'SHAFT-LPT ASSY', '', '', '', 0, 0),
('', '000800028949', 'GA01', '', '0000-00-00', '0000-00-00', '', '', '', '', '000000000100016057', 'SHAFT-LPT ASSY', '', '', '', 0, 0),
('', '000800028951', 'GA01', 'COUNTER 1', '0000-00-00', '0000-00-00', '', '', '', '', '000000000100016067', 'EXCITER-IGNITION UNIT', '', '', '', 0, 0),
('', '000800028952', 'GA01', '', '0000-00-00', '0000-00-00', '', '', '', '', '000000000100016067', 'EXCITER-IGNITION UNIT', '', '', '', 0, 0),
('', '000800028953', 'GA01', '', '0000-00-00', '0000-00-00', '', '', '', '', '000000000100016067', 'EXCITER-IGNITION UNIT', '', '', '', 0, 0),
('', '000800028968', 'GA01', 'TEST SLAT PRIMARY CONTROL VALVE. Long. t', '0000-00-00', '0000-00-00', '', '', '', '', '3822483-1:99193', '1ST STAGE COMP IMPELLER', '', '', '', 0, 0),
('', '000800028982', 'GA01', '', '0000-00-00', '0000-00-00', 'PK-GIA', '', '', '', '', '', '', '', '', 0, 0),
('50000304', '000800028985', 'GA01', 'HPT ROTOR MODULE FINAL BALANCE', '0000-00-00', '0000-00-00', '', '', '', '', '000000000100016057', 'SHAFT-LPT ASSY', '', '', '', 0, 0),
('50000304', '000800028986', 'GA01', 'HPT ROTOR MODULE FINAL BALANCE', '0000-00-00', '0000-00-00', '', '', '', '', '000000000100016054', 'ROTOR ASSY-HP TURBINE', '', '', '', 0, 0),
('50000304', '000800028993', 'GA01', 'TEST 123', '0000-00-00', '0000-00-00', '', '', '', '', '000000000100016056', 'LPT MODULE', '', '', '', 0, 0),
('', '000800028994', 'GA01', 'Reference Order', '0000-00-00', '0000-00-00', 'PK-GXA', '', '', '', '000000000100016056', 'LPT MODULE', '', '', '', 0, 0),
('', '000800028996', 'GA01', 'HPT ROTOR MODULE FINAL BALANCE', '0000-00-00', '0000-00-00', '', '', '', '', '000000000100016054', 'ROTOR ASSY-HP TURBINE', '', '', '', 0, 0),
('', '000800028997', 'GA01', 'CODE BASED TL', '0000-00-00', '0000-00-00', '', '', '', '', '000000000100016064', 'PROBE-PT25', '', '', '', 0, 0),
('', '000800028998', 'GA01', 'COUNTER 1', '0000-00-00', '0000-00-00', '', '', '', '', '000000000100016067', 'EXCITER-IGNITION UNIT', '', '', '', 0, 0),
('50000314', '000800028999', 'GA01', 'HPT ROTOR MODULE FINAL BALANCE', '0000-00-00', '0000-00-00', '', '', '', '', '000000000100016057', 'SHAFT-LPT ASSY', '', '', '', 0, 0),
('50000320', '000800029000', 'GA01', 'CODE BASED TL', '0000-00-00', '0000-00-00', '', '', '', '', '000000000100016064', 'PROBE-PT25', '', '', '', 0, 0),
('50000312', '000800029001', 'GA01', 'COUNTER 28/11', '0000-00-00', '0000-00-00', '', '', '', '', '000000000100016052', 'COMBUSTION ASSY', '', '', '', 0, 0),
('', '000800029002', 'GA01', '', '0000-00-00', '0000-00-00', '', '', '', '', '000000000100016094', 'NOZZLE SEG ASSY-LPT STG 2', '', '', '', 0, 0),
('', '000800029004', 'GA01', 'test', '0000-00-00', '0000-00-00', 'PK-GXA', '', '', '', '000000000100016094', 'NOZZLE SEG ASSY-LPT STG 2', '', '', '', 0, 0),
('', '000800029005', 'GA01', '', '0000-00-00', '0000-00-00', 'PK-GXA', '', '', '', '000000000100016094', 'NOZZLE SEG ASSY-LPT STG 2', '', '', '', 0, 0),
('', '000800029006', 'GA01', '', '0000-00-00', '0000-00-00', '', '', '', '', '000000000100016057', 'SHAFT-LPT ASSY', '', '', '', 0, 0),
('00002662', '000800029008', 'GA01', '', '0000-00-00', '0000-00-00', 'PK-GAE.72.00.E1', '', '', '', 'GE90-85BG11:062W0', 'ENGINE GE90LVE ,*V, TRIM AIR', '', '', '', 0, 0),
('00002663', '000800029009', 'GA01', 'TEST SLAT PRIMARY CONTROL VALVE. Long. t', '0000-00-00', '0000-00-00', 'PK-GAE.72.00.E1', '', '', '', 'GE90-85BG11:062W0', 'ENGINE GE90LVE ,*V, TRIM AIR', '', '', '', 0, 0),
('00002664', '000800029010', 'GA01', 'TEST SLAT PRIMARY CONTROL VALVE. Long. t', '0000-00-00', '0000-00-00', 'PK-GAE.72.00.E1', '', '', '', 'GE90-85BG11:062W0', 'ENGINE GE90LVE ,*V, TRIM AIR', '', '', '', 0, 0),
('00002665', '000800029011', 'GA01', 'TEST SLAT PRIMARY CONTROL VALVE. Long. t', '0000-00-00', '0000-00-00', 'PK-GAE.72.00.E1', '', '', '', 'GE90-85BG11:062W0', 'ENGINE GE90LVE ,*V, TRIM AIR', '', '', '', 0, 0),
('00002666', '000800029012', 'GA01', 'TEST SLAT PRIMARY CONTROL VALVE. Long. t', '0000-00-00', '0000-00-00', 'PK-GAE.72.00.E1', '', '', '', 'GE90-85BG11:062W0', 'ENGINE GE90LVE ,*V, TRIM AIR', '', '', '', 0, 0),
('00002667', '000800029013', 'GA01', 'TEST SLAT PRIMARY CONTROL VALVE. Long. t', '0000-00-00', '0000-00-00', 'PK-GAE.72.00.E1', '', '', '', 'GE90-85BG11:062W0', 'ENGINE GE90LVE ,*V, TRIM AIR', '', '', '', 0, 0),
('00002668', '000800029014', 'GA01', 'TEST SLAT PRIMARY CONTROL VALVE. Long. t', '0000-00-00', '0000-00-00', 'PK-GAE.72.00.E1', '', '', '', 'GE90-85BG11:062W0', 'ENGINE GE90LVE ,*V, TRIM AIR', '', '', '', 0, 0),
('00002669', '000800029015', 'GA01', 'TEST SLAT PRIMARY CONTROL VALVE. Long. t', '0000-00-00', '0000-00-00', 'PK-GAE.72.00.E1', '', '', '', 'GE90-85BG11:062W0', 'ENGINE GE90LVE ,*V, TRIM AIR', '', '', '', 0, 0),
('00002670', '000800029016', 'GA01', 'TEST SLAT PRIMARY CONTROL VALVE. Long. t', '0000-00-00', '0000-00-00', 'PK-GAE.72.00.E1', '', '', '', 'GE90-85BG11:062W0', 'ENGINE GE90LVE ,*V, TRIM AIR', '', '', '', 0, 0),
('', '000800029017', 'GA01', 'Reference Order', '0000-00-00', '0000-00-00', 'PK-GIA', '', '', '', '000000000100018646', 'SLIDE RAFT', '', '', '', 0, 0),
('', '000800029020', 'GA01', '', '0000-00-00', '0000-00-00', '', '', '', '', '000000000100018646', 'SLIDE RAFT', '', '', '', 0, 0),
('', '000800029028', 'GA01', '', '0000-00-00', '0000-00-00', '', '', '', '', '000000000100018646', 'SLIDE RAFT', '', '', '', 0, 0),
('', '000800029029', 'GA01', '', '0000-00-00', '0000-00-00', '', '', '', '', '000000000100018646', 'SLIDE RAFT', '', '', '', 0, 0),
('00002678', '000800029035', 'GA01', 'OFF-WING INFLATION CYLINDER PRESSURE', '0000-00-00', '0000-00-00', 'AK-GIA', '', '', '', '', '', '', '', '', 0, 0),
('00002679', '000800029036', 'GA01', 'OFF-WING INFLATION CYLINDER PRESSURE', '0000-00-00', '0000-00-00', 'AK-GHW', '', '', '', '', '', '', '', '', 0, 0),
('', '000800029037', 'GA01', '', '0000-00-00', '0000-00-00', '', '', '', '', '000000000100016064', 'PROBE-PT25', '', '', '', 0, 0),
('', '000800029038', 'GA01', 'reference order', '0000-00-00', '0000-00-00', 'PK-GXA', '', '', '', '000000000100016064', 'PROBE-PT25', '', '', '', 0, 0),
('', '000800029043', 'GA01', '', '0000-00-00', '0000-00-00', '', '', '', '', '000000000100016064', 'PROBE-PT25', '', '', '', 0, 0),
('00002683', '000800029044', 'GA01', 'OIl replacement', '0000-00-00', '0000-00-00', '', '', '', '', '3822483-1:99193', '1ST STAGE COMP IMPELLER', '', '', '', 0, 0),
('00002683', '000800029045', 'GA01', 'Prepare for Brake Application Test.', '0000-00-00', '0000-00-00', '', '', '', '', '3822483-1:99193', '1ST STAGE COMP IMPELLER', '', '', '', 0, 0),
('', '000800029048', 'GA01', '', '0000-00-00', '0000-00-00', '', '', '', '', '000000000100018591', 'SLIDE RAFT', '', '', '', 0, 0),
('50000304', '000800029050', 'GA01', 'HPT ROTOR MODULE FINAL BALANCE', '0000-00-00', '0000-00-00', '', '', '', '', '000000000100016057', 'SHAFT-LPT ASSY', '', '', '', 0, 0),
('', '000800029052', 'GA01', '', '0000-00-00', '0000-00-00', '', '', '', '', '000000000100016057', 'SHAFT-LPT ASSY', '', '', '', 0, 0),
('', '000800029053', 'GA01', '', '0000-00-00', '0000-00-00', '', '', '', '', '000000000100016057', 'SHAFT-LPT ASSY', '', '', '', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `progress_status`
--

CREATE TABLE `progress_status` (
  `ID_PROGRESS_STATUS` int(11) NOT NULL,
  `PROGRESS_STATUS` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `progress_status`
--

INSERT INTO `progress_status` (`ID_PROGRESS_STATUS`, `PROGRESS_STATUS`) VALUES
(1, 'Open'),
(2, 'Progress in Hangar'),
(3, 'Progress in WSSS'),
(4, 'Progress in WSSE'),
(5, 'Progress in WSCB'),
(6, 'Progress in WSLS'),
(7, 'Progress in WSSW'),
(8, 'Close');

-- --------------------------------------------------------

--
-- Table structure for table `project`
--

CREATE TABLE `project` (
  `Id` int(11) NOT NULL,
  `customer_name` bigint(20) DEFAULT NULL,
  `aircraft_registered` varchar(255) DEFAULT NULL,
  `project_name` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `finish_date` date DEFAULT NULL,
  `location` varchar(255) DEFAULT NULL,
  `status_project` varchar(255) DEFAULT NULL,
  `type_project` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `project`
--

INSERT INTO `project` (`Id`, `customer_name`, `aircraft_registered`, `project_name`, `description`, `start_date`, `finish_date`, `location`, `status_project`, `type_project`) VALUES
(4, 2, '92dhf4534', 'All Service', 'test', '2018-02-01', '2018-02-09', 'Hangar 1', 'In Progress', 'Contract'),
(5, 3, '92dhf', 'All Service 2', 'test', '2018-02-02', '2018-02-02', 'Hangar 4', 'On Hold', 'Contract'),
(6, 4, '09281237', 'All Service 3', 'okjsjas', '2018-01-29', '2018-02-27', 'Hangar 3', 'In Progress', 'Retail'),
(7, 5, '643', 'Test', 'test', '2018-02-02', '2018-02-16', 'Hangar 1', 'Not started', 'Contract'),
(8, 6, '678495', 'lkjhgsdsd', 'dsdsd', '2018-02-02', '2018-02-21', 'Hangar 1', 'On Hold', 'Contract');

-- --------------------------------------------------------

--
-- Table structure for table `project_planning`
--

CREATE TABLE `project_planning` (
  `ID_PROJECT` int(10) NOT NULL,
  `REVISION` varchar(30) NOT NULL,
  `AREA` varchar(100) NOT NULL,
  `PHASE` varchar(100) NOT NULL,
  `START_DATE` varchar(12) NOT NULL,
  `END_DATE` varchar(12) NOT NULL,
  `UID` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `project_planning`
--

INSERT INTO `project_planning` (`ID_PROJECT`, `REVISION`, `AREA`, `PHASE`, `START_DATE`, `END_DATE`, `UID`) VALUES
(10, '1', '', '', '0000-00-00', '0000-00-00', 0),
(11, 'Hangar 4 (GAH4**C*)', '', '', '0000-00-00', '0000-00-00', 0),
(12, '', '', '', '12', '12', 0),
(13, '00002601', 'Hangar', 'Removal', '01/01/2018', '01/03/2018', 0),
(14, '00002601', 'Hangar', 'Inspection', '01/01/2018', '01/10/2018', 0),
(15, '00002601', 'Hangar', 'Rectification', '01/01/2018', '01/19/2018', 0),
(16, '00002601', 'Hangar', 'Installation', '01/01/2018', '01/25/2018', 0),
(17, '00002601', 'Hangar', 'RTS', '01/01/2018', '01/18/2018', 0),
(18, '00002601', 'WSSS', 'Rectification', '01/01/2018', '01/11/2018', 0),
(19, '00002601', 'WSSE', 'Rectification', '01/08/2018', '01/23/2018', 0),
(20, '00002601', 'WSCB', 'Rectification', '01/16/2018', '01/26/2018', 0),
(21, '00002601', 'WSLS & WSSW', 'Rectification', '01/10/2018', '01/17/2018', 0),
(22, '00002613', 'Hangar', 'Removal', '', '', 0),
(23, '00002613', 'Hangar', 'Inspection', '', '', 0),
(24, '00002613', 'Hangar', 'Rectification', '', '', 0),
(25, '00002613', 'Hangar', 'Installation', '', '', 0),
(26, '00002613', 'Hangar', 'RTS', '', '', 0),
(27, '00002613', 'WSSS', 'Rectification', '', '', 0),
(28, '00002613', 'WSSE', 'Rectification', '', '', 0),
(29, '00002613', 'WSCB', 'Rectification', '', '', 0),
(30, '00002613', 'WSLS & WSSW', 'Rectification', '', '', 0),
(31, '00002608', 'Hangar', 'Removal', '01/04/2018', '01/05/2018', 0),
(32, '00002608', 'Hangar', 'Inspection', '', '', 0),
(33, '00002608', 'Hangar', 'Rectification', '', '', 0),
(34, '00002608', 'Hangar', 'Installation', '', '', 0),
(35, '00002608', 'Hangar', 'RTS', '', '', 0),
(36, '00002608', 'WSSS', 'Rectification', '', '', 0),
(37, '00002608', 'WSSE', 'Rectification', '', '', 0),
(38, '00002608', 'WSCB', 'Rectification', '', '', 0),
(39, '00002608', 'WSLS & WSSW', 'Rectification', '', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `revision`
--

CREATE TABLE `revision` (
  `PLPL` varchar(10) NOT NULL,
  `REVISION` varchar(20) NOT NULL,
  `START_DATE` date NOT NULL,
  `REVSTTM` varchar(12) NOT NULL,
  `END_DATE` date NOT NULL,
  `REVENDTM` varchar(12) NOT NULL,
  `REV_DESC` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `swift`
--

CREATE TABLE `swift` (
  `Id` int(11) NOT NULL,
  `Functionalloc` varchar(25) NOT NULL DEFAULT '',
  `Order` varchar(254) NOT NULL DEFAULT '',
  `Order_Type` varchar(255) NOT NULL,
  `Work_center` varchar(255) NOT NULL,
  `Work` varchar(255) DEFAULT NULL,
  `Oper_Act` varchar(255) DEFAULT NULL,
  `Opr_short_text` varchar(255) DEFAULT NULL,
  `Actual_work` varchar(255) DEFAULT NULL,
  `Unit_for_work` varchar(255) DEFAULT NULL,
  `Actual_start` varchar(255) DEFAULT NULL,
  `Act_finish_date` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `technical_details_daily`
--

CREATE TABLE `technical_details_daily` (
  `Id` int(11) NOT NULL,
  `task_progress` longtext,
  `followup_result` longtext,
  `remarks` longtext,
  `id_daily` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `technical_details_daily`
--

INSERT INTO `technical_details_daily` (`Id`, `task_progress`, `followup_result`, `remarks`, `id_daily`) VALUES
(1, '<p>\n	<span style=\"color: rgb(0, 0, 0); font-family: Arial; font-size: 13px; text-align: center; white-space: pre-wrap; background-color: rgb(255, 0, 0);\">ETA 6 FEB</span></p>', '<p>\n	<span style=\"color: rgb(0, 0, 0); font-family: Arial; font-size: 13px; text-align: center; white-space: pre-wrap; background-color: rgb(255, 0, 0);\">ETA 6 FEB</span></p>', '<p>\n	<span style=\"color: rgb(0, 0, 0); font-family: Arial; font-size: 13px; text-align: center; white-space: pre-wrap; background-color: rgb(255, 0, 0);\">ETA 6 FEB</span></p>', '46'),
(2, '<p>\r\n	<span style=\"color: rgb(0, 0, 0); font-family: Arial; font-size: 13px; text-align: center; white-space: pre-wrap; background-color: rgb(255, 0, 0);\">ETA 6 FEB</span></p>\r\n', '<p>\r\n	<span style=\"color: rgb(0, 0, 0); font-family: Arial; font-size: 13px; text-align: center; white-space: pre-wrap; background-color: rgb(255, 0, 0);\">ETA 6 FEB</span></p>\r\n', '<p>\r\n	<span style=\"color: rgb(0, 0, 0); font-family: Arial; font-size: 13px; text-align: center; white-space: pre-wrap; background-color: rgb(255, 0, 0);\">ETA 6 FEB</span></p>\r\n', '47'),
(3, '<p>\r\n	<span style=\"color: rgb(0, 0, 0); font-family: Arial; font-size: 13px; text-align: center; white-space: pre-wrap; background-color: rgb(255, 0, 0);\">ETA 6 FEB</span></p>\r\n', '<p>\r\n	<span style=\"color: rgb(0, 0, 0); font-family: Arial; font-size: 13px; text-align: center; white-space: pre-wrap; background-color: rgb(255, 0, 0);\">ETA 6 FEB</span></p>\r\n', '<p>\r\n	<span style=\"color: rgb(0, 0, 0); font-family: Arial; font-size: 13px; text-align: center; white-space: pre-wrap; background-color: rgb(255, 0, 0);\">ETA 6 FEB</span></p>\r\n', '48'),
(4, '<p>\r\n	<span style=\"color: rgb(0, 0, 0); font-family: Arial; font-size: 13px; text-align: center; white-space: pre-wrap; background-color: rgb(255, 0, 0);\">ETA 6 FEB</span></p>\r\n', '<p>\r\n	<span style=\"color: rgb(0, 0, 0); font-family: Arial; font-size: 13px; text-align: center; white-space: pre-wrap; background-color: rgb(255, 0, 0);\">ETA 6 FEB</span></p>\r\n', '<p>\r\n	<span style=\"color: rgb(0, 0, 0); font-family: Arial; font-size: 13px; text-align: center; white-space: pre-wrap; background-color: rgb(255, 0, 0);\">ETA 6 FEB</span></p>\r\n', '43');

-- --------------------------------------------------------

--
-- Table structure for table `ticket`
--

CREATE TABLE `ticket` (
  `Id` int(11) NOT NULL,
  `create_date_ticket` datetime DEFAULT NULL,
  `subject` varchar(255) DEFAULT NULL,
  `department` varchar(255) DEFAULT NULL,
  `id_project` int(11) DEFAULT NULL,
  `service` varchar(255) DEFAULT NULL,
  `priority` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `last_replay` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ticket`
--

INSERT INTO `ticket` (`Id`, `create_date_ticket`, `subject`, `department`, `id_project`, `service`, `priority`, `status`, `last_replay`) VALUES
(1, '2018-02-08 15:54:06', 'DP', 'TB', 4, 'Tires', 'LOW', 'OPEN', '2018-02-08 15:54:06'),
(13, '2018-02-20 09:08:31', 'test', 'test 1', 5, 'Test 1', 'Medium', 'Close', '2018-02-20 09:08:31'),
(14, '2018-02-20 16:02:28', 'dfd', 'dfd', 4, 'f', 'Hight', 'Open Replied', '2018-02-20 16:02:28'),
(15, '2018-02-21 04:38:26', 'Test 1', 'Test 1', 7, 'Test 1', 'Hight', 'New', '2018-02-21 04:38:41'),
(16, '2018-02-21 10:05:48', 'ousud', 'sidsud', 4, 'sidsdsd', 'Medium', 'Open Replied', '2018-02-21 10:05:48');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `ID_USER` int(11) NOT NULL,
  `NAME` varchar(100) NOT NULL,
  `USERNAME` varchar(50) NOT NULL,
  `PASSWORD` varchar(50) NOT NULL,
  `EMAIL` varchar(100) NOT NULL,
  `GROUP_ID` int(1) NOT NULL,
  `CUSTOMER_ID` int(1) NOT NULL,
  `last_login` datetime NOT NULL,
  `STATUS` int(11) NOT NULL DEFAULT '0',
  `WORK_AREA_ID` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`ID_USER`, `NAME`, `USERNAME`, `PASSWORD`, `EMAIL`, `GROUP_ID`, `CUSTOMER_ID`, `last_login`, `STATUS`, `WORK_AREA_ID`) VALUES
(1, 'Admin', 'admin', '21232f297a57a5a743894a0e4a801fc3', '', 1, 0, '2018-01-07 11:00:01', 1, 0),
(2, 'PT AirSwiss', 'airasia', '', 'airasia@gmail.com', 4, 2, '0000-00-00 00:00:00', 0, 0),
(3, 'PT Lion', 'lion', '', '', 4, 3, '0000-00-00 00:00:00', 0, 0),
(4, 'PT OD', 'od', '', '', 4, 4, '0000-00-00 00:00:00', 0, 0),
(5, 'PT AirGo', 'airgo', '', '', 4, 5, '0000-00-00 00:00:00', 0, 0),
(18, 'NM', 'NM', 'e0f3dba3248a6ccb26950955635d93e2', 'NM', 4, 3, '0000-00-00 00:00:00', 0, 2),
(19, 'asa', 'asa', '457391c9c82bfdcbb4947278c0401e41', 'asa', 4, 2, '0000-00-00 00:00:00', 0, 2),
(20, 'HUHU', 'HUHU', 'd9be7d75cf68eeeff166219a1d6cdf71', 'HUHU', 4, 3, '0000-00-00 00:00:00', 0, 3),
(21, 'vbn', 'vbn', 'b5de674a38a691fb24d04233e8aa498b', 'vbn', 4, 3, '0000-00-00 00:00:00', 1, 3);

-- --------------------------------------------------------

--
-- Table structure for table `user_group`
--

CREATE TABLE `user_group` (
  `ID_USER_GROUP` int(11) NOT NULL,
  `USER_GROUP` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_group`
--

INSERT INTO `user_group` (`ID_USER_GROUP`, `USER_GROUP`) VALUES
(1, 'Administrator'),
(2, 'TRADER'),
(3, 'MANAGER'),
(4, 'CUSTOMER');

-- --------------------------------------------------------

--
-- Table structure for table `work_area`
--

CREATE TABLE `work_area` (
  `ID_WORK_AREA` int(3) NOT NULL,
  `WORK_AREA` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `work_area`
--

INSERT INTO `work_area` (`ID_WORK_AREA`, `WORK_AREA`) VALUES
(1, 'Hangar 1'),
(2, 'Hangar 3'),
(3, 'Hangar 4');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `area`
--
ALTER TABLE `area`
  ADD PRIMARY KEY (`ID_AREA`);

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`ID_CUSTOMER`);

--
-- Indexes for table `daily_day`
--
ALTER TABLE `daily_day`
  ADD PRIMARY KEY (`Id`),
  ADD UNIQUE KEY `create_date_daily` (`create_date_daily`,`area`);

--
-- Indexes for table `daily_report`
--
ALTER TABLE `daily_report`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `detail_main_project`
--
ALTER TABLE `detail_main_project`
  ADD PRIMARY KEY (`ID_PHASE`);

--
-- Indexes for table `info`
--
ALTER TABLE `info`
  ADD PRIMARY KEY (`GENERAL_INFO`);

--
-- Indexes for table `jobcard`
--
ALTER TABLE `jobcard`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `order` (`order`),
  ADD UNIQUE KEY `seq` (`seq`,`id_project`);

--
-- Indexes for table `mdr`
--
ALTER TABLE `mdr`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `mdr_order` (`mdr_order`),
  ADD UNIQUE KEY `seq` (`seq`,`id_project`);

--
-- Indexes for table `mrm`
--
ALTER TABLE `mrm`
  ADD PRIMARY KEY (`no`);

--
-- Indexes for table `m_plant`
--
ALTER TABLE `m_plant`
  ADD PRIMARY KEY (`ID_PLANT`);

--
-- Indexes for table `order_list`
--
ALTER TABLE `order_list`
  ADD PRIMARY KEY (`ORDER_NO`);

--
-- Indexes for table `progress_status`
--
ALTER TABLE `progress_status`
  ADD PRIMARY KEY (`ID_PROGRESS_STATUS`);

--
-- Indexes for table `project`
--
ALTER TABLE `project`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `project_planning`
--
ALTER TABLE `project_planning`
  ADD PRIMARY KEY (`ID_PROJECT`);

--
-- Indexes for table `revision`
--
ALTER TABLE `revision`
  ADD PRIMARY KEY (`REVISION`);

--
-- Indexes for table `swift`
--
ALTER TABLE `swift`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `technical_details_daily`
--
ALTER TABLE `technical_details_daily`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `ticket`
--
ALTER TABLE `ticket`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`ID_USER`);

--
-- Indexes for table `user_group`
--
ALTER TABLE `user_group`
  ADD PRIMARY KEY (`ID_USER_GROUP`);

--
-- Indexes for table `work_area`
--
ALTER TABLE `work_area`
  ADD PRIMARY KEY (`ID_WORK_AREA`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `area`
--
ALTER TABLE `area`
  MODIFY `ID_AREA` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `customer`
--
ALTER TABLE `customer`
  MODIFY `ID_CUSTOMER` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `daily_day`
--
ALTER TABLE `daily_day`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;

--
-- AUTO_INCREMENT for table `daily_report`
--
ALTER TABLE `daily_report`
  MODIFY `ID` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=147;

--
-- AUTO_INCREMENT for table `detail_main_project`
--
ALTER TABLE `detail_main_project`
  MODIFY `ID_PHASE` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `jobcard`
--
ALTER TABLE `jobcard`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=73;

--
-- AUTO_INCREMENT for table `mdr`
--
ALTER TABLE `mdr`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT for table `mrm`
--
ALTER TABLE `mrm`
  MODIFY `no` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=120;

--
-- AUTO_INCREMENT for table `m_plant`
--
ALTER TABLE `m_plant`
  MODIFY `ID_PLANT` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `progress_status`
--
ALTER TABLE `progress_status`
  MODIFY `ID_PROGRESS_STATUS` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `project`
--
ALTER TABLE `project`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `project_planning`
--
ALTER TABLE `project_planning`
  MODIFY `ID_PROJECT` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT for table `swift`
--
ALTER TABLE `swift`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `technical_details_daily`
--
ALTER TABLE `technical_details_daily`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `ticket`
--
ALTER TABLE `ticket`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `ID_USER` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `user_group`
--
ALTER TABLE `user_group`
  MODIFY `ID_USER_GROUP` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `work_area`
--
ALTER TABLE `work_area`
  MODIFY `ID_WORK_AREA` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
