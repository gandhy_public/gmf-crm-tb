(function($) {
    var data = {
        activeCell: null,
        selects: {},
        input: {},
        selectCols: [],
        editableCols: {},
        cellType: {},
        cellTypeCols: [],
        isSelector: false,
        defalutInput: null,
        ajax: {}
    };
    $.fn.tableEdit = function(options) {
        // console.log(options);
        var defalutOptions = {};
        var inputShow;
        var className = "editablegrid-";
        var idPK;
        var cellParent;
        var indexParent;
        var value;
        //data for update
        var entryData = {
            tablename: null,
            id: null,
            newvalue: null,
            oldvalue: null,
            colname: null,
            coltype: null
        };
        options = $.extend(defalutOptions, options);
        entryData.tablename = this.attr('name');
        var wrapper = this.wrap('<div id="table-editable"></div>').parent();
        wrapper.append('<input \
                    class="editable-input" type="text" \
                    style="box;display:none; \
                    ">');
        data.editableCols = options.editableCols;
        data.cellType = options.cellType;
        data.ajax = options.ajax;
        var selectCols = data.selectCols = Object.keys(options.select); //An array of column numbers that display the selection box
        var cellTypeCols = data.cellTypeCols = Object.keys(options.cellType[0]);
        if (selectCols.length > 0) {
            for (var i = 0, len = selectCols.length; i < len; i++) {
                var colNum = selectCols[i];
                var selectOptions = options.select[colNum];
                var select = data.selects[colNum] = createSelect(selectOptions, colNum);
                wrapper.append(select);
            }
        }
        if (cellTypeCols.length > 0) {
            for (var i = 0, len = cellTypeCols.length; i < len; i++) {
                var colNum = cellTypeCols[i];
                var TypeData = options.cellType[0][colNum];
                var inputonly = data.input[colNum] = createInput(TypeData);
                wrapper.append(inputonly);
            }
        }
        var input = wrapper.find('input.editable-input');
        data.defalutInput = input[0];
        var parentbox = "";
        var parentheader = "";
        var wrapPos = "";
        //clearing input
        $(document).on('click', function(event) {
            if (!$(event.target).hasClass('editable-input') && !$(event.target).is("td")) {
                //get value and send to td
                if (data.activeCell) {
                    cellParent = data.activeCell;
                    updateData();
                    var inputValue = input.val();
                    if (input.attr('type') == 'date') {
                        inputValue = moment(inputValue, 'YYYY-MM-DD').format('DD MMM YYYY');
                    }
                    $(data.activeCell).html('').append(inputValue); //datatable need hardcode
                    data.activeCell = null;
                    input.hide();
                }
            }
        });
        wrapper.on('keydown', 'select.editable-input,input.editable-input,textarea.editable-input', function(event) {
            switch (event.keyCode) {
                case 38:
                    if (!$(event.target).is("textarea") && (!$(event.target).is("input") && !$(event.target).attr('type') == 'date'))
                        event.preventDefault();
                    break;
                case 37:
                    if ($(event.target).is("select"))
                        event.preventDefault();
                    break;
                case 40:
                    if (!$(event.target).is("textarea") && (!$(event.target).is("input") && !$(event.target).attr('type') == 'date'))
                        event.preventDefault();
                    break;
                case 39:
                    if ($(event.target).is("select"))
                        event.preventDefault();
                    break;
                case 13:
                    if ($(event.target).is("textarea"))
                        event.preventDefault();
                    break;
                case 9:
                    break;
            }
        });
        wrapper.on('click', 'td', function(event) {
            if ($(event.target).is('a') || $(event.target).is('button'))
                return;
            if ($(event.target).hasClass('editable-input') && ($(event.target).is("input") || $(event.target).is("select") || $(event.target).is("textarea"))) {
                this.focus();
                return;
            }
            var cell = event.target;
            var $cellPos = $(this);
            wrapPos = wrapper.position();
            var cellPos = $cellPos.position();
            var parent = this.parentElement.parentElement;
            parentbox = $(this).parent().parent().parent().parent();
            parentheader = $(this).parent().parent().parent().parent().prev();
            var index = getIndexOf(cell);
            if (data.activeCell) { //Update the value of the cell to the value of input before moving the input
                cellParent = data.activeCell;
                updateData();
                var inputValue = input.val();
                if (input.attr('type') == 'date' && inputValue != "") {
                    inputValue = moment(inputValue, 'YYYY-MM-DD').format('DD MMM YYYY');
                }
                $(data.activeCell).html('').append(inputValue); //datatable need harcode
                input.val("");
            }
            data.activeCell = cell;
            var position = cell.getBoundingClientRect();
            value = cell.innerHTML;
            if (options.cellType[0][index] == 'date') {
                value = moment(value, 'DD MMM YYYY').format('YYYY-MM-DD');
            }
            if (data.selectCols.indexOf(String(index)) !== -1) {
                $(data.selects[index]).val("");
                //create new every
                input = $(data.selects[index]);
                data.isSelector = true;
            } else {
                input = $(data.input[index]);
                data.isSelector = false;
            }
            input.val("");
            if (value) { //Value Input
                if (value.indexOf('<span>') != -1) {
                    value = $(value).html();
                }
                input.val(value);
            }
            input.css({
                width: position.width + 'px',
                height: position.height - 16 + 'px',
            })[0];
            inputShow = $(input[0]);
            if ($(inputShow).is('select')) {
                //check is value exist in option inputshow
                if (!$(inputShow).find("option[value='" + value + "']").length) {
                    $(inputShow).prepend("<option value='" + value + "' selected='selected'>" + value + "</option>");
                }
                //if not add to first option
            }
            if (value) { //Value Input
                if (value.indexOf('<span>') != -1) {
                    value = $(value).html();
                }
                inputShow.val(value);
            }
            $('table.dataTable td').removeClass('edited');
            entryData.oldvalue = inputShow.val();
            if (!$(this).hasClass('focus')) {
                $('table.dataTable td').removeClass('focus');
                $(this).addClass('focus');
            } else {
                //change click to keypress enter
                if (event.originalEvent === undefined) var a = "";
                else {
                    var e = jQuery.Event("keydown");
                    e.keyCode = 13;
                    $(this).trigger(e);
                    return;
                }
                $('table.dataTable td').removeClass('focus');
                $('table.dataTable td').removeClass('edited');
                if (data.editableCols[0][index]) {
                    $(this).addClass('edited');
                    $(this).html('').append(inputShow);
                    inputShow.show();
                    $('.select2EditDT-' + index).select2();
                    $('.select2EditDT-' + index).select2("open");
                } else {
                    //add box shadow
                    $(this).addClass('focus');
                }
            }
            movescroll(event.target, kc);
        });
        $(function() { /* DOM ready */
            $("select.editable-input").change(function() {
                var toActiveCell;
                toActiveCell = findCell(data.activeCell, 'ENTER');
                if (toActiveCell) {
                    $(toActiveCell).trigger('click');
                    // movescroll(toActiveCell, event.keyCode);
                }
            });
        });
        $(document).on('change', 'select.editable-input', function() {
            // var toActiveCell;
            // toActiveCell = findCell(data.activeCell, 'ENTER');
            // if (toActiveCell) {
            //     $(toActiveCell).trigger('click');
            // }
        });
        $('select.editable-input,input.editable-input').keydown(function(event) {
            switch (event.keyCode) {
                case 38:
                    event.preventDefault();
                    break;
                case 37:
                    break;
                case 40:
                    event.preventDefault();
                    break;
                case 39:
                    break;
                case 13:
                    // event.preventDefault();
                    break;
                case 9:
                    break;
            }
        });
        wrapper.on('click.select', 'select', function(event) {
            event.stopPropagation();
        });
        wrapper.on('click.input', 'input.editable-input', function(event) {
            event.stopPropagation();
        });

        function updateData() {
            entryData.newvalue = inputShow.val();
            var $idPK = $(cellParent).parent().find('td').eq(1).html();
            var $idPK2 = $(cellParent).parent().find('td').eq(1);
            var idp;
            if ($idPK) {
                if ($idPK.indexOf('<a') != -1) {
                    idp = $idPK2.find('a').html();
                } else {
                    idp = $idPK2.html();
                }
            }
            entryData.id = idp;
            var $th = $(cellParent).parent().parent().prev();
            if (entryData.oldvalue != entryData.newvalue) {
                // data.activeCell=
                indexParent = getIndexOf(cellParent);
                $(data.activeCell).html('').append('<span class="last-editable-input">' + entryData.newvalue + '</span>');
                entryData.colname = $th.find('th').eq(indexParent).attr('class').split(" ")[0].replace(className, "");
                $.ajax({
                        url: data.ajax.url,
                        type: data.ajax.type,
                        dataType: data.ajax.dataType,
                        method: 'POST',
                        cache: false,
                        data: entryData
                    })
                    .done(function(json) {
                        json = json.replace(/(\r\n\t|\n|\r\t)/gm, "")
                        // console.log("Plugin : " + json);
                        data.ajax.success(json);
                    }).fail(function(a) {
                        data.ajax.error(a);
                    });
            }
        }
        $(document).keypress(function(e) {
            if (e.which == 13 && $(e.target).is('select')) { //If "Enter" Pressed.
                //Submit form 
            }
        });
        //Listening for keyboard events
        var kc;
        $(document).keydown(function(event) {
            // console.log(event.target, event.keyCode);
            if (!$(event.target).is(":focus") && ($(event.target).hasClass('editable-input') || $(event.target).is("td") || $(event.target).is('input.select2-search__field') || $(event.target).is("body"))) {
                kc = event.keyCode;
                var toActiveCell;
                switch (event.keyCode) {
                    case 38:
                        toActiveCell = findCell(data.activeCell, 'UP');
                        break;
                    case 37:
                        toActiveCell = findCell(data.activeCell, 'LEFT');
                        break;
                    case 40:
                        toActiveCell = findCell(data.activeCell, 'DOWN');
                        break;
                    case 39:
                        toActiveCell = findCell(data.activeCell, 'RIGHT');
                        break;
                    case 13:
                        if ($(event.target).is('input.select2-search__field')) {
                            toActiveCell = findCell(data.activeCell, 'ENTER');
                            break;
                        }
                        $(data.activeCell).trigger('click');
                        $(data.activeCell).children().focus();
                        if ($(data.activeCell).children().is("textarea"))
                            event.preventDefault();
                        break;
                    case 27:
                        if ($(event.target).is('input.select2-search__field')) {
                            toActiveCell = findCell(data.activeCell, 'ENTER');
                            break;
                        }
                        $(data.activeCell).trigger('click');
                        $(data.activeCell).children().focus();
                    case 9:
                        toActiveCell = findCell(data.activeCell, 'TAB');
                        break;
                }
                if (toActiveCell) {
                    $(toActiveCell).trigger('click');
                }
            } else if ($(event.target).is(":focus")) {
                var toActiveCell;
                switch (event.keyCode) {
                    case 13:
                        toActiveCell = findCell(data.activeCell, 'ENTER');
                        break;
                    case 9:
                        toActiveCell = findCell(data.activeCell, 'TAB');
                        break;
                }
                if (toActiveCell) {
                    $(toActiveCell).trigger('click');
                }
            }
        });
    };
    var i = 0;
    //Find the next cell of the current cell
    function findCell(cell, direction) {
        if (!cell) {
            return;
        }
        var line = cell.parentElement,
            index = [].indexOf.call(line.children, cell);
        switch (direction) {
            case 'DOWN':
                var nextLine = line.nextElementSibling;
                if (nextLine) {
                    return $(nextLine).children()[index];
                } else {
                    return null;
                }
            case 'UP':
                var prevLine = line.previousElementSibling;
                if (prevLine) {
                    return $(prevLine).children()[index];
                } else {
                    return null;
                }
            case 'LEFT':
                return cell.previousElementSibling;
            case 'RIGHT':
                return cell.nextElementSibling;
            case 'TAB':
                return cell.nextElementSibling;
            case 'ENTER':
                var nextLine = line.nextElementSibling;
                if (nextLine) {
                    return $(nextLine).children()[index];
                } else {
                    return null;
                }
        }
    }
    //Create a selection box
    function createSelect(options, colNum) {
        if (Object.prototype.toString.call(options) !== '[object Array]') {
            throw new Error('ErrorCreateSelect');
        }
        var htmlStr = '<select class="select2EditDT-' + colNum + ' editable-input" style="display:none;" >';
        for (var i = 0, len = options.length; i < len; i++) {
            htmlStr += '<option value="' + options[i].value + '">' + options[i].label + '</option>';
        }
        htmlStr += '</select>';
        return $(htmlStr)[0];
    }
    //Create a input
    function createInput(options) {
        var htmlStr;
        if (options == 'html') {
            htmlStr = '<textarea  \
                    class="editable-input" style="display:none;"  \
                    ></textarea>';
        } else {
            if (options == "string") {
                options = "text"
            } else if (options == "integer") {
                options = "number";
            }
            htmlStr = '<input type="' + options + '" style="display:none;" \
                    class="editable-input" type="text" \
                    />';
        }
        return $(htmlStr)[0];
    }
    //Get the index of the cell;
    function getIndexOf(cell) {
        return [].indexOf.call(cell.parentElement.children, cell);
    }

    function getColNameHeader(cell) {
        return [].indexOf.call(cell.parentElement.children, cell);
    }

    function movescroll(cell, direction) {
        var posc = cell.getBoundingClientRect();
        var scb = $(".dataTables_scrollBody");
        var tb = $(".dataTables_scrollBody table");
        var fc = $('.DTFC_LeftBodyWrapper');
        var d = $(document);
        var w = $(window)
        var dt = $(document).scrollTop();
        if (scb.length > 0) {
            var poscroll = $(scb).scrollLeft();
            var poswt = $(d).scrollTop();
            var wb = $(w).height();
            var sw = $(scb).scrollWidth;
            var dr = $(scb).offset().left + $(scb).width();
            var dl = $(scb).offset().left;
            var dt = $(scb).offset().top;
            var db = $(scb).offset().top + $(scb).height();
            var cellwidth = $(cell).outerWidth(true);
            var cellheight = $(cell).outerHeight(true);
            var xl = posc.left;
            var xr = posc.left + cellwidth;
            var yt = posc.top;
            var yb = posc.top + $(cell).height();
            //with X right
            if (direction == 39 && ((dr - xr) <= cellwidth)) {
                var addition = 0;
                if (xr > dr)
                    addition = xr - dr;
                $(scb).animate({
                    scrollLeft: poscroll + cellwidth + addition
                }, 200);
            }
            //with X Left
            var fcl = 0;
            if (fc.length > 0)
                fcl = $(fc).outerWidth(true);
            if (direction == 37 && ((xl - (dl + fcl)) <= cellwidth)) {
                var addition = 0;
                if ((dl + fcl) > xl)
                    addition = (dl + fcl) - xl;
                $(scb).animate({
                    scrollLeft: poscroll - cellwidth - addition
                }, 200);
            }
            //with Y Top
            var ht = $(".fixedHeader-floating");
            if (ht.length > 0) {
                htb = $(ht).outerHeight(true);
                if (direction == 38 && ((yt - htb) <= cellheight)) {
                    $(d).scrollTop(poswt);
                    $('html,body').animate({
                        scrollTop: poswt - cellheight
                    }, 100);
                }
            }
            //with Y Buttom
            if (direction == 40 && ((wb - 22 - yb) <= (cellheight))) {
                var addition = 0;
                $(d).scrollTop(poswt);
                if (yb > (wb - 22))
                    addition = yb - (wb - 22);
                $('html,body').animate({
                    scrollTop: poswt + cellheight + addition
                }, 200);
            }
        }
    }
})(jQuery);